PROJECT_ROOT=/ichec/home/users/nix/gaia/GossipContracts

export EXP=exp6
for seed in {2..3}
do
	export SEED=$seed
	sbatch --job-name=$EXP.$SEED --output=$EXP.$SEED.out $PROJECT_ROOT/kay/snap4Islands.sbatch
done
