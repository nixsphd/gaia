#!/bin/bash

ISLAND="$1"
export SEED0=`expr $SEED \\* 100 + $ISLAND \\* 10 + 0`

if [ "$ISLAND" -eq "0" ]; then
	echo "Experiment (Island Server and Client $ISLAND_PORT$ISLAND Master $MASTER_IPADDRESS:$MASTER_PORT$ISLAND) $ECJ_CONFIG_FILE $LOG_DIR $SEED0 $SEED1"
	java -cp $CLASSPATH ec.Evolve \
		-file $ECJ_CONFIG_FILE \
		-p exch.i-am-server=true \
		-p exch.server-addr=$ISLAND_IPADDRESS \
		-p exch.id=Island$ISLAND \
		-p exch.client-port=$ISLAND_PORT$ISLAND \
		-p seed.0=$SEED0 \
		-p checkpoint-directory=$LOG_DIR \
		-p checkpoint-prefix=island$ISLAND \
		-p stat.file=$LOG_DIR/master$ISLAND.stat \
		-p stat.child.0.file=$LOG_DIR/koza$ISLAND.stat \
		-p stat.child.1.file=$LOG_DIR/egc$ISLAND.stat \
		-p eval.masterproblem=ie.nix.ecj.PeerSimProblem\$MasterProblem \
		-p eval.masterproblem.max-jobs-per-slave=2 \
		-p eval.master.host=$MASTER_IPADDRESS \
		-p eval.master.port=$MASTER_PORT$ISLAND \
		-p eval.compression=true \
		-p silent=false;
else
	echo "Experiment (Island Client $ISLAND_PORT$ISLAND Master $MASTER_IPADDRESS:$MASTER_PORT$ISLAND) $ECJ_CONFIG_FILE $LOG_DIR $SEED0 $SEED1"
	java -cp $CLASSPATH ec.Evolve \
		-file $ECJ_CONFIG_FILE \
		-p exch.server-addr=$ISLAND_IPADDRESS \
		-p exch.id=Island$ISLAND \
		-p exch.client-port=$ISLAND_PORT$ISLAND \
		-p seed.0=$SEED0 \
		-p checkpoint-directory=$LOG_DIR \
		-p stat.file=$LOG_DIR/master$ISLAND.stat \
		-p stat.child.0.file=$LOG_DIR/koza$ISLAND.stat \
		-p stat.child.1.file=$LOG_DIR/egc$ISLAND.stat \
		-p eval.masterproblem=ie.nix.ecj.PeerSimProblem\$MasterProblem \
		-p eval.masterproblem.max-jobs-per-slave=2 \
		-p eval.master.host=$MASTER_IPADDRESS \
		-p eval.master.port=$MASTER_PORT$ISLAND \
		-p eval.compression=true \
		-p silent=false;
fi