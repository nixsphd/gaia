#!/bin/bash

echo "Experiment (Slave $MASTER_IPADDRESS:$MASTER_PORT) $ECJ_CONFIG_FILE"

java -cp $CLASSPATH ec.eval.Slave \
	-file $ECJ_CONFIG_FILE\
	-p seed.0=$SEED\
	-p eval.masterproblem=ie.nix.ecj.PeerSimProblem\$MasterProblem\
	-p eval.masterproblem.max-jobs-per-slave=1\
	-p eval.master.host=$MASTER_IPADDRESS\
	-p eval.master.port=$MASTER_PORT\
	-p eval.slave.silent=true