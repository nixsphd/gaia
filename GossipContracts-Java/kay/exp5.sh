PROJECT_ROOT=/ichec/home/users/nix/gaia/GossipContracts

export EXP=exp5
for seed in {0..20}
do
	export SEED=$seed
	sbatch --job-name=$EXP.$SEED --output=$EXP.$SEED.out $PROJECT_ROOT/kay/snap.sbatch
done
