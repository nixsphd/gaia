#!/bin/bash

echo "Experiment (Test) $ECJ_CONFIG_FILE $LOG_DIR"

java -cp $CLASSPATH ec.Evolve \
	-file $ECJ_CONFIG_FILE\
	-p seed.0=$SEED\
	-p checkpoint-directory=$LOG_DIR\
	-p stat.file=$LOG_DIR/master.stat\
	-p stat.child.0.file=$LOG_DIR/koza.stat\
	-p stat.child.1.file=$LOG_DIR/egc.stat\
	-p eval.problem.logging=console\
	-p silent=false