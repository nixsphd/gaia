#!/bin/sh 

#SBATCH --time=12:00:00
#SBATCH --nodes=1
#SBATCH -A ngcom014c
#SBATCH -p ProdQ

module load java/11 taskfarm

#export PROJECT_ROOT=/Users/nicolamcdonnell/Desktop/Gaia/GossipContracts
export PROJECT_ROOT=/ichec/home/users/nix/gaia/GossipContracts
export CLASSPATH=$PROJECT_ROOT/bin/main:$PROJECT_ROOT/build/resources/main:$PROJECT_ROOT/lib/*
export MASTER_IPADDRESS=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)
export MASTER_PORT=12358
export LOG_DIR=$PROJECT_ROOT/results/$SLURM_JOB_ID
export ECJ_CONFIG_FILE=$PROJECT_ROOT/src/main/resources/ie/nix/ecj/automata/gp/Automata.params
export PEERSIM_CONFIG_FILE=$PROJECT_ROOT/src/test/resources/ie/nix/ecj/automata/Automata.properties

mkdir -p $LOG_DIR
cp $ECJ_CONFIG_FILE $LOG_DIR
cp $PEERSIM_CONFIG_FILE $LOG_DIR
cd $PROJECT_ROOT

taskfarm kay/master_slave_tasks