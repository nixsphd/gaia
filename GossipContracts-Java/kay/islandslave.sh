#!/bin/bash

ISLAND="$1"

echo "Experiment (Island $ISLAND Master $MASTER_IPADDRESS:$MASTER_PORT$ISLAND) $ECJ_CONFIG_FILE $LOG_DIR $SEED$ISLAND"

java -cp $CLASSPATH ec.eval.Slave \
	-file $ECJ_CONFIG_FILE \
	-p exch.server-addr=$ISLAND_IPADDRESS \
	-p exch.id=Island$ISLAND \
	-p exch.client-port=$ISLAND_PORT$ISLAND \
	-p seed.0=$SEED$ISLAND \
	-p eval.masterproblem=ie.nix.ecj.PeerSimProblem\$MasterProblem \
	-p eval.masterproblem.max-jobs-per-slave=2 \
	-p eval.master.host=$MASTER_IPADDRESS \
	-p eval.master.port=$MASTER_PORT$ISLAND \
	-p eval.compression=true \
	-p eval.slave.silent=false