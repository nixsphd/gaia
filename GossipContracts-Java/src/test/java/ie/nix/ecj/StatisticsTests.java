package ie.nix.ecj;	

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Permission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.EvolutionState;
import ec.Individual;
import ie.nix.ecj.Statistics.KozaFitnessWithStats;

public class StatisticsTests {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String TEST_EGC_AGENT_PARAMS = "src/test/resources/ie/nix/egc/TestEGCAgent.params";
	
	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {
	}

	public static void customSecurityManager() {
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}
	
	@Test()
	@DisplayName("Fitness Test")
	public void fitnessTest() {

		EvolutionState state = new EvolutionState();
		
		KozaFitnessWithStats kozaFitnessWithStats = new KozaFitnessWithStats();
		KozaFitnessWithStats kozaFitnessWithStats2 = new KozaFitnessWithStats();
		
		kozaFitnessWithStats.setStandardizedFitness(state, 10);
		
		writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2);

		LOGGER.debug("After kozaFitnessWithStats={}", kozaFitnessWithStats.fitness());
		LOGGER.debug("After kozaFitnessWithStats2={}", kozaFitnessWithStats2.fitness());
		
		Assertions.assertEquals(kozaFitnessWithStats.fitness(), kozaFitnessWithStats2.fitness(),
				"The fitnesses are different sizes kozaFitnessWithStats=" + kozaFitnessWithStats.fitness()
						+ " and kozaFitnessWithStats2=" + kozaFitnessWithStats.fitness());
	}
	
	@Test()
	@DisplayName("Stat Count Test")
	public void statCountTest() {

		EvolutionState state = new EvolutionState();
		
		KozaFitnessWithStats kozaFitnessWithStats = new KozaFitnessWithStats();
		KozaFitnessWithStats kozaFitnessWithStats2 = new KozaFitnessWithStats();
		
		kozaFitnessWithStats.recordStat("statName", 1);
		
		writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2);

		LOGGER.debug("After kozaFitnessWithStats={}", kozaFitnessWithStats.getStatsMap().size());
		LOGGER.debug("After kozaFitnessWithStats2={}", kozaFitnessWithStats2.getStatsMap().size());

		Assertions.assertEquals(kozaFitnessWithStats.statsMap.size(), kozaFitnessWithStats2.getStatsMap().size(),
				"The stats are different sizes kozaFitnessWithStats=" + kozaFitnessWithStats.getStatsMap().size()
						+ " and kozaFitnessWithStats2=" + kozaFitnessWithStats.getStatsMap().size());

	}
	
	@Test()
	@DisplayName("Stat Name Test")
	public void statNameTest() {

		EvolutionState state = new EvolutionState();
		
		KozaFitnessWithStats kozaFitnessWithStats = new KozaFitnessWithStats();
		KozaFitnessWithStats kozaFitnessWithStats2 = new KozaFitnessWithStats();
		
		kozaFitnessWithStats.recordStat("statName", 1);
		
		writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2);

		LOGGER.debug("After kozaFitnessWithStats={}", kozaFitnessWithStats.getStatsMap().keySet().toArray()[0]);
		LOGGER.debug("After kozaFitnessWithStats2={}", kozaFitnessWithStats2.getStatsMap().keySet().toArray()[0]);

		Assertions.assertEquals(kozaFitnessWithStats.getStatsMap().keySet().toArray()[0], 
				kozaFitnessWithStats2.getStatsMap().keySet().toArray()[0],
				"The stats are different sizes kozaFitnessWithStats=" + 
				kozaFitnessWithStats.getStatsMap().keySet().toArray()[0] + 
				" and kozaFitnessWithStats2=" + 
				kozaFitnessWithStats.getStatsMap().keySet().toArray()[0]);
	}

	@Test()
	@DisplayName("Stat Value Test")
	public void statValueTest() {

		EvolutionState state = new EvolutionState();

		KozaFitnessWithStats kozaFitnessWithStats = new KozaFitnessWithStats();
		KozaFitnessWithStats kozaFitnessWithStats2 = new KozaFitnessWithStats();

		String statName = "statName";

		kozaFitnessWithStats.recordStat(statName, 1);
		kozaFitnessWithStats.recordStat(statName, 5);
		kozaFitnessWithStats.recordStat(statName, 10);

		writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2);

		checkStat(kozaFitnessWithStats, kozaFitnessWithStats2, statName);
	}
	
	@Test()
	@DisplayName("Two Stat Value Test")
	public void twoStatValueTest() {

		EvolutionState state = new EvolutionState();

		KozaFitnessWithStats kozaFitnessWithStats = new KozaFitnessWithStats();
		KozaFitnessWithStats kozaFitnessWithStats2 = new KozaFitnessWithStats();

		String statName = "statName";
		String statName2 = "statName2";

		kozaFitnessWithStats.recordStat(statName, 1);
		kozaFitnessWithStats.recordStat(statName, 5);
		kozaFitnessWithStats.recordStat(statName, 10);
		
		kozaFitnessWithStats.recordStat(statName2, -1);
		kozaFitnessWithStats.recordStat(statName2, 50);
		kozaFitnessWithStats.recordStat(statName2, 100);

		writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2);

		checkStat(kozaFitnessWithStats, kozaFitnessWithStats2, statName);
		checkStat(kozaFitnessWithStats, kozaFitnessWithStats2, statName2);
	}

	@Test()
	@DisplayName("Objective Fitness Count Test")
	public void objectiveFitnessCountTest() {
	
		EvolutionState state = new EvolutionState();
		
		KozaFitnessWithStats kozaFitnessWithStats = new KozaFitnessWithStats();
		KozaFitnessWithStats kozaFitnessWithStats2 = new KozaFitnessWithStats();
		
		kozaFitnessWithStats.recordObjectiveFitnesses("evaluation", 1d);
		
		writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2);
	
		LOGGER.debug("After kozaFitnessWithStats={}", kozaFitnessWithStats.getObjectiveFitnessesMap().size());
		LOGGER.debug("After kozaFitnessWithStats2={}", kozaFitnessWithStats2.getObjectiveFitnessesMap().size());
	
		Assertions.assertEquals(kozaFitnessWithStats.getObjectiveFitnessesMap().size(), 
				kozaFitnessWithStats2.getObjectiveFitnessesMap().size(),
				"The stats are different sizes kozaFitnessWithStats=" 
				+ kozaFitnessWithStats.getObjectiveFitnessesMap().size()
				+ " and kozaFitnessWithStats2=" 
				+ kozaFitnessWithStats.getObjectiveFitnessesMap().size());
	
	}
	
	@Test()
	@DisplayName("Two Objective Fitness Test")
	public void twoObjectiveFitnessesTest() {

		String objectiveName1 = "evaluation";
		String objectiveName2 = "convergence-time";
		EvolutionState state = new EvolutionState();

		KozaFitnessWithStats kozaFitnessWithStats = new KozaFitnessWithStats();
		KozaFitnessWithStats kozaFitnessWithStats2 = new KozaFitnessWithStats();

		kozaFitnessWithStats.recordObjectiveFitnesses(objectiveName1, 1d);
		kozaFitnessWithStats.recordObjectiveFitnesses(objectiveName2, 100d);

		writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2);

		checkObjectiveFitnesses(kozaFitnessWithStats, kozaFitnessWithStats2, objectiveName1);
		checkObjectiveFitnesses(kozaFitnessWithStats, kozaFitnessWithStats2, objectiveName2);
	}

	public void writeReadFitness(EvolutionState state, KozaFitnessWithStats kozaFitnessWithStats, 
			KozaFitnessWithStats kozaFitnessWithStats2) {
	
		OutputStream outputStream = new ByteArrayOutputStream();
		DataOutput dataOutput = new DataOutputStream(outputStream);
		
		try {
			kozaFitnessWithStats.writeFitness(state, dataOutput);
			((OutputStream) dataOutput).flush();
			
			InputStream inputStream = new ByteArrayInputStream(((ByteArrayOutputStream)outputStream).toByteArray());
			DataInput dataInput = new DataInputStream(inputStream);
	        
			kozaFitnessWithStats2.readFitness(state, dataInput);
	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void checkStat(KozaFitnessWithStats kozaFitnessWithStats, 
			KozaFitnessWithStats kozaFitnessWithStats2, String statName) {
		LOGGER.debug("After kozaFitnessWithStats={}", 
				kozaFitnessWithStats.getStatsMap().get(statName));
		LOGGER.debug("After kozaFitnessWithStats2={}", 
				kozaFitnessWithStats2.getStatsMap().get(statName));

		Assertions.assertEquals(kozaFitnessWithStats.getStatsMap().get(statName).getCount(),
				kozaFitnessWithStats2.getStatsMap().get(statName).getCount(),
				"The stats valus have different counts kozaFitnessWithStats="
						+ kozaFitnessWithStats.getStatsMap().get(statName).getCount() 
						+ " and kozaFitnessWithStats2="
						+ kozaFitnessWithStats.getStatsMap().get(statName).getCount());

		Assertions.assertEquals(kozaFitnessWithStats.getStatsMap().get(statName).getMin(),
				kozaFitnessWithStats2.getStatsMap().get(statName).getMin(),
				"The stats valus have different min kozaFitnessWithStats="
						+ kozaFitnessWithStats.getStatsMap().get(statName).getMin() 
						+ " and kozaFitnessWithStats2="
						+ kozaFitnessWithStats.getStatsMap().get(statName).getMin());

		Assertions.assertEquals(kozaFitnessWithStats.getStatsMap().get(statName).getMax(),
				kozaFitnessWithStats2.getStatsMap().get(statName).getMax(),
				"The stats valus have different max kozaFitnessWithStats="
						+ kozaFitnessWithStats.getStatsMap().get(statName).getMax() + " and kozaFitnessWithStats2="
						+ kozaFitnessWithStats.getStatsMap().get(statName).getMax());

		Assertions.assertEquals(kozaFitnessWithStats.getStatsMap().get(statName).getSum(),
				kozaFitnessWithStats2.getStatsMap().get(statName).getSum(),
				"The stats valus have different sum kozaFitnessWithStats="
						+ kozaFitnessWithStats.getStatsMap().get(statName).getSum() + " and kozaFitnessWithStats2="
						+ kozaFitnessWithStats.getStatsMap().get(statName).getSum());
	}
	
	public void checkObjectiveFitnesses(KozaFitnessWithStats kozaFitnessWithStats,
			KozaFitnessWithStats kozaFitnessWithStats2, String objectiveName) {
		LOGGER.debug("After kozaFitnessWithStats={}", 
				kozaFitnessWithStats.getObjectiveFitnessesMap().get(objectiveName));
		LOGGER.debug("After kozaFitnessWithStats2={}", 
				kozaFitnessWithStats2.getObjectiveFitnessesMap().get(objectiveName));
		
		Assertions.assertEquals(kozaFitnessWithStats.getObjectiveFitnessesMap().get(objectiveName),
				kozaFitnessWithStats2.getObjectiveFitnessesMap().get(objectiveName),
				"The stats valus have different counts kozaFitnessWithStats="
						+ kozaFitnessWithStats.getObjectiveFitnessesMap().get(objectiveName) 
						+ " and kozaFitnessWithStats2="
						+ kozaFitnessWithStats.getObjectiveFitnessesMap().get(objectiveName));
		
	}
	
	@Test()
	@DisplayName("Individual Test")
	public void individualTest() {

		customSecurityManager();
		
		String[] properties = new String[]{
				"-file", TEST_EGC_AGENT_PARAMS,
				"-p", "seed.0=1",
				"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldAwardProblem",
				"-p", "eval.problem.shouldAwardTreeIndex=2",
				"-p", "eval.problem.number_of_runs=10",
				"-p", "generations=1",
				"-p", "pop.subpop.0.size=1",
//				"-p", "eval.problem.logging=console",
//				"-p", "silent=false"		
			};	
		
		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});

		Individual individual1 = PeerSimProblem.getGPProblem().gpIndividual;
		Individual individual2 = (Individual)individual1.clone();

		writeReadIndividual(PeerSimProblem.getGPProblem().state, individual1, individual2);

		LOGGER.debug("After individual1={}", individual1.hashCode());
		LOGGER.debug("After individual2={}", individual2.hashCode());

		Assertions.assertEquals(individual1.hashCode(), individual2.hashCode(),
				"The individuals are different hashCode individual1=" + individual1.hashCode()
						+ " and individual2=" + individual2.hashCode());

	}
	
	public void writeReadIndividual(EvolutionState state, Individual individual1, 
			Individual individual2) {
	
		OutputStream outputStream = new ByteArrayOutputStream();
		DataOutput dataOutput = new DataOutputStream(outputStream);
		
		try {
			individual1.writeIndividual(state, dataOutput);
			((OutputStream) dataOutput).flush();
			
			InputStream inputStream = new ByteArrayInputStream(((ByteArrayOutputStream)outputStream).toByteArray());
			DataInput dataInput = new DataInputStream(inputStream);
	        
			individual2.readIndividual(state, dataInput);
	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}