package ie.nix.ecj.automata;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.TypedData;
import ie.nix.ecj.gp.Variable;

@SuppressWarnings("serial")
public abstract class Nodes {

	private static final Logger LOGGER = LogManager.getLogger();

	public static class Value extends Variable {

		public static double value;

		public Value() {
			super("Value", (results, result) -> ((TypedData)result).set(Value.value));
		}

	}

	public static class DerivedValue extends Variable {

		public static double value;

		public DerivedValue() {
			super("DerivedValue", (results, result) -> ((TypedData)result).set(DerivedValue.value));
		}

	}
	
	public static class MessageValue extends Variable {

		public static double value;

		public MessageValue() {
			super("MessageValue", (results, result) -> ((TypedData)result).set(MessageValue.value));
		}

	}

}
