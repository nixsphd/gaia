package ie.nix.ecj.automata.max;

import java.util.DoubleSummaryStatistics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.ecj.automata.Automata;
import peersim.core.Network;

public class MaxProtocolTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	@SuppressWarnings("unchecked")
	@Test()
	@DisplayName("Max Protocol")
	public void maxProtocol() {
		String[] properties = new String[] { 
			"src/test/resources/ie/nix/ecj/automata/Automata.properties"
		};

        peersim.Simulator.main(properties);
        
        DoubleSummaryStatistics automataValueStats = new DoubleSummaryStatistics();
		DoubleSummaryStatistics automataDerivedValueStats = new DoubleSummaryStatistics();
		for (int n = 0; n < Network.size(); n++) {
			Automata<Integer> automata = (Automata<Integer>)Network.get(n);
			automataValueStats.accept(automata.getValue());
			automataDerivedValueStats.accept(automata.getDerivedValue());
		}
		double valueMax = automataValueStats.getMax();
		double derivedAgv = automataDerivedValueStats.getAverage();

		double derivedMaxCount = 0;
		for (int n = 0; n < Network.size(); n++) {
			Automata<Integer> automata = (Automata<Integer>)Network.get(n);
			if (automata.getDerivedValue() == valueMax) {
				derivedMaxCount++;
			}
		}
		
		LOGGER.debug("valueMax={}, derivedAgv={}, derivedMaxCount={}", valueMax, derivedAgv, derivedMaxCount);	
		Assertions.assertEquals(valueMax, derivedAgv, 
				"The dervived value, "+derivedAgv+" is not max value "+valueMax+".");
	}
}