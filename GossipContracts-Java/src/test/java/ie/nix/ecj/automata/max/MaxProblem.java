package ie.nix.ecj.automata.max;

import java.util.DoubleSummaryStatistics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.automata.Automata;
import ie.nix.ecj.PeerSimProblem;
import peersim.core.CommonState;
import peersim.core.Network;

@SuppressWarnings("serial")
public class MaxProblem extends PeerSimProblem {

	private static final Logger LOGGER = LogManager.getLogger();
	
	@Override
	public double evaluateSimulation() {
//		return evaluateSimulationMaxAvgDelta();
		return evaluateSimulationCorrectCount();
	}
	
	@SuppressWarnings("unchecked")
	public double evaluateSimulationCorrectCount() {
		DoubleSummaryStatistics automataValueStats = new DoubleSummaryStatistics();
		for (int n = 0; n < Network.size(); n++) {
			Automata<Integer> automata = (Automata<Integer>)Network.get(n);
			automataValueStats.accept(automata.getValue());
			LOGGER.trace("[{}]automata({})={}", CommonState.getTime(), n, automata);
		}
		double max = automataValueStats.getMax();

		double derivedIsMax = 0;
		for (int n = 0; n < Network.size(); n++) {
			Automata<Integer> automata = (Automata<Integer>)Network.get(n);
			if (automata.getDerivedValue() == max) {
				derivedIsMax +=  1;
			} else {
				LOGGER.trace("[{}]automata({})={} was right!", CommonState.getTime(), n, automata);
			}
			LOGGER.trace("[{}]automata({})={}", CommonState.getTime(), n, automata);
		}
		
		double evaluation = Network.size() - derivedIsMax;
		LOGGER.debug("max={}, derivedIsMax={}, evaluation={}", max, derivedIsMax, evaluation);
		return evaluation;
	}
	
	@SuppressWarnings("unchecked")
	public double evaluateSimulationMaxAvgDelta() {
		DoubleSummaryStatistics automataValueStats = new DoubleSummaryStatistics();
		DoubleSummaryStatistics automataDerivedValueStats = new DoubleSummaryStatistics();
		for (int n = 0; n < Network.size(); n++) {
			Automata<Integer> automata = (Automata<Integer>)Network.get(n);
			automataValueStats.accept(automata.getValue());
			automataDerivedValueStats.accept(automata.getDerivedValue());
			LOGGER.trace("[{}]automata({})={}", CommonState.getTime(), n, automata);
		}
		double max = automataValueStats.getMax();
		double derivedAvg = automataDerivedValueStats.getAverage();
		
		double result = Math.abs(max - derivedAvg);
		LOGGER.trace("max={}, derivedAvg={}, result={}", max, derivedAvg, result);
		return result;
	}
}
