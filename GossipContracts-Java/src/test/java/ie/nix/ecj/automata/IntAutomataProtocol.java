package ie.nix.ecj.automata;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.automata.Automata;
import ie.nix.ecj.automata.Nodes.DerivedValue;
import ie.nix.ecj.automata.Nodes.MessageValue;
import ie.nix.ecj.automata.Nodes.Value;
import ie.nix.ecj.PeerSimProblem;
import peersim.core.CommonState;

public class IntAutomataProtocol extends AutomataProtocol<Integer> {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public IntAutomataProtocol(String prefix) {
		super(prefix);
	}

	@Override
	public void nextCycle(Automata<Integer> automata) {
//		double messageValue = getMessageValueMaxValeuAndDerivedValue(automata);
		double messageValue = getMessageValueFromTree(automata);
		
		Message<Integer> message = new Message<Integer>((int)messageValue);
		Automata<Integer> toAutomata = getRightAutomata();
		sendTenderMessages(message, toAutomata);
		LOGGER.debug("[{}] Sent {} to {}", CommonState.getTime(), message, toAutomata);
	}

	@SuppressWarnings("unused")
	private double getMessageValueMaxValeuAndDerivedValue(Automata<Integer> automata) {
		Value.value = automata.getValue();
		DerivedValue.value = automata.getDerivedValue();
		return (automata.getValue() > automata.getDerivedValue())? automata.getValue() : automata.getDerivedValue();
	}

	private double getMessageValueFromTree(Automata<Integer> automata) {
		Value.value = automata.getValue();
		DerivedValue.value = automata.getDerivedValue();
		return PeerSimProblem.getGPProblem().evaluateTreeForDouble(0);
	}
	
	@Override
	public void processEvent(Automata<Integer> automata, Message<Integer> message) {
//		double newDerivedValue = getNewDerivedValueFromMessage(automata, message);
		double newDerivedValue = getNewDerivedValueFromTree(automata, message);
		automata.setDerivedValue((int)newDerivedValue);
	}
	
	@SuppressWarnings("unused")
	private double getNewDerivedValueFromMessage(Automata<Integer> automata, Message<Integer> message) {
		double newDerivedValue = message.getValue();
		LOGGER.debug("[{}] {} newDerivedValue={} ", CommonState.getTime(),  automata, newDerivedValue);
		return newDerivedValue;
	}

	private double getNewDerivedValueFromTree(Automata<Integer> automata, Message<Integer> message) {
		Value.value = automata.getValue();
		DerivedValue.value = automata.getDerivedValue();
		MessageValue.value = message.getValue();
		LOGGER.debug("[{}] {} recieved message {} ", CommonState.getTime(), automata, message);
		
		double newDerivedValue = PeerSimProblem.getGPProblem().evaluateTreeForDouble(1);
		LOGGER.debug("[{}] {} newDerivedValue={} ", CommonState.getTime(),  automata, newDerivedValue);
		return newDerivedValue;
	}
	
}
