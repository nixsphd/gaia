package ie.nix.ecj.automata.max;

import java.security.Permission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.ecj.PeerSimProblem;

public class MaxProblemTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}
	
	@Test()
	@DisplayName("Max Problem GP")
	public void maxProblemGP() {
		String[] properties = new String[]{
				"-file", "src/test/resources/ie/nix/ecj/automata/gp/Automata.params",
				"-p", "seed.0=1", 		
				"-p", "silent=true"	
			};
		testProblem(properties);
	}
	
	@Test()
	@DisplayName("Max Problem GE")
	public void maxProblemGE() {
		String[] properties = new String[]{
				"-file", "src/test/resources/ie/nix/ecj/automata/ge/Automata.params",
				"-p", "seed.0=1", 		
				"-p", "silent=true"	
			};
		testProblem(properties);
	}
	
	public void testProblem(String[] properties) {

		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});
		
		double bestAdjustedFitness = PeerSimProblem.getGPProblem().getBestAdjustedFitness();
		
		LOGGER.debug("bestAdjustedFitness={}", bestAdjustedFitness);		
		Assertions.assertEquals(1d, bestAdjustedFitness, 
				"Did not get a good enough fitness "+bestAdjustedFitness+".");
	}
	
}