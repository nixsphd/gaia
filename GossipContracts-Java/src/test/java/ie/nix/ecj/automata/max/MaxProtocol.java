package ie.nix.ecj.automata.max;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.automata.Automata;
import ie.nix.ecj.automata.AutomataProtocol;
import peersim.core.CommonState;

public class MaxProtocol extends AutomataProtocol<Integer> {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public MaxProtocol(String prefix) {
		super(prefix);
	} 
	
	@Override
	public void nextCycle(Automata<Integer> automata) {

		double messageValue = automata.getValue();
//		double messageValue = (automata.getDerivedValue() - automata.getValue()) / automata.getValue();
		if (automata.getDerivedValue() > automata.getValue()) {
			messageValue = automata.getDerivedValue();
		}
		Message<Integer> message = new Message<Integer>((int)messageValue);
		
		Automata<Integer> toAutomata = getRightAutomata();
		sendTenderMessages(message, toAutomata);
		LOGGER.trace("[{}] Sent {} to {}", CommonState.getTime(), message, toAutomata);
		
		toAutomata = getLeftAutomata();
		sendTenderMessages(message, toAutomata);
		LOGGER.trace("[{}] Sent {} to {}", CommonState.getTime(), message, toAutomata);
		
	}

	@Override
	public void processEvent(Automata<Integer> automata, Message<Integer> message) {
		if (message.getValue() > automata.getDerivedValue()) {
			automata.setDerivedValue(message.getValue());
//			automata.setDerivedValue(automata.getValue() - automata.getDerivedValue());
			LOGGER.trace("[{}] {} recieved message {}, updated DerivedValue", CommonState.getTime(), automata, message);
			
		} else {
			LOGGER.trace("[{}] {} recieved message {}, doing nothing", CommonState.getTime(), automata, message);
			
		}
		
	}
		
}
