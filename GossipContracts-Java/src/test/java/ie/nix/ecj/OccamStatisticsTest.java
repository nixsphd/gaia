package ie.nix.ecj;	

import java.security.Permission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.EvolutionState;
import ec.util.MersenneTwisterFast;
import ie.nix.ecj.gp.ArithmeticFunctions;
import ie.nix.ecj.gp.ArithmeticFunctions.Min;
import ie.nix.ecj.gp.ArithmeticOperator;
import ie.nix.ecj.gp.ConditionalOperator;
import ie.nix.ecj.gp.Constant;
import ie.nix.ecj.gp.Constant.False;
import ie.nix.ecj.gp.Constant.True;
import ie.nix.ecj.gp.Constant.Zero;
import ie.nix.ecj.gp.ERC;
import ie.nix.ecj.gp.ERC.ERC1;
import ie.nix.ecj.gp.ERC.ERC10;
import ie.nix.ecj.gp.LogicalOperator;
import ie.nix.ecj.gp.LogicalOperator.Not;
import ie.nix.ecj.gp.Node;
import ie.nix.ecj.gp.Random;
import ie.nix.ecj.gp.RelationalOperator;
import ie.nix.ecj.gp.RelationalOperator.LessThan;
import ie.nix.ecj.gp.Variable;
import ie.nix.ecj.gp.Variable.X;
import ie.nix.ecj.gp.Variable.Y;
import ie.nix.ecj.gp.Variable.Z;

@SuppressWarnings("serial")
public class OccamStatisticsTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class Maybe extends Variable {

	    public static boolean value;
	    
		public Maybe() {
			super("Maybe", (results, result) -> result.set(Maybe.value));
		}	
	}
	
	public static class MaybeNot extends Variable {

	    public static boolean value;
	    
		public MaybeNot() {
			super("MaybeNot", (results, result) -> result.set(MaybeNot.value));
		}	
	}
	
	public static class Positive extends Variable {

	    public static boolean value;
	    
		public Positive() {
			super("+ve", (results, result) -> result.set(Positive.value));
		}	
		
		public boolean isPositive() {
			return true;
		}
	}
	
	private static final String TEST_EGC_AGENT_PARAMS = "src/test/resources/ie/nix/egc/TestEGCAgent.params";

	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {}

	public static void customSecurityManager() {
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}
	
	@Test()
	@DisplayName("Occam StatisticsTest")
	public void occamStatisticsTest() {

		customSecurityManager();
		
		String[] properties = new String[]{
			"-file", TEST_EGC_AGENT_PARAMS,
			"-p", "seed.0=1",
			"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldTenderProblem",
			"-p", "eval.problem.shouldTenderTreeIndex=0",
			"-p", "eval.problem.number_of_runs=10",
			"-p", "generations=2",
			"-p", "pop.subpop.0.size=1",
			"-p", "stat=ie.nix.ecj.OccamStatistics",
			"-p", "stat.file=$occam.stat",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false"
		};	
		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});
		
		EvolutionState state = PeerSimProblem.getGPProblem().state;
		Assertions.assertTrue(state.statistics instanceof OccamStatistics);
		
		OccamStatistics occamStatistics = (OccamStatistics)state.statistics;
		occamStatistics.statisticslog = 0;
		occamStatistics.postEvaluationStatistics(state);

	}
	
	@Test()
	@DisplayName("ArithmeticOperator C + C Test")
	public void occamArithmeticOperatorCAndCTest() {

		ArithmeticOperator.Add node = new ArithmeticOperator.Add();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);
		
		String expectedOccamString = "3.0";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticOperator X Add 2 Test")
	public void occamArithmeticOperatorXAdd2Test() {

		ArithmeticOperator.Add node = new ArithmeticOperator.Add();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Constant(2);

		String expectedOccamString = "(x + 2.0)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticOperator X Add 0 Test")
	public void occamArithmeticOperatorXAdd0Test() {

		ArithmeticOperator.Add node = new ArithmeticOperator.Add();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Constant(0);

		String expectedOccamString = "x";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperator 0 Not Constant Add Test")
	public void occamArithmeticOperator0NotConstantAddTest() {

		ArithmeticOperator.Add node = new ArithmeticOperator.Add();
		
		node.children = new Node[2];
		node.children[0] = new Zero();
		node.children[1] = new X();

		String expectedOccamString = "x";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperato Sub Test")
	public void occamArithmeticOperatorSubTest() {

		ArithmeticOperator.Sub node = new ArithmeticOperator.Sub();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);
		
		String expectedOccamString = "-1.0";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperato X Sub X Test")
	public void occamArithmeticOperatorXSubXTest() {

		ArithmeticOperator.Sub node = new ArithmeticOperator.Sub();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new X();
		
		String expectedOccamString = "0.0";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperato X Sub Y Test")
	public void occamArithmeticOperatorXSubYTest() {

		ArithmeticOperator.Sub node = new ArithmeticOperator.Sub();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Y();
		
		String expectedOccamString = "(" + node.children[0].toStringForHumans() + 
				" " + node.toStringForHumans() + " " + 
				node.children[1].toStringForHumans() +")";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticOperato X Sub 0 Test")
	public void occamArithmeticOperatorXSub0Test() {

		ArithmeticOperator.Sub node = new ArithmeticOperator.Sub();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Zero();
		
		String expectedOccamString = node.children[0].toStringForHumans();

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticOperato 0 Sub Y Test")
	public void occamArithmeticOperator0SubYTest() {

		ArithmeticOperator.Sub node = new ArithmeticOperator.Sub();
		
		node.children = new Node[2];
		node.children[0] = new Zero();
		node.children[1] = new Y();
		
		String expectedOccamString = "(" + node.children[0].toStringForHumans() + 
				" " + node.toStringForHumans() + " " + 
				node.children[1].toStringForHumans() +")";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperato Mul Test")
	public void occamArithmeticOperatorMulTest() {

		ArithmeticOperator.Mul node = new ArithmeticOperator.Mul();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);
		
		String expectedOccamString = "2.0";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperato X Mul 2 Test")
	public void occamArithmeticOperatorXMul2Test() {

		ArithmeticOperator.Mul node = new ArithmeticOperator.Mul();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Constant(2);
		
		String expectedOccamString = 
				"(" + node.children[0].toStringForHumans() + 
				" " + node.toStringForHumans() + " " + 
				node.children[1].toStringForHumans() +")";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperato X Mul 1 Test")
	public void occamArithmeticOperatorXMul1Test() {

		ArithmeticOperator.Mul node = new ArithmeticOperator.Mul();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Constant(1);
		
		String expectedOccamString = "x";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperato 1 Mul X Test")
	public void occamArithmeticOperator1MulXTest() {

		ArithmeticOperator.Mul node = new ArithmeticOperator.Mul();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new X();
		
		String expectedOccamString = "x";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperator Div Test")
	public void occamArithmeticOperatorDivTest() {

		ArithmeticOperator.Div node = new ArithmeticOperator.Div();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);
		
		String expectedOccamString = "0.5";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperator X Div Y Test")
	public void occamArithmeticOperatorXDivYTest() {
	
		ArithmeticOperator.Div node = new ArithmeticOperator.Div();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Y();
		
		String expectedOccamString = node.toStringForHumans() + "(" + 
				node.children[0].toStringForHumans() + ", " + 
				node.children[1].toStringForHumans() +")";
	
		occamTest(node, expectedOccamString);
	}

	
	@Test()
	@DisplayName("ArithmeticOperator X Div X Test")
	public void occamArithmeticOperatorXDivXTest() {

		ArithmeticOperator.Div node = new ArithmeticOperator.Div();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new X();
		
		String expectedOccamString = "1.0";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperator X Div 0 Test")
	public void occamArithmeticOperatorXDiv0Test() {

		ArithmeticOperator.Div node = new ArithmeticOperator.Div();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Constant(0);
		
		String expectedOccamString = "0.0";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperator Div X Eval to 0 Test")
	public void occamArithmeticOperatorDivXEvalTo0Test() {

		ArithmeticOperator.Div node = new ArithmeticOperator.Div();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new ArithmeticOperator.Sub();
		node.children[1].children = new Node[2];
		node.children[1].children[0] = new X();
		node.children[1].children[1] = new X();
		
		String expectedOccamString = "0.0";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticOperator 0 Div X Test")
	public void occamArithmeticOperator0DivXTest() {

		ArithmeticOperator.Div node = new ArithmeticOperator.Div();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0);
		node.children[1] = new X();
		
		String expectedOccamString = "0.0";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("LogicalOperator Constant And Test")
	public void occamLogicalOperatorConstantAndTest() {

		LogicalOperator.And node = new LogicalOperator.And();
		
		node.children = new Node[2];
		node.children[0] = new True();
		node.children[1] = new False();
		
		String expectedOccamString = Boolean.FALSE.toString();
		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("LogicalOperator True And Maybe Test")
	public void occamLogicalOperatorTrueAndMaybeTest() {

		LogicalOperator.And node = new LogicalOperator.And();
		
		node.children = new Node[2];
		node.children[0] = new True();
		node.children[1] = new Maybe();
		
		String expectedOccamString = "(" + node.children[0].toStringForHumans() + 
				" " + node.toStringForHumans() + " " + 
				node.children[1].toStringForHumans() +")";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("LogicalOperator True Or Maybe Test")
	public void occamLogicalOperatorTrueOrMaybeTest() {

		LogicalOperator.Or node = new LogicalOperator.Or();
		
		node.children = new Node[2];
		node.children[0] = new True();
		node.children[1] = new Maybe();
		
		String expectedOccamString = "true";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("LogicalOperator False Or Maybe Test")
	public void occamLogicalOperatorFalseOrMaybeTest() {

		LogicalOperator.Or node = new LogicalOperator.Or();
		
		node.children = new Node[2];
		node.children[0] = new False();
		node.children[1] = new Maybe();
		
		String expectedOccamString = "(" + node.children[0].toStringForHumans() + 
				" " + node.toStringForHumans() + " " + 
				node.children[1].toStringForHumans() +")";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("LogicalOperator Maybe Or Maybe Test")
	public void occamLogicalOperatorMaybeOrMaybeTest() {

		LogicalOperator.Or node = new LogicalOperator.Or();
		
		node.children = new Node[2];
		node.children[0] = new Maybe();
		node.children[1] = new Maybe();
		
		String expectedOccamString = "(" + node.children[0].toStringForHumans() + 
				" " + node.toStringForHumans() + " " + 
				node.children[1].toStringForHumans() +")";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("LogicalOperator Not Test")
	public void occamLogicalOperatorNotTest() {

		LogicalOperator.Not node = new LogicalOperator.Not();
		
		node.children = new Node[1];
		node.children[0] = new True();
		
		String expectedOccamString = "false";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("LogicalOperator Not Maybe Test")
	public void occamLogicalOperatorNotMaybeTest() {
	
		LogicalOperator.Not node = new LogicalOperator.Not();
		
		node.children = new Node[1];
		node.children[0] = new Maybe();
		
		String expectedOccamString = node.toStringForHumans() + 
				"(" + node.children[0].toStringForHumans() +")";
	
		occamTest(node, expectedOccamString);
	}

	
	@Test()
	@DisplayName("LogicalOperator Not Not Maybe Test")
	public void occamLogicalOperatorNotNotMaybeTest() {
	
		LogicalOperator.Not node = new LogicalOperator.Not();
		
		node.children = new Node[1];
		node.children[0] = new LogicalOperator.Not();
		node.children[0].children[0] = new Maybe();
		
		String expectedOccamString = "Maybe";
	
		occamTest(node, expectedOccamString);
	}

	
	@Test()
	@DisplayName("LogicalOperator Not Not Test")
	public void occamLogicalOperatorNotNotTest() {

		LogicalOperator.Not node = new LogicalOperator.Not();
		
		node.children = new Node[1];
		node.children[0] = new LogicalOperator.Not();
		node.children[0].children[0] = new True();
		
		String expectedOccamString = "true";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("RelationalOperator Equals Test")
	public void occamRelationalOperatorEqualsTest() {

		RelationalOperator.Equals node = new RelationalOperator.Equals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);
		
		String expectedOccamString = "false";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("RelationalOperator X Equals Const Test")
	public void occamRelationalOperatorXEqualsConstTest() {

		RelationalOperator.Equals node = new RelationalOperator.Equals();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Constant(2);
		
		String expectedOccamString = "(" + 
				node.children[0].toStringForHumans() +  " " + 
				node.toStringForHumans() + " " + 
				node.children[1].toStringForHumans() +")";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("RelationalOperator X Equals X Test")
	public void occamRelationalOperatorXEqualsXTest() {

		RelationalOperator.Equals node = new RelationalOperator.Equals();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new X();
		
		String expectedOccamString = "true";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("RelationalOperator X Equals Y Test")
	public void occamRelationalOperatorXEqualsYTest() {

		RelationalOperator.Equals node = new RelationalOperator.Equals();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Y();
		
		String expectedOccamString = "(" + 
				node.children[0].toStringForHumans() +  " " + 
				node.toStringForHumans() + " " + 
				node.children[1].toStringForHumans() +")";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator X < X Test")
	public void occamRelationalOperatorXLTXTest() {

		RelationalOperator.LessThan node = new RelationalOperator.LessThan();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new X();
		
		String expectedOccamString = "false";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator 0 < Positive Test")
	public void occamRelationalOperator0LTPositiveTest() {

		RelationalOperator.LessThan node = new RelationalOperator.LessThan();
		
		node.children = new Node[2];
		node.children[0] = new Zero();
		node.children[1] = new Positive();
		
		String expectedOccamString = "(0.0 < +ve)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator 0 <= Positive Test")
	public void occamRelationalOperator0LEPositiveTest() {

		RelationalOperator.LessThanOrEquals node = 
				new RelationalOperator.LessThanOrEquals();
		
		node.children = new Node[2];
		node.children[0] = new Zero();
		node.children[1] = new Positive();
		
		String expectedOccamString = "(0.0 <= +ve)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator 0 > Positive Test")
	public void occamRelationalOperator0GTPositiveTest() {

		RelationalOperator.GreaterThan node = 
				new RelationalOperator.GreaterThan();
		
		node.children = new Node[2];
		node.children[0] = new Zero();
		node.children[1] = new Positive();
		
		String expectedOccamString = "false";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator 0 >= Positive Test")
	public void occamRelationalOperator0GEPositiveTest() {

		RelationalOperator.GreaterThanOrEquals node = 
				new RelationalOperator.GreaterThanOrEquals();
		
		node.children = new Node[2];
		node.children[0] = new Zero();
		node.children[1] = new Positive();
		
		String expectedOccamString = "(0.0 >= +ve)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator Positive < 0 Test")
	public void occamRelationalOperatorPositiveLT0Test() {

		RelationalOperator.LessThan node = new RelationalOperator.LessThan();
		
		node.children = new Node[2];
		node.children[0] = new Positive();
		node.children[1] = new Zero();
		
		String expectedOccamString = "false";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator Positive <= 0 Test")
	public void occamRelationalOperatorPositiveLE0Test() {

		RelationalOperator.LessThanOrEquals node = 
				new RelationalOperator.LessThanOrEquals();
		
		node.children = new Node[2];
		node.children[0] = new Positive();
		node.children[1] = new Zero();
		
		String expectedOccamString = "(+ve <= 0.0)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator Positive > 0 Test")
	public void occamRelationalOperatorPositiveGT0Test() {

		RelationalOperator.GreaterThan node = 
				new RelationalOperator.GreaterThan();
		
		node.children = new Node[2];
		node.children[0] = new Positive();
		node.children[1] = new Zero();
		
		String expectedOccamString = "true";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("RelationalOperator Positive >= 0 Test")
	public void occamRelationalOperatorPositiveGE0Test() {

		RelationalOperator.GreaterThanOrEquals node = 
				new RelationalOperator.GreaterThanOrEquals();
		
		node.children = new Node[2];
		node.children[0] = new Positive();
		node.children[1] = new Zero();
		
		String expectedOccamString = "true";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ConditionalOperator if Maybe Maybe MaybeNot Test")
	public void occamConditionalOperatorIfMaybeMaybeMaybeNotTest() {

		ConditionalOperator.IfBoolean node = 
				new ConditionalOperator.IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new Maybe();
		node.children[1] = new Maybe();
		node.children[2] = new MaybeNot();
		
		String expectedOccamString = "(Maybe ? Maybe : MaybeNot)";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ConditionalOperator If True Maybe MaybeNot Test")
	public void occamConditionalOperatorIfTrueMaybeMaybeNotTest() {

		ConditionalOperator.IfBoolean node = 
				new ConditionalOperator.IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new True();
		node.children[1] = new Maybe();
		node.children[2] = new MaybeNot();
		
		String expectedOccamString = 
				node.children[1].toStringForHumans();

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ConditionalOperator If False Maybe MaybeNot Test")
	public void occamConditionalOperatorIfFalseMaybeMaybeNotTest() {

		ConditionalOperator.IfBoolean node = 
				new ConditionalOperator.IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new False();
		node.children[1] = new Maybe();
		node.children[2] = new MaybeNot();
		
		String expectedOccamString = 
				node.children[2].toStringForHumans();

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ConditionalOperator If Maybe X Y Test")
	public void occamConditionalOperatorIfMaybeXYTest() {

		ConditionalOperator.IfBoolean node = 
				new ConditionalOperator.IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new Maybe();
		node.children[1] = new X();
		node.children[2] = new Y();
		
		String expectedOccamString = "(Maybe ? x : y)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ConditionalOperator If True X Y Test")
	public void occamConditionalOperatorIfTrueXYTest() {

		ConditionalOperator.IfBoolean node = 
				new ConditionalOperator.IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new True();
		node.children[1] = new X();
		node.children[2] = new Y();
		
		String expectedOccamString = 
				node.children[1].toStringForHumans();

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ConditionalOperator If False X Y Test")
	public void occamConditionalOperatorIfFalseXYTest() {

		ConditionalOperator.IfBoolean node = 
				new ConditionalOperator.IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new False();
		node.children[1] = new X();
		node.children[2] = new Y();
		
		String expectedOccamString = 
				node.children[2].toStringForHumans();

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ConditionalOperator If X Equals X Y Z Test")
	public void occamConditionalOperatorIfXEqualsXYZTest() {

		ConditionalOperator.IfBoolean node = 
				new ConditionalOperator.IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new RelationalOperator.Equals();
		node.children[0].children = new Node[2];
		node.children[0].children[0] = new X();
		node.children[0].children[1] = new X();
		node.children[1] = new Y();
		node.children[2] = new Z();
		
		String expectedOccamString = 
				node.children[1].toStringForHumans();

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ConditionalOperator If If Not X LT X Y Z Y Z Test")
	public void occamConditionalOperatorIfIfNotXLTXYZYZTest() {

		ConditionalOperator.IfBoolean node = 
				new ConditionalOperator.IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new ConditionalOperator.IfBoolean();
		node.children[0].children[0] = new LogicalOperator.Not();
		node.children[0].children[0].children[0] = new RelationalOperator.LessThan();
		node.children[0].children[0].children[0].children = new Node[2];
		node.children[0].children[0].children[0].children[0] = new X();
		node.children[0].children[0].children[0].children[1] = new X();
		node.children[0].children[1] = new Y();
		node.children[0].children[2] = new Z();
		node.children[1] = new Y();
		node.children[2] = new Z();
		
		String expectedOccamString = "(y ? y : z)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Abs 1 Test")
	public void occamArithmeticFunctionsAbs1Test() {

		ArithmeticFunctions.Abs node = new ArithmeticFunctions.Abs();
		
		node.children = new Node[1];
		node.children[0] = new Constant(1);
		
		String expectedOccamString = "1.0";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Abs -1 Test")
	public void occamArithmeticFunctionsAbsMinus1Test() {

		ArithmeticFunctions.Abs node = new ArithmeticFunctions.Abs();
		
		node.children = new Node[1];
		node.children[0] = new Constant(-1);
		
		String expectedOccamString = "1.0";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Abs X Test")
	public void occamArithmeticFunctionsAbsXTest() {

		ArithmeticFunctions.Abs node = new ArithmeticFunctions.Abs();
		
		node.children = new Node[1];
		node.children[0] = new X();
		
		String expectedOccamString = "abs(x)";

		occamTest(node, expectedOccamString);
	}

	@Test()
	@DisplayName("ArithmeticFunctions Abs Test")
	public void occamArithmeticFunctionsAbsTest() {

		ArithmeticFunctions.Abs node = new ArithmeticFunctions.Abs();
		
		node.children = new Node[1];
		node.children[0] = new Constant(-1d);
		
		String expectedOccamString = "1.0";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Abs Positive Test")
	public void occamArithmeticFunctionsAbsPositiveTest() {

		ArithmeticFunctions.Abs node = new ArithmeticFunctions.Abs();
		
		node.children = new Node[1];
		node.children[0] = new Positive();
		
		String expectedOccamString = "+ve";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Min 1 2 Test")
	public void occamArithmeticFunctionsMin12Test() {

		ArithmeticFunctions.Min node = new ArithmeticFunctions.Min();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);
		
		String expectedOccamString = "1.0";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Min X Y Test")
	public void occamArithmeticFunctionsMinXYTest() {

		ArithmeticFunctions.Min node = new ArithmeticFunctions.Min();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Y();
		
		String expectedOccamString = "min(x, y)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Min X X Test")
	public void occamArithmeticFunctionsMinXXTest() {

		ArithmeticFunctions.Min node = new ArithmeticFunctions.Min();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new X();
		
		String expectedOccamString = "x";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Min 0 Positive Test")
	public void occamArithmeticFunctionsMin0PositiveTest() {

		ArithmeticFunctions.Min node = new ArithmeticFunctions.Min();
		
		node.children = new Node[2];
		node.children[0] = new Zero();
		node.children[1] = new Positive();
		
		String expectedOccamString = "0.0";

		occamTest(node, expectedOccamString);
		
		node.children[1] = new Zero();
		node.children[0] = new Positive();

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Max 1 2 Test")
	public void occamArithmeticFunctionsMax12Test() {

		ArithmeticFunctions.Max node = new ArithmeticFunctions.Max();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);
		
		String expectedOccamString = "2.0";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Max X Y Test")
	public void occamArithmeticFunctionsMaxXYTest() {

		ArithmeticFunctions.Max node = new ArithmeticFunctions.Max();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new Y();
		
		String expectedOccamString = "max(x, y)";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Max X X Test")
	public void occamArithmeticFunctionsMaxXXTest() {

		ArithmeticFunctions.Max node = new ArithmeticFunctions.Max();
		
		node.children = new Node[2];
		node.children[0] = new X();
		node.children[1] = new X();
		
		String expectedOccamString = "x";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ArithmeticFunctions Max 0 Positive Test")
	public void occamArithmeticFunctionsMax0PositiveTest() {

		ArithmeticFunctions.Max node = new ArithmeticFunctions.Max();
		
		node.children = new Node[2];
		node.children[0] = new Zero();
		node.children[1] = new Positive();
		
		String expectedOccamString = "+ve";

		occamTest(node, expectedOccamString);
		
		node.children[1] = new Zero();
		node.children[0] = new Positive();

		occamTest(node, expectedOccamString);
	}
	
	/*
	 * Random
	 */
	@Test()
	@DisplayName("Random Equals Test")
	public void occamRandonEqualsTest() {

		RelationalOperator.Equals node = new RelationalOperator.Equals();
		
		node.children = new Node[2];
		node.children[0] = new Random();
		node.children[1] = new Random();
		
		String expectedOccamString = "(rand() == rand())";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("Random GreaterThan Test")
	public void occamRandonGreaterThanTest() {

		RelationalOperator.GreaterThan node = new RelationalOperator.GreaterThan();
		
		node.children = new Node[2];
		node.children[0] = new Random();
		node.children[1] = new Random();
		
		String expectedOccamString = "(rand() > rand())";

		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("Random Or GreaterThan Test")
	public void occamRandonOrGreaterThanTest() {
		
		LogicalOperator.Or node = new LogicalOperator.Or();
	
		node.children[0] = new RelationalOperator.GreaterThan();
		node.children[0].children[0] = new Random();
		node.children[0].children[1] = (Random)node.children[0].children[0].clone();
		node.children[1] = new Maybe();
		
		String expectedOccamString = "((rand() > rand()) || Maybe)";
		occamTest(node, expectedOccamString);
	}
	
	/*
	 * ERCs
	 */
	@Test()
	@DisplayName("ERC + ERC Test")
	public void occamERCPlusERCTest() {

		EvolutionState state = getEvolutionState();
		
		ArithmeticOperator.Add node = new ArithmeticOperator.Add();
		
		node.children[0] = new ERC1();
		node.children[0].resetNode(state, 0);
		node.children[1] = new ERC10();
		node.children[1].resetNode(state, 0);
		
		String expectedOccamString = String.valueOf(
				((ERC)node.children[0]).evalConstant().getDouble() + 
				((ERC)node.children[1]).evalConstant().getDouble());
		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ERC + X Test")
	public void occamERCPlusXTest() {

		EvolutionState state = getEvolutionState();
		
		ArithmeticOperator.Add node = new ArithmeticOperator.Add();
		
		node.children[0] = new ERC1();
		node.children[0].resetNode(state, 0);
		node.children[1] = new X();
		
		String expectedOccamString = "(" + String.valueOf(
				((ERC)node.children[0]).evalConstant().getDouble()) +
				" + x)";
		occamTest(node, expectedOccamString);
	}
	
	@Test()
	@DisplayName("ERC + 0 Test")
	public void occamERCPlusZeroTest() {

		EvolutionState state = getEvolutionState();
		
		ArithmeticOperator.Add node = new ArithmeticOperator.Add();
		
		node.children[0] = new ERC1();
		node.children[0].resetNode(state, 0);
		node.children[1] = new Zero();
		
		String expectedOccamString = 
				String.valueOf(((ERC)node.children[0]).evalConstant().getDouble());
		occamTest(node, expectedOccamString);
	}
	
//	!(itemSize < itemSize) || ((binCapacity min ERC1[0.1]) < (offer min rand))
	@Test()
	@DisplayName("Big Test")
	public void occamBigTest() {

		EvolutionState state = getEvolutionState();
		
		Node node = new LogicalOperator.Or();
		
		node.children[0] = new Not();
		node.children[0].children[0] = new LessThan();
		node.children[0].children[0].children[0] = new X();
		node.children[0].children[0].children[1] = new X();
		node.children[1] = new LessThan();
		node.children[1].children[0] = new Min();
		node.children[1].children[0].children[0] = new X();
		node.children[1].children[0].children[1] = new Y();
		node.children[1].children[1] = new Min();
		node.children[1].children[1].children[0] = new X();
		node.children[1].children[1].children[1] = new Random();
		
		String expectedOccamString = "true";
		occamTest(node, expectedOccamString);
	}

	public EvolutionState getEvolutionState() {
		EvolutionState state = new EvolutionState();
		state.random = new MersenneTwisterFast[1];
		state.random[0] = new MersenneTwisterFast();
		return state;
	}
	
	public void occamTest(Node node, String expectedOccamString) {

		String actualOccamString = node.toOccamStringForHumans();

		LOGGER.debug("expectedOccamString={}", expectedOccamString);
		LOGGER.debug("actualOccamString={}", actualOccamString);
		Assertions.assertEquals(expectedOccamString, actualOccamString, 
				"Expected "+expectedOccamString+", but  got"+actualOccamString+".");
	}
}