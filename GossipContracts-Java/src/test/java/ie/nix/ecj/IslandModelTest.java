package ie.nix.ecj;	

import java.io.IOException;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.Evolve;
import ec.eval.Slave;

public class IslandModelTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String AUTOMATA_ISLANDS_PARAMS = 
			"src/test/resources/ie/nix/ecj/TestAutomataIslands.params";
	
	private static final String AUTOMATA_ISLANDS_MASTER_SLAVE_PARAMS = 
			"src/test/resources/ie/nix/ecj/TestAutomataIslandsMasterSlave.params";
	
	private static final String SNAP_ISLANDS_PARAMS = 
			"src/main/resources/ie/nix/examples/snap/Snap4Islands.params";
	
	private static final String SNAP_ISLANDS_MASTER_SLAVE_PARAMS = 
			"src/test/resources/ie/nix/ecj/TestSnapIslandsMasterSlave.params";
	
	/*
	 * Helper methods
	 */
	@SuppressWarnings("rawtypes")
	public static Process startJVM(Class mainClass, String[] properties) {
		try {
			String separator = System.getProperty("file.separator");
			String classpath = System.getProperty("java.class.path");
			String java = System.getProperty("java.home") + separator + "bin" + separator + "java";
			ArrayList<String> command = new ArrayList<String>();
			command.add(java);
			command.add("-cp");
			command.add(classpath);
			command.add(mainClass.getName());
			command.addAll(Arrays.asList(properties));
			
			ProcessBuilder processBuilder = new ProcessBuilder(command);
			processBuilder.inheritIO();
			Process process = processBuilder.start();
			return process;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Thread startThread(String[] properties) {
		Thread thread = new Thread() {
			public void run() {
				LOGGER.debug("threads={}", this);	
				Assertions.assertThrows(RuntimeException.class, () -> {
					ec.Evolve.main(properties);
				});
			}
		};
		thread.start();
		return thread;
	}

	@BeforeAll
	static void beforeAll() {
			
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}

	@Disabled
	@Test()
	@DisplayName("Automata Island Model Test")
	public void automataIslandModelTest() {

		int generations = 10;
		int popSize = 100;
		
		String[] island0Properties = new String[]{
				"-file", AUTOMATA_ISLANDS_PARAMS,
				"-p", "exch.i-am-server=true",
				"-p", "exch.server-addr=127.0.0.1",
				"-p", "exch.id=Island0",
				"-p", "exch.client-port=16180",
				"-p", "seed.0=0",
				"-p", "stat.file=$island0.stat",
				"-p", "stat.child.0.file=$koza0.stat",
				"-p", "checkpoint-prefix=island0",
				"-p", "pop.subpop.0.size="+popSize,
				"-p", "generations="+generations,
				"-p", "exch.chatty=true",
//				"-p", "eval.problem.logging=console",
				"-p", "silent=false"
		};
		String[] island1Properties = new String[]{
				"-file", AUTOMATA_ISLANDS_PARAMS,
				"-p", "exch.server-addr=127.0.0.1",
				"-p", "exch.id=Island1",
				"-p", "exch.client-port=16181",
				"-p", "seed.0=1",
				"-p", "stat.file=$Island1.stat",
				"-p", "stat.child.0.file=$koza1.stat",
				"-p", "checkpoint-prefix=island1",
				"-p", "pop.subpop.0.size="+popSize,
				"-p", "generations="+generations,
				"-p", "exch.chatty=true",
				"-p", "silent=false"
		};
		String[] island2Properties = new String[]{
				"-file", AUTOMATA_ISLANDS_PARAMS,
				"-p", "exch.server-addr=127.0.0.1",
				"-p", "exch.id=Island2",
				"-p", "exch.client-port=16182",
				"-p", "seed.0=2",
				"-p", "stat.file=$Island2.stat",
				"-p", "stat.child.0.file=$koza2.stat",
				"-p", "checkpoint-prefix=island2",
				"-p", "pop.subpop.0.size="+popSize,
				"-p", "generations="+generations,
				"-p", "exch.chatty=true",
				"-p", "silent=false"
		};
		String[] island3Properties = new String[]{
				"-file", AUTOMATA_ISLANDS_PARAMS,
				"-p", "exch.server-addr=127.0.0.1",
				"-p", "exch.id=Island3",
				"-p", "exch.client-port=16183",
				"-p", "seed.0=3",
				"-p", "stat.file=$Island3.stat",
				"-p", "stat.child.0.file=$koza3.stat",
				"-p", "checkpoint-prefix=island3",
				"-p", "pop.subpop.0.size="+popSize,
				"-p", "generations="+generations,
				"-p", "exch.chatty=true",
				"-p", "silent=false"
		};
		
		testProblem(island0Properties, island1Properties, 
				island2Properties, island3Properties);
	}
	
	public void testProblem(String[] island0Properties, 
			String[] island1Properties, String[] island2Properties, 
			String[] island3Properties) {

		Thread island0Thread = startThread(island0Properties);
		Process island1Process = startJVM(Evolve.class, island1Properties);
		Process island2Process = startJVM(Evolve.class, island2Properties);
		Process island3Process = startJVM(Evolve.class, island3Properties);
		LOGGER.info("Waiting for island0Thread={}, island1Process={}, "
				+ "island2Process={}, island3Process={}", 
				island0Thread, island1Process, island2Process, island3Process);
		
		try {
			island1Process.waitFor(5, TimeUnit.MINUTES);
			island2Process.waitFor(5, TimeUnit.MINUTES);
			island3Process.waitFor(5, TimeUnit.MINUTES);
			LOGGER.info("Done waiting for island1Process={}, "
					+ "island2Process={}, island3Process={}", 
					island1Process, island2Process, island3Process);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		island1Process.destroy();
		island2Process.destroy();
		island3Process.destroy();
		
		try {
			island0Thread.join(60 * 1000); // 1 min
			LOGGER.info("Done waiting for island0Thread={}", island0Thread);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Disabled
	@Test()
	@DisplayName("Automata Island Master Slave Model Test")
	public void automataIslandMasterSlaveModelTest() {

		int generations = 10;
		int popSize = 20;
		int maxJobsPerSlave = 2;
		
		String[] island0MasterProperties = new String[]{
				"-file", AUTOMATA_ISLANDS_MASTER_SLAVE_PARAMS,
				"-p", "eval.masterproblem=ec.eval.MasterProblem",
				"-p", "eval.master.host=127.0.0.1",
				"-p", "eval.master.port=12350",
				"-p", "eval.masterproblem.max-jobs-per-slave="+maxJobsPerSlave,
				"-p", "eval.compression=true",
				"-p", "exch.i-am-server=true",
				"-p", "exch.server-addr=127.0.0.1",
				"-p", "exch.id=Island0",
				"-p", "exch.client-port=16180",
				"-p", "seed.0=0",
				"-p", "stat.file=$island0.stat",
				"-p", "stat.child.0.file=$koza0.stat",
				"-p", "checkpoint-prefix=island0",
				"-p", "pop.subpop.0.size="+popSize,
				"-p", "generations="+generations,
				"-p", "exch.chatty=true",
//				"-p", "eval.problem.logging=console",
				"-p", "silent=false"
		};
		String[] island0SlaveProperties = new String[]{
				"-file", AUTOMATA_ISLANDS_MASTER_SLAVE_PARAMS,
				"-p", "eval.masterproblem=ec.eval.MasterProblem",
				"-p", "eval.master.host=127.0.0.1",
				"-p", "eval.master.port=12350",
				"-p", "eval.masterproblem.max-jobs-per-slave="+maxJobsPerSlave,
				"-p", "eval.compression=true",
				"-p", "exch.server-addr=127.0.0.1",
				"-p", "exch.id=Island0",
				"-p", "exch.client-port=16180",
				"-p", "seed.0=0",
				"-p", "stat.file=$island0.stat",
				"-p", "stat.child.0.file=$koza0.stat",
				"-p", "checkpoint-prefix=island0",
				"-p", "pop.subpop.0.size="+popSize,
				"-p", "generations="+generations,
				"-p", "exch.chatty=true",
//				"-p", "eval.problem.logging=console",
				"-p", "silent=false"
		};
		String[] island1MasterProperties = new String[]{
				"-file", AUTOMATA_ISLANDS_MASTER_SLAVE_PARAMS,
				"-p", "eval.masterproblem=ec.eval.MasterProblem",
				"-p", "eval.master.host=127.0.0.1",
				"-p", "eval.master.port=12351",
				"-p", "eval.masterproblem.max-jobs-per-slave="+maxJobsPerSlave,
				"-p", "eval.compression=true",
				"-p", "exch.server-addr=127.0.0.1",
				"-p", "exch.id=Island1",
				"-p", "exch.client-port=16181",
				"-p", "seed.0=1",
				"-p", "stat.file=$Island1.stat",
				"-p", "stat.child.0.file=$koza1.stat",
				"-p", "checkpoint-prefix=island1",
				"-p", "pop.subpop.0.size="+popSize,
				"-p", "generations="+generations,
				"-p", "exch.chatty=true",
				"-p", "silent=false"
		};
		String[] island1SlaveProperties = new String[]{
				"-file", AUTOMATA_ISLANDS_MASTER_SLAVE_PARAMS,
				"-p", "eval.masterproblem=ec.eval.MasterProblem",
				"-p", "eval.master.host=127.0.0.1",
				"-p", "eval.master.port=12351",
				"-p", "eval.masterproblem.max-jobs-per-slave="+maxJobsPerSlave,
				"-p", "eval.compression=true",
				"-p", "exch.server-addr=127.0.0.1",
				"-p", "exch.id=Island1",
				"-p", "exch.client-port=16181",
				"-p", "seed.0=1",
				"-p", "stat.file=$Island1.stat",
				"-p", "stat.child.0.file=$koza1.stat",
				"-p", "checkpoint-prefix=island1",
				"-p", "pop.subpop.0.size="+popSize,
				"-p", "generations="+generations,
				"-p", "exch.chatty=true",
				"-p", "silent=false"
		};
		
		testMasterSlaveProblem(island0MasterProperties, island0SlaveProperties, 
				island1MasterProperties, island1SlaveProperties);
	}
	public void testMasterSlaveProblem(
			String[] island0MasterProperties, String[] island0SlaveProperties, 
			String[] island1MasterProperties, String[] island1SlaveProperties) {

		Thread island0MasterThread 		= startThread(island0MasterProperties);
		Process island0SlaveProcess 	= startJVM(Slave.class, island0SlaveProperties);
		Process island1MasterProcess 	= startJVM(Evolve.class, island1MasterProperties);
		Process island1SlaveProcess 	= startJVM(Slave.class, island1SlaveProperties);
		LOGGER.info("Waiting for island0MasterThread={}, island0SlaveProcess={}, "
				+ "island1MasterProcess={}, island1SlaveProcess={}", 
				island0MasterThread, island0SlaveProcess, 
				island1MasterProcess, island1SlaveProcess);
		
		try {
			island0SlaveProcess.waitFor(5, TimeUnit.MINUTES);
			island1MasterProcess.waitFor(5, TimeUnit.MINUTES);
			island1SlaveProcess.waitFor(5, TimeUnit.MINUTES);
			LOGGER.info("Done waiting for island0SlaveProcess={}, "
					+ "island1MasterProcess={}, island1SlaveProcess={}", 
					island0SlaveProcess, 
					island1MasterProcess, island1SlaveProcess);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		island0SlaveProcess.destroy();
		island1MasterProcess.destroy();
		island1SlaveProcess.destroy();
		
		try {
			island0MasterThread.join(60 * 1000); // 1 min
			LOGGER.info("Done waiting for island0MasterThread={}", 
					island0MasterThread);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}