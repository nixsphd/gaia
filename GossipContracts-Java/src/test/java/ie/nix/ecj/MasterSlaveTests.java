package ie.nix.ecj;	

import java.io.IOException;
import java.security.Permission;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.eval.Slave;

public class MasterSlaveTests {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String TEST_EGC_AGENT_PARAMS = 
			"src/test/resources/ie/nix/egc/TestEGCAgent.params";
	
	@SuppressWarnings("rawtypes")
	public static Process startJVM(Class mainClass, String[] properties) {
		try {
			String separator = System.getProperty("file.separator");
			String classpath = System.getProperty("java.class.path");
			String java = System.getProperty("java.home") + 
					separator + "bin" + separator + "java";
			ArrayList<String> command = new ArrayList<String>();
			command.add(java);
			command.add("-cp");
			command.add(classpath);
			command.add(mainClass.getName());
			command.addAll(Arrays.asList(properties));
			
			ProcessBuilder processBuilder = new ProcessBuilder(command);
			processBuilder.inheritIO();
			Process process = processBuilder.start();
			return process;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Thread masterThread(String[] masterProperties) {
		Thread thread = new Thread() {
			public void run() {
				LOGGER.debug("threads={}", this);	
				Assertions.assertThrows(RuntimeException.class, () -> {
					ec.Evolve.main(masterProperties);
				});
			}
		};
		thread.start();
		return thread;
	}

	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {
			
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}

	@Test()
	@DisplayName("Master Test")
	public void masterTest() {
		String[] masterProperties = new String[]{
			"-file", TEST_EGC_AGENT_PARAMS,
			"-p", "seed.0=1",
			"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldTenderProblem",
			"-p", "eval.problem.shouldTenderTreeIndex=0",
			"-p", "eval.problem.number_of_runs=10",
			"-p", "generations=2",
			"-p", "pop.subpop.0.size=10",
			"-p", "eval.masterproblem=ec.eval.MasterProblem",
			"-p", "eval.master.host=127.0.0.1",
			"-p", "eval.master.port=12358",
			"-p", "eval.masterproblem.max-jobs-per-slave=1",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false"
		};	
		
		String[] slaveProperties = new String[]{
			"-file", TEST_EGC_AGENT_PARAMS,
			"-p", "seed.0=1",
			"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldTenderProblem",
			"-p", "eval.problem.shouldTenderTreeIndex=0",
			"-p", "eval.problem.number_of_runs=10",
			"-p", "generations=2",
			"-p", "pop.subpop.0.size=10",
			"-p", "eval.masterproblem=ec.eval.MasterProblem",
			"-p", "eval.master.host=127.0.0.1",
			"-p", "eval.master.port=12358",
			"-p", "eval.masterproblem.max-jobs-per-slave=1",
			"-p", "eval.slave.silent=true",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false"
		};	
		
		testProblem(masterProperties, slaveProperties);
	}
	
	public void testProblem(String[] masterProperties, String[] slaveProperties) {

		Thread masterThread = masterThread(masterProperties);
		Process slaveProcess = startJVM(Slave.class, slaveProperties);
		LOGGER.debug("Waiting for masterThread={}, slaveProcess={}", 
				masterThread, slaveProcess);
		
		try {
			slaveProcess.waitFor();
			LOGGER.debug("Done waiting for masterThread={}, slaveProcess={}",
					masterThread, slaveProcess);	
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

}