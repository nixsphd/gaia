package ie.nix.ecj.gp;

import java.util.function.Supplier;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.gp.koza.KozaFitness;
import ec.simple.SimpleProblemForm;
import ie.nix.ecj.TypedData;
import ie.nix.ecj.gp.Variable;

@SuppressWarnings("serial")
public class DoubleTestProblem extends GPProblem implements SimpleProblemForm {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static int numberOfRuns = 100;
	public static double bestAdjustedFitness = 0;
	public static Supplier<Double> expectedResultSupplier = () -> Math.PI;
	
	@Override
	public void evaluate(final EvolutionState state, final Individual individual, 
			final int subpopulation, final int threadnum) {
		
		GPIndividual gpIndividual = (GPIndividual)individual;
    	TypedData actualResult = (TypedData)(input);
    
        if (!individual.evaluated)  {

            double sum = 0.0;
        	int hits = 0;
            
            for (int run = 0; run < 100; run++) {

            	initVariables(state, threadnum);

                gpIndividual.trees[0].child.eval(state, threadnum, actualResult, stack, gpIndividual, this);

                double expectedResult = expectedResultSupplier.get();
                if (expectedResult != actualResult.getDouble()) {
                	 sum +=  Math.abs(expectedResult - actualResult.getDouble());
				} else {
					hits++;
				}
                
                LOGGER.trace("sum={}", sum);
                
            }

            // the fitness better be KozaFitness!
            KozaFitness kozaFitness = ((KozaFitness)individual.fitness);
            kozaFitness.setStandardizedFitness(state, sum);
            kozaFitness.hits = hits;
            individual.evaluated = true;
            
            if (bestAdjustedFitness < kozaFitness.adjustedFitness()) {
            	bestAdjustedFitness = kozaFitness.adjustedFitness();
            	LOGGER.trace("bestAdjustedFitness={}", bestAdjustedFitness);
            }
        }
	}

	public void initVariables(final EvolutionState state, final int threadnum) {
		Variable.A.value = state.random[threadnum].nextBoolean();
		Variable.B.value = state.random[threadnum].nextBoolean();
		Variable.C.value = state.random[threadnum].nextBoolean();
		Variable.X.value = state.random[threadnum].nextDouble() * state.random[threadnum].nextInt(5);
		Variable.Y.value = state.random[threadnum].nextDouble() * state.random[threadnum].nextInt(5);
		Variable.Z.value = state.random[threadnum].nextDouble() * Math.PI;
	}
}

