package ie.nix.ecj.gp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.EvolutionState;
import ec.gp.GPNode;
import ec.util.MersenneTwisterFast;
import ie.nix.ecj.TypedData;
import ie.nix.ecj.gp.ArithmeticOperator.Add;
import ie.nix.ecj.gp.ArithmeticOperator.Div;
import ie.nix.ecj.gp.ArithmeticOperator.Mul;
import ie.nix.ecj.gp.ArithmeticOperator.Sub;
import ie.nix.ecj.gp.ConditionalOperator.IfBoolean;
import ie.nix.ecj.gp.ConditionalOperator.IfDouble;
import ie.nix.ecj.gp.ERC.ERC1;
import ie.nix.ecj.gp.LogicalOperator.And;
import ie.nix.ecj.gp.LogicalOperator.Not;
import ie.nix.ecj.gp.LogicalOperator.Or;
import ie.nix.ecj.gp.RelationalOperator.Equals;
import ie.nix.ecj.gp.RelationalOperator.GreaterThan;
import ie.nix.ecj.gp.RelationalOperator.GreaterThanOrEquals;
import ie.nix.ecj.gp.RelationalOperator.LessThan;
import ie.nix.ecj.gp.RelationalOperator.LessThanOrEquals;
import ie.nix.ecj.gp.RelationalOperator.NotEquals;
import ie.nix.ecj.gp.Variable.A;
import ie.nix.ecj.gp.Variable.B;
import ie.nix.ecj.gp.Variable.C;
import ie.nix.ecj.gp.Variable.X;
import ie.nix.ecj.gp.Variable.Y;
import ie.nix.ecj.gp.Variable.Z;

public class NodeTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	/*
	 * Helper methods
	 */
	
	/*
	 * Arithmetic Operators
	 */
	@Test
	@DisplayName("Add")
	public void add() {
		
		Add node = new Add();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);

		double expectedResult = 3d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Subtract")
	public void subtract() {
		
		Sub node = new Sub();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);

		double expectedResult = -1d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Multiply")
	public void multiply() {
		
		Mul node = new Mul();
		
		node.children = new Node[2];
		node.children[0] = new Constant(10);
		node.children[1] = new Constant(2);

		double expectedResult = 20d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Divide")
	public void divide() {
		
		Div node = new Div();
		
		node.children = new Node[2];
		node.children[0] = new Constant(10);
		node.children[1] = new Constant(2);

		double expectedResult = 5d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Divide by Zero")
	public void divideByZero() {
		
		Div node = new Div();
		
		node.children = new Node[2];
		node.children[0] = new Constant(10);
		node.children[1] = new Constant(0);

		double expectedResult = 0d;
		
		testNode(node, expectedResult);
		
	}
	
	/*
	 * Conditional Operators
	 */
	@Test
	@DisplayName("If Double True")
	public void ifDoubleTrue() {
		
		IfDouble node = new IfDouble();
		
		node.children = new Node[3];
		node.children[0] = new Constant.True();
		node.children[1] = new Constant(1);
		node.children[2] = new Constant(2);

		double expectedResult = 1d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("If Double False")
	public void ifDoubleFalse() {
		
		IfDouble node = new IfDouble();
		
		node.children = new Node[3];
		node.children[0] = new Constant.False();
		node.children[1] = new Constant(1);
		node.children[2] = new Constant(2);

		double expectedResult = 2d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("If Boolean True")
	public void ifBooleanTrue() {
		
		IfBoolean node = new IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new Constant.True();
		node.children[1] = new Constant.True();
		node.children[2] = new Constant.False();

		boolean expectedResult = true;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("If Boolean False")
	public void ifBooleanFalse() {

		IfBoolean node = new IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new Constant.False();
		node.children[1] = new Constant.True();
		node.children[2] = new Constant.False();

		boolean expectedResult = false;
		
		testNode(node, expectedResult);
		
	}

	/*
	 * Logical Operators
	 */
	@Test
	@DisplayName("Not")
	public void not() {
		
		Not node = new Not();
		
		node.children = new Node[1];
		node.children[0] = new Constant.True();

		boolean expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("And")
	public void and() {
		
		And node = new And();
		
		node.children = new Node[2];
		node.children[0] = new Constant.True();
		node.children[1] = new Constant.True();

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.True();
		node.children[1] = new Constant.False();

		expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.False();
		node.children[1] = new Constant.True();

		expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.False();
		node.children[1] = new Constant.False();

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Or")
	public void or() {
		
		Or node = new Or();
		
		node.children = new Node[2];
		node.children[0] = new Constant.True();
		node.children[1] = new Constant.True();

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.True();
		node.children[1] = new Constant.False();

		expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.False();
		node.children[1] = new Constant.True();

		expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.False();
		node.children[1] = new Constant.False();

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	/*
	 * Rational Operators
	 */
	@Test
	@DisplayName("Equals")
	public void equals() {
		
		Equals node = new Equals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(2d);

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Not Equals")
	public void notEquals() {
		
		NotEquals node = new NotEquals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(2d);

		expectedResult = true;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Less Than")
	public void lessThan() {
		
		LessThan node = new LessThan();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(2d);
		node.children[1] = new Constant(1d);

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Less Than Or Equals")
	public void lessThanOrEquals() {
		
		LessThanOrEquals node = new LessThanOrEquals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(2d);
		node.children[1] = new Constant(1d);

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Greater Than")
	public void greaterThan() {
		
		GreaterThan node = new GreaterThan();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(2d);
		node.children[1] = new Constant(1d);

		expectedResult = true;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Greater Than Or Equals")
	public void greaterThanOrEquals() {
		
		GreaterThanOrEquals node = new GreaterThanOrEquals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(2d);
		node.children[1] = new Constant(1d);

		expectedResult = true;
		
		testNode(node, expectedResult);
		
	}
	
	/*
	 * Variables
	 */
	@Test
	@DisplayName("Variable")
	public void variable() {
		
		A nodeA = new A();
		A.value = true;
		boolean expectedResult = true;
		testNode(nodeA, expectedResult);

		B nodeB = new B();
		B.value = false;
		expectedResult = false;
		testNode(nodeB, expectedResult);

		C nodeC = new C();
		C.value = true;
		expectedResult = true;
		testNode(nodeC, expectedResult);
		
		X nodeX = new X();
		X.value = 1d;
		double expectedResult2 = 1d;
		testNode(nodeX, expectedResult2);
		
		Y nodeY = new Y();
		Y.value = 2d;
		expectedResult2 = 2d;
		testNode(nodeY, expectedResult2);
		
		Z nodeZ = new Z();
		Z.value = 3d;
		expectedResult2 = 3d;
		testNode(nodeZ, expectedResult2);
		
	}
	
	/*
	 * ERC
	 */
	@Test
	@DisplayName("ERC")
	public void erc() {
		EvolutionState state = getEvolutionState();
		
		ERC1 nodeA = new ERC1();
		nodeA.resetNode(state, 0);
		double expectedResult = 
				nodeA.evalConstant().getDouble();
		testNode(nodeA, expectedResult);
		
	}
	
	@Test()
	@DisplayName("Fitness Test")
	public void fitnessTest() {

		EvolutionState state = getEvolutionState();
		ERC1 erc1 = new ERC1();
		erc1.resetNode(state, 0);
		ERC1 erc2 = new ERC1();
		erc2.resetNode(state, 0);

		LOGGER.debug("Before erc1.value={}, erc1.max={}", 
				erc1.value, erc1.max);
		LOGGER.debug("Before erc2.value={}, erc2.max={}", 
				erc2.value, erc2.max);
		
		writeReadERC(state, erc1, erc2);

		LOGGER.debug("After erc1.value={}, erc1.max={}", 
				erc1.value, erc1.max);
		LOGGER.debug("After erc2.value={}, erc2.max={}", 
				erc2.value, erc2.max);
		
		Assertions.assertEquals(erc1.value, erc2.value,
				"The values are different erc1=" + erc1.value
						+ " and erc2=" + erc2.value);
		Assertions.assertEquals(erc1.value, erc2.value,
				"The maxs are different erc1=" + erc1.max
						+ " and erc2=" + erc2.max);
	}
	
	private void writeReadERC(EvolutionState state, ERC1 erc1, ERC1 erc2) {
		OutputStream outputStream = new ByteArrayOutputStream();
		DataOutput dataOutput = new DataOutputStream(outputStream);
		
		try {
			erc1.writeNode(state, dataOutput);
			((OutputStream) dataOutput).flush();
			
			InputStream inputStream = new ByteArrayInputStream(((ByteArrayOutputStream)outputStream).toByteArray());
			DataInput dataInput = new DataInputStream(inputStream);
	        
			erc2.readNode(state, dataInput);
	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/*
	 * Random
	 */
	@Test
	@DisplayName("Random")
	public void random() {

		EvolutionState state = getEvolutionState();
		state.random[0].setSeed(0);
		TypedData result = new TypedData();
		Random nodeA = new Random();
		nodeA.eval(state, 0, result, null, null, null);
		
		double expectedResult = result.getDouble();
		state.random[0].setSeed(0);
				
		testNode(nodeA, state, expectedResult);
		
	}
	
	/*
	 * Test Node
	 */
	public void testNode(GPNode node, double expectedResult) {
		// (EvolutionState state, int thread, GPData input, ADFStack stack, 
		//		GPIndividual individual, Problem problem)
		TypedData actualResult = new TypedData();
		node.eval(null, 0, actualResult, null, null, null);

		LOGGER.debug("expectedResult={}", expectedResult);
		LOGGER.debug("actualResult={}", actualResult);
		Assertions.assertEquals(expectedResult, actualResult.getDouble(), 
				"Expected "+expectedResult+", but  got"+actualResult.getDouble()+".");
	}
	
	public void testNode(GPNode node, EvolutionState state, double expectedResult) {
		// (EvolutionState state, int thread, GPData input, ADFStack stack, 
		//		GPIndividual individual, Problem problem)
		TypedData actualResult = new TypedData();
		node.eval(state, 0, actualResult, null, null, null);

		LOGGER.debug("expectedResult={}", expectedResult);
		LOGGER.debug("actualResult={}", actualResult);
		Assertions.assertEquals(expectedResult, actualResult.getDouble(), 
				"Expected "+expectedResult+", but  got"+actualResult.getDouble()+".");
	}

	public void testNode(GPNode node, boolean expectedResult) {
		// (EvolutionState state, int thread, GPData input, ADFStack stack, 
		//		GPIndividual individual, Problem problem)
		TypedData actualResult = new TypedData();
		node.eval(null, 0, actualResult, null, null, null);

		LOGGER.debug("expectedResult={}", expectedResult);
		LOGGER.debug("actualResult={}", actualResult);
		Assertions.assertEquals(expectedResult, actualResult.getBoolean(), 
				"Expected "+expectedResult+", but  got"+actualResult.getBoolean()+".");
	}
	
	public EvolutionState getEvolutionState() {
		EvolutionState state = new EvolutionState();
		state.random = new MersenneTwisterFast[1];
		state.random[0] = new MersenneTwisterFast();
		return state;
	}
	
}