package ie.nix.ecj.gp;

import java.security.Permission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.ecj.gp.Variable.A;
import ie.nix.ecj.gp.Variable.B;
import ie.nix.ecj.gp.Variable.C;
import ie.nix.ecj.gp.Variable.X;
import ie.nix.ecj.gp.Variable.Y;
import ie.nix.ecj.gp.Variable.Z;

public class FunctionSetTest {

	private static final Logger LOGGER = LogManager.getLogger();

	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {
			}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}

	@Test()
	@DisplayName("Constant PI")
	public void constantPI() {

		DoubleTestProblem.expectedResultSupplier = () -> Math.PI;

		testFunctionSetDouble();
	}

	@Test()
	@DisplayName("Constant 12")
	public void constant12() {

		DoubleTestProblem.expectedResultSupplier = () -> 12d;

		testFunctionSetDouble();
	}

	@Test()
	@DisplayName("Variable X")
	public void variableX() {

		DoubleTestProblem.expectedResultSupplier = () -> X.value;

		testFunctionSetDouble();
	}

	@Test()
	@DisplayName("PI * X")
	public void piByX() {

		DoubleTestProblem.expectedResultSupplier = () -> Math.PI * X.value;

		testFunctionSetDouble();
	}

	@Test()
	@DisplayName("Constant * X")
	public void constantByX() {

		DoubleTestProblem.expectedResultSupplier = () -> 6d * X.value;

		testFunctionSetDouble();
	}

	@Test()
	@DisplayName("Regression (X * Z) + Y")
	public void regression() {

		DoubleTestProblem.expectedResultSupplier = () -> (X.value * Z.value) + Y.value;

		testFunctionSetDouble();
	}

	@Test()
	@DisplayName("If Double x > y ? x : y")
	public void ifDouble() {

		DoubleTestProblem.expectedResultSupplier = () -> (X.value > Z.value) ? X.value : Y.value;

		testFunctionSetDouble();
	}

	@Test()
	@DisplayName("Constant TRUE")
	public void constantTrue() {

		BooleanTestProblem.expectedResultSupplier = () -> Boolean.TRUE;

		testFunctionSetBoolean();
	}

	@Test()
	@DisplayName("Relational x > Math.PI")
	public void relationalLessThan() {

		BooleanTestProblem.expectedResultSupplier = () -> X.value > Math.PI;

		testFunctionSetBoolean();
	}

	@Test()
	@DisplayName("Relational y > x")
	public void relationalGreaterThan() {

		BooleanTestProblem.expectedResultSupplier = () -> Y.value < X.value;

		testFunctionSetBoolean();
	}

	@Test()
	@DisplayName("Logical a || b")
	public void logicalOr() {

		BooleanTestProblem.expectedResultSupplier = () -> A.value || B.value;

		testFunctionSetBoolean();
	}

	@Test()
	@DisplayName("Logical a || b && c")
	public void logicalOrAndAnd() {

		BooleanTestProblem.expectedResultSupplier = () -> A.value || B.value && C.value;

		testFunctionSetBoolean();
	}

	@Test()
	@DisplayName("Relational Logical (x < y) || (y < z)")
	public void relationalLogical() {

		BooleanTestProblem.expectedResultSupplier = () -> X.value < Y.value || Y.value < Z.value;

		testFunctionSetBoolean();
	}

	public void testFunctionSetDouble() {
		String[] properties = new String[] { 
				"-file", "src/test/resources/ie/nix/ecj/gp/TestGP.params", 
				"-p", "gp.tc.0.returns=double", 
				"-p", "eval.problem=ie.nix.ecj.gp.DoubleTestProblem", 
				"-p", "seed.0=1", 
//				"-p", "silent=false" 
			};

		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		}); 

		LOGGER.info("bestAdjustedFitness={}", DoubleTestProblem.bestAdjustedFitness);
		Assertions.assertEquals(1d, DoubleTestProblem.bestAdjustedFitness,
				"Did not get a good enough fitness " + DoubleTestProblem.bestAdjustedFitness + ".");
	}

	public void testFunctionSetBoolean() {
		String[] properties = new String[] { 
				"-file", "src/test/resources/ie/nix/ecj/gp/TestGP.params", 
				"-p", "gp.tc.0.returns=boolean", 
				"-p", "eval.problem=ie.nix.ecj.gp.BooleanTestProblem", 
				"-p", "seed.0=1",
//			"-p", "silent=false"	
		};

		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});

		LOGGER.debug("bestAdjustedFitness={}", BooleanTestProblem.bestAdjustedFitness);
		Assertions.assertEquals(1d, BooleanTestProblem.bestAdjustedFitness,
				"Did not get a good enough fitness " + BooleanTestProblem.bestAdjustedFitness + ".");
	}
}