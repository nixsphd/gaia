package ie.nix.ecj.ge;

import java.security.Permission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;

import ie.nix.ecj.gp.BooleanTestProblem;
import ie.nix.ecj.gp.DoubleTestProblem;

public class FunctionSetTest extends ie.nix.ecj.gp.FunctionSetTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	@BeforeAll
	static void beforeAll() {
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}
	
	public void testFunctionSetDouble() {
		String[] properties = new String[]{
			"-file", "src/test/resources/ie/nix/ecj/ge/TestGE.params",
			"-p", "gp.tc.0.returns=double",
			"-p", "eval.problem.problem=ie.nix.ecj.gp.DoubleTestProblem",
			"-p", "ge.species.file.0=DoubleGrammar.grammar",
			"-p", "seed.0=1",
//			"-p", "silent=false"	
		};

		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});
		
		LOGGER.debug("bestAdjustedFitness={}", DoubleTestProblem.bestAdjustedFitness);		
		Assertions.assertEquals(1d, DoubleTestProblem.bestAdjustedFitness, 
				"Did not get a good enough fitness "+DoubleTestProblem.bestAdjustedFitness+".");
	}
	
	public void testFunctionSetBoolean() {
		String[] properties = new String[]{
			"-file", "src/test/resources/ie/nix/ecj/ge/TestGE.params",
			"-p", "gp.tc.0.returns=boolean",
			"-p", "eval.problem.problem=ie.nix.ecj.gp.BooleanTestProblem",
			"-p", "ge.species.file.0=BooleanGrammar.grammar",
			"-p", "seed.0=1",
//			"-p", "silent=false"	
		};
		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});
		
		LOGGER.debug("bestAdjustedFitness={}", BooleanTestProblem.bestAdjustedFitness);		
		Assertions.assertEquals(1d, BooleanTestProblem.bestAdjustedFitness, 
				"Did not get a good enough fitness "+BooleanTestProblem.bestAdjustedFitness+".");
	}
}