package ie.nix.egc;	

import java.io.File;
import java.security.Permission;
import java.util.Arrays;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ec.Evolve;
import ie.nix.ecj.MasterSlaveTests;
import ie.nix.ecj.PeerSimProblem;
import ie.nix.ecj.Statistics.KozaFitnessWithStats;

public class TestEGCAgentTests {

	private static final Logger LOGGER = LogManager.getLogger();

	private static final String TEST_EGC_AGENT_PARAMS = "src/test/resources/ie/nix/egc/TestEGCAgent.params";
	
	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {
			
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}
	
	@Test()
	@DisplayName("Should Tender Test")
	public void shouldTenderTest() {
		String[] properties = new String[]{
			"-file", TEST_EGC_AGENT_PARAMS,
			"-p", "seed.0=1",
			"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldTenderProblem",
			"-p", "eval.problem.shouldTenderTreeIndex=0",
			"-p", "eval.problem.number_of_runs=10",
			"-p", "generations=1",
			"-p", "pop.subpop.0.size=50",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false"
		};	
		testProblem(properties);
	}
	
	@Test()
	@DisplayName("Get Guide Test")
	public void getGuideTest() {
		String[] properties = new String[]{
				"-file", TEST_EGC_AGENT_PARAMS,
				"-p", "seed.0=1",
				"-p", "eval.problem=ie.nix.egc.TestEGCProblem$GetGuideProblem",
				"-p", "eval.problem.getGuideTreeIndex=3",
				"-p", "eval.problem.number_of_runs=10",
				"-p", "generations=1",
				"-p", "pop.subpop.0.size=10",
//				"-p", "eval.problem.logging=console",
//				"-p", "silent=false"	
			};	
		testProblem(properties);
	}

	@Test()
	@DisplayName("Tender To Double Test Lowest FromIndex Best")
	public void tenderToDoubleLowestIndexBestTest() {
			String[] properties = new String[]{
					"-file", TEST_EGC_AGENT_PARAMS,
					"-p", "seed.0=1",
					"-p", "eval.problem=ie.nix.egc.TestEGCProblem$TenderToDoubleProblem",
					"-p", "eval.problem.tenderToDoubleTreeIndex=5",
					"-p", "eval.problem.number_of_runs=10",
					"-p", "generations=1",
					"-p", "pop.subpop.0.size=10",
	//				"-p", "eval.problem.logging=console",
	//				"-p", "silent=false"	
				};	
			testProblem(properties);
		}

	@Test()
	@DisplayName("Should Bid Test")
	public void shouldBidTest() {
		String[] properties = new String[]{
			"-file", TEST_EGC_AGENT_PARAMS,
			"-p", "seed.0=1",
			"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldBidProblem",
			"-p", "eval.problem.shouldBidTreeIndex=1",
			"-p", "eval.problem.number_of_runs=10",
			"-p", "generations=1",
			"-p", "pop.subpop.0.size=50",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false"
		};	
		testProblem(properties);
	}
	
	@Test()
	@DisplayName("Get Offer Test")
	public void getOfferTest() {
		String[] properties = new String[]{
				"-file", TEST_EGC_AGENT_PARAMS,
				"-p", "seed.0=1",
				"-p", "eval.problem=ie.nix.egc.TestEGCProblem$GetOfferProblem",
				"-p", "eval.problem.getOfferTreeIndex=4",
				"-p", "eval.problem.number_of_runs=10",
				"-p", "generations=1",
				"-p", "pop.subpop.0.size=10",
//				"-p", "eval.problem.logging=console",
//				"-p", "silent=false"	
			};	
		testProblem(properties);
	}
	
	@Test()
	@DisplayName("Bid To Double Test Highest FromIndex Best")
	public void bidToDoubleHighestIndexBestTest() {
		String[] properties = new String[]{
				"-file", TEST_EGC_AGENT_PARAMS,
				"-p", "seed.0=1",
				"-p", "eval.problem=ie.nix.egc.TestEGCProblem$BidToDoubleProblem",
				"-p", "eval.problem.bidToDoubleTreeIndex=6",
				"-p", "eval.problem.number_of_runs=10",
				"-p", "generations=1",
				"-p", "pop.subpop.0.size=50",
//				"-p", "eval.problem.logging=console",
//				"-p", "silent=false"	
			};	
		testProblem(properties);
	}
	
	@Test()
	@DisplayName("Should Award Test")
	public void shouldAwardTest() {
			String[] properties = new String[]{
					"-file", TEST_EGC_AGENT_PARAMS,
					"-p", "seed.0=1",
					"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldAwardProblem",
					"-p", "eval.problem.shouldAwardTreeIndex=2",
					"-p", "eval.problem.number_of_runs=10",
					"-p", "generations=1",
					"-p", "pop.subpop.0.size=50",
	//				"-p", "eval.problem.logging=console",
	//				"-p", "silent=false"	
				};	
			testProblem(properties);
		}

	@Test()
	@DisplayName("Big Test")
	public void bigTest() {
		String[] properties = new String[]{
				"-file", TEST_EGC_AGENT_PARAMS,
				"-p", "seed.0=1",
				"-p", "eval.problem=ie.nix.egc.TestEGCProblem$BigProblem",
				"-p", "eval.problem.shouldTenderTreeIndex=0",
				"-p", "eval.problem.shouldBidTreeIndex=1",
				"-p", "eval.problem.shouldAwardTreeIndex=2",
				"-p", "eval.problem.getGuideTreeIndex=3",
				"-p", "eval.problem.getOfferTreeIndex=4",
				"-p", "eval.problem.tenderToDoubleTreeIndex=5",
				"-p", "eval.problem.bidToDoubleTreeIndex=6",
				"-p", "eval.problem.number_of_runs=1",
				"-p", "generations=10",
				"-p", "pop.subpop.0.size=500",
//				"-p", "eval.problem.logging=console",
//				"-p", "silent=false"	
			};	
		testProblem(properties);
	}
	
	@Test()
	@DisplayName("Checkpoint Test")
	public void checkpointTest() {
		String uid = Integer.toString(new Random().nextInt(10000));
		String checkpointPrefix = 
				"test-egc-agent-"+uid;
		String checkpointDirectory = "/tmp";
				
		String[] properties1 = new String[]{
				"-file", TEST_EGC_AGENT_PARAMS,
				"-p", "seed.0=1",
				"-p", "eval.problem=ie.nix.egc.TestEGCProblem$BigProblem",
				"-p", "eval.problem.shouldTenderTreeIndex=0",
				"-p", "eval.problem.shouldBidTreeIndex=1",
				"-p", "eval.problem.shouldAwardTreeIndex=2",
				"-p", "eval.problem.getGuideTreeIndex=3",
				"-p", "eval.problem.getOfferTreeIndex=4",
				"-p", "eval.problem.tenderToDoubleTreeIndex=5",
				"-p", "eval.problem.bidToDoubleTreeIndex=6",
				"-p", "eval.problem.number_of_runs=2",
				"-p", "generations=10",
				"-p", "pop.subpop.0.size=500",
				"-p", "checkpoint=true",
				"-p", "checkpoint-modulo=1",
				"-p", "checkpoint-prefix="+checkpointPrefix,
				"-p", "checkpoint-directory="+checkpointDirectory,
//				"-p", "eval.problem.logging=file",
//				"-p", "silent=false",
			};	
		
		Process firstProcess = MasterSlaveTests.startJVM(Evolve.class, properties1);
		LOGGER.debug("Waiting for secondProcess={}", firstProcess);
		
		try {
			firstProcess.waitFor();
			LOGGER.debug("Done waiting for secondProcess={}", firstProcess);	
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		String[] properties2 = new String[]{
			"-checkpoint", checkpointDirectory+"/"+checkpointPrefix+".2.gz"};	
		
		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties2);
		});
		
	    final File[] files = new File(checkpointDirectory).listFiles(
	    		(dir,name) -> name.matches("test-egc-agent-.*gz" ));
	    Arrays.asList(files).stream().forEach(File::delete);
	    
		double bestAdjustedFitness = PeerSimProblem.getGPProblem().getBestAdjustedFitness();
		LOGGER.debug("bestAdjustedFitness={}", bestAdjustedFitness);		
		Assertions.assertEquals(1d, bestAdjustedFitness, 
				"Did not get a good enough fitness "+bestAdjustedFitness+".");
		
	}

	@Test()
	@DisplayName("evaluation Test")
	public void evaluationTest() {
		String[] properties = new String[]{
			"-file", TEST_EGC_AGENT_PARAMS,
			"-p", "seed.0=1",
			"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldTenderProblem",
			"-p", "eval.problem.shouldTenderTreeIndex=0",
			"-p", "eval.problem.number_of_runs=1",
			"-p", "eval.problem.evaluation_weight=1.0",
			"-p", "eval.problem.convergence_time_weight=0.0",
			"-p", "eval.problem.messages_sent_weight=0.0",
			"-p", "generations=1",
			"-p", "pop.subpop.0.size=50",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false"
		};	
		testProblem(properties);
		
		KozaFitnessWithStats kozaFitnessWithStats = 
    			((KozaFitnessWithStats)PeerSimProblem.getGPProblem().getBestFitness());
		double evaluation = kozaFitnessWithStats.getObjectiveFitnessesMap().
				get(PeerSimProblem.EVALUATION_FITNESS_NAME);
		LOGGER.info("evaluation={}", evaluation);		
		Assertions.assertEquals(0d, evaluation, 
				"Number of messages was "+evaluation+", but should be 0.");
	}
	
	@Test()
	@DisplayName("Convergence Time Test")
	public void convergenceTimeTest() {
		String[] properties = new String[]{
			"-file", TEST_EGC_AGENT_PARAMS,
			"-p", "seed.0=1",
			"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldTenderProblem",
			"-p", "eval.problem.shouldTenderTreeIndex=0",
			"-p", "eval.problem.number_of_runs=1",
			"-p", "eval.problem.evaluation_weight=0.0",
			"-p", "eval.problem.convergence_time_weight=1.0",
			"-p", "eval.problem.messages_sent_weight=0.0",
			"-p", "generations=1",
			"-p", "pop.subpop.0.size=50",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false"
		};	
		testProblem(properties);
		
		KozaFitnessWithStats kozaFitnessWithStats = 
    			((KozaFitnessWithStats)PeerSimProblem.getGPProblem().getBestFitness());
		double convergenceTime = kozaFitnessWithStats.getObjectiveFitnessesMap().
				get(PeerSimProblem.CONVERGENCE_TIME_FITNESS_NAME);
		LOGGER.info("convergenceTime={}", convergenceTime);		
		Assertions.assertEquals(0d, convergenceTime, 
				"Convergence time was "+convergenceTime+", but should be 0.");
	}
	
	@Test()
	@DisplayName("Messages Sent Test")
	public void messagesSentTest() {
		String[] properties = new String[]{
			"-file", TEST_EGC_AGENT_PARAMS,
			"-p", "seed.0=1",
			"-p", "eval.problem=ie.nix.egc.TestEGCProblem$ShouldTenderProblem",
			"-p", "eval.problem.shouldTenderTreeIndex=0",
			"-p", "eval.problem.number_of_runs=1",
			"-p", "eval.problem.evaluation_weight=0.0",
			"-p", "eval.problem.convergence_time_weight=0.0",
			"-p", "eval.problem.messages_sent_weight=1.0",
			"-p", "generations=1",
			"-p", "pop.subpop.0.size=50",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false"
		};	
		testProblem(properties);
		
		KozaFitnessWithStats kozaFitnessWithStats = 
    			((KozaFitnessWithStats)PeerSimProblem.getGPProblem().getBestFitness());
		double messageCount = kozaFitnessWithStats.getObjectiveFitnessesMap().
				get(PeerSimProblem.MESSAGES_SENT_FITNESS_NAME);
		LOGGER.info("messageCount={}", messageCount);		
		Assertions.assertEquals(0d, messageCount, 
				"Number of messages was "+messageCount+", but should be 0.");
	}
	
	public void testProblem(String[] properties) {
	
		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});
		
		double bestAdjustedFitness = PeerSimProblem.getGPProblem().getBestAdjustedFitness();
		
		LOGGER.debug("bestAdjustedFitness={}", bestAdjustedFitness);		
		Assertions.assertEquals(1d, bestAdjustedFitness, 
				"Did not get a good enough fitness "+bestAdjustedFitness+".");
	}
	
}