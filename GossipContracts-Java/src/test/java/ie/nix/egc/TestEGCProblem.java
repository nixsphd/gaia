package ie.nix.egc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.gc.BidMessage;
import ie.nix.gc.TestNode;
import peersim.core.Network;

@SuppressWarnings("serial")
public class TestEGCProblem extends EGCProblem {

	private static final Logger LOGGER = LogManager.getLogger();

	public static final int TENDERER_1_INDEX = 5;
	public static final int TENDERER_2_INDEX = 6;
	public static final int BIDDER_1_INDEX = 10;
	public static final int BIDDER_2_INDEX = 11;

	public static class ShouldTenderProblem extends TestEGCProblem {

		public double evaluateSimulation() {

			int wrongBehaviour = 0;

			for (int n = 0; n < Network.size(); n++) {
				TestNode testNode = getNode(n);
				LOGGER.trace("testNode={}", testNode);

				TestEGCAgent testAgent = getAgent(n);
				LOGGER.trace("testAgent={}", testAgent);

				int index = testNode.getIndex();
				if ((index == TENDERER_1_INDEX || index == TENDERER_2_INDEX)
						&& testAgent.getMessages().tendersSent.size() != 1) {
					wrongBehaviour++;
					LOGGER.trace(
							"Wrong Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
							testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);

				} else if ((index != TENDERER_1_INDEX && index != TENDERER_2_INDEX)
						&& testAgent.getMessages().tendersSent.size() == 1) {
					wrongBehaviour++;
					LOGGER.trace(
							"Wrong Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
							testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);

				} else {
					LOGGER.trace(
							"Right Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
							testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
				}
			}

			double result = wrongBehaviour;
			LOGGER.debug("wrongBehaviour={}, result={}, Network.size()={}", wrongBehaviour, result, Network.size());
			return result;
		}

	}

	public static class ShouldBidProblem extends TestEGCProblem {

		public double evaluateSimulation() {

			int wrongBehaviour = 0;

			for (int n = 0; n < Network.size(); n++) {
				TestNode testNode = getNode(n);
				LOGGER.trace("testNode={}", testNode);

				TestEGCAgent testAgent = getAgent(n);
				LOGGER.trace("testAgent={}", testAgent);

				int index = testNode.getIndex();
				if ((index == BIDDER_1_INDEX || index == BIDDER_2_INDEX)
						&& testAgent.getMessages().bidsSent.size() != 1) {
					if (testAgent.getMessages().tendersReceivedCount > 0) {
						wrongBehaviour++;
						LOGGER.trace("Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, Index()={}, testAgent={}",
								BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);
					} else {
						LOGGER.trace(
								"Ignoring Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, Index()={}, testAgent={}",
								BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);
					}

				} else if ((index != BIDDER_1_INDEX && index != BIDDER_2_INDEX)
						&& testAgent.getMessages().bidsSent.size() == 1) {
					wrongBehaviour++;
					LOGGER.trace("Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, Index()={}, testAgent={}",
							BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);

				} else {
					LOGGER.trace("Right Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, Index()={}, testAgent={}",
							BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);
				}
			}

			double result = wrongBehaviour;
			LOGGER.debug("wrongBehaviour={}, result={}", wrongBehaviour, result);
			return result;
		}
	}

	public static class ShouldAwardProblem extends TestEGCProblem {

		public double evaluateSimulation() {

			int wrongBehaviour = 0;

			for (int n = 0; n < Network.size(); n++) {
				TestNode testNode = getNode(n);
				LOGGER.trace("testNode={}", testNode);

				TestEGCAgent testAgent = getAgent(n);
				LOGGER.trace("testAgent={}", testAgent);

				int index = testNode.getIndex();
				if ((index == TENDERER_1_INDEX || index == TENDERER_2_INDEX)
						&& testAgent.getMessages().awardsSent.size() != 1) {
					if (testAgent.getMessages().bidsReceivedCount > 0) {
						wrongBehaviour++;
						LOGGER.trace(
								"Wrong Behaviour: testAgent={}, Index()={}, "
										+ "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
								testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
					} else {
						LOGGER.trace(
								"Ignoring Behaviour: testAgent={}, Index()={}, "
										+ "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
								testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
					}

				} else if ((index != TENDERER_1_INDEX && index != TENDERER_2_INDEX)
						&& testAgent.getMessages().awardsSent.size() == 1) {
					wrongBehaviour++;
					LOGGER.trace(
							"Wrong Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
							testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);

				} else {
					LOGGER.trace(
							"Right Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
							testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
				}
			}

			double result = wrongBehaviour;
			LOGGER.debug("wrongBehaviour={}, result={}", wrongBehaviour, result);
			return result;
		}
	}

	public static class GetGuideProblem extends TestEGCProblem {

		public double evaluateSimulation() {

			int wrongBehaviour = 0;

			for (int n = 0; n < Network.size(); n++) {
				TestNode testNode = getNode(n);
				LOGGER.trace("testNode={}", testNode);

				TestEGCAgent testAgent = getAgent(n);
				LOGGER.trace("testAgent={}", testAgent);

				int index = testNode.getIndex();
				if ((index == TENDERER_1_INDEX || index == TENDERER_2_INDEX)
						&& testAgent.getMessages().tendersSent.size() == 1) {
					double guide = testAgent.getMessages().tendersSent.get(0).getGuide();
					if (guide != index) {
						wrongBehaviour++;
						LOGGER.trace("Wrong Behaviour: TENDERER_INDEX={}, Index()={}, testAgent={}, guide={}",
								TENDERER_1_INDEX, index, testAgent, guide);
					} else {
						LOGGER.trace("Right Behaviour: TENDERER_INDEX={}, Index()={}, testAgent={}, guide={}",
								TENDERER_1_INDEX, index, testAgent, guide);

					}
				}

			}

			double result = wrongBehaviour;
			LOGGER.debug("wrongBehaviour={}, result={}", wrongBehaviour, result);
			return result;
		}
	}

	public static class GetOfferProblem extends TestEGCProblem {

		public double evaluateSimulation() {

			int wrongBehaviour = 0;

			for (int n = 0; n < Network.size(); n++) {
				TestNode testNode = getNode(n);
				LOGGER.trace("testNode={}", testNode);

				TestEGCAgent testAgent = getAgent(n);
				LOGGER.trace("testAgent={}", testAgent);

				int index = testNode.getIndex();
				if (index == BIDDER_1_INDEX || index == BIDDER_2_INDEX) {
					if (testAgent.getMessages().bidsSent.size() == 1) {
						double offer = testAgent.getMessages().bidsSent.get(0).getOffer();
						if (offer != index) {
							wrongBehaviour++;
							LOGGER.trace(
									"Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX,={}, "
											+ "Index()={}, testAgent={}, offer={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, index, testAgent, offer);
						} else {
							LOGGER.trace(
									"Right Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX,={}, "
											+ "Index()={}, testAgent={}, offer={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, index, testAgent, offer);
						}
					} else {
						LOGGER.trace("Ignore Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX,={}, "
								+ "Index()={}, testAgent={}", BIDDER_1_INDEX, BIDDER_2_INDEX, index, testAgent);
					}
				}

			}

			double result = wrongBehaviour;
			LOGGER.debug("wrongBehaviour={}, result={}", wrongBehaviour, result);
			return result;
		}
	}

	public static class TenderToDoubleProblem extends TestEGCProblem {

		public double evaluateSimulation() {

			int wrongBehaviour = 0;

			for (int n = 0; n < Network.size(); n++) {
				TestNode testNode = getNode(n);
				LOGGER.trace("testNode={}", testNode);

				TestEGCAgent testAgent = getAgent(n);
				LOGGER.trace("testAgent={}", testAgent);

				if (testNode.getIndex() == BIDDER_1_INDEX || testNode.getIndex() == BIDDER_2_INDEX) {
					if (testAgent.getMessages().tendersReceivedCount == 2
							&& testAgent.getMessages().bidsSent.size() == 1) {

						int bidToIndex = testAgent.getMessages().bidsSent.get(0).getTo().getNode().getIndex();
						if (bidToIndex != Math.min(TENDERER_1_INDEX, TENDERER_2_INDEX)) {
							wrongBehaviour++;
							LOGGER.trace(
									"Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, "
											+ "Index()={}, testAgent={}, bidToIndex={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent, bidToIndex);
						} else {
							LOGGER.trace(
									"Right Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={},"
											+ " Index()={}, testAgent={}, bidToIndex={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent, bidToIndex);

						}
					} else {
						LOGGER.trace(
								"Ignore Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}," + " Index()={}, testAgent={}",
								BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);
					}
				}

			}

			double result = wrongBehaviour;
			LOGGER.debug("wrongBehaviour={}, result={}", wrongBehaviour, result);
			return result;
		}
	}

	public static class BidToDoubleProblem extends TestEGCProblem {

		public double evaluateSimulation() {

			int wrongBehaviour = 0;

			for (int n = 0; n < Network.size(); n++) {
				TestNode testNode = getNode(n);
				LOGGER.trace("testNode={}", testNode);

				TestEGCAgent testAgent = getAgent(n);
				LOGGER.trace("testAgent={}", testAgent);

				if (testNode.getIndex() == TENDERER_1_INDEX || testNode.getIndex() == TENDERER_2_INDEX) {
					if (testAgent.getMessages().bidsReceivedCount == 2
							&& testAgent.getMessages().awardsSent.size() == 1) {

						int awardToIndex = testAgent.getMessages().awardsSent.get(0).getTo().getNode().getIndex();
						if (awardToIndex != Math.max(BIDDER_1_INDEX, BIDDER_2_INDEX)) {
							wrongBehaviour++;
							LOGGER.trace(
									"Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, "
											+ "Index()={}, testAgent={}, awardToIndex={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent, awardToIndex);
						} else {
							LOGGER.trace(
									"Right Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={},"
											+ " Index()={}, testAgent={}, awardToIndex={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent, awardToIndex);

						}
					} else {
						LOGGER.trace(
								"Ignore Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}," + " Index()={}, testAgent={}",
								BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);
					}
				}

			}

			double result = wrongBehaviour;
			LOGGER.debug("wrongBehaviour={}, result={}", wrongBehaviour, result);
			return result;
		}
	}

	public static class BigProblem extends TestEGCProblem {

		public double evaluateSimulation() {

			int wrongBehaviour = 0;

			for (int n = 0; n < Network.size(); n++) {
				TestNode testNode = getNode(n);
				TestEGCAgent testAgent = getAgent(n);
				int index = testNode.getIndex();
				LOGGER.trace("testNode={}, testAgent={}, index={}", testNode, testAgent, index);

				if (index == TENDERER_1_INDEX || index == TENDERER_2_INDEX) {
					if (testAgent.getMessages().tendersSent.size() != 1) {
						wrongBehaviour++;
						LOGGER.trace("Wrong Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
								testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
					} else {
						double guide = testAgent.getMessages().tendersSent.get(0).getGuide();
						if (guide != index) {
							wrongBehaviour++;
							LOGGER.trace("Wrong Behaviour: TENDERER_INDEX={}, Index()={}, testAgent={}, guide={}",
									TENDERER_1_INDEX, index, testAgent, guide);
						} 
					}
					
					if (testAgent.getMessages().bidsReceivedCount == 2 && 
						testAgent.getMessages().awardsSent.size() == 1) {
						int awardToIndex = testAgent.getMessages().awardsSent.get(0).getTo().getNode().getIndex();
						if (awardToIndex != Math.max(BIDDER_1_INDEX, BIDDER_2_INDEX)) {
							wrongBehaviour++;
							LOGGER.trace(
									"Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, "
											+ "Index()={}, testAgent={}, awardToIndex={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent, awardToIndex);
						}
					}
					
					if ((testAgent.getMessages().awardsSent.size() != 1) &&
						(testAgent.getMessages().bidsReceivedCount > 0)) {
						wrongBehaviour++;
						LOGGER.trace(
								"Wrong Behaviour: testAgent={}, Index()={}, "
										+ "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
								testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
					}
					

				} else if (index != TENDERER_1_INDEX && index != TENDERER_2_INDEX) {
					if (testAgent.getMessages().tendersSent.size() == 1) {
						wrongBehaviour++;
						LOGGER.trace(
								"Wrong Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
								testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
					} 
					if (testAgent.getMessages().awardsSent.size() == 1) {
						wrongBehaviour++;
						LOGGER.trace(
								"Wrong Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
								testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
					}

				} else {
					LOGGER.trace(
							"Right Behaviour: testAgent={}, Index()={}, " + "TENDERER_1_INDEX={}, TENDERER_2_INDEX={}",
							testAgent, testNode.getIndex(), TENDERER_1_INDEX, TENDERER_2_INDEX);
				}

				if (index == BIDDER_1_INDEX || index == BIDDER_2_INDEX) {
					
					if (testAgent.getMessages().tendersReceivedCount > 0 && 
						testAgent.getMessages().bidsSent.size() != 1) {
						wrongBehaviour++;
						LOGGER.trace("Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, Index()={}, testAgent={}",
								BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);
					} 
					
					if (testAgent.getMessages().tendersReceivedCount == 2
						&& testAgent.getMessages().bidsSent.size() == 1) {

						int bidToIndex = testAgent.getMessages().bidsSent.get(0).getTo().getNode().getIndex();
						if (bidToIndex != Math.min(TENDERER_1_INDEX, TENDERER_2_INDEX)) {
							wrongBehaviour++;
							LOGGER.trace(
									"Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, "
											+ "Index()={}, testAgent={}, bidToIndex={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent, bidToIndex);
						} 
					}
					
					if (testAgent.getMessages().bidsSent.size() == 1 ) {
						BidMessage<TestNode, Integer, Integer> bidMessage = testAgent.getMessages().bidsSent.get(0);
						double offer = bidMessage.getOffer();
						int bidSenderIndex = bidMessage.getFrom().getNodeAdapter().getIndex();
						if (offer != bidSenderIndex) {
							wrongBehaviour++;
							LOGGER.trace(
									"Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX,={}, "
											+ "Index()={}, testAgent={}, offer={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, index, testAgent, offer);
						} else {
							LOGGER.trace(
									"Right Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX,={}, "
											+ "Index()={}, testAgent={}, offer={}",
									BIDDER_1_INDEX, BIDDER_2_INDEX, index, testAgent, offer);
						}
					}
		
				} else if ((index != BIDDER_1_INDEX && index != BIDDER_2_INDEX)
						&& testAgent.getMessages().bidsSent.size() == 1) {
					wrongBehaviour++;
					LOGGER.trace("Wrong Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, Index()={}, testAgent={}",
							BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);

				} else {
					LOGGER.trace("Right Behaviour: BIDDER_1_INDEX={}, BIDDER_2_INDEX={}, Index()={}, testAgent={}",
							BIDDER_1_INDEX, BIDDER_2_INDEX, testNode.getIndex(), testAgent);
				}
			}

			double result = wrongBehaviour;
			LOGGER.trace("wrongBehaviour={}, result={}", wrongBehaviour, result);
			return result;

		}
	}

	protected TestEGCAgent getAgent(int index) {
		return (TestEGCAgent) ((ie.nix.gc.TestGCAgent.Adapter) Network.get(index)).getProtocol(0);
	}

	protected TestNode getNode(int index) {
		return ((ie.nix.gc.TestGCAgent.Adapter) Network.get(index)).getNode();
	}
}
