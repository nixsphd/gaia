package ie.nix.egc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.TypedData;
import ie.nix.ecj.gp.Constant;
import ie.nix.ecj.gp.Variable;
import ie.nix.gc.AwardMessage;
import ie.nix.gc.BidMessage;
import ie.nix.gc.TenderMessage;
import ie.nix.gc.TestNode;
import peersim.core.CommonState;
import peersim.core.Node;

@SuppressWarnings("serial")
public class TestEGCAgent extends EGCAgent<TestNode, Integer, Integer> {

	private static final Logger LOGGER = LogManager.getLogger(); 
	
	public static class NodeIndex extends Variable {

		public static double value;

		public NodeIndex() {
			super("NodeIndex", (results, result) -> ((TypedData)result).set(NodeIndex.value));
		}

	}
	
	public static class FromIndex extends Variable {

		public static double value;

		public FromIndex() {
			super("FromIndex", (results, result) -> ((TypedData)result).set(FromIndex.value));
		}

	}
	
	public static class Five extends Constant {	
		public Five() {
			super("5", 5d);
		}	
	}
	
	public static class Six extends Constant {	
		public Six() {
			super("6", 6d);
		}	
	}
	
	public static class Ten extends Constant {	
		public Ten() {
			super("10", 10d);
		}	
	}
	
	public static class Eleven extends Constant {	
		public Eleven() {
			super("11", 11d);
		}	
	}
	
	public class Messages extends EGCAgent<TestNode, Integer, Integer>.Messages {
		
		public List<TenderMessage<TestNode, Integer, Integer>> tendersSent = new ArrayList<TenderMessage<TestNode, Integer, Integer>>();		
		public int tendersReceivedCount = 0;
		public List<BidMessage<TestNode, Integer, Integer>> bidsSent = new ArrayList<BidMessage<TestNode, Integer, Integer>>();
		public int bidsReceivedCount = 0;
		public List<AwardMessage<TestNode, Integer, Integer>> awardsSent = new ArrayList<AwardMessage<TestNode, Integer, Integer>>();
		public int awardsReceivedCount = 0;
		
		@Override
		protected TenderMessage<TestNode, Integer, Integer> createTenderMessage(ie.nix.gc.GCAgent<TestNode, Integer, Integer> toAgent) {
			TenderMessage<TestNode, Integer, Integer> tenderMessage = super.createTenderMessage(toAgent);
			tendersSent.add(tenderMessage);
			LOGGER.trace("[{}]~tenderMessage={}, tendersSent={}", 
					CommonState.getTime(), tenderMessage, tendersSent.size());
			return tenderMessage;
		}

		@Override
		protected BidMessage<TestNode, Integer, Integer> createBidMessage(TenderMessage<TestNode, Integer, Integer> tenderMessage) {
			BidMessage<TestNode, Integer, Integer> bidMessage = super.createBidMessage(tenderMessage);
			bidsSent.add(bidMessage);
			LOGGER.trace("[{}]~tenderMessage={}, bidMessage={},  bidsSent={}", 
					CommonState.getTime(), tenderMessage, bidMessage, bidsSent.size());
			return bidMessage;
		}

		@Override
		protected AwardMessage<TestNode, Integer, Integer> createAwardMessage(BidMessage<TestNode, Integer, Integer> bidMessage) {
			AwardMessage<TestNode, Integer, Integer> awardMessage = super.createAwardMessage(bidMessage);
			awardsSent.add(awardMessage);
			LOGGER.debug("[{}]~bidMessage={}, awardMessage={}, tendersSent={}", 
					CommonState.getTime(), bidMessage, awardMessage, awardsSent.size());
			return awardMessage;
		}
	}

	public TestEGCAgent(String prefix) {
		super(prefix);
		this.messages = TestEGCAgent.this.new Messages();
		LOGGER.trace("[{}]~new TestAgent", CommonState.getTime()); 
	}
	
	@Override
	public TestEGCAgent clone() {
		TestEGCAgent foo = null;
		foo = (TestEGCAgent)super.clone();
		foo.messages = foo.new Messages();
        LOGGER.trace("[{}]", util.getTime());
		return foo;
	}
	
	@Override
	public String toString() {
		return "TestAgent["
			+ "index="+this.getNode().getIndex()+", "
			+ "tendersSent="+((Messages)messages).tendersSent.size()+", "
			+ "tendersReceived="+((Messages)messages).tendersReceivedCount+", "
			+ "bidsSent="+((Messages)messages).bidsSent.size()+", "
			+ "bidsReceived="+((Messages)messages).bidsReceivedCount+", "
			+ "awardsSent="+((Messages)messages).awardsSent.size()+", "
			+ "awardsReceived="+((Messages)messages).awardsReceivedCount+"]";
	}
	
	public Messages getMessages() {
		return (Messages)messages;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void processEvent(Node node, int protocolID, Object event) {
		if (event instanceof TenderMessage) {
			if (!((Messages)messages).haveRecievedTender((TenderMessage<TestNode, Integer, Integer>)event)) {
				((Messages)messages).tendersReceivedCount++;
				LOGGER.debug("[{}]~agent={}, event={}", 
						CommonState.getTime(), this, event);
			}
		
		} else if (event instanceof BidMessage) {
			((Messages)messages).bidsReceivedCount++;
				LOGGER.debug("[{}]~agent={}, event={}", 
						CommonState.getTime(), this, event);
		
		} else if (event instanceof AwardMessage) {
			((Messages)messages).awardsReceivedCount++;
				LOGGER.debug("[{}]~agent={}, event={}", 
						CommonState.getTime(), this, event);
		
		}
		super.processEvent(node, protocolID, event);
	}

	@Override
	protected boolean shouldTender(TestNode node) {
		NodeIndex.value = node.getIndex();
		if (evolveShouldTender()) {
			return super.shouldTender(node);
		} else {
			int index = node.getIndex();
			return index == TestEGCProblem.TENDERER_1_INDEX ||
				   index == TestEGCProblem.TENDERER_2_INDEX;
		}
	}

	@Override
	protected boolean shouldBid(TenderMessage<TestNode, Integer, Integer> tenderMessage) {
		NodeIndex.value = tenderMessage.getTo().getNode().getIndex();
		FromIndex.value = tenderMessage.getFrom().getNode().getIndex();
		if (evolveShouldBid()) {
			return super.shouldBid(tenderMessage);
		} else {
			int index = tenderMessage.getTo().getNode().getIndex();
			return index == TestEGCProblem.BIDDER_1_INDEX ||
				   index == TestEGCProblem.BIDDER_2_INDEX ;
		}
	}

	@Override
	protected boolean shouldAward(BidMessage<TestNode, Integer, Integer> bestBid) {
		NodeIndex.value = bestBid.getTo().getNode().getIndex();
		FromIndex.value = bestBid.getFrom().getNode().getIndex();
		if (evolveShouldAward()) {
			return super.shouldAward(bestBid);
		} else {
			return true;
		}
	}

	@Override
	protected double getGuide(TestNode node, Integer task) {
		NodeIndex.value = node.getIndex();
		if (evolveGetGuide()) {
			return super.getGuide(node, task);
		} else {
			return NodeIndex.value;
		}
	}
	
	@Override
	protected double getOffer(TenderMessage<TestNode, Integer, Integer> tenderMessage)  {
		NodeIndex.value = tenderMessage.getTo().getNode().getIndex();
		FromIndex.value = tenderMessage.getFrom().getNode().getIndex();
		if (evolveGetOffer()) {
			return super.getOffer(tenderMessage);
		} else {
			return NodeIndex.value;
		}
	}
	
	@Override
	protected double tenderToDouble(TenderMessage<TestNode, Integer, Integer> tenderMessage)  {
		NodeIndex.value = tenderMessage.getTo().getNode().getIndex();
		FromIndex.value = tenderMessage.getFrom().getNode().getIndex();
		if (evolveTenderToDouble()) {
			return super.tenderToDouble(tenderMessage);
		} else {
			return FromIndex.value;
		}
	}
	
	@Override
	protected double bidToDouble(BidMessage<TestNode, Integer, Integer> bidMessage)  {
		NodeIndex.value = bidMessage.getTo().getNode().getIndex();
		FromIndex.value = bidMessage.getFrom().getNode().getIndex();
		if (evolveBidToDouble()) {
			return super.bidToDouble(bidMessage);
		} else {
			return -FromIndex.value;
		}
	}

}
