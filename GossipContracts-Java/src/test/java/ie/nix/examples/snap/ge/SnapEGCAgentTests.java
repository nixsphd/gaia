package ie.nix.examples.snap.ge;	

import java.security.Permission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.ecj.PeerSimProblem;

public class SnapEGCAgentTests {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String SNAP_ECJ_SNAP_AGENT_PARAMS = 
			"src/main/resources/ie/nix/examples/snap/ge/Snap.params";
	
	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {
			
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}
	
	@Disabled
	@Test()
	@DisplayName("EGC Snap GE Test")
	public void egcSnapGETest() {
		String[] properties = new String[]{
			"-file", SNAP_ECJ_SNAP_AGENT_PARAMS,
			"-p", "seed.0=0",
//			"-p", "gp.problem.shouldTenderTreeIndex=-1",
//			"-p", "gp.problem.getGuideTreeIndex=-1",
//			"-p", "gp.problem.tenderToDoubleTreeIndex=-1",
//			"-p", "gp.problem.shouldBidTreeIndex=-1",
//			"-p", "gp.problem.getOfferTreeIndex=-1",
//			"-p", "gp.problem.bidToDoubleTreeIndex=-1",
//			"-p", "gp.problem.shouldAwardTreeIndex=-1",
			"-p", "gp.problem.number_of_runs=1",
			"-p", "generations=10",
			"-p", "pop.subpop.0.size=100",
			"-p", "checkpoint=false",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false",
		};	
		testProblem(properties);
	}
	
	public void testProblem(String[] properties) {

		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});
		
		double bestAdjustedFitness = PeerSimProblem.getGPProblem().getBestAdjustedFitness();
		
		LOGGER.debug("bestAdjustedFitness={}", bestAdjustedFitness);		
		Assertions.assertEquals(1d, bestAdjustedFitness, 
				"Did not get a good enough fitness "+bestAdjustedFitness+".");
	}
	
}