package ie.nix.examples.snap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.gc.NodeAdapter;

public class SnapGCAgentTests {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	/*
	 * Helper methods
	 */
	@SuppressWarnings("unchecked")
	public static int getNumberOfSnaps() {
		int numberOfSnaps = 0;
		for (int n = 0; n < peersim.core.Network.size(); n++) {
			Snap snap = ((NodeAdapter<Snap>)peersim.core.Network.get(n)).getNode();
			if (snap.snapped()) {
				numberOfSnaps++;
			}
			LOGGER.trace("n={}, snap={}",n,snap.getSwapableCard());
		}
		return numberOfSnaps;
	}
	
	@Test
    @DisplayName("GC Snap Test")
    public void gcSnapTest() {

		int numberOfSnaps = 100;
		
		peersim.Simulator.main(new String[] {
			"src/main/resources/ie/nix/examples/snap/Snap.properties",
			"random.seed=1",
			"network.size="+numberOfSnaps,
			"protocol.agent.tender-gossip-counter="+0,
			"control.network.step="+300, 
			"control.observer=ie.nix.examples.snap.SnapObserver",
			"control.observer.step=300",
			"include.control observer convergence_test network"
		});
		
		Assertions.assertTrue(getNumberOfSnaps() == numberOfSnaps,
				"Snaps should be " + numberOfSnaps + ", but is "+ getNumberOfSnaps() + ".");
		
	}
}
