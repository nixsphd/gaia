package ie.nix.examples.snap;	

import java.security.Permission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import ie.nix.ecj.PeerSimProblem;

public class SnapEGCAgentTests {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String SNAP_ECJ_SNAP_AGENT_PARAMS = 
			"src/main/resources/ie/nix/examples/snap/Snap.params";
	
	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {
			
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}
	
	@Test()
	@Tag("SystemTest")
	@DisplayName("EGC Snap Test")
	public void egcSnapTest() {
		String[] properties = new String[]{
			"-file", SNAP_ECJ_SNAP_AGENT_PARAMS,
			"-p", "seed.0=0",
			"-p", "checkpoint=false",
			"-p", "generations=10",
			"-p", "pop.subpop.0.size=10",
//			"-p", "eval.problem.logging=console",
//			"-p", "silent=false",
			"-p", "eval.problem.shouldTenderTreeIndex=-1",
//			"-p", "eval.problem.getGuideTreeIndex=-1",
//			"-p", "eval.problem.tenderToDoubleTreeIndex=-1",
//			"-p", "eval.problem.shouldBidTreeIndex=-1",
//			"-p", "eval.problem.getOfferTreeIndex=-1",
//			"-p", "eval.problem.bidToDoubleTreeIndex=-1",
//			"-p", "eval.problem.shouldAwardTreeIndex=-1",
			"-p", "eval.problem.number_of_runs=1",
			"-p", "gp.problem.evaluation_weight=1.0",
			"-p", "gp.problem.convergence_time_weight=0.0",
			"-p", "gp.problem.messages_sent_weight=0.0"

		};	
		testProblem(properties);
	}
	
	public void testProblem(String[] properties) {

		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});
		
		double bestAdjustedFitness = PeerSimProblem.getGPProblem().getBestAdjustedFitness();
		
		LOGGER.debug("bestAdjustedFitness={}", bestAdjustedFitness);		
		Assertions.assertEquals(1d, bestAdjustedFitness, 
				"Did not get a good enough fitness "+bestAdjustedFitness+".");
	}
	
}