package ie.nix.examples.snap;

import java.time.Duration;
import java.time.Instant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.gc.NodeAdapter;

public class SnapGCAgentTestsWithGossip {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	/*
	 * Helper methods
	 */
	@SuppressWarnings("unchecked")
	public static int getNumberOfSnaps() {
		int numberOfSnaps = 0;
		for (int n = 0; n < peersim.core.Network.size(); n++) {
			Snap snap = ((NodeAdapter<Snap>)peersim.core.Network.get(n)).getNode();
			if (snap.snapped()) {
				numberOfSnaps++;
			}
			LOGGER.trace("n={}, snap={}",n,snap.getSwapableCard());
		}
		return numberOfSnaps;
	}
	
	@Test
    @DisplayName("GC Gossip Snap Test")
    public void gcGossipSnapTest() {

		int numberOfSnaps = 200;
		int tenderGossipCounter = 4;
		int cycles = 100;
		
		Instant start = Instant.now();
		
		peersim.Simulator.main(new String[] {
			"src/main/resources/ie/nix/examples/snap/Snap.properties",
			"random.seed=1",
			"network.size="+numberOfSnaps,
			"protocol.agent.tender-gossip-counter="+tenderGossipCounter,
			"simulation.endtime="+(cycles*300),
			"control.observer=ie.nix.examples.snap.SnapObserver",
			"control.observer.step=300",
			"include.control observer convergence_test network"
		});

		Instant end = Instant.now();
		
		Duration interval = Duration.between(start, end);
		System.out.println("Execution time in sec: " + interval.toMillis() * 1000);
		
		Assertions.assertTrue(getNumberOfSnaps() == numberOfSnaps,
				"Snaps should be " + numberOfSnaps + ", but is "+ getNumberOfSnaps() + ".");
		
	}
	
}
