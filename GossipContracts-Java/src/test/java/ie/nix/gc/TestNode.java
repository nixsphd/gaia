package ie.nix.gc;

public class TestNode {
	
	private int index;
	
	public TestNode(int index) {
		this.index = index;
	}
	
	@Override
	public String toString() {
		return "TestNode["+index+"]";
	}

	public int getIndex() {
		return index;
	}
	
}