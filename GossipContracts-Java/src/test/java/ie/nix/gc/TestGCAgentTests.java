package ie.nix.gc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.gc.StrategyGCAgent.BidToDouble;
import ie.nix.gc.StrategyGCAgent.TenderToDouble;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.edsim.EDSimulator;

@SuppressWarnings("unchecked")
public class TestGCAgentTests {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static int random_seed = 1;
	private static int neighbourhoodSize = 2;
	private static int agentProtocol = 0;

	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] { 
				"src/test/resources/ie/nix/gc/TestGCAgent.properties"});
		
	}

	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to " + random_seed);
	}

	@AfterEach
	void afterEach() {
		TestGCAgent.clearAll();
		System.out.println();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}

	/*
	 * Helper methods
	 */
	protected TestNode getNode(int n) {
		TestNode testNode = ((NodeAdapter<TestNode>)Network.get(n)).getNode();
		LOGGER.trace("[{}]~testNode={}", CommonState.getTime(), testNode);
		return testNode;
	}
	
	protected TestGCAgent getAgent(int n) {
		TestGCAgent testAgent = ((TestGCAgent)((NodeAdapter<TestNode>)Network.get(n)).getProtocol(agentProtocol));
		LOGGER.trace("[{}]~testAgent={}", CommonState.getTime(), testAgent);
		return testAgent;
	}
	
	protected Stream<TestGCAgent> getAgents() {
        Stream.Builder<TestGCAgent> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(getAgent(n));;
		}
		return streamBuilder.build();
	}
	
	/*
	 * Send Tenders
	 */

	@Test
	@DisplayName("One Should Tender")
	public void oneShouldTenderTest() {
		shouldTenderTest(1);

	}

	@Test
	@DisplayName("Two Should Tender")
    public void twoShouldTenderTest() {
		shouldTenderTest(2);
	
	}

	protected void shouldTenderTest(int numberToTender) {
		shouldTenderOnce(numberToTender);
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfTenderersSent() == numberToTender,
				"Tenderers should be " + numberToTender + ", but is " + getNumberOfTenderersSent() + ".");
	}

	@SuppressWarnings("rawtypes")
	protected void shouldTenderOnce(int numberToTender) {
		TestGCAgent.shouldTender = (StrategyGCAgent agent, Object nodeObject) -> {
			TestNode node = (TestNode)nodeObject;
			boolean should = 
					!agent.getState().isTendering() && (node.getIndex() % (Network.size() / numberToTender) == 0);
			LOGGER.trace("StrategyAgent.shouldTender[{}]~node={}, should={}", 
					CommonState.getTime(), node, should);
			return should;
		};
	}

	protected int getNumberOfTenderersSent() {
		int tenderSent = 0;
		for (int n = 0; n < Network.size(); n++) {
			tenderSent += getAgent(n).tendersSent;
		}
		LOGGER.debug("[{}]~tenderSent={}", CommonState.getTime(), tenderSent);
		return tenderSent;
	}

	/*
	 * Tenders Received
	 */
	@Test
	@DisplayName("One Tenders Received")
	public void oneTendersReceivedTest() {
		int numberToTender = 1;
		tendersReceivedTest(numberToTender, numberToTender * neighbourhoodSize);
	
	}

	@Test
	@DisplayName("Two Tenders Received")
	public void twoTendersReceivedTest() {
		int numberToTender = 2;
		tendersReceivedTest(numberToTender, numberToTender * neighbourhoodSize);	
	}

	protected void tendersReceivedTest(int numberToTender, int numberOfTendersToBeReceived) {
		shouldTenderOnce(numberToTender);
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfTendersReceived() == numberOfTendersToBeReceived,
				"Tenders recieved id should be " + numberOfTendersToBeReceived + ", but is "
						+ getNumberOfTendersReceived() + ".");
	}
	
	protected int getNumberOfTendersReceived() {
		int tendersReceived = 0;
		for (int n = 0; n < Network.size(); n++) {
			tendersReceived += getAgent(n).tendersReceived;
		}
		return tendersReceived;
	}
	
	/*
	 * Should Bid
	 */
	@Test
	@DisplayName("One Should Bid")
	public void oneShouldBidTest() {
		int numberToTender = 1;
		int numberToBid = 1;
		oneShouldBidTest(numberToTender, numberToBid);
		
	}

	protected void oneShouldBidTest(int numberToTender, int numberToBid) {
		shouldTenderOnce(numberToTender);
		
		TestGCAgent.shouldTender = (StrategyGCAgent agent, Object nodeObject) -> {
			TestNode node = (TestNode)nodeObject;
			boolean should = node.getIndex() == 0;
			LOGGER.trace("StrategyAgent.shouldTender[{}]~node={}, should={}", 
					CommonState.getTime(), node, should);
			return should;
		};
		
		StrategyGCAgent.shouldBid = (StrategyGCAgent agent, TenderMessage tenderMessage) -> {
			boolean should = ((TestNode)tenderMessage.getTo().getNode()).getIndex() == 3;
			LOGGER.debug("StrategyAgent.shouldBid[{}]~tenderMessage={}, should={}", 
					CommonState.getTime(), tenderMessage, should);
			return should;
		};
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfBidsSent() == numberToBid,
				"Bid sent should be " + numberToBid + ", but is " + getNumberOfBidsSent() + ".");
		
	}

	@SuppressWarnings({ "rawtypes" })
	protected void oneShouldBidOnce() {
		
		StrategyGCAgent.getTask = (StrategyGCAgent agent, Object node) -> {
			ArrayList<Integer> toBidders = new ArrayList<Integer>();
			toBidders.add(0);
			LOGGER.debug("StrategyAgent.getTask[]~tenderMessage={}, task={}", 
					CommonState.getTime(), toBidders);
			return toBidders;
		};
		
		StrategyGCAgent.shouldBid = (StrategyGCAgent agent, TenderMessage tenderMessage) -> {
			ArrayList<Integer> bidders = (ArrayList<Integer>)tenderMessage.getTask();
			boolean should = bidders.contains(((TestNode)tenderMessage.getTo().getNode()).getIndex());
			LOGGER.debug("StrategyAgent.shouldBid[{}]~tenderMessage={}, should={}", 
					CommonState.getTime(), tenderMessage, should);
			return should;
		};
	
	}
	
	protected int getNumberOfBidsSent() {
		int bidsSent = 0;
		for (int n = 0; n < Network.size(); n++) {
			bidsSent += getAgent(n).bidsSent;
		}
		return bidsSent;
		
	}

	@Test
	@DisplayName("Two Should Bid")
	public void twoShouldBidTest() {
		int numberToTender = 1;
		shouldBidTest(numberToTender, numberToTender * neighbourhoodSize);
		
	}

	@Test
	@DisplayName("Four Should Bid")
	public void fourShouldBidTest() {
		
		CommonState.initializeRandom(16);
		
		int numberToTender = 2;
		shouldBidTest(numberToTender, numberToTender * neighbourhoodSize);
		
	}

	protected void shouldBidTest(int numberToTender, int numberToBid) {
		shouldTenderOnce(numberToTender);
		
		allShouldBidOnce();
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfBidders() == numberToBid,
				"Bidders should be " + numberToBid + ", but is " + getNumberOfBidders() + ".");
	}

	@SuppressWarnings({ "rawtypes" })
	protected void allShouldBidOnce() {
		StrategyGCAgent.shouldBid = (StrategyGCAgent agent, TenderMessage tenderMessage) -> {
			boolean should = !agent.getState().isBidding() && !agent.getState().isTendering();
			LOGGER.debug("StrategyAgent.shouldBid[{}]!should={}", 
					CommonState.getTime(), should);
			return should;
		};
	}
	
	protected int getNumberOfBidders() {
		int bidders = 0;
		for (int n = 0; n < Network.size(); n++) {
			if (getAgent(n).bidsSent > 0) {
				bidders++;
			}
		}
		return bidders;
	}

	/*
	 * Award Sent
	 */
	@Test
	@DisplayName("One Awarded")
	public void oneAwarded() {
		int numberToTender = 1;
		int numberToAward = 1;
		awardTest(numberToTender, numberToAward);
		
	}

	@Test
	@DisplayName("Two Awarded")
	public void twoAwarded() {
		int numberToTender = 2;
		awardTest(numberToTender, numberToTender);
		
	}

	protected void awardTest(int numberToTender, int numberToAward) {
		shouldTenderOnce(numberToTender);
		allShouldBidOnce();
		awardOne();
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfAwardsSent() == numberToAward,
				"Awards should be " + numberToAward + ", but is " + getNumberOfAwardsSent() + ".");
	}

	@SuppressWarnings("rawtypes")
	protected void awardOne() {
		StrategyGCAgent.bidToDouble = (StrategyGCAgent agent, BidMessage bidMessage) -> {
			return ((TestNode)bidMessage.getFrom().getNode()).getIndex();
		};
		StrategyGCAgent.shouldAward = (StrategyGCAgent agent, BidMessage bidMessage) -> {
			return true;
		};
	}
	
	protected int getNumberOfAwardsSent() {
		int awards = 0;
		for (int n = 0; n < Network.size(); n++) {
			if (getAgent(n).awardsSent > 0) {
				awards++;
			}
		}
		return awards;
	}
	
	/*
	 * Award Received
	 */
	@Test
	@DisplayName("One Award Received")
	public void oneAwardReceivedTest() {
		int numberToTender = 1;
		int numberOfAwardsToBeReceived = 1;
		awardReceivedTest(numberToTender, numberOfAwardsToBeReceived);	
	}

	@Test
	@DisplayName("Two Award Received")
	public void twoAwardReceivedTest() {
		int numberToTender = 2;
		awardReceivedTest(numberToTender, numberToTender);	
	}

	protected void awardReceivedTest(int numberToTender, int numberOfAwardsToBeReceived) {
		shouldTenderOnce(numberToTender);
		allShouldBidOnce();
		awardOne();
		
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfAwardsReceived() == numberOfAwardsToBeReceived,
				"Awards recieved id should be " + numberOfAwardsToBeReceived + ", but is "
						+ getNumberOfAwardsReceived() + ".");
	}
	
	protected int getNumberOfAwardsReceived() {
		int awardsReceived = 0;
		for (int n = 0; n < Network.size(); n++) {
			awardsReceived += getAgent(n).awardsReceived;
		}
		return awardsReceived;
	}
	
	/*
	 * Tender Comparator
	 */
	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Tender Message Expiring Last")
	public void tenderMessageExpiringLastTest() {
			
		TenderMessage tm1 = new TenderMessage(null, null, 50, 0, null, 0d);
		TenderMessage tm2 = new TenderMessage(null, null, 1, 0, null, 0d);
		TenderMessage tm3 = new TenderMessage(null, null, 10, 0, null, 0d);
		
		List<TenderMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(tm1);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm2);

		TenderToDouble tenderToDouble = StrategyGCAgent.expiringLastTenderToDouble;
		
		tenderMessageComparatorTest(actualTenderMessages, expectedTenderMessages, tenderToDouble);
	}

	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Tender Message Expiring First")
	public void tenderMessageExpiringFirstTest() {
		
		TenderMessage tm1 = new TenderMessage(null, null, 50, 0, null, 0d);
		TenderMessage tm2 = new TenderMessage(null, null, 1, 0, null, 0d);
		TenderMessage tm3 = new TenderMessage(null, null, 10, 0, null, 0d);
		
		List<TenderMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(tm2);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm1);
		
		TenderToDouble tenderToDouble = StrategyGCAgent.expiringFirstTenderToDouble;
		
		tenderMessageComparatorTest(actualTenderMessages, expectedTenderMessages, tenderToDouble);
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Tender Message Highest Guide")
	public void tenderMessageHighestGuideTest() {

		TenderMessage tm1 = new TenderMessage(null, null, 50, 0, null, 1.0);
		TenderMessage tm2 = new TenderMessage(null, null, 1, 0, null, 5.0);
		TenderMessage tm3 = new TenderMessage(null, null, 10, 0, null, 2.0);
		
		List<TenderMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(tm2);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm1);
		
		TenderToDouble tenderToDouble = StrategyGCAgent.highestGuideTenderToDouble;
		
		tenderMessageComparatorTest(actualTenderMessages, expectedTenderMessages, tenderToDouble);
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Tender Message Lowest Guide")
	public void tenderMessageLowestGuideTest() {
		
		TenderMessage tm1 = new TenderMessage(null, null, 50, 0, null, 1.0);
		TenderMessage tm2 = new TenderMessage(null, null, 1, 0, null, 5.0);
		TenderMessage tm3 = new TenderMessage(null, null, 10, 0, null, 2.0);
		
		List<TenderMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(tm1);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm2);
		
		TenderToDouble tenderToDouble = StrategyGCAgent.lowestGuideTenderToDouble;
		
		tenderMessageComparatorTest(actualTenderMessages, expectedTenderMessages, tenderToDouble);
	}
	

	@SuppressWarnings("rawtypes")
	private void tenderMessageComparatorTest(List<TenderMessage> actualTenderMessages,
			List<TenderMessage> expectedTenderMessages, TenderToDouble expiringLastTenderToDouble) {

		Comparator<TenderMessage> comparator = 
				Comparator.comparingDouble(
						tenderMessage -> expiringLastTenderToDouble.tenderToDouble(null, tenderMessage));
		
		Collections.sort(actualTenderMessages, comparator);
		
		LOGGER.debug("[{}]~expectedTenderMessages={}", CommonState.getTime(), expectedTenderMessages);
		LOGGER.debug("[{}]~actualTenderMessages={}", CommonState.getTime(), actualTenderMessages);

		Assertions.assertEquals(expectedTenderMessages, actualTenderMessages);
		
	}

	@SuppressWarnings("rawtypes")
	public void tenderMessageComparatorTest(List<TenderMessage> actualTenderMessages, List<TenderMessage> expectedTenderMessages,
			Comparator<TenderMessage> comparator) {
		Collections.sort(actualTenderMessages, comparator);
		
		LOGGER.debug("[{}]~expectedTenderMessages={}", CommonState.getTime(), expectedTenderMessages);
		LOGGER.debug("[{}]~actualTenderMessages={}", CommonState.getTime(), actualTenderMessages);

		Assertions.assertEquals(expectedTenderMessages, actualTenderMessages);
	}
	
	/*
	 * Bid Comparator
	 */
	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Bid Message Expiring Last")
	public void bidMessageExpiringLastTest() {

		TenderMessage tm = new TenderMessage(null, null, 20, 0, null, 0d);
		BidMessage bm1 = new BidMessage(tm, 50, null, 0d);
		BidMessage bm2 = new BidMessage(tm, 1, null, 0d);
		BidMessage bm3 = new BidMessage(tm, 10, null, 0d);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm1);
		expectedTenderMessages.add(bm3);
		expectedTenderMessages.add(bm2);

		BidToDouble bidToDouble = StrategyGCAgent.expiringLastBidToDouble;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, bidToDouble);
	}

	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Bid Message Expiring First")
	public void bidMessageExpiringFirstTest() {
		
		TenderMessage tm = new TenderMessage(null, null, 20, 0, null, 0d);
		BidMessage bm1 = new BidMessage(tm, 50, null, 0d);
		BidMessage bm2 = new BidMessage(tm, 1, null, 0d);
		BidMessage bm3 = new BidMessage(tm, 10, null, 0d);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm2);
		expectedTenderMessages.add(bm3);
		expectedTenderMessages.add(bm1);

		BidToDouble bidToDouble = StrategyGCAgent.expiringFirstBidToDouble;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, bidToDouble);
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Bid Message Highest Offer")
	public void bidMessageHighestOfferTest() {

		TenderMessage tm = new TenderMessage(null, null, 20, 0, null, 0d);
		BidMessage bm1 = new BidMessage(tm, 50, 2.0);
		BidMessage bm2 = new BidMessage(tm, 1, 4.0);
		BidMessage bm3 = new BidMessage(tm, 10, 1.0);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm2);
		expectedTenderMessages.add(bm1);
		expectedTenderMessages.add(bm3);

		BidToDouble bidToDouble = StrategyGCAgent.highestOfferBidToDouble;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, bidToDouble);
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Bid Message Lowest Offer")
	public void bidMessageLowestOfferTest() {

		TenderMessage tm = new TenderMessage(null, null, 20, 0, null, 0d);
		BidMessage bm1 = new BidMessage(tm, 50, 2.0);
		BidMessage bm2 = new BidMessage(tm, 1, 4.0);
		BidMessage bm3 = new BidMessage(tm, 10, 1.0);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm3);
		expectedTenderMessages.add(bm1);
		expectedTenderMessages.add(bm2);

		BidToDouble bidToDouble = StrategyGCAgent.lowestOfferBidToDouble;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, bidToDouble);
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Test
	@DisplayName("Bid Message Lowest Offer (negative numbers)")
	public void bidMessageLowestOfferNegNumbersTest() {

		TenderMessage tm = new TenderMessage(null, null, 20, 0, null, 0d);
		BidMessage bm1 = new BidMessage(tm, 50, 2.0);
		BidMessage bm2 = new BidMessage(tm, 1, -4.0);
		BidMessage bm3 = new BidMessage(tm, 10, -1.0);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm2);
		expectedTenderMessages.add(bm3);
		expectedTenderMessages.add(bm1);

		BidToDouble bidToDouble = StrategyGCAgent.lowestOfferBidToDouble;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, bidToDouble);
	}

	public void bidMessageComparatorTest(List<BidMessage> actualBidMessages, List<BidMessage> expectedBidMessages,
			BidToDouble bidToDouble) {

		Comparator<BidMessage> comparator = 
				Comparator.comparingDouble(
						bidMessage -> bidToDouble.bidToDouble(null, bidMessage));
		
		Collections.sort(actualBidMessages, comparator);
		
		LOGGER.debug("[{}]~expectedBidMessages={}", CommonState.getTime(), expectedBidMessages);
		LOGGER.debug("[{}]~actualBidMessages={}", CommonState.getTime(), actualBidMessages);

		Assertions.assertEquals(expectedBidMessages, actualBidMessages);
		
	}
	
	@SuppressWarnings("rawtypes")
	public void bidMessageComparatorTest(List<BidMessage> actualBidMessages, List<BidMessage> expectedBidMessages,
			Comparator<BidMessage> comparator) {
		
		Collections.sort(actualBidMessages, comparator);
		
		LOGGER.debug("[{}]~expectedBidMessages={}", CommonState.getTime(), expectedBidMessages);
		LOGGER.debug("[{}]~actualBidMessages={}", CommonState.getTime(), actualBidMessages);

		Assertions.assertEquals(expectedBidMessages, actualBidMessages);
	}
}