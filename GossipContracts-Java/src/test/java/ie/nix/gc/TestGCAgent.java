package ie.nix.gc;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.core.Node;

public class TestGCAgent extends StrategyGCAgent<TestNode, ArrayList<Integer>, Object> {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class Adapter extends NodeAdapter<TestNode> {
		
		public Adapter(String prefix) {
			super(prefix);
			LOGGER.debug("Adapter[" + CommonState.getTime() +", "+getIndex()+"]");
		}

		@Override
		public void initNode(NodeAdapter<TestNode> nodeAdapter) {
			nodeAdapter.setNode(new TestNode(nodeAdapter.getIndex()));
		}

	}
	
	public class Messages extends GCAgent<TestNode, ArrayList<Integer>, Object>.Messages {
		
		@Override
//		protected TenderMessage<TestNode,ArrayList<Integer>,Object> createTenderMessage(Agent<TestNode,ArrayList<Integer>,Object> toAgent, ArrayList<Integer> task) {
		protected TenderMessage<TestNode,ArrayList<Integer>,Object> createTenderMessage(GCAgent<TestNode,ArrayList<Integer>,Object> toAgent) {
			TenderMessage<TestNode,ArrayList<Integer>,Object> tenderMessage = super.createTenderMessage(toAgent);
			tendersSent++;
			LOGGER.trace("[{}]~tenderMessage={}, tendersSent={}", 
					CommonState.getTime(), tenderMessage, tendersSent);
			return tenderMessage;
		}

		@Override
		protected BidMessage<TestNode,ArrayList<Integer>, Object> createBidMessage(TenderMessage<TestNode,ArrayList<Integer>,Object> tenderMessage) {
			BidMessage<TestNode,ArrayList<Integer>,Object> bidMessage = super.createBidMessage(tenderMessage);
			bidsSent++;
			LOGGER.trace("[{}]~tenderMessage={}, bidMessage={},  bidsSent={}", 
					CommonState.getTime(), tenderMessage, bidMessage, bidsSent);
			return bidMessage;
		}

		@Override
		protected AwardMessage<TestNode,ArrayList<Integer>,Object> createAwardMessage(BidMessage<TestNode,ArrayList<Integer>,Object> bidMessage) {
			AwardMessage<TestNode,ArrayList<Integer>,Object> awardMessage = super.createAwardMessage(bidMessage);
			awardsSent++;
			LOGGER.debug("[{}]~bidMessage={}, awardMessage={}, tendersSent={}", 
					CommonState.getTime(), bidMessage, awardMessage, awardsSent);
			return awardMessage;
		}
	}
	
	public int tendersSent = 0;		
	public int tendersReceived = 0;
	public int bidsSent = 0;
	public int bidsReceived = 0;
	public int awardsReceived = 0;
	public int awardsSent = 0;
	
	public TestGCAgent(String prefix) {
		super(prefix);
		this.messages = TestGCAgent.this.new Messages();
		LOGGER.trace("[{}]~new TestAgent", CommonState.getTime()); 
	}
	
	@Override
	public TestGCAgent clone() {
		TestGCAgent foo = null;
		foo = (TestGCAgent)super.clone();
		foo.messages = foo.new Messages();
        LOGGER.trace("[{}]", util.getTime());
		return foo;
	}

	@Override
	public String toString() {
		return "TestAgent["
			+ "index="+getNode().getIndex()+", "
			+ "tendersSent="+tendersSent+", "
			+ "tendersReceived="+tendersReceived+", "
			+ "bidsSent="+bidsSent+", "
			+ "bidsReceived="+bidsReceived+", "
			+ "awardsSent="+awardsSent+", "
			+ "awardsReceived="+awardsReceived+"]";
	}
	
	@Override
	public void processEvent(Node node, int protocolID, Object event) {
		if (event instanceof TenderMessage) {
			tendersReceived++;
			LOGGER.debug("[{}]~agent={}, event={}", 
					CommonState.getTime(), this, event);
		
		} else if (event instanceof BidMessage) {
			bidsReceived++;
			LOGGER.debug("[{}]~agent={}, event={}", 
					CommonState.getTime(), this, event);
		
		} else if (event instanceof AwardMessage) {
			awardsReceived++;
			LOGGER.debug("[{}]~agent={}, event={}", 
					CommonState.getTime(), this, event);
		
		}
		super.processEvent(node, protocolID, event);
	}

}
