package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import peersim.core.CommonState;

public class TestGCAgentWIthGossipTests {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private TestGCAgentTests helper;
	static int N = 10000;
	static int K = 2;
	static int tenderGossipCount = 1;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/ie/nix/gc/TestGCAgent.properties",
				"network.size="+N,
				"init.network.k="+K,
				"protocol.agent.tender-gossip-counter="+tenderGossipCount,
				});
		
	}

	@BeforeEach
	void beforeEach() {
		helper = new TestGCAgentTests();
		helper.beforeEach();
	}

	@AfterEach
	void afterEach() {
		helper.afterEach();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	/*
	 * Tenders Sent
	 */
	@Test
	@Tag("UnitTest")
    @DisplayName("One Should Tender")
    public void oneShouldTenderTest() {
		helper.shouldTenderTest(1);
    
    }
	
	@Test
	@Tag("UnitTest")
	@DisplayName("Two Should Tender")
    public void twoShouldTenderTest() {
		helper.shouldTenderTest(2);
	
	}
	
	/*
	 * Tenders Received
	 */
	@Test
	@Tag("UnitTest")
	@DisplayName("One Tenderer Tenders Received")
	public void oneTendersReceivedTest() {
		int numberToTender = 1;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.tendersReceivedTest(numberToTender, expectedTenders);
	
	}

	@Test
	@Tag("UnitTest")
	@DisplayName("Two Tenderers Tenders Received")
	public void twoTendersReceivedTest() {
		int numberToTender = 2;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.tendersReceivedTest(numberToTender, expectedTenders);	
	}

	protected int calculateExpectedTenders(int numberToTender) {
		int tendersPerTestNode = 0;
		for (int l = tenderGossipCount+1; l > 0; l--) {
			tendersPerTestNode += Math.pow(K, l);
		}
		LOGGER.debug("[{}]~networkSize={}, tenderGossipCount={}, tendersPerTestNode={}", 
				CommonState.getTime(), K, tenderGossipCount, tendersPerTestNode);
		return numberToTender * tendersPerTestNode;
	}
	
	@Test
	@Tag("UnitTest")
	@DisplayName("Two Should Bid")
	public void twoShouldBidTest() {
		int numberToTender = 1;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.shouldBidTest(numberToTender, expectedTenders);
		
	}
	
	@Test
	@Tag("UnitTest")
	@DisplayName("Four Should Bid")
	public void fourShouldBidTest() {
		int numberToTender = 2;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.shouldBidTest(numberToTender, expectedTenders);
		
	}

}