# install.packages("plotly")

library(plotly)

# *** Occam Trees ***
# Subpopulation 0:
# Occam Tree 0:
#     true
# Occam Tree 1:
#     max(task, (task - swappableCard))
# Occam Tree 2:
#     max(myCard, task)
# Occam Tree 3:
#     ((task == myCard) || (min(0.0, task) == max(task, myCard)))
# Occam Tree 4:
#     task
# Occam Tree 5:
#     max(proposal, min(swappableCard, guide))
# Occam Tree 6:
#     !((((min(swappableCard, proposal) - myCard) - ((proposal / myCard) * (task + guide))) == (max(0.0, task) - ((proposal - swappableCard) + max(0.0, task)))))

shouldTender = function(myCard, swappableCard) {
    # print(sprintf("myCard=%d, swappableCard=%d -> %d",
    #               myCard, swappableCard, TRUE))
    TRUE
}

getGuide  = function(myCard, swappableCard, task) {
    TRUE
}

tenderToDouble = function(swappableCard, task) {
    # print(sprintf("myCard=%d, swappableCard=%d, task=%d, guide=%d -> %d",
    #               myCard, swappableCard, task, guide, max(myCard, task)))
    swappableCard + task
}

shouldBid = function(myCard, swappableCard, task, guide) {
    task == myCard
}

getOffer = function(myCard, swappableCard, task, guide) {
    task
}

bidToDouble = function(myCard, swappableCard, task, guide, proposal, offer) {
    max(proposal, task)
}

shouldAward = function(myCard, swappableCard, task, guide, proposal, offer) {
    # !((((min(swappableCard, proposal) - myCard) - ((proposal / myCard) * (task + guide))) == (max(0.0, task) - ((proposal - swappableCard) + max(0.0, task)))))
    !((((min(task, proposal) - myCard) - ((proposal / myCard) * (task + task))) == (task - proposal)))
}

myCardSeq = seq(0, 100)
taskSeq = seq(0, 100)
proposalSeq = seq(0, 100)

swappableCard = 100

MyCard_Task <- data.frame()
for(myCard in myCardSeq) {
    for (task in taskSeq) {
        MyCard_Task <- rbind(MyCard_Task, 
            data.frame(MyCard=myCard, Task=task, 
                # shouldTender=shouldTender(myCard, task),
                tenderToDouble=tenderToDouble(
                    swappableCard=swappableCard, task=task)
                # shouldBid=shouldBid(myCard=myCard,
                    # swappableCard=swappableCard, task=task, guide=task)
                ))
    }
}

MyCard_Proposal <- data.frame()
for(myCard in myCardSeq) {
    for (proposal in proposalSeq) {
        MyCard_Proposal <- rbind(MyCard_Proposal, 
            data.frame(MyCard=myCard, Proposal=proposal, 
                bidToDouble=bidToDouble(myCard=myCard, swappableCard,
                    task=swappableCard, guide=swappableCard, proposal=proposal, 
                    offer=swappableCard),
                shouldAward=shouldAward(myCard=myCard, swappableCard, 
                    task=swappableCard, guide=swappableCard, proposal=proposal, 
                    offer=swappableCard)))
    }
}

myCard = 100

Task_Proposal <- data.frame()
for(task in taskSeq) {
    for (proposal in proposalSeq) {
        Task_Proposal <- rbind(Task_Proposal, 
            data.frame(Task=task, Proposal=proposal, 
                tenderToDouble=tenderToDouble(myCard=myCard, 
                    swappableCard=proposal,task=task, guide=task),
                shouldBid=shouldBid(myCard=myCard, swappableCard=proposal, 
                    task=task, guide=task),
                bidToDouble=bidToDouble(myCard=myCard, swappableCard=task, 
                    task=task, guide=task, proposal=proposal, offer=task),
                shouldAward=shouldAward(myCard=myCard, swappableCard=task, 
                    task=task, guide=task, proposal=proposal, offer=task)))
    }
}

ggplot(MyCard_Task, aes(x=MyCard, y=Task)) +
    geom_tile(aes(fill = shouldTender))

ggplot(MyCard_Task, aes(MyCard, y=Task)) + 
    geom_tile(aes(fill = tenderToDouble)) +
    scale_fill_gradient2(midpoint=0)
path <- file.path("tenderToDoubleMyCard_Task")
savePlot(path)

ggplot(Task_Proposal, aes(x=Task, y=Proposal)) + 
    geom_tile(aes(fill = tenderToDouble)) +
    scale_fill_gradient2(midpoint=0)

ggplot(MyCard_Task, aes(MyCard, y=Task)) +
    geom_tile(aes(fill = shouldBid))

ggplot(Task_Proposal, aes(x=Task, y=Proposal)) +
    geom_tile(aes(fill = shouldBid))

ggplot(Task_Proposal, aes(x=Task, y=Proposal)) + 
    geom_tile(aes(fill = bidToDouble)) +
    scale_fill_gradient2(midpoint=0)

ggplot(MyCard_Proposal, aes(x=MyCard, y=Proposal)) + 
    geom_tile(aes(fill = bidToDouble)) +
    scale_fill_gradient2(midpoint=0)

ggplot(Task_Proposal, aes(x=Task, y=Proposal)) +
    geom_tile(aes(fill = shouldAward))

ggplot(MyCard_Proposal, aes(x=MyCard, y=Proposal)) +
    geom_tile(aes(fill = shouldAward))
