package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class AwardMessage<N,T, P> extends Message<N,T,P> {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	private final int contractID;
	private final T task;
	private final P proposal;
	
	public AwardMessage(BidMessage<N,T,P> bidMessage) {
		this(bidMessage, bidMessage.getExpirationTime());
	}
	
	public AwardMessage(BidMessage<N,T, P> bidMessage, long expirationTime) {
		super(bidMessage.to, bidMessage.from, expirationTime, 0);
		this.contractID = bidMessage.getContractID();
		this.task = bidMessage.getTask();
		this.proposal = bidMessage.getProposal();
		LOGGER.trace("[{}]~{}", CommonState.getTime(), this);
	}
	
	@Override
	public String toString() {
		return "AwardMessage " 
				+ from + " -> " + to + ", "
				+ "expirationTime=" + expirationTime +", "
				+ "contractID=" + contractID + ", "
				+ "task=" + task +", "
				+ "proposal=" + proposal;
	}
	
	public int getContractID() {
		return contractID;
	}

	public T getTask() {
		return task;
	}

	public P getProposal() {
		return proposal;
	}
	
}