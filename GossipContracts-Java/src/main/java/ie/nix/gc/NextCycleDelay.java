package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.edsim.NextCycleEvent;

public class NextCycleDelay extends NextCycleEvent {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final int DEVIATION = 10;

	public NextCycleDelay(String n) {
		super(n);
	}

	@Override
	protected long nextDelay(long step) {
		double nextGaiussian = CommonState.r.nextGaussian();
		long nextDelay = (long)(step + ((step/DEVIATION) * nextGaiussian));
		LOGGER.trace("step={}, nextGaiussian={}, nextDelay={}", 
				step, nextGaiussian, nextDelay);
		
		return super.nextDelay(nextDelay);
	}

}
