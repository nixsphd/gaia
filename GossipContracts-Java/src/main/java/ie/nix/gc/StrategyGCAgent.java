package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class StrategyGCAgent<N,T,P> extends GCAgent<N,T,P> {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public interface ShouldTender<N,T,P> {
		boolean shouldTender(StrategyGCAgent<N,T,P> agent, N node);
	}
	
	public interface ShouldBid<N,T,P>  {
		boolean shouldBid(StrategyGCAgent<N,T,P> agent, TenderMessage<N,T,P> tenderMessage);
	}

	public interface ShouldAward<N,T,P> {
		boolean shouldAward(StrategyGCAgent<N,T,P> agent, BidMessage<N,T,P> bidMessage);
	}
	
	public interface GetTask<N,T,P> {
		T getTask(StrategyGCAgent<N,T,P> agent, N node);
	}
	
	public interface GetProposal<N,T,P> {
		P getProposal(StrategyGCAgent<N,T,P> agent, TenderMessage<N,T,P> tenderMessage);
	}
	
	public interface GetGuide<N,T,P> {
		double getGuide(StrategyGCAgent<N,T,P> agent, N node, T task);
	}
	
	public interface GetOffer<N,T,P> {
		double getOffer(StrategyGCAgent<N,T,P> agent, TenderMessage<N,T,P> tenderMessage);
	}
	
	public interface TenderToDouble<N,T,P> {
		double tenderToDouble(StrategyGCAgent<N,T,P> agent, TenderMessage<N,T,P> tenderMessage);
	}
	
	public interface BidToDouble<N,T,P> {
		double bidToDouble(StrategyGCAgent<N,T,P> agent, BidMessage<N,T,P> bidMessage);
	}

	public interface Award<N,T,P> {
		void award(StrategyGCAgent<N,T,P> agent, AwardMessage<N,T,P> awardMessage);
	}

	public interface Awarded<N,T,P> {
		void awarded(StrategyGCAgent<N,T,P> agent, AwardMessage<N,T,P> awardMessage);
	}
	
	@SuppressWarnings("rawtypes")
	public static TenderToDouble expiringLastTenderToDouble = new TenderToDouble() {
		@Override
		public double tenderToDouble(StrategyGCAgent agent, TenderMessage tenderMessage) {
			return -tenderMessage.expirationTime;
		}
	};
	
	@SuppressWarnings("rawtypes")
	public static TenderToDouble expiringFirstTenderToDouble = new TenderToDouble() {
		@Override
		public double tenderToDouble(StrategyGCAgent agent, TenderMessage tenderMessage) {
			return tenderMessage.expirationTime;
		}
	};
	
	@SuppressWarnings("rawtypes")
	public static TenderToDouble highestGuideTenderToDouble = new TenderToDouble() {
		@Override
		public double tenderToDouble(StrategyGCAgent agent, TenderMessage tenderMessage) {
			return -tenderMessage.getGuide();
		}
	};

	@SuppressWarnings("rawtypes")
	public static TenderToDouble lowestGuideTenderToDouble = new TenderToDouble() {
		@Override
		public double tenderToDouble(StrategyGCAgent agent, TenderMessage tenderMessage) {
			return tenderMessage.getGuide();
		}
	};
	
	@SuppressWarnings("rawtypes")
	public static BidToDouble expiringLastBidToDouble = new BidToDouble() {
		@Override
		public double bidToDouble(StrategyGCAgent agent, BidMessage bidMessage) {
			return -bidMessage.expirationTime;
		}
	};

	@SuppressWarnings("rawtypes")
	public static BidToDouble expiringFirstBidToDouble = new BidToDouble() {
		@Override
		public double bidToDouble(StrategyGCAgent agent, BidMessage bidMessage) {
			return bidMessage.expirationTime;
		}
	};

	@SuppressWarnings("rawtypes")
	public static BidToDouble highestOfferBidToDouble = new BidToDouble() {
		@Override
		public double bidToDouble(StrategyGCAgent agent, BidMessage bidMessage) {
			return -bidMessage.getOffer();
		}
	};

	@SuppressWarnings("rawtypes")
	public static BidToDouble lowestOfferBidToDouble = new BidToDouble() {
		@Override
		public double bidToDouble(StrategyGCAgent agent, BidMessage bidMessage) {
			return bidMessage.getOffer();
		}
	};
	
	@SuppressWarnings("rawtypes")
	public static ShouldTender shouldTender;
	@SuppressWarnings("rawtypes")
	public static ShouldBid shouldBid;
	@SuppressWarnings("rawtypes")
	public static ShouldAward shouldAward;

	@SuppressWarnings("rawtypes")
	public static GetTask getTask;
	@SuppressWarnings("rawtypes")
	public static GetProposal getProposal;
	
	@SuppressWarnings("rawtypes")
	public static GetGuide getGuide;
	@SuppressWarnings("rawtypes")
	public static GetOffer getOffer;
		
	@SuppressWarnings("rawtypes")
	public static TenderToDouble tenderToDouble;
	@SuppressWarnings("rawtypes")
	public static BidToDouble bidToDouble;
	
	@SuppressWarnings("rawtypes")
	public static Award award;
	@SuppressWarnings("rawtypes")
	public static Awarded awarded;

	public static void clearAll() {
		
		StrategyGCAgent.shouldTender = null;
		StrategyGCAgent.shouldBid = null;
		StrategyGCAgent.shouldAward = null;
		
		StrategyGCAgent.getTask = null;
		StrategyGCAgent.getProposal = null;
		StrategyGCAgent.getGuide = null;
		StrategyGCAgent.getOffer = null;
		
		StrategyGCAgent.bidToDouble = null;
		StrategyGCAgent.tenderToDouble = null;
		
		StrategyGCAgent.award = null;
		StrategyGCAgent.awarded = null;
		
	}

	public StrategyGCAgent(String prefix) {
		super(prefix);
	}

	@Override
	public String toString() {
		return "StrategyAgent";
	}

	@SuppressWarnings("unchecked")
	@Override
	protected boolean shouldTender(N node) {
		boolean should;
		if (shouldTender != null) {
			should = shouldTender.shouldTender(this, node);
		} else {
			should = super.shouldTender(node);
		}
		LOGGER.trace("[{}]~node={} -> {}", CommonState.getTime(), node, should);
		return should;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected boolean shouldBid(TenderMessage<N,T,P> tenderMessage) {
		boolean should;
		if (shouldBid != null) {
			should = shouldBid.shouldBid(this, tenderMessage);
		} else {
			should = super.shouldBid(tenderMessage);
		}
		LOGGER.debug("[{}]~tenderMessage={} -> {}", 
				CommonState.getTime(), tenderMessage, should);
		return should;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected boolean shouldAward(BidMessage<N,T,P> bidMessage) {
		boolean should;
		if (shouldAward != null) {
			should = shouldAward.shouldAward(this, bidMessage);
		} else {
			should = super.shouldAward(bidMessage);
		}
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), should);
		return should;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T getTask(N node) {
		T task;
		if (getTask != null) {
			task = (T)getTask.getTask(this, node);
		} else {
			task = super.getTask(node);
		}
		LOGGER.debug("[{}]~task={}", 
				CommonState.getTime(), task);
		return task;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected double getGuide(N node, T task) {
		double guide;
		if (getGuide != null) {
			guide = getGuide.getGuide(this, node, task);
		} else {
			guide = super.getGuide(node, task);
		}
		LOGGER.debug("[{}]~node={}, task={} -> {}", 
				CommonState.getTime(), node, task, guide);
		return guide;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected P getProposal(TenderMessage<N,T,P> tenderMessage) {
		P proposal;
		if (getProposal != null) {
			proposal = (P)getProposal.getProposal(this, tenderMessage);
		} else {
			proposal = super.getProposal(tenderMessage);
		}
		LOGGER.debug("[{}]~tenderMessage={}, proposal={} -> {}", 
				CommonState.getTime(), tenderMessage, proposal);
		return proposal;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected double getOffer(TenderMessage<N,T,P> tenderMessage) {
		double offer;
		if (getOffer != null) {
			offer = getOffer.getOffer(this, tenderMessage);
		} else {
			offer = super.getOffer(tenderMessage);
		}
		LOGGER.debug("[{}]~tenderMessage={} -> {}", 
				CommonState.getTime(), tenderMessage, offer);
		return offer;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected double tenderToDouble(TenderMessage<N,T,P> tenderMessage) {
		LOGGER.trace("[{}]", CommonState.getTime());
		if (tenderToDouble != null) {
			return tenderToDouble.tenderToDouble(this, tenderMessage);
		} else {
			return super.tenderToDouble(tenderMessage);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected double bidToDouble(BidMessage<N,T,P> bidMessage) {
		LOGGER.trace("[{}]", CommonState.getTime());
		if (bidToDouble != null) {
			return bidToDouble.bidToDouble(this, bidMessage);
		} else {
			return super.bidToDouble(bidMessage);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void award(AwardMessage<N,T,P> awardMessage) {
		LOGGER.trace("[{}]~awardMessage={}", CommonState.getTime(), awardMessage);
		if (award != null) {
			award.award(this, awardMessage);
		} else {
			super.award(awardMessage);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void awarded(AwardMessage<N,T,P> awardMessage) {
		LOGGER.trace("[{}]~awardMessage={}", CommonState.getTime(), awardMessage);
		if (awarded != null) {
			awarded.awarded(this, awardMessage);
		} else {
			super.awarded(awardMessage);
		}
	}

}
