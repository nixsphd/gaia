package ie.nix.gc;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Message<N,T,P> implements Cloneable {

	@SuppressWarnings("rawtypes")
	private static Map<Class<? extends Message>, Long> messageCounts = 
			new HashMap<Class<? extends Message>, Long>();
	
	@SuppressWarnings("rawtypes")
	public static void incrumentMessageCount(Class<? extends Message> messageClass) {
		Long messageCount =  Optional.ofNullable(messageCounts.get(messageClass))
				.orElse(Long.valueOf(0));  
		messageCounts.put(messageClass, Long.valueOf(messageCount + 1));
	}

	@SuppressWarnings("rawtypes")
	public static long getMessageCount(Class<? extends Message> messageClass) {
		Long messageCount =  Optional.ofNullable(messageCounts.get(messageClass))
				.orElse(Long.valueOf(0));  
		return messageCount;
	}
	
	public static void resetMessageCount() {
		messageCounts.clear();
	}
	
	public static long getMessageCount() {
		long messageCount =  messageCounts.values().stream().
				mapToLong(count -> count.longValue()).sum();
		return messageCount;
	}
	
	protected GCAgent<N,T,P> to;
	protected final GCAgent<N,T,P> from;
	protected final long expirationTime;
	protected int gossipCounter;

	public Message(GCAgent<N,T,P> from, GCAgent<N,T,P> to, int expirationTime) {
		this(from, to, expirationTime, 0);
	}
	
	public Message(GCAgent<N,T,P> from, GCAgent<N,T,P> to, long expirationTime, int gossipCounter) {
		this.from = from;
		this.to = to;
		this.expirationTime = expirationTime;
		this.gossipCounter = gossipCounter;
		Message.incrumentMessageCount(getClass());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Message<N,T,P> clone() {
		Message<N,T,P> foo = null;
		try {
			foo = (Message<N,T,P>)super.clone();
			Message.incrumentMessageCount(getClass());
			
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return foo;
	}
	
	@Override
	public String toString() {
		return "Message " 
				+ from + " -> " + to + ", "
				+ "expirationTime=" + expirationTime;
	}

	public GCAgent<N,T,P> getTo() {
		return to;
	}

	public void setTo(GCAgent<N, T, P> to) {
		this.to = to;
	}

	public GCAgent<N,T,P> getFrom() {
		return from;
	}
	
	public boolean hasExpired(long now) {
		return now >= expirationTime;
	}

	public long getExpirationTime() {
		return expirationTime;
	}

	public long getGossipCounter() {
		return gossipCounter;
	}
	
	public void decrumentGossipCounter() {
		gossipCounter--;		
	}
	
}
