package ie.nix.gc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.edsim.EDSimulator;

// TODO - Could I separate everything calling PeerSim API into a subclass, maybe a Protocol 
// Adaptor, like I have a NodeAdaptor?
public class GCAgent<N,T,P> implements CDProtocol, EDProtocol {

	private static final Logger LOGGER = LogManager.getLogger();
	
	/******************************************************************************************
	 * 
	 * 	Util APIs
	 * 
	 ******************************************************************************************/
	protected class Util implements Cloneable {
		
		@SuppressWarnings("unchecked")
		public Util clone() {
			Util foo = null;
			try {
				foo = (Util)super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return foo;
		}
		
		public long getTime() {
			return CommonState.getTime();
		}
		
		public int randomInt(int max) {
			return CommonState.r.nextInt(max);
		}
		
		public double randomDouble() {
			return CommonState.r.nextDouble();
		}
	}
	
	/******************************************************************************************
	 * 
	 * 	Config APIs
	 * 
	 ******************************************************************************************/
	protected class Config implements Cloneable {

		private static final String STEP = "step";
		private static final String TENDER_TIMEOUT = "tender-timeout";
		private static final String TENDER_GOSSIP_COUNTER = "tender-gossip-counter";
	    private static final String BID_BUFFER = "bid-buffer";
		private static final String AWARD_TIMEOUT = "award-timeout";
		
		private final String prefix;	
		private final int step;	
	    private final int tenderTimeout;
	    private final int tenderGossipCount;
		private final int bidBuffer;
	    private final int awardTimeout;
	    	    
	    public Config(String prefix) {
	    	this.prefix = prefix;
	    	this.step = Configuration.getInt(prefix+"."+STEP);
			this.tenderTimeout = Configuration.getInt(prefix+"."+TENDER_TIMEOUT, DEFAULT_TENDER_TIMEOUT);
			this.tenderGossipCount = Configuration.getInt(prefix+"."+TENDER_GOSSIP_COUNTER, DEFAULT_TENDER_GOSSIP_COUNT);
			this.bidBuffer = Configuration.getInt(prefix+"."+BID_BUFFER, DEFAULT_BID_BUFFER);
			this.awardTimeout = Configuration.getInt(prefix+"."+AWARD_TIMEOUT, DEFAULT_AWARD_TIMEOUT);
	    }
		
		@SuppressWarnings("unchecked")
		public Config clone() {
			Config foo = null;
			try {
				foo = (Config)super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return foo;
		}
		
		protected int getStep() {
			return step;
		}
	    
		protected int getTenderTimeout() {
			return tenderTimeout;
		}

		protected int getAwardTimeout() {
			return awardTimeout;
		}

		protected boolean shouldGossip() {
			return tenderGossipCount > 0;
		}

		protected int getTenderGossipCount() {
			return tenderGossipCount;
		}

		protected int getBidBuffer() {
			return bidBuffer;
		}

	}
	
	/******************************************************************************************
	 * 
	 * 	Message APIs
	 * 
	 ******************************************************************************************/
	protected class Messages implements Cloneable {
	    
		private TenderMessage<N,T,P> tendersSent;		
	    private HashMap<Integer, TenderMessage<N,T,P>> tendersRecieved;
		private BidMessage<N,T,P> bidSent;
	    private List<BidMessage<N,T,P>> bidsRecieved;	
	    private AwardMessage<N,T,P> awardsSent;
		private AwardMessage<N,T,P> awardRecieved;
		
	    public Messages() {
			this.tendersSent = null;
			this.tendersRecieved = new HashMap<Integer, TenderMessage<N,T,P>>();
			this.bidSent = null;
			this.bidsRecieved = new ArrayList<BidMessage<N,T,P>>();
			this.awardsSent = null;
			this.awardRecieved = null;
		}
	    
	    @SuppressWarnings("unchecked")
		public Messages clone() {
	    	Messages foo = null;
			try {
				foo = (Messages)super.clone();
				foo.tendersRecieved = new HashMap<Integer, TenderMessage<N,T,P>>();
				foo.bidsRecieved = new ArrayList<BidMessage<N,T,P>>();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return foo;
		}

		private void clearMessages() {
			this.tendersSent = null;
			this.tendersRecieved.clear();
			this.bidSent = null;
			this.bidsRecieved.clear();
			this.awardsSent = null;
			this.awardRecieved = null;	
		}

		protected TenderMessage<N, T, P> createAndSendTenderMessage(GCAgent<N, T, P> agent, 
				Stream<GCAgent<N, T, P>> neighbourNodes) {
			// Create the tender message
			TenderMessage<N,T,P> tenderMessage = messages.createTenderMessage(GCAgent.this);
			// Prepare for the bids.
			messages.addSentTender(tenderMessage);
			transport.sendTenderMessages(tenderMessage, neighbourNodes);
			
			return tenderMessage;
		}
		

		/*
		 * Create messages
		 */
		protected TenderMessage<N,T,P> createTenderMessage(GCAgent<N,T,P> toAgent) {
			long expirationTime = util.getTime() + config.getTenderTimeout();
			int tenderGossipCount = config.getTenderGossipCount();
			T task = getTask(toAgent.getNode());
			double guide = getGuide(toAgent.getNode(), task);
			TenderMessage<N,T,P> tenderMessage = 
					new TenderMessage<N,T,P>(GCAgent.this, toAgent, expirationTime, tenderGossipCount, task, guide);
			LOGGER.trace("[{}]~tenderMessage={}", util.getTime(), tenderMessage);		
			return tenderMessage;
		}
		
		protected BidMessage<N, T, P> createAndSendBidMessage(TenderMessage<N, T, P> tenderMessage) {
			BidMessage<N,T,P> bidMessage = messages.createBidMessage(tenderMessage);
			messages.setBidSent(bidMessage);
			transport.sendMessage(bidMessage);
			return bidMessage;
		}

		protected BidMessage<N,T,P> createBidMessage(TenderMessage<N,T,P> tenderMessage) {
			BidMessage<N,T,P> bidMessage = new BidMessage<N,T,P>(tenderMessage, 
					getProposal(tenderMessage), getOffer(tenderMessage));
			LOGGER.trace("[{}]~bidMessage={}", util.getTime(), bidMessage);		
			return bidMessage;
		}

		protected AwardMessage<N, T, P> createAndSendAwardMessage(BidMessage<N, T, P> bidMessage) {
			AwardMessage<N,T,P> awardMessage = messages.createAwardMessage(bidMessage);
			messages.addAwardSent(awardMessage);
			transport.sendMessage(awardMessage);
			return awardMessage;
		}

		protected AwardMessage<N,T,P> createAwardMessage(BidMessage<N,T,P> bidMessage) {
			// Calculate the expiration time.
			long expirationTime = util.getTime() + config.getAwardTimeout();
			return createAwardMessage(bidMessage, expirationTime);
		}
		
		protected final AwardMessage<N,T,P> createAwardMessage(BidMessage<N,T,P> bidMessage, long expirationTime) {
			AwardMessage<N,T,P> awardMessage = new AwardMessage<N,T,P>(bidMessage, expirationTime);
			LOGGER.trace("[{}]~awardMessage={}", util.getTime(), awardMessage);
			return awardMessage;
		}
		
		/*
		 * Message Handlers
		 */
		/*
		 * Tender Received
		 */
		private final void handleTenderMessage(TenderMessage<N,T,P> tenderMessage) {
			// If it hasn't expired and it isn't already processed, as in we have it.
			if (!tenderMessage.hasExpired(util.getTime()) && !haveRecievedTender(tenderMessage)) {
				LOGGER.trace("[{}]]~tenderMessage={}", util.getTime(), tenderMessage);
				
				// Add the tender
				addRecievedTender(tenderMessage);

				long bidEvaluationDelay = 
						(tenderMessage.getExpirationTime() - config.getBidBuffer()) - util.getTime();
				LOGGER.trace("[{}] delay={}", util.getTime(), bidEvaluationDelay);	
				scheduler.addCallback(bidEvaluationDelay, new Callback(() -> {
					LOGGER.trace("[{}] slept for {}", util.getTime(), bidEvaluationDelay);	
					// Add a reevaluation just before this tender expires.
					evaluateTenders(tenderMessage);
				}));
				
				// Expire the tender after (hence the +1) the Tender is expired.
				long tenderExpirationDelay = (tenderMessage.getExpirationTime() + 1 - util.getTime());
				scheduler.addCallback(tenderExpirationDelay, new Callback(() -> {
					LOGGER.trace("[{}]~tenderRecievedt={}, after delay={}", 
							util.getTime(), tenderMessage, tenderExpirationDelay);
					removeRecievedTender(tenderMessage);
				}));
			
				if (config.shouldGossip() && tenderMessage.getGossipCounter() > 0) {
					Stream<GCAgent<N,T,P>> neighbours = network.getNeighbours();
					transport.resendTenderMessage(tenderMessage, neighbours);
				}
			}
		}
		
		/*
		 * Bids Received
		 */
		private final void handleBidMessage(BidMessage<N,T,P> bidMessage) {
			LOGGER.trace("[{}]~contractID={}, bidMessage={}", util.getTime(), bidMessage.getContractID(), bidMessage);
			List<BidMessage<N,T,P>> contractBids = getBidsRecieved(bidMessage.getContractID());
			if (contractBids != null) {
				// Add the new bid
				contractBids.add(bidMessage);
			} else {
				LOGGER.debug("[{}]~Ignoring {}, not acception bids for contractID={}", 
						util.getTime(), bidMessage, bidMessage.getContractID());
			}
		}

		/*
		 * Awards Received
		 */	
		private final void handleAwardMessage(AwardMessage<N,T,P> awardMessage) {
			LOGGER.debug("[{}]~awardMessage={}", util.getTime(), awardMessage);
			
			assert (hasSentBid()) : 
				this + " was not bidding but just won an award for " + awardMessage;
			assert (getBidSent().getContractID() == awardMessage.getContractID()) : 
				this + " was bidding for " + getBidSent() + " but just won an award for " + awardMessage;
			
			setRecievedAward(awardMessage);
					
			// Expire the award at the timeout, need the +1 to make sure the award is expired. 
			long awardExpirationTime = (awardMessage.getExpirationTime() + 1) - util.getTime();
			LOGGER.debug("[{}]~cleanup in awardExpirationTime={}, awardRecieved={}, awardMessage={}", 
					util.getTime(), awardExpirationTime, getAwardReceived(), awardMessage);
			scheduler.addCallback(awardExpirationTime, new Callback(() -> {
				LOGGER.debug("[{}]~Calling clearRecievedAward() awardRecieved={}, awardMessage={}", 
						util.getTime(), getAwardReceived(), awardMessage);
				clearRecievedAward();
			}));
			
			awarded(awardMessage);
		
		}
		
		/*
		 * Tenders Sent
		 */
	    private boolean hasSentTender() {
			return tendersSent != null;
		}
	    
		@SuppressWarnings("unused")
		private final TenderMessage<N,T,P> getSentTender() {
			return tendersSent;
		}

		private final void addSentTender(TenderMessage<N,T,P> tenderMessage) {
			LOGGER.trace("[{}]~contractID={}", util.getTime(), tenderMessage.getContractID());
			
			assert(tendersSent == null): "The tendersSent is not null, "+tendersSent;
			
			// Add to archive of the Tender messages
			tendersSent = tenderMessage;
			// Create an empty list to store the bids for the tender.
			bidsRecieved = new ArrayList<BidMessage<N,T,P>>();
		}
		
		private final void removeSentTender() {
			LOGGER.trace("[{}]~tendersSent={}", util.getTime(), tendersSent);

			assert(tendersSent != null): "The tendersSent is null, "+tendersSent;
			
			tendersSent = null;
			bidsRecieved = new ArrayList<BidMessage<N,T,P>>();
		}
		
		/*
		 * Tenders Received
		 */
		private final boolean hasRecievedTenders() {
			boolean hasRecievedTenders = tendersRecieved.size() > 0;
			LOGGER.trace("[{}] -> {}", util.getTime(), hasRecievedTenders);
			return hasRecievedTenders;
		}
		
		@SuppressWarnings("unused")
		private final int numberOfRecievedTenders() {
			int numberOfRecievedTenders = tendersRecieved.size();
			LOGGER.trace("[{}] -> {}", util.getTime(), numberOfRecievedTenders);
			return numberOfRecievedTenders;
		}

		public final boolean haveRecievedTender(TenderMessage<N,T,P> tenderMessage) {
			boolean haveRecievedTender = tendersRecieved.containsKey(tenderMessage.getContractID());
			LOGGER.trace("[{}] {} -> {}", util.getTime(), tenderMessage, haveRecievedTender);
			return haveRecievedTender;
		}

		private final List<TenderMessage<N,T,P>> getRecievedTenders() {
			return new ArrayList<>(tendersRecieved.values());
		}

		private final void addRecievedTender(TenderMessage<N,T,P> tenderMessage) {
			LOGGER.trace("[{}]~tenderMessage={}", util.getTime(), tenderMessage);
			tendersRecieved.put(tenderMessage.getContractID(), tenderMessage);
		}
		
		private final void removeRecievedTender(TenderMessage<N,T,P> tenderMessage) {
			LOGGER.trace("[{}]~tenderMessage={}", util.getTime(), tenderMessage);
			tendersRecieved.remove(tenderMessage.getContractID());
		}
		
		/*
		 * Bids Sent
		 */

		public boolean hasSentBid() {
			return bidSent != null;
		}

		private final BidMessage<N,T,P> getBidSent() {
			LOGGER.trace("[{}] -> {}", util.getTime(), bidSent);
			return bidSent;
		}

		private final void setBidSent(BidMessage<N,T,P> bidMessage) {
			LOGGER.trace("[{}]~currentBid={}", util.getTime(), bidSent);
			assert(bidSent == null): "The bidSent is not null, "+bidSent;
			bidSent = bidMessage;
		}
		
		private final void clearBidSent() {
			LOGGER.trace("[{}]~bidSent={}", util.getTime(), bidSent);
			assert(bidSent != null): "The bidSent is null!";
			bidSent = null;
		}

		/*
		 * Bids Received
		 */
		private final List<BidMessage<N,T,P>> getBidsRecieved(int contractID) {
			return bidsRecieved;
		}

		/*
		 * Awards Sent
		 */		
		public boolean hasSendAward() {
			return awardsSent != null;
		}
		
		private final void addAwardSent(AwardMessage<N,T,P> awardMessage) {
			LOGGER.trace("[{}]~{}", util.getTime(), awardMessage);
			// Add to archive of the Tender messages awarded
			awardsSent = awardMessage;
		}
		
		private final void removeAwardSent() {
			LOGGER.trace("[{}]~{}", util.getTime(), awardsSent);

			assert(awardsSent != null): "The awardsSent is null, "+awardsSent;
			
			awardsSent = null;
		}

		@SuppressWarnings("unused")
		private final AwardMessage<N,T,P> getSentAward(int contractID) {
			return awardsSent;
		}

		/*
		 * Awards Received
		 */
		private boolean hasReceivedAward() {
			return awardRecieved != null;
		}

		public AwardMessage<N,T,P> getAwardReceived() {
			return awardRecieved;
		}
		
		private final void setRecievedAward(AwardMessage<N,T,P> awardMessage) {
			LOGGER.trace("[{}]~{}", util.getTime(), awardRecieved);
			assert(awardRecieved == null): "The awardRecieved is not null, "+awardRecieved;
			awardRecieved = awardMessage;
		}
		
		private final void clearRecievedAward() {
			assert(awardRecieved != null): "The awardRecieved is null for "+this;
			LOGGER.trace("[{}]~{} contractID={}", 
					util.getTime(), this, awardRecieved.getContractID());
			awardRecieved = null;
		}
	}
	
	/******************************************************************************************
	 * 
	 * 	State APIs
	 * 
	 ******************************************************************************************/
	public class State implements Cloneable {
			
		@SuppressWarnings("unchecked")
		public State clone() {
			State foo = null;
			try {
				foo = (State) super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return foo;
		}
		 
		public final boolean isTendering() {
			boolean isTendering = messages.hasSentTender();
			LOGGER.trace("[{}] -> {}", util.getTime(),isTendering);
			return isTendering;
		}

		public final boolean isBidding() {
			boolean isBidding = (messages.hasSentBid());
			LOGGER.trace("[{}] -> {}", util.getTime(), isBidding);
			return isBidding;
		}

		public final boolean isAwarding() {
			boolean isAwarding = messages.hasSendAward();
			LOGGER.trace("[{}] -> {}", util.getTime(), isAwarding);
			return isAwarding;
		}

		public final boolean isBeingAwarded() {
			boolean isBeingAwarded = (messages.hasReceivedAward());
			LOGGER.trace("[{}] -> {}", util.getTime(), isBeingAwarded);
			return isBeingAwarded;
		}

		public boolean isIdle() {
			boolean isIdle = !isTendering() && !isBidding() && !isAwarding() && !isBeingAwarded();
			LOGGER.trace("[{}] -> {}", util.getTime(), isIdle);
			return isIdle;
		}

		protected boolean isManaging() {
			boolean canTender = isTendering() || isAwarding();
		    LOGGER.trace("[{}]~isTendering()={}, isAwarding={}", 
		    		util.getTime(), isTendering(), isAwarding());
			return canTender;
		}

		protected boolean isContracting() {
			boolean canTender = isBidding() || isBeingAwarded();
		    LOGGER.trace("[{}]~isBidding()={}, isBeingAwarded={}", 
		    		util.getTime(), isBidding(), isBeingAwarded());
			return canTender;
		}
	}
	
	/******************************************************************************************
	 * 
	 * 	Transport APIs
	 * 
	 ******************************************************************************************/
	protected class Transport implements Cloneable {

		@SuppressWarnings("unchecked")
		public Transport clone() {
			Transport foo = null;
			try {
				foo = (Transport)super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return foo;
		}
	
		private final void sendMessage(Message<N,T,P> message) {
			peersim.transport.Transport transport = (peersim.transport.Transport)
					message.getFrom().getNodeAdapter().getProtocol(FastConfig.getTransport(getProtocolID()));
			transport.send(message.getFrom().getNodeAdapter(), 
					message.getTo().getNodeAdapter(), message, getProtocolID());
			LOGGER.debug("[{}]~Sending {}", util.getTime(), message);
		}
		
		private final void sendTenderMessages(TenderMessage<N,T,P> tenderMessage, Stream<GCAgent<N,T,P>> neighbours) {
			neighbours.forEach(neighbour -> {
				Message<N,T,P> clonedMessage = tenderMessage.clone();
				clonedMessage.setTo(neighbour);
				sendMessage(clonedMessage);
			});
		}
		
		private final void resendTenderMessage(TenderMessage<N,T,P> tenderMessage, Stream<GCAgent<N,T,P>> neighbours) {
			neighbours.forEach(neighbour -> {
				if (neighbour != tenderMessage.getFrom()) {
					Message<N,T,P> clonedMessage = tenderMessage.clone();
					clonedMessage.setTo(neighbour);
					clonedMessage.decrumentGossipCounter();
					sendMessage(clonedMessage);
					LOGGER.trace("[{}]~Resending {}", util.getTime(), clonedMessage);
				} 
			});
		}
		
	}
	
	/******************************************************************************************
	 * 
	 * 	Network APIs
	 * 
	 ******************************************************************************************/
	protected class Network implements Cloneable {
	
		// TODO - Should I bring peersim.core.Network.size() API into this interface
		
		@SuppressWarnings("unchecked")
		public Network clone() {
			Network foo = null;
			try {
				foo = (Network)super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return foo;
		}
		
		@SuppressWarnings("unchecked")
		private Stream<GCAgent<N,T,P>> getNeighbours() {
			Linkable neighbourhood = (Linkable)getNodeAdapter().getProtocol(FastConfig.getLinkable(getProtocolID()));
			Stream.Builder<GCAgent<N,T,P>> builder = Stream.builder(); 
			for (int n = 0; n <  neighbourhood.degree(); n++) {
				GCAgent<N,T,P> neigbhour = (GCAgent<N,T,P>)neighbourhood.getNeighbor(n).getProtocol(getProtocolID());
				builder.add(neigbhour);
			}
			return builder.build();
		}
		
		/*
		 private Stream<GCAgent<N,T,P>> getNeighbours(int nodesToFind) {
			Linkable neighbourhood = (Linkable)getNodeAdapter().getProtocol(FastConfig.getLinkable(getProtocolID()));
			ArrayList<GCAgent<N,T,P>> nodes = new ArrayList<GCAgent<N,T,P>>(nodesToFind);
			int neighbourhoodSize = neighbourhood.degree();
			int allowedMisses = neighbourhoodSize*2;
			while (allowedMisses != 0 && nodes.size() < nodesToFind) {
				@SuppressWarnings("unchecked")
				GCAgent<N,T,P> neigbhour = (GCAgent<N,T,P>)neighbourhood.getNeighbor(
						util.randomInt(neighbourhoodSize)).getProtocol(getProtocolID());
				if (!nodes.contains(neigbhour)) {
					nodes.add(neigbhour);
				} else {
					allowedMisses--;
				}
			}
			return nodes.stream();
		}
		*/
	}
	
	/******************************************************************************************
	 * 
	 * 	Scheduler APIs
	 * 
	 ******************************************************************************************/
	protected class Scheduler implements Cloneable {

		@SuppressWarnings("unchecked")
		public Scheduler clone() {
			Scheduler foo = null;
			try {
				foo = (Scheduler)super.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
			return foo;
		}
		
		protected void addCallback(long delay, Callback callback) {
			assert (nodeAdapter != null): "nodeAdapter is null";
			
			LOGGER.trace("[{}]~delay={}, node={}", util.getTime(), delay, getNodeAdapter());
			EDSimulator.add(delay, callback, getNodeAdapter(), getProtocolID());
		}
		
		private final void handleCallback(Callback callback) {
			callback.action.run();
		}
	}
	
	private static final int DEFAULT_TENDER_TIMEOUT = 3;
	private static final int DEFAULT_TENDER_GOSSIP_COUNT = 3;
	private static final int DEFAULT_BID_BUFFER = 1;
	private static final int DEFAULT_AWARD_TIMEOUT = 1;

	protected Util util;
	protected Config config;
	protected Messages messages;
    protected State state;
    protected Transport transport;
    protected Network network;
    protected Scheduler scheduler;
    
	private final int protocolID;
	private NodeAdapter<N> nodeAdapter;
	
	public GCAgent(String prefix) {
		this.util = this.new Util();
		this.config = this.new Config(prefix);
		this.messages = this.new Messages();
		this.state = this.new State();
		this.transport = this.new Transport();
		this.network = this.new Network();
		this.scheduler = this.new Scheduler();
		
		this.protocolID = Configuration.lookupPid(prefix.replaceFirst("^protocol.", ""));

		LOGGER.trace("[{}]~prefix={}", util.getTime(), prefix);

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public GCAgent<N,T,P> clone() {
		GCAgent<N,T,P> foo = null;
		try {
			foo = (GCAgent<N,T,P>)super.clone();
			
			foo.util = foo.new Util();
			foo.config = foo.new Config(config.prefix);
			foo.messages = foo.new Messages();
			foo.state = foo.new State();
			foo.transport = foo.new Transport();
			foo.network = foo.new Network();
			foo.scheduler = foo.new Scheduler();
			
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.trace("[{}]", util.getTime());
		return foo;
	}

	public void shutdown() {
		this.messages.clearMessages();
        LOGGER.debug("[{}] {}", util.getTime(), this);
	}

	/******************************************************************************************
	 * 
	 * 	Customisable
	 * 
	 ******************************************************************************************/
	
	protected void preTender(N node) {}
	
	protected boolean shouldTender(N node) {
		boolean shouldTender = false;
	    LOGGER.trace("[{}] node={}, shouldTender={}", util.getTime(), node, shouldTender);
		return shouldTender;
	}
	
	protected boolean shouldBid(TenderMessage<N,T,P> tenderMessage) {
		boolean shouldBid = getState().isIdle();
        LOGGER.trace("[{}]~contractID={}, node={}, isIdle()={}, shouldBid={}", 
        		util.getTime(), tenderMessage.getContractID(), tenderMessage.getTo(), getState().isIdle(), shouldBid);
		return shouldBid;
	}
	
	protected boolean shouldAward(BidMessage<N,T,P> bestBid) {
		return true;
	}

	protected T getTask(N node) {
		return null;
	}

	protected double getGuide(N node, T task) {
		return 0;
	}

	protected P getProposal(TenderMessage<N,T,P> tenderMessage) {
		return null;
	}
	
	protected double getOffer(TenderMessage<N,T,P> tenderMessage) {
		return 0;
	}
	
	protected double tenderToDouble(TenderMessage<N,T,P> tenderMessage) {
		return -tenderMessage.getGuide();
	}
	
	protected double bidToDouble(BidMessage<N,T,P> bidMessage) {
		return -bidMessage.getOffer();
	}
	
	protected void award(AwardMessage<N,T,P> awardMessage) {
	    LOGGER.debug("[{}]~awardMessage={}", util.getTime(), awardMessage);
	}

	protected void awarded(AwardMessage<N,T,P> awardMessage) {
		LOGGER.debug("[{}]~awarded={}", util.getTime(), awardMessage);
		
	}
	
	protected void postTender(N node) {}
	
	/******************************************************************************************
	 * 
	 * 	Agent Flow
	 * 
	 ******************************************************************************************/
	
	private void tenderCycle(N node) {	
		
		LOGGER.trace("[{}]~node={}, id={}, isUp={}", 
				util.getTime(), node, nodeAdapter.getID(), nodeAdapter.isUp());

		preTender(node);
		
		// TODO - Should this really be an 'if' rather than a 'while'? Maybe the 'while' is a 
		// special case of agent? 
		// while (shouldTender(node)) {
		if (getState().isIdle() && shouldTender(node)) {
			LOGGER.debug("[{}]~node={}, shouldTender=true", util.getTime(), node);

			// Send to all the neighbors
			Stream<GCAgent<N,T,P>> neighbourNodes = network.getNeighbours();
			
			// Create and send the tender message
			TenderMessage<N,T,P> tenderMessage = 
					messages.createAndSendTenderMessage(this, neighbourNodes);
			
			// After a delay, review the bids
			scheduler.addCallback(config.getTenderTimeout(), new Callback(() -> {
				evaluateBids(tenderMessage);
			}));

			// After the award process call the post tender clean-up. We wait for 
			// the tender to time out, the award to time out and then at least 1 tick.
			int postTenderDelay = config.getTenderTimeout()+config.getAwardTimeout()+1;
			scheduler.addCallback(postTenderDelay, new Callback(() -> {
				postTender(node);
			}));
			
		} else {
			postTender(node);
		}
	}
	
	private final void evaluateTenders(TenderMessage<N,T,P> tenderMessage) {	
		// remove expired tenders....
		removeExpierdTenders();
		
		// If there's any left, then we get the top one.
		if (getState().isIdle() && messages.hasRecievedTenders()) {
			TenderMessage<N,T,P> topTender = getTopTender();
			int topTenderTimeout = 
					(int)(topTender.getExpirationTime() - config.getBidBuffer() - util.getTime());
			LOGGER.trace("[{}]~topTender={}, topTenderTimeout={}", 
					util.getTime(), topTender, topTenderTimeout);	
			if (topTenderTimeout <= 0 && shouldBid(topTender)) {
				LOGGER.trace("[{}]~node={}, shouldBid=true", util.getTime(), tenderMessage.getTo());
				bid(topTender);
			}
		}
	}

	private final void removeExpierdTenders() {
		LOGGER.trace("[{}] node={}", util.getTime());
		// We can remove the expired tenders as we go cause that we be modifing the list as we traverse it.
		List<TenderMessage<N,T,P>> tendersToRemove = new ArrayList<TenderMessage<N,T,P>>();
		for (TenderMessage<N,T,P> tender : messages.getRecievedTenders()) {
			if (tender.hasExpired(util.getTime())) {
				tendersToRemove.add(tender);
			}
		}
		if (tendersToRemove.size() > 0) {
			LOGGER.trace("[{}]~tendersToRemove={}", 
					util.getTime(), tendersToRemove);
			tendersToRemove.forEach(tender -> {
				messages.removeRecievedTender(tender);
			});
		}
	}

	private final TenderMessage<N,T,P> getTopTender() {
		
		Comparator<? super TenderMessage<N,T,P>> comparator = 
				Comparator.comparingDouble(tenderMessage -> tenderToDouble(tenderMessage));
		
		// Sort all the tenders again.	
		List<TenderMessage<N,T,P>> tenderList = messages.getRecievedTenders();
		Collections.sort(tenderList, comparator);
		TenderMessage<N,T,P> topTender = tenderList.get(0);
		
		List<TenderMessage<N,T,P>> topTenderList = tenderList.stream().
				filter(tender -> comparator.compare(tender, topTender) == 0).
				collect(Collectors.toList());
		LOGGER.trace("[{}]~Choice of {} top tenders, {}", 
				util.getTime(), topTenderList.size(), topTenderList);
		
		if (topTenderList.size() > 1) {
			TenderMessage<N,T,P> randomTopTender = topTenderList.get(util.randomInt(topTenderList.size()));
			LOGGER.trace("[{}]~Choice of {} top tenders, selecting {} from {}", 
					util.getTime(), topTenderList.size(), randomTopTender, topTenderList);
			return randomTopTender;
		} else {
			LOGGER.trace("[{}]~No choice selecting {}", 
					util.getTime(), topTender);
			return topTender;
		}
	}

	private final void bid(TenderMessage<N,T,P> tenderMessage) {
		
		// Creats and send a bid message
		BidMessage<N,T,P> bid = messages.createAndSendBidMessage(tenderMessage);
	
		// Expire the bid after (hence the +1) the Tender is expired.
		long bidExpirationDelay = (bid.getExpirationTime() + 1 - util.getTime());
		scheduler.addCallback(bidExpirationDelay, new Callback(() -> {
			LOGGER.trace("[{}]~bidSent={}, after delay={}", util.getTime(), messages.getBidSent(), bidExpirationDelay);
			if (messages.hasSentBid()) {
				messages.clearBidSent();
			}
		}));
	}
	
	private final void evaluateBids(TenderMessage<N,T,P> tender) {	
			
		int contractID = tender.getContractID();
		BidMessage<N,T,P> bestBid = getTopBid(tender);
		
		// Should I award?
		if (bestBid != null) {
			if (shouldAward(bestBid)) {
				LOGGER.debug("[{}]~Awarding contractID={} to {}", 
						util.getTime(), contractID, bestBid.getFrom());
				award(bestBid);
			} else {
				LOGGER.debug("[{}]~Not awarding contractID={}", util.getTime(), contractID);
			}
		} else {
			LOGGER.debug("[{}]~No bids for contractID={}", util.getTime(), contractID);
			
		}
	
		// Clean-up
		messages.removeSentTender();
	}
	
	private final BidMessage<N,T,P> getTopBid(TenderMessage<N,T,P> tender) {

		List<BidMessage<N,T,P>> tendersBidsRecieved = messages.getBidsRecieved(tender.getContractID());
		if (tendersBidsRecieved != null && tendersBidsRecieved.size() > 0) {
			
			Comparator<? super BidMessage<N,T,P>> comparator =
					Comparator.comparingDouble(bidMessage -> bidToDouble(bidMessage));
			
			// Get top bid
			Collections.sort(tendersBidsRecieved, comparator);
			final BidMessage<N,T,P> topBid = tendersBidsRecieved.get(0);
			
			// Get all the other equally good
			List<BidMessage<N,T,P>> topBidsList = tendersBidsRecieved.stream().
					filter(bid -> comparator.compare(bid, topBid) == 0).
					collect(Collectors.toList());
			
			// If there's several tops bids so randomly select one.
			if (topBidsList.size() > 1) {
				BidMessage<N,T,P> newTopBid = topBidsList.get(util.randomInt(topBidsList.size()));
				LOGGER.trace("[{}]~Choice of {} top bids, selecting {} from {}", 
						util.getTime(), topBidsList.size(), newTopBid, topBidsList);
				return newTopBid;
				
			} else {
				// Otherwise just take the one already selected, it's the outright best.
				return topBid;
				
			}
			
		} else {
			LOGGER.trace("[{}]~No bids recieved :(", util.getTime());
			return null;
			
		}
		
	}

	private final void award(BidMessage<N,T,P> bidMessage) {
		LOGGER.debug("[{}]~bidMessage={}", util.getTime(), bidMessage);
		
		AwardMessage<N,T,P> awardMessage = messages.createAndSendAwardMessage(bidMessage);
		
		award(awardMessage);
		
		// Expire the award at the timeout, give 1 tick extra to make sure it's expired.
		long awardExpirationTime = (awardMessage.getExpirationTime() + 1) - util.getTime();
		scheduler.addCallback(awardExpirationTime, new Callback(() -> {
			messages.removeAwardSent();
		}));
		
	}

	/******************************************************************************************
	 * 
	 * 	PeerSim Node Utilities
	 * 
	 ******************************************************************************************/
	
	public NodeAdapter<N> getNodeAdapter() {
		return nodeAdapter;
	}
	
	public void setNodeAdapter(NodeAdapter<N> nodeAdapter) {
		LOGGER.trace("[{}]~nodeAdapter={}", util.getTime(), nodeAdapter);
		this.nodeAdapter = nodeAdapter;
	}
	
	public N getNode() {
		return nodeAdapter.getNode();
	}
	
	/******************************************************************************************
	 * 
	 * 	PeerSim Protocol Utilities
	 * 
	 ******************************************************************************************/

	protected int getProtocolID() {
		return protocolID;
	}
	
	@Override
	public final void nextCycle(Node peerSimNode, int protocolID) {
		LOGGER.trace("[{}] node={}, protocolID={}", 
				util.getTime(), peerSimNode, protocolID);
		tenderCycle(nodeAdapter.getNode());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void processEvent(Node peerSimNode, int protocolID, Object event) {
		LOGGER.trace("[{}]~node={}, protocolID={}, event={}", 
				util.getTime(), peerSimNode, protocolID, event);
		if (event instanceof Callback) {
			scheduler.handleCallback(((Callback)event));
			
		} else if (event instanceof TenderMessage) {
			messages.handleTenderMessage((TenderMessage<N,T,P>)event);
		
		} else if (event instanceof BidMessage) {
			messages.handleBidMessage((BidMessage<N,T,P>) event);
					
		} else if (event instanceof AwardMessage) {
			messages.handleAwardMessage((AwardMessage<N,T,P>)event);
		
		}
	}

	public State getState() {
		return state;
	}


}