package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class TenderMessage<N,T,P> extends Message<N,T,P> { //implements Cloneable {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static int nextContractID = 1000;

	protected static int getNextContractID() {
		int contractID = nextContractID++;
		return contractID;
	}
	
	private final int contractID;
	private final T task;
	private final double guide;
	private long gossipCounter;

	public TenderMessage(GCAgent<N,T,P> from,GCAgent<N,T,P> to, long expirationTime, T task) {
		this(from, to, expirationTime, 0, getNextContractID(), task, 0);
	}
	
	public TenderMessage(GCAgent<N,T,P> from, GCAgent<N,T,P> to, long expirationTime, T task, double guide) {
		this(from, to, expirationTime, 0, getNextContractID(), task, guide);
	}
	
	public TenderMessage(GCAgent<N,T,P> from, GCAgent<N,T,P> to, long expirationTime, int gossipCounter, T task, double guide) {
		this(from, to, expirationTime, gossipCounter, getNextContractID(), task, guide);
	}
	
	private TenderMessage(GCAgent<N,T,P> from, GCAgent<N,T,P> to, long expirationTime, int gossipCounter, int contractID, T task, double guide) {
		super(from, to, expirationTime, gossipCounter);
		this.contractID = contractID;
		this.task = task;
		this.guide = guide;
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), this);
	}
	
	public String toString() {
		return "TenderMessage " 
				+ from + " -> " + to + ", "
				+ "expirationTime=" + expirationTime +", "
				+ "contractID=" + contractID + ", "
				+ "task=" + task  +", "
				+ "guide=" + guide  +", "
				+ "gossipCounter=" + gossipCounter;
	}

	public int getContractID() {
		return contractID;
	}

	public T getTask() {
		return task;
	}

	public double getGuide() {
		return guide;
	}

}
