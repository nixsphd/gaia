package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class BidMessage<N, T, P> extends Message<N,T,P> {

	private static final Logger LOGGER = LogManager.getLogger();	
		
	private final int contractID;
	private final T task;
	private final P proposal;
	private final double offer;

	public BidMessage(TenderMessage<N,T,P> tenderMessage, double offer) {
		this(tenderMessage, tenderMessage.getExpirationTime(), null, offer);
	}
	
	public BidMessage(TenderMessage<N,T,P> tenderMessage, P proposal, double offer) {
		this(tenderMessage, tenderMessage.getExpirationTime(), proposal, offer);
	}
	
	public BidMessage(TenderMessage<N,T,P> tenderMessage, long expirationTime, P proposal, double offer) {
		super(tenderMessage.to, tenderMessage.from, expirationTime, 0);
		this.contractID = tenderMessage.getContractID();
		this.task = tenderMessage.getTask();
		this.proposal = proposal;
		this.offer = offer;
		LOGGER.trace("[{}]~{}", CommonState.getTime(), this);
	}
	
	@Override
	public String toString() {
		return "BidMessage " 
				+ from + " -> " + to + ", "
				+ "expirationTime=" + expirationTime +", "
				+ "contractID=" + contractID + ", "
				+ "task=" + task +", "
				+ "proposal=" + proposal +", "
				+ "offer=" + offer;
	}

	public int getContractID() {
		return contractID;
	}
	
	public T getTask() {
		return task;
	}

	public P getProposal() {
		return proposal;
	}
	
	public double getOffer() {
		return offer;
	}
	
}