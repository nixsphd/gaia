package ie.nix.gc;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;

public class GCAgentObserver extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();

	private static final String PROTOCOL = "protocol";
	
	private int protocolID;

	public GCAgentObserver(String prefix) {
		super(prefix);

		this.protocolID = Configuration.getPid(prefix+"."+PROTOCOL);
		LOGGER.info("[{}]~protocolID={}", CommonState.getTime(), protocolID);
	}
	
	public void init() {
        LOGGER.trace("[{}]", CommonState.getTime());
        writeHeaders(generateCSVString(generateHeaderStream()));
	}
	
	protected Stream<Object> generateHeaderStream() {
		return Stream.of("time", "tenderingHosts", "biddingHosts", "awardingHosts", "beingAwardedHosts");
	}
	
	public boolean observe() {
		LOGGER.trace("[{}]", CommonState.getTime());
		
		List<GCAgent<?, ?, ?>> agents = getAgentsList();

		Set<GCAgent<?, ?, ?>> tenderingAgents = getTenderingHosts(agents);
		Set<GCAgent<?, ?, ?>> biddingAgents = getBiddingHosts(agents);
		Set<GCAgent<?, ?, ?>> awardingAgents = getAwardingHosts(agents);
		Set<GCAgent<?, ?, ?>> beingAwardedAgents = getBeingAwardedHosts(agents);
		
		String rowString = generateCSVString(Stream.of(CommonState.getTime(), 
				tenderingAgents.size(), biddingAgents.size(), 
				awardingAgents.size(), beingAwardedAgents.size()));
		writeRow(rowString);

		if (isLogTime()) {
			String headerString = generateCSVString(generateHeaderStream());
			LOGGER.info("[{}]~{}", CommonState.getTime(), headerString);
			LOGGER.info("[{}]~{}", CommonState.getTime(), rowString);
		}
		return false;
	}
	
	protected Stream<GCAgent<?, ?, ?>> getAgents() {
		Stream.Builder<GCAgent<?, ?, ?>> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(getAgent(n));
		}
		return streamBuilder.build();
	}

	protected List<GCAgent<?, ?, ?>> getAgentsList() {
		return getAgents().collect(Collectors.toList());
	}

	protected GCAgent<?, ?, ?> getAgent(int n) {
		GCAgent<?, ?, ?> agent = ((GCAgent<?, ?, ?>)((NodeAdapter<?>)Network.get(n)).getProtocol(protocolID));
		LOGGER.trace("[{}]~agent={}", CommonState.getTime(), agent);
		return agent;
	}
	
	protected Set<GCAgent<?, ?, ?>> getTenderingHosts(List<GCAgent<?, ?, ?>> agents) {
		return agents.parallelStream().filter(agent -> 
		agent.getState().isTendering()).collect(Collectors.toSet());
	}

	protected Set<GCAgent<?, ?, ?>> getBiddingHosts(List<GCAgent<?, ?, ?>> agents) {
		return agents.parallelStream().filter(agent -> 
		agent.getState().isBidding()).collect(Collectors.toSet());
	}
	
	protected Set<GCAgent<?, ?, ?>> getAwardingHosts(List<GCAgent<?, ?, ?>> agents) {
		return agents.parallelStream().filter(agent -> 
			agent.getState().isAwarding()).collect(Collectors.toSet());
	}
	
	protected Set<GCAgent<?, ?, ?>> getBeingAwardedHosts(List<GCAgent<?, ?, ?>> agents) {
		return agents.parallelStream().filter(agent -> 
			agent.getState().isBeingAwarded()).collect(Collectors.toSet());
	}

}
