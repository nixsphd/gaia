package ie.nix.gc;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.GeneralNode;
import peersim.core.Network;
import peersim.core.Protocol;

/*
 * 	NodeAdaptor 
 */
public abstract class NodeAdapter<N> extends GeneralNode implements Control {

	// TODO - this could be a PeerSim Adaptor, with a NodeAdaptor part 
	// and a ProtocolAdaptor part. 
	
	private static final Logger LOGGER = LogManager.getLogger();

	private static int networkSize = -1;

	public static int getNetworkSize() {
		return networkSize;
	}

	public static void setNetworkSize(int networkSize) {
		NodeAdapter.networkSize = networkSize;
		LOGGER.debug("[0]~networkSize={}", networkSize);
	}
	
	private N node;

	public NodeAdapter(String prefix) {
		super(prefix);
		linkAgents();
	}

	@SuppressWarnings("unchecked")
	@Override
	public NodeAdapter<N> clone() {
		NodeAdapter<N> foo = (NodeAdapter<N>) super.clone();
		foo.linkAgents();
		return foo;
	}

	@Override
	public String toString() {
		return "NodeAdapter[" + "node=" + node + "]";
	}

	@SuppressWarnings("unchecked")
	public void linkAgents() {
		for (Protocol protocol : protocol) {
			if (protocol instanceof GCAgent) {
				GCAgent<N, ?, ?> agent = (GCAgent<N, ?, ?>) protocol;
				agent.setNodeAdapter(this);
			}
		}
	}

	public void setNode(N node) {
		this.node = node;
	}

	public N getNode() {
		return node;
	}

	public boolean execute() {
		if (CommonState.getTime() == 0) {
			resizeNetwork();
			initNodes(getNodeAdapters());
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public void resizeNetwork() {
		int origionalNetworkSize = Network.size();
		LOGGER.trace("[{}]~origionalNetworkSize={}, networkSize={}", 
				CommonState.getTime(), origionalNetworkSize, networkSize);
		if (networkSize > 0 && networkSize != origionalNetworkSize) {
			
			if (origionalNetworkSize > networkSize) {
				for(int i = networkSize; i<origionalNetworkSize; ++i) {
					Network.remove();
				}
			} else {
				for (int n = origionalNetworkSize; n < networkSize; n++) {
					NodeAdapter<N> newNode = (NodeAdapter<N>)Network.prototype.clone();
					Network.add(newNode);
					initNode(newNode);
				}
			}
		}
		LOGGER.debug("[{}]~Network.size()={}", CommonState.getTime(), Network.size());
	}

	@SuppressWarnings("unchecked")
	protected Stream<NodeAdapter<N>> getNodeAdapters() {
		Stream.Builder<NodeAdapter<N>> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add((NodeAdapter<N>) Network.get(n));
		}
		return streamBuilder.build();
	}

	protected void initNodes(Stream<NodeAdapter<N>> nodeAdapters) {
		nodeAdapters.forEachOrdered(nodeAdapter -> {
			if (nodeAdapter.isUp() && nodeAdapter.getNode() == null) {
				initNode(nodeAdapter);
			}
		});
		LOGGER.debug("[{}]",CommonState.getTime());
	}

	public abstract void initNode(NodeAdapter<N> nodeAdapter);
	
}