package ie.nix.gc;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.StringJoiner;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;

public abstract class Observer implements Control {
	
	private static final Logger LOGGER = LogManager.getLogger();

	public static final String LOG_DIR = "log-dir";
	public static final String LOG_FILE = "log-file";
	public static final String STEP = "step";
	public static final String LOG_STEP = "log_step";
	
	public String doubleFormat = "%.2f";
	
	protected String logFile;
	protected int observeStep;
	protected int log_step;
	protected Supplier<Stream<String>> getHeaders;
	protected Supplier<Stream<String>> getRow;

	public Observer(String prefix) {
        Path logDirPath = FileSystems.getDefault().getPath(
        Configuration.getString(prefix + "." + LOG_DIR, Configuration.getString(LOG_DIR, ".")));
        if (!Files.exists(logDirPath)) {
        	try {
				Files.createDirectories(logDirPath);
		        LOGGER.trace("[{}]~Created {}", CommonState.getTime(), logDirPath);
			} catch (IOException e) {
		        LOGGER.error("[{}]~Error creating logfile path {}", CommonState.getTime(), logDirPath, e);
			}
        }
        this.logFile = logDirPath.resolve(Configuration.getString(prefix + "." + LOG_FILE, getName() + ".csv")).toString();
        
        this.observeStep = Configuration.getInt(prefix + "." + STEP);
		this.log_step = Configuration.getInt(prefix + "." + LOG_STEP, this.observeStep);
		
        LOGGER.info("[{}]~Logging to {}", CommonState.getTime(), logFile);
	}
	
	@Override
	public final boolean execute() {
	    LOGGER.trace("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			init();
			return false;
		} else {
			return observe();
		}
	}

	@SuppressWarnings("unchecked")
	protected <N> Stream<N> getNodes() {
		Stream.Builder<N> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(((NodeAdapter<N>)Network.get(n)).getNode());
		}
		return streamBuilder.build();
	}
	
	protected int getNumberOfNodes() {
		return  Network.size();
	}
	
	protected final String getName() {
		return this.getClass().getSimpleName();
	}

	protected void writeRow(Stream<Object> row) {
		String rowString = generateCSVString(row);
		writeRow(rowString);
	}

	protected String generateCSVString(Stream<Object> row) {
		StringJoiner rowString = new StringJoiner(",", "", "");
		row.forEachOrdered(entry -> {
			if (entry instanceof Double) {
				rowString.add(format((Double)entry));
			} else {
				rowString.add(entry.toString());
			}
		});
		return rowString.toString();
	}
	
	protected void truncateFile() {
		try {
			Files.write(Paths.get(logFile), "".getBytes(), 
					StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
        LOGGER.debug("[{}]~Truncated {}", CommonState.getTime(), logFile);
	}

	public void makeLogDir() throws IOException {
		Path parentDir = Paths.get(logFile).getParent();
		if (!Files.exists(parentDir)) {
		    Files.createDirectories(parentDir);
	        LOGGER.info("[{}]~Created {}", CommonState.getTime(), parentDir);
		}
	}
	protected void writeHeaders(String headersString) {
		try {
			Files.write(Paths.get(logFile), (headersString+'\n').getBytes(), 
					StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
        LOGGER.debug("[{}]~{}", CommonState.getTime(), headersString);
		
	}
	
	protected void writeRow(String rowString) {
		try {
			Files.write(Paths.get(logFile), (rowString+'\n').getBytes(), 
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}
        LOGGER.debug("[{}]~{}", CommonState.getTime(), rowString);
		
	}
	
	protected String format(double value) {
		return String.format(doubleFormat, value);
	}

	protected boolean isLogTime() {
		return CommonState.getTime() % log_step == 0;
	}

	protected void init() {
		try {
			makeLogDir();
			truncateFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected abstract boolean observe();
}