package ie.nix.egc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.PeerSimProblem;
import ie.nix.egc.EGCNodes.Guide;
import ie.nix.egc.EGCNodes.Offer;
import ie.nix.gc.BidMessage;
import ie.nix.gc.GCAgent;
import ie.nix.gc.TenderMessage;
import peersim.core.CommonState;

public class EGCAgent<N, T, P> extends GCAgent<N,T,P> {

	private static final long serialVersionUID 	= 1;
	private static final Logger LOGGER 			= LogManager.getLogger();

	public static final int TREE_NOT_SET 		= -1;
	
	public static int shouldTenderTreeIndex		= TREE_NOT_SET;
	public static int shouldBidTreeIndex 		= TREE_NOT_SET;
	public static int shouldAwardTreeIndex 		= TREE_NOT_SET;
	public static int getGuideTreeIndex 		= TREE_NOT_SET;
	public static int getOfferTreeIndex 		= TREE_NOT_SET;
	public static int tenderToDoubleTreeIndex 	= TREE_NOT_SET;
	public static int bidToDoubleTreeIndex 		= TREE_NOT_SET;
	
	public EGCAgent(String prefix) {
		super(prefix);
	}

	@Override
	protected boolean shouldTender(N node) {
		if (evolveShouldTender()) {
			updateNodeNodes(node);
			boolean shouldTender = PeerSimProblem.getGPProblem().evaluateTreeForBoolean(shouldTenderTreeIndex);
			LOGGER.debug("[{}] evolveShouldTender, shouldTender={} ", CommonState.getTime(),  shouldTender);
			return shouldTender;
		} else {
			return super.shouldTender(node);
		}
	}

	@Override
	protected double getGuide(N node, T task) {
		if (evolveGetGuide()) {
			updateNodeNodes(node);
			updateTaskNode(task);
			double getGuide = PeerSimProblem.getGPProblem().evaluateTreeForDouble(getGuideTreeIndex);
			LOGGER.debug("[{}] evolveGetGuide, getGuide={} ", CommonState.getTime(),  getGuide);
			return getGuide;
		} else {
			return super.getGuide(node, task);
		}
	}

	@Override
	protected double tenderToDouble(TenderMessage<N,T,P> tenderMessage) {
		if (evolveTenderToDouble()) {
			updateNodeNodes(tenderMessage.getTo().getNode());
			updateTaskNode(tenderMessage.getTask());
			updateGuideNode(tenderMessage.getGuide());
			double tenderToDouble = PeerSimProblem.getGPProblem().evaluateTreeForDouble(tenderToDoubleTreeIndex);
			LOGGER.debug("[{}] evolveTenderToDouble, tenderToDouble={}", CommonState.getTime(),  tenderToDouble);
			return tenderToDouble;
		} else {
			return super.tenderToDouble(tenderMessage);
		}
	}

	@Override
	protected boolean shouldBid(TenderMessage<N, T, P> tenderMessage) {
		if (evolveShouldBid()) {
			updateNodeNodes(tenderMessage.getTo().getNode());
			updateTaskNode(tenderMessage.getTask());
			updateGuideNode(tenderMessage.getGuide());
			boolean shouldBid = PeerSimProblem.getGPProblem().evaluateTreeForBoolean(shouldBidTreeIndex);
			LOGGER.debug("[{}] evolveShouldBid, shouldBid={} ", CommonState.getTime(),  shouldBid);
			return shouldBid;
		} else {
			return super.shouldBid(tenderMessage);
		}
	}
	
	@Override
	protected double getOffer(TenderMessage<N,T,P> tenderMessage) {
		if (evolveGetOffer()) {
			updateNodeNodes(tenderMessage.getTo().getNode());
			updateTaskNode(tenderMessage.getTask());
			updateGuideNode(tenderMessage.getGuide());
			double getOffer = PeerSimProblem.getGPProblem().evaluateTreeForDouble(getOfferTreeIndex);
			LOGGER.debug("[{}] evolveGetOffer, getOffer={} ", CommonState.getTime(),  getOffer);
			return getOffer;
		} else {
			return super.getOffer(tenderMessage);
		}
	}

	@Override
	protected double bidToDouble(BidMessage<N,T,P> bidMessage) {
		if (evolveBidToDouble()) {
			updateNodeNodes(bidMessage.getTo().getNode());
			updateTaskNode(bidMessage.getTask());
			updateProposalNode(bidMessage.getProposal());
			updateOfferNode(bidMessage.getOffer());
			double bidToDouble = PeerSimProblem.getGPProblem().evaluateTreeForDouble(bidToDoubleTreeIndex);
			LOGGER.debug("[{}] evolveBidToDouble, bidToDouble={}", CommonState.getTime(),  bidToDouble);
			return bidToDouble;
		} else {
			return super.bidToDouble(bidMessage);
		}
	}

	@Override
	protected boolean shouldAward(BidMessage<N, T, P> bidMessage) {
		if (evolveShouldAward()) {
			updateNodeNodes(bidMessage.getTo().getNode());
			updateTaskNode(bidMessage.getTask());
			updateProposalNode(bidMessage.getProposal());
			updateOfferNode(bidMessage.getOffer());
			boolean shouldAward = PeerSimProblem.getGPProblem().evaluateTreeForBoolean(shouldAwardTreeIndex);
			LOGGER.debug("[{}] evolveShouldAward, shouldAward={} ", CommonState.getTime(),  shouldAward);
			return shouldAward;
		} else {
			return super.shouldAward(bidMessage);
		}
	}

	protected boolean evolveBidToDouble() {
		return bidToDoubleTreeIndex != TREE_NOT_SET;
	}
	
	protected boolean evolveShouldTender() {
		return shouldTenderTreeIndex != TREE_NOT_SET;
	}

	protected boolean evolveGetGuide() {
		return getGuideTreeIndex != TREE_NOT_SET;
	}

	protected boolean evolveShouldBid() {
		return shouldBidTreeIndex != TREE_NOT_SET;
	}

	protected boolean evolveTenderToDouble() {
		return tenderToDoubleTreeIndex != TREE_NOT_SET;
	}

	protected boolean evolveGetOffer() {
		return getOfferTreeIndex != TREE_NOT_SET;
	}

	protected boolean evolveShouldAward() {
		return shouldAwardTreeIndex != TREE_NOT_SET;
	}

	protected void updateNodeNodes(N node) {}

	protected void updateTaskNode(T task) {}
	
	protected void updateGuideNode(double guide) {
		Guide.value = guide;
		
	}

	protected void updateProposalNode(P proposal) {}

	protected void updateOfferNode(double offer) {
		Offer.value = offer;
		
	}

}
