package ie.nix.egc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;
import ie.nix.ecj.PeerSimProblem;

public class EGCProblem extends PeerSimProblem implements SimpleProblemForm {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;

	protected static final String P_SHOULD_TENDER_TREE_INDEX 	= "shouldTenderTreeIndex";
	protected static final String P_SHOULD_BID_TREE_INDEX 		= "shouldBidTreeIndex";
	protected static final String P_SHOULD_AWARD_TREE_INDEX 	= "shouldAwardTreeIndex";

	protected static final String P_GET_GUIDE_TREE_INDEX 		= "getGuideTreeIndex";
	protected static final String P_GET_OFFER_TREE_INDEX 		= "getOfferTreeIndex";
	
	protected static final String P_TENDER_TO_DOUBLE_TREE_INDEX = "tenderToDoubleTreeIndex";
	protected static final String P_BID_TO_DOUBLE_TREE_INDEX 	= "bidToDoubleTreeIndex";
	
	protected int shouldTenderTreeIndex;
	protected int shouldBidTreeIndex;
	protected int shouldAwardTreeIndex;
	protected int getGuideTreeIndex;
	protected int getOfferTreeIndex;
	protected int tenderToDoubleTreeIndex;
	protected int bidToDoubleTreeIndex;
	
	@Override
	public String toString() {
		return this.getClass().getCanonicalName();
	}

	public Object clone() {
		PeerSimProblem clone = (PeerSimProblem)(super.clone());
		LOGGER.trace("clone={}", clone);
		return clone;
	}
	
	public void setup(final EvolutionState state, final Parameter base) {
	    // very important, remember this
	    super.setup(state, base);
		
		shouldTenderTreeIndex = state.parameters.getIntWithDefault(
				base.push(P_SHOULD_TENDER_TREE_INDEX), 
				defaultBase().push(P_SHOULD_TENDER_TREE_INDEX), 
				EGCAgent.TREE_NOT_SET);
		shouldBidTreeIndex = state.parameters.getIntWithDefault(
				base.push(P_SHOULD_BID_TREE_INDEX), defaultBase().
				push(P_SHOULD_BID_TREE_INDEX),
				EGCAgent.TREE_NOT_SET);
		shouldAwardTreeIndex = state.parameters.getIntWithDefault(
				base.push(P_SHOULD_AWARD_TREE_INDEX), 
				defaultBase().push(P_SHOULD_AWARD_TREE_INDEX),
				EGCAgent.TREE_NOT_SET);
		getGuideTreeIndex = state.parameters.getIntWithDefault(
				base.push(P_GET_GUIDE_TREE_INDEX), 
				defaultBase().push(P_GET_GUIDE_TREE_INDEX),
				EGCAgent.TREE_NOT_SET);	
		getOfferTreeIndex = state.parameters.getIntWithDefault(
				base.push(P_GET_OFFER_TREE_INDEX), 
				defaultBase().push(P_GET_OFFER_TREE_INDEX),
				EGCAgent.TREE_NOT_SET);	
		tenderToDoubleTreeIndex = state.parameters.getIntWithDefault(
				base.push(P_TENDER_TO_DOUBLE_TREE_INDEX), 
				defaultBase().push(P_TENDER_TO_DOUBLE_TREE_INDEX),
				EGCAgent.TREE_NOT_SET);
		bidToDoubleTreeIndex = state.parameters.getIntWithDefault(
				base.push(P_BID_TO_DOUBLE_TREE_INDEX), 
				defaultBase().push(P_BID_TO_DOUBLE_TREE_INDEX),
				EGCAgent.TREE_NOT_SET);
		
		LOGGER.debug("shouldTenderTreeIndex={}", shouldTenderTreeIndex);
		LOGGER.debug("shouldBidTreeIndex={}", shouldBidTreeIndex);
		LOGGER.debug("shouldAwardTreeIndex={}", shouldAwardTreeIndex);
		LOGGER.debug("getGuideTreeIndex={}", getGuideTreeIndex);
		LOGGER.debug("getOfferTreeIndex={}", getOfferTreeIndex);
		LOGGER.debug("tenderToDoubleTreeIndex={}", tenderToDoubleTreeIndex);
		LOGGER.debug("bidToDoubleTreeIndex={}", bidToDoubleTreeIndex);
	}
	
	public void initSimulation() {
		
		if (shouldTenderTreeIndex != EGCAgent.TREE_NOT_SET) {
			EGCAgent.shouldTenderTreeIndex = shouldTenderTreeIndex;
		}
		if (shouldBidTreeIndex != EGCAgent.TREE_NOT_SET) {
			EGCAgent.shouldBidTreeIndex = shouldBidTreeIndex;
		}	
		if (shouldAwardTreeIndex != EGCAgent.TREE_NOT_SET) {
			EGCAgent.shouldAwardTreeIndex = shouldAwardTreeIndex;
		}	
		if (getGuideTreeIndex != EGCAgent.TREE_NOT_SET) {
			EGCAgent.getGuideTreeIndex = getGuideTreeIndex;
		}
		if (getOfferTreeIndex != EGCAgent.TREE_NOT_SET) {
			EGCAgent.getOfferTreeIndex = getOfferTreeIndex;
		}
		if (tenderToDoubleTreeIndex != EGCAgent.TREE_NOT_SET) {
			EGCAgent.tenderToDoubleTreeIndex = tenderToDoubleTreeIndex;
		}
		if (bidToDoubleTreeIndex != EGCAgent.TREE_NOT_SET) {
			EGCAgent.bidToDoubleTreeIndex = bidToDoubleTreeIndex;
		}
		LOGGER.trace("EGCAgent.shouldTenderTreeIndex={}", EGCAgent.shouldTenderTreeIndex);
		LOGGER.trace("EGCAgent.shouldBidTreeIndex={}", EGCAgent.shouldBidTreeIndex);
		LOGGER.trace("EGCAgent.shouldAwardTreeIndex={}", EGCAgent.shouldAwardTreeIndex);
		LOGGER.trace("EGCAgent.getGuideTreeIndex={}", EGCAgent.getGuideTreeIndex);
		LOGGER.trace("EGCAgent.getOfferTreeIndex={}", EGCAgent.getOfferTreeIndex);
		LOGGER.trace("EGCAgent.tenderToDoubleTreeIndex={}", EGCAgent.tenderToDoubleTreeIndex);
		LOGGER.trace("EGCAgent.bidToDoubleTreeIndex={}", EGCAgent.bidToDoubleTreeIndex);
		
	}

}
