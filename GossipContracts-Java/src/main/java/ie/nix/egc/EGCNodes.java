package ie.nix.egc;

import ie.nix.ecj.gp.Variable;

public class EGCNodes {
	
	public static class Guide extends Variable {

		private static final long serialVersionUID = 1;
		
		public static double value;

		public Guide() {
			super("guide", (results, result) -> result.set(Guide.value));
		}

	}
	
	public static class Offer extends Variable {

		private static final long serialVersionUID = 1;
		
		public static double value;

		public Offer() {
			super("offer", (results, result) -> result.set(Offer.value));
		}

	}
}
