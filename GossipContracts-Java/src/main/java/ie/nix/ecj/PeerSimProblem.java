package ie.nix.ecj;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Individual;
import ec.Subpopulation;
import ec.gp.GPIndividual;
import ec.gp.koza.KozaFitness;
import ec.util.Parameter;
import java.util.DoubleSummaryStatistics;
import ie.nix.ecj.Statistics.KozaFitnessWithStats;
import ie.nix.gc.Message;
import peersim.cdsim.CDSimulator;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.edsim.EDSimulator;

public class PeerSimProblem extends ec.gp.GPProblem {

	public class NullOutputStream extends OutputStream {
		@Override
		public void write(int b) throws IOException {}
	}

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final long serialVersionUID = 1;
	
	public static class ConvergenceTest implements Control {

		private static final long serialVersionUID = 1;
		
		public ConvergenceTest(String prefix) {
			super();    
			LOGGER.trace("[{}]", CommonState.getTime());
		}

		@Override
		public final boolean execute() {
		    LOGGER.trace("[{}]", CommonState.getTime());
			if (CommonState.getTime() == 0) {
				init();
				return false;
			} else {
				return isConverged();
			}
		}
		
		public void init() {}

		public boolean isConverged() {
			return false;
		}
	}
	
	public static class MasterProblem extends ec.eval.MasterProblem {
		
		private static final long serialVersionUID = 1;

		@Override
		public void prepareToEvaluate(EvolutionState state, int threadnum) {
			problem.prepareToEvaluate(state, threadnum);
			super.prepareToEvaluate(state, threadnum);
		}

		@Override
		public void finishEvaluating(EvolutionState state, int threadnum) {
			super.finishEvaluating(state, threadnum);
			problem.finishEvaluating(state, threadnum);
		}

	}
	
	public static final String SILENT 							= "silent";
	public static final String FILE 							= "file";
	public static final String CONSOLE 							= "console";

	public static final String P_NUMBER_OF_RUNS 				= "number_of_runs";
	public static final String P_LOGGING 						= "logging";
	
	public static final String P_PROPERTIES 					= "properties";
	public static final String P_PROTOCOL 						= "protocol";
	public static final String P_PROTOCOL_NAME 					= "protocol_name";

	public static final String P_EVALUATION_WEIGHT 				= "evaluation_weight";
	public static final String P_CONCERGENCE_TIME_WEIGHT 		= "convergence_time_weight";
	public static final String P_MESSAGES_SENT_WEIGHT 			= "messages_sent_weight";

	public static final String EVALUATION_FITNESS_NAME	 		= "evaluation";
	public static final String CONVERGENCE_TIME_FITNESS_NAME 	= "convergence_time";
	public static final String MESSAGES_SENT_FITNESS_NAME 		= "messages_sent";

	protected static boolean propertiesFileLoaded 				= false;

	public static PeerSimProblem gpProblem;

	public static PeerSimProblem getGPProblem() {
		LOGGER.trace("-> {}", gpProblem);
		return gpProblem;
	}

	public static void setGPProblem(PeerSimProblem gpProblem) {
		PeerSimProblem.gpProblem = gpProblem;
		LOGGER.trace("GPProblem.gpProblem={}", gpProblem);
	}
	
	protected String logging;
	protected String propertiesFile;
	protected String protocol;
	protected String protocolName;
	protected int numberOfRuns;
	protected EvolutionState state;
	protected int threadnum;
	protected GPIndividual gpIndividual;	
	protected int run;
	
	// the weight of the evaluation in the fitness
	protected double evaluationWeight;
	// the weight of the convergence speed in the fitness
	protected double convergenceTimeWeight;
	// the weight of the minimise the number of messages in the fitness
	protected double messagesSentWeight;
	
	static double evaluationMax;
	static double convergenceTimeMax;
	static double messagesSentMax;
	
	@Override
	public String toString() {
		return this.getClass().getCanonicalName();
	}

	public Object clone() {
		PeerSimProblem clone = (PeerSimProblem)(super.clone());
		clone.state = state;
		clone.threadnum = threadnum;
		clone.gpIndividual = gpIndividual;
		LOGGER.trace("clone={}", clone);
		return clone;
	}
	
	/*
	 * Set-up
	 */
	public void setup(final EvolutionState state, final Parameter base) {
	    // very important, remember this
	    super.setup(state, base);

		logging = state.parameters.getStringWithDefault(
				base.push(P_LOGGING), defaultBase().push(P_LOGGING), CONSOLE);
		LOGGER.debug("logging={}", logging);
		assert (logging.equals(SILENT) || logging.equals(FILE) || logging.equals(CONSOLE)) : 
			"logging is "+logging+", it needs to be "+SILENT+" or "+FILE+" or "+CONSOLE+".";
		
	    propertiesFile = state.parameters.getStringWithDefault(
	    		base.push(P_PROPERTIES), defaultBase().push(P_PROPERTIES), null);
		LOGGER.debug("propertiesFile={}", propertiesFile);
		assert (propertiesFile != null) : "Specify the properties file in the params file";
        
		protocol = state.parameters.getStringWithDefault
				(base.push(P_PROTOCOL), defaultBase().push(P_PROTOCOL), null);
		LOGGER.debug("protocol={}", protocol);
		assert (protocol != null) : "Specify the protocol file in the params file";
		
		protocolName = state.parameters.getStringWithDefault(
				base.push(P_PROTOCOL_NAME), defaultBase().push(P_PROTOCOL_NAME), null);
		LOGGER.debug("protocolName={}", protocolName);
		assert (protocolName != null) : "Specify the "+P_PROTOCOL_NAME+" file in the params file";

		numberOfRuns = state.parameters.getIntWithDefault(
				base.push(P_NUMBER_OF_RUNS), defaultBase().push(P_NUMBER_OF_RUNS), 0);
		LOGGER.debug("numberOfRuns={}", numberOfRuns);
		assert (numberOfRuns >= 1) : "numberOfRuns is "+numberOfRuns+", it needs to be >= 1.";

		evaluationWeight = state.parameters.getDoubleWithDefault(
				base.push(P_EVALUATION_WEIGHT), defaultBase().push(P_EVALUATION_WEIGHT), 1d);
		LOGGER.debug("evaluationWeight={}", evaluationWeight);
		assert (evaluationWeight >= 0) : "evaluationWeight is "+evaluationWeight+", it needs to be >= 0.";
		
		convergenceTimeWeight = state.parameters.getDoubleWithDefault(
				base.push(P_CONCERGENCE_TIME_WEIGHT), defaultBase().push(P_CONCERGENCE_TIME_WEIGHT), 0d);
		LOGGER.debug("convergenceTimeWeight={}", convergenceTimeWeight);
		assert (convergenceTimeWeight >= 0) : "convergenceTimeWeight is "+convergenceTimeWeight+", it needs to be >= 0.";
		
		messagesSentWeight = state.parameters.getDoubleWithDefault(
				base.push(P_MESSAGES_SENT_WEIGHT), defaultBase().push(P_MESSAGES_SENT_WEIGHT), 0d);
		LOGGER.debug("messagesSentWeight={}", messagesSentWeight);
		assert (messagesSentWeight >= 0) : "messagesSentWeight is "+messagesSentWeight+", it needs to be >= 0.";
		
		bestFitness = null;
		
	}

	/*
	 * Evaluation
	 */
	public void evaluate(final EvolutionState state, final Individual individual, 
                         final int subpopulation, final int threadnum) {

		setupLogging();

		LOGGER.trace("Evaluating individual={}", individual.hashCode());
	    this.state = state;
		this.threadnum = threadnum;
		this.gpIndividual = (GPIndividual)individual;

		PeerSimProblem.setGPProblem(this);
		
		double totalEvaluation = 0;
		long totalConvergenceTime = 0;
		long totalMessagesSent = 0;
		
        if (!gpIndividual.evaluated) {          
        	if (gpIndividual.fitness instanceof KozaFitnessWithStats) {
        		clearStats();
        		Message.resetMessageCount();
            }
        	
            int hits = 0;
            for (run = 0; run < numberOfRuns; run++) {
	        	initSimulation();
	        	
                runSimulation();
                
                double evaluation = evaluateSimulation();
	            if (evaluation == 0) hits++;
            	long convergenceTime = CommonState.getTime(); 
            	long messagesSent = Message.getMessageCount();
                totalEvaluation += evaluation;
                totalConvergenceTime += convergenceTime;
                totalMessagesSent += messagesSent;

            	LOGGER.debug("generation={}, gpIndividual={}, run={}, "
            			+ "evaluation={}, convergenceTime={}, messagesSent={}", 
	    				state.generation, gpIndividual.hashCode(), run, 
	    				evaluation, convergenceTime, messagesSent);
            	
                if (gpIndividual.fitness instanceof KozaFitnessWithStats) {
	            	recordStat(EVALUATION_FITNESS_NAME, evaluation);
	            	recordStat(CONVERGENCE_TIME_FITNESS_NAME, (double)convergenceTime);
	            	recordStat(MESSAGES_SENT_FITNESS_NAME, (double)messagesSent);            	
	            	LOGGER.debug("generation={}, gpIndividual={}, run={}, {}={}, {}={}, {}={}", 
	    				state.generation, gpIndividual.hashCode(), run, 
	    				EVALUATION_FITNESS_NAME, evaluation, 
	    				CONVERGENCE_TIME_FITNESS_NAME, convergenceTime, 
	    				MESSAGES_SENT_FITNESS_NAME, messagesSent);
	            }
            }
            
            if (gpIndividual.fitness instanceof KozaFitnessWithStats) {
            	KozaFitnessWithStats kozaFitnessWithStats = 
            			((KozaFitnessWithStats)gpIndividual.fitness);
            	kozaFitnessWithStats.setStandardizedFitness(state, Double.MAX_VALUE);
 	    		gpIndividual.evaluated = true;
 	    		
            	kozaFitnessWithStats.recordObjectiveFitnesses(
            			EVALUATION_FITNESS_NAME, 
            			totalEvaluation/numberOfRuns);
            	kozaFitnessWithStats.recordObjectiveFitnesses(
            			CONVERGENCE_TIME_FITNESS_NAME, 
            			totalConvergenceTime/numberOfRuns);
            	kozaFitnessWithStats.recordObjectiveFitnesses(
            			MESSAGES_SENT_FITNESS_NAME, 
            			totalMessagesSent/numberOfRuns);
 	    		
            } else if (gpIndividual.fitness instanceof KozaFitness) {
             	KozaFitness kozaFitness = (KozaFitness)gpIndividual.fitness; 
             	kozaFitness.setStandardizedFitness(state, totalEvaluation/numberOfRuns);
 	    		kozaFitness.hits = hits;
 	    		gpIndividual.evaluated = true;
 	    		
 	    		LOGGER.debug("generation={}, gpIndividual={}, hits={}, "
 	    				+ "totalEvaluation={}, adjustedFitness={}", 
 	    				state.generation, gpIndividual.hashCode(), hits, 
 	    				totalEvaluation, 
 	    				String.format("%.03f", kozaFitness.adjustedFitness()));
 	    		updateBestFitness(kozaFitness);
 	    		
            }
        } 
    }
	
	protected void initSimulation() {}

	protected void runSimulation() {
		if (!propertiesFileLoaded) {
			String[] properties = getSimulationProperties();
	        LOGGER.trace("Loading properties={}", Arrays.toString(properties));
	        LOGGER.trace("Running simulation");
	        peersim.Simulator.main(properties);
	        propertiesFileLoaded = true;
	        
	    } else {
	        LOGGER.trace("Not loading properties");
			CommonState.initializeRandom(run);
	        LOGGER.trace("Running simulation");
	    	if( CDSimulator.isConfigurationCycleDriven()){
		    	CDSimulator.nextExperiment();
			}
			else if( EDSimulator.isConfigurationEventDriven() ) {	
		    	EDSimulator.nextExperiment();
			}
	    }
	}

	protected String[] getSimulationProperties() {
		return new String[] { 
				propertiesFile, 
				"protocol." + protocolName + "=" + protocol};
	}

	protected double evaluateSimulation() {
		double result = Double.MAX_VALUE;
		LOGGER.error("DEFAULT evaluateSimulation result={}", result);
		return result;
	}
	
	@Override
	public void finishEvaluating(EvolutionState state, int threadnum) {
		if (state.population.subpops.get(0).individuals.get(0).fitness 
				instanceof KozaFitnessWithStats) { 
			// Used for testing: Set the best fitness back to null 
			resetBestAdjustedFitness();
			
			// If it the first generations set up the max's
	 		if (state.generation == 0) {
	 			setObjectiveFitnessesMaxes(state);
			}
			LOGGER.debug("state.generation={}, evaluationMax={}, "
					+ "convergenceTimeMax={}, messagesSentMax={}", 
					state.generation, evaluationMax, 
					convergenceTimeMax, messagesSentMax);
			
			// Update the fitnesses...
			for (Subpopulation subpop : state.population.subpops) {
				for (int index = 0; index < subpop.individuals.size(); index++) {
					gpIndividual = (GPIndividual)subpop.individuals.get(index);
					setNormalisedFitness(state);
				}
			}
		}
	}

	public void setObjectiveFitnessesMaxes(EvolutionState state) {
		// Stats used to normalise the three objective fitnesses
		DoubleSummaryStatistics evaluationStats = new DoubleSummaryStatistics();
		DoubleSummaryStatistics convergenceTimeStats = new DoubleSummaryStatistics();
		DoubleSummaryStatistics messagesSentStats = new DoubleSummaryStatistics();
		for (Subpopulation subpop : state.population.subpops) {
			for (int index = 0; index < subpop.individuals.size(); index++) {
				Individual individual = subpop.individuals.get(index);
		    	KozaFitnessWithStats kozaFitnessWithStats = 
		    			((KozaFitnessWithStats)individual.fitness);
				evaluationStats.accept(kozaFitnessWithStats.getObjectiveFitnessesMap().
		    		get(EVALUATION_FITNESS_NAME));
		    	convergenceTimeStats.accept(kozaFitnessWithStats.getObjectiveFitnessesMap().
		    		get(CONVERGENCE_TIME_FITNESS_NAME));
		    	messagesSentStats.accept(kozaFitnessWithStats.getObjectiveFitnessesMap().
		    		get(MESSAGES_SENT_FITNESS_NAME));
		    }
		}
		evaluationMax = evaluationStats.getMax();
		convergenceTimeMax = convergenceTimeStats.getMax();
		messagesSentMax = messagesSentStats.getMax();
	}

	public void setNormalisedFitness(EvolutionState state) {
		KozaFitnessWithStats kozaFitnessWithStats = 
				((KozaFitnessWithStats)gpIndividual.fitness);
		double evaluation = kozaFitnessWithStats.getObjectiveFitnessesMap().
				get(EVALUATION_FITNESS_NAME);
		double convergenceTime = kozaFitnessWithStats.getObjectiveFitnessesMap().
				get(CONVERGENCE_TIME_FITNESS_NAME);
		double messagesSent = kozaFitnessWithStats.getObjectiveFitnessesMap().
				get(MESSAGES_SENT_FITNESS_NAME);
		double fitness = (evaluationMax != 0 ? 
				evaluationWeight * (evaluation/evaluationMax) : 0);
		fitness += (convergenceTimeMax != 0 ? 
				convergenceTimeWeight * (convergenceTime/convergenceTimeMax) : 0);
		fitness += (messagesSentMax != 0 ? 
				messagesSentWeight * (messagesSent/messagesSentMax) : 0);
		kozaFitnessWithStats.setStandardizedFitness(state, fitness);
		updateBestFitness(kozaFitnessWithStats);
	    recordStat("standard_fitness", fitness);
		LOGGER.debug("generation={}, gpIndividual={}, "
			+ "evaluation={}, evaluationMax={}, evaluationWeight={}, "
			+ "convergenceTime={}, convergenceTimeMax={}, convergenceTimeWeight={}, "
			+ "messagesSent={}, messagesSentMax={}, messagesSentWeight={}, "
			+ "fitness={}, adjustedFitness={}", 
			state.generation, gpIndividual.hashCode(), 
			evaluation, evaluationMax, evaluationWeight, 
			convergenceTime, convergenceTimeMax, convergenceTimeWeight, 
			messagesSent, messagesSentMax, messagesSentWeight, 
			String.format("%.04f", fitness), 
			String.format("%.04f", kozaFitnessWithStats.adjustedFitness()));
	}

	/*
	 * Evaluate a tree
	 */
	public double evaluateTreeForDouble(int treeNumber) {
		gpIndividual.trees[treeNumber].child.eval(
				state, threadnum, input, stack, gpIndividual, this);
		double result = ((TypedData)input).getDouble();
		LOGGER.trace("-> {}", result);
		return result;
	}
	
	public boolean evaluateTreeForBoolean(int treeNumber) {
		gpIndividual.trees[treeNumber].child.eval(
				state, threadnum, input, stack, gpIndividual, this);
		boolean result = ((TypedData)input).getBoolean();
		LOGGER.trace("-> {}", result);
		return result;
	}

	/*
	 * Stats
	 */
	protected void clearStats() {
		if (gpIndividual.fitness instanceof KozaFitnessWithStats) {
    		KozaFitnessWithStats kozaFitnessWithStats = (KozaFitnessWithStats)gpIndividual.fitness;
    		kozaFitnessWithStats.getStatsMap().clear();
		}
	}
	
	protected void recordStat(String statName, double stat) {
		if (gpIndividual.fitness instanceof KozaFitnessWithStats) {
    		KozaFitnessWithStats kozaFitnessWithStats = (KozaFitnessWithStats)gpIndividual.fitness;
    		kozaFitnessWithStats.recordStat(statName, stat);
		} else {
			LOGGER.error("Failed to add stats {}={}, Fitness is of type {}, but must be of type must be {}.", 
					statName, stat, gpIndividual.fitness.getClass().getName(), KozaFitnessWithStats.class.getName());
		}
	}

	/*
	 * Logging
	 */
	public void setupLogging() {
		if (logging.equals(FILE)) {
			redirectSystemOutErr();
		} else if (logging.equals(SILENT)) {
			nullSystemOutErr();
		}
	}

	private void redirectSystemOutErr() {
		try {
			File errFile = new File("logs/err.txt");
			FileOutputStream errFOS = new FileOutputStream(errFile);
			PrintStream errPS = new PrintStream(errFOS);
			System.setErr(errPS);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			File outFile = new File("logs/out.txt");
			FileOutputStream outFOS = new FileOutputStream(outFile);
			PrintStream outPS = new PrintStream(outFOS);
			System.setOut(outPS);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void nullSystemOutErr() {
		System.setErr(new PrintStream(new NullOutputStream()));
		System.setOut(new PrintStream(new NullOutputStream()));
	}
	
	/*
	 *  Used for testing
	 */
	private KozaFitness bestFitness;
	
	public KozaFitness getBestFitness() {
		return bestFitness;
	}

	public double getBestAdjustedFitness() {
		return bestFitness.adjustedFitness();
	}

	public void resetBestAdjustedFitness() {
		this.bestFitness = null;
	}
	
	public void updateBestFitness(KozaFitness kozaFitness) {
		if (bestFitness == null || 
				bestFitness.adjustedFitness() < kozaFitness.adjustedFitness()) {
			this.bestFitness = kozaFitness;
			LOGGER.debug("bestAdjustedFitness={}", bestFitness.adjustedFitness());
		}
	}

}
