package ie.nix.ecj;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Individual;
import ec.Subpopulation;
import ec.gp.koza.KozaFitness;
import ec.util.Parameter;

public class Statistics extends ec.Statistics {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class KozaFitnessWithStats extends KozaFitness {

		private static final long serialVersionUID = 1;
		
		protected SortedMap<String, DoubleSummaryStatistics> statsMap;
		
		protected SortedMap<String, Double> objectiveFitnessesMap;

		public KozaFitnessWithStats() {
			this.statsMap = new TreeMap<String, DoubleSummaryStatistics>();
			this.objectiveFitnessesMap = new TreeMap<String, Double>();
		}

		@Override
		public KozaFitnessWithStats clone() {
			KozaFitnessWithStats foo = null;
			foo = (KozaFitnessWithStats) super.clone();
			foo.statsMap = 
					new TreeMap<String, DoubleSummaryStatistics>(statsMap);
			foo.objectiveFitnessesMap = 
					new TreeMap<String, Double>(objectiveFitnessesMap);
			return foo;
		}

		public void recordStat(String statName, double stat) {
			if (statsMap != null) {
				if (!statsMap.containsKey(statName)) {
					statsMap.put(statName, new DoubleSummaryStatistics());
				}
				DoubleSummaryStatistics summaryStat = statsMap.get(statName);
				summaryStat.accept(stat);	
				LOGGER.trace("{}={}", statName, stat);
			} else {
				LOGGER.error("Failed to add stats {}={}, "
						+ "Fitness type must be KozaFitnessWithStats", 
						statName, stat);
			}
			
		}
		
		public SortedMap<String, DoubleSummaryStatistics> getStatsMap() {
			return statsMap;
		}
		
		public void recordObjectiveFitnesses(String objectiveName, double objectiveFitness) {
			if (objectiveFitnessesMap != null) {
				objectiveFitnessesMap.put(objectiveName, objectiveFitness);
				LOGGER.trace("{}={}", objectiveName, objectiveFitness);
			} else {
				LOGGER.error("Failed to add objective fitness {}={}, "
						+ "Fitness type must be KozaFitnessWithStats", 
						objectiveName, objectiveFitness);
			}
			
		}
		
		public SortedMap<String, Double> getObjectiveFitnessesMap() {
			return objectiveFitnessesMap;
		}
		
		public void writeFitness(final EvolutionState state, final DataOutput dataOutput) throws IOException {
			super.writeFitness(state, dataOutput);
			writeStats(state, dataOutput);
			writeObjectiveFitnesses(state, dataOutput);
		}

		/** Writes Stats out to DataOutput */
		protected void writeStats(final EvolutionState state, final DataOutput dataOutput) throws IOException {
			if (statsMap == null) {
				dataOutput.writeInt(-1);
			} else {
				int numberOfStats = statsMap.size();
				dataOutput.writeInt(numberOfStats);
				for (Entry<String, DoubleSummaryStatistics> entry: statsMap.entrySet()) {
					dataOutput.writeUTF(entry.getKey());
					dataOutput.writeLong(entry.getValue().getCount());
					dataOutput.writeDouble(entry.getValue().getMin());
					dataOutput.writeDouble(entry.getValue().getMax());
					dataOutput.writeDouble(entry.getValue().getSum());
				}
			}
		}
		
		/** Writes Objective Fitnesses out to DataOutput */
		protected void writeObjectiveFitnesses(EvolutionState state, DataOutput dataOutput) throws IOException {
			if (objectiveFitnessesMap == null) {
				dataOutput.writeInt(-1);
			} else {
				int numberOfObjectiveFitnesses = objectiveFitnessesMap.size();
				dataOutput.writeInt(numberOfObjectiveFitnesses);
				for (Entry<String, Double> entry: objectiveFitnessesMap.entrySet()) {
					dataOutput.writeUTF(entry.getKey());
					dataOutput.writeDouble(entry.getValue());
				}
			}
			
		}
	    
		public void readFitness(final EvolutionState state, final DataInput dataInput) throws IOException {
			super.readFitness(state, dataInput);
			readStats(state, dataInput);
			readObjectiveFitnesses(state, dataInput);
		}
		
		/** Reads Stats in from DataInput. */
		protected void readStats(final EvolutionState state, final DataInput dataInput) throws IOException {
			int numberOfStats = dataInput.readInt();
			if (numberOfStats >= 0) {
				for (int i = 0; i < numberOfStats; i++) {
					String statName = dataInput.readUTF();
					long count = dataInput.readLong();
					double min = dataInput.readDouble();
					double max = dataInput.readDouble(); 
					double sum = dataInput.readDouble();
					statsMap.put(statName, new DoubleSummaryStatistics(count, min, max, sum));
				}
			}
		}
	
		/** Writes Objective Fitnesses out to DataOutput */
		protected void readObjectiveFitnesses(EvolutionState state, DataInput dataInput) throws IOException {
			int numberOfObjectiveFitnesses = dataInput.readInt();
			if (numberOfObjectiveFitnesses >= 0) {
				for (int i = 0; i < numberOfObjectiveFitnesses; i++) {
					String objectiveFitnessesName = dataInput.readUTF();
					double objectiveFitnesses = dataInput.readDouble();
					objectiveFitnessesMap.put(objectiveFitnessesName, objectiveFitnesses);
				}
			}
			
		}

	}

	public static class DoubleSummaryStatistics implements Serializable {

		private static final long serialVersionUID = 1;

		private long count;
		private double sum;
		private double min = Double.POSITIVE_INFINITY;
		private double max = Double.NEGATIVE_INFINITY;

		public DoubleSummaryStatistics() {
			super();
		}

		public DoubleSummaryStatistics(long count, double min, double max, double sum) {
			this.count = count;
			this.min = min;
			this.max = max;
			this.sum = sum;
		}

		public void accept(double value) {
			++count;
			sum += value;
			min = Math.min(min, value);
			max = Math.max(max, value);
		}

		public long getCount() {
			return count;
		}

		public double getMin() {
			return min;
		}

		public double getMax() {
			return max;
		}

		public double getSum() {
			return sum;
		}

		public double getAverage() {
			return sum/count;
		}
	}
	
	public static final String COMMA = ", ";

	// The parameter string and log number of the file for our readable population
	protected static final String P_FILE = "file";
	// the ID for the file to wrote to.
	protected int fileLog;
	// record is the headers are written so we don't wrte them twoce.
	protected boolean wroteHeaders;

	@Override
	public void setup(final EvolutionState state, final Parameter base) {
		super.setup(state, base);

		// set up popFile
		File file = state.parameters.getFile(base.push(P_FILE), null);
		if (file != null) {
			try {
				fileLog = state.output.addLog(file, true);
			} catch (IOException i) {
				state.output.fatal("An IOException occurred while trying to create the log " + fileLog + ":\n" + i);
			}
		}

	}

	@Override
	public void postEvaluationStatistics(EvolutionState state) {
		super.postEvaluationStatistics(state);
		writeStatistics(state);
	}

	public void writeStatistics(EvolutionState state) {
		
		int generation = state.generation;

		for (Subpopulation subpop : state.population.subpops) {
			for (int individualIndex = 0; individualIndex < subpop.individuals.size(); individualIndex++) {

				Individual individual = subpop.individuals.get(individualIndex);

				if (individual.fitness instanceof KozaFitnessWithStats) {
					KozaFitnessWithStats kozaFitnessWithStats = (KozaFitnessWithStats) individual.fitness;
					Map<String, DoubleSummaryStatistics> statsMap = kozaFitnessWithStats.getStatsMap();
					writeHeaders(state, statsMap);
					writeRow(state, generation, individualIndex, 
							kozaFitnessWithStats.fitness(), 
							kozaFitnessWithStats.hits, statsMap);
				}

			}
		}
	}

	protected void writeHeaders(EvolutionState state, Map<String, DoubleSummaryStatistics> individualStats) {
		if (!wroteHeaders) {

			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("generation").append(COMMA);
			stringBuilder.append("individual").append(COMMA);
			stringBuilder.append("fitness").append(COMMA);
			stringBuilder.append("hits");

			for (String statName : individualStats.keySet()) {
				stringBuilder.append(COMMA).append(statName);
			}

			String headers = stringBuilder.toString();
			if (!headers.isEmpty()) {
				state.output.println(headers, fileLog);
			}
			LOGGER.debug(headers);
			wroteHeaders = true;
		}

	}

	public void writeRow(EvolutionState state, int generation, int individualIndex,
			double fitness, int hits, Map<String, DoubleSummaryStatistics> individualStats) {

		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(generation).append(COMMA);
		stringBuilder.append(individualIndex).append(COMMA);
		stringBuilder.append(fitness).append(COMMA);
		stringBuilder.append(hits);

		for (String statName : individualStats.keySet()) {
			DoubleSummaryStatistics summaryStatistics = individualStats.get(statName);
			stringBuilder.append(COMMA).append(summaryStatistics.getAverage());
		}

		String row = stringBuilder.toString();
		state.output.println(row, fileLog);
		LOGGER.debug(row);
	}

}
