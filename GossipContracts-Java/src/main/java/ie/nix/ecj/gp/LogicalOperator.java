package ie.nix.ecj.gp;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.OccamStatistics.OccamStatisticsNode;
import ie.nix.ecj.TypedData;

public abstract class LogicalOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class Not extends LogicalOperator {

		private static final long serialVersionUID = 1;
		
		public Not() {
			super("not", "!", 1,  (childrensResults, result) -> 
			((TypedData)result).set(!
					((TypedData)childrensResults[0]).getBoolean()));
		}	

		@Override
		public boolean isConstant() {
			boolean isConstant = ((OccamStatisticsNode)children[0]).isConstant();
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}

		@Override
		public boolean isRedundant() {
			return ((OccamStatisticsNode)children[0]) instanceof Not;
		}

		@Override
		public OccamStatisticsNode getNextRelevantChild() {
			return ((OccamStatisticsNode)((OccamStatisticsNode)children[0]).childAt(0));
		}
		
	}
	
	public static class And extends LogicalOperator {

		private static final long serialVersionUID = 1;
		
		public And() {
			super("and", "&&", 2,  (childrensResults, result) -> {
				((TypedData)result).set(
						((TypedData)childrensResults[0]).getBoolean() && 
						((TypedData)childrensResults[1]).getBoolean());
                LOGGER.trace("{} && {} -> {}", 
                		((TypedData)childrensResults[0]).getBoolean(),
                		((TypedData)childrensResults[1]).getBoolean(),
                		((TypedData)result).getBoolean());
			});
		}

		@Override
		public boolean isConstant() {
			boolean isConstant = Stream.of(children).
					allMatch(child -> ((OccamStatisticsNode)child).isConstant());
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}
	}
	
	public static class Or extends LogicalOperator {

		private static final long serialVersionUID = 1;
		
		public Or() {
			super("or", "||", 2,  (childrensResults, result) -> {
				((TypedData)result).set(
						((TypedData)childrensResults[0]).getBoolean() || 
						((TypedData)childrensResults[1]).getBoolean());
                LOGGER.trace("{} || {} -> {}", 
                		((TypedData)childrensResults[0]).getBoolean(),
                		((TypedData)childrensResults[1]).getBoolean(),
                		((TypedData)result).getBoolean());
			});
		}

		@Override
		public boolean isConstant() {
			boolean isConstant = Stream.of(children).
					anyMatch(child -> ((OccamStatisticsNode)child).isConstant() && 
						((OccamStatisticsNode)child).evalConstant().getBoolean() == true);
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}
		
		@Override
		public TypedData evalConstant() {
			TypedData result = new TypedData();
			result.set(true);
			LOGGER.trace("result={}", result);
			return result;
		}
		
	}

	protected LogicalOperator(String symbol, String stringForHumans, int numberOfChildren, Eval eval) {
		super(symbol, stringForHumans, numberOfChildren, eval);
	}

}
