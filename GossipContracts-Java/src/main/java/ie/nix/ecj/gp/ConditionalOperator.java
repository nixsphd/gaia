package ie.nix.ecj.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.OccamStatistics.OccamStatisticsNode;
import ie.nix.ecj.TypedData;

public class ConditionalOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class IfDouble extends ConditionalOperator {

		private static final long serialVersionUID = 1;
		
		public IfDouble() {
			super("ifd", (results, result) -> 
			result.set(results[0].getBoolean() ? 
					results[1].getDouble() : 
					results[2].getDouble()));
		}	
	}
	
	public static class IfBoolean extends ConditionalOperator {

		private static final long serialVersionUID = 1;
		
		public IfBoolean() {
			super("ifb",  (results, result) -> 
				result.set(results[0].getBoolean() ? 
						results[1].getBoolean() : 
						results[2].getBoolean()));
		}	
	}
	
	protected ConditionalOperator(String symbol, Eval eval) {
		super(symbol, 3, eval);
	}
	
	protected ConditionalOperator(String symbol, String stringForHumans, Eval eval) {
		super(symbol, stringForHumans, 3, eval);
	}
	
	@Override
	public boolean isRedundant() {
		return ((OccamStatisticsNode)children[0]).isConstant();
	}

	@Override
	public OccamStatisticsNode getNextRelevantChild() {
		TypedData result = ((OccamStatisticsNode)children[0]).evalConstant();
		if (result.getBoolean()) {
			return ((OccamStatisticsNode)children[1]);
			
		} else {
			return ((OccamStatisticsNode)children[2]);
			
		}
	}
	
	@Override
	public String toOccamStringForHumansFunction() {
		String occamString = "(" + 
				((OccamStatisticsNode)children[0]).toOccamStringForHumans() + " ? " + 
				((OccamStatisticsNode)children[1]).toOccamStringForHumans() + " : " + 
				((OccamStatisticsNode)children[2]).toOccamStringForHumans() + ")";
		return occamString;
	}


}
