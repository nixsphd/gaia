package ie.nix.ecj.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.TypedData;

public abstract class Variable extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class A extends Variable {

		private static final long serialVersionUID = 1;
		
		public static boolean value;
		
		public A() { 
			super("a", (results, result) -> ((TypedData)result).set(A.value));
		}

	}

	public static class B extends Variable {

		private static final long serialVersionUID = 1;
		
		public static boolean value;
		
		public B() { 
			super("b", (results, result) -> ((TypedData)result).set(B.value));
		}

	}
	
	public static class C extends Variable {

		private static final long serialVersionUID = 1;
		
		public static boolean value;
		
		public C() { 
			super("c", (results, result) -> ((TypedData)result).set(C.value));
		}

	}

	public static class X extends Variable {

		private static final long serialVersionUID = 1;
		
	    public static double value;
	    
		public X() {
			super("x", (results, result) -> result.set(X.value));
		}	
	}
	
	public static class Y extends Variable {

		private static final long serialVersionUID = 1;
		
	    public static double value;
	    
		public Y() {
			super("y", (results, result) -> result.set(Y.value));
		}	
	}

	public static class Z extends Variable {

		private static final long serialVersionUID = 1;
		
	    public static double value;
	    
		public Z() {
			super("z", (results, result) -> result.set(Z.value));
		}	
	}

	public Variable(String symbol, Eval eval) {
		super(symbol, 0, eval);
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equals = this.getClass().equals(object.getClass());
		LOGGER.trace("equals={}", equals);
		return equals;
	}

	public boolean isConstant() {
		boolean isConstant = false;
		LOGGER.trace("node={}, isConstant={}", 
				this, isConstant);
		return isConstant;
	}
	
}
