package ie.nix.ecj.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.OccamStatistics.OccamStatisticsNode;
import ie.nix.ecj.TypedData;

public class ArithmeticOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class Add extends ArithmeticOperator {

		private static final long serialVersionUID = 1;
		
		public Add() {
			super("+", 2, (results, result) -> 
				result.set(results[0].getDouble() + results[1].getDouble()));
		}
		
		@Override
		public boolean isRedundant() {
			boolean isRedundant = 
					((OccamStatisticsNode)children[0]).isZero() || 
					((OccamStatisticsNode)children[1]).isZero();
			LOGGER.trace("isRedundant={}", isRedundant);
			return isRedundant;
		}
		
		@Override
		public OccamStatisticsNode getNextRelevantChild() {
			if (((OccamStatisticsNode)children[0]).isZero()) {
				return (OccamStatisticsNode)children[1];
			} else {
				return (OccamStatisticsNode)children[0];
			}
		}
		
	}

	public static class Minus extends ArithmeticOperator {

		private static final long serialVersionUID = 1;
		
		public Minus() {
			super("-", 1, (results, result) -> 
				result.set(-results[0].getDouble()));
		}	
	}
	
	public static class Sub extends ArithmeticOperator {

		private static final long serialVersionUID = 1;
		
		public Sub() {
			super("-", 2, (results, result) -> 
				result.set(results[0].getDouble() - results[1].getDouble()));
		}

		@Override
		public boolean isConstant() {
			boolean isConstant = super.isConstant() ||
				((OccamStatisticsNode)children[0]).equals(children[1]);
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}

		@Override
		public TypedData evalConstant() {
			TypedData result = new TypedData();
			if (super.isConstant()) {
				result = super.evalConstant();
			} else {
				result.set(0d);
			}
			LOGGER.trace("result={}", result);
			return result;
		}
		
		@Override
		public boolean isRedundant() {
			return ((OccamStatisticsNode)children[1]).isZero();
		}
		
		@Override
		public OccamStatisticsNode getNextRelevantChild() {
			if (((OccamStatisticsNode)children[1]).isZero()) {
				return ((OccamStatisticsNode)children[0]);
				
			} else {
				return null;
			}
		}
	}
	
	public static class Mul extends ArithmeticOperator {

		private static final long serialVersionUID = 1;
		
		public Mul() {
			super("*", 2, (results, result) -> 
				result.set(results[0].getDouble() * results[1].getDouble()));
		}	
		
		@Override
		public boolean isConstant() {
			boolean isConstant = super.isConstant() ||
					((OccamStatisticsNode)children[0]).isZero() || 
					((OccamStatisticsNode)children[1]).isZero();
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}
		
		@Override
		public TypedData evalConstant() {
			TypedData result = new TypedData();
			if (super.isConstant()) {
				result = super.evalConstant();
			} else {
				result.set(0d);
			}
			LOGGER.trace("result={}", result);
			return result;
		}
		
		@Override
		public boolean isRedundant() {
			return ((OccamStatisticsNode)children[0]).isOne() || 
					((OccamStatisticsNode)children[1]).isOne();
		}

		@Override
		public OccamStatisticsNode getNextRelevantChild() {
			if (((OccamStatisticsNode)children[0]).isOne()) {
				return ((OccamStatisticsNode)children[1]);
				
			} else if (((OccamStatisticsNode)children[1]).isOne()) {
				return ((OccamStatisticsNode)children[0]);
				
			} else {
				return null;
			}
		}
		
	}
	
	public static class Div extends ArithmeticOperator {
		
		private static final long serialVersionUID = 1;
		
		public Div() {
			super("div", 2, (results, result) -> {
				if (results[1].getDouble() == 0) {
					result.set(0);
				} else {
					result.set(results[0].getDouble() / results[1].getDouble());
				}
			});
		}
		
		@Override
		public boolean isConstant() {
			boolean isConstant = super.isConstant() ||
				((OccamStatisticsNode)children[0]).equals(children[1]) ||
				(((OccamStatisticsNode) children[0]).isZero() || 
					((OccamStatisticsNode) children[1]).isZero());
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}
		
		@Override
		public TypedData evalConstant() {
			TypedData result = new TypedData();
			if (((OccamStatisticsNode)children[0]).equals(children[1])) {
				result.set(1d);
			} else if (((OccamStatisticsNode)children[0]).isZero() || 
					   ((OccamStatisticsNode)children[1]).isZero()) {
				result.set(0d);
			} else {
				result = super.evalConstant();
			}
			LOGGER.trace("result={}", result);
			return result;
		}
		
		@Override
		public String toOccamStringForHumansBinaryOperator() {
			return toStringForHumans() +"(" + 
					((OccamStatisticsNode)childAt(0)).toOccamStringForHumans() + ", " + 
					((OccamStatisticsNode)childAt(1)).toOccamStringForHumans() + ")";
		}
		
	}
	
	protected ArithmeticOperator(String symbol, int numberOfChildren, Eval eval) {
		super(symbol, numberOfChildren, eval);
	}
}
