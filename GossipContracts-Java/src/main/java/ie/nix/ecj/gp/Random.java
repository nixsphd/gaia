package ie.nix.ecj.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ie.nix.ecj.TypedData;

public class Random extends Node {

	private static final long serialVersionUID = 1;
	private static final Logger LOGGER = LogManager.getLogger();
	
	public Random() {
		super("rand", 0);
	}

	public void eval(EvolutionState state, int thread, GPData input, ADFStack stack, 
			GPIndividual individual, Problem problem) {
		TypedData result = (TypedData)input;
		result.set(state.random[thread].nextDouble());
		LOGGER.trace("result={}", result);
	}
	
	public boolean isConstant() {
		return false;
	}

	@Override
	public TypedData evalConstant() {
		LOGGER.error("ERROR this should never be called on class {}", 
				getClass().getName());
		throw new RuntimeException("evalConstant should never be called on Random");
	}
	
	@Override
	public String toStringForHumans() {
		String occamString = symbol+"()";
		return occamString;
	}

}