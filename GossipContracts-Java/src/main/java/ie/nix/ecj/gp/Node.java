package ie.nix.ecj.gp;

import java.io.Serializable;
import java.util.function.BiConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import ie.nix.ecj.OccamStatistics.OccamStatisticsNode;
import ie.nix.ecj.TypedData;

public class Node extends GPNode implements OccamStatisticsNode {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public interface Eval extends BiConsumer<TypedData[], TypedData>, Serializable {};
	
	public String symbol;
	public int numberOfChildren;
	protected String stringForHumans;
	public BiConsumer<TypedData[], TypedData> eval;
	
	protected Node(String symbol, int numberOfChildren) {
		this(symbol, symbol, numberOfChildren, null);
	}
	
	protected Node(String symbol, int numberOfChildren, Eval eval) {
		this(symbol, symbol, numberOfChildren, eval);
	}
	
	protected Node(String symbol, String stringForHumans, int numberOfChildren, Eval eval) {
		this.symbol= symbol;
		this.numberOfChildren = numberOfChildren;
		this.stringForHumans = stringForHumans;
		this.eval = eval;
		
		// This is just for unit testing
		children = new GPNode[numberOfChildren];
		LOGGER.trace("symbol={}, stringForHumans={}, numberOfChildren={}, eval={}", 
				symbol, stringForHumans, numberOfChildren, eval);
	}
	
	@Override
	public String toString() { 
		return symbol; 
    }
	
	@Override
	public String toStringForHumans() {
		return stringForHumans;
	}

	@Override
    public int expectedChildren() { 
    	return numberOfChildren; 
    }

	@Override
	public OccamStatisticsNode childAt(int at) {
		if (at < children.length) {
			return (OccamStatisticsNode)children[at];
		} else {
			LOGGER.error("Trying to access a non existant "
					+ "child at {}, there are {} choldren",
					at, children.length);
			return null;
		}
	}
	
	public void eval(EvolutionState state, int thread, GPData input, ADFStack stack, 
			GPIndividual individual, Problem problem) {
		TypedData[] childrensResults = new TypedData[numberOfChildren];
		for (int c = 0; c < numberOfChildren; c++) {
			childrensResults[c] = (TypedData)input.clone();
			children[c].eval(state,thread,childrensResults[c],stack,individual,problem);
		}
		if (eval != null) {
			TypedData result = (TypedData)input;
			eval.accept(childrensResults, result);
			LOGGER.trace("result={}", result);
		} else {
			LOGGER.error("eval not set for {}", this);
		}
		
	}

	/*
	 * Occam Statistics APIs
	 */
	@Override
	public TypedData evalConstant() {
		TypedData result = new TypedData();
		TypedData[] childrensResults = new TypedData[expectedChildren()];
	
		for (int c = 0; c < expectedChildren(); c++) {
			childrensResults[c] = ((OccamStatisticsNode)childAt(c)).evalConstant();
		}
	
		eval.accept(childrensResults, result);
		LOGGER.trace("result={}", result);
		return result;
	}
	
	public boolean equals(Node object) {
		boolean equals = false;
		LOGGER.trace("equals={}", equals);
		return equals;
	}

}
