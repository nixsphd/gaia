package ie.nix.ecj.gp;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.OccamStatistics.OccamStatisticsNode;
import ie.nix.ecj.TypedData;

public class RelationalOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class Equals extends RelationalOperator {

		private static final long serialVersionUID = 1;
		
		public Equals() {
			super("==", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() == results[1].getDouble()));
		}
		
	}
	
	public static class NotEquals extends RelationalOperator {

		private static final long serialVersionUID = 1;
		
		public NotEquals() {
			super("!=", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() != results[1].getDouble()));
		}	
		@Override
		public TypedData evalConstant() {
			TypedData result = new TypedData();
			if (children[0].equals(children[1])) {
				result.set(false);
			} else {
				result = super.evalConstant();
			}
			LOGGER.trace("result={}", result);
			return result;
		}
	}
	
	public static class GreaterThan extends RelationalOperator {

		private static final long serialVersionUID = 1;
		
		public GreaterThan() {
			super("gt", ">", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() > results[1].getDouble()));
		}
		
		@Override
		public boolean isConstant() {
			boolean isConstant = super.isConstant() || 
					(((OccamStatisticsNode)children[0]).isZero() &&
					 ((OccamStatisticsNode)children[1]).isPositive())  || 
					(((OccamStatisticsNode)children[0]).isPositive() &&
					 ((OccamStatisticsNode)children[1]).isZero());
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}
		
		@Override
		public TypedData evalConstant() {
			TypedData result = new TypedData();
			if (children[0].equals(children[1])) {
				result.set(false);
				
			} else if (((OccamStatisticsNode)children[0]).isZero() &&
					   ((OccamStatisticsNode)children[1]).isPositive()) {
				result.set(false);
			
			} else if (((OccamStatisticsNode)children[0]).isPositive() &&
					   ((OccamStatisticsNode)children[1]).isZero()) {
				result.set(true);
				
			} else {
				result = super.evalConstant();
			}
			LOGGER.trace("result={}", result);
			return result;
		}
	}
	
	public static class GreaterThanOrEquals extends RelationalOperator {

		private static final long serialVersionUID = 1;
		
		public GreaterThanOrEquals() {
			super("ge", ">=", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() >= results[1].getDouble()));
		}
		
		@Override
		public boolean isConstant() {
			boolean isConstant = super.isConstant() || 
					(((OccamStatisticsNode)children[0]).isPositive() &&
					 ((OccamStatisticsNode)children[1]).isZero());
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}
		
		@Override
		public TypedData evalConstant() {
			TypedData result = new TypedData();
			if (children[0].equals(children[1])) {
				result.set(false);
				
			} else if (((OccamStatisticsNode)children[0]).isPositive() &&
					   ((OccamStatisticsNode)children[1]).isZero()) {
				result.set(true);
				
			} else {
				result = super.evalConstant();
			}
			LOGGER.trace("result={}", result);
			return result;
		}
	}
	
	public static class LessThan extends RelationalOperator {

		private static final long serialVersionUID = 1;
		
		public LessThan() {
			super("lt", "<", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() < results[1].getDouble()));
		}	
		
		@Override
		public boolean isConstant() {
			boolean isConstant = super.isConstant() || 
					(((OccamStatisticsNode)children[0]).isPositive() &&
					 ((OccamStatisticsNode)children[1]).isZero());
			LOGGER.trace("isConstant={}", isConstant);
			return isConstant;
		}
		
		@Override
		public TypedData evalConstant() {
			TypedData result = new TypedData();
			if (children[0].equals(children[1])) {
				result.set(false);
				
			} else if (((OccamStatisticsNode)children[0]).isPositive() &&
					   ((OccamStatisticsNode)children[1]).isZero()) {
				result.set(false);
			
			} else {
				result = super.evalConstant();
			}
			LOGGER.trace("result={}", result);
			return result;
		}
	}
		
	public static class LessThanOrEquals extends RelationalOperator {

		private static final long serialVersionUID = 1;
		
		public LessThanOrEquals() {
			super("le", "<=", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() <= results[1].getDouble()));
		}	
	}
	
	protected RelationalOperator(String symbol, int numberOfChildren, Eval eval) {
		this(symbol, symbol, numberOfChildren, eval);
	}
	
	protected RelationalOperator(String symbol, String stringForHumans, int numberOfChildren, Eval eval) {
		super(symbol, stringForHumans, numberOfChildren, eval);
	}
	
	@Override
	public boolean isConstant() {
		boolean isConstant = Stream.of(children).
				allMatch(child -> ((OccamStatisticsNode)child).isConstant()) || 
				children[0].equals(children[1]);
		LOGGER.trace("isConstant={}", isConstant);
		return isConstant;
	}

	@Override
	public TypedData evalConstant() {
		TypedData result = new TypedData();
		if (children[0].equals(children[1])) {
			result.set(true);
		} else {
			result = super.evalConstant();
		}
		LOGGER.trace("result={}", result);
		return result;
	}
	
}
