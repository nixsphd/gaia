package ie.nix.ecj.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.OccamStatistics.OccamStatisticsNode;

public class ArithmeticFunctions extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class Abs extends ArithmeticFunctions {
		
		private static final long serialVersionUID = 1;
		
		public Abs() {
			super("abs", 1, (results, result) -> 
				result.set(Math.abs(results[0].getDouble())));
		}
		
		@Override
		public boolean isRedundant() {
			boolean isRedundant = 
					((OccamStatisticsNode)children[0]).isPositive();
			LOGGER.trace("isRedundant={}", isRedundant);
			return isRedundant;
		}
		
		@Override
		public OccamStatisticsNode getNextRelevantChild() {
			return (OccamStatisticsNode)children[0];
		}

		@Override
		public String toOccamStringForHumansUnaryOperator() {
			String occamString = toStringForHumans() + "(" + 
					((OccamStatisticsNode)children[0]).toOccamStringForHumans() + ")";
			return occamString;
		}
		
	}
	
	public static class Min extends ArithmeticFunctions {

		private static final long serialVersionUID = 1;
		
		public Min() {
			super("min", 2, (results, result) -> 
				result.set((results[0].getDouble() < results[1].getDouble())?
							results[0].getDouble() : results[1].getDouble()));
		}	
		
		@Override
		public boolean isRedundant() {
			boolean isRedundant = 
					((OccamStatisticsNode)children[0]).equals(children[1]);
	
			isRedundant = isRedundant ||
					((OccamStatisticsNode)children[0]).isPositive() && 
					((OccamStatisticsNode)children[1]).isZero();
			
			isRedundant = isRedundant ||
					((OccamStatisticsNode)children[1]).isPositive() && 
					((OccamStatisticsNode)children[0]).isZero();
			
			LOGGER.trace("isRedundant={}", isRedundant);
			return isRedundant;
		}
		
		@Override
		public OccamStatisticsNode getNextRelevantChild() {
			if (((OccamStatisticsNode)children[0]).equals(children[1])) {
				return (OccamStatisticsNode)children[0];
				
			} else if (((OccamStatisticsNode)children[0]).isPositive() && 
					   ((OccamStatisticsNode)children[1]).isZero()) {
				return (OccamStatisticsNode)children[1];		
				
			} else if (((OccamStatisticsNode)children[1]).isPositive() && 
					   ((OccamStatisticsNode)children[0]).isZero()) {
				return (OccamStatisticsNode)children[0];
				
			} else {
				return super.getNextRelevantChild();
			}
		}
	}
	
	public static class Max extends ArithmeticFunctions {
		
		private static final long serialVersionUID = 1;
		
		public Max() {
			super("max", 2, (results, result) -> 
				result.set((results[0].getDouble() > results[1].getDouble())?
							results[0].getDouble() : results[1].getDouble()));
		}
		
		@Override
		public boolean isRedundant() {
			boolean isRedundant = 
					((OccamStatisticsNode)children[0]).equals(children[1]);
	
			isRedundant = isRedundant ||
					((OccamStatisticsNode)children[0]).isPositive() && 
					((OccamStatisticsNode)children[1]).isZero();
			
			isRedundant = isRedundant ||
					((OccamStatisticsNode)children[1]).isPositive() && 
					((OccamStatisticsNode)children[0]).isZero();
			
			LOGGER.trace("isRedundant={}", isRedundant);
			return isRedundant;
		}
		
		@Override
		public OccamStatisticsNode getNextRelevantChild() {
			if (((OccamStatisticsNode)children[0]).equals(children[1])) {
				return (OccamStatisticsNode)children[0];
				
			} else if (((OccamStatisticsNode)children[0]).isPositive() && 
					   ((OccamStatisticsNode)children[1]).isZero()) {
				return (OccamStatisticsNode)children[0];		
				
			} else if (((OccamStatisticsNode)children[1]).isPositive() && 
					   ((OccamStatisticsNode)children[0]).isZero()) {
				return (OccamStatisticsNode)children[1];
				
			} else {
				return super.getNextRelevantChild();
			}
		}
		
	}
	
	protected ArithmeticFunctions(String symbol, int numberOfChildren, Eval eval) {
		super(symbol, numberOfChildren, eval);
	}
	
	public String toOccamStringForHumansBinaryOperator() {
		String occamString = toStringForHumans() + "(" + 
				((OccamStatisticsNode)children[0]).toOccamStringForHumans() + ", " + 
				((OccamStatisticsNode)children[1]).toOccamStringForHumans() + ")";
		return occamString;
	}

}
