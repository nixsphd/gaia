package ie.nix.ecj.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.ecj.OccamStatistics.OccamStatisticsNode;
import ie.nix.ecj.TypedData;

public class Constant extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class True extends Constant {	

		private static final long serialVersionUID = 1;
		
		public True() {
			super("true", Boolean.TRUE);
		}	
	}
	
	public static class False extends Constant {	

		private static final long serialVersionUID = 1;
		
		public False() {
			super("false", Boolean.FALSE);
		}	
	}
	
	public static class Zero extends Constant {	

		private static final long serialVersionUID = 1;
		
		public Zero() {
			super("0.0", 0d);
		}

		@Override
		public boolean isZero() {
			return true;
		}	
		
	}
	
	public static class One extends Constant {	

		private static final long serialVersionUID = 1;
		
		public One() {
			super("1.0", 1d);
		}

		@Override
		public boolean isOne() {
			return true;
		}	
		
	}
	
	public static class Five extends Constant {	

		private static final long serialVersionUID = 1;
		
		public Five() {
			super("5.0", 5d);
		}	
	}
	
	public static class Ten extends Constant {	

		private static final long serialVersionUID = 1;
		
		public Ten() {
			super("10.0", 10d);
		}
		
	}
	
	public static class Hundred extends Constant {	

		private static final long serialVersionUID = 1;
		
		public Hundred() {
			super("100.0", 100d);
		}
		
	}
	
	public static class Thousand extends Constant {	

		private static final long serialVersionUID = 1;
		
		public Thousand() {
			super("1000.0", 1000d);
		}
		
	}
	
	public static class PI extends Constant {	

		private static final long serialVersionUID = 1;
		
		public PI() {
			super("PI", Math.PI);
		}	
	}
	
	public static class E extends Constant {	

		private static final long serialVersionUID = 1;
		
		public E() {
			super("E", Math.E);
		}	
	}
	
	public static class Phi extends Constant {	

		private static final long serialVersionUID = 1;
		
		public Phi() {
			super("PHI", (1 + Math.sqrt(5)/2));
		}	
	}

	public Constant(double doubeValue) {
		super(String.format("%.1f",doubeValue), 0, (results, result) -> {
				result.set(doubeValue);
				LOGGER.trace("result.doubeValue={}", result.getDouble());
			});
	}
	
	public Constant(String symbol, double doubeValue) {
		super(symbol, 0, (results, result) -> {
				result.set(doubeValue);
				LOGGER.trace("result.doubeValue={}", result.getDouble());
			});
	}

	public Constant(Boolean booleanValue) {
		super(String.valueOf(booleanValue), 0, (results, result) -> {
			((TypedData)result).set(booleanValue);
			LOGGER.trace("result.booleanValue={}", ((TypedData)result).getBoolean());
		});
	}
	
	public Constant(String symbol, Boolean booleanValue) {
		super(symbol, 0, (results, result) -> {
			((TypedData)result).set(booleanValue);
			LOGGER.trace("result.booleanValue={}", ((TypedData)result).getBoolean());
		});
	}
	
	public boolean isConstant() {
		boolean isConstant = true;
		LOGGER.trace("node={}, isConstant={}", 
				this, isConstant);
		return isConstant;
	}
	
	@Override
	public boolean equals(Object object) {
		boolean equals = object instanceof OccamStatisticsNode &&
				((OccamStatisticsNode)object).isConstant() && 
				evalConstant().equals(
						((OccamStatisticsNode)object).evalConstant());
		LOGGER.trace("equals={}", equals);
		return equals;
	}
	
}
