package ie.nix.ecj;

import ec.gp.GPData;

public class TypedData extends GPData {
	
	private static final long serialVersionUID = 1;

	protected enum Type {
		Double,
		Boolean
	}
	
	protected Type type;

	private double doubleData;
	private boolean booleanData;
	
	public boolean equals(TypedData typedData) {
		switch (type) {
			case Double: 
				return doubleData == typedData.getDouble();
			case Boolean: 
				return booleanData == typedData.getBoolean();
			default: 
				return false;
		}
	}

	@Override
	public String toString() {
		switch (type) {
			case Double: 
				return "Data["+doubleData+"]";
			case Boolean: 
				return "Data["+booleanData+"]";
			default: 
				return "Data[unknown-type]";
		}
	}

	@Override
    public void copyTo(final GPData gpd) { 
		((TypedData)gpd).doubleData = doubleData; 
    	((TypedData)gpd).booleanData = booleanData; 
    	((TypedData)gpd).type = type;
    }

	public String toStringForHumans() {
		switch (type) {
		case Double: 
			return String.valueOf(doubleData);
		case Boolean: 
			return String.valueOf(booleanData);
		default: 
			return "ERROR" + toString();
	}
	}
	
	public double getDouble() {
		assert(type == Type.Double) : this+" trying to get a double from a "+type+" type.";
		return doubleData;
	}
	
	public boolean getBoolean() {
		assert(type == Type.Boolean) : this+" trying to get a boolean from a "+type+" type.";
		return booleanData;
	}
	
	public void set(double doubleData) {
		this.doubleData = doubleData;
		type = Type.Double;
	}
	
	public void set(boolean booleanData) {
		this.booleanData = booleanData;
		type = Type.Boolean;
	}
	    
}
