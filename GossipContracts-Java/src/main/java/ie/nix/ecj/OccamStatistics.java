package ie.nix.ecj;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.ge.GEIndividual;
import ec.gp.ge.GESpecies;
import ec.simple.SimpleStatistics;
import ec.util.Parameter;

public class OccamStatistics extends SimpleStatistics {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public interface OccamStatisticsNode {
		
		public int expectedChildren();
		
		public TypedData evalConstant();

		public String toStringForHumans();
		
		public OccamStatisticsNode childAt(int at);
		
		public default boolean isRedundant() {
			return false;
		}
		
		public default OccamStatisticsNode getNextRelevantChild() {
			return this;
		}
		
		public default boolean isConstant() {
			boolean isConstant = true;
			for (int c = 0; c < expectedChildren(); c++) {
				OccamStatisticsNode child = (OccamStatisticsNode)childAt(c);
				isConstant = isConstant && child.isConstant();
			}
			LOGGER.trace("node={}, isConstant={}", this, isConstant);
			return isConstant;
		}
	
		public default boolean isZero() {
			return (isConstant() && evalConstant().getDouble() == 0.0);
		}
	
		public default boolean isOne() {
			return (isConstant() && evalConstant().getDouble() == 1.0);
		}
		
		public default boolean isPositive() {
			return (isConstant() && evalConstant().getDouble() >= 0);
		}
	
		public default String toOccamStringForHumans() {
			
			if (isRedundant()) {
				return getNextRelevantChild().toOccamStringForHumans();
				
			} else if (isConstant()) {
				String result = evalConstant().toStringForHumans();
				return result;
				
			} else {
				
				String occamStringForHumans = new String();
				
				if (expectedChildren() == 0) {
					occamStringForHumans += toStringForHumans();
							
				} else if (expectedChildren() == 1) {
					occamStringForHumans += toOccamStringForHumansUnaryOperator();
					
				} else if (expectedChildren() == 2) {
					occamStringForHumans += toOccamStringForHumansBinaryOperator();
					
				} else {
					occamStringForHumans += toOccamStringForHumansFunction();
				}
				
				LOGGER.trace("occamStringForHumans={}", occamStringForHumans);
				return occamStringForHumans;
				
			}
			
		}
	
		public default String toOccamStringForHumansUnaryOperator() {
			return toStringForHumans() + "(" + 
					((OccamStatisticsNode)childAt(0)).toOccamStringForHumans() + ")";
		}
	
		public default String toOccamStringForHumansBinaryOperator() {
			return "(" + ((OccamStatisticsNode)childAt(0)).toOccamStringForHumans() + 
					" " + toStringForHumans() + " " + 
					((OccamStatisticsNode)childAt(1)).toOccamStringForHumans() + ")";
		}

		public default String toOccamStringForHumansFunction() {
			String occamString = toStringForHumans() + 
					"(" + ((OccamStatisticsNode)childAt(0)).toOccamStringForHumans();
			for (int x = 1; x < expectedChildren(); x++) {
				occamString += ", " + ((OccamStatisticsNode)childAt(x)).toOccamStringForHumans();
			}
			return occamString += ")";
		}
	}

	private static final String P_NUMBER_OF_TREES = "num-trees";
	private static final String P_HEADER = "header";
	private static final String P_FOOTER = "footer";
	
	private int numberOfTrees = 0;
	private String[] headers;
	private String[] footers;
	
	@Override
	public void setup(final EvolutionState state, final Parameter base) {
		super.setup(state, base);
		numberOfTrees = state.parameters.getIntWithDefault(
				base.push(P_NUMBER_OF_TREES), null, 0);
		LOGGER.trace("numberOfTrees={}", numberOfTrees);
		headers = new String[numberOfTrees];
		footers = new String[numberOfTrees];
		
		for (int tree = 0; tree < numberOfTrees; tree++) {
			String treeHeaderParam = "tree."+tree+"."+P_HEADER;
			headers[tree] = state.parameters.getStringWithDefault(
					base.push(treeHeaderParam), null, "");
			LOGGER.trace("headers[{}]={}", tree, headers[tree]);
			
			String treeFooterParam = "tree."+tree+"."+P_FOOTER;
			footers[tree] = state.parameters.getStringWithDefault(
					base.push(treeFooterParam), null, ";");
			LOGGER.trace("footers[{}]={}", tree, footers[tree]);
		}
	}

	@Override
	public void postEvaluationStatistics(EvolutionState state) {
		super.postEvaluationStatistics(state);
		state.output.println("*** Occam Trees ***", statisticslog);
		for(int x = 0; x< state.population.subpops.size(); x++) {
			state.output.println("Subpopulation " + x + ":",statisticslog);
			writeOccamStatistics(state, best_of_run[x]);
		}
	}

	/** Logs the best individual of the run. */
	public void finalStatistics(final EvolutionState state, final int result) {
		super.finalStatistics(state, result);
		state.output.println("*** Occam Trees ***", statisticslog);
		for(int x = 0; x< state.population.subpops.size(); x++) {
			state.output.println("Subpopulation " + x + ":",statisticslog);
			writeOccamStatistics(state, best_of_run[x]);
		}

	}

	protected void writeOccamStatistics(EvolutionState state, Individual bestIndividualOfRun) {
		if (bestIndividualOfRun instanceof GPIndividual) {
			printTrees(state, statisticslog, (GPIndividual)bestIndividualOfRun);
			
		} else if (bestIndividualOfRun instanceof GEIndividual) {
	        GPIndividual bestGPIndividualOfRun = (((GESpecies)bestIndividualOfRun.species).
	        		map(state, (GEIndividual)bestIndividualOfRun, 0, null));
	        
	        if (bestGPIndividualOfRun != null) {
	        	printTrees(state, statisticslog, bestGPIndividualOfRun);
	        	
	        } else {
				state.output.println("Error printing tree, individual is of type "
						+ "GEIndividual but the corresponding GPIndividual could "
						+ "not be retrived.", statisticslog);
				
	        }
		} else {
			state.output.println("Error printing tree, individual is of type "
					+ bestIndividualOfRun.getClass() +" but should be of "
					+ GPIndividual.class, statisticslog);
			
		}
		
	}
	
	protected void printTrees(final EvolutionState state, final int statisticslog, GPIndividual individual) {
		for (int t = 0; t < individual.trees.length; t++) {
			if (headers.length > t) {
				state.output.print(headers[t], statisticslog);
			} else {
				state.output.println("Occam Tree "+t+":", statisticslog);
			}
			if (individual.trees[t].child instanceof OccamStatisticsNode) {
				OccamStatisticsNode node = (OccamStatisticsNode)individual.trees[t].child;
				String occam = node.toOccamStringForHumans();
				LOGGER.trace("occam={}", occam);
				state.output.print(occam, statisticslog);
				if (footers.length > t) {
					state.output.println(footers[t], statisticslog);
				} else {
					state.output.println("", statisticslog);
				}
			} else {
				state.output.println("Error printing tree, "
						+ "root node is of type "
						+ individual.trees[t].child.getClass()
						+" but shoudl be of "+OccamStatisticsNode.class, statisticslog);
			}
		}
	}

}
