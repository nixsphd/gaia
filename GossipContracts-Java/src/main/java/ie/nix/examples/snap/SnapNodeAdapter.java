package ie.nix.examples.snap;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.gc.NodeAdapter;
import peersim.core.CommonState;

public class SnapNodeAdapter extends NodeAdapter<Snap> {
		
	private static final Logger LOGGER = LogManager.getLogger();
	
	public SnapNodeAdapter(String prefix) {
		super(prefix);
		LOGGER.debug("[{}] prefix={}, index={}", 
			CommonState.getTime(), prefix, getIndex());
	}

	@Override
	protected void initNodes(Stream<NodeAdapter<Snap>> nodeAdapters) {
		
		List<Integer> cards = IntStream.range(0, peersim.core.Network.size()).
			    boxed().collect(Collectors.toList());
		Collections.shuffle(cards);
		
		nodeAdapters.forEachOrdered(nodeAdapter -> {
			int myCard = nodeAdapter.getIndex();
			int swapableCard = cards.get(nodeAdapter.getIndex());
			Snap snap = new Snap(myCard,swapableCard);
			nodeAdapter.setNode(snap);
			LOGGER.debug("[{}] nodeAdapter={}, index={}, snap={}", 
					CommonState.getTime(), nodeAdapter, getIndex(), snap);
		});
	}

	@Override
	public void initNode(NodeAdapter<Snap> nodeAdapter) {}
}
