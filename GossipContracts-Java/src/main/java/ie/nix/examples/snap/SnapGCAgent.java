package ie.nix.examples.snap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.gc.GCAgent;
import ie.nix.gc.AwardMessage;
import ie.nix.gc.BidMessage;
import ie.nix.gc.TenderMessage;

// TODO - Can I get rid of the 'peersim.core.CommonState' import. 
// It's used in NodeAdapter. I probably need to have a global 
// scheduler object.

public class SnapGCAgent extends GCAgent<Snap, Integer, Integer> {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public SnapGCAgent(String prefix) {
		super(prefix);
	}

	@Override
	protected boolean shouldTender(Snap snap) {
		boolean shouldTender = !snap.snapped() && getState().isIdle();
		LOGGER.debug(
				"[{}]~node={}, shouldTender={}, snap.snapped()={}, " + "snapAgent.isIdle()={}",
				util.getTime(), snap, shouldTender, snap.snapped(), getState().isIdle());
		return shouldTender;
	}
	
	@Override
	protected Integer getTask(Snap snap) {
		Integer task = snap.getSwapableCard();
		LOGGER.debug("[{}]~node={}, task={}",
				util.getTime(), snap, task);
		return task;
	}

	@Override
	protected double tenderToDouble(TenderMessage<Snap, Integer, Integer> tenderMessage) {
		int myCard = tenderMessage.getTo().getNode().getMyCard();
		int card = tenderMessage.getTask();
		return Math.abs(card - myCard);
	}

	@Override
	protected boolean shouldBid(TenderMessage<Snap, Integer, Integer> tenderMessage) {
		Snap snap = tenderMessage.getTo().getNode();
		boolean shouldBid = !snap.snapped() && getState().isIdle();
		LOGGER.debug("[{}]~node={}, should={}, snap.snapped()={}, " + "snapAgent.isIdle()={}",
				util.getTime(), snap, shouldBid, snap.snapped(), getState().isIdle());
		return shouldBid;
	}

	@Override
	protected Integer getProposal(TenderMessage<Snap, Integer, Integer> tenderMessage) {
		Snap snap = tenderMessage.getTo().getNode();
		Integer proposal = snap.getSwapableCard();
		LOGGER.debug("[{}]~node={}, proposal={}", util.getTime(), snap, proposal);
		return proposal;
	}

	@Override
	protected double bidToDouble(BidMessage<Snap, Integer, Integer> bidMessage) {
		int myCard = bidMessage.getTo().getNode().getMyCard();
		int card = bidMessage.getTask();
		return Math.abs(card - myCard);
	}

	@Override
	protected boolean shouldAward(BidMessage<Snap, Integer, Integer> bidMessage) {
		boolean shouldAward = true;
		return shouldAward;
	}

	@Override
	protected void award(AwardMessage<Snap, Integer, Integer> awardMessage) {
		Snap snap = awardMessage.getFrom().getNode();

		// Part 1 of the swap
		snap.swapCard((int) awardMessage.getProposal());
		// TODO - go into an inactive state
		LOGGER.debug("[{}]~snap={}", util.getTime(), snap);
	}

	@Override
	protected void awarded(AwardMessage<Snap, Integer, Integer> awardMessage) {
		Snap snap = awardMessage.getTo().getNode();

		// Part 2 of the swap
		snap.swapCard((int) awardMessage.getTask());
		// TODO - go into an inactive state
		LOGGER.debug("[{}]~snap={}", util.getTime(), snap);
	}

}