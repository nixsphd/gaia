package ie.nix.examples.snap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Snap {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	private int myCard;
	private int swapableCard;

	public Snap(int myCard, int swapableCard) {
		this.myCard = myCard;
		this.swapableCard = swapableCard;
		LOGGER.trace("myCard={}, swapableCard={}", myCard, swapableCard);
	}

	public int getMyCard() {
		return myCard;
	}

	public int getSwapableCard() {
		return swapableCard;
	}
	
	public void swapCard(int swapableCard) {
		this.swapableCard = swapableCard;
	}
	
	public boolean snapped() {
		return (myCard == swapableCard);
	}
	
	public String toString() {
		return "Snap["+myCard+", "+swapableCard+"]";
	}
	
}