package ie.nix.examples.snap;

import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class SnapObserver extends ie.nix.gc.Observer {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public SnapObserver(String prefix) {
		super(prefix);
	}

	@Override
	public void init() {
        LOGGER.trace("[{}]", CommonState.getTime());
        String headerString = generateCSVString(generateHeaderStream(getNumberOfNodes()));
        writeHeaders(headerString);
	}
	
	protected Stream<Object> generateHeaderStream(int numberOfNodes) {
		return IntStream.range(0, numberOfNodes).mapToObj(n -> n);
	}
	
	@Override
	public boolean observe() {
		
		Stream<Snap> snapStream = getNodes();
		String rowString = generateCSVString(snapStream.map(snap -> snap.getSwapableCard()));
		writeRow(rowString);

		if (isLogTime()) {
			snapStream = getNodes();
			int snaps = 0;
			int distance = 0;
			for (Snap snap : (Iterable<Snap>) snapStream::iterator) {
				if (snap.snapped()) {
					snaps++;
				}
				distance += Math.abs(snap.getMyCard() - snap.getSwapableCard());
			}

			String headerString = generateCSVString(generateHeaderStream(getNumberOfNodes()));
			LOGGER.info("[{}]~snaps={} out of {}, distance={}", 
					CommonState.getTime(), snaps, peersim.core.Network.size(), distance);
			LOGGER.info("[{}]~       myCard={}", CommonState.getTime(), headerString);
			LOGGER.info("[{}]~swappableCard={}", CommonState.getTime(), rowString);

			return (distance == 0);
		}
		return false;
	}
}