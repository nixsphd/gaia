package ie.nix.examples.snap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.egc.EGCAgent;
import ie.nix.examples.snap.SnapEGCNodes.MyCard;
import ie.nix.examples.snap.SnapEGCNodes.Proposal;
import ie.nix.examples.snap.SnapEGCNodes.SwappableCard;
import ie.nix.examples.snap.SnapEGCNodes.Task;
import ie.nix.gc.AwardMessage;
import ie.nix.gc.BidMessage;
import ie.nix.gc.TenderMessage;

public class SnapEGCAgent extends EGCAgent<Snap, Integer, Integer> {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public SnapEGCAgent(String prefix) {
		super(prefix);
		LOGGER.debug("[{}]~prefix={}",
				util.getTime(), prefix);
	}

	@Override
	public String toString() {
		Snap snap = getNodeAdapter().getNode();
		return "SnapEGCAgent-["+snap.getMyCard()+", "+snap.getSwapableCard()+"]";
	}

	/*
	 * Non evolved methods
	 */
	@Override
	protected Integer getTask(Snap snap) {
		Integer task = snap.getSwapableCard();
		LOGGER.debug("[{}]~node={}, task={}",
				util.getTime(), snap, task);
		return task;
	}
	
	@Override
	protected Integer getProposal(TenderMessage<Snap, Integer, Integer> tenderMessage) {
		Snap snap = tenderMessage.getTo().getNode();
		Integer proposal = snap.getSwapableCard();
		LOGGER.debug("[{}]~node={}, proposal={}", util.getTime(), snap, proposal);
		return proposal;
	}

	@Override
	protected void award(AwardMessage<Snap, Integer, Integer> awardMessage) {
		Snap snap = awardMessage.getFrom().getNode();
	
		// Part 1 of the swap
		snap.swapCard((int) awardMessage.getProposal());
		// TODO - go into an inactive state
		LOGGER.debug("[{}]~snap={}", util.getTime(), snap);
	}

	@Override
	protected void awarded(AwardMessage<Snap, Integer, Integer> awardMessage) {
		Snap snap = awardMessage.getTo().getNode();
	
		// Part 2 of the swap
		snap.swapCard((int) awardMessage.getTask());
		// TODO - go into an inactive state
		LOGGER.debug("[{}]~snap={}", util.getTime(), snap);
	}

	/*
	 * Evolved methods
	 */
	@Override
	protected boolean shouldTender(Snap snap) {
		if (evolveShouldTender()) {
			return super.shouldTender(snap);
		} else {
			boolean shouldTender = !snap.snapped(); // && util.randomDouble() <= 0.8;
			LOGGER.debug("[{}]~snap={}, shouldTender={}, snap.snapped()={}, snapAgent.isIdle()={}",
					util.getTime(), snap, shouldTender, snap.snapped(), getState().isIdle());
			return shouldTender;
		}
	}
	
	@Override
	protected double getGuide(Snap snap, Integer task) {
		if (evolveGetGuide()) {
			return super.getGuide(snap, task);
		} else {
			return 0;
		}
	}

	@Override
	protected double tenderToDouble(TenderMessage<Snap, Integer, Integer> tenderMessage)  {
		if (evolveTenderToDouble()) {
			return super.tenderToDouble(tenderMessage);
		} else {
			int myCard = tenderMessage.getTo().getNode().getMyCard();
			int card = tenderMessage.getTask();
			return Math.abs(card - myCard);
		}
	}
	
	@Override
	protected boolean shouldBid(TenderMessage<Snap, Integer, Integer> tenderMessage) {
		if (evolveShouldBid()) {
			return super.shouldBid(tenderMessage);
		} else {
			Snap snap = tenderMessage.getTo().getNode();
			boolean shouldBid = !snap.snapped();
			LOGGER.debug("[{}]~node={}, should={}, snap.snapped()={}, " + "snapAgent.isIdle()={}",
					util.getTime(), snap, shouldBid, snap.snapped(), getState().isIdle());
			return shouldBid;
		}
	}

	@Override
	protected double getOffer(TenderMessage<Snap, Integer, Integer> tenderMessage)  {
		if (evolveGetOffer()) {
			return super.getOffer(tenderMessage);
		} else {
			return 0;
		}
	}
	
	@Override
	protected double bidToDouble(BidMessage<Snap, Integer, Integer> bidMessage)  {
		if (evolveBidToDouble()) {
			return super.bidToDouble(bidMessage);
		} else {
			int myCard = bidMessage.getTo().getNode().getMyCard();
			int card = bidMessage.getTask();
			return Math.abs(card - myCard);
		}
	}

	@Override
	protected boolean shouldAward(BidMessage<Snap, Integer, Integer> bidMessage) {
		if (evolveShouldAward()) {
			return super.shouldAward(bidMessage);
		} else {
			boolean shouldAward = true;
			return shouldAward;
		}
	}

	@Override
	protected void updateNodeNodes(Snap snap) {
		MyCard.value = snap.getMyCard();
		SwappableCard.value = snap.getSwapableCard();
	}

	@Override
	protected void updateTaskNode(Integer task) {
		Task.value = task;
		
	}

	@Override
	protected void updateProposalNode(Integer proposal) {
		Proposal.value = proposal;
		
	}
	
}
