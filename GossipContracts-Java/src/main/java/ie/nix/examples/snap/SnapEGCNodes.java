package ie.nix.examples.snap;

import ie.nix.ecj.gp.Variable;

public class SnapEGCNodes {

	@SuppressWarnings("serial")
	public static class MyCard extends Variable {

		public static double value;

		public MyCard() {
			super("myCard", (results, result) -> result.set(MyCard.value));
		}

		@Override
		public boolean isPositive() {
			return true;
		}
		
	}
	
	@SuppressWarnings("serial")
	public static class SwappableCard extends Variable {

		public static double value;

		public SwappableCard() {
			super("swappableCard", (results, result) -> result.set(SwappableCard.value));
		}
		
		@Override
		public boolean isPositive() {
			return true;
		}

	}
	
	@SuppressWarnings("serial")
	public static class Task extends Variable {

		public static double value;

		public Task() {
			super("task", (results, result) -> result.set(Task.value));
		}

		@Override
		public boolean isPositive() {
			return true;
		}

	}
	
	@SuppressWarnings("serial")
	public static class Proposal extends Variable {

		public static double value;

		public Proposal() {
			super("proposal", (results, result) -> result.set(Proposal.value));
		}

		@Override
		public boolean isPositive() {
			return true;
		}

	}
	
}
