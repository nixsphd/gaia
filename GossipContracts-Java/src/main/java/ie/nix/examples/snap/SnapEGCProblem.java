package ie.nix.examples.snap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.util.Parameter;
import ie.nix.egc.EGCProblem;
import peersim.core.Network;

@SuppressWarnings("serial")
public class SnapEGCProblem extends EGCProblem {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class SnapConvergenceTest extends ConvergenceTest {

		public SnapConvergenceTest(String prefix) {
			super(prefix);
		}

		@Override
		public boolean isConverged() {
			int numberNotSnapped = getNumberNotSnapped();
			boolean isConverged = numberNotSnapped == 0;
			return isConverged;
		}
		
	}
	
	public static int getNumberOfSnaps() {
		int numberOfSnaps = 0;
		for (int n = 0; n < Network.size(); n++) {
			Snap snap = getNode(n);
			if (snap.snapped()) {
				numberOfSnaps++;
			}
		}
		return numberOfSnaps;
	}
	
	public static int getNumberNotSnapped() {
		return Network.size() - getNumberOfSnaps();
	}
	
	public static double getFractionNotSnapped() {
		return 1d * getNumberNotSnapped() / Network.size();
	}
	
	public static int getDistance() {
		int distance = 0;
		for (int n = 0; n < Network.size(); n++) {
			Snap snap = getNode(n);
			distance += Math.abs(snap.getMyCard() - snap.getSwapableCard());
		}
		return distance;
	}

	public double getNormalizedDistance() {
		int maxDistance = Network.size() * Network.size() / 2;
		return 1d * getDistance() / maxDistance;
	}

	public static SnapGCAgent getAgent(int index) {
		return (SnapGCAgent) ((SnapNodeAdapter) Network.get(index)).getProtocol(0);
	}

	public static Snap getNode(int index) {
		return ((SnapNodeAdapter) Network.get(index)).getNode();
	}

	public double evaluateSimulation() {

		int numberOfSnaps = getNumberOfSnaps();
		recordStat("numberOfSnaps", (double)numberOfSnaps);
		
		int distance = getDistance();
		recordStat("distance", (double)distance);
	
		double evaluation = getNumberNotSnapped();
		LOGGER.trace("evaluate with number of snaps, generation={}, gpIndividual={}, "
				+ "evaluation={}, numberOfSnaps={}, distance={}", 
			state.generation, gpIndividual.hashCode(), evaluation, numberOfSnaps, distance);
		
		return evaluation;
		
	}
}
