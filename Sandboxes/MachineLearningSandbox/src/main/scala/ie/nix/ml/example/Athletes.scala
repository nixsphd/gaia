package ie.nix.ml.example

import ie.nix.nearest_neighbour.NearestNeighbour._
import ie.nix.util.CSVFileReader
import org.apache.logging.log4j.scala.Logging

object Athletes extends App with Logging {

//  implicit val getDistance: (Vector[Double], Vector[Double]) => Double = getManhattanDistance
//  implicit val normalise: Vector[Double] => Vector[Double] = rangeNormalise

  val ATHLETES_DATASET_NAME = "college_athletes"
  val ATHLETES_ID_COLUMN = "ID"
  val ATHLETES_TARGET_COLUMN = "Draft"

  val csvFileReader = CSVFileReader()
  val athletes = csvFileReader
    .readData(ATHLETES_DATASET_NAME)
    .dropColumn(ATHLETES_ID_COLUMN)
  logger info s"~athletes=$athletes"

  val newAthlete = Vector(5.5, 8d)
  val newAthlete2 = Vector(-4.5, -2d)
  val similarity = cosineSimilarity(newAthlete, newAthlete2)
  logger info s"~similarity=$similarity"

//  val nearestNeighbourWithManhattanDistance =
//    new NearestNeighbour(athletes, ATHLETES_TARGET_COLUMN)
//  val newAthleteClassification1 = nearestNeighbourWithManhattanDistance.nearestNeighbour(newAthlete)
//
//  val nearestNeighbourWithMinkowskiDistanceOrder2 =
//    new NearestNeighbour(athletes, ATHLETES_TARGET_COLUMN)(getMinkowskiDistanceOrder2, zNormalise)
//  val newAthleteClassification2 = nearestNeighbourWithMinkowskiDistanceOrder2.nearestNeighbour(newAthlete)
//  logger info s"~newAthleteClassification2=$newAthleteClassification2"
//
//  val kNearestNeighbour =
//    new KNearestNeighbour(athletes, ATHLETES_TARGET_COLUMN, k = 5)(getDistance, normalise)
//  val newAthleteClassification3 = kNearestNeighbour.nearestNeighbour(newAthlete)
//  logger info s"~newAthleteClassification3=$newAthleteClassification3"

}
