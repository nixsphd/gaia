package ie.nix.ml.example

import ie.nix.decision_trees.ID3
import ie.nix.util.CSVFileReader
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

object Tennis extends App with Logging {

  val WEATHER_DATASET_NAME = "weather"
  val WEATHER_DATASET_TARGET = "Play?"

  val csvFileReader = CSVFileReader()
  val tennis = csvFileReader.readData(WEATHER_DATASET_NAME)
  logger info s"~tennis=$tennis"

  val id3 = ID3()
  val hypothesis = id3.hypothesis(tennis, WEATHER_DATASET_TARGET, tennis.headers.filter(_ !== WEATHER_DATASET_TARGET))
  logger info s"~hypothesis=$hypothesis"
}
