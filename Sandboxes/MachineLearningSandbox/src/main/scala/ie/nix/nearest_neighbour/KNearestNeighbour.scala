package ie.nix.nearest_neighbour

import ie.nix.nearest_neighbour.NearestNeighbour.{getMean, rangeNormalise}
import ie.nix.util.DataFrame
import ie.nix.util.DataFrame.asDouble
import org.apache.logging.log4j.scala.Logging

import scala.math.pow

class KNearestNeighbour(data: DataFrame, target: String, val k: Int)(
    implicit getDistance: (Vector[Double], Vector[Double]) => Double,
    normalise: Vector[Double] => Vector[Double])
    extends NearestNeighbour(data, target)(getDistance, normalise)
    with Logging {

  override def nearestNeighbourClassification(classify: Vector[Double]): String = {
    val classificationOfSmallestKDistances =
      indexesOfKNNs(classify).map(indexes => data.get(indexes + 1, targetColumn))
    classificationOfSmallestKDistances.groupBy(identity).maxBy(_._2.size)._1
  }

  def nearestNeighbourRegression(classify: Vector[Double]): Double = {
    getMean(indexesOfKNNs(classify).map(index => targetValue(index)))
  }

  def nearestNeighbourWeightedRegression(classify: Vector[Double]): Double = {
    val weightedValue = (for { (distance, index) <- distancesAndIndexesOfKNNs(classify) } yield {
      targetValue(index) / pow(distance, 2)
    }).sum
    val weightNormalization = (for { (distance, index) <- distancesAndIndexesOfKNNs(classify) } yield {
      1d / pow(distance, 2)
    }).sum
    weightedValue / weightNormalization
  }

  private def targetValue(index: Int): Double = {
    data.getAs[Double](index, targetColumn)
  }

  def distancesAndIndexesOfKNNs(classify: Vector[Double]): Vector[(Double, Int)] = {
    val normalizedClassify = rangeNormalise(classify)
    val distances = for { neighbourData <- neighboursData } yield getDistance(neighbourData, normalizedClassify)
    val sortedDistancesWithIndexes = distances.zipWithIndex.sortBy(_._1)
    sortedDistancesWithIndexes.take(k)
  }

  def indexesOfKNNs(classify: Vector[Double]): Vector[Int] = distancesAndIndexesOfKNNs(classify).map(_._2)

}
