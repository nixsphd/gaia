package ie.nix.nearest_neighbour

import ie.nix.util.DataFrame
import ie.nix.util.DataFrame.asDouble
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.math.{pow, sqrt}

object NearestNeighbour {

  implicit val getDistance: (Vector[Double], Vector[Double]) => Double = getEuclideanDistance
  implicit val normalise: Vector[Double] => Vector[Double] = rangeNormalise

  def getEuclideanDistance(a: Vector[Double], b: Vector[Double]): Double = {
    require(a.size === b.size)
    sqrt(a.zip(b).map(ab => pow(ab._1 - ab._2, 2)).sum)
  }

  def getManhattanDistance(a: Vector[Double], b: Vector[Double]): Double = {
    require(a.size === b.size)
    a.zip(b).map(ab => (ab._1 - ab._2).abs).sum
  }

  def getMinkowskiDistanceOrder1(a: Vector[Double], b: Vector[Double]): Double =
    getMinkowskiDistance(1)(a: Vector[Double], b: Vector[Double])

  def getMinkowskiDistance(order: Int)(a: Vector[Double], b: Vector[Double]): Double = {
    require(a.size === b.size)
    pow(a.zip(b).map(ab => pow((ab._1 - ab._2).abs, order.toDouble)).sum, 1d / order.toDouble)
  }

  def getMinkowskiDistanceOrder2(a: Vector[Double], b: Vector[Double]): Double =
    getMinkowskiDistance(2)(a: Vector[Double], b: Vector[Double])

  def rangeNormalise(data: Vector[Double]): Vector[Double] = {
    data.map(attributeValues => (attributeValues - data.min) / (data.max - data.min))
  }

  def minAttributeValues(attributeColumns: Vector[Double]): Double = attributeColumns.min

  def maxAttributeValues(attributeColumns: Vector[Double]): Double = attributeColumns.max

  def zNormalise(data: Vector[Double]): Vector[Double] = {
    val mean = getMean(data)
    val sd = getStandardDeviation(data)
    data.map(attributeValues => (attributeValues - mean) / sd)
  }

  def getMean(data: Vector[Double]): Double = {
    data.sum / data.size
  }

  def getStandardDeviation(data: Vector[Double]): Double = {
    val mean = getMean(data)
    data.map(datum => pow(datum - mean, 2)).sum / data.size
  }

  def cosineSimilarity(a: Vector[Double], b: Vector[Double]): Double = {
    dotProduct(a, b) / (magnitude(a) * magnitude(b))
  }

  def dotProduct(a: Vector[Double], b: Vector[Double]): Double =
    a.zip(b).map(ab => ab._1 * ab._2).sum

  def magnitude(a: Vector[Double]): Double =
    sqrt(a.map(elementOfA => elementOfA * elementOfA).sum)

}

class NearestNeighbour(val data: DataFrame, target: String)(
    implicit val getDistance: (Vector[Double], Vector[Double]) => Double,
    implicit val normalise: Vector[Double] => Vector[Double])
    extends Logging {

  val attributeColumns: Vector[Vector[Double]] = data.subset(attributeColumnHeaders).getColumnsAs[Double]()

  def nearestNeighbourClassification(classify: Vector[Double]): String = {
    val normalizedClassify = normalise(classify)
    val distances = for { neighbourData <- neighboursData } yield getDistance(neighbourData, normalizedClassify)
    val (_, index) = distances.zipWithIndex.minBy(_._1)
    data.get(index + 1, targetColumn)

  }

  def neighboursData: Vector[Vector[Double]] = {
    for { row <- Vector.range(0, numberOfRows) } yield
      Vector.range(0, numberOfColumns).map(column => normalisedAttributeColumns(column)(row))
  }

  def numberOfColumns: Int = normalisedAttributeColumns.size

  def numberOfRows: Int = normalisedAttributeColumns(0).size

  def normalisedAttributeColumns: Vector[Vector[Double]] = attributeColumns.map(normalise)

  def attributeColumnHeaders: Vector[String] = data.headers.filter(_ !== target)

  def targetColumn: Int = data.headers.indexOf(target)

}
