package ie.nix.decision_trees

import ie.nix.util.DataFrame
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.math.log10

object ID3 {

  def apply(): ID3 = {
    new ID3()
  }

}

class ID3 extends Logging {

  def hypothesis(examples: DataFrame, target: String, attributes: Vector[String]): Node = {
    val targetData = getColumnData(examples, target)
    if (targetData.size === 0) {
      logger info "~Default"
      new Node("Default")

    } else if (getCardinality(targetData) === 1) {
      val onlyClass = targetData(0)
      logger trace s"~onlyClass=$onlyClass"
      new Node(onlyClass)

    } else if (attributes.size === 0) {
      val highestCardinalityClass = getHighestCardinalityValue(targetData)
      logger trace s"~highestCardinalityClass=$highestCardinalityClass"
      new Node(highestCardinalityClass)

    } else {

      val bestAttribute =
        attributes.map(attribute => (attribute, informationGain(examples, target, attribute))).maxBy(_._2)._1
      logger trace s"~bestAttribute=$bestAttribute"

      val childNodes
        : Vector[Node] = for { bestAttributeClass <- getClasses(getColumnData(examples, bestAttribute)) } yield {
        val classExamples = examples.filter(bestAttribute, bestAttributeClass)
        logger trace s"~classExamples=$classExamples"
        val classAttributes = attributes.filter(_ !== bestAttribute)
        logger trace s"~classAttributes=$classAttributes"
        Node(bestAttributeClass, Vector(hypothesis(classExamples, target, classAttributes)))
      }
      Node(bestAttribute, childNodes)

    }
  }

  def getCardinality(data: Vector[String]): Int = {
    data.toSet.size
  }

  def getHighestCardinalityValue(values: Vector[String]): String = {
    values.groupBy(identity).maxBy(_._2.size)._1
  }

  def entropy(examples: DataFrame, datasetName: String): Double = {
    val dataSet = getColumnData(examples, datasetName)
    val entropy = (for { dataClass <- getClasses(dataSet) } yield {
      val proportionOfClass = getProportionOfClass(dataClass, dataSet)
      val classEntropy = -proportionOfClass * log2(proportionOfClass)
      logger trace s"~dataClass=$dataClass, proportionOfClass=$proportionOfClass, classEntropy=$classEntropy"
      classEntropy
    }).sum
    logger debug s"~entropy=$entropy"
    entropy
  }

  def getColumnData(examples: DataFrame, columnName: String): Vector[String] = {
    examples.getColumn(columnName)
  }

  def getClasses(data: Vector[String]): Vector[String] = {
    data.toSet.toVector
  }

  def getProportionOfClass(dataClass: String, targetData: Vector[String]): Double = {
    targetData.count(_ === dataClass).toDouble / targetData.size.toDouble
  }

  def log2(x: Double): Double = {
    log10(x) / log10(2.0)
  }

  def informationGain(dataSet: DataFrame, targetName: String, attributeName: String): Double = {
    val cardinalityOfAttribute = dataSet.numberOfRowsOfData
    val totalEntrophy = entropy(dataSet, targetName)

    val informationGain = totalEntrophy - (for { attributeValue <- getClasses(getColumnData(dataSet, attributeName)) } yield {
      val attributeValueExamples = dataSet.filter(attributeName, attributeValue)
      val cardinalityOfAttributeValue = attributeValueExamples.numberOfRowsOfData
      val attributeValueEntrophy = entropy(attributeValueExamples, targetName)
      (cardinalityOfAttributeValue.toDouble / cardinalityOfAttribute.toDouble) * attributeValueEntrophy
    }).sum

    logger debug s"~informationGain=$informationGain"
    informationGain
  }

}

case class Node(label: String, childNodes: Vector[Node]) extends Logging {

  logger trace s"~label=$label, childNodes=$childNodes"

  def this(label: String) = this(label, Vector())

  override def toString: String = {
    if (childNodes.size === 0) {
      s"$label"
    } else {
      s"$label, [${childNodes.mkString(",")}]"
    }
  }

}
