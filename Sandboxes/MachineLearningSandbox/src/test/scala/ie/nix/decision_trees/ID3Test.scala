package ie.nix.decision_trees

import ie.nix.util.DataFrame
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ID3Test extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  override def beforeEach(): Unit = Random.setSeed(0)

  private def playColumnData = id3.getColumnData(examples, "Play?")

  private def id3 = ID3()

  private def examples =
    new DataFrame(
      Vector(
        Vector("Play?", "Windy", "Outlook"),
        Vector("no", "false", "sunny"),
        Vector("no", "true", "sunny"),
        Vector("yes", "false", "overcast"),
        Vector("yes", "false", "rainy"),
        Vector("yes", "false", "rainy"),
        Vector("no", "true", "rainy"),
        Vector("yes", "true", "overcast"),
        Vector("no", "false", "sunny"),
        Vector("yes", "false", "sunny"),
        Vector("yes", "false", "rainy"),
        Vector("yes", "true", "sunny"),
        Vector("yes", "true", "overcast"),
        Vector("yes", "false", "overcast"),
        Vector("no", "true", "rainy"),
      ))

  behavior of "ID3"

  it should "getHighestCardinalityValue" in {
    val highestCardinalityValue = id3.getHighestCardinalityValue(id3.getColumnData(examples, "Play?"))
    logger info s"~highestCardinalityValue=$highestCardinalityValue"

    assert(highestCardinalityValue === "yes")
  }

  it should "getColumnData" in {
    val columnData = id3.getColumnData(examples, "Play?")
    logger info s"~columnData=$columnData"

    assert(columnData.size === 14)
  }

  it should "getClasses" in {

    val classes = id3.getClasses(playColumnData)
    logger info s"~classes=$classes"

    assert(classes.size === 2)
    assert(classes.contains("yes"))
    assert(classes.contains("no"))

  }

  it should "getProportionOfClass" in {
    val proportionOfYesClass = id3.getProportionOfClass("yes", playColumnData)
    val proportionOfNoClass = id3.getProportionOfClass("no", playColumnData)
    logger info s"~proportionOfYesClass=$proportionOfYesClass, proportionOfNoClass=$proportionOfNoClass"

    assert(proportionOfYesClass + proportionOfNoClass === 1d)
    assert(proportionOfYesClass === 0.6428 +- 0.001)
    assert(proportionOfNoClass === 0.3571 +- 0.001)

  }

  it should "entropy" in {

    val entropyOfPlay = id3.entropy(examples, "Play?")
    logger info s"~entropyOfPlay=$entropyOfPlay"

    assert(entropyOfPlay === 0.9403 +- 0.001)
  }

  it should "informationGain" in {

    val informationGainForWindy = id3.informationGain(examples, "Play?", "Windy")
    logger info s"~informationGainForWindy=$informationGainForWindy"

    assert(informationGainForWindy === 0.0481 +- 0.001)
  }

  it should "hypothesis" in {

    val hypothesis = id3.hypothesis(examples, "Play?", Vector("Windy", "Outlook"))
    logger info s"~hypothesis=$hypothesis"

  }

}
