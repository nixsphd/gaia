package ie.nix.nearest_neighbour

import ie.nix.nearest_neighbour.NearestNeighbour.{getEuclideanDistance, getManhattanDistance, getMinkowskiDistance}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NearestNeighbourTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  behavior of "NearestNeighbour"

  it should "getEuclideanDistance" in {
    val a = Vector(1d, 1d)
    val b = Vector(2d, 2d)
    val euclideanDistance = getEuclideanDistance(a, b): Double
    logger info s"~euclideanDistance=$euclideanDistance"

    assert(euclideanDistance === 1.4142 +- 0.0001)
  }

  it should "getEuclideanDistance in 3D" in {
    val a = Vector(1d, 1d, 1d)
    val b = Vector(2d, 2d, 2d)
    val euclideanDistance = getEuclideanDistance(a, b): Double
    logger info s"~euclideanDistance=$euclideanDistance"

    assert(euclideanDistance === 1.7320 +- 0.0001)
  }

  it should "getManhattanDistance" in {
    val a = Vector(1d, 1d)
    val b = Vector(2d, 2d)
    val manhattanDistance = getManhattanDistance(a, b): Double
    logger info s"~manhattanDistance=$manhattanDistance"

    assert(manhattanDistance === 2d)
  }

  it should "getManhattanDistance in 3D" in {
    val a = Vector(1d, 1d, 1d)
    val b = Vector(2d, 2d, 2d)
    val manhattanDistance = getManhattanDistance(a, b): Double
    logger info s"~manhattanDistance=$manhattanDistance"

    assert(manhattanDistance === 3d)
  }

  it should "MinkowskiDistance" in {}

  it should "getMinkowskiDistance order 2" in {
    val a = Vector(1d, 1d)
    val b = Vector(2d, 2d)
    val minkowskiDistance = getMinkowskiDistance(2)(a, b): Double
    logger info s"~minkowskiDistance=$minkowskiDistance"

    assert(minkowskiDistance === 1.4142 +- 0.0001)
  }

  it should "getMinkowskiDistance in 3D order 2" in {
    val a = Vector(1d, 1d, 1d)
    val b = Vector(2d, 2d, 2d)
    val minkowskiDistance = getMinkowskiDistance(2)(a, b): Double
    logger info s"~minkowskiDistance=$minkowskiDistance"

    assert(minkowskiDistance === 1.7320 +- 0.0001)
  }

  it should "getMinkowskiDistance order 1" in {
    val a = Vector(1d, 1d)
    val b = Vector(2d, 2d)
    val minkowskiDistance = getMinkowskiDistance(1)(a, b): Double
    logger info s"~minkowskiDistance=$minkowskiDistance"

    assert(minkowskiDistance === 2d)
  }

  it should "getMinkowskiDistance in 3D order 1" in {
    val a = Vector(1d, 1d, 1d)
    val b = Vector(2d, 2d, 2d)
    val minkowskiDistance = getMinkowskiDistance(1)(a, b): Double
    logger info s"~minkowskiDistance=$minkowskiDistance"

    assert(minkowskiDistance === 3d)
  }

}
