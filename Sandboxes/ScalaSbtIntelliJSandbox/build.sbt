name := "ScalaSbtIntelliJSandbox"

version := "0.1"

scalaVersion := "2.12.8"

// Log4J2
libraryDependencies += "org.apache.logging.log4j" % "log4j-api" % "2.11.2"
libraryDependencies += "org.apache.logging.log4j" % "log4j-core" % "2.11.2"
libraryDependencies += "org.apache.logging.log4j" % "log4j-api-scala_2.12" % "11.0"
// ScalaTest
libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.1.1"

