Supervised Learning
	predict output when given input vector
	Two flavours:
		Regression target output os a real number or vector of real numbers
		Classification target output os a class label
	Choose a model class, f, a way of using some numerical parameter, W, to 
	map each imput vector, x, into a predicted output, y.
	Learning is then adjusting the parameters to reduce the discrepancy 
	between the target, t, and actual output, y.
	discrepancy = 1/2 (y-t)^2
	
Reinforcement Learning
	Select action to maximise rewards. Rewards are only given occasionally 
	Use a discount factor for delayed rewards so that we don't have to look 
	to far into the future.
	Difficult as rewards are often delayed so inly trying to learn dozens 
	of parameters, not millions like supervised and unsupervised learning.
	
Unsupervised Learning
	Discover a good internal representation of the input.
	Largely gnired except for clustering for about 40 years.
	Provide compact low dimensional representations of the input.
	Principal COmponent Analysis, PCA. one manifold.
	Goal can be to find clusters
	
Neural Networks
	Feed-forward Neural Networks information come into the input neurons 
	in one directions through hidden layers until it reached the output neurons.
	Most commonest type. 
	input layer
	hidden layer (more than one means its a Deep Neural Network)
	output layer
	
Recurrant Neural Network where information can flow around in cycles. Can 
	remember information for a long time.
	Much more powerful than Feed-forward. Complicated dynamics but very 
	difficult to train. More powerful. More biologically realistic.
	Natural way to model sequential data.
	Use the same weights at every timestep.
	Can remember things for a long time.
	
Symetrically connected Netral Network. 
	Connections have same weights in both directions and a much easier to 
	analyse.
	Have an energy function. Can't hav cycles.

Statistical Pattern Recognition
	Convert raw input into feature activstions, done using hand written programmes.
	Learning phase learns how t weight each of the features.
	Add up all the features then if this over a threshold it positive example of the target class
	Perceptrons are example of Statistical Pattern Recognition
	developed in 1960 Rosenblatt with lots of grand claims
	Minsky & Papet showed their limitations in 1969
	But Perceptron Convergence Procedure

