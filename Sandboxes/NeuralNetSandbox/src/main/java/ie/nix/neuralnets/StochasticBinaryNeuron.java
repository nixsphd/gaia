package ie.nix.neuralnets;

import java.util.stream.IntStream;

import ie.nix.util.Randomness;

/* McCulloch-Pitts 1943 */
public class StochasticBinaryNeuron {

	public int numberOfConnections;
	public double[] weights;
	public double bias;
	
	public StochasticBinaryNeuron(int numberOfConnections, double bias) {
		this.numberOfConnections = numberOfConnections;
		weights = new double[numberOfConnections];
		this.bias = bias;
	
	}
	
	public StochasticBinaryNeuron(double[] weights, double bias) {
		this.numberOfConnections = weights.length;
		this.weights = weights;
		this.bias = bias;
	
	}

	public int output(double[] activities) {
		double z = bias + IntStream.range(0, numberOfConnections).mapToDouble(i -> 
		weights[i] * activities[i]).sum();
		return ((Randomness.nextDouble() < (1 / (1 + Math.exp(-z)))) ? 1 : 0);
		
	}
	
	public static void main(String[] args) {
		
		Randomness.setSeed(17);
		
		double[] weights = {2,4,6,8,10};
		double bias = -200;
		double[] activities = {2,4,6,8,10};
		
		StochasticBinaryNeuron linearNeuron = new StochasticBinaryNeuron(weights, bias);
		int output = linearNeuron.output(activities);
		System.out.println("output = "+output);
		
		bias = -300;
		linearNeuron = new StochasticBinaryNeuron(weights, bias);
		output = linearNeuron.output(activities);
		System.out.println("output = "+output);
		
	}
}
