package ie.nix.neuralnets;

import java.util.stream.IntStream;

/* McCulloch-Pitts 1943 */
public class RectifiedLinearNeuron {

	public int numberOfConnections;
	public double[] weights;
	public double bias;
	
	public RectifiedLinearNeuron(int numberOfConnections, double bias) {
		this.numberOfConnections = numberOfConnections;
		weights = new double[numberOfConnections];
		this.bias = bias;
	
	}
	
	public RectifiedLinearNeuron(double[] weights, double bias) {
		this.numberOfConnections = weights.length;
		this.weights = weights;
		this.bias = bias;
	
	}

	public double output(double[] activities) {
		double z = bias + IntStream.range(0, numberOfConnections).mapToDouble(i -> 
		weights[i] * activities[i]).sum();
		return (z > 0 ? z : 0);
		
	}
	
	public static void main(String[] args) {
		
		double[] weights = {2,4,6,8,10};
		double bias = -100;
		double[] activities = {2,4,6,8,10};
		
		RectifiedLinearNeuron linearNeuron = new RectifiedLinearNeuron(weights, bias);
		double output = linearNeuron.output(activities);
		System.out.println("output = "+output);
		
		bias = -300;
		linearNeuron = new RectifiedLinearNeuron(weights, bias);
		output = linearNeuron.output(activities);
		System.out.println("output = "+output);
		
	}
}
