package ie.nix.neuralnets;

import java.util.stream.IntStream;

/* McCulloch-Pitts 1943 */
public class SigmoidNeurons {

	public int numberOfConnections;
	public double[] weights;
	public double bias;
	
	public SigmoidNeurons(int numberOfConnections, double bias) {
		this.numberOfConnections = numberOfConnections;
		weights = new double[numberOfConnections];
		this.bias = bias;
	
	}
	
	public SigmoidNeurons(double[] weights, double bias) {
		this.numberOfConnections = weights.length;
		this.weights = weights;
		this.bias = bias;
	
	}

	public double output(double[] activities) {
		double z = bias + IntStream.range(0, numberOfConnections).mapToDouble(i -> 
		weights[i] * activities[i]).sum();
		return 1 / (1 + Math.exp(-z));
		
	}
	
	public static void main(String[] args) {
		
		double[] weights = {2,4,6,8,10};
		double bias = -200;
		double[] activities = {2,4,6,8,10};
		
		SigmoidNeurons linearNeuron = new SigmoidNeurons(weights, bias);
		double output = linearNeuron.output(activities);
		System.out.println("output = "+output);
		
		bias = -300;
		linearNeuron = new SigmoidNeurons(weights, bias);
		output = linearNeuron.output(activities);
		System.out.println("output = "+output);
		
	}
}
