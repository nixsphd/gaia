package ie.nix.neuralnets;

import java.util.stream.IntStream;

/* McCulloch-Pitts 1943 */
public class BinaryThresholdNeuron {

	public int numberOfConnections;
	public double[] weights;
	public double threshold;
	
	public BinaryThresholdNeuron(int numberOfConnections, double threshold) {
		this.numberOfConnections = numberOfConnections;
		weights = new double[numberOfConnections];
		this.threshold = threshold;
	
	}
	
	public BinaryThresholdNeuron(double[] weights, double threshold) {
		this.numberOfConnections = weights.length;
		this.weights = weights;
		this.threshold = threshold;
	
	}

	public int output(double[] activities) {
		return (IntStream.range(0, numberOfConnections).mapToDouble(i -> 
		weights[i] * activities[i]).sum() > threshold? 1 : 0);
		
	}
	
	public static void main(String[] args) {
		
		double[] weights = {2,4,6,8,10};
		double threshold = 100;
		double[] activities = {2,4,6,8,10};
		
		BinaryThresholdNeuron linearNeuron = new BinaryThresholdNeuron(weights, threshold);
		int output = linearNeuron.output(activities);
		System.out.println("output = "+output);
		
		threshold = 300;
		linearNeuron = new BinaryThresholdNeuron(weights, threshold);
		output = linearNeuron.output(activities);
		System.out.println("output = "+output);
		
	}
}
