package ie.nix.neuralnets;

import java.util.stream.IntStream;

public class LinearNeuron {

	public int numberOfConnections;
	public double[] weights;
	public double bias;
	
	public LinearNeuron(int numberOfConnections, double bias) {
		this.numberOfConnections = numberOfConnections;
		weights = new double[numberOfConnections];
		this.bias = bias;
	
	}
	
	public LinearNeuron(double[] weights, double bias) {
		this.numberOfConnections = weights.length;
		this.weights = weights;
		this.bias = bias;
	
	}

	public double output(double[] activities) {
		return bias + IntStream.range(0, numberOfConnections).mapToDouble(i -> 
		weights[i] * activities[i]).sum();
		
	}
	
	public static void main(String[] args) {
		
		double[] weights = {2,4,6,8,10};
		double bias = 3;
		double[] activities = {2,4,6,8,10};
		
		LinearNeuron linearNeuron = new LinearNeuron(weights, bias);
		double output = linearNeuron.output(activities);
		
		System.out.println("output = "+output);
		
		
	}
}
