package ie.nix.neuralnets;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Perceptron {

	public int numberOfConnections;
	public int[] weights;
	
	public Perceptron(int numberOfConnections) {
		this.numberOfConnections = numberOfConnections;
		weights = new int[numberOfConnections+1];
	}
	
	public Perceptron(int numberOfConnections, int bias) {
		this(numberOfConnections);
		weights[numberOfConnections] = bias;
	}
	
	public Perceptron(int[] weights, int bias) {
		this(weights.length, bias);
		System.arraycopy(weights, 0, this.weights, 0, weights.length);
		
	}

	public int output(int[] activities) {
		double output =  IntStream.range(0, numberOfConnections).mapToDouble(i -> 
		weights[i] * activities[i]).sum() + (weights[numberOfConnections]*1);
		return (output >= 0 ? 1 : 0);
		
	}
	
	public void addToWeights(int error) {
		System.out.println("addToWeights error = "+error);
		System.out.println("addToWeights Before = "+this);
		IntStream.range(0, numberOfConnections+1).forEach(i -> this.weights[i] += error);
		System.out.println("addToWeights After = "+this);
		
	}
	
//	public void addToWeights(int[] weights) {
//		System.out.println("addToWeights Weights = "+Arrays.toString(weights));
//		System.out.println("addToWeights Before = "+this);
//		IntStream.range(0, numberOfConnections+1).forEach(i -> this.weights[i] += weights[i]);
//		System.out.println("addToWeights After = "+this);
//		
//	}
//	
//	public void subtractFromWeights(int[] weights) {
//		System.out.println("subtractFromWeights Weights = "+Arrays.toString(weights));
//		System.out.println("subtractFromWeights Before = "+this);
//		IntStream.range(0, numberOfConnections+1).forEach(i -> this.weights[i] -= weights[i]);
//		System.out.println("subtractFromWeights After = "+this);
//		
//	}
	
	public String toString() {
		return "weights:"+Arrays.toString(weights);
	}

	// Perceptron Learning Rule
	public static void perceptronLearningRule(Perceptron perceptron, int[][] trainingCases, int numberOfLearningLoops) {
		//	Let input x = ( I1, I2, .., In) where each Ii = 0 or 1.
		//	And let output y = 0 or 1.

		//	Algorithm is repeat forever:
		for (int ll = 0; ll < numberOfLearningLoops; ll++) {	
			for (int tc = 0; tc < trainingCases.length; tc++) {	
				// Given input x = ( I1, I2, .., In). Perceptron produces output y. We are told correct output O.
				System.out.println("Training wth "+Arrays.toString(trainingCases[tc]));
				int output = perceptron.output(trainingCases[tc]);
				// If we had wrong answer, change wi's and t, otherwise do nothing. 
				int actual = trainingCases[tc][perceptron.numberOfConnections];
				int error = output - actual;
				if (error != 0) {
					System.out.println("output="+output+" != actual="+actual+", error="+error);
	
					// If output y=0, and "correct" output O=1, then y is not large enough, so reduce threshold and 
					// increase wi's. This will increase the output. (Note inputs cannot be negative, so high positive 
					// weights means high positive summed input.)
//					if (output == 0) {
//						System.out.println("adding weights, reduce bias");
						perceptron.addToWeights(error);
						
//					} else {
//						System.out.println("subtracting weights, increae bias");
//						perceptron.subtractFromWeights(trainingCases[tc]);
//						
//					}
				}
			}
		}
	
	}

	public static void testAND() {
		int[][] trainingCases = {
				{1,1,1},
				{1,0,0},
				{0,1,0},
				{0,0,0},
		};	

		Perceptron addPerceptron = new Perceptron(new int[]{1,1},-2);
		System.out.println("AND perceptron = "+addPerceptron);
		
		for (int tc = 0; tc < trainingCases.length; tc++) {	
			System.out.println(Arrays.toString(trainingCases[tc])+"=>"+addPerceptron.output(trainingCases[tc]));
		}
	}

	public static void testOR() {
		int[][] trainingCases = {
				{1,1,1},
				{1,0,1},
				{0,1,1},
				{0,0,0},
		};	

		Perceptron addPerceptron = new Perceptron(new int[]{1,1},-1);
		System.out.println("OR perceptron = "+addPerceptron);
		
		for (int tc = 0; tc < trainingCases.length; tc++) {	
			System.out.println(Arrays.toString(trainingCases[tc])+"=>"+addPerceptron.output(trainingCases[tc]));
		}
	}
	
	public static void trainAND() {
		int[][] trainingCases = {
				{1,1,1},
				{1,0,0},
				{0,1,0},
				{0,0,0},
		};	
		Perceptron perceptron = new Perceptron(trainingCases[0].length-1);
		int numberOfLearningLoops = 2;
		
		perceptronLearningRule(perceptron, trainingCases, numberOfLearningLoops); //, trainngResults);
		System.out.println("AND perceptron = "+perceptron);
		
		for (int tc = 0; tc < trainingCases.length; tc++) {	
			System.out.println(Arrays.toString(trainingCases[tc])+"=>"+perceptron.output(trainingCases[tc]));
		}
	}
	
	public static void trainOR() {
		int[][] trainingCases = {
				{1,1,1},
				{1,0,1},
				{0,1,1},
				{0,0,0},
		};	
		Perceptron perceptron = new Perceptron(trainingCases[0].length-1);
		int numberOfLearningLoops = 100;
		
		perceptronLearningRule(perceptron, trainingCases, numberOfLearningLoops); //, trainngResults);
		System.out.println("OR perceptron = "+perceptron);

		for (int tc = 0; tc < trainingCases.length; tc++) {	
			System.out.println(Arrays.toString(trainingCases[tc])+"=>"+perceptron.output(trainingCases[tc]));
		}
	}
	
	public static void main(String[] args) {
		
//		testAND();
//		testOR();
		
		trainAND();
//		trainOR();
				
	}
}
