package ie.j2s2j;

public class ImplementScalaTrait implements ScalaTrait {

    public void function() {
        System.out.println("ImplementScalaTrait.function()");
    }

    public void functionForString(String stringArgument) {
        System.out.println("ImplementScalaTrait.functionForString(" + stringArgument + ")");
    }

    public void functionWithDefaultImplementation(String stringArgument) {
        System.out.println("ImplementScalaTrait.functionWithDefaultImplementation(" + stringArgument + ")");
        ScalaTrait.super.functionWithDefaultImplementation(stringArgument);
    }
}