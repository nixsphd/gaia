package ie.j2s2j;

//https://lampwww.epfl.ch/~michelou/scala/using-scala-from-java.html

import static ie.j2s2j.AnonymousFunctionHolder.*;

public class JavaClass {
	
	public static void main(String[] args) {
		System.out.println("JavaClass.main()");

		// Call a methid on the Scala class
		new Scala().method();

		// Call a methos on a Scala object
		Scala.method2();

		// Call a hidden method on a Scala object.
		Scala$.MODULE$.method();

		// Getting a variable value
		System.out.println("Scala.variable()=" + Scala.variable());

		// Setting a variable
		Scala.variable_$eq(10);
		System.out.println("Scala.variable()=" + Scala.variable());

		// Calling an anonymous variable
		System.out.println("anonymousVariable=" + anonymousVariable());

		// Calling an anonymous functions
		System.out.println("anonymousFunction=" + anonymousFunction().apply("myArg"));

		// Extending a Scala trait and calling and a default implementatiom
		ImplementScalaTrait implementScalaTrait = new ImplementScalaTrait();
		implementScalaTrait.function();
		implementScalaTrait.functionForString("myArgumentFromJava");
		implementScalaTrait.functionWithDefaultImplementation("myArgumentFromJava");
	
		//
		ScalaClassWithDefaults scalaClassWithDefaults =
	            new ScalaClassWithDefaults("requiredArugument",
	            		ScalaClassWithDefaults.$lessinit$greater$default$2(),
	            		ScalaClassWithDefaults.$lessinit$greater$default$3());
	    System.out.println("scalaClassWithDefaults="+scalaClassWithDefaults);
		
	}
}
