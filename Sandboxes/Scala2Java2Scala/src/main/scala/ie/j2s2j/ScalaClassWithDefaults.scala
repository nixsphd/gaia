package ie.j2s2j

class ScalaClassWithDefaults(
    stringVariable: String,
    intVariableWithDefault: Int = -1,
    stringVariableWithDefault: String = "default") {
  
    override def toString = stringVariable+
      " (intVariableWithDefault="+intVariableWithDefault+
      ",stringVariableWithDefault="+stringVariableWithDefault+")"
}

object ScalaClassWithDefaults extends App {
  
  val scalaClassWithDefaults = 
    new ScalaClassWithDefaults("notADefault")
  
  println(scalaClassWithDefaults)
  
}