package ie.j2s2j

// https://lampwww.epfl.ch/~michelou/scala/using-scala-from-java.html

class Scala {

    def method() {
        println("class Scala.method()")
    }

}

object Scala {

    var variable: Double = 1

    def method() {
        println("object Scala.method()")
    }

    def method2() {
        println("object Scala.method2()")
    }

}
