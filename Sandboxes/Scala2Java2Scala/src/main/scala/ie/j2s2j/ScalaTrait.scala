package ie.j2s2j

trait ScalaTrait {

    def function()

    def functionForString(stringArgument: String)

    def functionWithDefaultImplementation(stringArgument: String) {
        print(s"ScalaTrait.functionWithDefaultImplementation($stringArgument)")
    }
}