package ie.j2s2j

object AnonymousFunctionHolder {

    var anonymousVariable = "anonymousVariable"

    var anonymousFunction = (argument: String) => {
        s"anonymousFunction($argument)"
    }

}