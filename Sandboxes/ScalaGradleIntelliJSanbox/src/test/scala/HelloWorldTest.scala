import org.scalatest.funsuite._

import org.apache.logging.log4j.scala.Logging

class HelloWorldTest extends AnyFunSuite with Logging {

    test("The HelloWorld variable is true") {
        logger.info(s"HelloWorld.variable=${HelloWorld.variable}")
        assert(HelloWorld.variable)
    }

    ignore("The HelloWorld variable is false") {
        logger.info(s"HelloWorld.variable=${HelloWorld.variable}")
        assert(!HelloWorld.variable)
    }

}