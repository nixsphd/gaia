import org.apache.logging.log4j.scala.Logging
import org.apache.logging.log4j.Level

object HelloWorld extends App with Logging {
  println("Hello World!!")
  logger.info("I said Hi.")
  
  def variable: Boolean = true
  logger.info(s"variable=${variable}")

}