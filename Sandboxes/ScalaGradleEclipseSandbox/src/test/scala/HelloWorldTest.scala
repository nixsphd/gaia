import org.scalatest._
import org.scalatest.funsuite._

import org.apache.logging.log4j.scala.Logging
import org.apache.logging.log4j.Level

class HelloWorldTest extends AnyFunSuite with Logging {

  test("The HelloWorld variable is true") {
    logger.info(s"HelloWorld.variable=${HelloWorld.variable}")
    assert(HelloWorld.variable == true)
  }
  
  ignore("The HelloWorld variable is false") {
    logger.info(s"HelloWorld.variable=${HelloWorld.variable}")
    assert(HelloWorld.variable == false)
  }
  
}