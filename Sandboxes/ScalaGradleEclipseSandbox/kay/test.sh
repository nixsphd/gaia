#!/bin/sh 

#SBATCH --time=01:00:00
#SBATCH --nodes=1
#SBATCH -A ngcom014c
#SBATCH -p DevQ

module load java/8

#export PROJECT_ROOT=/Users/nicolamcdonnell/Desktop/Gaia/Sandboxes/ScalaGradleEclipseSandbox
export PROJECT_ROOT=/ichec/home/users/nix/gaia/Sandboxes/ScalaGradleEclipseSandbox
export CLASSPATH=$PROJECT_ROOT/build/classes/scala/main:$PROJECT_ROOT/build/resources/main:$PROJECT_ROOT/lib/*

java -cp $CLASSPATH HelloWorld