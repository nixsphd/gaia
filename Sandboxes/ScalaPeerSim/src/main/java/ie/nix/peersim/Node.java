package ie.nix.peersim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.GeneralNode;

public class Node extends GeneralNode {
	
	private static final Logger LOGGER = 
			LogManager.getLogger();

	public Node(String prefix) {
		super(prefix);
		LOGGER.info("~prefix={}", prefix);
	}

	@SuppressWarnings("squid:S2975")
	@Override
	public Object clone() {
		Node foo = (Node)super.clone();
        LOGGER.info("~foo={}", foo);
		return foo;
	}
	
	public String toString() {
		return "Node-"+getIndex();
	}
	
}
