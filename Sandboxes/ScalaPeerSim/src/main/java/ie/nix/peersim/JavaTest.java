package ie.nix.peersim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JavaTest {

	private static final Logger LOGGER = 
			LogManager.getLogger();
	
	public static void main(String[] args) {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] { 
			"src/main/resources/java.properties"});

		LOGGER.info("~DONE.");
	}

}
