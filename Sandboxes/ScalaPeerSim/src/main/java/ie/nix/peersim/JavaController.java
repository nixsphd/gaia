package ie.nix.peersim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.Control;

public class JavaController implements Control {

	private static final Logger LOGGER = 
			LogManager.getLogger();
	
	public JavaController(String prefix) {
		LOGGER.info("~prefix={}", prefix);
	}
	
	@Override
	public boolean execute() {
        LOGGER.info("");
		return false;
	}

}
