package ie.nix.peersim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.cdsim.CDProtocol;
import peersim.core.Node;
import peersim.edsim.EDProtocol;

public class JavaProtocol implements CDProtocol, EDProtocol {
	
	private static final Logger LOGGER = 
			LogManager.getLogger();

	public JavaProtocol(String prefix) {
		LOGGER.info("~prefix={}", prefix);
	}
	
	public String toString() {
		return "JavaProtocol";
	}

	@SuppressWarnings("squid:S2975")
	@Override
	public Object clone() {
		JavaProtocol foo = null;
		try {
			foo = (JavaProtocol)super.clone();
			
		} catch (CloneNotSupportedException e) {
	        LOGGER.error("~CloneNotSupportedException", e);
		}
        LOGGER.info("~foo={}", foo);
		return foo;
	}

	@Override
	public void processEvent(Node node, int protocolID, Object event) {
        LOGGER.info("~node={}, protocolID={}, event={}", 
        		node, protocolID, event);
	}

	@Override
	public void nextCycle(Node node, int protocolID) {
        LOGGER.info("~node={}, protocolID={}", 
        		node, protocolID);
		
	}
}
