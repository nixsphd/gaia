package ie.nix.peersim.scala

import org.apache.logging.log4j.scala.Logging
import peersim.core.GeneralNode;

class Node(prefix: String) extends GeneralNode(prefix) with Logging {
  
  logger.info(s"~prefix=${prefix}")

  override def toString: String = {
    "Node-" + getIndex()
  }
  
  override def clone: Object = {
		val foo: Object = super.clone
    logger.info(s"~foo=$foo")
		foo
	}

}