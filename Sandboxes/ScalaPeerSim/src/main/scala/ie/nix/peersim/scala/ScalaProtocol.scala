package ie.nix.peersim.scala

import org.apache.logging.log4j.scala.Logging
import peersim.cdsim.CDProtocol
import peersim.edsim.EDProtocol;

class ScalaProtocol(prefix: String) extends CDProtocol with EDProtocol with Logging {

    logger.info(s"prefix=${prefix}")

    override def toString: String = {
        "ScalaProtocol"
    }

    override def clone: Object = {
        val foo: Object = super.clone
        logger.info(s"~foo=$foo");
        foo
    }

    override def processEvent(node: peersim.core.Node, protocolID: Int, event: Any) = {
        logger.info(s"~node=$node, protocolID=$protocolID, event=$event");
    }

    override def nextCycle(node: peersim.core.Node, protocolID: Int) = {
        logger.info(s"~node=$node, protocolID=$protocolID");
    }
}