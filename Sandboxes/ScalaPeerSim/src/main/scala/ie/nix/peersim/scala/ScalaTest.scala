package ie.nix.peersim.scala

import org.apache.logging.log4j.scala.Logging

object ScalaTest extends App with Logging {
  
  val peerSimParameters = 
    Array("src/main/resources/scala.properties")
  
  peersim.Simulator.main(peerSimParameters);
  
  logger.info(s"~DONE.")
  
}