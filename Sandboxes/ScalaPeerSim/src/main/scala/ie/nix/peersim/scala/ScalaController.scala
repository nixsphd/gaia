package ie.nix.peersim.scala

import org.apache.logging.log4j.scala.Logging

import peersim.core.Control;

class ScalaController(prefix: String) extends Control with Logging {
  
  logger.info(s"prefix=${prefix}")
  
  override def execute(): Boolean = {
    logger.info("")
		false
	}
  
}