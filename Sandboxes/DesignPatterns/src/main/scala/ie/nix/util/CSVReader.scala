package ie.nix.util

import ie.nix.util.CSVFileReader.RESOURCES_DIR
import org.apache.logging.log4j.scala.Logging

import scala.io.Source

trait CSVFileReader {

  def readData(dataSetName: String): DataFrame

}

object CSVFileReader {

  val RESOURCES_DIR = "src/main/resources"

  def apply(): CSVFileReader = {
    new CSVFileReaderImpl()
  }
}

@SuppressWarnings(Array("org.wartremover.warts.Null"))
class CSVFileReaderImpl() extends CSVFileReader with Logging {

  override def readData(dataSetName: String): DataFrame = {

    logger.info(s"~dataSetName=$dataSetName")

    val source = Source.fromFile(RESOURCES_DIR + "/" + dataSetName + ".csv")
    val lines = source.getLines()

    val headersString = lines.next()
    val headers = headersString.split(",").map(_.trim).toVector
    logger.trace(s"~headers=$headers")

    val dataFrame = DataFrame(headers)

    lines.foldLeft(dataFrame)((dataFrame, row) => {
      val rowValues = row.split(",").map(_.trim).toVector
      logger.trace(s"~rowValues=${rowValues.mkString(" ")}")
      dataFrame.addRow(rowValues)
    })
  }

}
