package ie.nix.patterns.proxy

import java.rmi.{Remote, RemoteException}

// No state and no implementations allowed in the remote
trait RemoteGumballMachine extends Remote {

  @throws(classOf[RemoteException])
  def getLocation: String

  @throws(classOf[RemoteException])
  def getCount: Int

  @throws(classOf[RemoteException])
  def getState: GumballMachineState

}
