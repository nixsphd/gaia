package ie.nix.patterns.adapter

import org.apache.logging.log4j.scala.Logging

object DuckTestDrive extends App with Logging {

  val duck: MallardDuck = new MallardDuck();
  val turkey: WildTurkey = new WildTurkey();
  val turkeyAdapter: TurkeyAdapter = new TurkeyAdapter(turkey);

  logger info s"The Turkey says...${turkey.gobble()}"
  logger info s"The Turkey ${turkey.fly()}"
  logger info s"The Duck says...${duck.quack()}"
  logger info s"The Duck says...${duck.fly()}"
  logger info s"The TurkeyAdapter says...${turkeyAdapter.quack()}"
  logger info s"The TurkeyAdapter says...${turkeyAdapter.fly()}"

}
