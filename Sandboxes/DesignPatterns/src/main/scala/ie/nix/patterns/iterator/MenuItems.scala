package ie.nix.patterns.iterator

class MenuItem(val name: String, val description: String, val vegetarian: Boolean, val price: Double) {

  def getName: String = name
  def getDescription: String = description
  def getPrice: Double = price
  def isVegetarian: Boolean = vegetarian

}
