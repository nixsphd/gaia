package ie.nix.patterns.template

import org.apache.logging.log4j.scala.Logging

class Coffee extends Logging {

  def prepareRecipe(): Unit = {
    boilWater();
    brewCoffeeGrinds();
    pourInCup();
    addSugarAndMilk();
  }

  def boilWater(): Unit = logger info "Boiling water"
  def brewCoffeeGrinds(): Unit = logger info "Dripping Coffee through filter"
  def pourInCup(): Unit = logger info "Pouring into cup"
  def addSugarAndMilk(): Unit = logger info "Adding Sugar and Milk"

}
