package ie.nix.patterns.factory

import ie.nix.patterns.factory.SimplePizzaFactory.createPizza

object PizzaStoreWithSimplePizzaFactory extends App {
  val pizzaStore: PizzaStoreWithSimplePizzaFactory = new PizzaStoreWithSimplePizzaFactory()
  val pizzaType = "cheese"
  pizzaStore.orderPizza(pizzaType)
}

class PizzaStoreWithSimplePizzaFactory() {

  def orderPizza(pizzaType: String): Pizza = {
    val pizza: Pizza = createPizza(pizzaType)
    pizza.prepare()
    pizza.bake()
    pizza.cut()
    pizza.box()
    pizza
  }

}
