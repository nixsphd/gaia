package ie.nix.patterns.singleton

/*

 Singleton assures that at any one time there is only one instance of me

 Examples:
 thread pools,
 caches,
 dialog boxes,
 objects that handle preferences and registry settings,
 objects used for logging, and
 objects that act as device drivers to devices like printers and graphics cards

 */
/*
Questions:
Is the class is immutable then each method which update the state needs to
return a new instance which also must be set in the maybeUniqueInstance
 */
object Singleton {

  private var maybeUniqueInstance: Option[Singleton] = None

  def instance: Singleton = {
    if (!maybeUniqueInstance.isDefined) {
      // Lazy instantiation
      maybeUniqueInstance = Some(new Singleton)
    }
    maybeUniqueInstance.get
  }
}

class Singleton private () {}
