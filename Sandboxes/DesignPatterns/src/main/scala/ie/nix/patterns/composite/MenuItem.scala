package ie.nix.patterns.composite

import org.apache.logging.log4j.scala.Logging

class MenuItem(name: String, description: String, vegetarian: Boolean, price: Double)
    extends MenuComponent
    with Logging {

  override def getName: String = name;
  override def getDescription: String = description;
  override def getPrice: Double = price;
  override def isVegetarian: Boolean = vegetarian;

  override def print(): Unit = logger info s" $getName()() ${if (isVegetarian) "(v)"}, $getPrice() -- $getDescription()"

}
