package ie.nix.patterns.singleton

import org.apache.logging.log4j.scala.Logging

import scala.reflect.ClassTag

class Dog protected () extends Logging {
  def bark(): Unit = logger info "Woof"
}

class Matilda protected () extends Dog with Logging {
  def openPR(): Unit = logger info "Opening a PR."
}

object Dog extends Logging {

  // Smell either need a mutable map or to use a var
  private var dogs: Map[ClassTag[_], Dog] = Map[ClassTag[_], Dog]()

  // need to pass an implicit argument which is a classTage for the
  // parameter type D
  def instance[D <: Dog](implicit dClassTag: ClassTag[D]): D = {
    val maybeDog = dogs.get(dClassTag)
    maybeDog match {
      // Smell - Need to case as can't check type of dog is D due to type erasure
      case Some(dog) => dog.asInstanceOf[D]
      case _ => {
        // Smell - kinda locks us down to a constructor with no args
        // Smell - Need to cast
        val dog: D = dClassTag.runtimeClass.newInstance().asInstanceOf[D]
        dogs += (dClassTag -> dog)
        dog
      }
    }
  }
}

object Run extends App with Logging {

  val matilda: Matilda = Dog.instance[Matilda]
  logger info s"matilda=${matilda.hashCode()}"
  matilda.openPR()

  val sameMatilda: Matilda = Dog.instance[Matilda]
  logger info s"sameMatilda=${matilda.hashCode()}"
  sameMatilda.openPR()

  assert(matilda eq sameMatilda)

  /*
  Gives compile error:
    type arguments [Int] do not conform to method instance's type
    parameter bounds [D <: ie.nix.patterns.singleton.Dog]
   */
  //val int: Int = Dog.instance[Int]

}
