package ie.nix.patterns.strategy

import org.apache.logging.log4j.scala.Logging

trait QuackBehavior {
  def quack(): Unit
}

class Quack extends QuackBehavior with Logging {
  def quack(): Unit = {
    logger info "Quack"
  }
}

class MuteQuack extends QuackBehavior with Logging {
  def quack(): Unit = {
    logger info "<< Silence >>"
  }
}

class Squeak extends QuackBehavior with Logging {
  def quack(): Unit = {
    logger info "Squeak"
  }
}
