package ie.nix.patterns.typeclasses.context_bound

sealed trait JSONValue {}
case class JSONObject(entries: Map[String, JSONValue]) extends JSONValue
case class JSONString(string: String) extends JSONValue
case class JSONNumber(number: Int) extends JSONValue
case object JSONNull extends JSONValue

// JSONConverter is renamed to simply json
trait JSON[A] {
  def json(a: A): JSONValue
}

object JSON {

  implicit val jsonInt = new JSON[Int] {
    override def json(int: Int): JSONValue = JSONNumber(int)
  }

  implicit def jsonPair[T1: JSON, T2: JSON]: JSON[(T1, T2)] =
    (pair: (T1, T2)) =>
      JSONObject(Map("1st" -> implicitly[JSON[T1]].json(pair._1), "2nd" -> implicitly[JSON[T2]].json(pair._2)))
}

object JSONWriter {

  def write(jsonNValue: JSONValue): String = jsonNValue match {
    case JSONObject(entries) =>
      entries.map(keyAndValue => s"${keyAndValue._1} : ${write(keyAndValue._2)}").mkString("{", ", ", "}")
    case JSONNumber(number) => s"$number"
    case JSONString(string) => string
    case JSONNull           => s"Null"
  }

  def write[A: JSON](value: A): String = write(implicitly[JSON[A]].json(value))

}
