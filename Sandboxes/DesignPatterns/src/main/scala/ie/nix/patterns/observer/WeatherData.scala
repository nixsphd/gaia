package ie.nix.patterns.observer

import ie.nix.patterns.observer.WeatherData.WeatherDataObserver
import org.apache.logging.log4j.scala.Logging

/*
    The Observer Pattern defines a one-to-many dependency between objects so
    that when one object changes state, all of its dependents are notified and
    updated automatically.
 */
class WeatherData {

  var weatherDataObservers: Vector[WeatherDataObserver] = Vector[WeatherDataObserver]()

  def temperature: Double = 0d
  def humidity: Double = 0d
  def pressure: Double = 0d

  def measurementsChanged(): Unit =
    weatherDataObservers
      .foreach(_.update(temperature, humidity, pressure))

  def registerObserver(observer: WeatherDataObserver): Unit =
    weatherDataObservers = weatherDataObservers :+ observer

  def removeObserver(observer: WeatherDataObserver): Unit =
    weatherDataObservers = weatherDataObservers.filterNot(_ == observer)

}

object WeatherData {
  trait WeatherDataObserver {
    def update(temperature: Double, humidity: Double, pressure: Double): Unit
  }
}

object WeatherDataDemo extends App with Logging {

  val currentConditionsDisplay = new WeatherDataObserver {
    override def update(temperature: Double, humidity: Double, pressure: Double): Unit =
      logger info s"currentConditionsDisplay temperature=$temperature, humidity=$humidity, pressure=$pressure"
  }

  val statisticsDisplay = new WeatherDataObserver {
    override def update(temperature: Double, humidity: Double, pressure: Double): Unit =
      logger info s"statisticsDisplay temperature=$temperature, humidity=$humidity, pressure=$pressure"
  }

  val forecastDisplay = new WeatherDataObserver {
    override def update(temperature: Double, humidity: Double, pressure: Double): Unit =
      logger info s"forecastDisplay temperature=$temperature, humidity=$humidity, pressure=$pressure"
  }

  val weatherData = new WeatherData
  weatherData.registerObserver(currentConditionsDisplay)
  weatherData.registerObserver(statisticsDisplay)

  weatherData.measurementsChanged()

}
