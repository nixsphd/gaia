package ie.nix.patterns.facade

import org.apache.logging.log4j.scala.Logging

object HomeTheaterDemo extends App with Logging {
  val homeTheaterFacade =
    new HomeTheaterFacade(new Amp, new Tuner, new DVD, new Cd, new Projector, new Screen, new Light, new Popper)

  homeTheaterFacade.watchMovie("Withnail & 1")
  logger info s"2.5 hours of bliss pass...."
  homeTheaterFacade.endMovie()

}

class HomeTheaterFacade(val amp: Amp,
                        var tuner: Tuner,
                        var dvd: DVD,
                        var cd: Cd,
                        var projector: Projector,
                        var screen: Screen,
                        var lights: Light,
                        var popper: Popper)
    extends Logging {

  def watchMovie(movie: String): Unit = {
    logger info s"Get ready to watch a movie..."
    popper.on()
    popper.pop();
    lights.dim(10);
    screen.down();
    projector.on();
    projector.wideScreenMode();
    amp.on();
    amp.setDvd(dvd);
    amp.setSurroundSound();
    amp.setVolume(5);
    dvd.on();
    dvd.play(movie);
  }

  def endMovie(): Unit = {
    logger info s"Shutting movie theater down..."
    popper.off()
    lights.on();
    screen.up();
    projector.off();
    amp.off();
    dvd.stop();
    dvd.eject();
    dvd.off();
  }

}

case class Amp() extends Logging {
  def on(): Unit = logger info s"on"
  def off(): Unit = logger info s"off"
  def setVolume(i: Int): Unit = logger info s"setVolume to $i"
  def setSurroundSound(): Unit = logger info s"setSurroundSound"
  def setDvd(dvd: DVD): Unit = logger info s"setDvd $dvd"
}

case class Tuner() extends Logging

case class DVD() extends Logging {
  def off(): Unit = logger info s"off"
  def eject(): Unit = logger info s"eject"
  def stop(): Unit = logger info s"stop"
  def play(movie: String): Unit = logger info s"play $movie"
  def on(): Unit = logger info s"on"
}

case class Cd() extends Logging

case class Projector() extends Logging {
  def off(): Unit = logger info s"off"
  def wideScreenMode(): Unit = logger info s"wideScreenMode"
  def on(): Unit = logger info s"on"
}

case class Screen() extends Logging {
  def up(): Unit = logger info s"up"
  def down(): Unit = logger info s"down"
}

case class Light() extends Logging {
  def on(): Unit = logger info s"on"
  def dim(i: Int): Unit = logger info s"dim to $i"
}

case class Popper() extends Logging {
  def off(): Unit = logger info s"off"
  def pop(): Unit = logger info s"pop"
  def on(): Unit = logger info s"on"
}
