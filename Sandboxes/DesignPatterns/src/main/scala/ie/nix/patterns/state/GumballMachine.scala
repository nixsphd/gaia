package ie.nix.patterns.state

import ie.nix.patterns.state.GumballMachine.{HAS_QUARTER, NO_QUARTER, SOLD, SOLD_OUT}
import org.apache.logging.log4j.scala.Logging

class GumballMachine(private var count: Int) extends Logging {

  override def toString: String = state match {
    case HAS_QUARTER => s"GumballMachine(HAS_QUARTER, count=$count)"
    case NO_QUARTER  => s"GumballMachine(NO_QUARTER, count=$count)"
    case SOLD_OUT    => s"GumballMachine(SOLD_OUT, count=$count)"
    case SOLD        => s"GumballMachine(SOLD, count=$count)"
  }

  private var state: Int = if (count > 0) NO_QUARTER else SOLD_OUT

  def getState: Int = state
  def getCount: Int = count

  def insertQuarter(): Unit = state match {
    case HAS_QUARTER =>
      logger info "You can’t insert another quarter"
    case NO_QUARTER =>
      logger info "You inserted a quarter"
      state = HAS_QUARTER;
    case SOLD_OUT =>
      logger info "You can’t insert a quarter, the machine is sold out"
    case SOLD =>
      logger info "Please wait, we’re already giving you a gumball"
  }

  def ejectQuarter(): Unit = state match {
    case HAS_QUARTER =>
      logger info "Quarter returned"
      state = NO_QUARTER;
    case NO_QUARTER =>
      logger info "You haven’t inserted a quarter"
    case SOLD_OUT =>
      logger info "Sorry, you already turned the crank"
    case SOLD =>
      logger info "You can’t eject, you haven’t inserted a quarter yet"
  }

  def turnCrank(): Unit = state match {
    case HAS_QUARTER =>
      logger info "You turned..."
      state = SOLD;
      dispense();
    case NO_QUARTER =>
      logger info "You turned but there’s no quarter"
    case SOLD_OUT =>
      logger info "You turned, but there are no gumballs"
    case SOLD =>
      logger info "Turning twice doesn't get you another gumball!"
  }

  def dispense(): Unit = state match {
    case HAS_QUARTER =>
      logger info "No gumball dispensed"
    case NO_QUARTER =>
      logger info "You need to pay first"
    case SOLD_OUT =>
      logger info "No gumball dispensed"
    case SOLD =>
      logger info "A gumball comes rolling out the slot"
      count = count - 1;
      if (count == 0) {
        logger info "Oops, out of gumballs!"
        state = SOLD_OUT;
      } else {
        state = NO_QUARTER;
      }
  }

}

object GumballMachine {
  val SOLD_OUT = 0
  val NO_QUARTER = 1
  val HAS_QUARTER = 2
  val SOLD = 3
}
