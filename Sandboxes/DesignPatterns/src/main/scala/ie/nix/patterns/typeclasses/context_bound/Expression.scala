package ie.nix.patterns.typeclasses.context_bound

import org.apache.logging.log4j.scala.Logging

// Expression Type Class
trait Expression[E] {
  def value(exp: E): Int
}

object Expression {
  implicit val expressionInt = new Expression[Int] {
    override def value(int: Int): Int = int
  }

  implicit def expressionPlusPair[T1: Expression, T2: Expression]: Expression[(T1, T2)] = { (pair: (T1, T2)) =>
    implicitly[Expression[T1]].value(pair._1) + implicitly[Expression[T2]].value(pair._2)
  }

}

object ExpressionEvaluator {
  def evaluate[E: Expression](exp: E): Int =
    implicitly[Expression[E]].value(exp)
}

object TestImplicits extends App with Logging {

  val tuples = (1, (2, 3))
  val tuplesAsValue = ExpressionEvaluator.evaluate(tuples)
  logger info s"expAsJSON=$tuplesAsValue"

  val tuplesAsJSON = JSONWriter.write(tuples)
  logger info s"expAsJSON=$tuplesAsJSON"
}
