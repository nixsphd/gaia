package ie.nix.patterns.observer

trait Observable[T] {
  t: T =>

  var observers: Vector[Observer[T]] = Vector[Observer[T]]()

  def registerObserver(observer: Observer[T]): Unit =
    observers = observers :+ observer

  def removeObserver(observer: Observer[T]): Unit =
    observers = observers.filterNot(_ == observer)

  def notifyObservers(arg: Any): Unit =
    observers.foreach(_.update(this, arg))

  def notifyObservers(): Unit =
    notifyObservers(None)

}
