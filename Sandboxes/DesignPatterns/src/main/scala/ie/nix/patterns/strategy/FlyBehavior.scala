package ie.nix.patterns.strategy

import org.apache.logging.log4j.scala.Logging

trait FlyBehavior {
  def fly(): Unit
}

class FlyWithWings extends FlyBehavior with Logging {
  def fly(): Unit = logger info "I’m flying!!"
}

class FlyNoWay extends FlyBehavior with Logging {
  def fly(): Unit = logger info "I can’t fly"
}

class FlyRocketPowered extends FlyBehavior with Logging {
  def fly(): Unit = logger info "I’m flying with a rocket!"
}
