package ie.nix.patterns.proxy

import org.apache.logging.log4j.scala.Logging

import scala.util.Random

class GumballMachine(private val location: String, private var count: Int) extends Logging {

  private val noQuarter = new NoQuarter(this)
  private val hasQuarter = new HasQuarter(this)
  private val sold = new Sold(this)
  private val winning = new Winning(this)
  private val soldOut = new SoldOut(this)

  private var state: GumballMachineState = if (count > 0) noQuarter else soldOut

  def getNoQuarter: GumballMachineState = noQuarter
  def getHasQuarter: GumballMachineState = hasQuarter
  def getSold: GumballMachineState = sold
  def getWinning: GumballMachineState = winning
  def getSoldOut: GumballMachineState = soldOut

  def setState(state: GumballMachineState): Unit = this.state = state
  def getState: GumballMachineState = state

  def getCount: Int = count
  def getLocation: String = location

  def insertQuarter(): Unit = state.insertQuarter()
  def ejectQuarter(): Unit = state.ejectQuarter()
  def turnCrank(): Unit = {
    state.turnCrank()
    state.dispense()
  }

  def releaseBall(): Unit = {
    logger info s"A gumball comes rolling out the slot..."
    if (count != 0) count = count - 1;
  }

}

sealed trait GumballMachineState extends Serializable with Logging {
  def toString: String
  def insertQuarter(): Unit
  def ejectQuarter(): Unit
  def turnCrank(): Unit
  def dispense(): Unit
}

class NoQuarter(@transient gumballMachine: GumballMachine) extends GumballMachineState with Logging {
  override def toString: String = "NoQuarter"

  override def insertQuarter(): Unit = {
    logger info s"You inserted a quarter"
    gumballMachine.setState(gumballMachine.getHasQuarter)
  }
  override def ejectQuarter(): Unit =
    logger info "You haven’t inserted a quarter"
  override def turnCrank(): Unit =
    logger info "You turned but there’s no quarter"
  override def dispense(): Unit =
    logger info "You need to pay first"
}

class HasQuarter(@transient gumballMachine: GumballMachine) extends GumballMachineState with Logging {
  override def toString: String = "HasQuarter"
  override def insertQuarter(): Unit =
    logger info "You can’t insert another quarter"
  override def ejectQuarter(): Unit = {
    logger info "Quarter returned"
    gumballMachine.setState(gumballMachine.getNoQuarter)
  }
  override def turnCrank(): Unit = {
    logger info "You turned..."
    if (Random.nextDouble() <= 0.9)
      gumballMachine.setState(gumballMachine.getSold)
    else
      gumballMachine.setState(gumballMachine.getWinning)
  }
  override def dispense(): Unit =
    logger info "No gumball dispensed"
}

class Sold(@transient gumballMachine: GumballMachine) extends GumballMachineState with Logging {
  override def toString: String = "Sold"
  override def insertQuarter(): Unit =
    logger info "Please wait, we’re already giving you a gumball"
  override def ejectQuarter(): Unit =
    logger info "You can’t eject, you haven’t inserted a quarter yet"
  override def turnCrank(): Unit =
    logger info "Turning twice doesn't get you another gumball!"
  override def dispense(): Unit = {
    logger info "A gumball comes rolling out the slot"
    gumballMachine.releaseBall()
    if (gumballMachine.getCount == 0) {
      logger info "Oops, out of gumballs!"
      gumballMachine.setState(gumballMachine.getSoldOut)
    } else {
      gumballMachine.setState(gumballMachine.getNoQuarter)
    }
  }
}

class Winning(@transient gumballMachine: GumballMachine) extends GumballMachineState with Logging {
  override def toString: String = "Sold"
  override def insertQuarter(): Unit =
    logger info "Please wait, we’re already giving you a gumball"
  override def ejectQuarter(): Unit =
    logger info "You can’t eject, you haven’t inserted a quarter yet"
  override def turnCrank(): Unit =
    logger info "Turning twice doesn't get you another gumball!"
  override def dispense(): Unit = {
    logger info "A gumball comes rolling out the slot"
    gumballMachine.releaseBall()
    if (gumballMachine.getCount == 0) {
      logger info "Oops, out of gumballs!"
      gumballMachine.setState(gumballMachine.getSoldOut)
    } else {
      gumballMachine.releaseBall()
      if (gumballMachine.getCount == 0) {
        logger info "Oops, out of gumballs!"
        gumballMachine.setState(gumballMachine.getSoldOut)
      } else {
        gumballMachine.setState(gumballMachine.getNoQuarter)
      }
    }
  }
}

class SoldOut(@transient gumballMachine: GumballMachine) extends GumballMachineState with Logging {
  override def toString: String = "SoldOut"
  override def insertQuarter(): Unit =
    logger info s"You can’t insert a quarter, the machine, $gumballMachine, is sold out"
  override def ejectQuarter(): Unit =
    logger info "Sorry, you already turned the crank"
  override def turnCrank(): Unit =
    logger info "You turned, but there are no gumballs"
  override def dispense(): Unit =
    logger info "No gumball dispensed"
}
