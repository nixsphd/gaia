package ie.nix.patterns.factory

import ie.nix.patterns.factory.pizza.{CaliforniaCheesePizza, ChicagoCheesePizza, NYCheesePizza}

abstract class AbstractPizzaStore {

  def orderPizza(pizzaType: String): Pizza = {
    val pizza = createPizza(pizzaType)
    pizza.prepare()
    pizza.bake()
    pizza.cut()
    pizza.box()
    pizza
  }

  def createPizza(pizzaType: String): Pizza

}

class NYPizzaStore extends AbstractPizzaStore {
  override def createPizza(pizzaType: String): Pizza = {
    pizzaType match {
      case "cheese" => new NYCheesePizza()
    }
  }
}

class ChicagoPizzaStore extends AbstractPizzaStore {
  override def createPizza(pizzaType: String): Pizza = {
    pizzaType match {
      case "cheese" => new ChicagoCheesePizza()
    }
  }
}

class CaliforniaPizzaStore extends AbstractPizzaStore {
  override def createPizza(pizzaType: String): Pizza = {
    pizzaType match {
      case "cheese" => new CaliforniaCheesePizza()
    }
  }
}
