package ie.nix.patterns.command

import ie.nix.patterns.command.CeilingFan.CeilingFanHighCommand
import ie.nix.patterns.command.GarageDoor.{GarageDoorClosedCommand, GarageDoorOpenCommand}
import ie.nix.patterns.command.Light.{LightOffCommand, LightOnCommand}

class SimpleRemoteControl(numberOfSlots: Int) {

  val onSlots = Array.fill[Command](numberOfSlots)(NoCommand)
  val offSlots = Array.fill[Command](numberOfSlots)(NoCommand)
  var undoCommand: Command = NoCommand

  def setOnCommand(slot: Int, command: Command): Unit =
    onSlots(slot) = command

  def setOffCommand(slot: Int, command: Command): Unit =
    offSlots(slot) = command

  def onButtonWasPressed(slot: Int): Unit = {
    onSlots(slot).execute()
    undoCommand = onSlots(slot)
  }

  def offButtonWasPressed(slot: Int): Unit = {
    offSlots(slot).execute()
    undoCommand = offSlots(slot)
  }

  def undoButtonWasPressed(): Unit = {
    undoCommand.undo()
  }

}

object RemoteControlTest extends App {

  // Receivers
  val kitchenLight = new Light("Kitchen")
  val garageDoor = new GarageDoor
  val ceilingFan = new CeilingFan("Kitchen")

  // Client creates commands and assigns receivers
  val lightOn = new LightOnCommand(kitchenLight)
  val lightOff = new LightOffCommand(kitchenLight)
  val garageDoorOpen = new GarageDoorOpenCommand(garageDoor)
  val garageDoorClosed = new GarageDoorClosedCommand(garageDoor)
  val ceilingFanHigh = new CeilingFanHighCommand(ceilingFan)

  // The Invoker
  val remote = new SimpleRemoteControl(4)

  // The Invoked and Receiver are separated by the Command pattern,
  // specifically the Command trait.
  remote.setOnCommand(0, lightOn)
  remote.setOffCommand(0, lightOff)
  remote.onButtonWasPressed(0)
  remote.offButtonWasPressed(0)

  remote.setOnCommand(1, garageDoorOpen)
  remote.setOffCommand(1, garageDoorClosed)
  remote.onButtonWasPressed(1)
  remote.offButtonWasPressed(1)

  remote.setOnCommand(2, ceilingFanHigh)
  remote.onButtonWasPressed(2)
  remote.undoButtonWasPressed()

  val macroCommand: MacroCommand = new MacroCommand(Vector[Command](garageDoorOpen, ceilingFanHigh, lightOn))
  remote.setOnCommand(3, macroCommand)
  remote.onButtonWasPressed(3)
  remote.undoButtonWasPressed()

}
