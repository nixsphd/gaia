package ie.nix.patterns.factory

import org.apache.logging.log4j.scala.Logging

trait Pizza extends Logging {

  val name: String;
  val dough: String;
  val sauce: String;
  val toppings: Array[String]

  def prepare(): Unit = {
    logger info s"Preparing name"
    logger info s"Tossing dough..."
    logger info s"Adding $sauce sauce..."
    toppings.foreach(topping => {
      logger info s"Adding $topping topping..."
    })
  }
  def bake(): Unit =
    logger info s"Bake for 25 minutes at 350"
  def cut(): Unit =
    logger info s"Cutting the pizza into diagonal slices"
  def box(): Unit =
    logger info s"Place pizza in official PizzaStore box"
}
