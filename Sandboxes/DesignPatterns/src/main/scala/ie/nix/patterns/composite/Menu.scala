package ie.nix.patterns.composite

import org.apache.logging.log4j.scala.Logging

class Menu(name: String, description: String) extends MenuComponent with Logging {

  private var menuComponents: Vector[MenuComponent] = Vector[MenuComponent]()

  override def add(menuComponent: MenuComponent): Unit = menuComponents = menuComponents :+ menuComponent
  override def remove(menuComponent: MenuComponent): Unit =
    menuComponents = menuComponents.filterNot(_ == menuComponent)
  override def getChild(i: Int): MenuComponent = menuComponents(i)

  override def getName: String = name;
  override def getDescription: String = description;

  override def print(): Unit = {
    logger info ""
    logger info s"$getName, $getDescription"
    logger info s"---------------------"
    for (menuComponent <- menuComponents) {
      menuComponent.print
    }
  }

}
