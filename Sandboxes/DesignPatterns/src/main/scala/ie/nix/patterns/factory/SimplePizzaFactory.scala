package ie.nix.patterns.factory

import ie.nix.patterns.factory.pizza.{CaliforniaCheesePizza, ChicagoCheesePizza, NYCheesePizza}

object SimplePizzaFactory {

  def createPizza(pizzaType: String): Pizza = {
    pizzaType match {
      case "NY cheese"         => new NYCheesePizza()
      case "Chicago cheese"    => new ChicagoCheesePizza()
      case "California cheese" => new CaliforniaCheesePizza()
    }
  }

}
