package ie.nix.patterns.state

import ie.nix.patterns.state.GumballMachine.{HAS_QUARTER, NO_QUARTER, SOLD_OUT}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object GumballMachineTestDrive extends App with Logging {

  val gumballMachine = new GumballMachine(5)

  logger info s"Buy a gumball from $gumballMachine"
  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  logger info s"$gumballMachine"

  assert(gumballMachine.getState == NO_QUARTER)
  assert(gumballMachine.getCount == 4)

  logger info s"Insert and eject quarter $gumballMachine"
  gumballMachine.insertQuarter()
  logger info s"$gumballMachine"
  assert(gumballMachine.getState == HAS_QUARTER)
  assert(gumballMachine.getCount == 4)

  gumballMachine.ejectQuarter()
  logger info s"$gumballMachine"
  assert(gumballMachine.getState == NO_QUARTER)
  assert(gumballMachine.getCount == 4)

  gumballMachine.turnCrank()
  logger info s"$gumballMachine"
  assert(gumballMachine.getState == NO_QUARTER)
  assert(gumballMachine.getCount == 4)

  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  assert(gumballMachine.getCount == 3)
  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  assert(gumballMachine.getCount == 2)
  gumballMachine.ejectQuarter()
  logger info s"$gumballMachine"
  assert(gumballMachine.getState == NO_QUARTER)
  assert(gumballMachine.getCount == 2)

  gumballMachine.insertQuarter()
  gumballMachine.insertQuarter()
  assert(gumballMachine.getState == HAS_QUARTER)
  assert(gumballMachine.getCount == 2)

  gumballMachine.turnCrank()
  assert(gumballMachine.getState == NO_QUARTER)
  assert(gumballMachine.getCount == 1)

  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  assert(gumballMachine.getState == SOLD_OUT)
  assert(gumballMachine.getCount == 0)

  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  assert(gumballMachine.getState == SOLD_OUT)
  assert(gumballMachine.getCount == 0)
  logger info s"$gumballMachine"

}

object NewGumballMachineTestDrive extends App with Logging {

  Random.setSeed(2L)
  val gumballMachine = new NewGumballMachine(5)

  logger info s"Buy a gumball from $gumballMachine"
  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  logger info s"$gumballMachine"

  assert(gumballMachine.getState == "NoQuarter")
  assert(gumballMachine.getCount == 4)

  logger info s"Insert and eject quarter $gumballMachine"
  gumballMachine.insertQuarter()
  logger info s"$gumballMachine"
  assert(gumballMachine.getState == "HasQuarter")
  assert(gumballMachine.getCount == 4)

  gumballMachine.ejectQuarter()
  logger info s"$gumballMachine"
  assert(gumballMachine.getState == "NoQuarter")
  assert(gumballMachine.getCount == 4)

  gumballMachine.turnCrank()
  logger info s"$gumballMachine"
  assert(gumballMachine.getState == "NoQuarter")
  assert(gumballMachine.getCount == 4)

  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  // this will be a winner so go from 4 to 2 not 3.
  assert(gumballMachine.getCount == 2)

  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  assert(gumballMachine.getCount == 1)
  gumballMachine.ejectQuarter()
  logger info s"$gumballMachine"
  assert(gumballMachine.getState == "NoQuarter")
  assert(gumballMachine.getCount == 1)

  gumballMachine.insertQuarter()
  gumballMachine.insertQuarter()
  assert(gumballMachine.getState == "HasQuarter")
  assert(gumballMachine.getCount == 1)

  gumballMachine.turnCrank()
  assert(gumballMachine.getState == "SoldOut")
  assert(gumballMachine.getCount == 0)

  gumballMachine.insertQuarter()
  gumballMachine.turnCrank()
  assert(gumballMachine.getState == "SoldOut")
  assert(gumballMachine.getCount == 0)

  logger info s"$gumballMachine"

}
