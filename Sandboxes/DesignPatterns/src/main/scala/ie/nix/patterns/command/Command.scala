package ie.nix.patterns.command

trait Command {

  def execute(): Unit
  def undo(): Unit

}

object NoCommand extends Command {
  def execute(): Unit = {}
  def undo(): Unit = {}
}
