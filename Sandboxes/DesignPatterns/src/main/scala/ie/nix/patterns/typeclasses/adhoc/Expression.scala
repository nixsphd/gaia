package ie.nix.patterns.typeclasses.adhoc

import ie.nix.patterns.typeclasses.adhoc.Expression2JSONConverter.expressionJSONConverter
import org.apache.logging.log4j.scala.Logging

// Algebraic Data Type = is a kind of composite type, i.e., a type formed by combining other types.
sealed trait Expression

// Expressions are raw data again, yeah!
case class Number(value: Int) extends Expression
case class Plus(leftExpression: Expression, rightExpression: Expression) extends Expression
case class Minus(leftExpression: Expression, rightExpression: Expression) extends Expression

object ExpressionEvaluator {
  def value(expression: Expression): Int = expression match {
    case Number(number)                         => number
    case Plus(leftExpression, rightExpression)  => value(leftExpression) + value(rightExpression)
    case Minus(leftExpression, rightExpression) => value(leftExpression) - value(rightExpression)
  }
}

object Expression2JSONConverter {
  val expressionJSONConverter = new JSONConverter[Expression] {
    override def convertToJSON(expression: Expression): JSONValue = expression match {
      case Number(number) => JSONNumber(number)
      case Plus(leftExpression, rightExpression) =>
        JSONObject(
          Map(("op" -> JSONString("+")),
              ("lhs" -> convertToJSON(leftExpression)),
              ("rhs" -> convertToJSON(rightExpression))))

      case Minus(leftExpression, rightExpression) =>
        JSONObject(
          Map(("op" -> JSONString("-")),
              ("lhs" -> convertToJSON(leftExpression)),
              ("rhs" -> convertToJSON(rightExpression))))
    }
  }

}

object TestAdHoc extends App with Logging {
  val exp = Plus(Number(1), Minus(Number(2), Number(3)))
  val expAsJSON = JSONWriter.write[Expression](exp, expressionJSONConverter)
  logger info s"expAsJSON=$expAsJSON"
}
