package ie.nix.patterns.state

import org.apache.logging.log4j.scala.Logging

class NewGumballMachine(private var count: Int) extends Logging {

  private val noQuarter = new NoQuarter(this)
  private val hasQuarter = new HasQuarter(this)
  private val sold = new Sold(this)
  private val winning = new Winning(this)
  private val soldOut = new SoldOut(this)

  private var state: GumballMachineState = if (count > 0) noQuarter else soldOut

  def getNoQuarter: GumballMachineState = noQuarter
  def getHasQuarter: GumballMachineState = hasQuarter
  def getSold: GumballMachineState = sold
  def getWinning: GumballMachineState = winning
  def getSoldOut: GumballMachineState = soldOut

  def setState(state: GumballMachineState): Unit = this.state = state
  def getState: String = state.toString

  def getCount: Int = count

  def insertQuarter(): Unit = state.insertQuarter()
  def ejectQuarter(): Unit = state.ejectQuarter()
  def turnCrank(): Unit = {
    state.turnCrank()
    state.dispense()
  }

  def releaseBall(): Unit = {
    logger info s"A gumball comes rolling out the slot..."
    if (count != 0) count = count - 1;
  }

}
