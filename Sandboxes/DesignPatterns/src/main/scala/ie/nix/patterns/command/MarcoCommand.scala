package ie.nix.patterns.command

class MacroCommand(var commands: Vector[Command]) extends Command {

  override def execute(): Unit = commands.foreach(_.execute())
  override def undo(): Unit = commands.reverse.foreach(_.undo())

}
