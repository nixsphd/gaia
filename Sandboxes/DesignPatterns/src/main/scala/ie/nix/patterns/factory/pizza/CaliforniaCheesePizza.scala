package ie.nix.patterns.factory.pizza

import ie.nix.patterns.factory.Pizza
import org.apache.logging.log4j.scala.Logging

class CaliforniaCheesePizza extends Pizza with Logging {

  val name = "California Style Cheese Pizza"
  val dough = "Medium Crust Dough"
  val sauce = "White Sauce"
  val toppings = Array[String]("Bacon")

}
