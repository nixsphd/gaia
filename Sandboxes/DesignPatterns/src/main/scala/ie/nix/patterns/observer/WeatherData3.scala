package ie.nix.patterns.observer

import org.apache.logging.log4j.scala.Logging

class WeatherData3 extends Observable[WeatherData3] {

  def temperature: Double = 0d
  def humidity: Double = 0d
  def pressure: Double = 0d

  def measurementsChanged(): Unit = {
    notifyObservers()
  }

}

object WeatherData3Demo extends App with Logging {

  val currentConditionsDisplay = new Observer[WeatherData3] {
    override def update(weatherData: WeatherData3, arg: Any): Unit =
      logger info s"currentConditionsDisplay temperature=${weatherData.temperature}," +
        s" humidity=${weatherData.humidity}, pressure=${weatherData.pressure}"
  }

  val statisticsDisplay = new Observer[WeatherData3] {
    override def update(weatherData: WeatherData3, arg: Any): Unit =
      logger info s"statisticsDisplay temperature=${weatherData.temperature}," +
        s" humidity=${weatherData.humidity}, pressure=${weatherData.pressure}"
  }

  val forecastDisplay = new Observer[WeatherData3] {
    override def update(weatherData: WeatherData3, arg: Any): Unit =
      logger info s"forecastDisplay temperature=${weatherData.temperature}," +
        s" humidity=${weatherData.humidity}, pressure=${weatherData.pressure}"
  }

  val weatherData = new WeatherData3()
  weatherData.registerObserver(currentConditionsDisplay)
  weatherData.registerObserver(statisticsDisplay)

  weatherData.measurementsChanged()

}
