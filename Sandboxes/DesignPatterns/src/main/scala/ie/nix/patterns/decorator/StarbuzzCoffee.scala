package ie.nix.patterns.decorator

import org.apache.logging.log4j.scala.Logging

/*
 The Decorator Pattern attaches additional responsibilities to an object
 dynamically. Decorators provide a flexible alternative to subclassing for
 extending functionality.
 */
object StarbuzzCoffee extends App with Logging {
  var myBeverage: Beverage = new DarkRoast("Small")
  myBeverage = new Milk(myBeverage)

  logger info s"myBeverage=${myBeverage.description}, ${myBeverage.cost}"
}

abstract class Beverage(val size: String) {
  def description: String
  def cost: Double
}

class DarkRoast(size: String) extends Beverage(size: String) {
  override def description: String = "Dark Roast"
  override def cost: Double = size match {
    case "Small" => 2.5
    case "Large" => 3.0
  }
}

// it’s vital that the decorators have the same type
abstract class CondimentDecorator(val beverage: Beverage) extends Beverage(beverage.size)

class Milk(beverage: Beverage) extends CondimentDecorator(beverage) {
  override def description: String = s"${beverage.description} wih Milk"
  override def cost: Double = beverage.cost + size match {
    case "Small" => 0.1
    case "Large" => 0.15
  }
}

class Mocka(beverage: Beverage) extends CondimentDecorator(beverage) {
  override def description: String = s"Mocka ${beverage.description}"
  override def cost: Double = beverage.cost + size match {
    case "Small" => 0.2
    case "Large" => 0.25
  }
}
