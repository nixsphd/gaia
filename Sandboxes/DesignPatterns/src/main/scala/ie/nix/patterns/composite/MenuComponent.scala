package ie.nix.patterns.composite

abstract class MenuComponent {

  def add(menuComponent: MenuComponent): Unit = throw new UnsupportedOperationException(s"Cannot add $menuComponent")
  def remove(menuComponent: MenuComponent): Unit =
    throw new UnsupportedOperationException(s"Cannot remove $menuComponent")
  def getChild(i: Int): MenuComponent = throw new UnsupportedOperationException(s"Cannot get MenuComponent $i")

  def getName: String = throw new UnsupportedOperationException
  def getDescription: String = throw new UnsupportedOperationException
  def getPrice: Double = throw new UnsupportedOperationException
  def isVegetarian: Boolean = throw new UnsupportedOperationException

  def print(): Unit = throw new UnsupportedOperationException

}
