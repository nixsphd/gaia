package ie.nix.patterns.iterator

import ie.nix.patterns.iterator.DinerMenu.MAX_ITEMS
import org.apache.logging.log4j.scala.Logging

class DinerMenu extends Logging with Menu {

  var numberOfItems: Int = 0;

  val menuItems = new Array[MenuItem](MAX_ITEMS);
  addItem("Vegetarian BLT", "(Fakin’) Bacon with lettuce & tomato on whole wheat", true, 2.99);
  addItem("BLT", "Bacon with lettuce & tomato on whole wheat", false, 2.99);
  addItem("Soup of the day", "Soup of the day, with a side of potato salad", false, 3.29);
  addItem("Hotdog", "A hot dog, with saurkraut, relish, onions, topped with cheese", false, 3.05);

  override def addItem(name: String, description: String, vegetarian: Boolean, price: Double): Unit = {
    val menuItem: MenuItem = new MenuItem(name, description, vegetarian, price)
    if (numberOfItems >= MAX_ITEMS) {
      logger error s"Sorry, menu is full! Can’t add item to menu"
    } else {
      menuItems(numberOfItems) = menuItem;
      numberOfItems = numberOfItems + 1;
    }
  }

  def getMenuItems(): Iterator[MenuItem] = menuItems.iterator

}

object DinerMenu {
  final val MAX_ITEMS: Int = 6;
}
