package ie.nix.patterns.proxy

import org.apache.logging.log4j.scala.Logging

import java.rmi.Naming
import scala.sys.exit

class GumballMonitor(machine: GumballMachine) extends Logging {

  def report(): Unit = {
    logger info s"Gumball Machine: ${machine.getLocation}"
    logger info s"Current inventory: ${machine.getCount}"
    logger info s"Current state: ${machine.getState}"
  }

}

class RemoteGumballMonitor(machine: RemoteGumballMachine) extends Logging {

  def report(): Unit = {
    logger info s"Gumball Machine: ${machine.getLocation}"
    logger info s"Current inventory: ${machine.getCount}"
    logger info s"Current state: ${machine.getState}"
  }

}

object GumballMonitor extends App with Logging {

  if (args.length < 1) { usage(); exit(1) }

  (1 to number).foreach(reports)

  def reports(number: Int): Unit = {
    val name = s"Machine$number"
    val remoteGumballMachine: RemoteGumballMachine = Naming.lookup(name) match {
      case remoteGumballMachine: RemoteGumballMachine => remoteGumballMachine
      case unexpectedType =>
        throw new RuntimeException(
          s"Unexpected remote object of type, ${unexpectedType.getClass}, expected RemoteGumballMachine")
    }
    val remoteGumballMonitor: RemoteGumballMonitor = new RemoteGumballMonitor(remoteGumballMachine)
    remoteGumballMonitor.report()
  }

  def usage(): Unit = logger info "GumballMonitor <name>"
  def number: Int = args(0).toInt;

}
