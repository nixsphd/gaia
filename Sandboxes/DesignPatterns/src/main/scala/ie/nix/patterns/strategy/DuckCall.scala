package ie.nix.patterns.strategy

class DuckCall(var quackBehavior: QuackBehavior) {
  def performDuckCall(): Unit = quackBehavior.quack()
}

object DuckCall extends App {
  val duckCall = new DuckCall(new Quack)
  duckCall.performDuckCall()
}
