package ie.nix.patterns.iterator

trait Menu {
  def addItem(name: String, description: String, vegetarian: Boolean, price: Double): Unit
  def getMenuItems(): Iterator[MenuItem]
}
