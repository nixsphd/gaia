package ie.nix.patterns.adapter

import org.apache.logging.log4j.scala.Logging

class TurkeyAdapter(var turkey: Turkey) extends Duck with Logging {
  def quack(): Unit = turkey.gobble()
  def fly(): Unit = Range(0, 5).foreach(_ => turkey.fly())

}
