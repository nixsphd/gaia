package ie.nix.patterns.adapter

trait Duck {
  def quack(): Unit
  def fly(): Unit
}
