package ie.nix.patterns.factory

import org.apache.logging.log4j.scala.Logging

object PizzaStore extends App with Logging {
  val nyStore = new NYPizzaStore();
  val chicagoStore = new ChicagoPizzaStore();
  val firstCheesePizza = nyStore.orderPizza("cheese");
  logger info s"firstCheesePizza=$firstCheesePizza"
  val secondCheesePizza = chicagoStore.orderPizza("cheese");
  logger info s"secondCheesePizza=$secondCheesePizza"
}
