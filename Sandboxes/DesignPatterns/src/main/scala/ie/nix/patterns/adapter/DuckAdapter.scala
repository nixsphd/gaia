package ie.nix.patterns.adapter

class DuckAdapter(duck: Duck) extends Turkey {
  override def gobble(): Unit = duck.quack()
  override def fly(): Unit = duck.fly()
}
