package ie.nix.patterns.singleton

object ChocolateBoiler {

  private var maybeUniqueInstance: Option[ChocolateBoiler] = None

  def instance: ChocolateBoiler = synchronized {
    if (!maybeUniqueInstance.isDefined) {
      // Lazy instantiation
      maybeUniqueInstance = Some(new ChocolateBoiler)
    }
    maybeUniqueInstance.get
  }

}

class ChocolateBoiler(private var empty: Boolean = true, private var boiled: Boolean = false) {

  def fill(): Unit = {
    if (isEmpty) {
      empty = false
      boiled = false
      // fill the boiler with a milk/chocolate mixture
    }
  }

  def drain(): Unit = {
    if (!isEmpty && isBoiled) { // drain the boiled milk and chocolate
      empty = true
    }
  }

  def boil(): Unit = {
    if (!isEmpty && !isBoiled) { // bring the contents to a boil
      boiled = true
    }
  }

  def isEmpty: Boolean = empty

  def isBoiled: Boolean = boiled
}
