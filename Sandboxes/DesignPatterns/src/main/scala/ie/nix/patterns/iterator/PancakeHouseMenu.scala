package ie.nix.patterns.iterator

import org.apache.logging.log4j.scala.Logging

class PancakeHouseMenu extends Menu with Logging {

  var menuItems = Vector[MenuItem]();
  addItem("K&B’s Pancake Breakfast", "Pancakes with scrambled eggs, and toast", true, 2.99)
  addItem("Regular Pancake Breakfast", "Pancakes with fried eggs, sausage", false, 2.99)
  addItem("Blueberry Pancakes", "Pancakes made with fresh blueberries", true, 3.49)
  addItem("Waffles", "Waffles, with your choice of blueberries or strawberries", true, 3.59);

  def addItem(name: String, description: String, vegetarian: Boolean, price: Double): Unit = {
    val menuItem = new MenuItem(name, description, vegetarian, price)
    menuItems = menuItems :+ menuItem
  }

  def getMenuItems(): Iterator[MenuItem] = menuItems.iterator

}
