package ie.nix.patterns.strategy

class ModelDuck extends Duck(new FlyNoWay(), new Quack()) {
  def display(): Unit = logger info "I’m a model duck"
}

object ModelDuckSimulator extends App {
  val modelDuck = new ModelDuck
  modelDuck.performQuack()
  modelDuck.performFly()
  modelDuck.setFlyBehavior(new FlyRocketPowered())
  modelDuck.performFly()

}
