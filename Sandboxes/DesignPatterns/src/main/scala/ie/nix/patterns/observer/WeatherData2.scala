package ie.nix.patterns.observer

import org.apache.logging.log4j.scala.Logging

class WeatherData2 extends java.util.Observable {

  def temperature: Double = 0d
  def humidity: Double = 0d
  def pressure: Double = 0d

  def measurementsChanged(): Unit = {
    setChanged()
    notifyObservers()
  }

}

object WeatherData2Demo extends App with Logging {

  val currentConditionsDisplay = new java.util.Observer {
    override def update(observable: java.util.Observable, arg: Any): Unit = observable match {
      case weatherData2: WeatherData2 =>
        logger info s"currentConditionsDisplay temperature=${weatherData2.temperature}," +
          s" humidity=${weatherData2.humidity}, pressure=${weatherData2.pressure}"
    }
  }

  val statisticsDisplay = new java.util.Observer {
    override def update(observable: java.util.Observable, arg: Any): Unit = observable match {
      case weatherData2: WeatherData2 =>
        logger info s"statisticsDisplay temperature=${weatherData2.temperature}," +
          s" humidity=${weatherData2.humidity}, pressure=${weatherData2.pressure}"
    }
  }

  val forecastDisplay = new java.util.Observer {
    override def update(observable: java.util.Observable, arg: Any): Unit = observable match {
      case weatherData2: WeatherData2 =>
        logger info s"forecastDisplay temperature=${weatherData2.temperature}," +
          s" humidity=${weatherData2.humidity}, pressure=${weatherData2.pressure}"
    }
  }

  val weatherData = new WeatherData2()
  weatherData.addObserver(currentConditionsDisplay)
  weatherData.addObserver(statisticsDisplay)

  weatherData.measurementsChanged()

}
