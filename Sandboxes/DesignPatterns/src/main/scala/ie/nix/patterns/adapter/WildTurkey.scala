package ie.nix.patterns.adapter

import org.apache.logging.log4j.scala.Logging

class WildTurkey extends Turkey with Logging {
  def gobble(): Unit = logger info s"Gobble gobble"
  def fly(): Unit = logger info s"I’m flying a short distance"

}
