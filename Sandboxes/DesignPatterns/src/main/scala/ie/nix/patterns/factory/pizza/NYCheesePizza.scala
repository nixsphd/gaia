package ie.nix.patterns.factory.pizza

import ie.nix.patterns.factory.Pizza
import org.apache.logging.log4j.scala.Logging

class NYCheesePizza extends Pizza with Logging {

  val name = "NY Style Sauce and Cheese Pizza"
  val dough = "Thin Crust Dough"
  val sauce = "Marinara Sauce"
  val toppings = Array[String]("Grated Reggiano Cheese")

  override def cut(): Unit = logger info s"Cutting the pizza into square slices"

}
