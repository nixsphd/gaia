package ie.nix.patterns.strategy

import org.apache.logging.log4j.scala.Logging

abstract class Duck(var flyBehavior: FlyBehavior, var quackBehavior: QuackBehavior) extends Logging {

  def display(): Unit

  def performFly(): Unit = flyBehavior.fly()

  def performQuack(): Unit = quackBehavior.quack()

  def swim(): Unit =
    logger info "All ducks float, even decoys!"

  def setFlyBehavior(flyBehavior: FlyBehavior): Unit = {
    this.flyBehavior = flyBehavior
  }

  def setQuackBehavior(quackBehavior: QuackBehavior): Unit = {
    this.quackBehavior = quackBehavior
  }

}
