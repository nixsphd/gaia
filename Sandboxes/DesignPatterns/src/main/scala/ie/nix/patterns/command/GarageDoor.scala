package ie.nix.patterns.command

import org.apache.logging.log4j.scala.Logging

class GarageDoor extends Logging {
  def open(): Unit = logger info s"GarageDoor OPEN"
  def close(): Unit = logger info s"GarageDoor CLOSE"
}

object GarageDoor {

  class GarageDoorOpenCommand(garageDoor: GarageDoor) extends Command {
    override def execute(): Unit = garageDoor.open()
    override def undo(): Unit = garageDoor.close()
  }
  class GarageDoorClosedCommand(garageDoor: GarageDoor) extends Command {
    override def execute(): Unit = garageDoor.close()
    override def undo(): Unit = garageDoor.open()
  }

}
