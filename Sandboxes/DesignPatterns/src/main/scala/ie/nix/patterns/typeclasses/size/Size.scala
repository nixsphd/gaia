package ie.nix.patterns.typeclasses.size

import org.apache.logging.log4j.scala.Logging

trait Size[S] {
  def zero: S
  def isPositive(s: S): Boolean
  def plus(s1: S, s2: S): S
  def minus(s1: S, s2: S): S
  def sum(sizes: Vector[S]): S = sizes match {
    case Vector()     => zero
    case head +: tail => plus(head, sum(tail))
  }
}

case class Size1D(x: Int)
case class Size2D(x: Int, y: Int)

object Size {
  implicit val size1D = new Size[Size1D] {
    override def zero: Size1D = Size1D(0)
    override def isPositive(s: Size1D): Boolean = s.x >= 0
    override def plus(s1: Size1D, s2: Size1D): Size1D = Size1D(s1.x + s2.x)
    override def minus(s1: Size1D, s2: Size1D): Size1D = Size1D(s1.x - s2.x)

  }
  implicit val size2D = new Size[Size2D] {
    override def zero: Size2D = Size2D(0, 0)
    override def isPositive(s: Size2D): Boolean = s.x >= 0 && s.y >= 0
    override def plus(s1: Size2D, s2: Size2D): Size2D = Size2D(s1.x + s2.x, s1.y + s2.y)
    override def minus(s1: Size2D, s2: Size2D): Size2D = Size2D(s1.x - s2.x, s1.y - s2.y)

  }
}

trait Item[S] {
  def size: S
}
case class Item1D(size: Size1D) extends Item[Size1D]
case class Item2D(size: Size2D) extends Item[Size2D]
case class DynamicItem1D(size: Size1D, arrivalTime: Int, departureTime: Int) extends Item[Size1D]
case class DynamicItem2D(size: Size2D, arrivalTime: Int, departureTime: Int) extends Item[Size2D]

object ItemUtils {
  def sum[S: Size](items: Vector[Item[S]]): S =
    implicitly[Size[S]].sum(items.map(_.size))
}

trait Bin[S] {
  implicit def size: Size[S] // <= This is a bit ugly :(
  def capacity: S
  def items: Vector[Item[S]]
  def usedCapacity: S = ItemUtils.sum(items)
  def freeCapacity: S = implicitly[Size[S]].minus(capacity, usedCapacity)
  def canFit(item: Item[S]): Boolean =
    implicitly[Size[S]].isPositive(implicitly[Size[S]].minus(freeCapacity, item.size))
}
case class BinItem1D(capacity: Size1D, items: Vector[Item1D])(implicit val size: Size[Size1D]) extends Bin[Size1D]
case class BinItem2D(capacity: Size2D, items: Vector[Item2D])(implicit val size: Size[Size2D]) extends Bin[Size2D]
case class BinDynamicItem1D(capacity: Size1D, items: Vector[DynamicItem1D])(implicit val size: Size[Size1D])
    extends Bin[Size1D]
case class BinDynamicItem2D(capacity: Size2D, items: Vector[DynamicItem2D])(implicit val size: Size[Size2D])
    extends Bin[Size2D]

object TestSize extends App with Logging {

  // Can't fit
  val capacity = Size1D(10)
  val items = Vector(Item1D(Size1D(2)), Item1D(Size1D(4)))
  val bin = BinItem1D(capacity, items)

  val item = Item1D(Size1D(5))
  val canFit = bin.canFit(item)
  logger info s"bin=$bin, canFit=$canFit, item=$item"

  // Can fit, just
  val item2 = Item1D(Size1D(4))
  val canFit2 = bin.canFit(item2)
  logger info s"bin=$bin, canFit=$canFit2, item=$item2"

  // DynamicItem2D
  val capacity3 = Size2D(10, 10)
  val items3 = Vector(DynamicItem2D(Size2D(2, 3), 0, 5), DynamicItem2D(Size2D(4, 5), 2, 7))
  val bin3 = BinDynamicItem2D(capacity3, items3)

  val item3 = Item2D(Size2D(5, 5))
  val canFit3 = bin3.canFit(item3)
  logger info s"bin=$bin3, canFit=$canFit3, item=$item3"

}
