package ie.nix.patterns.adapter

import org.apache.logging.log4j.scala.Logging

class MallardDuck extends Duck with Logging {
  def quack(): Unit = logger info s"Quack"
  def fly(): Unit = logger info s"I'm flying"
}
