package ie.nix.patterns.iterator

import org.apache.logging.log4j.scala.Logging

class Waitress(val pancakeHouseMenu: PancakeHouseMenu, val dinerMenu: DinerMenu) extends Logging {

  def printMenu(): Unit = {
    printMenu(pancakeHouseMenu)
    printMenu(dinerMenu)
  }

  def printMenu(menu: Menu): Unit = {
    for (menuItem: MenuItem <- menu.getMenuItems()) {
      logger info s"${menuItem.getName} ${menuItem.getPrice}  ${menuItem.getDescription}"
    }
  }
}
