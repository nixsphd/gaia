package ie.nix.patterns.template

import org.apache.logging.log4j.scala.Logging

class Tea extends Logging {

  def prepareRecipe(): Unit = {
    boilWater();
    steepTeaBag();
    pourInCup();
    addLemon();
  }

  def boilWater(): Unit = logger info "Boiling water"
  def steepTeaBag(): Unit = logger info "Steeping the tea"
  def addLemon(): Unit = logger info "Adding Lemon"
  def pourInCup(): Unit = logger info "Pouring into cup"

}
