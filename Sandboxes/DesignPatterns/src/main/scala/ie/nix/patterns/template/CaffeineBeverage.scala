package ie.nix.patterns.template

import org.apache.logging.log4j.scala.Logging

abstract class CaffeineBeverage extends Logging {

  // This is the Template Method
  final def prepareRecipe(): Unit = {
    boilWater()
    brew()
    pourInCup()
    if (customerWantsCondiments()) addCondiments()

  }

  def boilWater(): Unit = logger info "Boiling water"
  def brew(): Unit
  def pourInCup(): Unit = logger info "Pouring into cup"
  // The hook...
  def customerWantsCondiments(): Boolean = true
  def addCondiments(): Unit

}

class SuperTea extends CaffeineBeverage with Logging {
  override def brew(): Unit = logger info "Steeping the tea"
  override def addCondiments(): Unit = logger info "Adding lemon"
}

class SuperCoffee extends CaffeineBeverage with Logging {
  override def brew(): Unit = logger info "Dripping Coffee through filter"
  override def addCondiments(): Unit = logger info "Adding sugar and milk"
}

object BeverageTestDrive extends App with Logging {
  val teaHook: SuperTea = new SuperTea()
  val coffeeHook: SuperCoffee = new SuperCoffee()

  logger info "Making tea..."
  teaHook.prepareRecipe();

  logger info "Making coffee..."
  coffeeHook.prepareRecipe()
}
