package ie.nix.patterns.typeclasses.adhoc

sealed trait JSONValue {}
case class JSONObject(entries: Map[String, JSONValue]) extends JSONValue
case class JSONString(string: String) extends JSONValue
case class JSONNumber(number: Int) extends JSONValue
case object JSONNull extends JSONValue

object JSONWriter {

  def write(jsonNValue: JSONValue): String = jsonNValue match {
    case JSONObject(entries) =>
      entries.map(keyAndValue => s"${keyAndValue._1} : ${write(keyAndValue._2)}").mkString("{", ", ", "}")
    case JSONNumber(number) => s"$number"
    case JSONString(string) => string
    case JSONNull           => s"Null"
  }

  def write[A](value: A, converter: JSONConverter[A]): String =
    write(converter.convertToJSON(value))
}

// JSONConverter is the type class
trait JSONConverter[A] {
  def convertToJSON(a: A): JSONValue
}
