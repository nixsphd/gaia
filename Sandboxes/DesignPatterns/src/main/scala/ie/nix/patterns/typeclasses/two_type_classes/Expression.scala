package ie.nix.patterns.typeclasses.two_type_classes

import org.apache.logging.log4j.scala.Logging

// Algebraic Data Type = is a kind of composite type, i.e., a type formed by combining other types.
sealed trait Expression

// Expressions are raw data again, yeah!
case class Number(value: Int) extends Expression
case class Plus(leftExpression: Expression, rightExpression: Expression) extends Expression
case class Minus(leftExpression: Expression, rightExpression: Expression) extends Expression

object ExpressionEvaluator {
  def value(expression: Expression): Int = expression match {
    case Number(number)                         => number
    case Plus(leftExpression, rightExpression)  => value(leftExpression) + value(rightExpression)
    case Minus(leftExpression, rightExpression) => value(leftExpression) - value(rightExpression)
  }
}

object TestImplicits extends App with Logging {

  implicit val expressionJSONConverter = new JSON[Expression] {
    override def json(expression: Expression): JSONValue = expression match {
      case Number(number) => JSONNumber(number)
      case Plus(leftExpression, rightExpression) =>
        JSONObject(Map(("op" -> JSONString("+")), ("lhs" -> json(leftExpression)), ("rhs" -> json(rightExpression))))

      case Minus(leftExpression, rightExpression) =>
        JSONObject(Map(("op" -> JSONString("-")), ("lhs" -> json(leftExpression)), ("rhs" -> json(rightExpression))))
    }
  }

  val exp = Plus(Number(1), Minus(Number(2), Number(3)))
  val expAsJSON = JSONWriter.write[Expression](exp)
  logger info s"expAsJSON=$expAsJSON"
}
