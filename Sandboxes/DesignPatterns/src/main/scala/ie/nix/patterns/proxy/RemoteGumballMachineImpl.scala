package ie.nix.patterns.proxy

import java.rmi.registry.LocateRegistry
import java.rmi.server.UnicastRemoteObject
import java.rmi.{Naming, RemoteException}
import scala.sys.exit

class RemoteGumballMachineImpl(machine: GumballMachine) extends UnicastRemoteObject with RemoteGumballMachine {

  @throws(classOf[RemoteException])
  override def getLocation: String = machine.getLocation

  @throws(classOf[RemoteException])
  override def getCount: Int = machine.getCount

  @throws(classOf[RemoteException])
  override def getState: GumballMachineState = machine.getState

}

object Machines extends App {

  if (args.length < 2) { usage(); exit(1) }

  LocateRegistry.createRegistry(1099)

  (1 to number).foreach(createMachine(_, count))

  def usage(): Unit = System.out.println("RemoteGumballMachineImpl <number> <count>")
  def number: Int = args(0).toInt;
  def count: Int = args(1).toInt;

  def createMachine(number: Int, count: Int): Unit = {
    val name = s"Machine$number"
    val gumballMachine: GumballMachine = new GumballMachine(name, count);
    val service: RemoteGumballMachine = new RemoteGumballMachineImpl(gumballMachine);
    Naming.rebind(name, service);
  }

}
