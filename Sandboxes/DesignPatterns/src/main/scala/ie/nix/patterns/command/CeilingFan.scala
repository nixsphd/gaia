package ie.nix.patterns.command

import ie.nix.patterns.command.CeilingFan.{HIGH, LOW, MEDIUM, OFF}
import org.apache.logging.log4j.scala.Logging

class CeilingFan(val location: String, var speed: Int = OFF) extends Logging {

  def high(): Unit = speed(HIGH)
  def medium(): Unit = speed(MEDIUM)
  def low(): Unit = speed(LOW)
  def off(): Unit = speed(OFF)

  def speed(speed: Int): Unit = {
    this.speed = speed
    logger info s"$location ceiling fan speed set to $speed"
  }

}

object CeilingFan {

  final val HIGH = 3;
  final val MEDIUM = 2;
  final val LOW = 1;
  final val OFF = 0;

  abstract class CeilingFanCommand(ceilingFan: CeilingFan) extends Command {
    var previousSpeed = OFF
    override def execute(): Unit = previousSpeed = ceilingFan.speed
    override def undo(): Unit = previousSpeed match {
      case HIGH   => ceilingFan.high()
      case MEDIUM => ceilingFan.medium()
      case LOW    => ceilingFan.low()
      case OFF    => ceilingFan.off()
    }
  }

  class CeilingFanHighCommand(ceilingFan: CeilingFan) extends CeilingFanCommand(ceilingFan) {
    override def execute(): Unit = {
      super.execute()
      ceilingFan.high()
    }
  }

  class CeilingFanMediumCommand(ceilingFan: CeilingFan) extends CeilingFanCommand(ceilingFan) {
    override def execute(): Unit = {
      super.execute()
      ceilingFan.medium()
    }
  }

  class CeilingFanLowCommand(ceilingFan: CeilingFan) extends CeilingFanCommand(ceilingFan) {
    override def execute(): Unit = {
      super.execute()
      ceilingFan.low()
    }
  }

  class CeilingFanOffCommand(ceilingFan: CeilingFan) extends CeilingFanCommand(ceilingFan) {
    override def execute(): Unit = {
      super.execute()
      ceilingFan.off()
    }
  }

}
