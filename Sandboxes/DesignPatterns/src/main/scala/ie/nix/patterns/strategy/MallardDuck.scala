package ie.nix.patterns.strategy

class MallardDuck extends Duck(new FlyWithWings(), new Quack()) {

  def display(): Unit = logger info "I'm a real Mallard duck"

}

object MiniDuckSimulator extends App {
  val mallard = new MallardDuck
  mallard.performQuack()
  mallard.performFly()
}
