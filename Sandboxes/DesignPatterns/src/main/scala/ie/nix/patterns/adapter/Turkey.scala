package ie.nix.patterns.adapter

trait Turkey {
  def gobble(): Unit
  def fly(): Unit
}
