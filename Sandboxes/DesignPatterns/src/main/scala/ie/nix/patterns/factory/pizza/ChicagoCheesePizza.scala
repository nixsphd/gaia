package ie.nix.patterns.factory.pizza

import ie.nix.patterns.factory.Pizza
import org.apache.logging.log4j.scala.Logging

class ChicagoCheesePizza extends Pizza with Logging {

  val name = "Chicago Style Deep Dish Cheese Pizza"
  val dough = "Extra Thick Crust Dough"
  val sauce = "Plum Tomato Sauce"
  val toppings = Array[String]("Shredded Mozzarella")
}
