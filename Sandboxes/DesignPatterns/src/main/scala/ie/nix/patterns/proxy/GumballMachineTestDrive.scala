package ie.nix.patterns.proxy

import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

object GumballMachineTestDrive extends App with Logging {

  if (args.length < 2) { usage(); exit(1) }

  val gumballMachine: GumballMachine = new GumballMachine(name, count);
  val monitor: GumballMonitor = new GumballMonitor(gumballMachine);

  // rest of test code here
  monitor.report();

  def usage(): Unit = logger info "GumballMachine <name> <inventory>"
  def name: String = args(0);
  def count: Int = args(1).toInt;

}
