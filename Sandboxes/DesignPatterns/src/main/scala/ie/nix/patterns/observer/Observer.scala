package ie.nix.patterns.observer

trait Observer[T] {

  def update(observable: T, arg: Any): Unit

}
