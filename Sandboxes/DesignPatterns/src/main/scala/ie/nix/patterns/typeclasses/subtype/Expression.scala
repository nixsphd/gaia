package ie.nix.patterns.typeclasses.subtype

import org.apache.logging.log4j.scala.Logging

// Algebraic Data Type = is a kind of composite type, i.e., a type formed by combining other types.
sealed trait Expression extends JSONConvertible

// Originally these were just data until Expression extended JSONConvertible
case class Number(value: Int) extends Expression {
  override def convertToJSON: JSONValue = JSONNumber(value)
}
case class Plus(leftExpression: Expression, rightExpression: Expression) extends Expression {
  override def convertToJSON: JSONValue =
    JSONObject(
      Map(("op" -> JSONString("+")), ("lhs" -> leftExpression.convertToJSON), ("rhs" -> rightExpression.convertToJSON)))
}
case class Minus(leftExpression: Expression, rightExpression: Expression) extends Expression {
  override def convertToJSON: JSONValue =
    JSONObject(
      Map(("op" -> JSONString("-")), ("lhs" -> leftExpression.convertToJSON), ("rhs" -> rightExpression.convertToJSON)))

}

object ExpressionEvaluator {
  def value(expression: Expression): Int = expression match {
    case Number(number)                         => number
    case Plus(leftExpression, rightExpression)  => value(leftExpression) + value(rightExpression)
    case Minus(leftExpression, rightExpression) => value(leftExpression) - value(rightExpression)
  }
}

object TestSubType extends App with Logging {
  val exp = Plus(Number(1), Minus(Number(2), Number(3)))
  val expAsJSON = JSONWriter.write(exp)
  logger info s"expAsJSON=$expAsJSON"
}
