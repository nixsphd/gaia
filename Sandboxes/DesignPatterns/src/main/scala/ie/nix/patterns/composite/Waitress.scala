package ie.nix.patterns.composite

class Waitress(var allMenus: MenuComponent) {

  def printMenu(): Unit = allMenus.print

}
