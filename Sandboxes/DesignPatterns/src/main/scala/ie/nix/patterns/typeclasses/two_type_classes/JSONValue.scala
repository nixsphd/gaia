package ie.nix.patterns.typeclasses.two_type_classes

sealed trait JSONValue {}
case class JSONObject(entries: Map[String, JSONValue]) extends JSONValue
case class JSONString(string: String) extends JSONValue
case class JSONNumber(number: Int) extends JSONValue
case object JSONNull extends JSONValue

object JSONWriter {

  def write(jsonNValue: JSONValue): String = jsonNValue match {
    case JSONObject(entries) =>
      entries.map(keyAndValue => s"${keyAndValue._1} : ${write(keyAndValue._2)}").mkString("{", ", ", "}")
    case JSONNumber(number) => s"$number"
    case JSONString(string) => string
    case JSONNull           => s"Null"
  }

  def write[A: JSON](value: A): String = write(implicitly[JSON[A]].json(value))

}

// JSONConverter is renamed to simply json
trait JSON[A] {
  def json(a: A): JSONValue
}
