package ie.nix.patterns.command

import org.apache.logging.log4j.scala.Logging

class Light(name: String) extends Logging {
  def on(): Unit = logger info s"$name light ON"
  def off(): Unit = logger info s"$name light OFF"
}

object Light {
  // Light is the receiver
  class LightOnCommand(light: Light) extends Command {
    override def execute(): Unit = light.on()
    override def undo(): Unit = light.off()
  }
  // Light is the receiver
  class LightOffCommand(light: Light) extends Command {
    def execute(): Unit = light.off()
    override def undo(): Unit = light.on()
  }

}
