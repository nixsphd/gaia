package ie.nix.scala

import ie.nix.scala.ImmutableSomething.ImmutableSomethingImpl
import org.apache.logging.log4j.scala.Logging

object ImmutableWithMixin extends App with Logging {
    val value = "1 billion"
    val behaviour = new BehaviourImpl(value) with BehaviourMixin
    logger info s"behaviour=$behaviour"
    val newValue = "2 billion"
    val newBehaviour = behaviour.update(newValue)
    logger info s"newBehaviour=$newBehaviour"
    val newValue2 = "3 billion"
    val newBehaviour2 = newBehaviour.update(newValue2)
    logger info s"newBehaviour2=$newBehaviour2"
}

trait Behaviour {
    def getValue: String
    def update(newValue: String): Behaviour
}

class BehaviourImpl(val behaviourValue: String) extends Behaviour with Logging {
    override def toString(): String = {
        s"~behaviourValue=$behaviourValue"
    }
    override def update(newBehaviourValue: String): Behaviour = {
        updateImpl(newBehaviourValue)
    }
    def updateImpl(newBehaviourValue: String): Behaviour = {
        logger info s"~BehaviourImpl update to $newBehaviourValue"
        new BehaviourImpl(newBehaviourValue)
    }
    override def getValue: String = behaviourValue
}

trait BehaviourMixin extends Logging {
    behaviour: BehaviourImpl =>
    
    // Or keep it abstract?
    def mixinValue: Int = 0
    
    override def toString(): String = {
        s"~super=${super.toString()}, mixinValue=$mixinValue"
    }
    abstract override def update(newBehaviourValue: String): Behaviour = {
        val newMixinValue = mixinValue + 1
        val newBehaviour = behaviour.updateImpl(newBehaviourValue)
        logger info s"~BehaviourMixin update to $newBehaviourValue, $newMixinValue, $newBehaviour"
        new BehaviourImpl(newBehaviourValue) with BehaviourMixin {
            override def mixinValue: Int = newMixinValue
        }
    }
}

// ===================================================================== //

object ImmutableSomethingElse extends App with Logging {
    
    val immutableSomething = ImmutableSomething()
    val missingValue = immutableSomething.get(0)
    logger info s"~missingValue=$missingValue"
    
    val immutableSomethingWithAddedValue = immutableSomething.add(10d)
    logger info s"~immutableSomethingWithAddedValue=$immutableSomethingWithAddedValue"
    
//    val immutableSomethingElse = ImmutableSomething(Vector())
//    val immutableSomethingElse = new ImmutableSomething(Vector())
//    immutableSomething.copy(Vector())
    
    val immutableSomethingWithChatty = ChattyImmutableSomething()
    logger info s"~${immutableSomethingWithChatty.sayHello}"
    val updatedImmutableSomethingWithChatty = immutableSomethingWithChatty.add(20)
    logger info s"~updatedImmutableSomethingWithChatty=$updatedImmutableSomethingWithChatty"
    val isChatty = updatedImmutableSomethingWithChatty.isInstanceOf[Chatty]
    logger info s"~updatedImmutableSomethingWithChatty isChatty=$isChatty"
//    logger info s"~${updatedImmutableSomethingWithChatty.sayHello}"
    
    
}


trait ImmutableSomething {
    
    def get(index: Int): Double
    def add(value: Double): ImmutableSomething
}

object ImmutableSomething {
    
    def apply(): ImmutableSomething = {
        new ImmutableSomethingImpl(Vector())
    }
    
    class ImmutableSomethingImpl private[ImmutableSomething](val vector: Vector[Double]) extends ImmutableSomething {
        
        def this() =
            this(Vector())
        
        override def toString: String = vector.toString
    
        override def get(index: Int): Double =
            if (index < vector.size) vector(index) else Double.NaN
        
        override def add(value: Double): ImmutableSomethingImpl =
            new ImmutableSomethingImpl(vector :+ value)
    }
    
}

trait Chatty extends Logging {
    self : ImmutableSomething =>
    def sayHello(): Unit = logger info s"Hello! I have an ${self.get(0)}"
}

case class ChattyImmutableSomething() extends ImmutableSomethingImpl with Chatty


