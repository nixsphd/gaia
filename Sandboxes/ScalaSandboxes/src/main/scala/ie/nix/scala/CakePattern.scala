package ie.nix.scala

import org.apache.logging.log4j.scala.Logging

// ===================================================================== //

object NixsCakePattern extends App {
    val a1b1 = new AB("A1", "B1")
    a1b1.a.a()
    a1b1.b.b()
    
    val a2 = a1b1.a.setName("A2")
    val a2b1 = a1b1.setA(a2)
    a2b1.a.a()
    a2b1.b.b()
    
}

trait TraitA extends Logging {
    
    val a: A
    
    def A(name: String) = new AImpl(name)
    
    trait A {
        def name(): String
        def setName(name: String): A
        def a(): Unit
    }
    
    class AImpl(name: String) extends A {
        override def a(): Unit = {
            logger info s"~AImpl($name)"
        }
        override def setName(name: String): A = {
            new AImpl(name)
        }
        override def name(): String = name
    }
}

trait TraitB extends Logging {
    traitA: TraitA =>
    
    val b: B
    
    def B(name: String) = new BImpl(name)
    
    trait B {
        def name(): String
        def b (): Unit
    }
    
    class BImpl(name: String) extends B {
        def b(): Unit = {
            logger info s"~BImpl($name)"
            traitA.a.a()
        }
        override def name(): String = name
    }
    
}

//object AB extends TraitA with TraitB  {
//    // we can move the instantiation (and configuration) of the services to the ComponentRegistry module.
//    val a = A("A1")
//    val b = B
//}

class AB(aName: String, bName: String) extends TraitA with TraitB {
    override val a: A = A(aName)
    override val b: B = B(bName)
    def setA(newA: A) : AB = new AB(newA.name(), b.name())
}

// ===================================================================== //

// http://jonasboner.com/real-world-scala-dependency-injection-di/

object CakePattern extends App {
    
    val componentRegistry = ComponentRegistry
    val username = "nix"
    val password = "abc"
    componentRegistry.userService.create(username, password)
    val user = componentRegistry.userService.authenticate(username, password)
    componentRegistry.userService.delete(user)
}

class User(val username: String, val password: String) {
    override def toString: String = s"User($username)"
}

//class UserRepository {
//    def authenticate(user: User): User = {
//        println("UserRepository authenticating user: " + user)
//        user
//    }
//    def create(user: User)  =
//        println("UserRepository creating user: " + user)
//
//    def delete(user: User)  =
//        println("UserRepository deleting user: " + user)
//}

trait UserServiceComponent {
    // this: Foo with Bar with Baz =>
    this: UserRepositoryComponent =>
    
//    val userService = new UserService
    val userService: UserService // replace with and abstract member field
    
    def UserService = new UserServiceImpl
    
    trait UserService {
        def authenticate(username: String, password: String): User
        def create(username: String, password: String): Unit
        def delete(user: User): Unit
    }
    
    class UserServiceImpl extends UserService {
        def authenticate(username: String, password: String): User =
            userRepository.authenticate(new User(username, password))
        def create(username: String, password: String) =
            userRepository.create(new User(username, password))
        def delete(user: User) = userRepository.delete(user)
    }
}

trait UserRepositoryComponent {

//    val userRepository = new UserRepository
    val userRepository: UserRepository // replace with and abstract member field
    
    def AUserRepository = new UserRepositoryImplA
    def BUserRepository = new UserRepositoryImplB
    
    trait UserRepository {
        def authenticate(user: User): User
        def create(user: User): Unit
        def delete(user: User): Unit
    }
    
    class UserRepositoryImplA extends UserRepository {
        def authenticate(user: User): User = {
            println("authenticating user: " + user)
            user
        }
        def create(user: User) = println("creating user: " + user)
        def delete(user: User) = println("deleting user: " + user)
    }
    
    class UserRepositoryImplB extends UserRepository {
        def authenticate(user: User): User = {
            println("authenticating user: " + user)
            user
        }
        def create(user: User) = println("creating user: " + user)
        def delete(user: User) = println("deleting user: " + user)
    }
}

// ===================================================================== //

//object ComponentRegistry extends
//    UserServiceComponent with
//    UserRepositoryComponent
object ComponentRegistry extends
    UserServiceComponent with
    UserRepositoryComponent
{
    // we can move the instantiation (and configuration) of the services to the ComponentRegistry module.
    val userRepository = AUserRepository
    val userService = UserService
}

// ===================================================================== //

trait TestEnvironment extends
    UserServiceComponent with
    UserRepositoryComponent
{
    val userRepository = mock(classOf[UserRepository])
    val userService = mock(classOf[UserService])
    
    def mock(classOf: Class[UserRepository]): UserRepository
    def mock(classOf: Class[UserService]): UserService
}

// ===================================================================== //
// ===================================================================== //

import scala.language.reflectiveCalls

object CakePattern2 extends App {
    
    val client = new Client(Config)
}

trait OnOffDevice {
    def on(): Unit
    def off(): Unit
}

trait SensorDevice {
    def isCoffeePresent: Boolean
}

class Heater extends OnOffDevice {
    def on(): Unit = println("heater.on")
    def off(): Unit = println("heater.off")
}

class PotSensor extends SensorDevice {
    def isCoffeePresent = true
}

class Warmer(env: {
    val potSensor: SensorDevice
    val heater: OnOffDevice
}) {
    def trigger(): Unit = {
        if (env.potSensor.isCoffeePresent) env.heater.on
        else env.heater.off
    }
}


class Client(env: Config.X) {
    env.warmer.trigger
}

object Config {
    type X = { val warmer: Warmer }
    
    lazy val potSensor = new PotSensor
    lazy val heater = new Heater
    lazy val warmer = new Warmer(this) // this is where injection happens
}

// ===================================================================== //

// don't really like that the actual wiring (importing the implicit declarations) is scattered and
// tangled with the application code.

// Important to define this first!!!
object Services {
    implicit val potSensor = new PotSensor
    implicit val heater = new Heater
}

import ie.nix.scala.Services._

object CakePattern3 extends App {
    
    val warmer2 = new Warmer2
    warmer2.trigger
}

// service declaring two dependencies that it wants injected
class Warmer2(implicit val sensor: SensorDevice, implicit val onOff: OnOffDevice) {
    
    def trigger(): Unit = {
        if (sensor.isCoffeePresent) onOff.on
        else onOff.off
    }
}

// ===================================================================== //

object CakePattern4 extends App {
    val warmer = CoffeeMachine.warmer
    warmer.trigger
}

trait OnOffDeviceComponent {
    val onOff: OnOffDevice
    trait OnOffDevice {
        def on(): Unit
        def off(): Unit
    }
}
trait SensorDeviceComponent {
    val sensor: SensorDevice
    trait SensorDevice {
        def isCoffeePresent: Boolean
    }
}

trait OnOffDeviceComponentImpl extends OnOffDeviceComponent {
    class Heater extends OnOffDevice {
        def on(): Unit = println("heater.on")
        def off(): Unit = println("heater.off")
    }
}
trait SensorDeviceComponentImpl extends SensorDeviceComponent {
    class PotSensor extends SensorDevice {
        def isCoffeePresent = true
    }
}

trait WarmerComponentImpl {
    this: SensorDeviceComponent with OnOffDeviceComponent =>
    class Warmer {
        def trigger(): Unit = {
            if (sensor.isCoffeePresent) onOff.on
            else onOff.off
        }
    }
}

object CoffeeMachine extends
    OnOffDeviceComponentImpl with
    SensorDeviceComponentImpl with
    WarmerComponentImpl {
    
    val onOff = new Heater
    val sensor = new PotSensor
    val warmer = new Warmer
}