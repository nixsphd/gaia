package ie.nix.scala

object Run extends App {
    
    TesCaseObject.
}

case class A(value: Int) {}

case object TesCaseObject {
    def transform(a: A): A = {
        a.copy(value=a.value+1)
    }
}
