package ie.nix.scala

import org.apache.logging.log4j.scala.Logging


object MixinDemo extends App with Logging {
    
    val actualClass = new ActualClass2
    val actualClassWithMixin1 = new ActualClass2() with Mixin2
    val actualClassWithMixin1AndMixin2 = new ActualClass2() with Mixin3 with Mixin3
    
    def inheritanceOrderDemo() = {
        logger info s"~actualClass.sayHi()"
        actualClass.sayHi()
        
        logger info s"~actualClass.everyoneSayHi()"
        actualClass.everyoneSayHi()
        
        logger info s"~actualClassWithMixin1.everyoneSayHi()"
        actualClassWithMixin1.everyoneSayHi()
        
        logger info s"~actualClassWithMixin1AndMixin2.everyoneSayHi()"
        actualClassWithMixin1AndMixin2.everyoneSayHi()
    }
    
    //    logger info s"~actualClass.add(10)"
    //    val newActualClass = actualClass.add(10)
    //
    //    val vector = Vector()
    //    ActualClass(vector)
    //
    //    logger info s"~actualClass.get(0)=${actualClass.get(0)}"
    //    logger info s"~newActualClass.get(0)=${newActualClass.get(0)}"
    
    logger info s"~Done"
    
}

case class ActualClass2() extends SuperTrait with Logging {
    override def sayHi() = logger info s"~ActualClass says Hi!"
    override def everyoneSayHi() = {
        logger info s"~ActualClass says Hi!"
        super.everyoneSayHi()
    }
    //    override def get(index: Int): Double = {
    //        if (index < vector.size) vector(index) else Double.NaN
    //    }
    //    override def add(value: Double): SuperTrait = {
    //        copy(vector :+ value)
    //    }
}

trait Mixin3 extends ActualClass2 with Logging {
    override def sayHi() = logger info s"~Mixin1 says Hi!"
    override def everyoneSayHi() = {
        logger info s"~Mixin1 says Hi!"
        super.everyoneSayHi()
        super.copy()
    }
//    override def copy(): Unit = {
//        super.copy()
//    }
}

// ===================================================================== //

object MixinDemo1 extends App with Logging {
    
    val actualClass = new ActualClass
    val actualClassWithMixin1 = new ActualClass() with Mixin1
    val actualClassWithMixin1AndMixin2 = new ActualClass() with Mixin1 with Mixin2
    
    def inheritanceOrderDemo() = {
        logger info s"~actualClass.sayHi()"
        actualClass.sayHi()
    
        logger info s"~actualClass.everyoneSayHi()"
        actualClass.everyoneSayHi()
    
        logger info s"~actualClassWithMixin1.everyoneSayHi()"
        actualClassWithMixin1.everyoneSayHi()
    
        logger info s"~actualClassWithMixin1AndMixin2.everyoneSayHi()"
        actualClassWithMixin1AndMixin2.everyoneSayHi()
    }
    
//    logger info s"~actualClass.add(10)"
//    val newActualClass = actualClass.add(10)
//
//    val vector = Vector()
//    ActualClass(vector)
//
//    logger info s"~actualClass.get(0)=${actualClass.get(0)}"
//    logger info s"~newActualClass.get(0)=${newActualClass.get(0)}"
    
    logger info s"~Done"
    
}

trait SuperTrait extends Logging {
    def sayHi() = logger info s"~SuperTrait says Hi!"
    def everyoneSayHi() = logger info s"~SuperTrait says Hi!"
//    def get(index: Int): Double
//    def add(value: Double): SuperTrait
}

trait Mixin1 extends SuperTrait with Logging {
    override def sayHi() = logger info s"~Mixin1 says Hi!"
    override def everyoneSayHi() = {
        logger info s"~Mixin1 says Hi!"
        super.everyoneSayHi()
    }
}

trait Mixin2 extends SuperTrait with Logging {
    override def sayHi() = logger info s"~Mixin2 says Hi!"
    override def everyoneSayHi() = {
        logger info s"~Mixin2 says Hi!"
        super.everyoneSayHi()
    }
}

//object ActualClass {
//    private def ActualClass(vector: Vector[Double] = Vector()): ActualClass = ???
//}
case class ActualClass () extends SuperTrait with Logging {
    override def sayHi() = logger info s"~ActualClass says Hi!"
    override def everyoneSayHi() = {
        logger info s"~ActualClass says Hi!"
        super.everyoneSayHi()
    }
//    override def get(index: Int): Double = {
//        if (index < vector.size) vector(index) else Double.NaN
//    }
//    override def add(value: Double): SuperTrait = {
//        copy(vector :+ value)
//    }
}

// ===================================================================== //

// http://baddotrobot.com/blog/2014/09/22/scala-mixins/

class Customer()
trait Basket
trait FullBasket
//object FullBasket

class OracleCustomers {
    
    type CustomerId = Int
    type CustomerQuery = String
    
    def add(customer: Customer) = {???}
    def getCustomer(id: CustomerId) = {???}
    def getBasketValue(query: CustomerQuery) = {???}
    def ship(query: CustomerQuery) = {???}
}

// <- bad name, this is really a "fixture"
trait CustomersTestFixture {
    val customers: Customers
//    this: Customers =>
    
    type Customers = Vector[Customer]
    
    def RandomCustomer(): Customer = new Customer()
    def RandomDiscountedCustomer(): Customer = new Customer()
    def RandomFullBasket(): Basket = new Basket {}
    def RandomExpiredCustomer(): Customer = new Customer()
    
    
    
    def addSomeCustomersWithFullBaskets() = {
//        val randomCustomer = RandomCustomer().with(new Basket() {})
//        customers.:+(RandomCustomer().with(RandomFullBasket())
//        customers.:+(RandomDiscountedCustomer().with(RandomFullBasket())
    }
    
    def addSomeCustomersWithEmptyBaskets() = {
        customers.:+(RandomCustomer())
        customers.:+(RandomExpiredCustomer())
    }
}


