package ie.nix.ecj;

import ec.gp.GPData;

public class Data extends GPData {
	
	private static final long serialVersionUID = 1;

	private double doubleData;
	
	public boolean equals(Data typedData) {
		return doubleData == typedData.getDouble();
	}

	@Override
	public String toString() {
		return "Data["+doubleData+"]";
	}

	@Override
    public void copyTo(final GPData gpd) { 
		((Data)gpd).doubleData = doubleData;
    }

	public String toStringForHumans() {
		return String.valueOf(doubleData);
		
	}
	
	public double getDouble() {
		return doubleData;
	}
	
	public void set(double doubleData) {
		this.doubleData = doubleData;
	}
	    
}
