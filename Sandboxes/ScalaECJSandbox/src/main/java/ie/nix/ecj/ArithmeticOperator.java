package ie.nix.ecj;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ArithmeticOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public static class Add extends ArithmeticOperator {

		private static final long serialVersionUID = 1;
		
		public Add() {
			super("+", 2, (results, result) -> 
				result.set(results[0].getDouble() + results[1].getDouble()));
		}
		
	}

	public static class Minus extends ArithmeticOperator {

		private static final long serialVersionUID = 1;
		
		public Minus() {
			super("-", 1, (results, result) -> 
				result.set(-results[0].getDouble()));
		}	
	}
	
	public static class Sub extends ArithmeticOperator {

		private static final long serialVersionUID = 1;
		
		public Sub() {
			super("-", 2, (results, result) -> 
				result.set(results[0].getDouble() - results[1].getDouble()));
		}

	}
	
	public static class Mul extends ArithmeticOperator {

		private static final long serialVersionUID = 1;
		
		public Mul() {
			super("*", 2, (results, result) -> 
				result.set(results[0].getDouble() * results[1].getDouble()));
		}	
		
	}
	
	public static class Div extends ArithmeticOperator {
		
		private static final long serialVersionUID = 1;
		
		public Div() {
			super("div", 2, (results, result) -> {
				if (results[1].getDouble() == 0) {
					result.set(0);
				} else {
					result.set(results[0].getDouble() / results[1].getDouble());
				}
			});
		}
		
	}
	
	protected ArithmeticOperator(String symbol, int numberOfChildren, Eval eval) {
		super(symbol, numberOfChildren, eval);
	}
}
