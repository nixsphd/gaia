package ie.nix.ecj;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.koza.KozaFitness;
import ec.util.Parameter;
import ie.nix.ecj.Variable.X;
import ie.nix.ecj.Variable.Y;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Problem extends ec.gp.GPProblem {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final long serialVersionUID = 1;
	
	public static final String P_NUMBER_OF_RUNS = "number_of_runs";
	
	protected int numberOfRuns;

	@Override
	public String toString() {
		return this.getClass().getCanonicalName();
	}
	
	/*
	 * Set-up
	 */
	public void setup(final EvolutionState state, final Parameter base) {
	    // very important, remember this
	    super.setup(state, base);

		numberOfRuns = state.parameters.getIntWithDefault(
				base.push(P_NUMBER_OF_RUNS), defaultBase().push(P_NUMBER_OF_RUNS), 0);
		LOGGER.debug("numberOfRuns={}", numberOfRuns);
		assert (numberOfRuns >= 1) : "numberOfRuns is "+numberOfRuns+", it needs to be >= 1.";
		
	}

	/*
	 * Evaluation
	 */
	public void evaluate(final EvolutionState state, final Individual individual, 
                         final int subpopulation, final int threadnum) {

		LOGGER.trace("~Evaluating individual={}", individual.hashCode());

        if (!individual.evaluated) {
			GPIndividual gpIndividual = (GPIndividual)individual;

        	double totalEvaluation = 0;
            int hits = 0;
            for (int run = 0; run < numberOfRuns; run++) {
            	
            	X.value = state.random[0].nextDouble();
            	Y.value = state.random[0].nextDouble();
				double expectedResult = X.value * Y.value;

				Data input = (Data)(this.input);
            	gpIndividual.trees[0].child.eval(
                    	state, threadnum, input, stack, gpIndividual,this);

            	double actualResult = input.getDouble();
            	double evaluation = Math.abs(expectedResult - actualResult);
            	if (evaluation == 0.0) {
            		hits++;
            	}
            	
            	LOGGER.debug("~generation={}, gpIndividual={}, run={}, "
            			+ "evaluation={}, hits={}", 
	    				state.generation, gpIndividual.hashCode(), run, 
	    				evaluation, hits);
            	
            	totalEvaluation += evaluation;
            	
         
            }
            
            gpIndividual.evaluated = true;
         	KozaFitness kozaFitness = (KozaFitness)gpIndividual.fitness; 
         	kozaFitness.setStandardizedFitness(state, totalEvaluation/numberOfRuns);
	    	kozaFitness.hits = hits;
	    	
			LOGGER.info("~fitness={}, hits={}", 
					kozaFitness.adjustedFitness(), hits);		
        } 
    }

}
