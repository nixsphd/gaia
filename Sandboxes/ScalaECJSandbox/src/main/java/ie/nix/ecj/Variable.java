package ie.nix.ecj;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Variable extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;

	public static class X extends Variable {

		private static final long serialVersionUID = 1;
		
	    public static double value;
	    
		public X() {
			super("x", (results, result) -> result.set(X.value));
		}	
	}
	
	public static class Y extends Variable {

		private static final long serialVersionUID = 1;
		
	    public static double value;
	    
		public Y() {
			super("y", (results, result) -> result.set(Y.value));
		}	
	}

	public Variable(String symbol, Eval eval) {
		super(symbol, 0, eval);
	}
	
}
