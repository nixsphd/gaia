package ie.nix.ecj;

import java.io.Serializable;
import java.util.function.BiConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;

public class Node extends GPNode {

	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = 1;
	
	public interface Eval extends BiConsumer<Data[], Data>, Serializable {}

	public final String symbol;
	public final int numberOfChildren;
	protected final String stringForHumans;
	public final BiConsumer<Data[], Data> eval;
	
	protected Node(String symbol, int numberOfChildren) {
		this(symbol, symbol, numberOfChildren, null);
	}
	
	protected Node(String symbol, int numberOfChildren, Eval eval) {
		this(symbol, symbol, numberOfChildren, eval);
	}
	
	protected Node(String symbol, String stringForHumans, int numberOfChildren, Eval eval) {
		this.symbol= symbol;
		this.numberOfChildren = numberOfChildren;
		this.stringForHumans = stringForHumans;
		this.eval = eval;
		
		// This is just for unit testing
		children = new GPNode[numberOfChildren];
		LOGGER.trace("symbol={}, stringForHumans={}, numberOfChildren={}, eval={}", 
				symbol, stringForHumans, numberOfChildren, eval);
	}
	
	@Override
	public String toString() { 
		return symbol; 
    }
	
	@Override
	public String toStringForHumans() {
		return stringForHumans;
	}

	@Override
    public int expectedChildren() { 
    	return numberOfChildren; 
    }
	
	public void eval(EvolutionState state, int thread, GPData input, ADFStack stack, 
			GPIndividual individual, Problem problem) {
		Data[] childrensResults = new Data[numberOfChildren];
		for (int c = 0; c < numberOfChildren; c++) {
			childrensResults[c] = (Data)input.clone();
			children[c].eval(state,thread,childrensResults[c],stack,individual,problem);
		}
		if (eval != null) {
			Data result = (Data)input;
			eval.accept(childrensResults, result);
			LOGGER.trace("result={}", result);
		} else {
			LOGGER.error("eval not set for {}", this);
		}
		
	}

}
