package ie.nix.ecj;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JavaTest {

	private static final Logger LOGGER = 
			LogManager.getLogger();
	
	private static final String TEST_EGC_AGENT_PARAMS = 
			"src/main/resources/java.params";
	
	public static void main(String[] args) {
		String[] properties = new String[]{
				"-file", TEST_EGC_AGENT_PARAMS,
			};	

		ec.Evolve.main(properties);
		LOGGER.info("~DONE.");		
	}
		
}
