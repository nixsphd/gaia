package ie.nix.ecj.scala

import ec.gp.koza.KozaFitness
import ec.{EvolutionState, Fitness, Individual}
import ec.gp.{ADFStack, GPIndividual, GPProblem}
import ec.util.Parameter
import ie.nix.ecj.scala.Problem.{DEFAULT_NUMBER_OF_RUNS, NUMBER_OF_RUNS_PARAMETER_NAME}
import ie.nix.ecj.scala.variable.X
import ie.nix.ecj.scala.variable.Y
import org.apache.logging.log4j.scala.Logging

object Problem extends Logging {
    val NUMBER_OF_RUNS_PARAMETER_NAME: String = "number_of_runs"
    val DEFAULT_NUMBER_OF_RUNS: Int = 1
}

class Problem(private var numberOfRuns: Int = DEFAULT_NUMBER_OF_RUNS) extends GPProblem with Logging {

    override def toString: String = getClass.getCanonicalName

    override def setup(state: EvolutionState, base: Parameter): Unit = {

        super.setup(state, base)

        numberOfRuns = state.parameters.getIntWithDefault(
            base.push(NUMBER_OF_RUNS_PARAMETER_NAME),
            defaultBase().push(NUMBER_OF_RUNS_PARAMETER_NAME), numberOfRuns)
        require(numberOfRuns >= 1)
        logger.debug(s"numberOfRuns=$numberOfRuns")

    }

    override def evaluate(evolutionState: EvolutionState, individual: Individual,
                          subpopulation: Int, threadnum: Int): Unit = {

        if (! isIndividualEvaluated(individual)) {

            val (evaluation, hits) = evaluateIndividual(
                evolutionState, individual, subpopulation, threadnum)

            updateIndividualsFitness(evolutionState, individual.fitness, evaluation, hits)

            setIndividualAsEvaluated(individual)

        }
    }

    private def isIndividualEvaluated (individual: Individual) = {
        individual.evaluated
    }

    private def setIndividualAsEvaluated (individual: Individual): Unit = {
        individual.evaluated = true
    }

    private def evaluateIndividual(evolutionState: EvolutionState, individual: Individual,
                           subpopulation: Int, threadNumber: Int): (Double, Int) = {

        (individual, input) match {
            case (gpIndividual: GPIndividual, data: Data) =>
                val gpIndividualEvaluator = new GPIndividualEvaluator(evolutionState, threadNumber, subpopulation,
                    numberOfRuns, data, stack, this)
                gpIndividualEvaluator.evaluateGPIndividual(gpIndividual)
            case _ =>
                require(individual.isInstanceOf[GPIndividual])
                require(input.isInstanceOf[Data])
                (Double.MaxValue, 0)
        }
    }

    private def updateIndividualsFitness(evolutionState: EvolutionState, fitness: Fitness,
                                         evaluation: Double, hits: Int): Unit = {
        fitness match {
            case kozaFitness: KozaFitness =>
                updateIndividualsKozaFitness(evolutionState, kozaFitness, evaluation, hits)
            case _ =>
                require(fitness.isInstanceOf[KozaFitness])
        }
    }

    private def updateIndividualsKozaFitness(evolutionState: EvolutionState, kozaFitness: KozaFitness,
                                     evaluation: Double, hits: Int): Unit = {
        kozaFitness.setStandardizedFitness(
            evolutionState, evaluation / numberOfRuns)
        kozaFitness.hits = hits
        logger.info(s"~adjustedFitness=${kozaFitness.adjustedFitness}, " +
          s"hits=${kozaFitness.hits}")
    }

}

class GPIndividualEvaluator(evolutionState: EvolutionState, threadNumber: Int,
                            subpopulation: Int, numberOfRuns: Int, data: Data,
                            adfStack: ADFStack, problem: Problem
                           ) extends Logging {

    def evaluateGPIndividual(gpIndividual: GPIndividual): (Double, Int) = {
        var totalEvaluation = 0.0
        var hits = 0
        for (run <- 0 until numberOfRuns) {
            val evaluation: Double = evaluateGPIndividualForSingleRun(gpIndividual)
            if (evaluation == 0.0) {
                hits += 1
            }
            logger.debug("~gpIndividual={}, run={}, evaluation={}, hits={}",
                gpIndividual.hashCode(), run, evaluation, hits)
            totalEvaluation += evaluation
        }
        (totalEvaluation, hits)
    }

    def evaluateGPIndividualForSingleRun(gpIndividual: GPIndividual): Double = {

        X.setValue(nextRandomDouble())
        Y.setValue(nextRandomDouble())

        val expectedResult: Double = X.value * Y.value

        gpIndividual.trees(0).child.eval(evolutionState, threadNumber,
            data, adfStack, gpIndividual, problem)
        val actualResult: Double = data.doubleData()
        Math.abs(expectedResult - actualResult)

    }

    private def nextRandomDouble(): Double = evolutionState.random(threadNumber).nextDouble()

}