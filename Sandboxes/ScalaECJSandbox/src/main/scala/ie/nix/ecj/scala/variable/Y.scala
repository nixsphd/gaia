package ie.nix.ecj.scala.variable

import ie.nix.ecj.scala.Node

object Y {
    private var _value: Double = -1
    def value: Double = _value
    def setValue(value: Double): Unit = _value = value
}

class Y extends Node("x", "x",
    0, (_, result) => result.setDoubleData(Y.value))