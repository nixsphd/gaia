package ie.nix.ecj.scala

import org.apache.logging.log4j.scala.Logging

import ec.gp.GPData

class Data() extends GPData with Logging {

  private var _doubleData: Double = -1

  def this(dataToCopy: Data) = {
    this(); setDoubleData(dataToCopy.doubleData())
  }

  override def equals(any: Any): Boolean = any match {
    case data: Data => data.doubleData == doubleData
    case _ => false
  }

  override def clone(): AnyRef = super.clone()

  override def toString: String = "Data[" + doubleData + "]"

  override def copyTo(gpd: GPData): Unit = gpd match {
    case data: Data => data.setDoubleData(doubleData())
  }

  def doubleData(): Double = _doubleData

  def setDoubleData(doubleData: Double): Unit = this._doubleData = doubleData

  def toStringForHumans: String  = doubleData().toString

}