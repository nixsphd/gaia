package ie.nix.ecj.scala.arithmetic_operator

import ie.nix.ecj.scala.Node
import org.apache.logging.log4j.scala.Logging

class Add() extends Node("+", "+", 2,
    eval = (results, result) =>
        result.setDoubleData(results.head.doubleData + results(1).doubleData))
  with Logging

class Sub() extends Node("-", "-", 2,
    eval = (results, result) =>
        result.setDoubleData(results.head.doubleData - results(1).doubleData)
) with Logging

class Mul() extends Node("*", "*", 2,
    eval = (results, result) => {
        result.setDoubleData(results.head.doubleData * results(1).doubleData)
    }
) with Logging

class Div() extends Node("div", "div", 2,
    eval = (results, result) => results(1).doubleData() match {
        case divisor if divisor == 0.0 => result.setDoubleData(0)
        case divisor => result.setDoubleData(results.head.doubleData / divisor)
    }) with Logging

class Minus() extends Node("-", "-", 1,
    eval = (results, result) =>
        result.setDoubleData(-results.head.doubleData)
) with Logging

