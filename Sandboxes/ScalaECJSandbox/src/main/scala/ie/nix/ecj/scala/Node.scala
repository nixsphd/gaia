package ie.nix.ecj.scala

import ec.EvolutionState
import ec.gp.{ADFStack, GPData, GPIndividual, GPNode}
import org.apache.logging.log4j.scala.Logging

object Node {
//    type Eval = (Array[Data], Data) => Unit
    type Eval = (List[Data], Data) => Unit
}

class Node(symbol: String,
                stringForHumans: String,
                numberOfChildren: Int = 0,
                val eval: Node.Eval) extends GPNode with Logging {

    logger.trace("symbol={}, stringForHumans={}, expectedChildren={}, eval={}",
        symbol, toStringForHumans, expectedChildren, eval)

    override def toString: String = symbol

    override def toStringForHumans: String = stringForHumans

    override def expectedChildren: Int = numberOfChildren

    override def eval(evolutionState: EvolutionState, threadNumber: Int,
                      gpData: GPData, adfStack: ADFStack,
                      gpIndividual: GPIndividual, problem: ec.Problem): Unit = {

        gpData match {
            case data: Data =>
                val childrensResults: List[Data] = List.fill(expectedChildren)(new Data())
                for ((child, childResult) <- children zip childrensResults) {
                    child.eval(evolutionState,threadNumber,
                        childResult, adfStack, gpIndividual, problem)
                }
                if (eval != null) {
                     eval(childrensResults, data)
                    logger.trace(s"childrensResults=$childrensResults, result=$data")
                } else {
                    logger.error(s"eval not set for $this")
                }
            case _ => require(gpData.isInstanceOf[Data])
        }
    }
}