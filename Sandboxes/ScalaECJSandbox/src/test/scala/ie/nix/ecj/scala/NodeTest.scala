package ie.nix.ecj.scala

import ec.gp.GPNode
import org.scalatest.flatspec.AnyFlatSpec

class NodeTest extends AnyFlatSpec {

    behavior of "Node"

    it should "toString" in {
        val node = _createNode()
        assert("symbol" == node.toString)
    }

    it should "toStringForHumans" in {
        val node = _createNode()
        assert("stringForHumans" == node.toStringForHumans)

    }

    it should "expectedChildren" in {
        val node = _createNode()
        assert(1 == node.expectedChildren)
    }

    it should "eval when no children" in {
        val node = _createNode(0)
        val data = new Data
        node.eval(null, 0, data, null, null, null)
        assert(10 == data.doubleData())
    }

    private def _createNode(numberOfChildren: Int = 1) = {
        val node = new Node("symbol", "stringForHumans", numberOfChildren,
            (_, result) => result.setDoubleData(10))
        node.children = new Array[GPNode](numberOfChildren)
        node
    }

}
