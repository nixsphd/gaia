package ie.nix.ecj.scala

// ScalaMock docs: https://www.javadoc.io/doc/org.scalamock/scalamock_2.12/latest/org/scalamock/index.html

import ec.gp.koza.KozaFitness
import ec.{EvolutionState, Fitness, Individual}
import ec.gp.{ADFStack, GPData, GPIndividual, GPNode, GPTree}
import ec.simple.{SimpleDefaults, SimpleFitness}
import ec.util.{MersenneTwisterFast, Parameter}
import ec.vector.IntegerVectorIndividual
import ie.nix.ecj.scala.variable.X
import ie.nix.ecj.scala.variable.Y
import org.apache.logging.log4j.scala.Logging
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.PrivateMethodTester._

class ProblemSpec extends AnyFlatSpec with Logging {

    def IDEAL_EVAL_FUNCTION(): Double = X.value * Y.value
    def IDEAL_EVALUATION():  Double = 0.0

    def NOT_IDEAL_EVAL_FUNCTION(): Double = 1.0
    def NOT_IDEAL_EVALUATION(): Double = 1.0 - X.value * Y.value

    // private methids to be tested
    private val evaluateIndividual = PrivateMethod[(Double, Int)]('evaluateIndividual)
    private val updateIndividualsKozaFitness = PrivateMethod[Unit]('updateIndividualsKozaFitness)
    private val updateIndividualsFitness = PrivateMethod[Unit]('updateIndividualsFitness)

    behavior of "Problem"

    it should "evaluate an individual when it's a GPIndividual" in {

        val numberOfRuns: Int = 10

        val problem: Problem = new Problem(numberOfRuns)
        problem.input = new Data

        val evolutionState: EvolutionState = new EvolutionStateMock

        val gpIndividual: GPIndividual = new GPIndividualMock(new NodeMock(IDEAL_EVAL_FUNCTION))

        // Using ScalaTest PrivateMethodTester to call the private method
        val (actualEvaluation, actualHits) =  problem invokePrivate evaluateIndividual(evolutionState, gpIndividual, 0, 0)

        // Must calculate expected after calling evaluateGPIndividualForSingleRun as it sets X.valur and Y.value
        // which is used in the function
        val expectedEvaluation = IDEAL_EVALUATION()
        val expectedHits = numberOfRuns

        logger.info(s"~expectedEvaluation=$expectedEvaluation")
        logger.info(s"~actualEvaluation=$actualEvaluation")
        assert(expectedEvaluation == actualEvaluation)

        logger.info(s"~expectedHits=$expectedHits")
        logger.info(s"~actualHits=$actualHits")
        assert(expectedHits == actualHits)
    }

    it should "evaluate an individual should fail when its not a GPIndividual" in {

        val numberOfRuns: Int = 10

        val problem: Problem = new Problem(numberOfRuns)
        problem.input = new Data

        val evolutionState: EvolutionState = new EvolutionStateMock

        val individual: Individual = new IntegerVectorIndividual

        intercept[IllegalArgumentException] {
            // Using ScalaTest PrivateMethodTester to call the private method
            val (actualEvaluation, actualHits) = problem invokePrivate evaluateIndividual(evolutionState, individual, 0, 0)
        }

    }

    it should "evaluate an individual should fail when its not a Data" in {

        val numberOfRuns: Int = 10

        val problem: Problem = new Problem(numberOfRuns)
        // Not setting the input to be a Data

        val evolutionState: EvolutionState = new EvolutionStateMock

        val gpIndividual: GPIndividual = new GPIndividualMock(new NodeMock(IDEAL_EVAL_FUNCTION))

        intercept[IllegalArgumentException] {
            // Using ScalaTest PrivateMethodTester to call the private method
            val (actualEvaluation, actualHits) = problem invokePrivate evaluateIndividual(evolutionState, gpIndividual, 0, 0)
        }

    }

    it should "update the Individuals fitness when its a KozaFitness" in {

        val problem: Problem = new Problem()

        val evolutionState: EvolutionState = new EvolutionStateMock

        val kozaFitness: KozaFitness = new KozaFitness

        val expectedEvaluation: Double = 1.0
        val expectedHits: Int = 10

        // Using ScalaTest PrivateMethodTester to call the private method
        problem invokePrivate updateIndividualsFitness(evolutionState, kozaFitness, expectedEvaluation, expectedHits)

        val actualEvaluation: Double = kozaFitness.standardizedFitness()
        val actualHits: Int = kozaFitness.hits

        logger.info(s"~expectedEvaluation=$expectedEvaluation")
        logger.info(s"~actualEvaluation=$actualEvaluation")
        assert(expectedEvaluation == actualEvaluation)

        logger.info(s"~expectedHits=$expectedHits")
        logger.info(s"~actualHits=$actualHits")
        assert(expectedHits == actualHits)

    }

    it should "update the Individuals fitness should fail when its a not a KozaFitness" in {

        val problem: Problem = new Problem()

        val evolutionState: EvolutionState = new EvolutionStateMock

        val fitness: Fitness = new SimpleFitness

        val expectedEvaluation: Double = 1.0
        val expectedHits: Int = 10

        intercept[IllegalArgumentException] {
            // Using ScalaTest PrivateMethodTester to call the private method
            problem invokePrivate updateIndividualsFitness(evolutionState, fitness, expectedEvaluation, expectedHits)
        }
    }

    it should "update a KozaFitness" in {

        val problem: Problem = new Problem()

        val evolutionState: EvolutionState = new EvolutionStateMock

        val kozaFitness: KozaFitness = new KozaFitness

        val expectedEvaluation: Double = 1.0
        val expectedHits: Int = 10

        // Using ScalaTest PrivateMethodTester to call the private method
        problem invokePrivate updateIndividualsKozaFitness(evolutionState, kozaFitness, expectedEvaluation, expectedHits)

        val actualEvaluation: Double = kozaFitness.standardizedFitness()
        val actualHits: Int = kozaFitness.hits

        logger.info(s"~expectedEvaluation=$expectedEvaluation")
        logger.info(s"~actualEvaluation=$actualEvaluation")
        assert(expectedEvaluation == actualEvaluation)

        logger.info(s"~expectedHits=$expectedHits")
        logger.info(s"~actualHits=$actualHits")
        assert(expectedHits == actualHits)
    }

    behavior of "GPIndividualEvaluator"

    it should "evaluate GPIndividual when its ideal" in {

        val numberOfRuns: Int = 10
        val gpIndividualEvaluator: GPIndividualEvaluator =
            new GPIndividualEvaluator(new EvolutionStateMock, 0,
                0, numberOfRuns, new Data, null, null)

        val gpIndividual: GPIndividual = new GPIndividualMock(
            new NodeMock(IDEAL_EVAL_FUNCTION))

        val (actualEvaluation, actualHits) = gpIndividualEvaluator.evaluateGPIndividual(gpIndividual)
        // Must calculate expected after calling evaluateGPIndividualForSingleRun as it sets X.valur and Y.value
        // which is used in the function
        val expectedEvaluation = IDEAL_EVALUATION()
        val expectedHits = numberOfRuns

        logger.info(s"~expectedEvaluation=$expectedEvaluation")
        logger.info(s"~actualEvaluation=$actualEvaluation")
        assert(expectedEvaluation == actualEvaluation)

        logger.info(s"~expectedHits=$expectedHits")
        logger.info(s"~actualHits=$actualHits")
        assert(expectedHits == actualHits)

    }

    it should "evaluate GPIndividual when its not ideal" in {

        val numberOfRuns: Int = 10
        val gpIndividualEvaluator: GPIndividualEvaluator =
            new GPIndividualEvaluator(new EvolutionStateMock, 0,
                0, numberOfRuns, new Data, null, null)

        val gpIndividual: GPIndividual = new GPIndividualMock(
            new NodeMock(NOT_IDEAL_EVAL_FUNCTION))

        val (actualEvaluation, actualHits) = gpIndividualEvaluator.evaluateGPIndividual(gpIndividual)
        val expectedHits = 0

        logger.info(s"~actualEvaluation=$actualEvaluation")
        assert(IDEAL_EVALUATION != actualEvaluation)

        logger.info(s"~expectedHits=$expectedHits")
        logger.info(s"~actualHits=$actualHits")
        assert(expectedHits == actualHits)

    }

    it should "evaluate GPIndividual For Single Run when its ideal" in {

        val gpIndividualEvaluator: GPIndividualEvaluator =
            new GPIndividualEvaluator(new EvolutionStateMock, 0,
                0, 10, new Data, null, null)

        val gpIndividual: GPIndividual = new GPIndividualMock(
            new NodeMock(IDEAL_EVAL_FUNCTION))

        val actualEvaluation = gpIndividualEvaluator.evaluateGPIndividualForSingleRun(gpIndividual)
        // Must calculate expected after calling evaluateGPIndividualForSingleRun as it sets X.valur and Y.value
        // which is used in the function
        val expectedEvaluation = IDEAL_EVALUATION()

        logger.info(s"~expectedEvaluation=$expectedEvaluation")
        logger.info(s"~actualEvaluation=$actualEvaluation")
        assert(expectedEvaluation == actualEvaluation)

    }

    it should "evaluate GPIndividual For Single Run when its not ideal" in {

        val gpIndividualEvaluator: GPIndividualEvaluator =
            new GPIndividualEvaluator(new EvolutionStateMock, 0,
                0, 10, new Data, null, null)

        val gpIndividual: GPIndividual = new GPIndividualMock(
            new NodeMock(NOT_IDEAL_EVAL_FUNCTION))

        val actualEvaluation = gpIndividualEvaluator.evaluateGPIndividualForSingleRun(gpIndividual)
        // Must calculate expected after calling evaluateGPIndividualForSingleRun as it sets X.valur and Y.value
        // which is used in the function
        val expectedEvaluation = NOT_IDEAL_EVALUATION()

        logger.error(s"~expectedEvaluation=$expectedEvaluation")
        logger.error(s"~actualEvaluation=$actualEvaluation")
        assert(expectedEvaluation == actualEvaluation)

    }

    private class EvolutionStateMock() extends EvolutionState {
        // Set up the random number generator for thread 0
        random = new Array[MersenneTwisterFast](1)
        random(0) = new MersenneTwisterFast

    }

    private class IndividualMock() extends Individual {

        // methids implemented
        override def defaultBase: Parameter = SimpleDefaults.base().push("individual")

        override def hashCode(): Int = 0

        override def equals(obj: Any): Boolean = false
    }

    private class GPIndividualMock(rootNode: GPNode) extends GPIndividual {
        trees = new Array[GPTree](1)
        trees(0) = new GPTree
        trees(0).child = rootNode

    }

    private class NodeMock(evalData: () => Double) extends GPNode {

        override def toString: String = {
            s"NodeMock($evalData)"
        }

        override def eval(evolutionState: EvolutionState, i: Int, gpData: GPData, adfStack: ADFStack, gpIndividual: GPIndividual, problem: ec.Problem): Unit = {
            gpData match {
                case data: Data => data.setDoubleData(evalData())
            }
        }
    }
}
