package ie.nix.ecj.scala

import org.apache.logging.log4j.scala.Logging

object ScalaTest extends App with Logging {

	private val TEST_EGC_AGENT_PARAMS: String =
		"src/main/resources/scala.params"

	val properties: Array[String] =
		Array("-file", TEST_EGC_AGENT_PARAMS)
	ec.Evolve.main(properties)
	logger.info("~DONE.")

}
