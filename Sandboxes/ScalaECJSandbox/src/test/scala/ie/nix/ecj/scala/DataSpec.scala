package ie.nix.ecj.scala

import org.scalatest.flatspec.AnyFlatSpec

class DataSpec extends AnyFlatSpec {

    behavior of "Data"

    it should "copyTo" in {
        val dataDoubleData = 10
        val data: Data = new Data()
        data.setDoubleData(dataDoubleData)

        val dataToCopyTo: Data = new Data()

        data.copyTo(dataToCopyTo)

        assert(data.doubleData() == dataToCopyTo.doubleData())
        assert(dataToCopyTo.doubleData() == dataDoubleData)
    }

    it should "doubleData" in {
        val dataDoubleData = 10
        val data: Data = new Data()
        data.setDoubleData(dataDoubleData)

        assert(data.doubleData() == dataDoubleData)
    }

    it should "setDoubleData" in {
        val dataDoubleData = 10
        val data: Data = new Data()
        data.setDoubleData(dataDoubleData)

        assert(data.doubleData() == dataDoubleData)

    }

}
