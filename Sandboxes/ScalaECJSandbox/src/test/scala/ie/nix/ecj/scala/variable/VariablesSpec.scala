package ie.nix.ecj.scala.variable

// https://www.youtube.com/watch?v=W9yMkao_AZg

import ie.nix.ecj.scala.{Data, Node}
import org.apache.logging.log4j.scala.Logging
import org.scalatest.flatspec.AnyFlatSpec

class VariablesSpec extends AnyFlatSpec with Logging {

    "An X" should "have it's value set by eval" in {
        val expectedValue = 0.5
        X.setValue(expectedValue)

        _accertEquals(expectedValue, _evaluateVariable(new X))
    }

    "A Y" should "have it's value set by eval" in {
        val expectedValue = 0.5
        Y.setValue(expectedValue)

        _accertEquals(expectedValue, _evaluateVariable(new Y))
    }

    private def _evaluateVariable(variable: Node) = {
        val result = new Data
        variable.eval(null, result)
        result.doubleData()
    }

    private def _accertEquals(expectedValue: Double, actualValue: Double) = {
        logger.info(s"expectedValue=$expectedValue")
        logger.info(s"actualValue=$actualValue")
        assert(expectedValue == actualValue)
    }

}
