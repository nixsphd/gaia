/***********************
 Conway's Game of Life

 The rules are as follows:
 �ۢ A cell is born if it has exactly 3 neighbours
 �ۢ A cell dies of loneliness if it has less than 2 neighbours
 �ۢ A cell dies of overcrowding if it has more than 3 neighbours
 �ۢ A cell survives to the next generation if it does not die of loneliness or overcrowding

 A live cell takes value 1, a dead cell takes value 0
 ************************/
#include <stdio.h>
#include <stdlib.h>
#define NI_D 198                                    /* array sizes */
#define NJ_D 198
#define NSTEPS 500                                /* number of time steps */

struct myMPI_MPIData mpi_data;
//This is a reference to the generated struct containing all necessary MPI variables

struct myMPI_GridData grid_data;
//This is a reference to the generated struct containing all necessary Grid variables

struct myMPI_2DGridSpace grid_space;
//This is a reference to the generated struct containing all necessary array structures

int gen_fill_structure(struct myMPI_MPIData *mpi_data,
		struct myMPI_GridData *grid_data, struct myMPI_2DGridSpace *grid_space) {
	/* Fill Structure, SCENARIO 2: Use the following nested loop to populate the grid
	 * chunk allocated to the current processor in the same manner the chunk would
	 * be populated on the global grid in the serial version
	 */
	int glob_row, glob_col; //These are the global grid row and column indices

	for (glob_row = 1; glob_row <= grid_data->NI; glob_row++) {
		for (glob_col = 1; glob_col <= grid_data->NJ; glob_col++) {
			/*
			 * Check if current process has glob_row and glob_col using methods
			 * gen_i_have_row(int glob_row, grid_data) and gen_i_have_col(int glob_col, grid_data)
			 * which are defined in the wrapper file. These check to see if the global row or column
			 * passed in as argument values, match the rows or columns distributed to the local process
			 *
			 * int gen_i_have_row(int glob_row, grid_data)
			 * is a method which checks to see if the global row passed in matches that in
			 * the local data chunk allocated to the current process. 1 is returned if true, 0 if false
			 * - glob_row is the index of the row to be checked
			 * - grid_data is the struct containing grid related variables
			 *
			 * int gen_i_have_col(int glob_col, grid_data)
			 * is a method which checks to see if the global column passed in matches that in
			 * the local data chunk allocated to the current process. 1 is returned if true, 0 if false
			 * - glob_col is the index of the column to be checked
			 * - grid_data is the struct containing grid related variables
			 */

			if ((gen_i_have_row(glob_row, grid_data) == 1) && (gen_i_have_col(
					glob_col, grid_data) == 1)) {
				//If current processor has glob_row and glob_col, convert i and j
				//(local row and column indices) accordingly
				i = glob_row - grid_data->myFirstRow_glob + 1;
				j = glob_col - grid_data->myFirstCol_glob + 1;

				//Logic for populating grid_space->local_array[i][j] goes here...
				x = rand() / ((float) RAND_MAX + 1);
				if (x < 0.5) {
					grid_space->local_array[i][j] = 0;
				} else {
					grid_space->local_array[i][j] = 1;
				}

			}
		}
	}
	return 0;
}

int gen_access_data(struct myMPI_MPIData *mpi_data,
		struct myMPI_GridData *grid_data, struct myMPI_2DGridSpace *grid_space) {
	int i = 0;
	int j = 0;
	int i_left, i_right, j_bot, j_top;

	//The following loop provides access to all local cells EXCLUDING ghost cells
	for (i = grid_data->ghost_rows; i <= grid_data->innerLastRow; i++) {
		for (j = grid_data->ghost_cols; j <= grid_data->innerLastCol; j++) {
			//The following indices provide access to cells left,right,top and bottom across dimensions
			i_left = i - 1;
			i_right = i + 1;
			j_bot = j - 1;
			j_top = j + 1;

			calc_sum = grid_space->local_array[i_left][j_top]
					+ grid_space->local_array[i][j_top]
					+ grid_space->local_array[i_right][j_top]
					+ grid_space->local_array[i_left][j]
					+ grid_space->local_array[i_right][j]
					+ grid_space->local_array[i_left][j_bot]
					+ grid_space->local_array[i][j_bot]
					+ grid_space->local_array[i_right][j_bot];

			switch (calc_sum) {
			case 3:
				grid_space->tmp_array[i][j] = 1;
				break;
			case 2:
				grid_space->tmp_array[i][j] = old[i][j];
				break;
			default:
				grid_space->tmp_array[i][j] = 0;

			}

			/* copy new_array state into old state */
			for (i = grid_data->ghost_rows; i <= grid_data->innerLastRow; i++) {
				for (j = grid_data->ghost_cols; j <= grid_data->innerLastCol; j++) {
					grid_space->local_array[i][j] = grid_space->tmp_array[i][j];
				}
			}

		}
	}
	return 0;
}

int gen_gather_result(
		struct myMPI_MPIData *mpi_data,
		struct myMPI_GridData *grid_data,
		struct myMPI_2DGridSpace *grid_space) {
	/*
	 * Post processing, SCENARIO 1: A reduction to the root process
	 *
	 * int gen_root_reduce(int sendValue, int recvValue, MPI_Op operation)
	 *   performs a reduction of a single value to the root process and returns the operated value
	 * - sendValue is the address of the send value
	 * - recvValue is the address of the receive value (significant only at root)
	 * - operation is the reduce operation, e.g. MPI_SUM
	 */

	/*  Iterations are done; sum the number of live cells */
	int num_live = 0;
	int i, j;

	for (i = grid_data->ghost_rows; i <= grid_data->innerLastRow; i++) {
		for (j = grid_data->ghost_cols; j <= grid_data->innerLastCol; j++) {
			num_live = num_live + grid_space->local_array[i][j];
		}
	}

	num_live = gen_root_reduce(value, sum, MPI_SUM);

	if (mpi_data->ROOT == 1) {
		printf("\nNumber of live cells = %d\n", num_live);
	}

	return 0;
}

int main(int argc, char *argv[]) {
	//The following variables take their initial values from the set properties but may be edited here
	grid_data.NI = 198;
	grid_data.NJ = 198;
	mpi_data.ndims = 2;
	mpi_data.dims[0] = 0;
	mpi_data.dims[1] = 0;
	mpi_data.periods[0] = 1;
	mpi_data.periods[1] = 1;
	mpi_data.reorder = 0;

	MPI_Init(&argc, &argv); //Initialize the MPI execution environment

	// This function uses the variables defined above
	gen_set_MPI_data(&mpi_data, &grid_data);

	/* Function choices exist to allocate memory to the array structures:
	 * 1: gen_allocate_structure_all(&mpi_data,&grid_data,&grid_space)
	 *   	allocates memory to all arrays (local, temporary-local and global sized) passed in as arguments
	 * 2: gen_allocate_structure_loc(&grid_data,&grid_space)
	 *  	allocates memory to the local-sized array passed in as an argument
	 * 3: gen_allocate_structure_loc_glob(&mpi_data,&grid_data,&grid_space)
	 *  	allocates memory to the global and local sized arrays passed in as arguments
	 * 4: gen_allocate_structure_glob(&mpi_data,&grid_data,&grid_space)
	 *  	allocates memory to the global-sized array passed in as argument
	 */
	gen_allocate_structure_all(&mpi_data, &grid_data, &grid_space);

	gen_fill_structure(&mpi_data, &grid_data, &grid_space);

	int i, j, n, ni, nj, calc_sum, num_live;
	int i_left, i_right, j_bot, j_top;


	/*  time steps */
	for (n = 0; n < NSTEPS; n++) {

		gen_boundaries_swap_2dAccess(&mpi_data, &grid_data,
				grid_space.local_array);

		gen_access_data(&mpi_data, &grid_data, &grid_space); //A skeleton for this function is included in this file.
		//This function provides a template to access data across the neighbouring cells.

	}

	/*
	 * gen_root_gather_2d(grid_space->global_array, tag, mpi_data, grid_data)
	 * Root receives custom dataypes from other processes and unpacks them into a global array
	 * - global_array is global-sized array on the root process
	 * - tag is the message tag, needs to be the same value as the tag in gen_send_to_root()
	 * - mpi_data is the structure containing MPI related data variables
	 * - grid_data is the structure containing Grid related data variables
	 */
	gen_root_gather_2d(grid_space->global_array, tag, mpi_data, grid_data);

}
