################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../myMPI_AppFragments.c \
../myMPI_MPIWrappers.c \
../parallel_gol.c \
../serail_gol.c 

OBJS += \
./myMPI_AppFragments.o \
./myMPI_MPIWrappers.o \
./parallel_gol.o \
./serail_gol.o 

C_DEPS += \
./myMPI_AppFragments.d \
./myMPI_MPIWrappers.d \
./parallel_gol.d \
./serail_gol.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


