/***********************
  Conway's Game of Life

  The rules are as follows:
   	�ۢ A cell is born if it has exactly 3 neighbours
 	�ۢ A cell dies of loneliness if it has less than 2 neighbours
 	�ۢ A cell dies of overcrowding if it has more than 3 neighbours
 	�ۢ A cell survives to the next generation if it does not die of loneliness or overcrowding

 	A live cell takes value 1, a dead cell takes value 0
************************/
#include <stdio.h>
#include <stdlib.h>
#define NI_D 198                                    /* array sizes */
#define NJ_D 198
#define NSTEPS 500                                /* number of time steps */

int main(int argc, char *argv[])
{
  	int i, j, n, ni,nj,calc_sum, num_live;
	int i_left,i_right,j_bot,j_top;

    int **old;
    int **new_array;
    float x;
    /* allocate arrays */
    ni = NI_D + 2;                           /* add 2 for left and right ghost cells */
    nj = NJ_D + 2;

    old = malloc(ni*sizeof(int*));
    new_array = malloc(ni*sizeof(int*));

    for(i=0; i<ni; i++) {
        old[i] = malloc(nj*sizeof(int));
        new_array[i] = malloc(nj*sizeof(int));
    }
    /*  initialize elements of old to 0 or 1 */
    for(i=1; i<=NI_D; i++) {
        for(j=1; j<=NJ_D; j++) {
            x = rand()/((float)RAND_MAX + 1);
            if(x < 0.5) {
                old[i][j] = 0;
            } else {
                old[i][j] = 1;
            }
        }
    }
    /*  time steps */
    for(n=0; n<NSTEPS; n++) {
         /* corner boundary conditions */
        old[0][0] = old[NI_D][NJ_D];
        old[0][NJ_D+1] = old[NI_D][1];

        old[NI_D+1][NJ_D+1] = old[1][1];
        old[NI_D+1][0] = old[1][NJ_D];
        /* left-right boundary conditions */
        for(i=1; i<=NI_D; i++) {
            old[i][0] = old[i][NJ_D];
            old[i][NJ_D+1] = old[i][1];
        }
        /* top-bottom boundary conditions */
        for(j=1; j<=NJ_D; j++) {
            old[0][j] = old[NI_D][j];
            old[NI_D+1][j] = old[1][j];
        }
        for(i=1; i<=NI_D; i++) {
            for(j=1; j<=NJ_D; j++) {

		        i_left = i-1;
		        i_right = i+1;
		        j_bot = j-1;
		        j_top = j+1;

		        calc_sum =  old[i_left][j_top] + old[i][j_top] + old[i_right][j_top]
		              + old[i_left][j ]              + old[i_right][j ]
		              + old[i_left][j_bot] + old[i][j_bot] + old[i_right][j_bot];

                switch(calc_sum) {
                    case 3:
                        new_array[i][j] = 1;
                        break;
                    case 2:
                        new_array[i][j] = old[i][j];
                        break;
                    default:
                        new_array[i][j] = 0;
                }
            }
        }
        /* copy new_array state into old state */
        for(i=1; i<=NI_D; i++) {
            for(j=1; j<=NJ_D; j++) {
                old[i][j] = new_array[i][j];
            }
        }
    }
    /*  Iterations are done; sum the number of live cells */
    num_live = 0;

    for(i=1; i<=NI_D; i++) {
        for(j=1; j<=NJ_D; j++) {
            num_live = num_live + old[i][j];
        }
    }
    printf("\nNumber of live cells = %d\n", num_live);
    return 0;
}
