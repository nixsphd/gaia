rules = [
  Disable
  DisableSyntax
  ExplicitResultTypes
  OrganizeImports
  ProcedureSyntax
  RemoveUnused
]

Disable.symbols = [
  {
    regex = "^\\Qscala/math/Big\\E.*$"
    message = "There is an outstanding issue with scala arbitrary precision numbers: https://github.com/scala/bug/issues/9670"
  }

  {
    symbol = "scala.collection.JavaConversions"
    message = "Prefer JavaConverters"
  }

  {
    symbol = "scala/Enumeration"
    message = "Prefer a sealed abstract class"
  }

  {
    regex = {
      includes = [
        "^\\Qscala/util/Either.LeftProjection#get().\\E$"
        "^\\Qscala/util/Either.RightProjection#get().\\E$"
        "^\\Qscala/util/Try#get().\\E$"
        "^\\Qscala/Option#get().\\E$"
        "^\\Qscala/collection/IterableLike#head().\\E$"
      ]
    }
    message = "not a total function"
  }

  {
    regex = {
      includes = [
        "^\\Qsun/\\E.*$",
        "^\\Qjava/awt/\\E.*$"
      ]
    }
    message = "Use not allowed"
  }
]

Disable.ifSynthetic = [
  "java/io/Serializable"
  "scala/Product"

  "scala/Option.option2Iterable"
  "scala/Predef.any2stringadd"
]

DisableSyntax.regex = [
  "println"
]

DisableSyntax {
  noAsInstanceOf = true
  noFinalize = true
  noFinalVal = true
  noImplicitConversion = true
  noIsInstanceOf = true
  noNulls = true
  noReturns = true
  noSemicolons = true
  noTabs = true
  noVars = true
  noWhileLoops = true
  noXml = true
}

ExplicitResultTypes {
  unsafeShortenNames = true
  fatalWarnings = true

  memberKind = [Def, Val]
  memberVisibility = [Public, Protected]

  skipSimpleDefinitions = false
  skipLocalImplicits = false
}

OrganizeImports {
  expandRelative = true
  groups = ["re:javax?\\.", "*", "scala."]
  groupedImports = Merge
  removeUnused = true
}

RemoveUnused {
  imports = false # conflicts with `OrganizeImports`
}