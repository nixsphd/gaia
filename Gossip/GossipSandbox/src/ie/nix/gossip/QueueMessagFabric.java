package ie.nix.gossip;

import java.util.LinkedList;
import java.util.Queue;

public class QueueMessagFabric implements Gossip.MessageFabric {
		
	private final Queue<Gossip.Message> messages;

	public QueueMessagFabric() {
		this.messages = new LinkedList<Gossip.Message>();
	}

	public void sendMessage(Gossip.Message message) {
			message.getReceiver().getMessageFabric().receiveMessage(message);
	}

	public void receiveMessage(Gossip.Message message) {
		synchronized (messages) { 
			messages.add(message);
		}
	}

	public Gossip.Message nextMessage() {
		Gossip.Message message = null;
		synchronized (messages) { 
			message = messages.poll();
		}
		return message;
	}
	
}
