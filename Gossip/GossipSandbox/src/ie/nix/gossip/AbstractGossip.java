package ie.nix.gossip;

public abstract class AbstractGossip implements Gossip {

	private final NetworkFabric networkOverlay;
	private final MessageFabric gossipFabric;
	
	public AbstractGossip(NetworkFabric networkOverlay, MessageFabric gossipFabric) {
		this.networkOverlay = networkOverlay;
		this.gossipFabric = gossipFabric;
	}

	public abstract void handleMessage(Message message);
	
	public abstract void gossip();

	@Override
	public NetworkFabric getNetworkFabric() {
		return networkOverlay;
	}

	@Override
	public MessageFabric getMessageFabric() {
		return gossipFabric;
	}
	
}
