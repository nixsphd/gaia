package ie.nix.gossip;

public class AbstractMessage implements Gossip.Message {
	
	private final Gossip sender;
	private final Gossip receiver;
	private final Type type;
	private final Payload payload;
	
	public AbstractMessage(Gossip sender, Gossip receiver, Type type, Payload payload) {
		this.sender = sender;
		this.receiver = receiver;
		this.type = type;
		this.payload = payload;
	}

	public Gossip getSender() {
		return sender;
	}

	public Gossip getReceiver() {
		return receiver;
	}

	public Type getType() {
		return type;
	}

	public Payload getPayload() {
		return payload;
	}
	
}

