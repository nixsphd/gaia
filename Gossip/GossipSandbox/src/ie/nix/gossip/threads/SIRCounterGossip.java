package ie.nix.gossip.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ie.nix.gossip.QueueMessagFabric;
import ie.nix.gossip.PushPullGossip;
import ie.nix.gossip.RandomNetworkFabric;
import ie.nix.util.Randomness;

public class SIRCounterGossip extends PushPullGossip {
	
	enum SIRState {
		Suseptable,
		Infected,
		Removed
	}

	// allowedNumberOfUpdates
	static int allowedNumberOfUpdates = 10;

	SIRState state;
	
	// currentNumberOUpdates
	int numberOfUpdates;

	public SIRCounterGossip(NetworkFabric networkOverlay, MessageFabric gossipFabric) {
		super(networkOverlay, gossipFabric);

		state = SIRState.Suseptable;
		numberOfUpdates = 0;
	};
	
	public synchronized void setState(SIRState state) {
		this.state = state;
		
	}
	
	public synchronized SIRState getState() {
		return this.state;
	}

	public void onPush(PushPullGossip.Message pushPullMessage) {
		if (getState() == SIRState.Suseptable) {
			incNumberOfNodesInfected();
	    	System.err.println("Number Of Nodes Infected is "+numberOfNodesInfected());
			setState(SIRState.Infected);
			numberOfUpdates++;
			
		} else if (getState() == SIRState.Infected) {
			if (numberOfUpdates < allowedNumberOfUpdates) {
				numberOfUpdates++;
				
			} else {
				incNumberOfNodesRemoved();
				decNumberOfNodesInfected();
				System.err.println("Number Of Nodes Removed is "+numberOfNodesRemoved());
				setState(SIRState.Removed);
				
			}
		}
		
	}

	@Override
	public void onPull(PushPullGossip.Message pushPullMessage) {
//		System.out.println("~MessageHandler received 'Pull' message");	
		if (getState() == SIRState.Infected || state == SIRState.Removed) {
			// send update message to sender.
			getMessageFabric().sendMessage(new PushPullGossip.Message(this, pushPullMessage.getSender(), PushPullGossip.Message.Type.Push));
		}
	}

	// number of nodes
	static int N = 5;

	static int numberOfNodesInfected = 0;
	
	public synchronized static void incNumberOfNodesInfected() {
		numberOfNodesInfected++;
	}
	
	public synchronized static int numberOfNodesInfected() {
		return numberOfNodesInfected;
	}
	
	public synchronized static void decNumberOfNodesInfected() {
		numberOfNodesInfected--;
	}
	
	// number removed
	static int numberOfNodesRemoved = 0;

	public synchronized static int numberOfNodesRemoved() {
		return numberOfNodesRemoved;
	}
	
	public synchronized static void incNumberOfNodesRemoved() {
		numberOfNodesRemoved++;
	}
	
	public static void main(String[] args) {
		// initialise the random number generator
		Randomness.setSeed(71);
		
		NetworkFabric networkOverlay = new RandomNetworkFabric();
		
		List<ThreadedGossip> gossips = new ArrayList<ThreadedGossip>(N);	
		for (int n = 0; n < N; n++) {
			SIRCounterGossip siGossip = new SIRCounterGossip(networkOverlay, new QueueMessagFabric());
			networkOverlay.addGossip(siGossip);	
			
			ThreadedGossip threadedGossip = new ThreadedGossip(siGossip);
			gossips.add(threadedGossip);
		}
		System.err.println("Initialised "+N+" SIRCounterGossip");
		
		SIRCounterGossip randomSIRGossip = (SIRCounterGossip)networkOverlay.getPeer();
		randomSIRGossip.setState(SIRState.Infected);
		incNumberOfNodesInfected();
		System.err.println("Number of nodes infected is "+numberOfNodesInfected());	
		System.err.println("Number of nodes removed is "+numberOfNodesRemoved());		
		
		System.err.println("Starting all the threads");	
		gossips.forEach(gossip -> gossip.start());
		
		
		while (numberOfNodesRemoved() < N) {
			System.err.println("Number of nodes infected is "+numberOfNodesInfected());	
			System.err.println("Number of nodes removed is "+numberOfNodesRemoved());		
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.err.println("Shutting down, all node infected");	
		gossips.forEach(node -> node.shutdown());
		
		System.err.println("Done!");	
		
	}
}