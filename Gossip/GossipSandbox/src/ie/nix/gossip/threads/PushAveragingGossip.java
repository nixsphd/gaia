package ie.nix.gossip.threads;

import ie.nix.gossip.Gossip;
import ie.nix.gossip.PushPullGossip;
import ie.nix.gossip.QueueMessagFabric;
import ie.nix.gossip.RandomNetworkFabric;
import ie.nix.util.Randomness;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PushAveragingGossip extends PushPullGossip {
	
	public class MessagePayload implements Gossip.Message.Payload {
		public double x;
		public double w; // weight
	};
	
	// number of nodes
	static int N = 100;
	
	private double x;
	private double w; // weight
	
	public PushAveragingGossip(NetworkFabric networkFabric, Gossip.MessageFabric gossipFabric) {
		super(networkFabric, gossipFabric);
//		System.out.println("NDB::PushAveragingGossip(" +networkFabric+ ",  "+gossipFabric+")");
	}
	
	public PushAveragingGossip(NetworkFabric networkFabric, Gossip.MessageFabric gossipFabric, double initial_x) {
		super(networkFabric, gossipFabric);
		this.x = initial_x;
		this.w = 1.0;		
//		System.out.println("NDB::PushAveragingGossip(" +networkFabric+ ",  "+gossipFabric+")");
	}

	public double getAverage() {
		return x/w;
	}

	@Override
	protected Gossip.Message.Payload getPushPayload() {
		MessagePayload payload = new MessagePayload();
		payload.x = x/2.0;
		payload.w = w/2.0;
		x = x/2.0;
		w = w/2.0;
		return payload;
				
	}

	@Override
	public void onPush(PushPullGossip.Message pushPullMessage) {
		MessagePayload payload = (MessagePayload)pushPullMessage.getPayload();
		x = payload.x + x;
		w = payload.w + w;
	}
	
	@Override
	public void onPull(PushPullGossip.Message pushPullMessage) {
		// Deliberatly do nothing as not allowed in this case.
		
	}

	public static void main(String[] args) {
		// initialise the random number generator
		Randomness.setSeed(71);
		
		PushPullGossip.pushGossip = true;
		PushPullGossip.pullGossip = false;
		
		NetworkFabric networkOverlay = new RandomNetworkFabric();

		List<PushAveragingGossip> gossips = new ArrayList<PushAveragingGossip>(N);
		List<ThreadedGossip> threadedGossips = new ArrayList<ThreadedGossip>(N);		
		for (int n = 0; n < N; n++) {
			double x = Randomness.nextDouble()*100.0;
			PushAveragingGossip pushAveragingGossip = new PushAveragingGossip(networkOverlay, new QueueMessagFabric(), x);
			networkOverlay.addGossip(pushAveragingGossip);	
			gossips.add(pushAveragingGossip);
			
			ThreadedGossip threadedGossip = new ThreadedGossip(pushAveragingGossip);
			threadedGossips.add(threadedGossip);
		}
		System.err.println("Initialised "+N+" PushAveragingGossip");
		
		double average = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).average().getAsDouble();
		double currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
		double currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
		System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);

		System.err.println("Starting all the threads");	
		threadedGossips.forEach(gossip -> gossip.start());
		
		while (currrentSD >= 0.001) {
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
			currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
			System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);
			
		}

		System.err.println("Shutting down, have converged");	
		threadedGossips.forEach(gossip -> gossip.shutdown());
		
		currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
		currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
		System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);
		
		System.out.println("Done!");	
					
		
	}

}
