package ie.nix.gossip.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ie.nix.gossip.Gossip;
import ie.nix.gossip.PushPullGossip;
import ie.nix.gossip.QueueMessagFabric;
import ie.nix.gossip.RandomNetworkFabric;
import ie.nix.util.Randomness;

public class PushPullAveragingGossip extends PushPullGossip {

	public class MessagePayload implements Gossip.Message.Payload {
		public double x;
	};
	
	// number of nodes
	static int N = 100;
	
	private double x;
	
	public PushPullAveragingGossip(NetworkFabric networkOverlay, MessageFabric gossipFabric) {
		super(networkOverlay, gossipFabric);
	}

	@Override
	public void onPush(PushPullGossip.Message pushPullMessage) {
		MessagePayload payload = (MessagePayload)pushPullMessage.getPayload();
		x = (payload.x + x)/2;
	}

	@Override
	public void onPull(PushPullGossip.Message pushPullMessage) {
		MessagePayload payload = (MessagePayload)pushPullMessage.getPayload();
		x = (payload.x + x)/2;
	}
	
	protected Gossip.Message.Payload getPushPayload() {
		MessagePayload payload = new MessagePayload();
		payload.x = this.x;
		return payload;
	}
	
	protected Gossip.Message.Payload getPullPayload() {
		MessagePayload payload = new MessagePayload();
		payload.x = this.x;
		return payload;
	}

	public static void main(String[] args) {		
		// initialise the random number generator
		Randomness.setSeed(71);
		
		PushPullGossip.pushGossip = true;
		PushPullGossip.pullGossip = true;
		
		NetworkFabric networkOverlay = new RandomNetworkFabric();

		List<PushAveragingGossip> gossips = new ArrayList<PushAveragingGossip>(N);
		List<ThreadedGossip> threadedGossips = new ArrayList<ThreadedGossip>(N);		
		for (int n = 0; n < N; n++) {
			double x = Randomness.nextDouble()*100.0;
			PushAveragingGossip pushAveragingGossip = new PushAveragingGossip(networkOverlay, new QueueMessagFabric(), x);
			networkOverlay.addGossip(pushAveragingGossip);	
			gossips.add(pushAveragingGossip);
			
			ThreadedGossip threadedGossip = new ThreadedGossip(pushAveragingGossip);
			threadedGossips.add(threadedGossip);
		}
		System.err.println("Initialised "+N+" PushPullAveragingGossip");
		
		double average = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).average().getAsDouble();
		double currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
		double currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
		System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);

		System.err.println("Starting all the threads");	
		threadedGossips.forEach(gossip -> gossip.start());
		
		while (currrentSD >= 0.000001) {
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
			currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
			System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);
			
		}

		System.err.println("Shutting down, have converged");	
		threadedGossips.forEach(gossip -> gossip.shutdown());
		
		currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
		currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
		System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);
		
		System.out.println("Done!");	
		
	}
}
