package ie.nix.gossip.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ie.nix.gossip.QueueMessagFabric;
import ie.nix.gossip.Gossip;
import ie.nix.gossip.PushPullGossip;
import ie.nix.gossip.RandomNetworkFabric;
import ie.nix.util.Randomness;


public class SIGossip extends PushPullGossip {

	enum SIState {
		Suseptable,
		Infected
	};
	
	SIState state;
	
	public SIGossip(NetworkFabric networkOverlay, Gossip.MessageFabric gossipFabric) {
		super(networkOverlay, gossipFabric);
		setState(SIState.Suseptable);
		
	}
	
	public synchronized void setState(SIState state) {
		this.state = state;
		
	}
	
	public synchronized SIState getState() {
		return this.state;
	}

	@Override
	public void onPush(PushPullGossip.Message pushPullMessage) {
//		System.out.println("~MessageHandler received 'Push' message");	
		if (getState() == SIState.Suseptable) {
			setState(SIState.Infected);
			incNumberOfNodesInfected();
	    	System.err.println("Number Of Nodes Infected is "+numberOfNodesInfected);
		}
	}
	
	@Override
	public void onPull(PushPullGossip.Message pushPullMessage) {
//		System.out.println("~MessageHandler received 'Pull' message");	
		if (getState() == SIState.Infected) {
			// send update message to sender.
			this.getMessageFabric().sendMessage(new PushPullGossip.Message(this, (SIGossip)pushPullMessage.getSender(), PushPullGossip.Message.Type.Push));
		}
	}

	// number of nodes
	static int N = 5;

	static int numberOfNodesInfected = 0;
	
	public synchronized static void incNumberOfNodesInfected() {
		numberOfNodesInfected++;
	}
	
	public synchronized static int numberOfNodesInfected() {
		return numberOfNodesInfected;
	}
	
	public static void main(String[] args) {
		// initialise the random number generator
		Randomness.setSeed(71);
		
		// Shared by all gossips.
		NetworkFabric networkOverlay = new RandomNetworkFabric();
		
		List<ThreadedGossip> gossips = new ArrayList<ThreadedGossip>(N);	
		for (int n = 0; n < N; n++) {
			SIGossip siGossip = new SIGossip(networkOverlay, new QueueMessagFabric());
			networkOverlay.addGossip(siGossip);	
//			System.err.println("Added gossip to NetworkOverly");
			
			ThreadedGossip threadedGossip = new ThreadedGossip(siGossip);
			gossips.add(threadedGossip);
		}
		System.err.println("Initialised "+N+" SIGossip");
		
		SIGossip randomSIGossip = (SIGossip)networkOverlay.getPeer();
    	randomSIGossip.setState(SIState.Infected);
		incNumberOfNodesInfected();
		System.err.println("Infected one SIGossip");
    	System.err.println("Number of Nodes Infected is "+numberOfNodesInfected);
		
		System.err.println("Starting all the threads");	
		gossips.forEach(gossip -> gossip.start());
		
		while (numberOfNodesInfected() < N) {
			System.err.println("Number of nodes infected is "+numberOfNodesInfected);		
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.err.println("Shutting down, all node infected");	
		gossips.forEach(node -> node.shutdown());
		
		System.err.println("Done!");	
		
	}
}
