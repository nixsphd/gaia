package ie.nix.gossip.threads;

import java.util.concurrent.TimeUnit;

import ie.nix.gossip.Gossip;
import ie.nix.gossip.Gossip.Message;

public class ThreadedGossip {
		
	private Thread messageHandlerThread;
	private Thread ativeThread;
	private Gossip gossip;
	
	// gossip cycle
	private int t = 1;

	public ThreadedGossip(Gossip gossip) {
		super();
		this.gossip = gossip;
	}

	public void start() {
//		System.out.println(Thread.currentThread().getName()+"~Starting");
		initMessageHandlerThread();
		initActiveThread();
		
	}
	
	public void initActiveThread() {
		ativeThread = new Thread(() -> {
			// loop
			while (!Thread.currentThread().isInterrupted()) {
//				System.out.println(Thread.currentThread().getName()+"~Gossiping...");
				gossip.gossip();
					
				// FIX wait (t)
				try {
					TimeUnit.SECONDS.sleep(t);	
				} catch (InterruptedException e) {
					break;
				}
			}
		});	
		ativeThread.start();
	}

	public void initMessageHandlerThread() {
		messageHandlerThread = new Thread(() -> {
			while (!Thread.currentThread().isInterrupted()) {
//				System.out.println(Thread.currentThread().getName()+"~Listening");
				Message message = gossip.getMessageFabric().nextMessage();
				if (message != null) {
					gossip.handleMessage(message);
				}
			}
		});	
		messageHandlerThread.start();
	}

	public void shutdown() {
//		System.out.println(Thread.currentThread().getName()+"~Shutting down");
		ativeThread.interrupt();
		messageHandlerThread.interrupt();
		
	}
	
}
