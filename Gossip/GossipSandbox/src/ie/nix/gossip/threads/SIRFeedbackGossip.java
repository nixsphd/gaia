package ie.nix.gossip.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ie.nix.gossip.AbstractGossip;
import ie.nix.gossip.AbstractMessage;
import ie.nix.gossip.Gossip;
import ie.nix.gossip.QueueMessagFabric;
import ie.nix.gossip.RandomNetworkFabric;
import ie.nix.util.Randomness;

public class SIRFeedbackGossip extends AbstractGossip { // extends PushPullGossip {
	
	public static class Message extends AbstractMessage {
		
		enum Type implements Gossip.Message.Type {
			Update,
			UpdateRequest,
			Feedback
		};
		
		public Message(Gossip sender, Gossip receiver, Type type) {
			super(sender, receiver, type, null);
			
		}
	
		public Message(Gossip sender, Gossip receiver, Type type, Payload payload) {
			super(sender, receiver, type, payload);
			
		}
		
		public Type getType() {
			return (Type)super.getType();
		}
	
		@Override
		public String toString() {
			return "Message [getType()=" + getType() + ", getSender()=" + getSender() + ", getReceiver()="
					+ getReceiver() + "]";
		}
		
	}

	enum State {
		Suseptable,
		Infected,
		Removed
	}
	
	public static boolean pushGossip = true;
	public static boolean pullGossip = true;
	
	// number of nodes
	static int N = 10;

	// number infected sp we cam stop when 0.
	static int numberOfNodesInfected = 0;
	
	// number removed
	static int numberOfNodesRemoved = 0;

	// feedback ratio
	static final double k = 1.0/5;
		
	public synchronized static int numberOfNodesInfected() {
		return numberOfNodesInfected;
	}

	public synchronized static void incNumberOfNodesInfected() {
		numberOfNodesInfected++;
	}

	public synchronized static void decNumberOfNodesInfected() {
		numberOfNodesInfected--;
	}

	public synchronized static int numberOfNodesRemoved() {
		return numberOfNodesRemoved;
	}

	public synchronized static void incNumberOfNodesRemoved() {
		numberOfNodesRemoved++;
	}

	State state;

	public SIRFeedbackGossip(NetworkFabric networkOverlay, MessageFabric gossipFabric) {
		super(networkOverlay, gossipFabric);
		setState(State.Suseptable);
		
	}
	
	public void gossip() {
//		System.out.println("NDB::SIRFeedbackGossip.gossip()");
		Gossip neighbour = getNetworkFabric().getPeerFor(this);
		if (pushGossip && getState() == State.Infected) {
			// Send a push
			getMessageFabric().sendMessage(new Message(this, neighbour, Message.Type.Update));
		}
		if (pullGossip) {
			// Send a push
			getMessageFabric().sendMessage(new Message(this, neighbour, Message.Type.UpdateRequest));
		}	
		
	}
	
	@Override
	public void handleMessage(Gossip.Message message) {
		if (message instanceof Message) {
			Message sIRFeedbackMessage = (Message)message;
			switch (sIRFeedbackMessage.getType()) {
				case Update: {
//				    System.out.println("SIRFeedbackGossip.handleMessage()~Received 'Update' message");
				    onUpdate(sIRFeedbackMessage);
					break;
				}
				case UpdateRequest: {
//				    System.out.println("SIRFeedbackGossip.handleMessage()~Received 'UpdateRequest' message");
					onUpdateRequest(sIRFeedbackMessage);
					break;
				}
				case Feedback: {
//				    System.out.println("SIRFeedbackGossip.handleMessage()~Received 'Feedback' message");
					onFeedback(sIRFeedbackMessage);
					break;
				}
			}
		}
		
	}
	
	private void onUpdate(Message sIRFeedbackMessage) {
//	    System.out.println("SIRFeedbackGossip.onUpdate()");
		if (getState() == State.Infected || getState() == State.Removed) {
//		    System.out.println("SIRFeedbackGossip.onUpdate()~Sending Feedback message.");
			getMessageFabric().sendMessage(new Message(this, sIRFeedbackMessage.getSender(), Message.Type.Feedback));
						
		} else {
		    if (getState() == State.Suseptable) {
				incNumberOfNodesInfected();
//		    	System.out.println("Number Of Nodes Infected is "+numberOfNodesInfected());
				setState(State.Infected);
			} else {
//			    System.out.println("SIRFeedbackGossip.update()~Not suseptable.");
			}
		}
	}
	
	private void onUpdateRequest(Message sIRFeedbackMessage) {
		if (getState() == State.Infected) {
			// send update message to sender.
			getMessageFabric().sendMessage(new Message(this, sIRFeedbackMessage.getSender(), Message.Type.Update));
		}
		
	}
	
	private void onFeedback(Message sIRFeedbackMessage) {
		if (Randomness.nextDouble() <= k) {
			incNumberOfNodesRemoved();
			decNumberOfNodesInfected();
//			System.out.println("Number Of Nodes Removed is "+numberOfNodesRemoved());
			setState(State.Removed);
		}
	}
				
	public synchronized void setState(State state) {
		this.state = state;
		
	}
	
	public synchronized State getState() {
		return this.state;
	}

	public static void main(String[] args) {
		// initialise the random number generator
		Randomness.setSeed(71);
		
		NetworkFabric networkOverlay = new RandomNetworkFabric();
		
		List<ThreadedGossip> gossips = new ArrayList<ThreadedGossip>(N);	
		for (int n = 0; n < N; n++) {
			SIRFeedbackGossip siGossip = new SIRFeedbackGossip(networkOverlay, new QueueMessagFabric());
			networkOverlay.addGossip(siGossip);	
			
			ThreadedGossip threadedGossip = new ThreadedGossip(siGossip);
			gossips.add(threadedGossip);
		}
		System.err.println("Initialised "+N+" SIRFeedbackGossip");
		
		SIRFeedbackGossip randomSIRFeedbackGossip = (SIRFeedbackGossip)networkOverlay.getPeer();
		randomSIRFeedbackGossip.setState(State.Infected);
		incNumberOfNodesInfected();
		System.err.println("Number of nodes infected is "+numberOfNodesInfected());	
		System.err.println("Number of nodes removed is "+numberOfNodesRemoved());		
		
		System.err.println("Starting all the threads");	
		gossips.forEach(gossip -> gossip.start());
		
		while (numberOfNodesRemoved() < N) {
			System.err.println("Number of nodes infected is "+numberOfNodesInfected());	
			System.err.println("Number of nodes removed is "+numberOfNodesRemoved());		
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.err.println("Shutting down, all node infected");	
		gossips.forEach(node -> node.shutdown());
		
		System.err.println("Done!");	

	}
}
