package ie.nix.gossip;

import ie.nix.gossip.Gossip;

public abstract class PushPullGossip extends AbstractGossip {

	public static boolean pushGossip = true;
	public static boolean pullGossip = true;
	
	public static class Message extends AbstractMessage {
		
		public enum Type implements Gossip.Message.Type {
			Push,
			Pull
		}
		
		public Message(Gossip sender, Gossip receiver, Type type) {
			super(sender, receiver, type, null);
			
		};

		public Message(Gossip sender, Gossip receiver, Type type, Payload payload) {
			super(sender, receiver, type, payload);
			
		};

		public Type getType() {
			return (Type)super.getType();
		}

		@Override
		public String toString() {
			return "Message [getType()=" + getType() + ", getSender()=" + getSender() + ", getReceiver()="
					+ getReceiver() + "]";
		}
		
	}
	
	public PushPullGossip(NetworkFabric networkOverlay, MessageFabric gossipFabric) {
		super(networkOverlay, gossipFabric);
		
	}

	public void handleMessage(Gossip.Message message) {
		if (message instanceof Message) {
			Message pushPullMessage = (Message)message;
			switch (pushPullMessage.getType()) {
				case Push: {
//					System.out.println(this+"~MessageHandler received 'Push' message");
					if (pullGossip) {
						// Send a pull
						getMessageFabric().sendMessage(new Message(this, pushPullMessage.getSender(),  PushPullGossip.Message.Type.Pull, getPullPayload()));
									
					}
					onPush(pushPullMessage);
					break;
				}
				case Pull: {
//					System.out.println(this+"~MessageHandler received 'Pull' message");
					onPull(pushPullMessage);
					break;
				}
			}
		}
	}
	
	public void gossip() {
//		System.out.println("NDB::PushPullGossip.gossip()");
		Gossip neighbour = getNetworkFabric().getPeerFor(this);
		if (pushGossip) {
			// Send a push
			getMessageFabric().sendMessage(new Message(this, neighbour, PushPullGossip.Message.Type.Push, getPushPayload()));
					
		}
	}

	protected Gossip.Message.Payload getPushPayload() {
		return null;
	}
	
	protected Gossip.Message.Payload getPullPayload() {
		return null;
	}
	
	public abstract void onPush(PushPullGossip.Message pushPullMessage);

	public abstract void onPull(PushPullGossip.Message pushPullMessage);
	
}
