package ie.nix.gossip.peersim;

import java.lang.reflect.InvocationTargetException;

import ie.nix.gossip.Gossip;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.Node;
import peersim.core.Control;
import peersim.core.GeneralNode;
import peersim.core.Network;
import peersim.edsim.EDProtocol;

public class PeerSimGossip extends GeneralNode implements Gossip {
	
	public static class Protocol implements CDProtocol, EDProtocol {
		
		public Protocol(String prefix) {
//			super(prefix);
		}
		
		public Object clone() {
			Protocol gossipProtocol = null;
			try { gossipProtocol = (Protocol) super.clone(); }
			catch( CloneNotSupportedException e ) {} // never happens
//			System.out.println("NDB::PeerSimGossipProtocol().clone()");
			return gossipProtocol;
		}
		
		@Override
		public void nextCycle(Node node, int protocolID) {
			System.out.println("NDB::AverageGossip.nextCycle("+node.getID()+", " +protocolID+")");

			PeerSimGossip peerSimGossip = (PeerSimGossip)node;
			peerSimGossip.gossip();
			
//			SingleValueNode svn = (SingleValueNode)node;
//			
//			Linkable linkable = (Linkable) node.getProtocol(FastConfig.getLinkable(protocolID));
//			 
//			 if (linkable.degree() > 0) {
//				 Node peerNode = linkable.getNeighbor(CommonState.r.nextInt(linkable.degree()));
//				 SingleValueNode svpn = (SingleValueNode)peerNode;
//		                // XXX quick and dirty handling of failures
//		                // (message would be lost anyway, we save time)
//				 if (!peerNode.isUp()) {
//					 return;
//				 } else {
//					 ((Transport)node.getProtocol(FastConfig.getTransport(protocolID))).send(
//							 node, peerNode, new AverageMessage(svn.getValue(),node), protocolID);
//				 }
//			 }

		}

		public void processEvent(Node node, int protocolID, Object event) {

			PeerSimGossip peerSimGossip = (PeerSimGossip)node;
			Gossip.Message message = (Gossip.Message)event;
			
			System.out.println("NDB::AverageGossip.processEvent("+node+", " +protocolID+", " +event+")");
			
			peerSimGossip.getDelegate().handleMessage(message);
			
//			SingleValueNode svn = (SingleValueNode)node;
//			AverageMessage aem = (AverageMessage)event;
//			/*
//			 * All we need to check whether the sender is null, because if it is, it 
//			 * means that we do not need to answer the message (it is already an answer).
//			 */
//			if (aem.sender != null) {
//				((Transport)node.getProtocol(FastConfig.getTransport(pid))).send(
//					node, aem.sender, new AverageMessage(svn.getValue(),null), pid);
//			}
//			svn.setValue((svn.getValue() + aem.value) / 2.0);

		}
		
	}
	
	public static class Init implements Control {

		protected static final String PAR_NETWORK_FABRIC = "networkfabric";
		protected static final String PAR_MESSAGE_FABRIC = "messagefabric";
		protected static final String PAR_DELEGATE = "delegate";
		
		protected String prefix;

		PeerSimGossip peerSimGossip;
		NetworkFabric networkFabric;
		MessageFabric gossipFabric;
		
		public Init(String prefix) {
//			super(prefix);
			this.prefix = prefix;
			System.out.println("NDB::PeerSimGossip.Init(" + prefix + ")");

		}
		
		@Override
		public boolean execute() {
			for (int i = 0; i < Network.size(); ++i) {
		        Node node = (Node)Network.get(i);
		        
		        peerSimGossip =  (PeerSimGossip) node;
		        networkFabric = (NetworkFabric) node.getProtocol(Configuration.getPid(prefix + "." + PAR_NETWORK_FABRIC));
				gossipFabric = (MessageFabric) node.getProtocol(Configuration.getPid(prefix + "." + PAR_MESSAGE_FABRIC));
			
				try {
					@SuppressWarnings("unchecked")
					Gossip gossipDelegate = (Gossip)(Configuration.getClass(prefix + "." + PAR_DELEGATE)
							.getConstructor(NetworkFabric.class, MessageFabric.class)
							.newInstance(networkFabric, gossipFabric));
					peerSimGossip.setDelegate(gossipDelegate);
					
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				}

//				System.out.println("NDB::PeerSimGossip$Init.execute("+node.getID()+")");
			}
			return false;
		}
		
	}

	protected Gossip delegate;
	
	public PeerSimGossip(String prefix) {
		super(prefix);
//		System.out.println("NDB::PeerSimGossip(" + prefix + ")");

	}	
	
	public Object clone() {
		PeerSimGossip gossip = (PeerSimGossip) super.clone();
//		System.out.println("NDB::PeerSimGossip().clone()");
		return gossip;
	}

	@Override
	public String toString() {
		return "PeerSimGossip [delegate=" + delegate + "]";
	}
	
	public Gossip getDelegate() {
		return delegate;
	}

	public void setDelegate(Gossip delegate) {
		System.out.println("NDB::PeerSimGossip.setDelegate(" + delegate + ")");
		this.delegate = delegate;
	}

	@Override
	public NetworkFabric getNetworkFabric() {
		return getDelegate().getNetworkFabric();
	}

	@Override
	public MessageFabric getMessageFabric() {
		return getDelegate().getMessageFabric();
	}

	@Override
	public void gossip() {
		getDelegate().gossip();
		
	}

	@Override
	public void handleMessage(Message message) {
		getDelegate().handleMessage(message);
		
	}

	public static void main(String[] args) {
		
		peersim.Simulator.main(args);
		
		// initialise the random number generator
//		Randomness.setSeed(71);
		
//		PushPullGossip.pushGossip = true;
//		PushPullGossip.pullGossip = false;
		
//		RandomNetworkFabric networkOverlay = new RandomNetworkFabric();

//		List<PushAveragingGossip> gossips = new ArrayList<PushAveragingGossip>(N);
//		List<ThreadedGossip> threadedGossips = new ArrayList<ThreadedGossip>(N);		
//		for (int n = 0; n < N; n++) {
//			double x = Randomness.nextDouble()*100.0;
//			PushAveragingGossip pushAveragingGossip = new PushAveragingGossip(networkOverlay, new QueueMessagFabric(), x);
//			networkOverlay.addGossip(pushAveragingGossip);	
//			gossips.add(pushAveragingGossip);
//			
//			ThreadedGossip threadedGossip = new ThreadedGossip(pushAveragingGossip);
//			threadedGossips.add(threadedGossip);
//		}
//		System.err.println("Initialised "+N+" PushAveragingGossip");
//		
//		double average = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).average().getAsDouble();
//		double currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
//		double currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
//		System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);
//
//		System.err.println("Starting all the threads");	
//		threadedGossips.forEach(gossip -> gossip.start());
//		
//		while (currrentSD >= 0.001) {
//			try {
//				TimeUnit.SECONDS.sleep(1);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			
//			currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
//			currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
//			System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);
//			
//		}
//
//		System.err.println("Shutting down, have converged");	
//		threadedGossips.forEach(gossip -> gossip.shutdown());
//		
//		currentAverage = gossips.stream().mapToDouble(gossip -> gossip.getAverage()).summaryStatistics().getAverage();
//		currrentSD = Math.sqrt(gossips.stream().mapToDouble(gossip -> Math.pow(gossip.getAverage() - average, 2.0)).sum() /(gossips.size()-1));
//		System.err.println("average="+average+", currentAverage "+currentAverage+", currrentSD="+currrentSD);
		
		System.out.println("Done!");	
					
		
	}

}
