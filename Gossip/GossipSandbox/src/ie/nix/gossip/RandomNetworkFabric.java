package ie.nix.gossip;

import java.util.ArrayList;
import java.util.List;

import ie.nix.gossip.Gossip;
import ie.nix.util.Randomness;

public class RandomNetworkFabric implements Gossip.NetworkFabric {
	
	// nodes in the system
	private List<Gossip> gossips;
	
	public RandomNetworkFabric() {
		this(0);		
	}
	
	public RandomNetworkFabric(int numberOfGossips) {
		gossips = new ArrayList<Gossip>(numberOfGossips);	
		
	}

	@Override
	public Gossip getPeer() {
		return gossips.get(Randomness.nextInt(gossips.size()-1));
	}

	@Override
	// return a random node but not one with id = notId.
	public Gossip getPeerFor(Gossip notThisGossip) {
		Gossip gossip = gossips.get(Randomness.nextInt(gossips.size()-1));
		while (notThisGossip == gossip) {
			gossip = gossips.get(Randomness.nextInt(gossips.size()-1));
		}
		return gossip;	
	}

	@Override
	public void addGossip(Gossip gossip) {
		gossips.add(gossip);
		
	}

	@Override
	public void addGossips(List<? extends Gossip> gossips) {
		this.gossips.addAll(gossips);	
		
	}

}
