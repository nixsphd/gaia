package ie.nix.gossip.sandbox;

import ie.nix.util.Randomness;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

public class SIRCounterWithFeedbackGossip {
	
	public static class Message {
		enum MessageType {
			Update,
			UpdateRequest,
			Feedback
		};
		public final Node sender;
		public final MessageType type;
		
		public Message(Node sender, MessageType type) {
			this.sender = sender;
			this.type = type;
		}
		
	}
	
	public static class Node {
		
		enum State {
			Suseptable,
			Infected,
			Removed
		};
		
		public final int id;
		boolean push = true;
		boolean pull = true;
		State state;
		int numberOfUpdates = 0;
		final Queue<Message> messages;
		Thread messageHandlerThread;
		Thread ativeThread;
		
		public Node(int id) {
			this.id = id;
			state = State.Suseptable;
			messages = new LinkedList<Message>();
		
		}

		public void init() {
			initMessageHandlerThread();
			initActiveThread();
		}

		public void initActiveThread() {
			ativeThread = new Thread(() -> {
				// loop
				while (!Thread.currentThread().isInterrupted()) {
//			    	System.out.println(id+"~Being active...");
			    	
					// p <- randon peer
					Node p = getRandomPeerNot(id);
					
					// if push and state in I then 
					if (push && (state == State.Infected)) {
						// send update to p
//				    	System.out.println(id+"~Active sending 'Update' message to "+p.id);
						p.messages.add(new Message(this, Message.MessageType.Update));
					}
					// endif
						
					// if pull then
					if (pull && (state != State.Removed)) {
						// send update-request to p
						p.messages.add(new Message(this, Message.MessageType.UpdateRequest));
					}
					// endif
						
					// wait (t)
					try {
						TimeUnit.SECONDS.sleep(t);	
					} catch (InterruptedException e) {
						break;
					}
				}
			});	
			ativeThread.start();
		}

		public void initMessageHandlerThread() {
			messageHandlerThread = new Thread(() -> {
				while (!Thread.currentThread().isInterrupted()) {
//			    	System.out.println(id+"~Checking for messages....");
					Message message = messages.poll();
					if (message != null) {
						switch (message.type) {
							case Update: {
//						    	System.out.println(id+"~MessageHandler received 'Update' message");
								update();
								break;
							}
							case UpdateRequest: {
//						    	System.out.println(id+"~MessageHandler received 'UpdateRequest' message");
								updateRequest(message.sender);
								break;
							}
							case Feedback: {
//						    	System.out.println(id+"~MessageHandler received 'Feedback' message");
								feedback(message.sender);
								break;
							}
						}
					}
				}
			});	
			messageHandlerThread.start();
		}
		
		public void shutdown() {
	    	System.out.println(id+"~shutdown");
	    	ativeThread.interrupt();
			messageHandlerThread.interrupt();
		}
			
		public synchronized void setState(State state) {
			this.state = state;
			
		}
		
		public State getState() {
			return this.state;
		}
		
		public void updateRequest(Node sender) {
			if (state == State.Infected || state == State.Removed) {
				// send update message to sender.
				sender.messages.add(new Message(this, Message.MessageType.Update));
			}
		}

		public void update() {
			if (state == State.Suseptable) {
				incNumberOfNodesInfected();
		    	System.out.println("Number Of Nodes Infected is "+numberOfNodesInfected());
				setState(State.Infected);
				numberOfUpdates++;
				
			} else {
				feedback(this);
			}
			
		}

		public void feedback(Node sender) {
			if (Randomness.nextDouble() <= k) {
				incNumberOfNodesRemoved();
				decNumberOfNodesInfected();
				System.out.println("Number Of Nodes Removed is "+numberOfNodesRemoved());
				setState(State.Removed);
			}
		}
			
	}
		

	// number of nodes
	static int N = 10;
	
	// nodes in the system
	static List<Node> nodes = new ArrayList<Node>(N);
	
	// number infected sp we cam stop when 0.
	static int numberOfNodesInfected = 0;
	
	// number removed
	static int numberOfNodesRemoved = 0;

	// gossip cycle
	static final int t = 1;
	
	// feedback ratio
	static final double k = 1.0/5;
	
	// allowedNumberOfUpdates
	static int allowedNumberOfUpdates = 10;
	
	// return a random node.
	public static Node getRandomPeer() {
		return nodes.get(Randomness.nextInt(N-1));
		
	};
	
	// return a random node but not one with id = notId.
	public static Node getRandomPeerNot(int notId) {
		Node peer = nodes.get(Randomness.nextInt(N-1));
		while (peer.id == notId) {
			peer = nodes.get(Randomness.nextInt(N-1));
		}
		return peer;
		
	};
	
	public synchronized static int numberOfNodesInfected() {
		return numberOfNodesInfected;
	}
	
	public synchronized static void incNumberOfNodesInfected() {
		numberOfNodesInfected++;
	}
	
	public synchronized static void decNumberOfNodesInfected() {
		numberOfNodesInfected--;
	}

	public synchronized static int numberOfNodesRemoved() {
		return numberOfNodesRemoved;
	}
	
	public synchronized static void incNumberOfNodesRemoved() {
		numberOfNodesRemoved++;
	}
	
	public static void main(String[] args) {
		// initialise the random number generator
		Randomness.setSeed(71);
		
		nodes = new ArrayList<Node>(N);
		for (int n = 0; n < N; n++) {
			nodes.add(n, new Node(n));
		}

		System.err.println("Starting all the threads");	
		nodes.forEach(node -> node.init());
		
		Node starterNode = getRandomPeer();
		starterNode.update();
		
		while (numberOfNodesRemoved() < N) {
//			System.err.println("Number of nodes infected is "+numberOfNodesInfected());		
//			System.err.println("Number of nodes removed is "+numberOfNodesRemoved());		
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.err.println("Shutting down, all node removed");	
		nodes.forEach(node -> node.shutdown());
		
		System.out.println("Done!");	
		
	}
}
