package ie.nix.gossip;

import java.util.List;

public interface Gossip {
	
	public interface NetworkFabric {

		public Gossip getPeer();

		public Gossip getPeerFor(Gossip notThisGossip);

		public void addGossip(Gossip gossip);
		
		public void addGossips(List<? extends Gossip> gossips);
		
	}
	
	public interface MessageFabric {

		public void sendMessage(Message message);

		public void receiveMessage(Message message);
		
		// Can return null if there is no message.
		public Message nextMessage();
		
	}
	
	public interface Message {

		public interface Type {};
		
		public interface Payload {};
		
		public Gossip getSender();

		public Gossip getReceiver();

		public Type getType();

		// This might need to be serialisable later.
		public Payload getPayload();
		
	}
	
	public NetworkFabric getNetworkFabric();
	
	public MessageFabric getMessageFabric();

	public void gossip();
	
	public void handleMessage(Message message);
	
}
