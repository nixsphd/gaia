package ie.nix.peersim.vector;

import ie.nix.peersim.sandbox.SingleValueNode;
import peersim.core.CommonState;
import peersim.core.Node;
import peersim.core.Protocol;

public class SetterGetterNodeAdapter implements Protocol {
	
	public SetterGetterNodeAdapter(String prefix) {
		System.out.println("NDB::NodeAdaptor(" + prefix + ")");

	}

	public Object clone() {
		SetterGetterNodeAdapter nodeAdaptor = null;
		try { 
			nodeAdaptor = (SetterGetterNodeAdapter) super.clone(); 
		} catch (CloneNotSupportedException e) {
			// never happens
		} 
		return nodeAdaptor;
	}
	
	/**
	 * @inheritDoc
	 */
	public double getValue() {
		Node node = CommonState.getNode();
		if (node instanceof SingleValueNode) {
			SingleValueNode singleValueNode = (SingleValueNode)node;
			return singleValueNode.getValue();
		} else {
			throw new RuntimeException("The node "+node+" is not a SingleValueNode");
		}
	}

	//--------------------------------------------------------------------------

	/**
	 * @inheritDoc
	 */
	public void setValue(double value) {
		this.value = value;
	}
}

