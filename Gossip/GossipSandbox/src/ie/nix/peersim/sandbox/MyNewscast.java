package ie.nix.peersim.sandbox;

import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;

/*
 * Newscast is an epidemic content distribution and topology management protocol. 
 * Every peer in the system has a partial view of the set of nodes which is in fact 
 * a fixed size set of node descriptors. Each descriptor is a tuple consisting of 
 * a peer address and a time-stamp recording the time when the descriptor was created.
 * Each node updates its state by choosing a random neighbor and exchanging views. 
 * After the exchange both peers merge the two views and keep the freshest entries 
 * only. In this manner, old information (descriptors) is removed from the system. 
 * This process allows the protocol to repair the overlay topology removing dead 
 * links with minimum effort which is very useful in highly dynamic system where 
 * nodes join and leave continuously.
 */
public class MyNewscast implements CDProtocol, Linkable {

	/**
	 * Cache size.
	 * @config
	 */
	private static final String PAR_CACHE_NEIGHBOURS = "cached-neighbours";
	
	/** Neighbors currently in the cache */
	private Node[] neighbours;
	
	/** Time stamps currently in the cache */
	private int[] timestamps;
	
	public MyNewscast(String n) {
		final int neighbourCacheSize = Configuration.getInt(n + "." + PAR_CACHE_NEIGHBOURS);
		if (MyNewscast.tn == null || MyNewscast.tn.length < neighbourCacheSize) {
			MyNewscast.tn = new Node[neighbourCacheSize];
			MyNewscast.ts = new int[neighbourCacheSize];
		}
		neighbours = new Node[neighbourCacheSize];
		timestamps = new int[neighbourCacheSize];
	}
	
	/*
	 * Object
	 */
	public Object clone() {
		/*
		 * Do the super.clone()...
		 */
		MyNewscast clone = null;
		try { 
			clone = (MyNewscast) super.clone(); 
		} catch( CloneNotSupportedException e ) {
			// never happens
		} 
		
		/*
		 * Create the neighbour and timestamp arrays.
		 */
		clone.neighbours = new Node[neighbours.length];
		clone.timestamps = new int[timestamps.length];
		/*
		 * Copy the content of the neighbour and timestamp arrays.
		 */
		System.arraycopy(neighbours, 0, clone.neighbours, 0, neighbours.length);
		System.arraycopy(timestamps, 0, clone.timestamps, 0, timestamps.length);
		
		return clone;
	}
	
	public String toString() {
		if( neighbours == null ) return "DEAD!";
		
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < degree(); ++i) {
			sb.append(" (" + neighbours[i].getIndex() + "," + timestamps[i] + ")");
		}
		return sb.toString();
	}
	
	/*
	 * Cleanable
	 */
	@Override
	public void onKill() {
		/*
		 * Dereference all arrays so they can be garbage collected.
		 */
		neighbours = null;
		timestamps = null;
	}

	/*
	 * Linkable
	 */
	@Override
	public int degree() {
		/** Might be less than cache size. So count backwards from the end while the array is 
		 * empty
		 */
		int len = neighbours.length - 1;
		while (len >= 0 && neighbours[len] == null)
			len--;
		return len + 1;
	}

	@Override
	public Node getNeighbor(int i) {
		return neighbours[i];
	}

	@Override
	public boolean addNeighbor(Node neighbour) {
		/*
		 * Find the first free inter, i, to add in the new neighbour. 
		 * if the neighbour is already included return false.
		 */
		int i;
		for (i = 0; i < neighbours.length && neighbours[i] != null; i++) {
			if (neighbours[i] == neighbour)
				return false;
		}
		
		
		if (i < neighbours.length) {
			/*
			 * If this is not the first neighbuor, and the previously added timestamp is in the past.
			 */
			if (i > 0 && timestamps[i - 1] < CommonState.getIntTime()) {
				/*
				 *  we need to insert to the first position, so moving everything along by 1. and set the index
				 *  to 0.
				 */
				for (int j = neighbours.length - 2; j >= 0; --j) {
					neighbours[j + 1] = neighbours[j];
					timestamps[j + 1] = timestamps[j];
				}
				i = 0;
			}
			/*
			 * Add the neighbour and the current timestamp.
			 */
			neighbours[i] = neighbour;
			timestamps[i] = CommonState.getIntTime();
			return true;
			
		} else {
			/*
			 * If their is no empty space to add the neighbour throw an IndexOutOfBounds Exception.
			 */
			throw new IndexOutOfBoundsException();
		}
	}

	@Override
	public boolean contains(Node neighbor) {
		/* 
		 * loop through all the neighbours looking for a match
		 */
		for (int i = 0; i < neighbours.length; i++) {
			if (neighbours[i] == neighbor)
				return true;
		}
		return false;
	}

	@Override
	public void pack() {
		/*
		 * Do nothing
		 */
	}
	
	/*
	 * CDProtocol
	 */

	/*
	 *  We are using static temporary arrays to avoid garbage collection
	 *  of them. these are used by all SimpleNewscast protocols included
	 *  in the protocol array so its size is the maximum of the cache sizes
	 */
	/** Temp array for merging. Its size is the same as the neighbour array size. */
	private static Node[] tn;

	/** Temp array for merging. Its size is the same as the neighbour array size. */
	private static int[] ts;
	
	@Override
	public void nextCycle(Node node, int protocolID) {
		
//		System.out.println("NDB::MyNewscast.nextCycle(" + node.getID() + "," + protocolID + ")");

		/*
		 * Get a peer, returning if their isn't one accessabe.
		 */
		Node peerNode = getPeer();
		if (peerNode == null) {
			System.err.println("Newscast: no accessible peer");
			return;
		}

		/* 
		 * Get the MyNewscast protocol for that peer and build the static mergeed meighbout list.
		 */
		MyNewscast peer = (MyNewscast) (peerNode.getProtocol(protocolID));
		merge(node, peer, peerNode);

		/*
		 *  Set new merged neighbout list in this and the peer.
		 */
		System.arraycopy(MyNewscast.tn, 0, neighbours, 0, neighbours.length);
		System.arraycopy(MyNewscast.ts, 0, timestamps, 0, timestamps.length);
		System.arraycopy(MyNewscast.tn, 0, peer.neighbours, 0, neighbours.length);
		System.arraycopy(MyNewscast.ts, 0, peer.timestamps, 0, timestamps.length);

		/*
		 * Set first element to be each other
		 */
		neighbours[0] = peerNode;
		peer.neighbours[0] = node;
		/*
		 * Set the timestamps to be now.
		 */
		timestamps[0] = peer.timestamps[0] = CommonState.getIntTime();
		
	}

	/**
	 * Returns a peer node which is accessible (has ok fail state). This
	 * implementation starts with a random node. If that is not reachable,
	 * proceed first towards the older and then younger nodes until the first
	 * reachable is found.
	 * @return null if no accessible peers are found, the peer otherwise.
	 */
	private Node getPeer() {
		final int d = degree();
		if (d == 0)
			return null;
		/*
		 * Select a random valid index, and the node at that index
		 */
		int index = CommonState.r.nextInt(d);
		Node result = neighbours[index];

		/*
		 * If it up return it
		 */
		if (result.isUp())
			return result;

		/*
		 * Otherwise, proceed towards older entries, if one is found return it
		 */
		for (int i = index + 1; i < d; ++i)
			if (neighbours[i].isUp())
				return neighbours[i];

		/*
		 * Failing that, proceed towards younger entries, if one is found return it.
		 */
		for (int i = index - 1; i >= 0; --i)
			if (neighbours[i].isUp())
				return neighbours[i];

		/*
		 * If this fails then their is no accessible peer, so return null.
		 */
		return null;
		
	}
	
	/*
	 * Check if a node is in a merged neighbour list.
	 */
	private static boolean contains(int size, Node peer) {
		for (int i = 0; i < size; i++) {
			if (MyNewscast.tn[i] == peer)
				return true;
		}
		return false;
	}
	
	/**
	 * Merge the content of two nodes and adds a new version of the identifier.
	 * The result is in the static temporary arrays. The first element is not
	 * defined, it is reserved for the freshest new updates so it will be
	 * different for peer and this. The elements of the static temporary arrays
	 * will not contain neither peerNode nor thisNode.
	 * @param myNode
	 *          the node that hosts this newscast protocol instance (process)
	 * @param peerNewscast
	 *          The peer with which we perform cache exchange
	 * @param peerNode
	 *          the node that hosts the peer newscast protocol instance
	 */
	private void merge(Node myNode, MyNewscast peerNewscast, Node peerNode) {
		int i1 = 0; /* Index first neighbours */
		int i2 = 0; /* Index second neighbours */
		boolean first;
		boolean lastTieWinner = CommonState.r.nextBoolean();
		int i = 1; // Index new cache. first element set in the end
		// MyNewscast.tn[0] is always null. it's never written anywhere
		final int d1 = degree();
		final int d2 = peerNewscast.degree();
		// cachesize is cache.length

		// merging two arrays
		/*
		 * While the new neighbours array is not full and their are nodes in the merging 
		 * neighbour arrays, continue.
		 */
		while (i < neighbours.length && i1 < d1 && i2 < d2) {
			
			if (timestamps[i1] == peerNewscast.timestamps[i2]) {
				/*
				 * If the time stamp are the same first is lastTieWinner,
				 * and lastTieWinner if flipped.
				 */
				lastTieWinner = first = !lastTieWinner;
//				lastTieWinner = first;
//				first = !lastTieWinner;
			} else {
				/*
				 * Otherwise, first is the one with the greater timestamp 
				 */
				first = (timestamps[i1] > peerNewscast.timestamps[i2]);
			}

			if (first) {
				/*
				 * The first has the greatest timestamp. If we're not adding the peerNode and 
				 * that it's not already included. 
				 */
				if (neighbours[i1] != peerNode && !MyNewscast.contains(i, neighbours[i1])) {
					/*
					 * Add the node and it's timestamp to the merged neighbour lists
					 */
					MyNewscast.tn[i] = neighbours[i1];
					MyNewscast.ts[i] = timestamps[i1];
					i++;
				}
				/*
				 * Increment the first peer index.
				 */
				i1++;
			} else {
				/*
				 * The second has the greatest timestamp. If we're not adding the peerNode and 
				 * that it's not already included. 
				 */
				if (peerNewscast.neighbours[i2] != myNode && !MyNewscast.contains(i, peerNewscast.neighbours[i2])) {
					MyNewscast.tn[i] = peerNewscast.neighbours[i2];
					MyNewscast.ts[i] = peerNewscast.timestamps[i2];
					i++;
				}
				/*
				 * Increment the first peer index.
				 */
				i2++;
			}
		}

		/*
		 *  If one of the original arrays got fully copied into tn and there
		 *  is still place, fill the rest with the other array
		 */
		if (i < neighbours.length) {
			/*
			 *  only one of the for cycles will be entered
			 */
			for (; i1 < d1 && i < neighbours.length; ++i1) {
				if (neighbours[i1] != peerNode && !MyNewscast.contains(i, neighbours[i1])) {
					MyNewscast.tn[i] = neighbours[i1];
					MyNewscast.ts[i] = timestamps[i1];
					i++;
				}
			}

			for (; i2 < d2 && i < neighbours.length; ++i2) {
				if (peerNewscast.neighbours[i2] != myNode && !MyNewscast.contains(i, peerNewscast.neighbours[i2])) {
					MyNewscast.tn[i] = peerNewscast.neighbours[i2];
					MyNewscast.ts[i] = peerNewscast.timestamps[i2];
					i++;
				}
			}
		}

		/*
		 *  If the two arrays were not enough to fill the buffer
		 *  fill in the rest with nulls
		 */
		if (i < neighbours.length) {
			for (; i < neighbours.length; ++i) {
				MyNewscast.tn[i] = null;
			}
		}
	}
}
