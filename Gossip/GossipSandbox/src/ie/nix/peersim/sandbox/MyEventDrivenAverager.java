package ie.nix.peersim.sandbox;

import peersim.cdsim.CDProtocol;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;
import peersim.vector.SingleValueHolder;

//public class MyEventDrivenAverager extends SingleValueHolder implements CDProtocol, EDProtocol {
public class MyEventDrivenAverager implements CDProtocol, EDProtocol {

	/**
	* The type of a message. It contains a value of type double and the
	* sender node of type {@link peersim.core.Node}.
	* 
	* This private class is the type of the message that the protocol uses.
	*  It is private because no other component has anything to do with the 
	*  message type.
	*/
	private static class AverageMessage {
		final double value;
        /** If not null,
        this has to be answered, otherwise this is the answer. */
        final Node sender;
	        
        public AverageMessage( double value, Node sender ) {
            this.value = value;
            this.sender = sender;
        } 
	}
	
	public MyEventDrivenAverager(String prefix) {
//		super(prefix);
	}

	/*
	 * Cloneable
	 */
//	@Override
//	public Object clone() {
//		return super.clone();
//	}
	/**
	 * Clones the value holder.
	 */
	public Object clone() {
		MyEventDrivenAverager svh=null;
		try { 
			svh=(MyEventDrivenAverager)super.clone(); 
		} catch (CloneNotSupportedException e ) {} // never happens
		return svh;
	}

	/*
	 * CDProtocol
	 */
	@Override
	public void nextCycle(Node node, int protocolID) {

		SingleValueNode svn = (SingleValueNode)node;
		
		Linkable linkable = (Linkable) node.getProtocol(FastConfig.getLinkable(protocolID));
		 
		 if (linkable.degree() > 0) {
			 Node peerNode = linkable.getNeighbor(CommonState.r.nextInt(linkable.degree()));
			 SingleValueNode svpn = (SingleValueNode)peerNode;
	                // XXX quick and dirty handling of failures
	                // (message would be lost anyway, we save time)
			 if (!peerNode.isUp()) {
				 return;
			 } else {
				 ((Transport)node.getProtocol(FastConfig.getTransport(protocolID))).send(
						 node, peerNode, new AverageMessage(svn.getValue(),node), protocolID);
			 }
		 }

	}

	/*
	 * EDProtocol
	 */
	public void processEvent(Node node, int pid, Object event) {
		SingleValueNode svn = (SingleValueNode)node;
		AverageMessage aem = (AverageMessage)event;
		/*
		 * All we need to check whether the sender is null, because if it is, it 
		 * means that we do not need to answer the message (it is already an answer).
		 */
		if (aem.sender != null) {
			((Transport)node.getProtocol(FastConfig.getTransport(pid))).send(
				node, aem.sender, new AverageMessage(svn.getValue(),null), pid);
		}
		svn.setValue((svn.getValue() + aem.value) / 2.0);

	}
}
