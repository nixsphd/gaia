package ie.nix.peersim.sandbox;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import peersim.config.Configuration;
import peersim.core.Node;
import peersim.graph.Graph;
import peersim.reports.GraphObserver;
import peersim.util.FileNameGenerator;

public class GraphPrinter extends GraphObserver {

	private static final String PAR_FILENAME_BASE = "file_base";
	private static final String PAR_COORDINATES_PROT = "coord_protocol";

    private final String graph_filename;
    private final FileNameGenerator fng;
    private final int coordPid;


	public GraphPrinter(String prefix) {
		super(prefix);
		coordPid = Configuration.getPid(prefix + "." + PAR_COORDINATES_PROT);
	    graph_filename = Configuration.getString(prefix + "." + PAR_FILENAME_BASE, "graph_dump");
	    fng = new FileNameGenerator(graph_filename, ".dat");
	}

	@Override
	public boolean execute() {
		try {
			// must call this first...
	        updateGraph();
	        System.out.print(name + ": ");
	        // initialize output streams
	        FileOutputStream fos = new FileOutputStream(fng.nextCounterName());
	        PrintStream printStream = new PrintStream(fos);
	        // dump topology:
	        graphToFile(g, printStream, coordPid);
	        fos.close();
	        System.out.println("Wrote to file " + fng.nextCounterName());
	        
	    } catch (IOException e) {
	        throw new RuntimeException(e);
	    }
	    return false;
	}

	private void graphToFile(Graph graph, PrintStream printStream, int coordPid) {
		for (int i = 1; i < graph.size(); i++) {
	        Node node = (Node)graph.getNode(i);
	        double x_to = ((MyCoords)node.getProtocol(coordPid)).getX();
	        double y_to = ((MyCoords)node.getProtocol(coordPid)).getY();
	        for (int index : graph.getNeighbours(i)) {
	        	Node neighbourNode = (Node) graph.getNode(index);
	        	double x_from = ((MyCoords) neighbourNode.getProtocol(coordPid)).getX();
	        	double y_from = ((MyCoords) neighbourNode.getProtocol(coordPid)).getY();
//	        	 n.neighbor(i).x n.neighbor(i).y \newline
//	        	 n.x n.y \newline
//	        	 \newline}
	        	printStream.println(x_from + " " + y_from);
	        	printStream.println(x_to + " " + y_to);
	        	printStream.println();
	        } 
        }	
	}
}
