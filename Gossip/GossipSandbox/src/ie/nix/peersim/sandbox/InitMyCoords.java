package ie.nix.peersim.sandbox;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;

public class InitMyCoords implements Control {

	private static final String PAR_PROT = "protocol";
   
	/** Protocol identifier, obtained from config property {@link #PAR_PROT}. */
    private static int pid;
    
    public InitMyCoords(String prefix) {
        pid = Configuration.getPid(prefix + "." + PAR_PROT);
    }
    
	@Override
	public boolean execute() {
		/**
		 * Initialize the node coordinates. The first node in the
		 * {@link Network} is the root node by default and it is located in the
		 * middle (the center of the square) of the surface area.
		 */
		Node node = Network.get(0);
		MyCoords myCoords = (MyCoords)node.getProtocol(pid);
		myCoords.setX(0.5);
		myCoords.setY(0.5);
		
		/*
		 * Randomly assign the coords between 0.0 and 1.0.
		 */
        for (int i = 1; i < Network.size(); i++) {
        	node = Network.get(i);
            myCoords = (MyCoords)node.getProtocol(pid);
            myCoords.setX(CommonState.r.nextDouble());
            myCoords.setY(CommonState.r.nextDouble());
        }
        return false;
	}

}
