package ie.nix.peersim.sandbox;

import peersim.cdsim.CDProtocol;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.vector.SingleValueHolder;

//public class MyAverager extends SingleValueHolder implements CDProtocol {
public class MyAverager implements CDProtocol {

//	String prefix;

	public MyAverager(String prefix) {
//		super(prefix);
//		this.prefix = prefix;
//		System.out.println("NDB::MyAverager(" + prefix + ")");
	}

//	@Override
//	public Object clone() {
////		System.out.println("NDB::MyAverager.clone()");
//		return super.clone();
//
//	}
	
	/**
	 * Clones the value holder.
	 */
	public Object clone() {
		SingleValueHolder svh=null;
		try { 
			svh=(SingleValueHolder)super.clone(); 
		} catch (CloneNotSupportedException e ) {} // never happens
		return svh;
	}

	@Override
	public void nextCycle(Node node, int protocolID) {
//		System.out.println("NDB::MyAverager.nextCycle(" + node.getID() + "," + protocolID + ")");
		
		SingleValueNode svn = (SingleValueNode)node;

		int linkableID = FastConfig.getLinkable(protocolID);
		Linkable linkable = (Linkable) node.getProtocol(linkableID);

//		System.out.println("NDB::MyAverager.nextCycle()~linkable=" + linkable);
//		System.out.println("NDB::MyAverager.nextCycle()~value=" + value);
		
        if (linkable.degree() > 0) {
            Node peer = linkable.getNeighbor(CommonState.r.nextInt(linkable.degree()));
    		SingleValueNode svpn = (SingleValueNode)peer;

            // Failure handling
            if (!peer.isUp())
                return;

//            MyAverager neighbor = (MyAverager) peer.getProtocol(protocolID);
//            double mean = (this.value + neighbor.value) / 2;
//            this.value = mean;
//            neighbor.value = mean;
            double mean = (svn.value + svpn.value) / 2;
            svn.value = mean;
            svpn.value = mean;
        }
        
	}

}
