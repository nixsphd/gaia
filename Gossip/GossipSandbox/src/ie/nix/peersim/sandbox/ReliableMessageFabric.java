package ie.nix.gossip.peersim;

import peersim.config.Configuration;
import peersim.core.Node;
import peersim.transport.Transport;
import  peersim.transport.UniformRandomTransport;
import ie.nix.gossip.Gossip.Message;
import ie.nix.gossip.Gossip.MessageFabric;
import ie.nix.gossip.Gossip.NetworkFabric;

public class ReliableMessageFabric implements Transport, MessageFabric {

	private static final String PAR_PROTOCOL = "protocol";
	
	protected UniformRandomTransport transport;
	protected int pid;
	
	public ReliableMessageFabric(String prefix) {
		transport = new UniformRandomTransport(prefix);
		pid = Configuration.getPid(prefix + "." + PAR_PROTOCOL);
//		System.out.println("NDB::ReliableMessageFabric(" + prefix + ")");

	}
	
	public Object clone() {
//		System.out.println("NDB::ReliableMessageFabric.clone()->"+this);
		return this;
	}
	
	@Override
	public String toString() {
		return "ReliableMessageFabric [transport=" + transport.getClass().getSimpleName() + "]";
	}

	@Override
	public void sendMessage(Message message) {
		System.out.println("NDB::ReliableMessageFabric.sendMessage(" + message + ")");
		transport.send(message.getSender(), message.getReceiver(), message, pid);

	}

	@Override
	public void receiveMessage(Message message) {
		System.out.println("NDB::ReliableMessageFabric.receiveMessage(" + message + ")");
//		transport.

	}

	@Override
	public Message nextMessage() {
		System.out.println("NDB::ReliableMessageFabric.nextMessage()");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void send(Node src, Node dest, Object msg, int pid) {
		transport.send(src, dest, msg, pid);
	}

	@Override
	public long getLatency(Node src, Node dest) {
		return transport.getLatency(src, dest);
	}

}
