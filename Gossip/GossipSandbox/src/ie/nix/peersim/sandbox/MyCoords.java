package ie.nix.peersim.sandbox;

import peersim.core.Protocol;

public class MyCoords implements Protocol {

	/** 2d coordinates components. */
    private double x, y;
    
	public MyCoords(String prefix) {
		x = y = -1;
	}

	public Object clone() {
		MyCoords newMyCoords;
		try {
			newMyCoords = (MyCoords)super.clone();
			newMyCoords.x = x;
			newMyCoords.y = y;
			return newMyCoords;
		} catch (CloneNotSupportedException e) {
			return null;
		}
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
    
    
    
}
