package ie.nix.gossip.peersim;

import java.util.List;

import ie.nix.gossip.Gossip;
import ie.nix.gossip.Gossip.NetworkFabric;
import peersim.core.CommonState;
import peersim.core.IdleProtocol;
import peersim.core.Node;

public class RandomNetworkFabric extends IdleProtocol implements NetworkFabric {

	public RandomNetworkFabric(String prefix) {
		super(prefix);
//		System.out.println("NDB::RandomNetworkFabric(" + prefix + ")");
	
	}
	
	public Object clone()
	{
		Object clone = super.clone();
//		System.out.println("NDB::RandomNetworkFabric.clone()->"+clone);
		return clone;
	}
	
	@Override
	public String toString() {
		return "RandomNetworkFabric [neighbors.len=" + len + "]";
	}

	@Override
	public Gossip getPeer() {
		Node node = getNeighbor(CommonState.r.nextInt(degree()));
		PeerSimGossip peerSimGossip = (PeerSimGossip)node;
		return peerSimGossip.getDelegate();
		
        // Failure handling
//        if (!peer.isUp())
//            return;
	}

	@Override
	public Gossip getPeerFor(Gossip notThisGossip) {
		Gossip gossip = getPeer();
		while (notThisGossip == gossip) {
			gossip = getPeer();
		}
		return gossip;
	}

	@Override
	public void addGossip(Gossip gossip) {
//		Node node = (Node)gossip;
//		addNeighbor(node);
		
	}

	@Override
	public void addGossips(List<? extends Gossip> gossips) {
//		for (Gossip gossip: gossips) {
//			addGossip(gossip);
//		}
	}
		
}
