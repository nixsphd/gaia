package ie.nix.peersim.sandbox;

import peersim.config.Configuration;
import peersim.core.Control;
import peersim.core.Network;
import peersim.util.IncrementalStats;
import peersim.vector.SingleValue;

public class MyObserver implements Control {

    private static final String PAR_PROT = "protocol";
    
    private final int pid;
    
    private final String name;
    

	/**
     * Creates a new observer reading configuration parameters.
     */
    public MyObserver(String name) {
        this.pid = Configuration.getPid(name + "." + PAR_PROT);
        this.name = name;
    }
    
	protected int getProtocolID() {
		return pid;
	}

	@Override
	public boolean execute() {
        long time = peersim.core.CommonState.getTime();

        IncrementalStats is = new IncrementalStats();

        for (int i = 0; i < Network.size(); i++) {
            SingleValue protocol = (SingleValue) Network.get(i).getProtocol(getProtocolID());
            is.add(protocol.getValue());
        }

        /* Printing statistics */
        System.out.println(name + ": " + time + " " + is);

        /* Terminate if accuracy target is reached */
        return false;
	}

}
