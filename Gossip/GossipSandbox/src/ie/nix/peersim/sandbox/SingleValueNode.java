package ie.nix.peersim.sandbox;

import peersim.core.GeneralNode;
//import peersim.core.Protocol;
import peersim.vector.SingleValue;

public class SingleValueNode extends GeneralNode implements SingleValue {//, Protocol {

	/** Value held by this protocol */
	protected double value;
	
	public SingleValueNode(String prefix) {
		super(prefix);
		System.out.println("NDB::SingleValueNode(" + prefix + ")");

	}

	@Override
	public double getValue() {
		return value;
	}

	@Override
	public void setValue(double value) {
		this.value = value;
	}

}
