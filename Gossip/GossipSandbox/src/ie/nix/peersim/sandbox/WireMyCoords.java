package ie.nix.peersim.sandbox;

import peersim.config.Configuration;
import peersim.core.Network;
import peersim.core.Node;
import peersim.dynamics.WireGraph;
import peersim.graph.Graph;

public class WireMyCoords extends WireGraph {

	/*
	 * The wiring class extends a standard Peersim component: 
	 * peersim.dynamics.WireGraph. It implements Control and 
	 * provides generic functionality for dealing with topologies, 
	 * providing a graph interface. The wiring logic has to be 
	 * in the wire() method that is called by the superclass. 
	 * By default, the wiring process considers the index 0 
	 * node as the root. 
	 * The class has to read from the configuration file both α (alpha 
	 * in the configuration file) and the coordinate container protocol 
	 * identifier (coord protocol in the configuration file). 
	 * This is done in the class constructor. The other parameter, 
	 * protocol is inherited from the superclass: it is the protocol 
	 * that implements the Linkable interface.
	 */

    private static final String PAR_COORDINATES_PROT = "coord_protocol";
    
	private static final String PAR_ALPHA = "alpha";
	   
	/** Protocol identifier, obtained from config property {@link #PAR_PROT}. */
    private static int coordPid;
	private final double alpha;
	
	public WireMyCoords(String prefix) {
		super(prefix);
		coordPid = Configuration.getPid(prefix + "." + PAR_COORDINATES_PROT);
        alpha = Configuration.getDouble(prefix + "." + PAR_ALPHA, 0.5);
	}

	@Override
	public void wire(Graph graph) {
		/** Contains the distance in hops from the root node for each node. */
	    int[] hops = new int[Network.size()];
	    // connect all the nodes other than roots
	    /*
	     * For each node
	     */
	    for (int i = 1; i < Network.size(); ++i) {
	        Node node = (Node)graph.getNode(i);
	        // Look for a suitable parent node between those already part of
	        // the overlay topology: alias FIND THE MINIMUM!
	        int candidate_index = 0;
	        double min = Double.POSITIVE_INFINITY;
	        /*
	         * For each alreay processed node, these are the parents.
	         */
	        for (int j = 0; j < i; j++) {
	            Node parentNode = (Node)graph.getNode(j);
	            double jHopDistance = hops[j];
	            double value = jHopDistance + (alpha * distance(node, parentNode, coordPid));
	            if (value < min) {
	                // candidate = parent; // best parent node to connect to
	                min = value;
	                candidate_index = j;
	            } 
	        }
	        hops[i] = hops[candidate_index] + 1;
	        graph.setEdge(i, candidate_index);
	    }
		
	}

	private double distance(Node node1, Node node2, int coordPid) {
		double x1 = ((MyCoords)node1.getProtocol(coordPid)).getX();
	    double y1 = ((MyCoords)node1.getProtocol(coordPid)).getY();
	    double x2 = ((MyCoords)node2.getProtocol(coordPid)).getX();
	    double y2 = ((MyCoords)node2.getProtocol(coordPid)).getY();
	    if (x1 == -1 || x2 == -1 || y1 == -1 || y2 == -1) {
	    // NOTE: in release 1.0 the line above incorrectly contains
	    // |-s instead of ||. Use latest CVS version, or fix it by hand.
	        throw new RuntimeException("Found un-initialized coordinate. Use e.g.,InetInitializer class in the config file.");
	    } 
	    return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));

	}
}
