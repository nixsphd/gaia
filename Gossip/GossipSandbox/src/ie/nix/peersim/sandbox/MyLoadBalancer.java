package ie.nix.peersim.sandbox;

import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.Control;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;
import peersim.vector.SingleValueHolder;

public class MyLoadBalancer extends SingleValueHolder implements CDProtocol {//, Control {

	public static class ResetQuotas implements Control {
		
		private static final String PAR_PROT = "protocol";
		
		/** Value obtained from config property {@link #PAR_PROT}. */
	    private final int protocolID;
	    
		public ResetQuotas(String prefix) {
			protocolID = Configuration.getPid(prefix + "." + PAR_PROT);
		}
		 
		/*
		 * Reset the quotas
		 */
		@Override
		public boolean execute() {
//			System.out.println("MyLoadBalancer.ResetQuota.execute()");
			for (int i = 0; i < Network.size(); ++i) {
				((MyLoadBalancer)Network.get(i).getProtocol(protocolID)).resetQuota();
			}
			return false;
		}
		
	}
	
	protected static final String PAR_QUOTA = "quota";
	
	/*
	 * a node is composed of two values: the local load (stored in super.value) and the quota. 
	 * The second one is the amount of “load” the node is allowed to transfer at each cycle. 
	 * The quota is necessary in order to model the upper bound of load that can 
	 * realistically be transferred in a time unit.
	 */
	protected double quota;
	protected double quota_remaining;

	public MyLoadBalancer(String prefix) {
		super(prefix);
		this.quota = (Configuration.getInt(prefix + "." + PAR_QUOTA, 1));
		this.quota_remaining = quota;
//		System.out.println("NDB::MyLoadBalancer(" + prefix + ", quota="+quota+")");
	}

	/*
	 * Every node contacts the most distant neighbor in its local view and then exchanges at 
	 * maximum the quota value. The concept of “distance” is expressed in terms of maximally 
	 * different load from the load of the current node. Comparing the distance, the protocol 
	 * chooses to perform a load balancing step using a push or pull approach.
	 *
	 * (non-Javadoc)
	 * @see peersim.cdsim.CDProtocol#nextCycle(peersim.core.Node, int)
	 */
	@Override
	public void nextCycle(Node node, int protocolID) {
//		System.out.println("MyLoadBalancer.nextCycle() ");
		
		int linkableID = FastConfig.getLinkable(protocolID);
		Linkable linkable = (Linkable) node.getProtocol(linkableID);

		double maxDistance = 0;
		MyLoadBalancer furthest = null;
		
		for (int i = 0; i < linkable.degree(); i++) {
			Node neighbourNode = linkable.getNeighbor(i);
			MyLoadBalancer neighbourLoadBalancer = (MyLoadBalancer)neighbourNode.getProtocol(protocolID);
			double distance = value - neighbourLoadBalancer.value;
			if (Math.abs(distance) > Math.abs(maxDistance)) {
				maxDistance = distance;
				furthest = neighbourLoadBalancer;
			}
		}
		
		if (furthest != null) {
			double swapAmount = maxDistance/2.0;
			double min_quota_remaining = Math.min(Math.abs(quota_remaining), Math.abs(furthest.quota_remaining));
			if (Math.abs(swapAmount) > min_quota_remaining) {
				if (swapAmount > 0.0) {
					swapAmount = min_quota_remaining;
					
				} else {
					swapAmount = -min_quota_remaining;
					
				}
			}
			
//			System.out.println("MyLoadBalancer.nextCycle() "+
//					value+"->"+(value - swapAmount)+" and "+
//					furthest.value+"->"+(furthest.value + swapAmount));
			
			value -= swapAmount;
			furthest.value += swapAmount;
			
			quota_remaining = Math.abs(swapAmount);
			furthest.quota_remaining = Math.abs(swapAmount);
		}
		
	}
	
	protected void resetQuota() {
//		System.out.println("MyLoadBalancer.resetQuota()~Resetting quotas");
		quota_remaining = quota;
		
	}

}
