#!/bin/sh 

#SBATCH --time=12:00:00
#SBATCH --nodes=1
#SBATCH -A ngcom014c
#SBATCH -p ProdQ

module load java/11 taskfarm

#export PROJECT_ROOT=/Users/nicolamcdonnell/Desktop/Gaia/Gossip/GossipPeerSim
export PROJECT_ROOT=/ichec/home/users/nix/gaia/Gossip/GossipPeerSim
export MASTER_IPADDRESS=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)
export LOG_DIR=$PROJECT_ROOT/results/$SLURM_JOB_ID
export ECJ_CONFIG_FILE=$PROJECT_ROOT/src/main/java/ie/nix/automata/gp/Automata.params

mkdir -p $LOG_DIR
cd $PROJECT_ROOT

taskfarm kay/master_slave_tasks