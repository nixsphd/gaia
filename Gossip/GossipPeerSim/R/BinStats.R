library(ggplot2)

setwd("~/Desktop/Gaia/Gossip/GossipPeerSim/R")

data <- read.csv("bin-stats.csv", strip.white = TRUE)
dataAtEnd <- data[data$time==10,]
qplot(generation, stdFreeSpace, data=dataAtEnd)

data <- read.csv("gp-stats.csv", strip.white = TRUE)
tapply(data$fitness, data$generation, max)
plot(tapply(data$fitness, data$generation, max), type = "l")

data <- read.csv("koza.stat", header = FALSE, sep=" ", strip.white = TRUE)
names(data) <- c("generation","mean",'bestInGeneration',"bestInRun", "best")
qplot(generation, bestInGeneration, data=data) + geom_line() + geom_point()

qplot(generation, mean, data=data) + geom_line() + geom_point()
