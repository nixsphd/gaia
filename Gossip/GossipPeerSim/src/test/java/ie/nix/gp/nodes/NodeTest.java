package ie.nix.gp.nodes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.gp.Node;
import ie.nix.gp.TypedData;
import ie.nix.gp.nodes.ArithmeticOperator.Add;
import ie.nix.gp.nodes.ArithmeticOperator.Div;
import ie.nix.gp.nodes.ArithmeticOperator.Mul;
import ie.nix.gp.nodes.ArithmeticOperator.Sub;
import ie.nix.gp.nodes.ConditionalOperator.IfDouble;
import ie.nix.gp.nodes.ConditionalOperator.IfBoolean;
import ie.nix.gp.nodes.LogicalOperator.Not;
import ie.nix.gp.nodes.LogicalOperator.And;
import ie.nix.gp.nodes.LogicalOperator.Or;
import ie.nix.gp.nodes.RationalOperator.Equals;
import ie.nix.gp.nodes.RationalOperator.NotEquals;
import ie.nix.gp.nodes.RationalOperator.GreaterThan;
import ie.nix.gp.nodes.RationalOperator.GreaterThanOrEquals;
import ie.nix.gp.nodes.RationalOperator.LessThan;
import ie.nix.gp.nodes.RationalOperator.LessThanOrEquals;
import ie.nix.gp.nodes.Variable.A;
import ie.nix.gp.nodes.Variable.B;
import ie.nix.gp.nodes.Variable.C;
import ie.nix.gp.nodes.Variable.X;
import ie.nix.gp.nodes.Variable.Y;
import ie.nix.gp.nodes.Variable.Z;
import ie.nix.gp.nodes.Constant;

public class NodeTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	/*
	 * Helper methods
	 */
	
	/*
	 * Arithmetic Operators
	 */
	@Test
	@DisplayName("Add")
	public void add() {
		
		Add node = new Add();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);

		double expectedResult = 3d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Subtract")
	public void subtract() {
		
		Sub node = new Sub();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1);
		node.children[1] = new Constant(2);

		double expectedResult = -1d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Multiply")
	public void multiply() {
		
		Mul node = new Mul();
		
		node.children = new Node[2];
		node.children[0] = new Constant(10);
		node.children[1] = new Constant(2);

		double expectedResult = 20d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Divide")
	public void divide() {
		
		Div node = new Div();
		
		node.children = new Node[2];
		node.children[0] = new Constant(10);
		node.children[1] = new Constant(2);

		double expectedResult = 5d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Divide by Zero")
	public void divideByZero() {
		
		Div node = new Div();
		
		node.children = new Node[2];
		node.children[0] = new Constant(10);
		node.children[1] = new Constant(0);

		double expectedResult = 0d;
		
		testNode(node, expectedResult);
		
	}
	
	/*
	 * Conditional Operators
	 */
	@Test
	@DisplayName("If Double True")
	public void ifDoubleTrue() {
		
		IfDouble node = new IfDouble();
		
		node.children = new Node[3];
		node.children[0] = new Constant.True();
		node.children[1] = new Constant(1);
		node.children[2] = new Constant(2);

		double expectedResult = 1d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("If Double False")
	public void ifDoubleFalse() {
		
		IfDouble node = new IfDouble();
		
		node.children = new Node[3];
		node.children[0] = new Constant.False();
		node.children[1] = new Constant(1);
		node.children[2] = new Constant(2);

		double expectedResult = 2d;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("If Boolean True")
	public void ifBooleanTrue() {
		
		IfBoolean node = new IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new Constant.True();
		node.children[1] = new Constant.True();
		node.children[2] = new Constant.False();

		boolean expectedResult = true;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("If Boolean False")
	public void ifBooleanFalse() {

		IfBoolean node = new IfBoolean();
		
		node.children = new Node[3];
		node.children[0] = new Constant.False();
		node.children[1] = new Constant.True();
		node.children[2] = new Constant.False();

		boolean expectedResult = false;
		
		testNode(node, expectedResult);
		
	}

	/*
	 * Logical Operators
	 */
	@Test
	@DisplayName("Not")
	public void not() {
		
		Not node = new Not();
		
		node.children = new Node[1];
		node.children[0] = new Constant.True();

		boolean expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("And")
	public void and() {
		
		And node = new And();
		
		node.children = new Node[2];
		node.children[0] = new Constant.True();
		node.children[1] = new Constant.True();

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.True();
		node.children[1] = new Constant.False();

		expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.False();
		node.children[1] = new Constant.True();

		expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.False();
		node.children[1] = new Constant.False();

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Or")
	public void or() {
		
		Or node = new Or();
		
		node.children = new Node[2];
		node.children[0] = new Constant.True();
		node.children[1] = new Constant.True();

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.True();
		node.children[1] = new Constant.False();

		expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.False();
		node.children[1] = new Constant.True();

		expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant.False();
		node.children[1] = new Constant.False();

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	/*
	 * Rational Operators
	 */
	@Test
	@DisplayName("Equals")
	public void equals() {
		
		Equals node = new Equals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(2d);

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Not Equals")
	public void notEquals() {
		
		NotEquals node = new NotEquals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(2d);

		expectedResult = true;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Less Than")
	public void lessThan() {
		
		LessThan node = new LessThan();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(2d);
		node.children[1] = new Constant(1d);

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Less Than Or Equals")
	public void lessThanOrEquals() {
		
		LessThanOrEquals node = new LessThanOrEquals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(2d);
		node.children[1] = new Constant(1d);

		expectedResult = false;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Greater Than")
	public void greaterThan() {
		
		GreaterThan node = new GreaterThan();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(2d);
		node.children[1] = new Constant(1d);

		expectedResult = true;
		
		testNode(node, expectedResult);
		
	}
	
	@Test
	@DisplayName("Greater Than Or Equals")
	public void greaterThanOrEquals() {
		
		GreaterThanOrEquals node = new GreaterThanOrEquals();
		
		node.children = new Node[2];
		node.children[0] = new Constant(0d);
		node.children[1] = new Constant(1d);

		boolean expectedResult = false;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(1d);
		node.children[1] = new Constant(1d);

		expectedResult = true;
		
		testNode(node, expectedResult);

		node.children[0] = new Constant(2d);
		node.children[1] = new Constant(1d);

		expectedResult = true;
		
		testNode(node, expectedResult);
		
	}
	
	/*
	 * Variables
	 */
	@Test
	@DisplayName("Variable")
	public void variable() {
		
		A nodeA = new A();
		A.value = true;
		boolean expectedResult = true;
		testNode(nodeA, expectedResult);

		B nodeB = new B();
		B.value = false;
		expectedResult = false;
		testNode(nodeB, expectedResult);

		C nodeC = new C();
		C.value = true;
		expectedResult = true;
		testNode(nodeC, expectedResult);
		
		X nodeX = new X();
		X.value = 1d;
		double expectedResult2 = 1d;
		testNode(nodeX, expectedResult2);
		
		Y nodeY = new Y();
		Y.value = 2d;
		expectedResult2 = 2d;
		testNode(nodeY, expectedResult2);
		
		Z nodeZ = new Z();
		Z.value = 3d;
		expectedResult2 = 3d;
		testNode(nodeZ, expectedResult2);
		
	}
	
	/*
	 * Test Node
	 */
	public void testNode(Node node, double expectedResult) {
		// (EvolutionState state, int thread, GPData input, ADFStack stack, 
		//		GPIndividual individual, Problem problem)
		TypedData actualResult = new TypedData();
		node.eval(null, 0, actualResult, null, null, null);

		LOGGER.info("expectedResult={}", expectedResult);
		LOGGER.info("actualResult={}", actualResult);
		Assertions.assertEquals(expectedResult, actualResult.getDouble(), 
				"Expected "+expectedResult+", but  got"+actualResult.getDouble()+".");
	}

	public void testNode(Node node, boolean expectedResult) {
		// (EvolutionState state, int thread, GPData input, ADFStack stack, 
		//		GPIndividual individual, Problem problem)
		TypedData actualResult = new TypedData();
		node.eval(null, 0, actualResult, null, null, null);

		LOGGER.info("expectedResult={}", expectedResult);
		LOGGER.info("actualResult={}", actualResult);
		Assertions.assertEquals(expectedResult, actualResult.getBoolean(), 
				"Expected "+expectedResult+", but  got"+actualResult.getBoolean()+".");
	}
	
}