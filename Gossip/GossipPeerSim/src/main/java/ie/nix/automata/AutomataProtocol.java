package ie.nix.automata;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.cdsim.CDProtocol;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;

public abstract class AutomataProtocol<T> implements CDProtocol, EDProtocol {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class Message<T> {
		
		protected T value;
		
		public Message(T value) {
			super();
			this.value = value;
		}

		@Override
		public String toString() {
			return "Message ["+value+"]";
		}

		public T getValue() {
			return value;
		}
		
		public void setValue(T value) {
			this.value = value;
		}
	}
	
	public AutomataProtocol(String prefix) {}
	
	@SuppressWarnings("rawtypes")
	public Object clone() {
		AutomataProtocol clonedProtocol = null;
		try {
			clonedProtocol = (AutomataProtocol) super.clone();
		} catch (CloneNotSupportedException e) {
		} // never happens
		return clonedProtocol;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void nextCycle(Node node, int protocolID) {
		Automata<T> automata = (Automata<T>)node;
		nextCycle(automata);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void processEvent(Node node, int pid, Object event) {
		Automata<T> automata = (Automata<T>)node;
		Message<T> message  = (Message<T>)event;
		processEvent(automata, message);
		LOGGER.trace("[{}] automata={} message={} ", CommonState.getTime(), automata, message);
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	protected void sendTenderMessages(Message<T> message, Automata<T> toAutomata) {
		Automata<T> fromAutomata = (Automata<T>)CommonState.getNode();
		int protocolID = CommonState.getPid();
		
		Transport transport = (Transport)fromAutomata.getProtocol(FastConfig.getTransport(protocolID));
		transport.send(fromAutomata, toAutomata, message, protocolID);
		LOGGER.debug("[{}]~Sending {}", CommonState.getTime(), message);
		
	}
	
	@SuppressWarnings("unchecked")
	protected Automata<T> getLeftAutomata() {

		Automata<T> automata = (Automata<T>)CommonState.getNode();
		int protocolID = CommonState.getPid();
		
		Linkable neighbourhood = (Linkable)automata.getProtocol(FastConfig.getLinkable(protocolID));
		Automata<T> rightAutomata = (Automata<T>)neighbourhood.getNeighbor(0);
		return rightAutomata;
		
	}
	
	@SuppressWarnings("unchecked")
	protected Automata<T> getRightAutomata() {

		Automata<T> automata = (Automata<T>)CommonState.getNode();
		int protocolID = CommonState.getPid();
		
		Linkable neighbourhood = (Linkable)automata.getProtocol(FastConfig.getLinkable(protocolID));
		Automata<T> rightAutomata = (Automata<T>)neighbourhood.getNeighbor(1);
		return rightAutomata;
		
	}
	
	public abstract void nextCycle(Automata<T> automata);
	
	public abstract void processEvent(Automata<T> automata, Message<T> message);
	
//	@SuppressWarnings("unchecked")
//	public static void main(String[] args) {
//		peersim.Simulator.main(new String[] {
//			"src/main/java/ie/nix/automata/Automata.properties"
//		});
//		
//		for (int n = 0; n < Network.size(); n++) {
//			Automata<Integer> automata = (Automata<Integer>)Network.get(n);
//			LOGGER.info("[{}]automata({})={}", CommonState.getTime(), n, automata);
//		}
//	}
}
