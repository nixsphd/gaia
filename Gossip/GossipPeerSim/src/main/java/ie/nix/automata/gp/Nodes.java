package ie.nix.automata.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.gp.TypedData;
import ie.nix.gp.nodes.ArithmeticOperator;
import ie.nix.gp.nodes.Variable;

@SuppressWarnings("serial")
public abstract class Nodes {

	private static final Logger LOGGER = LogManager.getLogger();

	public static class Value extends Variable {

		public static double value;

		public Value() {
			super("Value", (results, result) -> ((TypedData)result).set(Value.value));
		}

	}

	public static class DerivedValue extends Variable {

		public static double value;

		public DerivedValue() {
			super("DerivedValue", (results, result) -> ((TypedData)result).set(DerivedValue.value));
		}

	}
	
	public static class MessageValue extends Variable {

		public static double value;

		public MessageValue() {
			super("MessageValue", (results, result) -> ((TypedData)result).set(MessageValue.value));
		}

	}
	
	public static class Max extends ArithmeticOperator {
		public Max() {
			super("Max", 2, (results, result) -> 
				result.set((results[0].getDouble() > results[1].getDouble()) ?
						results[0].getDouble() : 
						results[1].getDouble()));
		}

	}
	
	public static class Min extends ArithmeticOperator {
		public Min() {
			super("Min", 2, (results, result) -> 
				result.set((results[0].getDouble() < results[1].getDouble()) ?
						results[0].getDouble() : 
						results[1].getDouble()));
		}

	}

}
