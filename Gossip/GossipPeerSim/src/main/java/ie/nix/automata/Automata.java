package ie.nix.automata;

import peersim.core.GeneralNode;
import peersim.core.Network;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.core.Control;

public class Automata<T> extends GeneralNode {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class IntAutomata extends Automata<Integer> {

		public IntAutomata(String prefix) {
			super(prefix);
		}
		
	}
	
	public static class IntAutomataInit implements Control {

		public IntAutomataInit(String prefix) {}

		@SuppressWarnings("unchecked")
		@Override
		public boolean execute() {
			for (int n = 0; n < Network.size(); n++) {
				Automata<Integer> automata = (Automata<Integer>)Network.get(n);
				int value = CommonState.r.nextInt(1000);
				automata.setValue(value);
				automata.setDerivedValue(0);
				
				LOGGER.trace("[{}]automata({})={}", CommonState.getTime(), n, automata);
			}
			return false;
		}
		
	}

	protected T value;
	protected T derivedValue;
	
	public Automata(String prefix) {
		super(prefix);
	}
	
	@Override
	public String toString() {
		return "Automata["+getIndex()+": "
				+ "value="+value+", "
				+ "derivedValue="+derivedValue+"]";
	}
	
	public T getValue() {
		return value;
	}
	
	public void setValue(T value) {
		this.value = value;
	}
	
	public T getDerivedValue() {
		return derivedValue;
	}
	
	public void setDerivedValue(T derivedValue) {
		this.derivedValue = derivedValue;
	}
}
