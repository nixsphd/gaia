package ie.nix.gp.nodes;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.gp.Node;
import ie.nix.gp.TypedData;

@SuppressWarnings("serial")
public class Constant extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class True extends Constant {	
		public True() {
			super("TRUE", Boolean.TRUE);
		}	
	}
	
	public static class False extends Constant {	
		public False() {
			super("FALSE", Boolean.FALSE);
		}	
	}
	
	public static class PI extends Constant {	
		public PI() {
			super("PI", Math.PI);
		}	
	}
	
	public static class E extends Constant {		
		public E() {
			super("E", Math.E);
		}	
	}
	
	public static class Phi extends Constant {	
		public Phi() {
			super("PHI", (1 + Math.sqrt(5)/2));
		}	
	}

	public Constant(double doubeValue) {
		super(String.format("%.1f",doubeValue), 0, (results, result) -> {
				result.set(doubeValue);
				LOGGER.trace("result.doubeValue={}", result.getDouble());
			});
	}
	
	public Constant(String symbol, double doubeValue) {
		super(symbol, 0, (results, result) -> {
				result.set(doubeValue);
				LOGGER.trace("result.doubeValue={}", result.getDouble());
			});
	}

	public Constant(String symbol, Boolean booleanValue) {
		super(symbol, 0, (results, result) -> {
			((TypedData)result).set(booleanValue);
			LOGGER.trace("result.booleanValue={}", ((TypedData)result).getBoolean());
		});
	}
	
}
