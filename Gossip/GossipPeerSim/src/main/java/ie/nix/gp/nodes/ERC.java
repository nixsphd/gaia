package ie.nix.gp.nodes;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import ec.util.Code;

import ie.nix.gp.Data;

@SuppressWarnings("serial")
public class ERC extends ec.gp.ERC {

	public static class ERC1 extends ERC {
		public ERC1() {
			super(1);
		}
		
		@Override
		public void resetNode(EvolutionState state, int threadnum) {
			value = (state.random[threadnum].nextDouble());

		}
	}
	
	public static class ERC10 extends ERC {	
		public ERC10() {
			super(10);
		}
	}
	
	public static class ERC100 extends ERC {	
		public ERC100() {
			super(100);
		}
	}
	
	public static class ERC1000 extends ERC {	
		public ERC1000() {
			super(1000);
		}
	}
	
	public int max;
	public double value;
	
	protected ERC(int max) {
		this.max = max;
	}

	@Override
	public String toString() {
		return name() + "[" + String.format("%.1f", value) + "]";
	}

	@Override
	public String name() { 
		return  super.name() + max; 
	}

	@Override
	public String encode() {
		return Code.encode(value);
	}

	@Override
	public void eval(EvolutionState state, int thread, GPData input, ADFStack stack, 
			GPIndividual individual, Problem problem) {
		Data data = (Data)input;
		data.set(value);
	
	}

	@Override
	public boolean nodeEquals(GPNode node) {
		if ((node instanceof ERC) && (((ERC)node).value == value)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void resetNode(EvolutionState state, int threadnum) {
		value = (state.random[threadnum].nextInt(max));

	}

	public void mutateERC(final EvolutionState state, final int threadnum) {
		value += state.random[threadnum].nextGaussian();
	}
}