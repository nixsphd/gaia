package ie.nix.gp;

import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;

public class Node extends GPNode {

	private static final long serialVersionUID = 1954374409248842806L;

	private static final Logger LOGGER = LogManager.getLogger();
	
	public String symbol;
	public int numberOfChildren;
	public BiConsumer<Data[], Data> eval;
	
	protected Node(String symbol, int numberOfChildren, BiConsumer<Data[], Data> eval) {
		this.symbol= symbol;
		this.numberOfChildren = numberOfChildren;
		this.eval = eval;
	}
	
	public String toString() { 
		return symbol; 
    }

    public int expectedChildren() { 
    		return numberOfChildren; 
    }
	
	public void eval(EvolutionState state, int thread, GPData input, ADFStack stack, 
			GPIndividual individual, Problem problem) {
    	Data[] childrensResults = new Data[numberOfChildren];
		for (int c = 0; c < numberOfChildren; c++) {
			childrensResults[c] = (Data)input.clone();
			children[c].eval(state,thread,childrensResults[c],stack,individual,problem);
		}
    	Data result = (Data)input;
		eval.accept(childrensResults, result);
		
		LOGGER.trace("result={}", result);
	}
}
