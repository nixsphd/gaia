package ie.nix.gp.peersim;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.koza.KozaFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;
import ie.nix.gp.TypedData;
import peersim.cdsim.CDSimulator;
import peersim.edsim.EDSimulator;

@SuppressWarnings("serial")
public class GPProblem extends ec.gp.GPProblem implements SimpleProblemForm {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String SILENT 				= "silent";
	private static final String FILE 				= "file";
	private static final String CONSOLE 			= "console";

	protected static final String P_PROPERTIES 		= "properties";
	protected static final String P_PROTOCOL 		= "protocol";
	protected static final String P_PROTOCOL_NAME 	= "protocol_name";
	protected static final String P_NUMBER_OF_RUNS 	= "number_of_runs";
	protected static final String P_LOGGING 		= "logging";
	
	protected static boolean propertiesFileLoaded = false;
	
	public static GPProblem gpProblem;

	public static GPProblem getGPProblem() {
		LOGGER.trace("-> {}", gpProblem);
		return gpProblem;
	}

	public static void setGPProblem(GPProblem gpProblem) {
		GPProblem.gpProblem = gpProblem;
		LOGGER.trace("GPProblem.gpProblem={}", gpProblem);
	}

	protected String[] properties;
	protected int numberOfRuns;
	protected EvolutionState state;
	protected int threadnum;
	protected GPIndividual gpIndividual;
	
	@Override
	public String toString() {
		return this.getClass().getCanonicalName();
	}

	public Object clone() {
		GPProblem clone = (GPProblem)(super.clone());
		clone.state = state;
		clone.threadnum = threadnum;
		clone.gpIndividual = gpIndividual;
		LOGGER.trace("clone={}", clone);
		return clone;
	}

	public void setup(final EvolutionState state, final Parameter base) {
	    // very important, remember this
	    super.setup(state, base);

		String logging = state.parameters.getStringWithDefault(base.push(P_LOGGING), defaultBase().push(P_LOGGING), CONSOLE);
		LOGGER.debug("logging={}", logging);
		assert (logging.equals(SILENT) || logging.equals(FILE) || logging.equals(CONSOLE)) : 
			"logging is "+logging+", it needs to be "+SILENT+" or "+FILE+" or "+CONSOLE+".";
		
		if (logging.equals(FILE)) {
			redirectSystemOutErr();
		} else if (logging.equals(SILENT)) {
			nullSystemOutErr();
		}
		
	    String propertiesFile = state.parameters.getString(base.push(P_PROPERTIES), null);
		LOGGER.debug("propertiesFile={}", propertiesFile);
		assert (propertiesFile != null) : "Specify the properties file in the params file";
        
		String protocol = state.parameters.getString(base.push(P_PROTOCOL), null);
		LOGGER.debug("protocol={}", protocol);
		assert (protocol != null) : "Specify the protocol file in the params file";
		
		String protocolName = state.parameters.getString(base.push(P_PROTOCOL_NAME), null);
		LOGGER.debug("protocolName={}", protocolName);
		assert (protocolName != null) : "Specify the protocolName file in the params file";
		
		properties = new String[] { 
				propertiesFile, 
				"protocol." + protocolName + "=" + protocol 
			};

		numberOfRuns = state.parameters.getInt(base.push(P_NUMBER_OF_RUNS), defaultBase().push(P_NUMBER_OF_RUNS));
		LOGGER.debug("numberOfRuns={}", numberOfRuns);
		assert (numberOfRuns >= 1) : "numberOfRuns is "+numberOfRuns+", it needs to be >= 1.";
	}
	
	public void evaluate(final EvolutionState state, final Individual individual, 
                         final int subpopulation, final int threadnum) {

		LOGGER.trace("Evaluating individual={}", individual.hashCode());
	    GPProblem.setGPProblem(this);
		this.state = state;
		this.threadnum = threadnum;
		this.gpIndividual = (GPIndividual)individual;
		
        if (!gpIndividual.evaluated) {
            
            double sum = 0.0;     
            int hits = 0;
            for (int run = 0; run < numberOfRuns; run++) {
	        	initSimulation();
                runSimulation();
        		double result = evaluateSimulation();
	            sum += result;
	            if (result == 0) {
	            	hits++;
	            }
	    		LOGGER.debug("run={}, result={}, hits={}", run, result, hits);
            }
            
            KozaFitness kozaFitness = ((KozaFitness)gpIndividual.fitness);
            kozaFitness.setStandardizedFitness(state, sum);
            kozaFitness.hits = hits;
            gpIndividual.evaluated = true;
    		LOGGER.debug("Runs {} completed, adjustedFitness={}", numberOfRuns, kozaFitness.adjustedFitness());
        }
    	
    }

	public void initSimulation() {}

	public void runSimulation() {
		if (!propertiesFileLoaded) {
	        LOGGER.debug("Loading properties={}", Arrays.toString(properties));
	        LOGGER.trace("Running simulation");
	        peersim.Simulator.main(properties);
	        propertiesFileLoaded = true;
	        
	    } else {
	    	if( CDSimulator.isConfigurationCycleDriven()){
		        LOGGER.trace("Running simulation");
		    	CDSimulator.nextExperiment();
			}
			else if( EDSimulator.isConfigurationEventDriven() ) {	
		        LOGGER.trace("Running simulation");
		    	EDSimulator.nextExperiment();
			}
	    }
	}

	public double evaluateSimulation() {
		double result = Double.MAX_VALUE;
		return result;
	}
	
	public double evaluateTreeForDouble(int treeNumber) {
		gpIndividual.trees[treeNumber].child.eval(
				state, threadnum, input, stack, gpIndividual, this);
		double result = ((TypedData)input).getDouble();
		LOGGER.trace("-> {}", result);
		return result;
	}
	
	public boolean evaluateTreeForBoolean(int treeNumber) {
		gpIndividual.trees[treeNumber].child.eval(
				state, threadnum, input, stack, gpIndividual, this);
		boolean result = ((TypedData)input).getBoolean();
		LOGGER.trace("-> {}", result);
		return result;
	}

	private void redirectSystemOutErr() {
		try {
			File errFile = new File("logs/err.txt");
			FileOutputStream errFOS = new FileOutputStream(errFile);
			PrintStream errPS = new PrintStream(errFOS);
			System.setErr(errPS);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			File outFile = new File("logs/out.txt");
			FileOutputStream outFOS = new FileOutputStream(outFile);
			PrintStream outPS = new PrintStream(outFOS);
			System.setOut(outPS);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void nullSystemOutErr() {
		System.setErr(new PrintStream(OutputStream.nullOutputStream()));
		System.setOut(new PrintStream(OutputStream.nullOutputStream()));
	}

}
