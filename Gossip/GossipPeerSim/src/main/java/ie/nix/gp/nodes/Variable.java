package ie.nix.gp.nodes;

import java.util.function.BiConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.gp.Data;
import ie.nix.gp.Node;
import ie.nix.gp.TypedData;

@SuppressWarnings("serial")
public abstract class Variable extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class A extends Variable {
		
		public static boolean value;
		
		public A() { 
			super("a", (results, result) -> ((TypedData)result).set(A.value));
		}

	}

	public static class B extends Variable {
		
		public static boolean value;
		
		public B() { 
			super("b", (results, result) -> ((TypedData)result).set(B.value));
		}

	}
	
	public static class C extends Variable {
		
		public static boolean value;
		
		public C() { 
			super("c", (results, result) -> ((TypedData)result).set(C.value));
		}

	}

	public static class X extends Variable {

	    public static double value;
	    
		public X() {
			super("x", (results, result) -> result.set(X.value));
		}	
	}
	
	public static class Y extends Variable {

	    public static double value;
	    
		public Y() {
			super("y", (results, result) -> result.set(Y.value));
		}	
	}

	public static class Z extends Variable {

	    public static double value;
	    
		public Z() {
			super("z", (results, result) -> result.set(Z.value));
		}	
	}

	public Variable(String symbol, BiConsumer<Data[], Data> eval) {
		super(symbol, 0, eval);
	}
	
}
