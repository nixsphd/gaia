package ie.nix.gp.nodes;

import java.util.function.BiConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.gp.Data;
import ie.nix.gp.Node;
import ie.nix.gp.TypedData;

@SuppressWarnings("serial")
public class RationalOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class Equals extends RationalOperator {
		public Equals() {
			super("==", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() == results[1].getDouble()));
		}	
	}
	
	public static class NotEquals extends RationalOperator {
		public NotEquals() {
			super("!=", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() != results[1].getDouble()));
		}	
	}
	
	public static class GreaterThan extends RationalOperator {
		public GreaterThan() {
			super("gt", ">", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() > results[1].getDouble()));
		}
	}
	
	public static class GreaterThanOrEquals extends RationalOperator {
		public GreaterThanOrEquals() {
			super("ge", ">=", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() >= results[1].getDouble()));
		}
	}
	
	public static class LessThan extends RationalOperator {
		public LessThan() {
			super("lt", "<", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() < results[1].getDouble()));
		}	
	}
		
	public static class LessThanOrEquals extends RationalOperator {
		public LessThanOrEquals() {
			super("le", "<=", 2,  (results, result) -> 
				((TypedData)result).set(results[0].getDouble() <= results[1].getDouble()));
		}	
	}

	protected String stringForHumans;
	
	protected RationalOperator(String symbol, int numberOfChildren, BiConsumer<Data[], Data> eval) {
		this(symbol, symbol, numberOfChildren, eval);
	}
	
	protected RationalOperator(String symbol, String stringForHumans, int numberOfChildren, BiConsumer<Data[], Data> eval) {
		super(symbol, numberOfChildren, eval);
		this.stringForHumans = stringForHumans;
	}

	public String toStringForHumans() { 
		return stringForHumans; 
	}
}
