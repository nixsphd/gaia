package ie.nix.rl.GridWorld;

import ie.nix.rl.GridWorld.Grid.Cell;
import ie.nix.rl.GridWorld.Grid.Move;
import ie.nix.rl.ReinforcementLearning.Agent;
import ie.nix.rl.ReinforcementLearning.dp.GeneralisedPolicyIterator;
import ie.nix.rl.ReinforcementLearning.dp.PolicyEvaluator;
import ie.nix.rl.ReinforcementLearning.dp.PolicyImprover;
import ie.nix.rl.ReinforcementLearning.mdp.Action;
import ie.nix.rl.ReinforcementLearning.mdp.State;
import ie.nix.rl.ReinforcementLearning.policy.ActionValueFunction;
import ie.nix.rl.ReinforcementLearning.policy.Policy;
import ie.nix.rl.ReinforcementLearning.policy.ValueFunction;
import ie.nix.util.DataFrame;

public class GridWorld {

    public static void Chapter3() {
    	
    	Grid grid = new Grid(5, 5);
    	grid.setSpecialCell(grid.getCell(1, 4), grid.getCell(1, 0), 10.0);
    	grid.setSpecialCell(grid.getCell(3, 4), grid.getCell(3, 2), 5.0);
    	
		Agent agent = new Agent();
		agent.setPolicy(grid.createEquiprobableStochasticPolicy());
		agent.setValueFunction(grid.createValueFunction());
		
		// Initialise the policy evaluator
		double theta = 0.0000001;
		double discount = 0.9;
		int numberOfSweeps = 1;
		
		// Evaluate the policy 
		PolicyEvaluator pe = new PolicyEvaluator(grid, theta, discount);
		double delta = pe.evaluate(agent.getPolicy(), agent.getValueFunction());

		// Improve the policy 
		PolicyImprover pi = new PolicyImprover(grid, discount);
		pi.improve(agent.getPolicy(), agent.getValueFunction());
		
		writePolicy(grid, agent.getPolicy(), "Policy.csv");
		writeValueFunction(grid, agent.getValueFunction(), "ValueFunction.csv");

	}
    
    public static void Chapter4() {
    	
    	Grid grid = new Grid(4, 4);
    	grid.setTerminal(grid.getCell(0, 0));
    	grid.setTerminal(grid.getCell(3, 3));
    	grid.setDefaultReward(-1);
    	grid.setBumpReward(-1);
    	grid.setSpecialReward(grid.getCell(0, 0), 0);
    	grid.setSpecialReward(grid.getCell(3, 3), 0);
    	
		writeGrid(grid, "DP_Grid.csv");
    	
		Agent agent = new Agent();
		agent.setPolicy(grid.createEquiprobablePolicy());
		agent.setValueFunction(grid.createValueFunction());
		
		// Initialise the policy evaluator
		double theta = 0.0000001;
		double discount = 1.0;
		int numberOfSweeps = 1;
		int numberOfIterations = 3;
	
		GeneralisedPolicyIterator gpi = new GeneralisedPolicyIterator(grid, theta, discount);
		gpi.iterate(agent, numberOfSweeps, numberOfIterations);

		writeValueFunction(grid, agent.getValueFunction(), "DP_ValueFunction.csv");
		writeEquiprobablePolicy(grid, agent.getPolicy(), "DP_Policy.csv");

	}
    
 public static void Chapter5() {
    	
    	Grid grid = new Grid(4, 4);
    	grid.setTerminal(grid.getCell(0, 0));
    	grid.setTerminal(grid.getCell(3, 3));
    	grid.setDefaultReward(-1);
    	grid.setBumpReward(-1);
    	grid.setSpecialReward(grid.getCell(0, 0), 0);
    	grid.setSpecialReward(grid.getCell(3, 3), 0);
    	
		writeGrid(grid, "MC_Grid.csv");
    	
		Agent agent = new Agent();
		agent.setPolicy(grid.createEquiprobablePolicy());
		agent.setActionValueFunction(grid.createActionValueFunction());
		
		// Initialise the policy evaluator
		double theta = 0.0000001;
		double discount = 1.0;
		int numberOfSweeps = 1;
		int numberOfIterations = 3;
		
		// Evaluate the policy 
		PolicyEvaluator pe = new PolicyEvaluator(grid, theta, discount);
		double delta = pe.evaluate(agent.getPolicy(), agent.getActionValueFunction(), numberOfSweeps);

		// Improve the policy 
		PolicyImprover pi = new PolicyImprover(grid, discount);
		pi.improve(agent.getPolicy(), agent.getActionValueFunction());

//		GeneralisedPolicyIterator gpi = new GeneralisedPolicyIterator(grid, theta, discount);
//		gpi.iterate(agent, numberOfSweeps, numberOfIterations);

		writeActionValueFunction(grid, agent.getActionValueFunction(), "MC_ActionValueFunction.csv");
		writeEquiprobablePolicy(grid, agent.getPolicy(), "MC_Policy.csv");

	}

	public static void main(String[] args) {
//		Chapter3();
//		Chapter4();
		Chapter5();
		System.out.println("Done.");
        
    }
	
	private static void writeGrid(Grid grid, String fileName) {
		DataFrame results = new DataFrame();
		if (results.getNumberOfColumns() == 0) {
			results.addColumns(new String[]{"x", "y", "Type"});
		}
		for (State state : grid.getStates()) {
			Cell cell = (Cell)state;
			results.addRow(cell.x, cell.y, cell.getType());
			
		}
		results.write(fileName);
		
	}
	
	protected static void writeEquiprobablePolicy(Grid grid, Policy policy, String fileName) {
		
		DataFrame policyResults = new DataFrame();
		if (policyResults.getNumberOfColumns() == 0) {
			policyResults.addColumns(new String[]{"x", "y", "Action"});
		}
		for (State state : grid.getStates()) {
			Cell cell = (Cell)state;
			if (state.getType() == State.Type.NonTerminal) {
				Action[] actions = policy.getActions(state);
				for (int a = 0; a < actions.length; a++) {
					Move move = (Move)actions[a];
					policyResults.addRow(cell.x, cell.y, move.type);
				}
			}
		}
		policyResults.write(fileName);
	}

	protected static void writeStochasticPolicy(Grid grid, Policy policy, String fileName) {
		
		DataFrame policyResults = new DataFrame();
		if (policyResults.getNumberOfColumns() == 0) {
			policyResults.addColumns(new String[]{"x", "y", "Action", "Probability"});
		}
		for (State state : grid.getStates()) {
			Cell cell = (Cell)state;
			if (state.getType() == State.Type.NonTerminal) {
				Action[] actions = policy.getActions(state);
				double[] probabilities = policy.getProbabilities(state);
				for (int a = 0; a < actions.length; a++) {
					Move move = (Move)actions[a];
					policyResults.addRow(cell.x, cell.y, move.type, probabilities[a]);
				}
			}
		}
		policyResults.write(fileName);
	}

	protected static void writeValueFunction(Grid grid, ValueFunction vf, String fileName) {
		DataFrame avResults = new DataFrame();
		if (avResults.getNumberOfColumns() == 0) {
			avResults.addColumns(new String[]{"x", "y", "Value"});
		}
		for (State state : grid.getStates()) {
			Cell cell = (Cell)state;
			if (state.getType() == State.Type.NonTerminal) {
				double value = vf.getValue(cell);
				avResults.addRow(cell.x, cell.y, value);
			}
		}
		avResults.write(fileName);
	}
	
	protected static void writeActionValueFunction(Grid grid, ActionValueFunction avf, String fileName) {
		DataFrame avResults = new DataFrame();
		if (avResults.getNumberOfColumns() == 0) {
			avResults.addColumns(new String[]{"x", "y", "Action", "Value"});
		}
		for (State state : grid.getStates()) {
			Cell cell = (Cell)state;
			if (state.getType() == State.Type.NonTerminal) {
				for (Action action : grid.getStateActionFunction().getActions(state)) {
					double value = avf.getValue(cell, action);
					avResults.addRow(cell.x, cell.y, action, value);
					
				}
			}
		}
		avResults.write(fileName);
	}
    
}
