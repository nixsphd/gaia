library(plyr)
library(reshape2)
library(ggplot2)

require(graphics); 
require(grDevices)

setwd("~/Desktop/Gaia/ReinforcementLearning/GridWorld/R")

grid <- read.csv("DP_Grid.csv",strip.white = TRUE)
qplot(x, y, data=grid, colour=Type)

valueFunction <- read.csv("DP_ValueFunction.csv",strip.white = TRUE)
qplot(x, y, data=valueFunction, colour=Value, size=Value)

ggplot(valueFunction, aes(x, y)) + geom_tile(aes(fill = Value), colour = "white") 

plot <- ggplot(valueFunction, aes(x, y))
plot <- plot + geom_tile(aes(fill = rescale), colour = "white") + 
    scale_fill_gradient(low = "white",high = "steelblue"))
plot   
        Rowv = valueFunction$y, Colv = valueFunction$x)
        RowSideColors = rc, ColSideColors = cc);

actionValueFunction <- read.csv("MC_ActionValueFunction.csv", strip.white = TRUE)
qplot(x, y, data=actionValueFunction, facet=Action~Value, facet=~Action)

ggplot(valueFunction, aes(x, y)) + geom_tile(aes(fill = Value), colour = "white") 

