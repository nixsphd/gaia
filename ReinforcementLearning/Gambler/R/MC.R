library(plyr)
library(reshape2)
library(ggplot2)
# library(scatterplot3d) 
setwd("~/Desktop/Gaia/ReinforcementLearning/Gambler/R")

# Value Estimation 
data <- read.csv("MCValueEstimates.csv",strip.white = TRUE)
positiveData <- subset(data, Visits > 0)
qplot(State, Value, data=positiveData, geom=("line"), group=Episode, 
      colour=Episode, size=Visits, facets = Type ~ .)
ggsave("MCValueEstimates.svg");

data <- read.csv("MCValueEstimates.csv",strip.white = TRUE)
finalData <- subset(data, Episode == max(data$Episode))
qplot(State, Value, data=finalData, geom=("line"), facets = Type ~ .)

# Action Value Estimation 
data <- read.csv("MCActionValueEstimates.csv",strip.white = TRUE)
finalData <- subset(data, Episode == max(data$Episode) & Value > 0.0)
qplot(State, Action, data=finalData, colour=Value, size=Value, facets = Type ~ .)
ggsave("MCActionValueEstimates.eps");

data <- read.csv("MCActionValueEstimates.csv",strip.white = TRUE)
visitedData <- subset(data, Visits > 0)
qplot(State, Action, data=visitedData, colour=Value, size=Visits, facets = Type ~ .)

data <- read.csv("MCActionValueEstimates.csv",strip.white = TRUE)
positiveData <- subset(data, Value > 0)
qplot(State, Action, data=positiveData, colour=Value, size=Value, facets = Type ~ .)

# Action Value Improvment 
data <- read.csv("MCActionValueImprovment.csv",strip.white = TRUE)
positiveData <- subset(data, Value >= 0.1)
qplot(State, Action, data=positiveData, colour=Value, size=Value, facets = Loop ~ .)
ggsave("MCActionValueImprovment.eps");

data <- read.csv("MCActionValueImprovment.csv",strip.white = TRUE)
positiveData <- subset(data, Loop == max(data$Loop) & Value >= 0.005)
qplot(State, Action, data=positiveData, colour=Value, size=Value)

# data <- read.csv("MCActionValueImprovment.csv",strip.white = TRUE)
# notVisitedData <- subset(data, Loop == max(data$Loop) & Visits == 0)
# qplot(State, Action, data=notVisitedData, colour=Value, size=Visits)

data <- read.csv("MC_AVI_Policy.csv",strip.white = TRUE)
significantData <- subset(data, Loop == max(data$Loop))
qplot(State, Action, data=significantData)

data <- read.csv("MC_AVI_Policy.csv",strip.white = TRUE)
qplot(State, Action, data=data, facets = Loop ~ .)

# Policy Improvment 
data <- read.csv("MC_PI_Policy.csv",strip.white = TRUE)
qplot(State, Action, data=data, colour=Episode)
ggsave("MCPolicyImprovement.eps");

data <- read.csv("MC_PI_Policy.csv",strip.white = TRUE)
initialFinalData <- subset(data, Episode == 0 | Episode == max(data$Episode))
qplot(State, Action, data=initialFinalData, facets = Episode ~ .)

data <- read.csv("MC_PI_ActionValueFunction.csv",strip.white = TRUE)
positiveData <- subset(data, Value > 0.0)
finalData <- subset(positiveData, Episode == max(data$Episode))
qplot(State, Action, data=finalData, colour=Value, size=Value, facets = Episode ~ .)

data <- read.csv("MC_PI_ActionValueFunction.csv",strip.white = TRUE)
initialData <- subset(data, Episode == 0)
qplot(State, Action, data=initialData, colour=Value, size=Value, facets = Episode ~ .)

data <- read.csv("MC_PI_ActionValueFunction.csv",strip.white = TRUE)
initialFinalData <- subset(data, Episode == 0 | Episode == max(data$Episode))
qplot(State, Action, data=initialFinalData, colour=Value, size=Value, facets = Episode ~ .)

# 
# # Policy Iteration
# data <- read.csv("PolicyIteration.csv",strip.white = TRUE)
# qplot(State, Value, data=data, group=Iteration, colour=Iteration)
# ggsave("PolicyIteration.svg");
# 
# qplot(State, Value, data=data, group=Iteration, colour=Iteration, facets = Iteration ~ .)
# 
# finalData <- subset(data, Iteration == max(data$Iteration))
# qplot(State, Value, data=finalData)
# 
# # Value Iteration 
# data <- read.csv("ValueIteration.csv",strip.white = TRUE)
# qplot(State, Value, data=data, group=Iteration, colour=Iteration)
# ggsave("ValueIteration.svg");
# 
# qplot(State, Value, data=data, group=Iteration, colour=Iteration, facets = Iteration ~ .)
# 
# finalData <- subset(data, Iteration == max(data$Iteration))
# qplot(State, Value, data=finalData)
# 
# Generalised Policy Iteration
data <- read.csv("MCGeneralisedPolicyIteration.csv",strip.white = TRUE)
finalData <- subset(data, IterationsNumber == max(data$IterationsNumber))
qplot(State, Action, data=finalData)
ggsave("GeneralisedPolicyIteration.svg");

avf_data <- read.csv("MC_GI_ActionValueFunction.csv",strip.white = TRUE)
positiveData <- subset(avf_data, Value > 0.0)
qplot(State, Action, data=positiveData, colour=Value, size=Value, 
      facets = IterationsNumber ~ .)

avf_data <- read.csv("MC_GI_ActionValueFunction.csv",strip.white = TRUE)
avf_finalData <- subset(avf_data, IterationsNumber == 
                            max(avf_data$IterationsNumber) & Value > 0.001)
qplot(State, Action, data=avf_finalData, colour=Value, size=Value)

