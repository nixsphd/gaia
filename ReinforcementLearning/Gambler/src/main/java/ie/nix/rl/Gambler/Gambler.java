package ie.nix.rl.Gambler;

import com.sun.glass.ui.CommonDialogs.Type;

import ie.nix.rl_java.Agent;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.TabularActionValueFunction;
import ie.nix.rl_java.policy.TabularPolicy;
import ie.nix.rl_java.policy.TabularValueFunction;
import ie.nix.rl_java.policy.ValueFunction;
import ie.nix.util.Randomness;

public class Gambler extends Agent {
		
		public Gambler(Casino casino) {
			
			// Initialise the policy with default action of not to bet
			// The fist and last states are terminal states so no need for a policy.
			setPolicy(createBoldPolicy(casino));
			
			// Initialise the value function
			ValueFunction vf = new TabularValueFunction(casino.getStates().length);
			setValueFunction(vf);
			
			ActionValueFunction avf = new TabularActionValueFunction(casino.getStates().length, casino.getActions().length);
			setActionValueFunction(avf);

		}

		public Policy createSoftPolicy(Casino casino) {
			Policy policy = new TabularPolicy(casino, Policy.Type.Equiprobable, 0.01);
			for (State state : casino.getStates()) {
				if (state.getType() == Type.NonTerminal) {
					Action[] possibleBets = casino.getStateActionFunction().getActions(state);
					policy.updateActions(state, possibleBets);

				}
	
			}
			return policy;
		}
		
		public Policy createBoldPolicy(Casino casino) {
			Policy policy = new TabularPolicy(casino, Policy.Type.Equiprobable);
			for (State state : casino.getStates()) {
				if (state.getType() == Type.NonTerminal) {
					Action[] possibleBets = casino.getStateActionFunction().getActions(state);
					policy.updateActions(state, new Action[]{possibleBets[possibleBets.length - 1]});
				}
			}
			return policy;
		}
		
		public Policy createRandomPolicy(Casino casino) {
			Policy policy = new TabularPolicy(casino, Policy.Type.Equiprobable);
			for (State state : casino.getStates()) {
				if (state.getType() == Type.NonTerminal) {
					Action[] possibleBets = casino.getStateActionFunction().getActions(state);
					policy.updateActions(state, new Action[]{possibleBets[Randomness.nextInt(possibleBets.length - 1)]});
				}
	
			}
			return policy;
		}
		
		public Policy createTimidPolicy(Casino casino) {
			Policy policy = new TabularPolicy(casino, Policy.Type.Equiprobable);
			for (State state : casino.getStates()) {
				if (state.getType() == Type.NonTerminal) {
					Action[] possibleBets = casino.getStateActionFunction().getActions(state);
					policy.updateActions(state, new Action[]{possibleBets[1]});
				}
	
			}
			return policy;
		}
		
		public Policy createEquiprobablePolicy(Casino casino) {
			Policy policy = new TabularPolicy(casino, Policy.Type.Equiprobable);
			for (State state : casino.getStates()) {
				if (state.getType() == Type.NonTerminal) {
					Action[] possibleBets = casino.getStateActionFunction().getActions(state);
					policy.updateActions(state, possibleBets);

				}
	
			}
			return policy;
			
		}
		
		public Policy createEquiprobableSoftPolicy(Casino casino) {
			Policy policy = new TabularPolicy(casino, Policy.Type.Equiprobable, 0.1);
			for (State state : casino.getStates()) {
				if (state.getType() == Type.NonTerminal) {
					Action[] possibleBets = casino.getStateActionFunction().getActions(state);
					policy.updateActions(state, possibleBets);

				}
	
			}
			return policy;
			
		}

		public Policy createDeterministicPolicy(Casino casino) {
			Policy policy = new TabularPolicy(casino, Policy.Type.Deterministic);
			for (State state : casino.getStates()) {
				if (state.getType() == Type.NonTerminal) {
					Action[] possibleBets = casino.getStateActionFunction().getActions(state);
					policy.updateAction(state, possibleBets[possibleBets.length - 1]);

				}
	
			}
			return policy;
		}

		
	}