package ie.nix.rl.Gambler;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateActionFunction;
import ie.nix.rl_java.mdp.TransitionFunction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.mdp.RewardFunction;
import ie.nix.util.Randomness;

public class Casino extends TabularFiniteMDP {

	public Casino(final int targetEuros, final double probabilityHeads) {
		
		super(targetEuros + 1, Math.round((targetEuros + 1) / 2.0f));
//		super(targetEuros + 1, targetEuros + 1);

		// Initialise the goal/task, win targetEuros
		setRewardFunction(new RewardFunction() {
			
			public double getReward(State initialState, Action actionTaken, State finalState) {
				return getReward(finalState);
			}

			public double getReward(State state) {
				if (state.getId() == targetEuros) {
					return 1;
				} else if (state.getId() == 0) {
					return 0;
				} else {
					return 0;
				}
			}
		});

		// Initialise the states; Capital
		newState(State.Type.Terminal);
		for (int s = 1; s < states.length - 1; s++) {
			newState();
		}
		newState(State.Type.Terminal);

		// Initialise the actions: Bets
		for (int a = 0; a < actions.length; a++) {
			newAction();
		}

		// Initialise the state action function which gives the allowed
		// actions for the agent in a state.
		StateActionFunction stateActionFunction = new StateActionFunction() {
			public Action[] getActions(State forState) {
				if (forState.getType() != State.Type.Terminal) {
					Action[] stateActions = new Action[Math.min(forState.getId(), targetEuros - forState.getId()) + 1];
//					Action[] stateActions = new Action[forState.getId()];
					System.arraycopy(actions, 0, stateActions, 0, stateActions.length);
					return stateActions;
				} else {
					return null;
				}
			}
		};

		setStateActionFunction(stateActionFunction);

		// Initialise the state transition function
		TransitionFunction stf = new TransitionFunction() {
			
			public double probabilityOf(State movingToState, State givenState, Action givenAction) {
				if (movingToState.getId() == givenState.getId() + givenAction.getId()) {
					return probabilityHeads;

				} else if (movingToState.getId() == givenState.getId() - givenAction.getId()) {
					return 1 - probabilityHeads;

				} else {
					return 0;
				}
			}

			public State getNextState(State givenState, Action givenAction) {
				if (Randomness.nextDouble() <= probabilityHeads) {
					return states[givenState.getId() + givenAction.getId()];
//					if ((givenState.getId() + givenAction.getId()) > states.length - 1) {
//						return states[0];
//					} else {
//						return states[givenState.getId() + givenAction.getId()];
//					}
					
				} else {
					return states[givenState.getId() - givenAction.getId()];
					
				}
			}

		};
		setStateTransitionFunction(stf);
	}

}