package ie.nix.rl.dp
import ie.nix.rl.ValueFunction
import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.demos.gambler.Gambler.ModestBetter
import ie.nix.rl.tabular.policy.DeterministicPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.PrivateMethodTester._
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class GeneralisedPolicyIteratorTest extends AnyFlatSpec  with BeforeAndAfterEach with Logging {
    
    // private methods to be tested
    private val evaluateAndImprovePolicy =
        PrivateMethod[(DeterministicPolicy, ValueFunction, Double)]('evaluateAndImprovePolicy)
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "GeneralisedPolicyIteratorTest"
    
    it should "evaluateAndImprovePolicy should improve policy" in {
        
        val casino = Casino(targetCapitol=7, probabilityHeads=0.4)
        val discount = 1d
        val numberOfSweeps=1
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, numberOfSweeps, discount)
        val policyImprover: PolicyImprover = PolicyImprover(casino, discount)
        
        val generalisedPolicyIterator: GeneralisedPolicyIterator = GeneralisedPolicyIterator(policyEvaluator, policyImprover)
        val policy =  ModestBetter(casino)
        
        val (improvedPolicy, valueFunction, difference) = generalisedPolicyIterator invokePrivate evaluateAndImprovePolicy(policy)
        logger.info(s"~improvedPolicy=$improvedPolicy, difference=$difference")
    
        assertTrue(difference != 0)
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(1)))
        assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(2)))
        assertEquals(casino.GamblersBet(3), improvedPolicy.getActionForState(casino.GamblersCapitol(3)))
        assertEquals(casino.GamblersBet(3), improvedPolicy.getActionForState(casino.GamblersCapitol(4)))
        assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(5)))
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(6)))
    }
    
    it should "iterate should improve policy" in {
        
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val discount = 1d
        val numberOfSweeps=1
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, numberOfSweeps, discount)
        val policyImprover: PolicyImprover = PolicyImprover(casino, discount)
        
        val generalisedPolicyIterator: GeneralisedPolicyIterator = GeneralisedPolicyIterator(policyEvaluator, policyImprover)
        val policy = ModestBetter(casino)
        logger.info(s"~policy=$policy")
        
        val (improvedPolicy, valueFunction) = generalisedPolicyIterator.iterate(policy)
        logger.info(s"~improvedPolicy=$improvedPolicy")
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(1)))
        assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(2)))
        assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(3)))
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(4)))
    }
    
}
