package ie.nix.rl.mc

import ie.nix.rl.demos.black_jack.BlackJackTable
import ie.nix.rl.demos.black_jack.Player.Sticker
import ie.nix.rl.demos.random_walk.RandomWalk
import ie.nix.rl.demos.random_walk.RandomWalk.rootMeanSquaredError
import ie.nix.rl.demos.random_walk.Walker.{Lefty, Righty}
import ie.nix.rl.tabular.policy.Policy.EquiprobablePolicy
import ie.nix.rl.{ActionValueFunction, Episode, Policy, StateAction, mc}
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PolicyEvaluatorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach(): Unit = {
        Random.setSeed(0)
    }
    
    behavior of "MC PolicyEvaluator"
    
    it should "get ActionValueFunction with one episode for Policy with Hit action" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        val playerPolicy = Sticker(blackJackTable)
        val discount: Double = 1d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(blackJackTable, discount)
        
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Hit
        val stateAction = StateAction(state, action)
        logger.info(s"~stateAction=$stateAction")
        val actionValueFunction: ActionValueFunction =
            policyEvaluator.getSampleAveragingValueFunctionForPolicy(playerPolicy,
                List(Episode(blackJackTable, stateAction)))
        logger.info(s"~actionValueFunction=$actionValueFunction")
        
        assertEquals(-1d, actionValueFunction.getValueForStateAction(stateAction), 0.0001)
        assertEquals(-1d, actionValueFunction.getValueForStateAction(StateAction(
            blackJackTable.BlackJackState(18, false, 1), blackJackTable.Stick)), 0.0001)
        assertEquals(0d, actionValueFunction.getValueForStateAction(StateAction(
            blackJackTable.BlackJackState(19, false, 1), blackJackTable.Stick)), 0.0001)
    
    }
    
    it should "get ActionValueFunction with one episode for Policy with Stick action" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        logger.info(s"~blackJackTable=$blackJackTable")
        
        val playerPolicy = Sticker(blackJackTable)
        logger.info(s"~playerPolicy=$playerPolicy")
        
        val discount: Double = 1d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(blackJackTable, discount)
        logger.info(s"~policyEvaluator=$policyEvaluator")
    
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Stick
        val stateAction = StateAction(state, action)
        val actionValueFunction: ActionValueFunction =
            policyEvaluator.getSampleAveragingValueFunctionForPolicy(playerPolicy,
                List(Episode(blackJackTable, stateAction)))
        logger.info(s"~actionValueFunction=$actionValueFunction")
    
        assertEquals(-1d, actionValueFunction.getValueForStateAction(stateAction), 0.0001)
        assertEquals(0d, actionValueFunction.getValueForStateAction(StateAction(
            blackJackTable.BlackJackState(18, false, 1), blackJackTable.Stick)), 0.0001)
    
    }
    
    it should "get ActionValueFunction with two episode for Policy" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        logger.info(s"~blackJackTable=$blackJackTable")
        
        val playerPolicy = Sticker(blackJackTable)
        logger.info(s"~playerPolicy=$playerPolicy")
    
        val discount: Double = 1d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(blackJackTable, discount)
        logger.info(s"~policyEvaluator=$policyEvaluator")
        
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val hitStateActionEpisode = StateAction(state, blackJackTable.Hit)
        val stickStateActionEpisode = StateAction(state, blackJackTable.Stick)
        logger.info(s"~hitStateActionEpisode=$hitStateActionEpisode, stickStateActionEpisode=$stickStateActionEpisode")
        val actionValueFunction: ActionValueFunction =
            policyEvaluator.getSampleAveragingValueFunctionForPolicy(playerPolicy, List(
                Episode(blackJackTable, hitStateActionEpisode),
                Episode(blackJackTable, stickStateActionEpisode)))
        logger.info(s"~actionValueFunction=$actionValueFunction")
        
        assertEquals(-1d, actionValueFunction.getValueForStateAction(hitStateActionEpisode), 0.0001)
        assertEquals(-1d, actionValueFunction.getValueForStateAction(stickStateActionEpisode), 0.0001)
        assertEquals(-1d, actionValueFunction.getValueForStateAction(StateAction(
            blackJackTable.BlackJackState(18, false, 1), blackJackTable.Stick)), 0.0001)
        assertEquals(0d, actionValueFunction.getValueForStateAction(StateAction(
            blackJackTable.BlackJackState(19, false, 1), blackJackTable.Stick)), 0.0001)
        
    }
    
    it should "get ActionValueFunction with fake two visits for Policy" in {
        
        val blackJackTable: BlackJackTable = BlackJackTable()
        val playerPolicy = Sticker(blackJackTable)
        val discount: Double = 1d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(blackJackTable, discount)
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=14, playerHasUsableAce=false, dealersVisibleCard=1)
        val hitStateAction = StateAction(state, blackJackTable.Hit)
        
        val actionValueFunction: ActionValueFunction =
            policyEvaluator.getSampleAveragingValueFunctionForPolicy(playerPolicy, List(
                Episode(blackJackTable, hitStateAction),
                Episode(blackJackTable, hitStateAction),
                Episode(blackJackTable, hitStateAction)))
        logger.info(s"~actionValueFunction=$actionValueFunction")
        
        assertEquals(-0.6666, actionValueFunction.getValueForStateAction(hitStateAction), 0.0001)
        assertEquals(-1d, actionValueFunction.getValueForStateAction(StateAction(
            blackJackTable.BlackJackState(16, false, 1), blackJackTable.Stick)), 0.0001)
        
    }
    
    it should "get ConstantStepSize ValueFunction For Righty Policy with 1 episode" in {
    
        val randomWalk = RandomWalk(numberOfTiles = 4)
        val policyEvaluator = mc.PolicyEvaluator(randomWalk)
        val policy = Righty(randomWalk)
        val episodes = Vector(Episode(randomWalk, StateAction(randomWalk.Tile(1), randomWalk.Right)))
        val initialValue = 0.5
        val stepSize = 0.1
        
        val valueFunction = policyEvaluator.
            getConstantStepSizeValueFunctionForPolicy(policy, episodes, initialValue, stepSize)
        logger.info(s"~valueFunction=$valueFunction")
    
        assertEquals(0d, valueFunction.getValueForState(randomWalk.Tile(0)), 0.000001)
        assertEquals(0.55, valueFunction.getValueForState(randomWalk.Tile(1)), 0.000001)
        assertEquals(0.55, valueFunction.getValueForState(randomWalk.Tile(2)), 0.000001)
        assertEquals(0.55, valueFunction.getValueForState(randomWalk.Tile(3)), 0.000001)
        assertEquals(0d, valueFunction.getValueForState(randomWalk.Tile(4)), 0.000001)
    }
    
    it should "get ConstantStepSize ValueFunction For Lefty Policy with 1 episode" in {
        
        val randomWalk = RandomWalk(numberOfTiles = 4)
        val policyEvaluator = mc.PolicyEvaluator(randomWalk)
        val policy = Lefty(randomWalk)
        val episodes = Vector(Episode(randomWalk, StateAction(randomWalk.Tile(3), randomWalk.Left)))
        val initialValue = 0.5
        val stepSize = 0.1
        
        val valueFunction = policyEvaluator.
            getConstantStepSizeValueFunctionForPolicy(policy, episodes, initialValue, stepSize)
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(0d, valueFunction.getValueForState(randomWalk.Tile(0)), 0.000001)
        assertEquals(0.45, valueFunction.getValueForState(randomWalk.Tile(1)), 0.000001)
        assertEquals(0.45, valueFunction.getValueForState(randomWalk.Tile(2)), 0.000001)
        assertEquals(0.45, valueFunction.getValueForState(randomWalk.Tile(3)), 0.000001)
        assertEquals(0d, valueFunction.getValueForState(randomWalk.Tile(4)), 0.000001)
    }
    
    it should "get ConstantStepSize ValueFunction For Righty Policy with 2 episodes" in {
        
        val randomWalk = RandomWalk(numberOfTiles = 4)
        val policyEvaluator = mc.PolicyEvaluator(randomWalk)
        val policy = Righty(randomWalk)
        val episodes = Vector(
            Episode(randomWalk, StateAction(randomWalk.Tile(1), randomWalk.Right)),
            Episode(randomWalk, StateAction(randomWalk.Tile(1), randomWalk.Right)))
        val initialValue = 0.5
        val stepSize = 0.1
        
        val valueFunction = policyEvaluator.
            getConstantStepSizeValueFunctionForPolicy(policy, episodes, initialValue, stepSize)
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(0d, valueFunction.getValueForState(randomWalk.Tile(0)), 0.000001)
        assertEquals(0.595, valueFunction.getValueForState(randomWalk.Tile(1)), 0.000001)
        assertEquals(0.595, valueFunction.getValueForState(randomWalk.Tile(2)), 0.000001)
        assertEquals(0.595, valueFunction.getValueForState(randomWalk.Tile(3)), 0.000001)
        assertEquals(0d, valueFunction.getValueForState(randomWalk.Tile(4)), 0.000001)
    }
    
    it should "get ConstantStepSize ValueFunction For Lefty Policy with 2 episodes" in {
        
        val randomWalk = RandomWalk(numberOfTiles = 4)
        val policyEvaluator = mc.PolicyEvaluator(randomWalk)
        val policy = Lefty(randomWalk)
        val episodes = Vector(
            Episode(randomWalk, StateAction(randomWalk.Tile(3), randomWalk.Left)),
            Episode(randomWalk, StateAction(randomWalk.Tile(3), randomWalk.Left)))
        val initialValue = 0.5
        val stepSize = 0.1
        
        val valueFunction = policyEvaluator.
            getConstantStepSizeValueFunctionForPolicy(policy, episodes, initialValue, stepSize)
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(0d, valueFunction.getValueForState(randomWalk.Tile(0)), 0.000001)
        assertEquals(0.405, valueFunction.getValueForState(randomWalk.Tile(1)), 0.000001)
        assertEquals(0.405, valueFunction.getValueForState(randomWalk.Tile(2)), 0.000001)
        assertEquals(0.405, valueFunction.getValueForState(randomWalk.Tile(3)), 0.000001)
        assertEquals(0d, valueFunction.getValueForState(randomWalk.Tile(4)), 0.000001)
    }
    
    it should "get ConstantStepSize ValueFunction For Equiprobable Policy with many episodes" in {
        
        val randomWalk = RandomWalk(numberOfTiles = 6)
        val policyEvaluator = mc.PolicyEvaluator(randomWalk)
        val policy = EquiprobablePolicy(randomWalk)
        val episodes = randomWalk.getStarts(numberOfStarts=200)
        val initialValue = 0.5
        val stepSize = 0.01
        
        val valueFunction = policyEvaluator.
            getConstantStepSizeValueFunctionForPolicy(policy, episodes, initialValue, stepSize)
        val error = rootMeanSquaredError(randomWalk, valueFunction)
        logger.info(s"~rootMeanSquaredError=$error, valueFunction=$valueFunction")
        
        assertEquals(0d, error, 0.1)
    }
    
}