package ie.nix.rl.dp

import ie.nix.rl.StateAction
import ie.nix.rl.dp.DynamicProgramming.{backupState, backupAllState, getBestAction}
import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.demos.gambler.Gambler.ModestBetter
import ie.nix.rl.dp.DynamicProgrammingTest.newTabularValueFunction
import ie.nix.rl.tabular.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random

class DynamicProgrammingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "DynamicProgrammingTest"
    
    it should "backupState to new value from 0" in {
        val casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val discount = 1d
    
        val stateAction = StateAction(casino.GamblersCapitol(3), casino.GamblersBet(1))
        val valueFunction = ValueFunction(casino, initialValue = 0d)
        val newStateValue = backupState(casino, discount, stateAction, valueFunction)
        logger.info(s"~newStateValue=$newStateValue")
        assertEquals(0.4, newStateValue, 0.0001)
    }
    
    it should "backupState to new value from 0.4" in {
        val casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val discount=1d
        val stateAction = StateAction(casino.GamblersCapitol(2), casino.GamblersBet(1))
        val valueFunction = ValueFunction(casino, initialValue = 0d).
            updateValueForState(casino.GamblersCapitol(3), 0.4)
        logger.info(s"~valueFunction=$valueFunction")
        
        val newStateValue = backupState(casino, discount, stateAction, valueFunction)
        
        logger.info(s"~newStateValue=$newStateValue")
        assertEquals(0.16, newStateValue, 0.0001)
    }
    
    it should "getBestAction when it's 1" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val discount = 1d
        
        val state = casino.GamblersCapitol(1)
        val valueFunction = newTabularValueFunction(casino)
        
        val bestAction = getBestAction(casino, discount, state, valueFunction)
        logger.info(s"~bestAction=$bestAction")
        
        assertEquals(casino.GamblersBet(1), bestAction)
    }
    
    it should "getBestAction when it's 2" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val discount = 1d
        
        val state = casino.GamblersCapitol(3)
        val valueFunction = newTabularValueFunction(casino)
        
        val bestAction = getBestAction(casino, discount, state, valueFunction)
        logger.info(s"~bestAction=$bestAction")
        
        assertEquals(casino.GamblersBet(2), bestAction)
    }
    
    it should "getBestAction when it's middle" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val discount = 1d
        
        val state = casino.GamblersCapitol(2)
        val valueFunction = newTabularValueFunction(casino)
        
        val bestAction = getBestAction(casino, discount, state, valueFunction)
        logger.info(s"~bestAction=$bestAction")
        
        assertEquals(casino.GamblersBet(2), bestAction)
    }
    
    it should "first backupStates " in {
        val casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val discount = 1d
        
        val policy = ModestBetter(casino)
        logger.info(s"~policy=$policy")
        
        val valueFunction = ValueFunction(casino, initialValue = 0d)
        logger.info(s"~valueFunction=$valueFunction")
        
        val newValueFunction = backupAllState(casino, discount, policy, valueFunction)
        logger.info(s"~newValueFunction=$newValueFunction")
        
        assertEquals(0d, newValueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(0d, newValueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(0.4, newValueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
    }
    
    it should "second backupStates " in {
        val casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val discount = 1d
        
        val policy = ModestBetter(casino)
        logger.info(s"~policy=$policy")
        
        val valueFunction = ValueFunction(casino, initialValue = 0d)
        valueFunction.updateValueForState(casino.GamblersCapitol(3), 0.4)
        logger.info(s"~valueFunction=$valueFunction")
        
        val newValueFunction: ValueFunction = backupAllState(casino, discount, policy, valueFunction)
        logger.info(s"~newValueFunction=$newValueFunction")
        
        assertEquals(0d, newValueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(0d, newValueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(0.4, newValueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
    }
}

object DynamicProgrammingTest {
    
    def newTabularValueFunction(casino: Casino) = {
        ValueFunction(casino, initialValue=0).
            updateValueForState(casino.GamblersCapitol(1), 0d).
            updateValueForState(casino.GamblersCapitol(2), 0.064).
            updateValueForState(casino.GamblersCapitol(3), 0.16).
            updateValueForState(casino.GamblersCapitol(4), 0.496)
    }
    
}
