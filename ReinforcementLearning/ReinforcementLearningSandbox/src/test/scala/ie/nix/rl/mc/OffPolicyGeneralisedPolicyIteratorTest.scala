package ie.nix.rl.mc

import ie.nix.rl.demos.black_jack.BlackJackTable
import ie.nix.rl.{Episode, StateAction}
import ie.nix.rl.demos.black_jack.Player.{Hitter, Sticker}
import ie.nix.rl.tabular.policy.Policy.difference
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.{assertEquals, assertNotEquals}
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class OffPolicyGeneralisedPolicyIteratorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "OffPolicyGeneralisedPolicyIterator"
    
    it should "iteratively improve BlackJackTable with Equiprobable BehaviourPolicy" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        val policy = Sticker(blackJackTable)
        val generalisedPolicyIterator: OffPolicyGeneralisedPolicyIterator =
            OffPolicyGeneralisedPolicyIterator(blackJackTable)
        val sumOfPlayersCards12State = blackJackTable.BlackJackState(
            sumOfPlayersCards=12, playerHasUsableAce=true, dealersVisibleCard=1)
        val episodes = Vector(Episode(blackJackTable, StateAction(sumOfPlayersCards12State, blackJackTable.Hit)))
        
        val (improvedPolicy, actionValueFunction) =
            generalisedPolicyIterator.iterateWithEquiprobableBehaviourPolicy(policy, episodes)
        logger.info(s"~policy        =$policy")
        logger.info(s"~improvedPolicy=$improvedPolicy")
        logger.info(s"~actionValueFunction=$actionValueFunction")
        
        assertNotEquals(policy, improvedPolicy)
        assertEquals(blackJackTable.Hit, improvedPolicy.getActionForState(sumOfPlayersCards12State))
        // (StateAction(BlackJackState(12, true, 1),Hit)->32.0,32.0)
        assertEquals(1d, actionValueFunction.getValueForStateAction(
            StateAction(sumOfPlayersCards12State, blackJackTable.Hit)), 0.00001)
    }
    
    it should "iteratively improve BlackJackTable RandomSoft BehaviourPolicy" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        val policy = Sticker(blackJackTable)
        logger.info(s"~policy        =$policy")
        val generalisedPolicyIterator: OffPolicyGeneralisedPolicyIterator =
            OffPolicyGeneralisedPolicyIterator(blackJackTable)
        val sumOfPlayersCards12State = blackJackTable.BlackJackState(
            sumOfPlayersCards=12, playerHasUsableAce=true, dealersVisibleCard=1)
        val episodes = Vector(
            Episode(blackJackTable, StateAction(sumOfPlayersCards12State, blackJackTable.Hit)),
            Episode(blackJackTable, StateAction(sumOfPlayersCards12State, blackJackTable.Stick)))
        
        val (improvedPolicy, actionValueFunction) =
            generalisedPolicyIterator.iterateWithRandomSoftPolicyBehaviourPolicy(policy, episodes)
        val policyDifference = difference(policy, improvedPolicy)
        logger.info(s"~improvedPolicy=$improvedPolicy")
        logger.info(s"~policyDifference=$policyDifference")
        logger.info(s"~actionValueFunction=$actionValueFunction")
    
        assertNotEquals(0d, policyDifference)
        assertEquals(blackJackTable.Hit, improvedPolicy.getActionForState(sumOfPlayersCards12State))
        // (StateAction(BlackJackState(12, true, 1),Hit)->22.160664819944596,23.32701559994168)
        assertEquals(2d, actionValueFunction.getValueForStateAction(
            StateAction(sumOfPlayersCards12State, blackJackTable.Hit)), 0.00001)
        
        // (StateAction(BlackJackState(12, true, 1),Hit)->0.0,1.16635)
        // (StateAction(BlackJackState(12, true, 1),Stick)->-1.05263,1.05263)
        
    }
    
    it should "iteratively improve BlackJackTable EGreedy BehaviourPolicy" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        val policy = Sticker(blackJackTable)
        logger.info(s"~policy        =$policy")
        val generalisedPolicyIterator: OffPolicyGeneralisedPolicyIterator =
            OffPolicyGeneralisedPolicyIterator(blackJackTable)
        val sumOfPlayersCards12State = blackJackTable.BlackJackState(
            sumOfPlayersCards=12, playerHasUsableAce=true, dealersVisibleCard=1)
        val episodes = Vector(
            Episode(blackJackTable, StateAction(sumOfPlayersCards12State, blackJackTable.Hit)),
            Episode(blackJackTable, StateAction(sumOfPlayersCards12State, blackJackTable.Hit)),
            Episode(blackJackTable, StateAction(sumOfPlayersCards12State, blackJackTable.Hit)),
            Episode(blackJackTable, StateAction(sumOfPlayersCards12State, blackJackTable.Hit)))
        
        val (improvedPolicy, _) = generalisedPolicyIterator.iterateWithEGreedyBehaviourPolicy(policy, episodes)
        val policyDifference = difference(policy, improvedPolicy)
        logger.info(s"~improvedPolicy=$improvedPolicy")
        logger.info(s"~policyDifference=$policyDifference")
        
        assertNotEquals(0d, policyDifference)
        assertEquals(blackJackTable.Hit, improvedPolicy.getActionForState(sumOfPlayersCards12State))
        
    }
}
