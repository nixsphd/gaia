package ie.nix.rl.mc

import ie.nix.rl.demos.black_jack.BlackJackTable
import ie.nix.rl.demos.black_jack.Player.Sticker
import ie.nix.rl.{Episode, Episodes, StateAction}
import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.demos.gambler.Gambler.ModestBetter
import ie.nix.rl.tabular.policy.SoftPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.{assertEquals, assertNotEquals}
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class GeneralisedPolicyIteratorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "GeneralisedPolicyIterator"
    
    it should "iteratively improve BlackJackTable with DeterministicPolicy" in {

        val blackJackTable: BlackJackTable = BlackJackTable()
        val policy = Sticker(blackJackTable)
        val generalisedPolicyIterator: GeneralisedPolicyIterator =
            GeneralisedPolicyIterator(blackJackTable)
        val episodes = blackJackTable.getExploringStart()
        
        val (improvedPolicy, _) = generalisedPolicyIterator.iterate(policy, episodes)
        logger.info(s"~policy        =$policy")
        logger.info(s"~improvedPolicy=$improvedPolicy")
    
        val sumOfPlayersCards16False1Action = improvedPolicy.getActionForState(blackJackTable.BlackJackState(
            sumOfPlayersCards=16, playerHasUsableAce=false, dealersVisibleCard=1))
        val sumOfPlayersCards16False2Action= improvedPolicy.getActionForState(blackJackTable.BlackJackState(
            sumOfPlayersCards=16, playerHasUsableAce=false, dealersVisibleCard=2))
        logger.info(s"~sumOfPlayersCards16False1Action=$sumOfPlayersCards16False1Action")
        logger.info(s"~sumOfPlayersCards16False2Action=$sumOfPlayersCards16False2Action")
        assertNotEquals(policy, improvedPolicy)
        assertEquals(blackJackTable.Hit, sumOfPlayersCards16False1Action)
        assertEquals(blackJackTable.Stick, sumOfPlayersCards16False2Action)
    
    }
    
    it should "iteratively improve BlackJackTable with SoftPolicy" in {
        
        val blackJackTable: BlackJackTable = BlackJackTable()
        val probabilityOfRandomAction = 0.1
        val policy = SoftPolicy(blackJackTable, probabilityOfRandomAction, Sticker(blackJackTable))
        val generalisedPolicyIterator: GeneralisedPolicyIterator =
            GeneralisedPolicyIterator(blackJackTable)
        val episodes = blackJackTable.getExploringStart()
        
        val (improvedPolicy, _) = generalisedPolicyIterator.iterate(policy, episodes)
        logger.info(s"~policy        =$policy")
        logger.info(s"~improvedPolicy=$improvedPolicy")
        
        val sumOfPlayersCards16False1Action = improvedPolicy.getActionForState(blackJackTable.BlackJackState(
            sumOfPlayersCards=16, playerHasUsableAce=false, dealersVisibleCard=1))
        val sumOfPlayersCards16False2Action= improvedPolicy.getActionForState(blackJackTable.BlackJackState(
            sumOfPlayersCards=16, playerHasUsableAce=false, dealersVisibleCard=2))
        logger.info(s"~sumOfPlayersCards16False1Action=$sumOfPlayersCards16False1Action")
        logger.info(s"~sumOfPlayersCards16False2Action=$sumOfPlayersCards16False2Action")
        assertNotEquals(policy, improvedPolicy)
        assertEquals(blackJackTable.Hit, sumOfPlayersCards16False1Action)
        assertEquals(blackJackTable.Stick, sumOfPlayersCards16False2Action)
        
    }
    
    it should "iteratively improve Casino with ExploringStarts" in {
        
        val casino = new Casino(targetCapitol=5, probabilityHeads=0.4) with Episodes
        val generalisedPolicyIterator: GeneralisedPolicyIterator = GeneralisedPolicyIterator(casino)
        val policy = ModestBetter(casino)
        val episodes = casino.getExploringStart()
        val (improvedPolicy, _) = generalisedPolicyIterator.iterate(policy, episodes)
        logger.info(s"~policy=$policy")
        logger.info(s"~improvedPolicy=$improvedPolicy")
        
        assertNotEquals(policy, improvedPolicy)
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(1)))
        // skipping 2 as it equiprobable for Bet(1) or Bet(2)
        assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(3)))
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(4)))
        
    }
    
    it should "iteratively improve Casino with 1000 sets of ExploringStarts" in {
        
        val casino = new Casino(targetCapitol=5, probabilityHeads=0.4) with Episodes
        val generalisedPolicyIterator: GeneralisedPolicyIterator = GeneralisedPolicyIterator(casino)
        val policy = ModestBetter(casino)
        val episodes = casino.getExploringStarts(1000)
        val (improvedPolicy, _) = generalisedPolicyIterator.iterate(policy, episodes)
        logger.info(s"~policy=$policy")
        logger.info(s"~improvedPolicy=$improvedPolicy")

        assertNotEquals(policy, improvedPolicy)
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(1)))
        // skipping 2 as it equiprobable for Bet(1) or Bet(2)
        assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(3)))
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(4)))
        
    }
    
}
