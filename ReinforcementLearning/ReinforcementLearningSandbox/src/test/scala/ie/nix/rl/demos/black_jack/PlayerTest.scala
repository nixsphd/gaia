package ie.nix.rl.demos.black_jack

import ie.nix.rl.demos.black_jack.Player.{Hitter, Sticker}
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PlayerTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "PlayerTest"
    
    it should "Hitter" in {
        val blackJackTable = new BlackJackTable()
        val hitter = Hitter(blackJackTable)
        logger.info(s"~hitter=$hitter")
    
        blackJackTable.getNonTerminalStates().foreach(state => {
            assertEquals(blackJackTable.Hit, hitter.getActionForState(state))
        })
    }
    
    it should "Sticker" in {
        val blackJackTable = new BlackJackTable()
        val sticker = Sticker(blackJackTable)
    
        blackJackTable.getNonTerminalStates().foreach(state => {
            assertEquals(blackJackTable.Stick, sticker.getActionForState(state))
        })
    }
    
}
