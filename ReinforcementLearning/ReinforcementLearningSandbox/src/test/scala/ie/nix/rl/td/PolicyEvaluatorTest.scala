package ie.nix.rl.td

import ie.nix.rl.{Episode, StateAction, Transition}
import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.demos.gambler.Gambler.ModestBetter
import ie.nix.rl.demos.random_walk.RandomWalk
import ie.nix.rl.demos.random_walk.RandomWalk.rootMeanSquaredError
import ie.nix.rl.tabular.policy.Policy.EquiprobablePolicy
import ie.nix.rl.tabular.vf.ConstantStepSize
import ie.nix.rl.tabular.vf.ConstantStepSize.StepSize
import ie.nix.rl.td.TemporalDifference.PolicyEvaluator
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PolicyEvaluatorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "TD PolicyEvaluator"
    
    it should "update ValueFunction For Episode Step" in {
    
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val initialValue = 0d
        val discount = 1d
        val stepSize = 1d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, initialValue, discount, stepSize)
        val valueFunction = givenConstantStepSizeValueFunction(casino)
        logger.info(s"~valueFunction=$valueFunction")
        val transitionReward = (Transition(
            StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)), casino.GamblersCapitol(2)), 0d)
        
        val updatedValueFunction = policyEvaluator.updateValueFunctionForEpisodeStep(valueFunction, transitionReward)
        logger.info(s"~updatedValueFunction=$updatedValueFunction")
    
        // V (S) ← V(S) + α[R + γV(S′) − V(S)] = 1 + 1[0 + 1*2 - 1] = 2
        assertEquals(2d, updatedValueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
    
    }
    
    it should "update ValueFunction For Episode Step with a discount" in {
        
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val discount = 0.9
        val initialValue = 0d
        val stepSize = 1d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, initialValue, discount, stepSize)
        val valueFunction = givenConstantStepSizeValueFunction(casino, discount = 0.9)
        logger.info(s"~valueFunction=$valueFunction")
        val transitionReward = (Transition(
            StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)), casino.GamblersCapitol(2)), 0d)
        
        val updatedValueFunction = policyEvaluator.updateValueFunctionForEpisodeStep(valueFunction, transitionReward)
        logger.info(s"~updatedValueFunction=$updatedValueFunction")
        
        // V (S) ← V(S) + α[R + γV(S′) − V(S)] = 1 + 1[0 + 0.9*2 - 1] = 2
        assertEquals(1.8, updatedValueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        
    }
    
    it should "update ValueFunction For Episode Step with a step size" in {
        
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val discount = 1d
        val stepSize = 0.1
        val initialValue = 0d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, initialValue, discount, stepSize)
        val valueFunction = givenConstantStepSizeValueFunction(casino, stepSize = 0.1)
        logger.info(s"~valueFunction=$valueFunction")
        val transitionReward = (Transition(
            StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)), casino.GamblersCapitol(2)), 0d)
    
        val updatedValueFunction = policyEvaluator.updateValueFunctionForEpisodeStep(valueFunction, transitionReward)
        logger.info(s"~updatedValueFunction=$updatedValueFunction")
        
        // V (S) ← V(S) + α[R + γV(S′) − V(S)] = 1 + 0.1[0 + 1*2 - 1] = 2
        assertEquals(0.11, updatedValueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        
    }
    
    it should "update ValueFunction For Episode Step with a discount and a step size" in {
        
        val casino = Casino(targetCapitol=5  , probabilityHeads=0.4)
        val initialValue = 0d
        val discount = 0.9
        val stepSize = 0.1
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, initialValue, discount, stepSize)
        val valueFunction = givenConstantStepSizeValueFunction(casino, discount = 0.9, stepSize = 0.1)
        logger.info(s"~valueFunction=$valueFunction")
        val transitionReward = (Transition(
            StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)), casino.GamblersCapitol(2)), 0d)
        
        val updatedValueFunction = policyEvaluator.updateValueFunctionForEpisodeStep(valueFunction, transitionReward)
        logger.info(s"~updatedValueFunction=$updatedValueFunction")
        
        // V (S) ← V(S) + α[R + γV(S′) − V(S)] = 1 + 0.1[0 + 0.9*2 - 1] = 2
        assertEquals(0.108, updatedValueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        
    }
    
    it should "get ValueFunction For Policy" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.9)
        val initialValue = 0d
        val discount = 1d
        val stepSize = 1
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, initialValue, discount, stepSize)
        val policy = ModestBetter(casino)
        val episodes = Vector(
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))))
    
        val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy, episodes)
        logger.info(s"~valueFunction=$valueFunction")
    
        assertEquals(0d, valueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(0d, valueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(0d, valueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
        assertEquals(1d, valueFunction.getValueForState(casino.GamblersCapitol(4)), 0.0001)
    }
    
    it should "get ValueFunction For Policy using four episodes from capitol is 1" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.9)
        val initialValue = 0d
        val discount = 1d
        val stepSize = 1d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, initialValue, discount, stepSize)
        val policy = ModestBetter(casino)
        val episodes = Vector(
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
        )
        
        val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy, episodes)
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(1d, valueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(1d, valueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(1d, valueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
        assertEquals(1d, valueFunction.getValueForState(casino.GamblersCapitol(4)), 0.0001)
    }
    
    it should "get ValueFunction For Policy using four episodes from capitol is 1 with discount is 0.9" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.9)
        val discount = 0.9
        val stepSize = 1d
        val initialValue = 0d
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, initialValue, discount, stepSize)
        val policy = ModestBetter(casino)
        val episodes = Vector(
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
        )
        
        val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy, episodes)
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(0.4782969, valueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(0.6561, valueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(0.9, valueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
        assertEquals(1d, valueFunction.getValueForState(casino.GamblersCapitol(4)), 0.0001)
    }
    
    it should "get ValueFunction For Policy using four episodes from capitol is 1 with step size is 0.5" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.9)
        val discount = 1d
        val initialValue = 0d
        val stepSize = 0.5
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, initialValue, discount, stepSize)
        
        val policy = ModestBetter(casino)
        val episodes = Vector(
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
            Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))),
        )
    
        val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy, episodes)
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(0.08203125, valueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(0.353515625, valueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(0.66796875, valueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
        assertEquals(0.896484375, valueFunction.getValueForState(casino.GamblersCapitol(4)), 0.0001)
    }
    
    it should "get ValueFunction For Policy using 100 episodes for random walk" in {
        val randomWalk = RandomWalk(numberOfTiles=6)
        val policyEvaluator: PolicyEvaluator =
            PolicyEvaluator(randomWalk, initialValue = 0.5, stepSize = 0.05)
    
        val policy = EquiprobablePolicy(randomWalk)
        val episodes = randomWalk.getStarts(numberOfStarts=200)
        
        val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy, episodes)
        logger.info(s"~valueFunction=$valueFunction")
        
        val error = rootMeanSquaredError(randomWalk, valueFunction)
        logger.info(s"~rootMeanSquaredError=$error")
        
        assertEquals(0d, error, 0.125)
    }
    
    private def givenConstantStepSizeValueFunction(casino: Casino, initialValue: Double = 0d,
                                                   discount: Double = 1d, stepSize: StepSize = 1d): ConstantStepSize = {
        Vector((casino.GamblersCapitol(1), 1d), (casino.GamblersCapitol(2), 2d),
            (casino.GamblersCapitol(3), 3d), (casino.GamblersCapitol(4), 4d)).
            foldLeft(ConstantStepSize(casino, initialValue, stepSize))((valueFunction, update) => {
                valueFunction.updateValueForState(update._1, update._2)
            })
    }
    
}
