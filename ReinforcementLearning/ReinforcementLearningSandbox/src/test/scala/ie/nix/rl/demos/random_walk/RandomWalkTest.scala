package ie.nix.rl.demos.random_walk

import ie.nix.rl.{State, StateAction, Transition}
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random

class RandomWalkTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "RandomWalk"
    
    it should "getInitialState with 4 tiles" in {
        
        val numberOfTiles = 4
        val randomWalk = RandomWalk(numberOfTiles)
        val initialState = randomWalk.getInitialState()
        
        logger.info(s"~initialState=$initialState")
        assertEquals(randomWalk.Tile(2), initialState)
        
    }
    
    it should "getInitialState with 5 tiles" in {
        
        val numberOfTiles = 5
        val randomWalk = RandomWalk(numberOfTiles)
        val initialState = randomWalk.getInitialState()
        
        logger.info(s"~initialState=$initialState")
        assertEquals(randomWalk.Tile(2), initialState)
        
    }
    
    it should "get Transition For StateAction(2,Right)" in {
    
        val numberOfTiles = 5
        val randomWalk = RandomWalk(numberOfTiles)
        val stateAction = StateAction(randomWalk.Tile(2), randomWalk.Right)
        val transition = randomWalk.getTransitionForStateAction(stateAction)
        
        logger.info(s"~transition=$transition")
        assertEquals(randomWalk.Tile(3), transition.finalState)
    }
    
    it should "get Transition For StateAction(4,Right)" in {
        
        val numberOfTiles = 5
        val randomWalk = RandomWalk(numberOfTiles)
        val stateAction = StateAction(randomWalk.Tile(4), randomWalk.Right)
        val transition = randomWalk.getTransitionForStateAction(stateAction)
        
        logger.info(s"~transition=$transition")
        assertTrue(transition.finalState.isTerminal)
        assertEquals(randomWalk.Tile(5), transition.finalState)
    }
    
    it should "get Transition For StateAction(1,Left)" in {
        
        val numberOfTiles = 5
        val randomWalk = RandomWalk(numberOfTiles)
        val stateAction = StateAction(randomWalk.Tile(1), randomWalk.Left)
        val transition = randomWalk.getTransitionForStateAction(stateAction)
        
        logger.info(s"~transition=$transition")
        assertTrue(transition.finalState.isTerminal)
        assertEquals(randomWalk.Tile(0), transition.finalState)
    }
    
    it should "get Reward For Transition to 5" in {
        
        val numberOfTiles = 5
        val randomWalk = RandomWalk(numberOfTiles)
        val transition = Transition(StateAction(randomWalk.Tile(4), randomWalk.Right), randomWalk.Tile(5))
        val reward = randomWalk.getRewardForTransition(transition)
    
        logger.info(s"~reward=$reward")
        assertEquals(1d, reward, 0.000001)
        
    }
    
    it should "get Reward For Transition to 0" in {
        
        val numberOfTiles = 5
        val randomWalk = RandomWalk(numberOfTiles)
        val transition = Transition(StateAction(randomWalk.Tile(1), randomWalk.Left), randomWalk.Tile(0))
        val reward = randomWalk.getRewardForTransition(transition)
        
        logger.info(s"~reward=$reward")
        assertEquals(0d, reward, 0.000001)
        
    }
    
    it should "get Reward For Transition to 1" in {
        
        val numberOfTiles = 5
        val randomWalk = RandomWalk(numberOfTiles)
        val transition = Transition(StateAction(randomWalk.Tile(2), randomWalk.Left), randomWalk.Tile(1))
        val reward = randomWalk.getRewardForTransition(transition)
        
        logger.info(s"~reward=$reward")
        assertEquals(0d, reward, 0.000001)
        
    }
    
}
