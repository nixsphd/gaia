package ie.nix.rl.tabular.vf

import ie.nix.rl.{FiniteMDP, State}
import ie.nix.rl.demos.gambler.Casino
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ValueFunctionTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "Value Function"
    
    it should "get Difference when same size when value function are the same size" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val initialValue = 0d
        val valueFunction = givenValueFunction(casino, initialValue, Vector(
            (casino.GamblersCapitol(1), 1d),
            (casino.GamblersCapitol(2), 2d),
            (casino.GamblersCapitol(3), 3d),
            (casino.GamblersCapitol(4), 4d)))
        val otherValueFunction = givenValueFunction(casino, initialValue, Vector(
            (casino.GamblersCapitol(1), 4d),
            (casino.GamblersCapitol(2), 3d),
            (casino.GamblersCapitol(3), 2d),
            (casino.GamblersCapitol(4), 1d)))
        val difference = valueFunction.getDifference(otherValueFunction)
        logger.info(s"~difference=$difference")
        assertEquals(8d, difference, 0.0001)
    
    }
    
    it should "get Difference when same size when value function are different sizes" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val initialValue = 0d
        val valueFunction = givenValueFunction(casino, initialValue, Vector(
            (casino.GamblersCapitol(2), 2d),
            (casino.GamblersCapitol(3), 3d),
            (casino.GamblersCapitol(4), 4d)))
        val otherValueFunction = givenValueFunction(casino, initialValue, Vector(
            (casino.GamblersCapitol(1), 4d),
            (casino.GamblersCapitol(2), 3d),
            (casino.GamblersCapitol(3), 2d)))
        val difference = valueFunction.getDifference(otherValueFunction)
        logger.info(s"~difference=$difference")
        assertEquals(10d, difference, 0.0001)
        
    }
    
    it should "get Difference when same size when value function are the same" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val initialValue = 0d
        val valueFunction = givenValueFunction(casino, initialValue, Vector(
            (casino.GamblersCapitol(1), 1d),
            (casino.GamblersCapitol(2), 2d),
            (casino.GamblersCapitol(3), 3d),
            (casino.GamblersCapitol(4), 4d)))
        val otherValueFunction = givenValueFunction(casino, initialValue, Vector(
            (casino.GamblersCapitol(1), 1d),
            (casino.GamblersCapitol(2), 2d),
            (casino.GamblersCapitol(3), 3d),
            (casino.GamblersCapitol(4), 4d)))
        val difference = valueFunction.getDifference(otherValueFunction)
        logger.info(s"~difference=$difference")
        assertEquals(0d, difference, 0.0001)
        
    }
    
    def givenValueFunction(environment: FiniteMDP, initialValue: Double = 0,
                           initialValueFunctionValues: Vector[(State, Double)]): ValueFunction = {
        initialValueFunctionValues.
            foldLeft(ValueFunction(environment, initialValue))((valueFunction, stateValue) => {
                val state = stateValue._1
                val value = stateValue._2
                valueFunction.updateValueForState(state, value)
            })
    }
    
}
