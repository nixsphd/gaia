package ie.nix.rl.tabular.avf

import ie.nix.rl.{Action, State, StateAction, Transition}
import ie.nix.rl.demos.black_jack.BlackJackTable
import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.mc.MonteCarlo.updateActionValueFunction
import ie.nix.rl.tabular.avf.ActionValueFunction.difference
import ie.nix.rl.tabular.policy.DeterministicPolicyTest.{newTabularActionValueFunctionConservative, newTabularActionValueFunctionFlaithulach, newTabularActionValueFunctionHitOver17}
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.{assertEquals, assertNotEquals, assertTrue}
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ActionValueFunctionTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "ActionValueFunctionTest"
    
    it should "getBestAction when hit" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        val actionValueFunction = newTabularActionValueFunctionHitOver17(blackJackTable)
        val sumOfPlayersCards17State = blackJackTable.BlackJackState(
            sumOfPlayersCards = 17, playerHasUsableAce = false, dealersVisibleCard = 1)
        
        val bestAction = actionValueFunction.getBestAction(sumOfPlayersCards17State)
        logger.info(s"~bestAction=$bestAction")
        
        assertEquals(blackJackTable.Hit, bestAction)
        
    }
    
    it should "getBestAction when stick" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        val actionValueFunction = newTabularActionValueFunctionHitOver17(blackJackTable)
        val sumOfPlayersCards18State = blackJackTable.BlackJackState(
            sumOfPlayersCards = 18, playerHasUsableAce = false, dealersVisibleCard = 1)
        
        val bestAction = actionValueFunction.getBestAction(sumOfPlayersCards18State)
        logger.info(s"~bestAction=$bestAction")
        
        assertEquals(blackJackTable.Stick, bestAction)
        
    }
    
    it should "get a difference for two different AVFs" in {
        
        val casino = Casino(targetCapitol=4 , probabilityHeads=0.4)
        val oneActionValueFunction = newTabularActionValueFunctionFlaithulach(casino)
        val anotherActionValueFunction = newTabularActionValueFunctionConservative(casino)
        
        val differenceBetweenAVFs = difference(oneActionValueFunction, anotherActionValueFunction)
        logger.info(s"~differenceBetweenAVFs=$differenceBetweenAVFs")
        
        assertNotEquals(0d, differenceBetweenAVFs)
        
    }
    
    it should "get Actions For State" in {
        
        val casino = Casino(targetCapitol=4 , probabilityHeads=0.4)
        val actionValueFunction = newTabularActionValueFunctionFlaithulach(casino)
        
        val actions = actionValueFunction.getActionsForState(casino.GamblersCapitol(2))
        logger.info(s"~actions=$actions")
    
        assertEquals(2, actions.size)
        assertTrue(actions.contains(casino.GamblersBet(1)))
        assertTrue(actions.contains(casino.GamblersBet(2)))
        
    }
    
    it should "get StateActions For State" in {
        
        val casino = Casino(targetCapitol=4 , probabilityHeads=0.4)
        val actionValueFunction = newTabularActionValueFunctionFlaithulach(casino)
        
        val stateActions = actionValueFunction.getStateActionsForState(casino.GamblersCapitol(2))
        logger.info(s"~stateActions=$stateActions")
        
        assertEquals(2, stateActions.size)
        assertTrue(stateActions.contains(StateAction(casino.GamblersCapitol(2), casino.GamblersBet(1))))
        assertTrue(stateActions.contains(StateAction(casino.GamblersCapitol(2), casino.GamblersBet(2))))
        
    }
    
}
