package ie.nix.rl.td

import ie.nix.rl.{StateAction, Transition}
import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.demos.random_walk.RandomWalk
import ie.nix.rl.demos.random_walk.RandomWalk.rootMeanSquaredError
import ie.nix.rl.tabular.policy.Policy.EquiprobablePolicy
import ie.nix.rl.tabular.vf.BatchUpdating
import ie.nix.rl.tabular.vf.ConstantStepSize.StepSize
import ie.nix.rl.td.TemporalDifference.BatchUpdatingPolicyEvaluator
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BatchPolicyEvaluatorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "BatchUpdatingPolicyEvaluator"
    
    it should "update ValueFunction For Episode Step without consolidating" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val policyEvaluator = BatchUpdatingPolicyEvaluator(casino)
        val valueFunction = givenValueFunction(casino, stepSize = 0.1)
        logger.info(s"~valueFunction=$valueFunction")
        
        val transitionReward = (Transition(StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)),
            casino.GamblersCapitol(2)), 0d)
        
        val updatedValueFunction = policyEvaluator.updateValueFunctionForEpisodeStep(valueFunction, transitionReward)
        val updatedBatchUpdatingValueFunction = updatedValueFunction.asInstanceOf[BatchUpdating]
        logger.info(s"~updatedBatchUpdatingValueFunction=$updatedBatchUpdatingValueFunction")

        // R + γV(S′) = 0 + 1*0.2 = 0.2
        val valueBeforeConsolidating = updatedValueFunction.getValueForState(casino.GamblersCapitol(1))
        assertEquals(1d, valueBeforeConsolidating, 0.0001)
    
        val batchValueBeforeConsolidating = updatedBatchUpdatingValueFunction.getBatchValueForState(casino.GamblersCapitol(1))
        assertEquals(1.1, batchValueBeforeConsolidating, 0.0001)
    
    }
    
    it should "update ValueFunction For Episode Step with consolidating" in {
        val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
        val policyEvaluator = BatchUpdatingPolicyEvaluator(casino)
        val valueFunction = givenValueFunction(casino, stepSize = 0.1)
        logger.info(s"~valueFunction=$valueFunction")
        val transitionReward = (Transition(StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)),
            casino.GamblersCapitol(2)), 0d)
    
        val updatedValueFunction = policyEvaluator.updateValueFunctionForEpisodeStep(valueFunction, transitionReward)
        val updatedBatchUpdatingValueFunction = updatedValueFunction.asInstanceOf[BatchUpdating]
        val consolidatedValueFunction = updatedBatchUpdatingValueFunction.consolidateBatch
        logger.info(s"~consolidatedValueFunction=$consolidatedValueFunction")
    
        // V(S) ← V(S) + α[target − V(S)] = 0.1 + 0.1[0.2 - 0.1] = 0.1 + 0.1[0.1] = 0.1 + 0.01 = 0.11
        val valueAfterConsolidating = consolidatedValueFunction.getValueForState(casino.GamblersCapitol(1))
        assertEquals(1.1, valueAfterConsolidating, 0.0001)
    
        val batchValueAfterConsolidating = consolidatedValueFunction.getBatchValueForState(casino.GamblersCapitol(1))
        assertEquals(1.1, batchValueAfterConsolidating, 0.0001)
    
    }
    
    it should "get ValueFunction For Policy using 50 episodes batch for random walk" in {
        val randomWalk = RandomWalk(numberOfTiles=6)
        val policyEvaluator =
                BatchUpdatingPolicyEvaluator(randomWalk, initialValue=0.5, stepSize=0.01)
        val policy = EquiprobablePolicy(randomWalk)
        val episodes = randomWalk.getStarts(numberOfStarts=50)
        
        val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy, episodes)
        logger.info(s"~valueFunction=$valueFunction")
        
        val error = rootMeanSquaredError(randomWalk, valueFunction)
        logger.info(s"~rootMeanSquaredError=$error")
        
        assertEquals(0d, error, 0.1)
    }
    
    def givenValueFunction(casino: Casino, initialValue: Double = 0d,
                                   stepSize: StepSize = 1d): BatchUpdating = {
        
        Vector((casino.GamblersCapitol(1), 1d), (casino.GamblersCapitol(2), 2d),
               (casino.GamblersCapitol(3), 3d), (casino.GamblersCapitol(4), 4d)).
            foldLeft(BatchUpdating(casino, initialValue, stepSize))((valueFunction, update) => {
                valueFunction.updateValueForState(update._1, update._2)
            })
    }
    
    def duplicate[E](vector: Vector[E], times: Int = 1): Vector[E] = {
        (1 to times).foldLeft(vector)((vectorOfDuplicates, _) => {
            vectorOfDuplicates ++ vector
        })
    }
    
}
