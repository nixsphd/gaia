package ie.nix.rl.tabular.policy

import ie.nix.rl.tabular.policy.PolicyTest.{TestAction, TestState}
import ie.nix.rl.{Action, State, StateAction}
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PolicyTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    behavior of "Policy"
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    it should "get the Action for a State" in {
        val stateAction = StateAction(TestState(1), TestAction(1))
        val policy = Policy().
            updateStateActionProbability(stateAction, 1d)
        val policyAction = policy.getActionForState(stateAction.state)
        
        logger.info(s"~policyAction=$policyAction")
        assertEquals(stateAction.action, policyAction)
        
    }
    
    it should "get the Action for a State from two even choices" in {
        val state = TestState(1)
        val stateAction1 = StateAction(state, TestAction(1))
        val stateAction2 = StateAction(state, TestAction(2))
        val policy = Policy().
            updateStateActionProbability(stateAction1, 0.5).
            updateStateActionProbability(stateAction2, 0.5)
        val policyAction = policy.getActionForState(state)
        
        logger.info(s"~policyAction=$policyAction")
        assertEquals(stateAction2.action, policyAction)
        
    }
    
    it should "get the Action for a State from two unbalanced choices" in {
        val state = TestState(1)
        val stateAction1 = StateAction(state, TestAction(1))
        val stateAction2 = StateAction(state, TestAction(2))
        val policy = Policy().
            updateStateActionProbability(stateAction1, 0.1).
            updateStateActionProbability(stateAction2, 0.9)
        val policyAction = policy.getActionForState(state)
        
        logger.info(s"~policyAction=$policyAction")
        assertEquals(stateAction2.action, policyAction)
        
    }
    
}

object PolicyTest {
    
    case class TestState(id: Int) extends State {
        
        override def toString: String = s"TestState($id)"
        
        override def isTerminal: Boolean = id == 0
        
    }
    
    case class TestAction(id: Int) extends Action {
        override def toString: String = s"TestAction($id)"
    }
    
}
