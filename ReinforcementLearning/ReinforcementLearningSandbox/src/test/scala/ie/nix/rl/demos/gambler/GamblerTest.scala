package ie.nix.rl.demos.gambler

import ie.nix.rl.demos.gambler.Gambler.{ModestBetter, OstentatiousBetter}
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class GamblerTest extends AnyFlatSpec with BeforeAndAfterEach {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "GamblerTest"
    
    it should "ModestBetter" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val modestBetter = ModestBetter(casino)
        
        assertEquals(casino.GamblersBet(1), modestBetter.getActionForState(casino.GamblersCapitol(1)))
        assertEquals(casino.GamblersBet(1), modestBetter.getActionForState(casino.GamblersCapitol(2)))
        assertEquals(casino.GamblersBet(1), modestBetter.getActionForState(casino.GamblersCapitol(3)))
        assertEquals(casino.GamblersBet(1), modestBetter.getActionForState(casino.GamblersCapitol(4)))
    }
    
    it should "OstentatiousBetter" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val ostentatiousBetter = OstentatiousBetter(casino)
    
        assertEquals(casino.GamblersBet(1), ostentatiousBetter.getActionForState(casino.GamblersCapitol(1)))
        assertEquals(casino.GamblersBet(2), ostentatiousBetter.getActionForState(casino.GamblersCapitol(2)))
        assertEquals(casino.GamblersBet(2), ostentatiousBetter.getActionForState(casino.GamblersCapitol(3)))
        assertEquals(casino.GamblersBet(1), ostentatiousBetter.getActionForState(casino.GamblersCapitol(4)))
    
    }
    
}
