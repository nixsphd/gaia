package ie.nix.rl.demos.gambler

import ie.nix.rl.{StateAction, Transition}
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CasinoTest extends AnyFlatSpec with Logging {
        
    behavior of "Casino"
    
    it should "have 5 states" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val states = casino.getStates()
        logger.trace(s"~states=$states")
        
        assertEquals(5, states.length)
    }
    
    it should "have 2 terminal states" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val terminalStates = casino.getStates().filter(state => state.isTerminal)
        logger.trace(s"~terminalStates)=$terminalStates")
        assertEquals(2, terminalStates.length)
    }
    
    it should "have 3 non-terminal states" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val nonTerminalStates = casino.getStates().filter(state => !state.isTerminal)
        logger.trace(s"~nonTerminalStates)=$nonTerminalStates")
        assertEquals(3, nonTerminalStates.length)
    }
    
    it should "have remain state transition probability of 1 for a no bet" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val noBetTransition = Transition(StateAction(casino.GamblersCapitol(1), casino.GamblersBet(0)), casino.GamblersCapitol(1))
        val noBetTransitionProbability = casino.getProbabilityOfTransition(noBetTransition)
        logger.trace(s"~noBetTransition)=$noBetTransition, noBetTransitionProbability=$noBetTransitionProbability")
        assertEquals(1d, noBetTransitionProbability, 0d)
    }
    
    it should "have change state transition probability of 1 for a no bet" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val noBetTransition = Transition(StateAction(casino.GamblersCapitol(1), casino.GamblersBet(0)), casino.GamblersCapitol(2))
        val noBetTransitionProbability = casino.getProbabilityOfTransition(noBetTransition)
        logger.trace(s"~noBetTransition)=$noBetTransition, noBetTransitionProbability=$noBetTransitionProbability")
        assertEquals(0d, noBetTransitionProbability, 0d)
    }
    
    it should "have state transition probability of 0.4 to the win state" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val winTransition = Transition(StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)), casino.GamblersCapitol(2))
        val winTransitionProbability = casino.getProbabilityOfTransition(winTransition)
        logger.trace(s"~winTransition)=$winTransition, winTransitionProbability=$winTransitionProbability")
        assertEquals(0.4, winTransitionProbability, 0d)
    }
    
    it should "have state transition probability of 0.6 to the loose state" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val looseTransition = Transition(StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)), casino.GamblersCapitol(0))
        val looseTransitionProbability = casino.getProbabilityOfTransition(looseTransition)
        logger.trace(s"~looseTransition)=$looseTransition, looseTransitionProbability=$looseTransitionProbability")
        assertEquals(0.6, looseTransitionProbability, 0d)
    }
    
    it should "have no state transition probability for any other state" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val noTransition = Transition(StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)), casino.GamblersCapitol(3))
        val noTransitionProbability = casino.getProbabilityOfTransition(noTransition)
        logger.trace(s"~noTransition)=$noTransition, noTransitionProbability=$noTransitionProbability")
        assertEquals(0d, noTransitionProbability, 0d)
    }
    
    it should "have no reward for a state less than the targetCapitol" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val notTotargetCapitolTransition = Transition(StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)), casino.GamblersCapitol(2))
        val notTotargetCapitolTransitionReward = casino.getRewardForTransition(notTotargetCapitolTransition)
        logger.trace(s"~notTotargetCapitolTransition)=$notTotargetCapitolTransition, notTotargetCapitolTransitionReward=$notTotargetCapitolTransitionReward")
        assertEquals(0d, notTotargetCapitolTransitionReward, 0d)
    }
    
    it should "have a reward of 1 for the targetCapitol state" in {
        val casino: Casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val totargetCapitolTransition = Transition(StateAction(casino.GamblersCapitol(2), casino.GamblersBet(2)), casino.GamblersCapitol(4))
        val totargetCapitolTransitionReward = casino.getRewardForTransition(totargetCapitolTransition)
        logger.trace(s"~totargetCapitolTransition)=$totargetCapitolTransition, totargetCapitolTransitionReward=$totargetCapitolTransitionReward")
        assertEquals(1d, totargetCapitolTransitionReward, 0d)
    }
    
}
