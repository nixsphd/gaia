package ie.nix.rl.tabular.vf

import ie.nix.rl.demos.gambler.Casino
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BatchUpdatingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "BatchUpdating"
    
    it should "add BatchValue For State from initial value" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val batchUpdating = BatchUpdating(casino, initialValue = 1d, stepSize = 1d)
    
        val updatedBatchUpdating = batchUpdating.addBatchValueForState(casino.GamblersCapitol(1), 10d)
    
        logger.info(s"~updatedBatchUpdating=$updatedBatchUpdating")
        assertEquals(1d, updatedBatchUpdating.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(10d, updatedBatchUpdating.getBatchValueForState(casino.GamblersCapitol(1)), 0.0001)
    }
    
    it should "addBatchValueForState from value 5" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val batchUpdating = BatchUpdating(casino, initialValue = 1d, stepSize = 1d).
            updateValueForState(casino.GamblersCapitol(1), 5d)
        
        val updatedBatchUpdating = batchUpdating.addBatchValueForState(casino.GamblersCapitol(1), 10d)
        
        logger.info(s"~updatedBatchUpdating=$updatedBatchUpdating")
        assertEquals(5d, updatedBatchUpdating.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(10d, updatedBatchUpdating.getBatchValueForState(casino.GamblersCapitol(1)), 0.0001)
    }
    
    it should "consolidateBatch from initial value" in {
        
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val batchUpdating = BatchUpdating(casino, initialValue = 1d, stepSize = 1d)
    
        val updatedBatchUpdating = batchUpdating.addBatchValueForState(casino.GamblersCapitol(1), 10d)
        val consolidatedBatchUpdating = updatedBatchUpdating.consolidateBatch
        
        logger.info(s"~consolidatedBatchUpdating=$consolidatedBatchUpdating")
        assertEquals(10d, consolidatedBatchUpdating.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(10d, consolidatedBatchUpdating.getBatchValueForState(casino.GamblersCapitol(1)), 0.0001)
        
    }
    
    it should "consolidateBatch from 5" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val batchUpdating = BatchUpdating(casino, initialValue = 1d, stepSize = 1d).
            updateValueForState(casino.GamblersCapitol(1), 5d)
    
        val updatedBatchUpdating = batchUpdating.addBatchValueForState(casino.GamblersCapitol(1), 10d)
        val consolidatedBatchUpdating = updatedBatchUpdating.consolidateBatch
        
        logger.info(s"~consolidatedBatchUpdating=$consolidatedBatchUpdating")
        assertEquals(10d, consolidatedBatchUpdating.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(10d, consolidatedBatchUpdating.getBatchValueForState(casino.GamblersCapitol(1)), 0.0001)
    }
    
    it should "addBatchValueForState" in {
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val batchUpdating = BatchUpdating(casino, initialValue = 1d, stepSize = 1d).
            updateValueForState(casino.GamblersCapitol(1), 5d)
        
        val updatedBatchUpdating = batchUpdating.addBatchValueForState(casino.GamblersCapitol(1), 10d)
        
        logger.info(s"~updatedBatchUpdating=$updatedBatchUpdating")
        assertEquals(5d, updatedBatchUpdating.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(10d, updatedBatchUpdating.getBatchValueForState(casino.GamblersCapitol(1)), 0.0001)
    }
    
    it should "updateValueForState" in {
    
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val batchUpdating = BatchUpdating(casino, initialValue = 1d, stepSize = 1d)
    
        val updatedBatchUpdating = batchUpdating.updateValueForState(casino.GamblersCapitol(1), 10d)
        logger.info(s"~updatedBatchUpdating=$updatedBatchUpdating")
        
        assertEquals(10, updatedBatchUpdating.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(10, updatedBatchUpdating.getBatchValueForState(casino.GamblersCapitol(1)), 0.0001)
        
    }
    
}
