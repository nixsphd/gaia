package ie.nix.rl.dp

import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.demos.gambler.Gambler.ModestBetter
import ie.nix.rl.dp.DynamicProgrammingTest.newTabularValueFunction
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PolicyImproverTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "DP PolicyImprover"
    
    it should "improve" in {
        
        val casino = Casino(targetCapitol=5, probabilityHeads=0.4)
        val policyImprover: PolicyImprover = PolicyImprover(casino, discount=1d)
        val policy = ModestBetter(casino)
        val valueFunction = newTabularValueFunction(casino)
    
        val (improvedPolicy, difference) = policyImprover.improve(policy, valueFunction)
        logger.info(s"~improvedPolicy=$improvedPolicy")
    
        assertTrue(difference != 0)
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(1)))
        assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(2)))
        assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(3)))
        assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(4)))
        
    }
    
}
