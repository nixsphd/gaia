package ie.nix.rl.dp

import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.demos.gambler.Gambler.ModestBetter
import ie.nix.rl.tabular.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PolicyEvaluatorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "DP PolicyEvaluator"
    
    it should "get a ValueFunction for a Policy with 1 sweep" in {
        val numberOfSweeps=1
        val casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, numberOfSweeps, discount=1d)
    
        val policy = ModestBetter(casino)
        logger.info(s"~policy=$policy")
        
        val valueFunction: ValueFunction = policyEvaluator.getValueFunctionForPolicy(policy)
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(0d, valueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(0d, valueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(0.4, valueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
    }
    
    it should "get a ValueFunction for a Policy with 2 sweep" in {
        val numberOfSweeps=2
        val casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, numberOfSweeps, discount=1d)
    
        val policy = ModestBetter(casino)
        logger.info(s"~policy=$policy")
        
        val valueFunction: ValueFunction = policyEvaluator.getValueFunctionForPolicy(policy)
        logger.info(s"~valueFunction=$valueFunction")
    
        assertEquals(0d, valueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(0.16, valueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(0.496, valueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
    }
    
    it should "get a ValueFunction for a Policy with 3 sweep" in {
        val numberOfSweeps=3
        val casino = Casino(targetCapitol=4, probabilityHeads=0.4)
        val policyEvaluator: PolicyEvaluator = PolicyEvaluator(casino, numberOfSweeps, discount=1d)
    
        val policy = ModestBetter(casino)
        logger.info(s"~policy=$policy")
        
        val valueFunction: ValueFunction = policyEvaluator.getValueFunctionForPolicy(policy)
        logger.info(s"~valueFunction=$valueFunction")
        
        assertEquals(0.064, valueFunction.getValueForState(casino.GamblersCapitol(1)), 0.0001)
        assertEquals(0.2368, valueFunction.getValueForState(casino.GamblersCapitol(2)), 0.0001)
        assertEquals(0.54208, valueFunction.getValueForState(casino.GamblersCapitol(3)), 0.0001)
    }
    
}
