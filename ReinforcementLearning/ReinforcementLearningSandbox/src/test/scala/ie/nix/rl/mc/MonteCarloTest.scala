package ie.nix.rl.mc

import ie.nix.rl.{StateAction, Transition}
import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.mc.MonteCarlo.updateActionValueFunction
import ie.nix.rl.tabular.avf.{ActionValueFunction, WeightedAveraging}
import ie.nix.rl.tabular.policy.Policy.EquiprobablePolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class MonteCarloTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "MonteCarlo"
    
    it should "update ActionValueFunction For Episode" in {
        
        val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
        val discount = 1d
        val actionValueFunction = ActionValueFunction(casino, initialValue = 0d)
        val stateAction11 = StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))
        val stateAction21 = StateAction(casino.GamblersCapitol(2), casino.GamblersBet(1))
        val stateAction31 = StateAction(casino.GamblersCapitol(3), casino.GamblersBet(1))
        val stateAction41 = StateAction(casino.GamblersCapitol(4), casino.GamblersBet(1))
        val visitedTransitionRewards = Vector(
            (Transition(stateAction11, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction21, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction31, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction41, casino.GamblersCapitol(1)), 1d))
         
        val updatedActionValueFunction: ActionValueFunction =
            updateActionValueFunction(actionValueFunction, discount, visitedTransitionRewards)
        logger.info(s"~updatedActionValueFunction=$updatedActionValueFunction")
        
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction11))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction21))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction31))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction41))
        
    }
    
    it should "update ActionValueFunction For Episode using a discount" in {
        
        val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
        val actionValueFunction = ActionValueFunction(casino, initialValue = 0d)
        val discount = 0.8
        val stateAction11 = StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))
        val stateAction21 = StateAction(casino.GamblersCapitol(2), casino.GamblersBet(1))
        val stateAction31 = StateAction(casino.GamblersCapitol(3), casino.GamblersBet(1))
        val stateAction41 = StateAction(casino.GamblersCapitol(4), casino.GamblersBet(1))
        val visitedTransitionRewards = Vector(
            (Transition(stateAction11, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction21, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction31, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction41, casino.GamblersCapitol(1)), 1d))
        //        val visitedStateActionRewards =
        //            visitedTransitionRewards.map(transitionReward => (transitionReward._1.stateAction, transitionReward._2))
        
        val updatedActionValueFunction: ActionValueFunction =
            updateActionValueFunction(actionValueFunction, discount, visitedTransitionRewards)
        logger.info(s"~updatedActionValueFunction=$updatedActionValueFunction")
        
        assertEquals(0.512, updatedActionValueFunction.getValueForStateAction(stateAction11), 0.00001)
        assertEquals(0.64, updatedActionValueFunction.getValueForStateAction(stateAction21), 0.00001)
        assertEquals(0.8, updatedActionValueFunction.getValueForStateAction(stateAction31), 0.00001)
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction41), 0.00001)
        
    }
    
    it should "update ActionValueFunction For offline Episode where target is behaviour" in {
        
        val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
        val discount = 1d
        val actionValueFunction = WeightedAveraging(casino, initialValue = 0d)
        val stateAction11 = StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))
        val stateAction21 = StateAction(casino.GamblersCapitol(2), casino.GamblersBet(1))
        val stateAction31 = StateAction(casino.GamblersCapitol(3), casino.GamblersBet(1))
        val stateAction41 = StateAction(casino.GamblersCapitol(4), casino.GamblersBet(1))
        val visitedTransitionRewards = Vector(
            (Transition(stateAction11, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction21, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction31, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction41, casino.GamblersCapitol(1)), 1d))
        val behaviourPolicy = EquiprobablePolicy(casino)
        
        val updatedActionValueFunction: ActionValueFunction =
            updateActionValueFunction(actionValueFunction, discount, visitedTransitionRewards, behaviourPolicy)
        logger.info(s"~updatedActionValueFunction=$updatedActionValueFunction")
        
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction11))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction21))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction31))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction41))
        
    }
    
    it should "update ActionValueFunction For offline Episode" in {
        
        val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
        val discount = 1d
        val stateAction11 = StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))
        val stateAction21 = StateAction(casino.GamblersCapitol(2), casino.GamblersBet(1))
        val stateAction31 = StateAction(casino.GamblersCapitol(3), casino.GamblersBet(1))
        val stateAction41 = StateAction(casino.GamblersCapitol(4), casino.GamblersBet(1))
        val visitedTransitionRewards = Vector(
            (Transition(stateAction11, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction21, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction31, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction41, casino.GamblersCapitol(1)), 1d))
        val actionValueFunction = WeightedAveraging(casino, initialValue = 0d)
        val behaviourPolicy = EquiprobablePolicy(casino)
        
        val updatedActionValueFunction: ActionValueFunction =
            updateActionValueFunction(actionValueFunction, discount, visitedTransitionRewards, behaviourPolicy)
        logger.info(s"~updatedActionValueFunction=$updatedActionValueFunction")
        
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction11))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction21))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction31))
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction41))
        
    }
    
    it should "update ActionValueFunction For offline Episode using a discount" in {
        
        val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
        val discount = 0.8
        val stateAction11 = StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1))
        val stateAction21 = StateAction(casino.GamblersCapitol(2), casino.GamblersBet(1))
        val stateAction31 = StateAction(casino.GamblersCapitol(3), casino.GamblersBet(1))
        val stateAction41 = StateAction(casino.GamblersCapitol(4), casino.GamblersBet(1))
        val visitedTransitionRewards = Vector(
            (Transition(stateAction11, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction21, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction31, casino.GamblersCapitol(1)), 0d),
            (Transition(stateAction41, casino.GamblersCapitol(1)), 1d))
        val actionValueFunction = WeightedAveraging(casino, initialValue = 0d)
        val behaviourPolicy = EquiprobablePolicy(casino)
        
        val updatedActionValueFunction: ActionValueFunction =
            updateActionValueFunction(actionValueFunction, discount, visitedTransitionRewards, behaviourPolicy)
        logger.info(s"~updatedActionValueFunction=$updatedActionValueFunction")
        
        assertEquals(0.512, updatedActionValueFunction.getValueForStateAction(stateAction11), 0.00001)
        assertEquals(0.64, updatedActionValueFunction.getValueForStateAction(stateAction21), 0.00001)
        assertEquals(0.8, updatedActionValueFunction.getValueForStateAction(stateAction31), 0.00001)
        assertEquals(1d, updatedActionValueFunction.getValueForStateAction(stateAction41), 0.00001)
        
    }
    
}
