package ie.nix.rl.tabular.vf

import ie.nix.rl.demos.random_walk.RandomWalk
import ie.nix.rl.tabular.vf.ConstantStepSize.StepSize
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ConstantStepSizeTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    override def beforeEach() {
        Random.setSeed(0)
    }
    
    behavior of "ConstantStepSizeTest"
    
    it should "update Value For State" in {
    
        val randomWalk = RandomWalk(numberOfTiles = 4)
        val valueFunction = givenValueFunction(randomWalk)
        
        val updatedValueFunction = valueFunction.updateValueForState(randomWalk.Tile(3), 2d)
        
        val updatedValue = updatedValueFunction.getValueForState(randomWalk.Tile(3))
    
        logger.info(s"~updatedValue=$updatedValue")
        assertEquals(2d, updatedValue, 0.0000001)
        
    }
    
    it should "update Value For State with StepSize 0.1" in {
        
        val randomWalk = RandomWalk(numberOfTiles = 4)
        val valueFunction = givenValueFunction(randomWalk, initialValue = 0.5, stepSize= 0.1)
    
        val updatedValueFunction = valueFunction.updateValueForState(randomWalk.Tile(3), 2d)
    
        val updatedValue = updatedValueFunction.getValueForState(randomWalk.Tile(3))
        
        logger.info(s"~updatedValue=$updatedValue")
        assertEquals(0.695, updatedValue, 0.0000001)
        
    }
    
    private def givenValueFunction(randomWalk: RandomWalk, initialValue: Double = 0d,
                                   stepSize: StepSize = 1d) = {
        Vector((randomWalk.Tile(1), 1d), (randomWalk.Tile(2), 1d), (randomWalk.Tile(3), 1d)).
            foldLeft(ConstantStepSize(randomWalk, initialValue, stepSize))((valueFunction, update) => {
                valueFunction.updateValueForState(update._1, update._2)
            })
    }
    
    
}
