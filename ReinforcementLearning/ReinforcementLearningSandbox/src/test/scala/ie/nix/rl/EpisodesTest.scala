package ie.nix.rl

import ie.nix.rl.demos.black_jack.BlackJackTable
import ie.nix.rl.demos.black_jack.Player.{Hitter, Sticker}
import ie.nix.rl.demos.gambler.Casino
import ie.nix.rl.demos.gambler.Gambler.ModestBetter
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.assertEquals
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class EpisodesTest extends AnyFlatSpec  with BeforeAndAfterEach with Logging {
    
    override def beforeEach(): Unit = {
        Random.setSeed(0)
    }
    
    behavior of "Episodes"
    
    it should "runEpisodeFromStateAction for Sticker policy" in {
    
        val blackJackTable: BlackJackTable = new BlackJackTable() with Episodes
        val stickerPolicy = Sticker(blackJackTable)
        logger.info(s"~stickerPolicy=$stickerPolicy")
        
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=12, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Hit
        val episode = Episode(blackJackTable, StateAction(state, action))
        logger.info(s"~episode=$episode")
        
        val transitionRewards = episode.runEpisode(stickerPolicy)
        logger.info(s"~transitionRewards=$transitionRewards")
    
        val visitedStateActions = transitionRewards.map(_._1.stateAction)
        val rewards = transitionRewards.map(_._2)
        assert(visitedStateActions.contains(episode.initialStateAction))
        assert(visitedStateActions.contains(
            StateAction(blackJackTable.BlackJackState(19, false, 1), blackJackTable.Stick)))
        assertEquals(rewards.sum, -1d, 0.00001)
    }
    
    it should "runEpisodeFromStateAction for Hitter policy" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable() with Episodes
        val hitterPolicy = Hitter(blackJackTable)
        logger.info(s"~hitterPolicy=$hitterPolicy")
        
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=12, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Hit
        val episode = Episode(blackJackTable, StateAction(state, action))
        logger.info(s"~episode=$episode")
        
        val transitionRewards = episode.runEpisode(hitterPolicy)
        logger.info(s"~transitionRewards=$transitionRewards")
    
        val visitedStateActions = transitionRewards.map(_._1.stateAction)
        val rewards = transitionRewards.map(_._2)
        logger.debug(s"~rewards=$rewards")
        assert(visitedStateActions.contains(episode.initialStateAction))
        assert(visitedStateActions.contains(
            StateAction(blackJackTable.BlackJackState(19, false, 1), blackJackTable.Hit)))
        assertEquals(rewards.sum, -1d, 0.00001)
    }
    
    it should "correctly order the visited StateActions" in {
    
        val casino = Casino(targetCapitol=5, probabilityHeads=0.8)
        val policy = ModestBetter(casino)
        val episode = Episode(casino, StateAction(casino.GamblersCapitol(1), casino.GamblersBet(1)))
    
        val transitionRewards = episode.runEpisode(policy)
        logger.debug(s"~transitionRewards=$transitionRewards")
    
        val visitedStateActions = transitionRewards.map(_._1.stateAction)
        
        assert(visitedStateActions(0) == episode.initialStateAction)
        assert(visitedStateActions(1) == StateAction(casino.GamblersCapitol(2), casino.GamblersBet(1)))
        assert(visitedStateActions(2) == StateAction(casino.GamblersCapitol(3), casino.GamblersBet(1)))
        assert(visitedStateActions(3) == StateAction(casino.GamblersCapitol(4), casino.GamblersBet(1)))
    
    }
    
    it should "getExploringStarts" in {
        val blackJackTable: BlackJackTable = new BlackJackTable() with Episodes
        val exploringStarts = blackJackTable.getExploringStart()
        logger.debug(s"~exploringStarts=$exploringStarts")
    
        assertEquals(400, exploringStarts.length)
        assertEquals(400, exploringStarts.toSet.size)
        
    }
    
    it should "getRandomStarts" in {
        val blackJackTable: BlackJackTable = new BlackJackTable() with Episodes
        val numberOfRandomStarts = 10
        val randomStarts = blackJackTable.getRandomStarts(numberOfRandomStarts)
        logger.info(s"~numberOfRandomStarts=$numberOfRandomStarts, randomStarts=$randomStarts")
        
        assertEquals(numberOfRandomStarts, randomStarts.length)
        assertEquals(numberOfRandomStarts, randomStarts.toSet.size)
        
    }
}
