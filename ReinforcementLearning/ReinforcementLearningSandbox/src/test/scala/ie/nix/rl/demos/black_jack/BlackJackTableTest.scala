package ie.nix.rl.demos.black_jack

import ie.nix.rl.Environment.Reward
import ie.nix.rl.{StateAction, Transition}
import org.apache.logging.log4j.scala.Logging
import org.junit.Assert.{assertEquals, assertNotEquals}
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.PrivateMethodTester._
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BlackJackTableTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {
    
    // private methods to be tested
    private val getStateAfterPlayerHit =
        PrivateMethod[BlackJackTable#BlackJackState]('getStateAfterPlayerHit)
    private val getRewardAfterDealerPlays =
        PrivateMethod[Reward]('getRewardAfterDealerPlays)
    private val getNextStateGivenPlayerCard =
        PrivateMethod[BlackJackTable#BlackJackState]('getNextStateGivenPlayerCard)
    
    override def beforeEach(): Unit = {
        Random.setSeed(0)
    }
    
    behavior of "BlackJackTableTest"
    
    it should "getTransitionForStateAction for Stick" in {
    
        val blackJackTable: BlackJackTable = new BlackJackTable()
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Stick
        val stateAction = StateAction(state, action)
        val transition: Transition = blackJackTable.getTransitionForStateAction(stateAction)
        logger.debug(s"~transition=$transition")
    
        assertEquals(state, transition.finalState)
    
    }
    
    it should "getTransitionForStateAction for Hit" in {
        
        val blackJackTable: BlackJackTable = new BlackJackTable()
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Hit
        val stateAction = StateAction(state, action)
        val transition: Transition = blackJackTable.getTransitionForStateAction(stateAction)
        logger.debug(s"~transition=$transition")
        
        assertNotEquals(state, transition.finalState)
        
    }
    
    it should "initialise the table" in {
    
        val blackJackTable = new BlackJackTable()
        val initialisedTable = blackJackTable.getInitialState()
        logger.debug(s"~initialisedTable=$initialisedTable")
    
        assertEquals(12, initialisedTable.sumOfPlayersCards)
        assertEquals(false, initialisedTable.playerHasUsableAce)
        assertEquals(7, initialisedTable.dealersVisibleCard)
    }
    
    it should "get State of 18 after Player Hit" in {
        
        val blackJackTable = new BlackJackTable()
    
        val initialState = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val newState = blackJackTable invokePrivate getStateAfterPlayerHit(initialState)
        logger.debug(s"~newState=$newState")
        
        assertEquals(18, newState.sumOfPlayersCards)
    }
    
    it should "get usable ace State after Player Hit" in {
    
        val blackJackTable = new BlackJackTable()
        ensureNextDealIsAnAce(blackJackTable)
        
        val initialState = blackJackTable.BlackJackState(
            sumOfPlayersCards=10, playerHasUsableAce=false, dealersVisibleCard=1)
        val newState = blackJackTable invokePrivate getStateAfterPlayerHit(initialState)
        logger.debug(s"~newState=$newState")
        
        assertEquals(21, newState.sumOfPlayersCards)
        assertEquals(true, newState.playerHasUsableAce)
    }
    
    it should "get Next State Given Player Card" in {
        
        val blackJackTable = new BlackJackTable()
        
        val initialState = blackJackTable.BlackJackState(
            sumOfPlayersCards=18, playerHasUsableAce=true, dealersVisibleCard=1)
        val newState = blackJackTable invokePrivate getNextStateGivenPlayerCard(initialState, 5)
        logger.info(s"~newState=$newState")
        
        assertEquals(18+5-10, newState.sumOfPlayersCards)
        assertEquals(false, newState.playerHasUsableAce)
    }
    
    it should "get 0 Reward For Transition hit to 19" in {
        val blackJackTable = new BlackJackTable()
        val initialState = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Hit
        val finalState = blackJackTable.BlackJackState(
            sumOfPlayersCards=19, playerHasUsableAce=false, dealersVisibleCard=1)
        val transition: Transition = Transition(StateAction(initialState, action), finalState)
        val reward: Reward =  blackJackTable.getRewardForTransition(transition)
        logger.debug(s"~transition=$transition, reward=$reward")
    
        assertEquals(0d, reward, 0.000001)
    }
    
    it should "get -1 Reward For Transition Hit to bust" in {
        val blackJackTable = new BlackJackTable()
        val initialState = blackJackTable.BlackJackState(
            sumOfPlayersCards=18, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Hit
        val finalState = blackJackTable.BlackJackState(
            sumOfPlayersCards=22, playerHasUsableAce=false, dealersVisibleCard=1)
        val transition: Transition = Transition(StateAction(initialState, action), finalState)
        val reward: Reward =  blackJackTable.getRewardForTransition(transition)
        logger.debug(s"~transition=$transition, reward=$reward")
        
        assertEquals(-1d, reward, 0.000001)
    }
    
    it should "get 1 Reward For Transition Stick at 21" in {
        val blackJackTable = new BlackJackTable()
        val initialState = blackJackTable.BlackJackState(
            sumOfPlayersCards=21, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Stick
        val finalState = blackJackTable.BlackJackState(
            sumOfPlayersCards=21, playerHasUsableAce=false, dealersVisibleCard=1)
        val transition: Transition = Transition(StateAction(initialState, action), finalState)
        val reward: Reward =  blackJackTable.getRewardForTransition(transition)
        logger.debug(s"~transition=$transition, reward=$reward")
        
        assertEquals(1d, reward, 0.000001)
    }
    
    it should "get -1 Reward For Transition Stick at 11" in {
        val blackJackTable = new BlackJackTable()
        val initialState = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val action = blackJackTable.Stick
        val finalState = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val transition: Transition = Transition(StateAction(initialState, action), finalState)
        val reward: Reward =  blackJackTable.getRewardForTransition(transition)
        logger.debug(s"~transition=$transition, reward=$reward")
        
        assertEquals(-1d, reward, 0.000001)
    }
    
    it should "get -1 Reward For State " in {
        val blackJackTable = new BlackJackTable()
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=11, playerHasUsableAce=false, dealersVisibleCard=1)
        val reward: Reward =  blackJackTable invokePrivate getRewardAfterDealerPlays(state)
        logger.debug(s"~state=$state, reward=$reward")
        
        assertEquals(-1d, reward, 0.000001)
    }
    
    it should "get 1 Reward For State " in {
        val blackJackTable = new BlackJackTable()
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=21, playerHasUsableAce=false, dealersVisibleCard=1)
        val reward: Reward =  blackJackTable invokePrivate getRewardAfterDealerPlays(state)
        logger.debug(s"~state=$state, reward=$reward")
        
        assertEquals(1d, reward, 0.000001)
    }
    
    it should "get 0 Reward For State " in {
        val blackJackTable = new BlackJackTable()
        val state = blackJackTable.BlackJackState(
            sumOfPlayersCards=18, playerHasUsableAce=false, dealersVisibleCard=1)
        val reward: Reward =  blackJackTable invokePrivate getRewardAfterDealerPlays(state)
        logger.debug(s"~state=$state, reward=$reward")
        
        assertEquals(0d, reward, 0.000001)
    }
    
    private def ensureNextDealIsAnAce(blackJackTable: BlackJackTable) = {
        Random.setSeed(1)
        blackJackTable.dealNextCard
    }
    
}
