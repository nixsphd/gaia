package ie.nix.rl_java.policy;

import ie.nix.rl_java.TestEnvironment;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.MDPTestUtils;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy.Type;
import ie.nix.util_java.Randomness;
import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class PolicyTest extends TestCase {
	
	TabularFiniteMDP environment;

    @Override
	protected void setUp() {

    	Randomness.setSeed(3);

    	int numberOfStates = 1;
    	int numberOfActions = 5;
    	
    	environment = new TestEnvironment(numberOfStates, numberOfActions);
	}
    
	public void testSelectActionDeterministicGreedy() {	
		TabularPolicy policy = newTabularPolicy(environment, Policy.Type.Deterministic, 0.0,
		    	new Action[][]{ 
					{environment.getActions()[1]},
				},
		    	new double[][]{
		    		{1.0}
				});
		
		Action extectedSelectedAction = environment.getActions()[1];
		testSelectAction(policy, extectedSelectedAction);
		
	}
	
	public void testSelectActionDeterministicSoft() {	
		TabularPolicy policy = newTabularPolicy(environment, Policy.Type.Deterministic, 0.1,
		    	new Action[][]{ 
					{environment.getActions()[1]},
				},
		    	new double[][]{
		    		{1.0}
				});
		
		Action extectedSelectedAction = environment.getActions()[1];
		testSelectAction(policy, extectedSelectedAction);
		
	}
	
	public void testSelectActionEquiprobableGreedy() {	
		TabularPolicy policy = newTabularPolicy(environment, Policy.Type.Equiprobable, 0.0,
		    	new Action[][]{ 
					{environment.getActions()[1], environment.getActions()[3]},
				},
		    	new double[][]{
		    		{0.5, 0.5}
				});
		
		Action extectedSelectedAction = environment.getActions()[3];
		testSelectAction(policy, extectedSelectedAction);
		
	}
	
	public void testSelectActionEquiprobableSoft() {	
		TabularPolicy policy = newTabularPolicy(environment, Policy.Type.Equiprobable, 0.1,
		    	new Action[][]{ 
					{environment.getActions()[0], environment.getActions()[1]},
				},
		    	new double[][]{
		    		{0.5, 0.5}
				});
		
		Action extectedSelectedAction = environment.getActions()[1];
		testSelectAction(policy, extectedSelectedAction);
		
	}
	
	public void testSelectActionEquiprobableBigSoft() {	
		TabularPolicy policy = newTabularPolicy(environment, Policy.Type.Equiprobable, 0.3,
		    	new Action[][]{ 
					{environment.getActions()[0], environment.getActions()[1], 
					 environment.getActions()[2], environment.getActions()[4]},
				},
		    	new double[][]{
		    		{0.25, 0.25, 0.25, 0.25}
				});
		
		Action extectedSelectedAction = environment.getActions()[3];
		testSelectAction(policy, extectedSelectedAction);
		
	}
	
	public void testSelectActionStochasticGreedy() {	
		TabularPolicy policy = newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
		    	new Action[][]{ 
					{environment.getActions()[0], environment.getActions()[1]},
				},
		    	new double[][]{
		    		{0.75, 0.25}
				});
		
		Action extectedSelectedAction = environment.getActions()[0];
		testSelectAction(policy, extectedSelectedAction);
		
	}
	
	public void testSelectActionStochasticSoft() {	
		TabularPolicy policy = newTabularPolicy(environment, Policy.Type.Stochastic, 0.1,
		    	new Action[][]{ 
					{environment.getActions()[0], environment.getActions()[1]},
				},
		    	new double[][]{
		    		{0.75, 0.25}
				});
		
		Action extectedSelectedAction = environment.getActions()[1];
		testSelectAction(policy, extectedSelectedAction);
		
	}
	
	public void testSelectActionStochasticBigSoft() {	
		TabularPolicy policy = newTabularPolicy(environment, Policy.Type.Stochastic, 0.2,
		    	new Action[][]{ 
					{environment.getActions()[0], environment.getActions()[1], environment.getActions()[3]}
				},
		    	new double[][]{
		    		{0.15, 0.15, 0.70}
				});
		
		Action extectedSelectedAction = environment.getActions()[3];
		testSelectAction(policy, extectedSelectedAction);
		
	}
	
	public void testSelectAction(TabularPolicy policy, Action extectedSelectedAction) {
		Action actualSelectedAction =  policy.selectAction(environment.getStates()[0]);

		System.out.println("NDB::testSelectAction()~extectedSelectedAction="+extectedSelectedAction);
    	System.out.println("NDB::testSelectAction()~actualSelectedAction="+actualSelectedAction);
		assertTrue(MDPTestUtils.equals(extectedSelectedAction, actualSelectedAction));

	}
	
	public static TabularPolicy newTabularPolicy(TabularFiniteMDP environment, Type type, double e, 
			Action[][] stateActions, double[][] stateActionProbabilities) {
		return new TabularPolicy(environment, type, e, stateActions, stateActionProbabilities);
		
	}
	
	public static boolean equals(TabularPolicy policyA, TabularPolicy policyB) {
		boolean equals = (policyA.type == policyB.type) && 
				(policyA.stateActions.length == policyB.stateActions.length);
		
		if (policyA.type == Type.Stochastic) {
			equals &= (policyA.stateActionProbabilities.length == policyB.stateActionProbabilities.length);
		}
		int sa = 0;
		while (equals && sa < policyA.stateActions.length) {
			equals = (policyA.stateActions[sa].length == policyB.stateActions[sa].length);
			int a = 0; 
			while (equals && a < policyA.stateActions[sa].length) {
				equals = (MDPTestUtils.equals(policyA.stateActions[sa][a], policyB.stateActions[sa][a]));
				a++;
			}
			sa++;
		}
		if (policyA.type == Type.Stochastic) {
			int sap = 0;
			while (equals && sap < policyA.stateActionProbabilities.length) {
				equals = (policyA.stateActionProbabilities[sap].length == policyB.stateActionProbabilities[sap].length);
				int p = 0; 
				while (equals && p < policyA.stateActionProbabilities[sap].length) {
					equals = (policyA.stateActionProbabilities[sap][p] == policyB.stateActionProbabilities[sap][p]);
					p++;
				}
				sap++;
			}
		}
		return equals;
	}
	
	public static boolean equals(TabularActionValueFunction actionValueFunctionA,
			TabularActionValueFunction actionValueFunctionB) {
		
		boolean equals = actionValueFunctionA.values.length == actionValueFunctionB.values.length;
		
		int ss = 0;
		while (equals && ss < actionValueFunctionA.values.length) {
			equals = actionValueFunctionA.values[ss].length == actionValueFunctionB.values[ss].length;
			int s = 0;
			while (equals && s < actionValueFunctionA.values[ss].length) {
				equals = (actionValueFunctionA.values[ss][s] == actionValueFunctionB.values[ss][s]);
				s++;
			}
			ss++;
		}
		return equals;
		
	}

	public static String toString(TabularPolicy policy) {
		StringBuffer string = new StringBuffer("PolicyTest.newTabularPolicy(environment, Policy.Type."+policy.type+", "+policy.e+",\n");
		string.append("\tnew Action[][]{\n");
		for (Action[] actions : policy.stateActions) {
			if (actions.length > 0) {
				string.append("\t\t{");
				for (Action action : actions) {
					if (action != null) {
						string.append("MDPTest.newAction("+action+"), ");
						
					} else {
						string.append("null, ");
						
					}
				}
				string.replace(string.length()-2,string.length(),"},\n");
			} else {
				string.append("\t\t{},\n");
			}
		}
		string.replace(string.length()-2,string.length(),"},\n");
		if (policy.type == Type.Stochastic) {
			string.append("\tnew double[][]{\n");
			for (double[] probabilities : policy.stateActionProbabilities) {
				if (probabilities.length > 0) {
					string.append("\t\t{");
					for (double probability : probabilities) {
						string.append(probability+", ");
					}
					string.replace(string.length()-2,string.length(),"},\n");
				} else {
					string.append("\t\t{},\n");
				}
			}
			string.replace(string.length()-2,string.length(),"});\n");
		} else {
			string.append("\tnull);\n");
		}
		return string.toString();
	}
	
	public static String toString(TabularActionValueFunction actionValueFunction) {
		StringBuffer string = new StringBuffer("new double[][]{\n");
		for (double[] state : actionValueFunction.values) {
			string.append("\t{");
			for (double value : state) {
				string.append(value+", ");
			}
			string.replace(string.length()-2,string.length(),"},\n");
		}
		string.replace(string.length()-2,string.length(),"},\n");
		return string.toString();
	}
	
	public static String toString(TabularValueFunction valueFunction) {
		StringBuffer string = new StringBuffer("values={\n");
		for (double value : valueFunction.values) {
				string.append(value+", ");
		}
		string.replace(string.length()-2,string.length(),"};\n");
		return string.toString();
	}
	
}
