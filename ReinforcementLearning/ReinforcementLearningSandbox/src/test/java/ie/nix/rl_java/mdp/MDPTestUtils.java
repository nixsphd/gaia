package ie.nix.rl_java.mdp;

/**
 * Unit test for simple App.
 */
public class MDPTestUtils {

	public static Action newAction(int id) {
		return new Action(id);
		
	}
	
	public static State newState(int id) {
		return new State(id);
		
	}
	
	public static boolean equals(Action actionA, Action actionB) {
		return (actionA == null && actionB == null) || 
				(actionA.getId() == actionB.getId());
		
	}
	
	public static boolean equals(State stateA, State stateB) {
		return (stateA.getId() == stateB.getId());
		
	}
	
	public static String toString(Action action) {
		StringBuffer string = new StringBuffer("MDPTest.newAction("+action.getId()+");");
		return string.toString();
	}

	public static String toString(Action[] actions) {
		StringBuffer string = new StringBuffer("new Action[]{\n");
		for (Action action : actions) {
			string.append("\tMDPTest.newAction("+action.getId()+")\n");
		}
		string.append("}");
		return string.toString();
	}
	
	public static String toString(State state) {
		StringBuffer string = new StringBuffer("MDPTest.newState("+state.getId()+");");
		return string.toString();
	}

	public static String toString(State[] states) {
		StringBuffer string = new StringBuffer("new State[]{\n");
		for (State state : states) {
			string.append("\tMDPTest.newState("+state.getId()+"),\n");
		}
		string.append("}");
		return string.toString();
	}
	
}
