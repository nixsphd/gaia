package ie.nix.rl_java.mc;

import ie.nix.rl_java.policy.PolicyTest;

public class SampleAverageActionValueFunctionTest {

	public static SampleAverageActionValueFunction newSampleAverageActionValueFunction(double[][] values, int[][] numberOfSamples) {
		return new SampleAverageActionValueFunction(values, numberOfSamples);
	}

	public static boolean equals(SampleAverageActionValueFunction sampleAverageActionValueFunctionA, 
			SampleAverageActionValueFunction sampleAverageActionValueFunctionB) {
		
		boolean equals = PolicyTest
            .equals(sampleAverageActionValueFunctionA,sampleAverageActionValueFunctionB);
		
		equals = sampleAverageActionValueFunctionA.numberOfSamples.length == sampleAverageActionValueFunctionB.numberOfSamples.length;
		
		int ss = 0;
		while (equals && ss < sampleAverageActionValueFunctionA.numberOfSamples.length) {
			equals = sampleAverageActionValueFunctionA.numberOfSamples[ss].length == sampleAverageActionValueFunctionB.numberOfSamples[ss].length;
			int s = 0;
			while (equals && s < sampleAverageActionValueFunctionA.numberOfSamples[ss].length) {
				equals = (sampleAverageActionValueFunctionA.numberOfSamples[ss][s] == sampleAverageActionValueFunctionB.numberOfSamples[ss][s]);
				s++;
			}
			ss++;
		}
		return equals;
	}
	
	public static String toString(SampleAverageActionValueFunction actionValueFunction) {
		StringBuffer string = new StringBuffer();
		string.append("SampleAverageActionValueFunctionTest.newSampleAverageActionValueFunction(\n");
		string.append(PolicyTest.toString(actionValueFunction));
		string.append("new int[][]{\n");
		for (int[] numberOfSamples : actionValueFunction.numberOfSamples) {
			string.append("\t{");
			for (int nos : numberOfSamples) {
				string.append(Integer.toString(nos)+", ");
			}
			string.replace(string.length()-2,string.length(),"},\n");
		}
		string.replace(string.length()-2,string.length(),"});\n");
		return string.toString();
		
	}
	
//	public static String toString(TabularValueFunction valueFunction) {
//		StringBuffer string = new StringBuffer("values={\n");
//		for (double value : valueFunction.values) {
//				string.append(value+", ");
//		}
//		string.replace(string.length()-2,string.length(),"};\n");
//		return string.toString();
//	}
}
