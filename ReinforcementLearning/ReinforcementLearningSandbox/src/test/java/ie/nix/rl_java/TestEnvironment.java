package ie.nix.rl_java;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.RewardFunction;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateActionFunction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.mdp.TransitionFunction;

public class TestEnvironment extends TabularFiniteMDP {

	public TestEnvironment(int numberOfStates, int numberOfActions) {
		
		super(numberOfStates, numberOfActions);
	
		// Initialise the states
		for (int s = 0; s < numberOfStates - 1; s++) {
			newState();
		}
		newState(State.Type.Terminal);
	
		// Initialise the actions
		for (int a = 0; a < numberOfActions; a++) {
			newAction();
		}
		
		// Initialise the state action function
		StateActionFunction stateActionFunction = new StateActionFunction() {
			public Action[] getActions(State forState) {
				return TestEnvironment.this.getActions();
			}
		};
		setStateActionFunction(stateActionFunction);
		
		// Initialise the state transition function
		TransitionFunction stf = new TransitionFunction() {
			public double probabilityOf(State movingToState, State givenState, Action givenAction) {
				if (movingToState.getId() == givenState.getId() + givenAction.getId()) {
					return 1;
	
				} else {
					return 0;
					
				}
			}
	
			public State getNextState(State givenState, Action givenAction) {
				State maxState = getStates()[getStates().length-1];
				int sumState = givenState.getId() + givenAction.getId();
				if (sumState > maxState.getId()) {
					return maxState;
					
				} else {
					return getStates()[givenState.getId() + givenAction.getId()];
					
				}
			}
	
		};
		setStateTransitionFunction(stf);
		
		// Initialise the Reward function
		RewardFunction rewardFunction = new RewardFunction() {
			
			public double getReward(State initialState, Action actionTaken, State finalState) {

//			 	System.out.println("NDB::RewardFunction.getReward()~initialState="+initialState+", actionTaken="+actionTaken+", finalState="+finalState);
				State maxState = getStates()[getStates().length-1];
				if (finalState.getId() == maxState.getId()) {
					// If we overshot we'd be in the final state but should not get a reward.
					int sumState = initialState.getId() + actionTaken.getId();
					if (sumState > maxState.getId()) {
						return 0;
						
					} else {
						return 1;
						
					}
					
				} else {
					return 0;
					
				}
			}
	
		};
		setRewardFunction(rewardFunction);
	}
	
}
