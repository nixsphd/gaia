package ie.nix.rl_java.mc;

import ie.nix.rl_java.Episode;
import ie.nix.rl_java.TestEnvironment;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.MDPTestUtils;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.PolicyTest;
import ie.nix.rl_java.policy.TabularPolicy;
import ie.nix.util_java.Randomness;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class PolicyImproverTest extends TestCase {
	
	TabularFiniteMDP environment;

    @Override
	protected void setUp() throws Exception {

    	Randomness.setSeed(0);

    	int numberOfStates = 10;
    	int numberOfActions = 10;
    	
    	environment = new TestEnvironment(numberOfStates, numberOfActions);
	}

    public static Test suite()
    {
        return new TestSuite( PolicyImproverTest.class );
    }
    
    public void testImproveOneEpisodes() {
    	TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
	    	new Action[][]{
	    			{MDPTestUtils.newAction(0)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{null,null,null,null,null,null,null,null,null,null}},
	    		new double[][]{
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
    	 
		State initialState = environment.getStates()[0];
		Action initialAction = environment.getActions()[1];
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
				new Action[][]{
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{null,null,null,null,null,null,null,null,null,null}},
			new double[][]{
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
				
		testImproveOneEpisodes(policy, initialState, initialAction, extectedImprovedPolicy);
	}
    
    public void testImproveOneEpisodesNoImprovment() {
    	TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
	    	new Action[][]{
	    			{MDPTestUtils.newAction(0)},
	    			{MDPTestUtils.newAction(0)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{null,null,null,null,null,null,null,null,null,null}},
	    		new double[][]{
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
    	 
		State initialState = environment.getStates()[0];
		Action initialAction = environment.getActions()[1];
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
		    	new Action[][]{
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)},
    			{null,null,null,null,null,null,null,null,null,null}},
    		new double[][]{
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
				
		testImproveOneEpisodes(policy, initialState, initialAction, extectedImprovedPolicy);
	}
    
    public void testImproveOneEpisodesDeadEnd() {
    	TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
    			new Action[][]{
				{MDPTestUtils.newAction(0), MDPTestUtils.newAction(1), MDPTestUtils.newAction(2),
						MDPTestUtils.newAction(3), MDPTestUtils.newAction(4), MDPTestUtils.newAction(5),
						MDPTestUtils.newAction(6), MDPTestUtils.newAction(7), MDPTestUtils.newAction(8),
						MDPTestUtils.newAction(9)},
				{MDPTestUtils.newAction(0)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{null,null,null,null,null,null,null,null,null,null}},
			new double[][]{
				{0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
    	
		State initialState = environment.getStates()[0];
		Action initialAction = environment.getActions()[2];
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
				new Action[][]{
				{MDPTestUtils.newAction(2)},
				{MDPTestUtils.newAction(0)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{null,null,null,null,null,null,null,null,null,null}},
			new double[][]{
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
				
		testImproveOneEpisodes(policy, initialState, initialAction, extectedImprovedPolicy);
	}

    public void testImproveOneEpisodesTwoNew() {
    	TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
	    	new Action[][]{
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{null,null,null,null,null,null,null,null,null,null}},
	    		new double[][]{
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
    	 
		State initialState = environment.getStates()[0];
		Action initialAction = environment.getActions()[2];
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
			new Action[][]{
				{MDPTestUtils.newAction(2)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{null,null,null,null,null,null,null,null,null,null}},
			new double[][]{
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
				
		testImproveOneEpisodes(policy, initialState, initialAction, extectedImprovedPolicy);
	}
    
    public void testImproveOneEpisodes(TabularPolicy policy, State initialState, Action initialAction, TabularPolicy extectedImprovedPolicy) {
    	Episode episode = new Episode(environment, initialState, initialAction);
    	
		SampleAverageActionValueFunction actionValueFunction = new SampleAverageActionValueFunction(environment.getNumberOfStates(), environment.getNumberOfActions());
		
		double theta = 0.0000001;
		double discount = 1.0;

		PolicyEvaluator policyEvaluator = new PolicyEvaluator(theta, discount);
		ArrayList<StateAction> actualVistedStateActions = new ArrayList<StateAction>();
		double delta = policyEvaluator.evaluate(policy, actionValueFunction, episode, actualVistedStateActions);
		System.out.println("NDB::testEavluateOneEpisodes()~actualVistedStateActions=\n"+toString(actualVistedStateActions));
		
		PolicyImprover policyImprover = new PolicyImprover(environment, theta, discount);
		policyImprover.improve(policy, actionValueFunction, actualVistedStateActions);
		
    	System.out.println("NDB::testImproveOneEpisodes()~actualImprovedPolicy=\n"+PolicyTest.toString(policy));
    	System.out.println("NDB::testImproveOneEpisodes()~extectedImprovedPolicy=\n"+PolicyTest.toString(extectedImprovedPolicy));

        assertTrue(PolicyTest.equals(extectedImprovedPolicy, policy));
    }
    
    public void testImproveMultipleEpisodes() {
    	TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
    	    	new Action[][]{
    	    			{MDPTestUtils.newAction(0)},
    	    			{MDPTestUtils.newAction(0)},
    	    			{MDPTestUtils.newAction(1)},
    	    			{MDPTestUtils.newAction(1)},
    	    			{MDPTestUtils.newAction(1)},
    	    			{MDPTestUtils.newAction(1)},
    	    			{MDPTestUtils.newAction(1)},
    	    			{MDPTestUtils.newAction(1)},
    	    			{MDPTestUtils.newAction(1)},
    	    			{null,null,null,null,null,null,null,null,null,null}},
    	    		new double[][]{
    	    			{1.0},
    	    			{1.0},
    	    			{1.0},
    	    			{1.0},
    	    			{1.0},
    	    			{1.0},
    	    			{1.0},
    	    			{1.0},
    	    			{1.0},
    	    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
    	
		State[] initialStates = {
				environment.getStates()[1],
				environment.getStates()[0],
		};
		Action[] initialActions = {
				environment.getActions()[1],
				environment.getActions()[2],
		};
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
				new Action[][]{
				{MDPTestUtils.newAction(2)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{null,null,null,null,null,null,null,null,null,null}},
			new double[][]{
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
		testImproveMultipleEpisodes(policy, initialStates, initialActions, extectedImprovedPolicy);
	}
    
    public void testImproveMultiEpisodesEquiProbable() {
		TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Equiprobable, 0.0,
			new Action[][]{
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(0)},
				{MDPTestUtils.newAction(1)},
				{null,null,null,null,null,null,null,null,null,null}},
			new double[][]{
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
		
		State[] initialStates = {
				environment.getStates()[7],
				environment.getStates()[7],
				environment.getStates()[7],
				environment.getStates()[7],
				environment.getStates()[7],
				environment.getStates()[7],
				environment.getStates()[7],
				environment.getStates()[7],
				environment.getStates()[7],
		};
		Action[] initialActions = {
				environment.getActions()[1],
				environment.getActions()[2],
				environment.getActions()[3],
				environment.getActions()[4],
				environment.getActions()[5],
				environment.getActions()[6],
				environment.getActions()[7],
				environment.getActions()[8],
				environment.getActions()[9],
		};
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Equiprobable, 0.0,
			new Action[][]{
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2)},
				{MDPTestUtils.newAction(1)},
				{null, null, null, null, null, null, null, null, null, null}},
			null);

		testImproveMultipleEpisodes(policy, initialStates, initialActions, extectedImprovedPolicy);
	}

	public void testImproveMultipleEpisodes(TabularPolicy policy, State[] initialStates, Action[] initialActions, TabularPolicy extectedImprovedPolicy) {
        int numberOfEpisodes = initialStates.length;
    	Episode[] episodes = new Episode[numberOfEpisodes];
    	for (int i = 0; i < numberOfEpisodes; i++) {
    		episodes[i] = new Episode(environment, initialStates[i], initialActions[i]);
    	}
    	
		SampleAverageActionValueFunction actionValueFunction = new SampleAverageActionValueFunction(environment.getNumberOfStates(), environment.getNumberOfActions());
		
		double theta = 0.0000001;
		double discount = 1.0;
		PolicyEvaluator policyEvaluator = new PolicyEvaluator(theta, discount);
		ArrayList<StateAction> actualVistedStateActions = new ArrayList<StateAction>();
		double delta = policyEvaluator.evaluate(policy, actionValueFunction, episodes, actualVistedStateActions);
		System.out.println("NDB::testEavluateOneEpisodes()~actualVistedStateActions=\n"+toString(actualVistedStateActions));
		
		PolicyImprover policyImprover = new PolicyImprover(environment, theta, discount);
    	
		policyImprover.improve(policy, actionValueFunction, actualVistedStateActions);
    	System.out.println("NDB::testImproveOneEpisodes()~actualImprovedPolicy=\n"+PolicyTest.toString(policy));
    	System.out.println("NDB::testImproveOneEpisodes()~extectedImprovedPolicy=\n"+PolicyTest.toString(extectedImprovedPolicy));

        assertTrue(PolicyTest.equals(extectedImprovedPolicy, policy));
        
    }
	
	public String toString(ArrayList<StateAction> actualVistedStateActions) {
		StringBuffer string = new StringBuffer("new StateAction[]{\n");
		for (StateAction stateAction : actualVistedStateActions) {
			string.append("\tnew StateAction("
					+ "MDPTest.newState("+stateAction.getState().getId()+"),"
					+" MDPTest.newAction("+stateAction.getAction().getId()+")),\n");
		}
		string.append("}");
		return string.toString();
	}

	public boolean equals(StateAction[] extectedVistedStateActions, StateAction[] actualVistedStateActions) {
		boolean equals = (extectedVistedStateActions.length == actualVistedStateActions.length);

		int numberOfVistedStateActions = extectedVistedStateActions.length-1;
		while (equals && numberOfVistedStateActions >= 0) {
			equals = ((extectedVistedStateActions[numberOfVistedStateActions].getState().getId() == 
					   actualVistedStateActions[numberOfVistedStateActions].getState().getId()) &&
					  (extectedVistedStateActions[numberOfVistedStateActions].getAction().getId() == 
					   actualVistedStateActions[numberOfVistedStateActions].getAction().getId()));
			numberOfVistedStateActions--;
			
		}
		return equals;
	}
	
	
}
