package ie.nix.rl_java;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.MDPTestUtils;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.PolicyTest;
import ie.nix.rl_java.policy.TabularPolicy;
import ie.nix.util_java.Randomness;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class EpisodeTest extends TestCase {
	
	TabularFiniteMDP environment;

    @Override
	protected void setUp() throws Exception {

    	Randomness.setSeed(0);

    	int numberOfStates = 10;
    	int numberOfActions = 10;
    	
    	environment = new TestEnvironment(numberOfStates, numberOfActions);
	}

	/**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( EpisodeTest.class );
    }

    public void testRunRecordStateActionsReward() {
    	TabularPolicy policy = new TabularPolicy(environment, Policy.Type.Equiprobable);
    	for (State state : environment.getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				policy.updateActions(state, new Action[]{environment.getActions()[1]});
			} else {
				policy.updateActions(state, new Action[]{environment.getActions()[0]});
			}
		}
		State initialState = environment.getStates()[1];
		Action initialAction = environment.getActions()[1];
		double expectedReward = 1.0;
		StateAction[] extectedVistedStateActions = new StateAction[]{
				new StateAction(MDPTestUtils.newState(1), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(2), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(3), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(4), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(5), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(6), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(7), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(8), MDPTestUtils.newAction(1)),
		};
	   
		testRunRecordStateActionsOneEpisode(policy, initialState, initialAction, expectedReward, extectedVistedStateActions);
	}

	public void testRunRecordStateActionsNoReward() {
		TabularPolicy policy = new TabularPolicy(environment, Policy.Type.Equiprobable);
    	for (State state : environment.getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				policy.updateAction(state, environment.getActions()[2]);
			}
		}
    	State initialState = environment.getStates()[1];
    	Action initialAction = environment.getActions()[1];
    	double expectedReward = 0.0;
    	StateAction[] extectedVistedStateActions = new StateAction[]{
    			new StateAction(MDPTestUtils.newState(1), MDPTestUtils.newAction(1)),
    			new StateAction(MDPTestUtils.newState(2), MDPTestUtils.newAction(2)),
    			new StateAction(MDPTestUtils.newState(4), MDPTestUtils.newAction(2)),
    			new StateAction(MDPTestUtils.newState(6), MDPTestUtils.newAction(2)),
    			new StateAction(MDPTestUtils.newState(8), MDPTestUtils.newAction(2)),
    	};
    	testRunRecordStateActionsOneEpisode(policy, initialState, initialAction, expectedReward, extectedVistedStateActions);
    }
    
    public void testRunRecordStateActionsOneEpisode(TabularPolicy policy, State initialState, Action initialAction, double expectedReward, StateAction[] extectedVistedStateActions) {

    	System.out.println("NDB::testRunRecordStateActions()~policy="+ PolicyTest.toString(policy));
    	
    	Episode episode = new Episode(environment, initialState, initialAction);
    	
    	ArrayList<StateAction> actualVistedStateActions = new ArrayList<StateAction>();
    	double actualReward = episode.runRecordStateActions(policy, actualVistedStateActions);
    	System.out.println("NDB::testRunRecordStateActions()~actualReward="+actualReward);
    	System.out.println("NDB::testRunRecordStateActions()~actualVistedStateActions=\n"+
    			toString(actualVistedStateActions.toArray(new StateAction[actualVistedStateActions.size()])));
    	
        assertEquals(expectedReward, actualReward, 0.0);
        assertTrue(equals(extectedVistedStateActions, 
        		actualVistedStateActions.toArray(new StateAction[actualVistedStateActions.size()])));
    }

	public String toString(StateAction[] actualVistedStateActions) {
		StringBuffer string = new StringBuffer("new StateAction[]{\n");
		for (StateAction stateAction : actualVistedStateActions) {
			string.append("\tnew StateAction("
					+ "MDPTest.newState("+stateAction.getState().getId()+"),"
					+ "MDPTest.newAction("+stateAction.getAction().getId()+")),\n");
		}
		string.append("}");
		return string.toString();
	}

	public boolean equals(StateAction[] extectedVistedStateActions, StateAction[] actualVistedStateActions) {
		boolean equals = (extectedVistedStateActions.length == actualVistedStateActions .length);

		int numberOfVistedStateActions = extectedVistedStateActions.length-1;
		while (equals && numberOfVistedStateActions >= 0) {
			equals = ((extectedVistedStateActions[numberOfVistedStateActions].getState().getId() == 
					   actualVistedStateActions[numberOfVistedStateActions].getState().getId()) &&
					  (extectedVistedStateActions[numberOfVistedStateActions].getAction().getId() == 
					   actualVistedStateActions[numberOfVistedStateActions].getAction().getId()));
			numberOfVistedStateActions--;
			
		}
		return equals;
	}
	
	
}
