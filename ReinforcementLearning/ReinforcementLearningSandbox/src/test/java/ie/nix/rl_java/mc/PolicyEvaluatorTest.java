package ie.nix.rl_java.mc;

import ie.nix.rl_java.Episode;
import ie.nix.rl_java.TestEnvironment;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.MDPTestUtils;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.PolicyTest;
import ie.nix.rl_java.policy.TabularPolicy;
import ie.nix.util_java.Randomness;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class PolicyEvaluatorTest extends TestCase {
	
	TabularFiniteMDP environment;

    @Override
	protected void setUp() throws Exception {
    	Randomness.setSeed(0);

    	int numberOfStates = 10;
    	int numberOfActions = 10;
    	
    	environment = new TestEnvironment(numberOfStates, numberOfActions);
	
	}

	/**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( PolicyEvaluatorTest.class );
    }
    
    public void testEavluateOneEpisodes() {
    	TabularPolicy policy = new TabularPolicy(environment, Policy.Type.Equiprobable);
    	for (State state : environment.getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				policy.updateAction(state, environment.getActions()[1]);
			}
		}
		State initialState = environment.getStates()[1];
		Action initialAction = environment.getActions()[1];
		StateAction[] extectedVistedStateActions = new StateAction[]{
				new StateAction(MDPTestUtils.newState(1), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(2), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(3), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(4), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(5), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(6), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(7), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(8), MDPTestUtils.newAction(1)),
		};
	   
		testEavluateOneEpisodes(policy, initialState, initialAction, extectedVistedStateActions);
	}
    
    public void testEavluateOneEpisodes(TabularPolicy policy, State initialState, Action initialAction, StateAction[] extectedVistedStateActions) {
    	Episode episode = new Episode(environment, initialState, initialAction);
    	
		SampleAverageActionValueFunction actionValueFunction = new SampleAverageActionValueFunction(environment.getNumberOfStates(), environment.getNumberOfActions());
		
		double theta = 0.0000001;
		double discount = 1.0;
		PolicyEvaluator policyEvaluator = new PolicyEvaluator(theta, discount);
		
		ArrayList<StateAction> actualVistedStateActions = new ArrayList<StateAction>();
		double delta = policyEvaluator.evaluate(policy, actionValueFunction, episode, actualVistedStateActions);
		
    	System.out.println("NDB::testEavluateOneEpisodes()~actualVistedStateActions=\n"+
    			toString(actualVistedStateActions.toArray(new StateAction[actualVistedStateActions.size()])));
		System.out.println("NDB::testEavluateMultipleEpisodes()~actionValueFunction=\n"+
				SampleAverageActionValueFunctionTest.toString(actionValueFunction));
    	System.out.println("NDB::testEavluateOneEpisodes()~actualVistedStateActions=\n"+
    	    	PolicyTest.toString(actionValueFunction));

        assertTrue(equals(extectedVistedStateActions, actualVistedStateActions.toArray(new StateAction[actualVistedStateActions.size()])));
    }
    
    public void testEavluateMultipleEpisodes() {
    	TabularPolicy policy = new TabularPolicy(environment, Policy.Type.Equiprobable);
    	for (State state : environment.getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				policy.updateActions(state, new Action[]{environment.getActions()[2]});
			}
		}
		State[] initialStates = {
				environment.getStates()[0],
				environment.getStates()[1],
		};
		Action[] initialActions = {
				environment.getActions()[1],
				environment.getActions()[3],
		};
		StateAction[] extectedVistedStateActions = new StateAction[]{	
				new StateAction(MDPTestUtils.newState(0), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(1), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(3), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(5), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(7), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(1), MDPTestUtils.newAction(3)),
				new StateAction(MDPTestUtils.newState(4), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(6), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(8), MDPTestUtils.newAction(2)),
		};
	   
		testEavluateMultipleEpisodes(policy, initialStates, initialActions, extectedVistedStateActions);
	}
    
    public void testEavluateMultipleEpisodes(TabularPolicy policy, State[] initialStates, Action[] initialActions, StateAction[] extectedVistedStateActions) {
        int numberOfEpisodes = initialStates.length;
    	Episode[] episodes = new Episode[numberOfEpisodes];
    	for (int i = 0; i < numberOfEpisodes; i++) {
    		episodes[i] = new Episode(environment, initialStates[i], initialActions[i]);
    	}
    	
		SampleAverageActionValueFunction actionValueFunction = new SampleAverageActionValueFunction(environment.getNumberOfStates(), environment.getNumberOfActions());
		
		double theta = 0.0000001;
		double discount = 1.0;
		PolicyEvaluator policyEvaluator = new PolicyEvaluator(theta, discount);

		ArrayList<StateAction> actualVistedStateActions = new ArrayList<StateAction>();
		double delta = policyEvaluator.evaluate(policy, actionValueFunction, episodes, actualVistedStateActions);
		System.out.println("NDB::testEavluateMultipleEpisodes()~actualVistedStateActions=\n"+
				toString(actualVistedStateActions.toArray(new StateAction[actualVistedStateActions.size()])));
		System.out.println("NDB::testEavluateMultipleEpisodes()~actionValueFunction=\n"+
				SampleAverageActionValueFunctionTest.toString(actionValueFunction));
		System.out.println("NDB::testEavluateMultipleEpisodes()~extectedVistedStateActions=\n"+toString(extectedVistedStateActions));
    	
        assertTrue(equals(extectedVistedStateActions, actualVistedStateActions.toArray(new StateAction[actualVistedStateActions.size()])));
    }
    
    public void testEavluateEpisodesWinLoose() {
		double theta = 0.0000001;
		double discount = 1.0;
    	TabularPolicy policy = new TabularPolicy(environment, Policy.Type.Equiprobable);
    	for (State state : environment.getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				policy.updateAction(state, environment.getActions()[2]);
			}
    	}
		SampleAverageActionValueFunction actualActionValueFunction = 
				new SampleAverageActionValueFunction(environment.getNumberOfStates(), environment.getNumberOfActions());
		
		PolicyEvaluator policyEvaluator = new PolicyEvaluator(theta, discount);

		ArrayList<StateAction> actualVistedStateActions = new ArrayList<StateAction>();
		
		SampleAverageActionValueFunction expectedActionValueFunction = 
				SampleAverageActionValueFunctionTest.newSampleAverageActionValueFunction(
						new double[][]{
							{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
							{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}},
						new int[][]{
							{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
							{0, 2, 0, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 2, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 2, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 2, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
							{0, 1, 1, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 0, 0, 0, 0, 0, 0, 0, 0}});
		StateAction[] extectedVistedStateActions = new StateAction[]{
				new StateAction(MDPTestUtils.newState(1), MDPTestUtils.newAction(1)),
				new StateAction(MDPTestUtils.newState(2), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(4), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(6), MDPTestUtils.newAction(2)),
				new StateAction(MDPTestUtils.newState(8), MDPTestUtils.newAction(1))};
	   
		Episode episode = new Episode(environment, environment.getStates()[1], environment.getActions()[1]);
		policyEvaluator.evaluate(policy, actualActionValueFunction, episode, actualVistedStateActions);
		
		System.out.println("NDB::testEavluateMultipleEpisodes()~actualActionValueFunction=\n"+
				SampleAverageActionValueFunctionTest.toString(actualActionValueFunction));

		policy.updateAction(environment.getStates()[8], environment.getActions()[1]);
		
		//Ensure it's empty to start with. Should the user do this??
		actualVistedStateActions.clear();
		policyEvaluator.evaluate(policy, actualActionValueFunction, episode, actualVistedStateActions);
	
		System.out.println("NDB::testEavluateMultipleEpisodes()~actualActionValueFunction=\n"+
				SampleAverageActionValueFunctionTest.toString(actualActionValueFunction));
		System.out.println("NDB::testEavluateMultipleEpisodes()~actualVistedStateActions=\n"+
				toString(actualVistedStateActions.toArray(new StateAction[actualVistedStateActions.size()])));
		System.out.println("NDB::testEavluateMultipleEpisodes()~extectedVistedStateActions=\n"+toString(extectedVistedStateActions));

		assertTrue(SampleAverageActionValueFunctionTest.equals(expectedActionValueFunction, actualActionValueFunction));

        assertTrue(equals(extectedVistedStateActions, actualVistedStateActions.toArray(new StateAction[actualVistedStateActions.size()])));
    }
		
	public String toString(StateAction[] vistedStateActions) {
		StringBuffer string = new StringBuffer("new StateAction[]{\n");
		for (StateAction stateAction : vistedStateActions) {
			string.append("\tnew StateAction("
					+ "MDPTest.newState("+stateAction.getState().getId()+"),"
					+" MDPTest.newAction("+stateAction.getAction().getId()+")),\n");
		}
		string.append("}");
		return string.toString();
	}
	
	public boolean equals(StateAction[] extectedVistedStateActions, StateAction[] actualVistedStateActions) {
		boolean equals = (extectedVistedStateActions.length == actualVistedStateActions.length);

		int numberOfVistedStateActions = extectedVistedStateActions.length-1;
		while (equals && numberOfVistedStateActions >= 0) {
			equals = ((extectedVistedStateActions[numberOfVistedStateActions].getState().getId() == 
					   actualVistedStateActions[numberOfVistedStateActions].getState().getId()) &&
					  (extectedVistedStateActions[numberOfVistedStateActions].getAction().getId() == 
					   actualVistedStateActions[numberOfVistedStateActions].getAction().getId()));
			numberOfVistedStateActions--;
			
		}
		return equals;
	}
	
	
}
