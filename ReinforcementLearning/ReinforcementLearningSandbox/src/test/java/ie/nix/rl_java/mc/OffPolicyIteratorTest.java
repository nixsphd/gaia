package ie.nix.rl_java.mc;

import ie.nix.rl_java.TestEnvironment;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.MDPTestUtils;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.PolicyTest;
import ie.nix.rl_java.policy.TabularPolicy;
import ie.nix.util_java.Randomness;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class OffPolicyIteratorTest extends TestCase {
	
	TabularFiniteMDP environment;
//	Agent agent;

    @Override
	protected void setUp() throws Exception {

    	Randomness.setSeed(0);

    	int numberOfStates = 10;
    	int numberOfActions = 2;
    	
    	environment = new TestEnvironment(numberOfStates, numberOfActions);
    	
	}

    public static Test suite() {
        return new TestSuite( OffPolicyIteratorTest.class );
    }
    
//    public void testOffPolicyIteratorOneEpisode() {
//
//    	Randomness.setSeed(2);
//    	
//    	TabularPolicy behaviourPolicy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Deterministic, 0.0,
//	    	new Action[][]{
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{null,null,null,null,null,null,null,null,null,null}},
//    		new double[][]{
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
//    	TabularPolicy estimationPolicy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Deterministic, 0.5,
//	    	new Action[][]{
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{MDPTest.newAction(1)},
//    			{null,null,null,null,null,null,null,null,null,null}},
//    		new double[][]{
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{1.0},
//    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
//    	 
//		State[] initialStates = {
//				environment.getStates()[5],
//		};
//		Action[] initialActions = {
//				environment.getActions()[1],
//		};
//		TabularPolicy extectedImprovedBehaviourPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Deterministic, 0.0,
//			new Action[][]{
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{null, null, null, null, null, null, null, null, null, null}},
//			new double[][]{
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}});
//				
//		testOffPolicyIterator(behaviourPolicy, estimationPolicy, initialStates, initialActions, extectedImprovedBehaviourPolicy);
//	}
    
    public void testOffPolicyIteratorEvaluation() {
    	TabularPolicy behaviourPolicy =  PolicyTest
            .newTabularPolicy(environment, Policy.Type.Deterministic, 0.0,
	    	new Action[][]{
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)}},
    		new double[][]{
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0}});
    	TabularPolicy estimationPolicy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Deterministic, 0.1,
	    	new Action[][]{
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(0)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)},
    			{MDPTestUtils.newAction(1)}},
    		new double[][]{
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0},
    			{1.0}});
    	 
    	ArrayList<StateAction> visitedStateActions = new ArrayList<StateAction>();
    	visitedStateActions.add(new StateAction(environment.getStates()[5], environment.getActions()[1]));
    	visitedStateActions.add(new StateAction(environment.getStates()[6], environment.getActions()[1]));
    	visitedStateActions.add(new StateAction(environment.getStates()[7], environment.getActions()[1]));
    	visitedStateActions.add(new StateAction(environment.getStates()[8], environment.getActions()[1]));
    	visitedStateActions.add(new StateAction(environment.getStates()[9], environment.getActions()[1]));
//		TabularPolicy extectedImprovedBehaviourPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Deterministic, 0.0,
//			new Action[][]{
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{MDPTest.newAction(1)},
//				{null, null, null, null, null, null, null, null, null, null}},
//			new double[][]{
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{1.0},
//				{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}});
				
		testOffPolicyIteratorEvaluation(behaviourPolicy, estimationPolicy, visitedStateActions); //, extectedImprovedBehaviourPolicy);
	}
    
    public void testOffPolicyIteratorEvaluation(TabularPolicy behaviourPolicy, TabularPolicy estimationPolicy, 
    		ArrayList<StateAction> visitedStateActions) { //, TabularPolicy extectedImprovedBehaviourPolicy) {
    	
		double theta = 0.0000001;
		double discount = 1.0;
		
		OffPolicyIterator opi = new OffPolicyIterator(environment, theta, discount);
		
		WeightedAverageActionValueFunction avf = new WeightedAverageActionValueFunction(
				environment.getNumberOfStates(), environment.getNumberOfActions());

//		int numberOfEpisodes = initialStates.length;
//    	Episode[] episodes = new Episode[numberOfEpisodes];
//    	for (int i = 0; i < numberOfEpisodes; i++) {
//    		episodes[i] = new Episode(environment, initialStates[i], initialActions[i]);
////        	System.out.println("NDB::testGeneralisedPolicyIterator()~episodes["+i+"]=state"+initialStates[i]+", action="+initialActions[i]);
//    	}

    	opi.evaluate(behaviourPolicy, estimationPolicy, avf, 1.0, visitedStateActions);
//    	opi.iterate(behaviourPolicy, estimationPolicy, avf, episodes);
		
//    	System.out.println("NDB::testOffPolicyIterator()~actualImprovedBehaviourPolicy=\n"+PolicyTest.toString(behaviourPolicy));
//    	System.out.println("NDB::testOffPolicyIterator()~extectedImprovedPolicy=\n"+PolicyTest.toString(extectedImprovedBehaviourPolicy));

//        assertTrue(PolicyTest.equals(extectedImprovedBehaviourPolicy, behaviourPolicy));
    }
    
	
}
