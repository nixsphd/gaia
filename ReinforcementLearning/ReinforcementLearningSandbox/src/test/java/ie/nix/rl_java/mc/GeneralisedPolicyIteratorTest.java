package ie.nix.rl_java.mc;

import ie.nix.rl_java.Episode;
import ie.nix.rl_java.TestEnvironment;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.MDPTestUtils;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.PolicyTest;
import ie.nix.rl_java.policy.TabularPolicy;
import ie.nix.util_java.Randomness;

import java.util.ArrayList;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class GeneralisedPolicyIteratorTest extends TestCase {
	
	TabularFiniteMDP environment;
//	Agent agent;

    @Override
	protected void setUp() throws Exception {

    	Randomness.setSeed(0);

    	int numberOfStates = 10;
    	int numberOfActions = 10;
    	
    	environment = new TestEnvironment(numberOfStates, numberOfActions);
    	
//    	agent = new Agent();
    	
	}

    public static Test suite() {
        return new TestSuite( GeneralisedPolicyIteratorTest.class );
    }
    
    public void testGeneralisedPolicyIteratorOneEpisode() {
    	TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
	    	new Action[][]{
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{null,null,null,null,null,null,null,null,null,null}},
	    		new double[][]{
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
    	 
		State[] initialStates = {
				environment.getStates()[0],
//				environment.getStates()[1],
//				environment.getStates()[2],
//				environment.getStates()[3],
//				environment.getStates()[4],
//				environment.getStates()[5],
//				environment.getStates()[6],
//				environment.getStates()[7],
//				environment.getStates()[8],
		};
		Action[] initialActions = {
				environment.getActions()[2],
//				environment.getActions()[2],
//				environment.getActions()[2],
//				environment.getActions()[2],
//				environment.getActions()[2],
//				environment.getActions()[2],
//				environment.getActions()[2],
//				environment.getActions()[2],
//				environment.getActions()[2],
		};
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
				new Action[][]{
				{MDPTestUtils.newAction(2)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{null, null, null, null, null, null, null, null, null, null}},
			new double[][]{
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}});
				
		testGeneralisedPolicyIterator(policy, initialStates, initialActions, extectedImprovedPolicy);
	}
    
    public void testGeneralisedPolicyIteratorTwoEpisode() {
    	TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
	    	new Action[][]{
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{null,null,null,null,null,null,null,null,null,null}},
	    		new double[][]{
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
    	 
		State[] initialStates = {
				environment.getStates()[0],
				environment.getStates()[0]
		};
		Action[] initialActions = {
				environment.getActions()[1],
				environment.getActions()[2]
		};
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
				new Action[][]{
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{MDPTestUtils.newAction(1)},
				{null, null, null, null, null, null, null, null, null, null}},
			new double[][]{
				{0.5, 0.5},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{1.0},
				{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}});
				
		testGeneralisedPolicyIterator(policy, initialStates, initialActions, extectedImprovedPolicy);
	}
    
    public void testGeneralisedPolicyIteratorExploringStarts() {
    	TabularPolicy policy =  PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
	    	new Action[][]{
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{MDPTestUtils.newAction(1)},
	    			{null,null,null,null,null,null,null,null,null,null}},
	    		new double[][]{
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{1.0},
	    			{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0}});
    	
    	ArrayList<State> initialStates = new ArrayList<State>();
    	ArrayList<Action> initialActions = new ArrayList<Action>();
    	for (State state : environment.getStates()) {
    		if (state.getType() == State.Type.NonTerminal) {
	        	for (Action action : environment.getActions()) {
	        		initialStates.add(state);
	        		initialActions.add(action);
	        	}
    		}
    	}
		TabularPolicy extectedImprovedPolicy = PolicyTest.newTabularPolicy(environment, Policy.Type.Stochastic, 0.0,
				new Action[][]{
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2), MDPTestUtils.newAction(3), MDPTestUtils.newAction(4), MDPTestUtils.newAction(5), MDPTestUtils.newAction(6), MDPTestUtils.newAction(7), MDPTestUtils.newAction(8), MDPTestUtils.newAction(9)},
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2), MDPTestUtils.newAction(3), MDPTestUtils.newAction(4), MDPTestUtils.newAction(5), MDPTestUtils.newAction(6), MDPTestUtils.newAction(7), MDPTestUtils.newAction(8)},
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2), MDPTestUtils.newAction(3), MDPTestUtils.newAction(4), MDPTestUtils.newAction(5), MDPTestUtils.newAction(6), MDPTestUtils.newAction(7)},
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2), MDPTestUtils.newAction(3), MDPTestUtils.newAction(4), MDPTestUtils.newAction(5), MDPTestUtils.newAction(6)},
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2), MDPTestUtils.newAction(3), MDPTestUtils.newAction(4), MDPTestUtils.newAction(5)},
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2), MDPTestUtils.newAction(3), MDPTestUtils.newAction(4)},
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2), MDPTestUtils.newAction(3)},
				{MDPTestUtils.newAction(1), MDPTestUtils.newAction(2)},
				{MDPTestUtils.newAction(1)},
				{null, null, null, null, null, null, null, null, null, null}},
			new double[][]{
				{0.1111111111111111, 0.1111111111111111, 0.1111111111111111, 0.1111111111111111, 0.1111111111111111, 0.1111111111111111, 0.1111111111111111, 0.1111111111111111, 0.1111111111111111},
				{0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125},
				{0.14285714285714285, 0.14285714285714285, 0.14285714285714285, 0.14285714285714285, 0.14285714285714285, 0.14285714285714285, 0.14285714285714285},
				{0.16666666666666666, 0.16666666666666666, 0.16666666666666666, 0.16666666666666666, 0.16666666666666666, 0.16666666666666666},
				{0.2, 0.2, 0.2, 0.2, 0.2},
				{0.25, 0.25, 0.25, 0.25},
				{0.3333333333333333, 0.3333333333333333, 0.3333333333333333},
				{0.5, 0.5},
				{1.0},
				{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}});
				
		testGeneralisedPolicyIterator(policy, 
				initialStates.toArray(new State[initialStates.size()]),
				initialActions.toArray(new Action[initialActions.size()]), extectedImprovedPolicy);
	}
    
    public void testGeneralisedPolicyIterator(TabularPolicy policy, State[] initialStates, Action[] initialActions, 
    		TabularPolicy extectedImprovedPolicy) {
    	
		double theta = 0.0000001;
		double discount = 1.0;
		GeneralisedPolicyIterator gip = new GeneralisedPolicyIterator(environment, theta, discount);
		
		ActionValueFunction avf = new SampleAverageActionValueFunction(
				environment.getNumberOfStates(), environment.getNumberOfActions());

		int numberOfEpisodes = initialStates.length;
    	Episode[] episodes = new Episode[numberOfEpisodes];
    	for (int i = 0; i < numberOfEpisodes; i++) {
    		episodes[i] = new Episode(environment, initialStates[i], initialActions[i]);
//        	System.out.println("NDB::testGeneralisedPolicyIterator()~episodes["+i+"]=state"+initialStates[i]+", action="+initialActions[i]);
    	}
    	
		gip.iterate(policy, avf, episodes);
		
    	System.out.println("NDB::testGeneralisedPolicyIterator()~actualImprovedPolicy=\n"+PolicyTest.toString(policy));
    	System.out.println("NDB::testGeneralisedPolicyIterator()~extectedImprovedPolicy=\n"+PolicyTest.toString(extectedImprovedPolicy));

        assertTrue(PolicyTest.equals(extectedImprovedPolicy, policy));
    }
    
	
}
