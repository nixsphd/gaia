package ie.nix.rl_java.gambler;

import ie.nix.rl_java.RLFramework;
import ie.nix.rl_java.dp.PolicyIterator;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.RewardFunction;
import ie.nix.rl_java.mdp.State;

public class RewardShapingGamblerProblem extends RLFramework {

	public class PotentialFunction {
		final int target;

		public PotentialFunction(int target) {
			super();
			this.target = target;
		}
		

		public double getPotentialReward(State state) {
			return (target - state.getId())/target;
		}
		
	}
	
	public static void main(String[] args) {
		
		rewardShaping();
		
		System.out.println("Done.");
		
	}

	private static void rewardShaping() {
		final int targetEuros = 100;
		final double probabilityHeads = 0.4;
		final double theta = 0.0000001;
		final double discount = 1.0;

		// Initialise the MDP
		Casino casino = new Casino(targetEuros, probabilityHeads);

		// Update the task so that the reward is based on reward shaping
		final RewardFunction normal = casino.getRewardFunction();
		
		RewardFunction scaledNormal = new RewardFunction() {
			public double getReward(State initialState, Action actionTaken, State finalState) {
				double normalReward = normal.getReward(initialState, actionTaken, finalState);
				return normalReward * targetEuros;
			}
			
		};	
		RewardFunction naiveRewardShaping = new RewardFunction() {
			
			public double getReward(State initialState, Action actionTaken, State finalState) {
				double normalReward = normal.getReward(initialState, actionTaken, finalState);
				return normalReward + finalState.getId();
			}
			
		};			
		RewardFunction potentialBasedRewardShaping = new RewardFunction() {
			
			public double getReward(State initialState, Action actionTaken, State finalState) {
				double normalReward = normal.getReward(initialState, actionTaken, finalState);
				return normalReward + ((discount * finalState.getId()) - initialState.getId());
			}
			
		};	

//		DataFrame rewardFunctions = new DataFrame();
//		PrintWriter rewardFunctionsWriter = rewardFunctions.initWriter("RewardFunctions.csv");
//
//		rewardFunctions.addColumns(new String[]{"RewardMethod", "InitialState", "Action", "FinalState", "Reward"});
//		rewardFunctions.writeHeaders(rewardFunctionsWriter);
//
//		for (State initialState: casino.getStates()) {
//			if (initialState.getType() != State.Type.Terminal) {
//				for (Action action: casino.getStateActionFunction().getActions(initialState)) {
//					for (State finalState: casino.getStates()) {
//						if (casino.getStateTransitionFunction().probabilityOf(finalState, initialState, action) > 0.0) {
//							rewardFunctions.addRow("Normal", initialState,  action, finalState, scaledNormal.getReward(initialState, action, finalState));
//							rewardFunctions.addRow("Naive Reward Shaping", initialState,  action, finalState, naiveRewardShaping.getReward(initialState, action, finalState));
//							rewardFunctions.addRow("Potential-based Reward Shaping", initialState,  action, finalState, potentialBasedRewardShaping.getReward(initialState, action, finalState));
//						}
//					}
//				}
//			}
//		}
//		rewardFunctions.writeRows(rewardFunctionsWriter);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);

		PolicyIterator pi = new PolicyIterator(casino, theta, discount);

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("RewardShaping.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Iteration", "State", "Action", "RewardMethod"});
//		}
//		results.writeHeaders(writer);
		
		casino.setRewardFunction(scaledNormal);
		
		int iteration = 1;
		boolean stable = false;
		do {
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow(iteration, state, action, "Normal");
//					}
//				}
//			}
			
			stable = pi.iterate(gambler, 1);
			
			iteration++;
			
		} while (!stable);

		casino.setRewardFunction(naiveRewardShaping);
		gambler = new Gambler(casino);
		
		iteration = 1;
		stable = false;
		do {
			
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow(iteration, state, action, "Naive Reward Shaping");
//					}
//				}
//			}
			
			stable = pi.iterate(gambler, 1);
			
			iteration++;
			
		} while (!stable);
		
		// Then we try with Naive Shaping
		casino.setRewardFunction(potentialBasedRewardShaping);
		gambler = new Gambler(casino);
		
		iteration = 1;
		stable = false;
		do {
			
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow(iteration, state, action, "Potential-based Reward Shaping");
//					}
//				}
//			}
			
			stable = pi.iterate(gambler, 1);
			
			iteration++;
			
		} while (!stable);
		
//		results.writeRows(writer);
		
	}
	
}
