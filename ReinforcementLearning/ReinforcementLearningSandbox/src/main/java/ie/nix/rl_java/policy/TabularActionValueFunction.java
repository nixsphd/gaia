package ie.nix.rl_java.policy;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;

public class TabularActionValueFunction implements ActionValueFunction {
	
	protected double[][] values;
	
	public TabularActionValueFunction(int numberOfStates, int numberOfActions) {
		this(new double[numberOfStates][numberOfActions]);
	}
	
	protected TabularActionValueFunction(double[][] values) {
		this.values = values;
		
	}
	
	public double getValue(State state, Action action) {
		return values[state.getId()][action.getId()];
	}
	
	public void updateValue(State forState, Action withAction, double value) {
		values[forState.getId()][withAction.getId()] = value;
	}

	public double getValue(StateAction stateAction) {
		return getValue(stateAction.getState(), stateAction.getAction());
	}

	public void updateValue(StateAction stateAction, double value) {
		values[stateAction.getState().getId()][stateAction.getAction().getId()] = value;
		
	}

	@Override
	public ActionValueFunction clone()  {
		TabularActionValueFunction newTabularActionValueFunction = new TabularActionValueFunction(this.values.length, this.values[0].length);
		for (int v = 0; v < this.values.length; v++) {
			System.arraycopy(this.values[v], 0, newTabularActionValueFunction.values[v], 0, this.values[v].length);
			
		}
		return newTabularActionValueFunction;
	}
}
