package ie.nix.rl_java;

import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.ValueFunction;
import ie.nix.rl_java.policy.Policy;

public class Agent {

	protected Policy policy;
	protected ValueFunction valueFunction;
	protected ActionValueFunction actionValueFunction;
	
	public Policy getPolicy() {
		return policy;
	}
	
	public void setPolicy(Policy policy) {
		this.policy = policy;
	}
	
	public ValueFunction getValueFunction() {
		return valueFunction;
	}
	
	public void setValueFunction(ValueFunction vf) {
		this.valueFunction = vf;
	}
	
	public void setActionValueFunction(ActionValueFunction avf) {
		this.actionValueFunction = avf;
		
	}

	public ActionValueFunction getActionValueFunction() {
		return actionValueFunction;
	}
	
}
