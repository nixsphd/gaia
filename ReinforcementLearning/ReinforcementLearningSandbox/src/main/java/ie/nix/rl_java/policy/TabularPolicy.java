package ie.nix.rl_java.policy;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.util_java.Randomness;
import java.util.Arrays;
import java.util.List;

import ie.nix.util_java.Equals;

public class TabularPolicy implements Policy {
	
	TabularFiniteMDP environment;
	Type type;
	Action[][] stateActions;
	double[][] stateActionProbabilities;
	
	double e;
	protected double delta = 0.0000001;

	public TabularPolicy(TabularFiniteMDP environment) {
		this(environment, Type.Deterministic, 0.0);
		
	}
	
	public TabularPolicy(TabularFiniteMDP environment, Type type) {
		this(environment, type, 0.0);
		
	}
	
	public TabularPolicy(TabularFiniteMDP environment, Type type, double e) {
		this.environment = environment;
		this.stateActions = new Action[environment.getNumberOfStates()][environment.getNumberOfActions()];
		this.type = type;
		this.e = e;

		if (type == Type.Stochastic) {
			this.stateActionProbabilities = new double[environment.getNumberOfStates()][environment.getNumberOfActions()];
		}
	}
	
	protected TabularPolicy(TabularFiniteMDP environment, Type type, double e, 
			Action[][] stateActions, double[][] stateActionProbabilities) {
		this.environment = environment;
		this.stateActions = stateActions;
		this.type = type;
		this.e = e;
		
		if (type == Type.Stochastic) {
			this.stateActionProbabilities = stateActionProbabilities;
		}
	}

	@Override
	public Type getType() {
		return type;
	}

	@Override
	public boolean isSoft() {
		return (e != 0.0);
	}

	@Override
	public double getE() {
		return 3;
	}

	@Override
	public State[] getStates() {
		return environment.getStates();
	}
	
	@Override
	public Action[] getPossibleActions(State forState) {

		return environment.getStateActionFunction().getActions(forState);

	}
	
	@Override
	public Action getAction(State givenState) {
		if (getType() != Type.Deterministic) {
			System.err.println("ERROR TabularPolicy.getAction() Policy is not Determinsitic");
		}
		return stateActions[givenState.getId()][0];
	}
	
	@Override
	public Action[] getActions(State givenState) {
		if (getType() != Type.Equiprobable && getType() != Type.Stochastic) {
			System.err.println("ERROR TabularPolicy.getActions() Policy is not Equiprobable or Stochastic");
		}
		return stateActions[givenState.getId()];
	}
	
	@Override
	public double[] getProbabilities(State givenState) {
		if (getType() != Type.Stochastic) {
			System.err.println("ERROR TabularPolicy.getProbabilities() Policy is not Stochastic");
		}
		return stateActionProbabilities[givenState.getId()];
	}
	
	@Override
	public double getProbability(StateAction stateAction) {
		Action[] actions = stateActions[stateAction.getState().getId()];
		for (int a = 0; a < actions.length; a++) {
			if (actions[a].getId() == stateAction.getAction().getId()) {
				if (type == Type.Deterministic) {
					return 1.0;
				} else if (type == Type.Equiprobable) {
					return 1.0/actions.length;
				} else {
					return stateActionProbabilities[stateAction.getState().getId()][a];
				}
			}
		}
		if (isSoft()) {
			return e/getPossibleActions(stateAction.getState()).length;
		} else {
			return 0.0;
		}
	}

	@Override
	public void updateAction(State forState, Action action) {
		if (getType() != Type.Deterministic) {
			System.err.println("ERROR TabularPolicy.updateAction() Policy is not Determinsitic");
		}
		stateActions[forState.getId()] = new Action[1];
		stateActions[forState.getId()][0] = action;
	}

	@Override
	public void updateActions(State forState, Action[] actions) {
		if (getType() != Type.Equiprobable) {
			System.err.println("ERROR TabularPolicy.updateActions() Policy is not Equiprobable");
		}
		stateActions[forState.getId()] = new Action[actions.length];
		System.arraycopy(actions, 0, stateActions[forState.getId()], 0, actions.length);
		
	}
	
	@Override
	public void updateActions(State forState, Action[] actions, double[] probabilities) {
		if (getType() != Type.Stochastic) {
			System.err.println("ERROR TabularPolicy.updateActions() Policy is not Stochastic");
		}
		
		stateActions[forState.getId()] = new Action[actions.length];
		System.arraycopy(actions, 0, stateActions[forState.getId()], 0, actions.length);
		
		validateProbabilities(probabilities);
		stateActionProbabilities[forState.getId()] = new double[probabilities.length];
		System.arraycopy(probabilities, 0, stateActionProbabilities[forState.getId()], 0, probabilities.length);
		
	}
	
	@Override
	public Action selectAction(State forState) {
		switch (getType()) {
			case Deterministic: 
				if (!isSoft()) {
					return selectActionDeterministicGreedy(forState);
				} else {
					return selectActionDeterministicSoft(forState);
				}
			case Equiprobable: 
				if (!isSoft()) {
					return selectActionEquiprobableGreedy(forState);
				} else {
					return selectActionEquiprobableSoft(forState);
				}
			case Stochastic: 
				if (!isSoft()) {
					return selectActionStochasticGreedy(forState);
				} else {
					return selectActionStochasticSoft(forState);
				}
		}
		System.err.println("ERROR TabularPolicy.updateAction() This should never happen, some how the policy type is not set, type="+getType());
		return null;
	}

	protected Action selectActionDeterministicGreedy(State forState) {
		return stateActions[forState.getId()][0];
	}
	
	protected Action selectActionDeterministicSoft(State forState) {
		Action greedyAction = stateActions[forState.getId()][0];
		Action[] possibleAction = getPossibleActions(forState);
		double[] softActionProbabilities = new double[possibleAction.length];

		for (int pa = 0; pa < possibleAction.length; pa++) {
			Action action = possibleAction[pa];
			if (action.getId() == greedyAction.getId()) {
//				softActionProbabilities[pa] = 1.0 - ((possibleAction.length -1)*e); 
				softActionProbabilities[pa] = 1.0 - e + (e/possibleAction.length);
			} else {
				softActionProbabilities[pa] = e/possibleAction.length;
			}
		
		}
		return selectAction(possibleAction, softActionProbabilities);
	}
	
	protected Action selectActionEquiprobableGreedy(State forState) {
		Action[] equiprobableActions = stateActions[forState.getId()];
		double[] equiprobableActionProbabilities = new double[equiprobableActions.length];
		Arrays.fill(equiprobableActionProbabilities, 1.0/equiprobableActions.length);
		return selectAction(equiprobableActions, equiprobableActionProbabilities);
		
	}
	
	protected Action selectActionEquiprobableSoft(State forState) {
		List<Action> equiprobableActionsList = Arrays.asList(getActions(forState));
		Action[] possibleAction = getPossibleActions(forState);
		
		double[] softActionProbabilities = new double[possibleAction.length];
//		double equiprobableProbability = (1.0 - (e * (possibleAction.length - equiprobableActionsList.size()))) / equiprobableActionsList.size();
		double equiprobableProbability = ((1.0 - e) / equiprobableActionsList.size()) + e / possibleAction.length;

		if (equiprobableProbability >= e) {
			for (int pa = 0; pa < possibleAction.length; pa++) {
				Action action = possibleAction[pa];
				if (equiprobableActionsList.contains(action)) {
					softActionProbabilities[pa] = equiprobableProbability;
					
				} else {
//					softActionProbabilities[pa] = e;
					softActionProbabilities[pa] = e/possibleAction.length;
					
				}
			
			}
		} else {
			// This is a special case where the e amount is greater than the equiprobable, so it'll 
			// add up to greater than one. Instead we just use the equiprobable ratio as normal. 
			Arrays.fill(softActionProbabilities, 1.0/possibleAction.length);
			
		}
		return selectAction(possibleAction, softActionProbabilities);
		
	}
	
	protected Action selectActionStochasticGreedy(State forState) {
		return selectAction(stateActions[forState.getId()], stateActionProbabilities[forState.getId()]);
		
	}
	
	protected Action selectActionStochasticSoft(State forState) {
		List<Action> probableActionsList = Arrays.asList(getActions(forState));
		double[] probableActionsProbabilities = stateActionProbabilities[forState.getId()];
		
		Action[] possibleAction = getPossibleActions(forState);
		double[] softActionProbabilities = new double[possibleAction.length];
		
		int numberOfProbableActions = probableActionsList.size();
		
		// Here we need to check that the probability of non3 of the probably actions is less than e. 
		// If they are they should be removed
		double probableProbabilityDifference = 0;
		for (Action action : probableActionsList) {
			double probability = probableActionsProbabilities[probableActionsList.indexOf(action)];
			if (probability < e/possibleAction.length) {
				// Removing messes up the index which we need to get the probabilities from 
				// probableActionsProbabilities, so instead we set that index to null.
				probableActionsList.set(probableActionsList.indexOf(action), null);
				probableProbabilityDifference += probability;
				numberOfProbableActions--;
			}
		}
		
//		probableProbabilityDifference -= (e * (possibleAction.length - numberOfProbableActions)) / numberOfProbableActions;
		probableProbabilityDifference += (-e/numberOfProbableActions) + e/possibleAction.length;;
		
		for (int pa = 0; pa < possibleAction.length; pa++) {
			Action action = possibleAction[pa];
			if (probableActionsList.contains(action)) {
				softActionProbabilities[pa] = probableActionsProbabilities[probableActionsList.indexOf(action)]
						+ probableProbabilityDifference;
				
			} else {
//				softActionProbabilities[pa] = e;
				softActionProbabilities[pa] = e/possibleAction.length;
			}
		
		}
		return selectAction(possibleAction, softActionProbabilities);
	
	}
	
	protected Action selectAction(Action[] actions, double[] probabilities) {

		validateProbabilities(probabilities);
		
		double select = Randomness.nextDouble();
		int actionIndex = 0;
		double accumProb = probabilities[actionIndex];
		while (Equals.lessThan(accumProb, select, delta)) {
			actionIndex++;
			accumProb += probabilities[actionIndex];
		}
		return actions[actionIndex];
	}

	protected void validateProbabilities(double[] probabilities) {
		double totalProbability = 0;
		for (double probability : probabilities){
			totalProbability += probability;
		}
		if (!Equals.equals(totalProbability, 1.0,  delta)) {
			System.out.println("ERROR TabularPolicy.updateActions() Probabilities not adding up to 1, totalProb="+totalProbability);
		}
	}

	public String toString() {
		StringBuilder string = new StringBuilder("\n[");
		for (int s = 0; s < stateActions.length; s++) {
			string.append("[");
			for (int sa = 0; sa < stateActions[s].length; sa++) {
				string.append(stateActions[s][sa]+"/"+String.format("%.1f", stateActionProbabilities[s][sa])+", ");
			}
			string.replace(string.length() - 2, string.length(),"],\n");
		}
		string.replace(string.length() - 2, string.length(),"]");
		return string.toString();
	}
	
}
