package ie.nix.rl_java.policy;

import ie.nix.rl_java.mdp.State;

public interface ValueFunction extends Cloneable {
	
	public double getValue(State state);

	public double[] getValues();

	public int getNumberOfValues();

	public void updateValue(State state, double value);

	public ValueFunction clone();
	
	// TODO - Generate backup diagram
}
