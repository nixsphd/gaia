package ie.nix.rl_java.mc;

import ie.nix.util_java.Equals;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.ValueFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

public class PolicyImprover {

	protected TabularFiniteMDP environment;
	protected double discount;
	protected double delta;
	
	public PolicyImprover(TabularFiniteMDP environment, double delta, double discount) {
		this.environment = environment;
		this.delta = delta;
		this.discount = discount;
		
	}
	
	public boolean improve(Policy policy, ValueFunction valueFunction, ArrayList<State> vistedStates) {
		System.out.println("ERROR PolicyImprover.improve(Policy, ValueFunction, State[]) NOT IMPLEMENTED");
		return false;
		
	}
	
	public boolean improve(Policy policy, ActionValueFunction actionValueFunction, ArrayList<StateAction> vistedStateActions) {
		HashSet<State> uniqueStateVisits = new HashSet<State>();
		for (StateAction visitedStateAction: vistedStateActions) {
			uniqueStateVisits.add(visitedStateAction.getState());
		}
		return improve(policy, actionValueFunction, uniqueStateVisits);
		
	}

	public boolean improve(Policy policy, ActionValueFunction actionValueFunction, Collection<State> vistedStates) {
		boolean policyUpdated = false;
		for (State state : vistedStates) {
			switch (policy.getType()) {
				case Deterministic: 
					policyUpdated = improveDeterministicPolicy(policy, actionValueFunction, state);
					break;
				case Equiprobable: 
					policyUpdated = improveEquiprobablePolicy(policy, actionValueFunction, state);
					break;
				case Stochastic: 
					policyUpdated = improveStochasticPolicy(policy, actionValueFunction, state);
					break;
				}
		}
		return policyUpdated;
			
	}
	

	public boolean improveDeterministicPolicy(Policy policy, ActionValueFunction actionValueFunction, State forState) {
		Action newAction = getBestAction(forState, actionValueFunction);
    	if (policy.getAction(forState) != newAction) {
//        	System.out.println("NDB::PolicyImprover.improveDeterministicPolicy()~state="+forState+", newAction=\n"+newAction);
			policy.updateAction(forState, newAction); 
			 return true;
		} else {
			return false;
		}
		
	}
	
	public boolean improveEquiprobablePolicy(Policy policy, ActionValueFunction actionValueFunction, State forState) {
		Action[] bestActions = getBestActions(forState, actionValueFunction, delta);
    	if (bestActions.length != 0 && 
		    (!Arrays.equals(policy.getActions(forState), bestActions))) {
//    		if (bestActions.length > 1) {
//    			System.out.println("NDB::PolicyImprover.improveEquiprobablePolicy()~state="+forState+", original actions=\n"+MDPTest.toString(policy.getActions(forState)));
//    			System.out.println("NDB::PolicyImprover.improveEquiprobablePolicy()~state="+forState+", bestActions=\n"+MDPTest.toString(bestActions));
//
//    		}
			policy.updateActions(forState, bestActions); 
			return true;
		} else {
			return false;
		}
		
	}
	
	public boolean improveStochasticPolicy(Policy policy, ActionValueFunction actionValueFunction, State forState) {
		Action[] newProbableActions = getProbableActions(forState, actionValueFunction, delta);
		double[] newProbabilities = getProbababilities(forState, newProbableActions, actionValueFunction);
//		if (newProbableActions.length == 0) {
			// What should I do, nothing or make all actions equiprobable?
//			newProbableActions = policy.getPossibleActions(forState);
//			newProbabilities = new double[newProbableActions.length];
//			Arrays.fill(newProbabilities, 1.0/newProbabilities.length);
//		}
		if  ((newProbableActions.length != 0) && (!Arrays.equals(policy.getActions(forState), newProbableActions) || 
			!Equals.equals(policy.getProbabilities(forState), newProbabilities, delta))) {
//				System.out.println("NDB::PolicyImprover.improveStochasticPolicy()~state="+forState+", original actions=\n"+MDPTest.toString(policy.getActions(forState)));
//		    	System.out.println("NDB::PolicyImprover.improveStochasticPolicy()~state="+forState+", original probabilities=\n"+ToString.toString(policy.getProbabilities(forState)));
//		    	System.out.println("NDB::PolicyImprover.improveStochasticPolicy()~state="+forState+", newProbableActions=\n"+MDPTest.toString(newProbableActions));
//		    	System.out.println("NDB::PolicyImprover.improveStochasticPolicy()~state="+forState+", newProbabilities=\n"+ToString.toString(newProbabilities));
				policy.updateActions(forState, newProbableActions, newProbabilities); 
			 return true;
		} else {
			return false;
		}
	}
	
	protected Action getBestAction(State forState, ActionValueFunction actionValueFunction) {
		
		// Get the actions for state s
		Action[] actions = environment.getStateActionFunction().getActions(forState);
	
		// Get the action values
		double[] actionValues = getActionValues(forState, actionValueFunction, actions);
		
		// Pick the best action based on the action values
		Action bestAction = null; 
		double bestActionValue = Double.NEGATIVE_INFINITY;
		for (int a = 1; a < actions.length; a++) {
			if (bestActionValue < actionValues[a]) {
				bestActionValue = actionValues[a];
				bestAction = actions[a];
			}
		}
		return bestAction;
	}
	
	protected Action[] getBestActions(State forState, ActionValueFunction actionValueFunction, double delta) {
		
		// Get the actions for state s
		Action[] actions = environment.getStateActionFunction().getActions(forState);
	
		// Get the action values
		double[] actionValues = getActionValues(forState, actionValueFunction, actions);			    	
//		System.out.println("NDB::PolicyImprover.getBestActions()~state="+forState+", actionValues=\n"+ToString.toString(actionValues));
		
		// Pick the best action based on the action values
		ArrayList<Action> bestActions = new ArrayList<Action>(actions.length);
		double bestActionValue = Double.NEGATIVE_INFINITY;
		for (int a = 0; a < actions.length; a++) {
			if (Equals.lessThan(bestActionValue, actionValues[a], delta)) {
				bestActionValue = actionValues[a];
				bestActions.clear();
				bestActions.add(actions[a]);
				
			} else if (Equals.equals(bestActionValue, actionValues[a], delta)) {
				bestActions.add(actions[a]);
				
			}
		}
		return bestActions.toArray(new Action[bestActions.size()]);
		
	}

	protected Action[] getProbableActions(State forState, ActionValueFunction actionValueFunction, double delta) {
				
			// Get the actions for state s
			Action[] actions = environment.getStateActionFunction().getActions(forState);
		
			// Get the action values
			double[] actionValues = getActionValues(forState, actionValueFunction, actions);			    	
//			System.out.println("NDB::PolicyImprover.getProbableActions()~state="+forState+", actionValues=\n"+ToString.toString(actionValues));
			
			// Pick the best action based on the action values
			ArrayList<Action> probableActionsList = new ArrayList<Action>(actions.length);
			for (int a = 0; a < actions.length; a++) {
				if (Equals.greaterThan(actionValues[a], 0, delta)) {
					probableActionsList.add(actions[a]);
//					System.out.println("NDB::PolicyImprover.getProbableActions()~state="+forState+", actionValues["+a+"]="+actionValues[a]+" Probable");
				
//				} else {
//					System.out.println("NDB::PolicyImprover.getProbableActions()~state="+forState+", actionValues["+a+"]="+actionValues[a]+" Not Probable");
//					
				}
			}
			Action[] probableActions = probableActionsList.toArray(new Action[probableActionsList.size()]);
//			System.out.println("NDB::PolicyImprover.getProbableActions()~state="+forState+", probableActions="+ToString.toString(probableActions));
			return probableActions;
			
		}

	protected double[] getProbababilities(State forState, Action[] forActions, ActionValueFunction actionValueFunction) {
		double[] newProbabilities = new double[forActions.length];
	
		// Get the action values
		double[] actionValues = getActionValues(forState, actionValueFunction, forActions);			    	
		
		double totalValues = 0;
		for (int a = 0; a < actionValues.length; a++) {
			totalValues += actionValues[a];
		}
		for (int a = 0; a < actionValues.length; a++) {
			newProbabilities[a] = actionValues[a]/totalValues;
		}
		return newProbabilities;
	}

	protected double[] getActionValues(State forState, ActionValueFunction actionValueFunction, Action[] forActions) {
		// Get all the action values for the possible actions.
		double[] actionValues = new double[forActions.length];
		for (int a = 0; a < forActions.length; a++) {
			Action action = forActions[a];
			actionValues[a] = actionValueFunction.getValue(forState, action);
		}
		return actionValues;
	}

}

