package ie.nix.rl_java.dp;

import ie.nix.rl_java.Agent;
import ie.nix.rl_java.policy.ValueFunction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy;

public class PolicyIterator  {
	
	protected PolicyEvaluator pe;
	protected PolicyImprover pi;

	public PolicyIterator(TabularFiniteMDP environment, double theta, double discount) {
		this(new PolicyEvaluator(environment, theta, discount), 
			new PolicyImprover(environment, discount));
	}
	
	public PolicyIterator(PolicyEvaluator pe, PolicyImprover pi) {
		this.pe = pe;
		this.pi = pi;
	}

	public boolean iterate(Agent agent) {
		return iterate(agent.getPolicy(), agent.getValueFunction());
		
	}
	
	public boolean iterate(Agent agent, int numberOfIterations) {
		return iterate(agent.getPolicy(), agent.getValueFunction(), numberOfIterations);
		
	}

	public boolean iterate(Policy policy, ValueFunction valueFunction) {
		boolean policyUpdated;
		do {
			// Evaluate the policy
			pe.evaluate(policy, valueFunction);
			
			// Improve the policy
			policyUpdated = pi.improve(policy, valueFunction);
			
		} while (policyUpdated);
		
		return !policyUpdated;
		
	}
	
	public boolean iterate(Policy policy, ValueFunction valueFunction, int numberOfIterations) {
		boolean policyUpdated;
		do {
			// Evaluate the policy
			pe.evaluate(policy, valueFunction);
			
			// Improve the policy
			policyUpdated = pi.improve(policy, valueFunction);
			
			numberOfIterations--;
			
		} while ((policyUpdated) && (numberOfIterations > 0));
		
		return !policyUpdated;
	}

}
