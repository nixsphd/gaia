package ie.nix.rl_java.mdp;

public class StateAction {
	
	protected State state; 
	protected Action action;
	
	public StateAction(State state, Action action) {
		super();
		this.state = state;
		this.action = action;
	}
	public State getState() {
		return state;
	}
	public Action getAction() {
		return action;
	}
	
	@Override
	public String toString() {
		return "["+state.getId() +", "+action.getId()+"]";
		
	}
	
}
