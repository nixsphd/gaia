package ie.nix.rl_java.mc;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.policy.TabularActionValueFunction;

public class WeightedAverageActionValueFunction extends TabularActionValueFunction {

	double[][] weights;
	
	public WeightedAverageActionValueFunction(int numberOfStates, int numberOfActions) {
		super(numberOfStates, numberOfActions);
		this.weights = new double[numberOfStates][numberOfActions];
		
	}
	
	protected WeightedAverageActionValueFunction(double[][] values, double[][] weights) {
		super(values);
		this.weights = weights;
		    
	}
	
	public void updateValue(State forState, Action withAction, double value, double weight) {
		values[forState.getId()][withAction.getId()] += value;
		weights[forState.getId()][withAction.getId()] += weight;
	}
	
	public void updateValue(StateAction stateAction, double value) {
		updateValue(stateAction.getState(), stateAction.getAction(), value);
		
	}
	
	public double getValue(State state, Action action) {
		if (weights[state.getId()][action.getId()] != 0) {
			return values[state.getId()][action.getId()]/weights[state.getId()][action.getId()];
		} else {
			return 0;
		}
	}

	public double getweight(State state, Action action) {
		return weights[state.getId()][action.getId()];
	}

	public double getweight(StateAction stateAction) {
		return weights[stateAction.getState().getId()][stateAction.getAction().getId()];
	}

}
