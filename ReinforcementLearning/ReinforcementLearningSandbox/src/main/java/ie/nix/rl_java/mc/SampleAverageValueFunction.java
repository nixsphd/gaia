package ie.nix.rl_java.mc;

import ie.nix.rl_java.policy.TabularValueFunction;
import ie.nix.rl_java.mdp.State;

public class SampleAverageValueFunction extends TabularValueFunction {

	protected int[] numberOfSamples;
	
	public SampleAverageValueFunction(int numberOfStates) {
		super(numberOfStates);
		numberOfSamples = new int[numberOfStates];
		
	}

	public void updateValue(State state, double value) {
		numberOfSamples[state.getId()] += 1;
		values[state.getId()] += (value - values[state.getId()])/numberOfSamples[state.getId()];
		
	}

	public int getNumberOfSamples(State state) {
		return numberOfSamples[state.getId()];
	}

}
