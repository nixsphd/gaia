package ie.nix.rl_java.dp;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.State.Type;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.ValueFunction;
import ie.nix.rl_java.policy.Policy;

public class PolicyEvaluator {
	
	protected TabularFiniteMDP environment;
	protected double theta;
	protected double discount;
	
	public PolicyEvaluator(TabularFiniteMDP environment, double theta, double discount) {
		this.environment = environment;
		this.theta = theta;
		this.discount = discount;
	}

	public double evaluate(Policy policy, ValueFunction valueFunction) {
		double delta; 
		// Full backup
		do {
			delta = sweep(policy, valueFunction);
		} while (delta > theta);
		
		return delta;
			
	}
	
	public double evaluate(Policy policy, ValueFunction valueFunction, int numberOfSweeps) {
		double delta; 
		do {
			delta = sweep(policy, valueFunction);
			numberOfSweeps--;
		} while ((delta > theta) && (numberOfSweeps > 0));
		return delta;
			
	}

	protected double sweep(Policy policy, ValueFunction valueFunction) {
		ValueFunction origionalValueFunction = valueFunction.clone();
		double delta = 0;
		// Sweep the state space
		for (State state : environment.getStates()) {
			// Only ever backup the non-terminal states.
			if (state.getType() == Type.NonTerminal) {
				// Can either have a second array or update 'in-place' as here.
				valueFunction.updateValue(state, backup(policy, origionalValueFunction, state)); 
				delta = Math.max(delta, Math.abs(origionalValueFunction.getValue(state) - valueFunction.getValue(state)));

				System.out.println("NDB::PolicyEvaluator.sweep()~state="+state+
						",old value="+origionalValueFunction.getValue(state) +
						", new value="+valueFunction.getValue(state));
					
			}
	
		}
		return delta;
	}

	protected double backup(Policy policy, ValueFunction valueFunction, State state) {
		
		double value = 0;
		
		if (policy.getType() == Policy.Type.Deterministic) {
			// Get the Actions for state s
			Action action = policy.getAction(state);
			// For all next states s', given s and a
			for (State nextState : environment.getStates()) {
				double prob = environment.getStateTransitionFunction().probabilityOf(nextState, state, action);
				if (prob != 0.0) {
					double reward = environment.getRewardFunction().getReward(state, action, nextState);
					
					//v(s) = p(s'|a,s) * (reward(s,a,s') + (y * v(s')))
					value += prob * (reward + (discount * valueFunction.getValue(nextState)));
					
				}
			}
			
		} else if (policy.getType() == Policy.Type.Equiprobable) {
			
			// Get the Actions for state s
			Action[] actions = policy.getActions(state);
			double probability = 1.0/actions.length;
			
			// Get all the action values for the possible actions.
			for (int a = 0; a < actions.length; a++) {
				Action action = actions[a];
				double actionValue = 0; 
				for (State nextState : environment.getStates()) {
					double prob = environment.getStateTransitionFunction().probabilityOf(nextState, state, action);
					if (prob != 0.0) {
						double reward = environment.getRewardFunction().getReward(state, action, nextState);
						actionValue += prob * (reward + (discount * valueFunction.getValue(nextState)));
						
						System.out.println("NDB::PolicyEvaluator.backup()~state="+state+
								",action="+a +
								",nextState="+nextState +
								", actionValue="+actionValue);
					}
				}
				value += probability * actionValue;
			}
		
		} else if (policy.getType() == Policy.Type.Stochastic) {
			// Get the Actions for state s
			Action[] actions = policy.getActions(state);
			double[] probabilities = policy.getProbabilities(state);
			
			// Get all the action values for the possible actions.
			for (int a = 0; a < actions.length; a++) {
				Action action = actions[a];
				double actionValue = 0; 
				for (State nextState : environment.getStates()) {
					double prob = environment.getStateTransitionFunction().probabilityOf(nextState, state, action);
					if (prob != 0.0) {
						double reward = environment.getRewardFunction().getReward(state, action, nextState);
						actionValue += prob * (reward + (discount * valueFunction.getValue(nextState)));
					}
				}
				value += probabilities[a] * actionValue;
			}
		}
		return value;
	}

	public double evaluate(Policy policy, ActionValueFunction actionValueFunction) {
		double delta; 
		// Full backup
		do {
			delta = sweep(policy, actionValueFunction);
		} while (delta > theta);
		
		return delta;
			
	}
	
	public double evaluate(Policy policy, ActionValueFunction actionValueFunction, int numberOfSweeps) {
		double delta; 
		do {
			delta = sweep(policy, actionValueFunction);
			numberOfSweeps--;
		} while ((delta > theta) && (numberOfSweeps > 0));
		return delta;
			
	}

	protected double sweep(Policy policy, ActionValueFunction actionValueFunction) {
		ActionValueFunction origionalActionValueFunction = actionValueFunction.clone();
		double delta = 0;
		// Sweep the state space
		for (State state : environment.getStates()) {
			// Only ever backup the non-terminal states.
			if (state.getType() == Type.NonTerminal) {
				// Can either have a second array or update 'in-place' as here.
				// Get the Actions for state s
				Action[] actions = policy.getActions(state);
				// Get all the action values for the possible actions.
				for (int a = 0; a < actions.length; a++) {
					StateAction stateAction = new StateAction(state, actions[a]);
					
					actionValueFunction.updateValue(stateAction, backup(policy, origionalActionValueFunction, stateAction));
					delta = Math.max(delta, Math.abs(origionalActionValueFunction.getValue(stateAction) - actionValueFunction.getValue(stateAction)));
//					System.out.println("NDB::PolicyEvaluator.sweep()~state="+state+", action="+actions[a]
//							+",old value="+tempValue+", new value="+actionValueFunction.getValue(stateAction));
					
				}
			}

		}
		return delta;
	}
	
	protected double backup(Policy policy, ActionValueFunction actionValueFunction, StateAction stateAction) {
		
		double actionValue = 0;

		if (policy.getType() == Policy.Type.Deterministic) {
			
			// For all next states s', given s and a
			for (State nextState : environment.getStates()) {
				double prob = environment.getStateTransitionFunction().probabilityOf(nextState, stateAction.getState(), stateAction.getAction());
				if (prob != 0.0) {
					double reward = environment.getRewardFunction().getReward(stateAction.getState(), stateAction.getAction(), nextState);

					if (nextState.getType() == Type.NonTerminal) {
						actionValue += prob * (reward + (discount * actionValueFunction.getValue(nextState, policy.getAction(nextState))));
					} else {
						actionValue += prob * reward;
					}
					
				}
			}

		} else if (policy.getType() == Policy.Type.Equiprobable) {
			
			// For all next states s', given s and a
			for (State nextState : environment.getStates()) {
				double prob = environment.getStateTransitionFunction().probabilityOf(nextState, stateAction.getState(), stateAction.getAction());
				if (prob != 0.0) {
					double reward = environment.getRewardFunction().getReward(stateAction.getState(), stateAction.getAction(), nextState);

					if (nextState.getType() == Type.NonTerminal) {
						
						Action[] actions = policy.getActions(nextState);
						double probability = 1.0/actions.length;

						for (int a = 0; a < actions.length; a++) {
							Action action = actions[a];
							actionValue += prob * (reward + (discount * probability * actionValueFunction.getValue(nextState, action)));
						}
						
					} else {
						actionValue += prob * reward;
					}
					
				}
			}
			
		
		} else if (policy.getType() == Policy.Type.Stochastic) {
			
			// For all next states s', given s and a
			for (State nextState : environment.getStates()) {
				double prob = environment.getStateTransitionFunction().probabilityOf(nextState, stateAction.getState(), stateAction.getAction());
				if (prob != 0.0) {
					double reward = environment.getRewardFunction().getReward(stateAction.getState(), stateAction.getAction(), nextState);

					if (nextState.getType() == Type.NonTerminal) {
						
						Action[] actions = policy.getActions(nextState);
						double[] probabilities = policy.getProbabilities(nextState);
						
						for (int a = 0; a < actions.length; a++) {
							Action action = actions[a];
							actionValue += prob * (reward + (discount * probabilities[a] * actionValueFunction.getValue(nextState, action)));
						}
						
					} else {
						actionValue += prob * reward;
					}
					
				}
			}
		} 
		
		return actionValue;
	}


}
