package ie.nix.rl_java.mdp;


public class State {

	public enum Type {
		NonTerminal,
		Terminal,
		Absorbing // Can only transition to itself and has a reward of 0.
	};

	private int id;
	protected Type type;
	
	protected State(int id) {
		this(id, Type.Terminal);
	}
	
	protected State(int id, Type type) {
		this.id = id;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public Type getType() {
		return type;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}
	
}
