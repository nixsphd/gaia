package ie.nix.rl_java.gambler;

import ie.nix.rl_java.RLFramework;
import ie.nix.rl_java.dp.GeneralisedPolicyIterator;
import ie.nix.rl_java.dp.PolicyEvaluator;
import ie.nix.rl_java.dp.PolicyImprover;
import ie.nix.rl_java.dp.PolicyIterator;
import ie.nix.rl_java.dp.ValueIterator;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;

import java.io.PrintWriter;

public class DPGamblerProblem extends RLFramework {

	public static void main(String[] args) {
		
		valueEstimates();
		
		actionValueEstimates();
		
		policyImprovement();
		
		policyIteration();
		
		valueIteration();
		
		generalisedPolicyIteration();

		oddAndEven();
		
		oddAndEven78();
		
		powersOfTwo();
		
		System.out.println("Done.");
		
	}
	
	public static void valueEstimates() {
		final int targetDollars = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;
		
		// Initialise the MDP
		Casino casino = new Casino(targetDollars, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);

		// Initialise the policy evaluator
		PolicyEvaluator pe = new PolicyEvaluator(casino, theta, discount);

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("DPValueEstimates.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Sweep", "State", "Value"});
//		}
//		results.writeHeaders(writer);

		// Evaluate the policy
		int sweep = 0;
		double delta = 0; 
		do {
			delta = pe.evaluate(gambler.getPolicy(), gambler.getValueFunction(), 1);
			
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					results.addRow(sweep, state, gambler.getValueFunction().getValue(state));
//				}
//			}
			sweep++;
			
		} while (delta > theta);
		
//		results.writeRows(writer);
	}
	
	public static void actionValueEstimates() {
		final int targetDollars = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;
		
		// Initialise the MDP
		Casino casino = new Casino(targetDollars, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		gambler.setPolicy(gambler.createEquiprobablePolicy(casino));

		// Initialise the policy evaluator
		PolicyEvaluator pe = new PolicyEvaluator(casino, theta, discount);

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("DPActionValueEstimates.csv");
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Sweep", "State", "Action", "Value"});
//		}
//		results.writeHeaders(writer);

		// Evaluate the policy
		int sweep = 0;
		double delta = 0;
		do {
			delta = pe.evaluate(gambler.getPolicy(), gambler.getActionValueFunction(),1);
//			System.out.println("NDB::GamblerProblemWithMC.actionValueEstimates()~sweep="+sweep+", delta="+delta);
//
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					for (Action action : casino.getActions()) {
//						results.addRow(sweep, state, action, gambler.getActionValueFunction().getValue(state, action));
//					}
//				}
//			}
//			sweep++;
//
		} while (delta > theta);
//
//		results.writeRows(writer);
	}
	
	public static void policyImprovement() {
		final int targetDollars = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;

		// Initialise the MDP
		Casino casino = new Casino(targetDollars, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		gambler.setPolicy(gambler.createEquiprobablePolicy(casino));

		// Initialise the policy evaluator
		PolicyEvaluator pe = new PolicyEvaluator(casino, theta, discount);
		
		// Initialise the policy improver
		PolicyImprover pi = new PolicyImprover(casino, discount);

//		DataFrame policyResults = new DataFrame();
//		PrintWriter policyWriter = policyResults.initWriter("DP_PI_Policy.csv");
//		if (policyResults.getNumberOfColumns() == 0) {
//			policyResults.addColumns(new String[]{"State", "Action", "Policy"});
//		}
//		policyResults.writeHeaders(policyWriter);
//
//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("DP_PI_ActionValueFunction.csv");
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"State", "Action", "Value", "Policy"});
//		}
//		results.writeHeaders(writer);
//
//		for (State state : casino.getStates()) {
//			if (state.getType() == State.Type.NonTerminal) {
//				Action[] actions = gambler.getPolicy().getActions(state);
//				for (int a = 0; a < actions.length; a++) {
//					policyResults.addRow(state, actions[a], "Initial");
//				}
//				for (Action action : casino.getStateActionFunction().getActions(state)) {
//					results.addRow(state, action, gambler.getActionValueFunction().getValue(state, action), "Initial");
//				}
//			}
//		}

//		policyResults.writeRows(policyWriter);
//		results.writeRows(writer);
		
		// Evaluate the policy
		pe.evaluate(gambler.getPolicy(), gambler.getValueFunction());
		pe.evaluate(gambler.getPolicy(), gambler.getActionValueFunction());
		
		// Improve the policy
		pi.improve(gambler.getPolicy(), gambler.getValueFunction());
		
		// Reevaluate the Action Value function now
		pe.evaluate(gambler.getPolicy(), gambler.getActionValueFunction());
		
//		for (State state : casino.getStates()) {
//			if (state.getType() == State.Type.NonTerminal) {
//				Action[] actions = gambler.getPolicy().getActions(state);
//				for (int a = 0; a < actions.length; a++) {
//					policyResults.addRow(state, actions[a], "Improved");
//				}
//				for (Action action : casino.getStateActionFunction().getActions(state)) {
//					results.addRow(state, action, gambler.getActionValueFunction().getValue(state, action), "Improved");
//				}
//			}
//		}
//		policyResults.writeRows(policyWriter);
//		results.writeRows(writer);

	}
	
	public static void policyIteration() {

		final int targetDollars = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;

		// Initialise the MDP
		Casino casino = new Casino(targetDollars, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		gambler.setPolicy(gambler.createEquiprobablePolicy(casino));

		PolicyEvaluator evaluator = new PolicyEvaluator(casino, theta, discount);
		PolicyImprover improver = new PolicyImprover(casino, discount);
		PolicyIterator pi = new PolicyIterator(evaluator, improver);

//		DataFrame piResults = new DataFrame();
//		PrintWriter pIWriter = piResults.initWriter("DPPolicyIteration.csv");
//
//		if (piResults.getNumberOfColumns() == 0) {
//			piResults.addColumns(new String[]{"Iteration", "State", "Action"});
//		}
//		piResults.writeHeaders(pIWriter);
//
//		DataFrame dpPIresults = new DataFrame();
//		PrintWriter dpPIWriter = dpPIresults.initWriter("DPPIActionValueFunction.csv");
//
//		if (dpPIresults.getNumberOfColumns() == 0) {
//			dpPIresults.addColumns(new String[]{"Iteration", "State", "Action", "Value"});
//		}
//		dpPIresults.writeHeaders(dpPIWriter);
//
//		int iteration = 1;
//		boolean stable = false;
//		do {
//
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						piResults.addRow(iteration, state, action);
//					}
//
//					evaluator.evaluate(gambler.getPolicy(), gambler.getActionValueFunction());
//					for (Action action : casino.getStateActionFunction().getActions(state)) {
//						dpPIresults.addRow(iteration, state, action, gambler.getActionValueFunction().getValue(state, action));
//					}
//
//				}
//			}
//
//			stable = pi.iterate(gambler, 1);
//
//			iteration++;
//
//		} while (!stable);
//
//		piResults.writeRows(pIWriter);
//		dpPIresults.writeRows(dpPIWriter);
		
		
	}

	public static void valueIteration() {
		final int targetDollars = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;

		// Initialise the MDP
		Casino casino = new Casino(targetDollars, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);

		ValueIterator vi = new ValueIterator(casino, theta, discount);

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("DPValueIteration.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Iteration", "State", "Action"});
//		}
//		results.writeHeaders(writer);

//		int iteration = 1;
//		boolean stable = false;
//		do {
//
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow(iteration, state, action);
//					}
//				}
//			}
//
//			stable = vi.iterate(gambler, 1);
//
//			iteration++;
//
//		} while (!stable);
		
//		results.writeRows(writer);
		
	}
	
	public static void generalisedPolicyIteration() {
		final int targetDollars = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;

		// Initialise the MDP
		Casino casino = new Casino(targetDollars, probabilityHeads);

		GeneralisedPolicyIterator gpi = new GeneralisedPolicyIterator(casino, theta, discount);

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("DPGeneralisedPolicyIteration.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Policy", "State", "Action", "NumberOfSweeps"});
//		}
//		results.writeHeaders(writer);
//
//		for (int numberOfSweeps = 1; numberOfSweeps < 10; numberOfSweeps += 2) {
//
//			// Initialise the policy and value function
//			Gambler gambler = new Gambler(casino);
//
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow("Initial", state, action, numberOfSweeps);
//					}
//				}
//			}
//
//			gpi.iterate(gambler, numberOfSweeps);
//
//			for (State state : casino.getStates()) {
//				if (state.getType() == State.Type.NonTerminal) {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow("Final", state, action, numberOfSweeps);
//					}
//				}
//			}
//		}
//
//		results.writeRows(writer);
		
	}

	public static void oddAndEven() {
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("OddAndEven.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Target Dollars", "State", "Action"});
//		}
//		results.writeHeaders(writer);

		int targetDollars = 99;
		varyTargetDollars(targetDollars, probabilityHeads, theta, discount);

		targetDollars = 100;
		varyTargetDollars(targetDollars, probabilityHeads, theta, discount);

	}

	public static void oddAndEven78() {
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("OddAndEven78.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Target Dollars", "State", "Action"});
//		}
//		results.writeHeaders(writer);

		int targetDollars = 7;
		varyTargetDollars(targetDollars, probabilityHeads, theta, discount);

		targetDollars = 8;
		varyTargetDollars(targetDollars, probabilityHeads, theta, discount);

	}

	public static void powersOfTwo() {
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("PowersOfTwo.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Target Dollars", "State", "Value"});
//		}
//		results.writeHeaders(writer);

		for (int targetDollars = 4; targetDollars < 24; targetDollars += 1) {
			varyTargetDollars(targetDollars, probabilityHeads, theta, discount);
		}


	}

	public static void varyTargetDollars(final int targetDollarsEven,
			final double probabilityHeads, double theta, double discount) {
		// Initialise the MDP
		Casino casino = new Casino(targetDollarsEven, probabilityHeads);

		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);

		PolicyIterator pi = new PolicyIterator(casino, theta, discount);

		pi.iterate(gambler);

//		for (State state : casino.getStates()) {
//			if (state.getType() == State.Type.NonTerminal) {
//				for (Action action : gambler.getPolicy().getActions(state)) {
//					results.addRow(targetDollarsEven, state, action);
//				}
//			}
//		}
//
//		results.writeRows(writer);
	}
	
}
