package ie.nix.rl_java.mdp;

public interface FiniteMDP {

	enum Type {
		StrictlyMarkov,
		NonMarkov
	}
	
	public State newState();
	
	public State newState(State.Type type);
	
	public Action newAction();
	
	public int getNumberOfStates();

	public int getNumberOfActions();

	public RewardFunction getRewardFunction();
	
	public void setRewardFunction(RewardFunction rewardFunction);
	
	public State[] getStates();
	
	public Action[] getActions();

	public TransitionFunction getStateTransitionFunction();
	
	public void setStateTransitionFunction(TransitionFunction stf);
	
	public StateActionFunction getStateActionFunction();
	
	public void setStateActionFunction(StateActionFunction stateActionFunction);
}