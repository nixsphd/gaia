package ie.nix.rl_java.mc;

import ie.nix.rl_java.Episode;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.Policy;

import java.util.ArrayList;

public class OffPolicyIterator  {

	protected PolicyImprover pi;

	public OffPolicyIterator(TabularFiniteMDP environment, double theta, double discount) {
		this(new PolicyImprover(environment, theta, discount));
	}
	
	public OffPolicyIterator(PolicyImprover pi) {
		this.pi = pi;
	}


	public boolean iterate(Policy behaviourPolicy, Policy estimationPolicy, WeightedAverageValueFunction valueFunction, Episode[] episodes) {
		System.out.println("ERROR OffPolicyIterator.iterate(Policy, ValueFunction, Episode[]) NOT IMPLEMENTED");
		return false;
	}
	
	public boolean iterate(Policy behaviourPolicy, Policy estimationPolicy,
			WeightedAverageActionValueFunction actionValueFunction, Episode[] episodes) {
		if (behaviourPolicy.isSoft()) {
			System.err.println("ERROR OffPolicyIterator.iterate() behaviourPolicy is soft.");
		}
		if (!estimationPolicy.isSoft()) {
			System.err.println("ERROR OffPolicyIterator.iterate() estimationPolicy is greedy.");
		}

		boolean policyUpdated = false;
		double reward = 0;

		for (int episodeNumber = 0; episodeNumber < episodes.length; episodeNumber++) {
			// Evaluate the policy
			ArrayList<StateAction> visitedStateActions = new ArrayList<StateAction>();

			reward = episodes[episodeNumber].runRecordStateActions(estimationPolicy, visitedStateActions);

			evaluate(behaviourPolicy, estimationPolicy, actionValueFunction, reward, visitedStateActions);

			// Improve the policy
			if (pi.improve(behaviourPolicy, actionValueFunction, visitedStateActions)) {
				policyUpdated = true;
				System.out.println("NDB::GeneralisedPolicyIterator.iterate()~policyUpdated="+policyUpdated);
			}

		}
		return policyUpdated;
	}

	public void evaluate(Policy behaviourPolicy, Policy estimationPolicy, WeightedAverageActionValueFunction actionValueFunction,
			double reward, ArrayList<StateAction> visitedStateActions) {
		int tau = visitedStateActions.size();
		for (int t = 0; t < visitedStateActions.size(); t++) {
			StateAction visitedStateAction = visitedStateActions.get(t);
			System.out.println("NDB::GeneralisedPolicyIterator.iterate()~state="+visitedStateAction.getState()+", behaviour="+behaviourPolicy.selectAction(visitedStateAction.getState()));
		 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~state="+visitedStateAction.getState()+", actual="+visitedStateAction.getAction());
			if (behaviourPolicy.selectAction(visitedStateAction.getState()).getId() != visitedStateAction.getAction().getId()) {
			 	tau = t;
			 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~tau="+tau);
			}
		}

		System.out.println("NDB::GeneralisedPolicyIterator.iterate()~tau="+tau+" means start at "+visitedStateActions.get(tau)+
				" to "+visitedStateActions.get(visitedStateActions.size() - 1));

		double weight = 1;
		for (int k = visitedStateActions.size() - 1; k >= tau; k--) {
			StateAction visitedStateAction = visitedStateActions.get(k);
		 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~estimationPolicy.getProbability("+visitedStateAction+")="+estimationPolicy.getProbability(visitedStateAction));
			weight *= 1.0/estimationPolicy.getProbability(visitedStateAction);

		 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~"
		 			+ "visitedStateAction="+visitedStateAction+", weight="+weight);
			actionValueFunction.updateValue(visitedStateAction, reward * weight);

		}
	}

	public boolean iterate(Policy behaviourPolicy, Policy estimationPolicy,
			ActionValueFunction actionValueFunction, Episode[] episodes, int numberOfIterations) {
		if (!behaviourPolicy.isSoft()) {
			System.err.println("ERROR OffPolicyIterator.iterate() behaviourPolicy is not greedy.");
		}
		if (estimationPolicy.isSoft()) {
			System.err.println("ERROR OffPolicyIterator.iterate() estimationPolicy is not soft.");
		}
		return false;

	}

}
