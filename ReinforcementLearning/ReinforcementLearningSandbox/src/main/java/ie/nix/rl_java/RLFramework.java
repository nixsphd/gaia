package ie.nix.rl_java;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.policy.ValueFunction;

public class RLFramework {
	
	public static void main(String[] args) {

		
	}
	
	public static String toString(Action[] actions) {
		StringBuilder string = new StringBuilder("[");
		if (actions.length > 0) {
	    	for (int s = 0; s < actions.length; s++) {
	    		string.append(actions[s].getId()+", ");
	    	}
			string.replace(string.length()-2, string.length(),"");
		}
		string.append("]");
    	return string.toString();
	}

	public static String toString(State[] states) {
		StringBuilder string = new StringBuilder("[");
		if (states.length > 0) {
	    	for (int s = 0; s < states.length; s++) {
	    		string.append(states[s].getId()+", ");
	    	}
			string.replace(string.length()-2, string.length(),"");
		}
		string.append("]");
    	return string.toString();
	}
	
	public static String toString(StateAction[] stateActions) {
		StringBuilder string = new StringBuilder("[");
		if (stateActions.length > 0) {
	    	for (int s = 0; s < stateActions.length; s++) {
	    		string.append("("+stateActions[s].getState().getId()+", "+stateActions[s].getAction().getId()+"), ");
	    	}
			string.replace(string.length()-2, string.length(),"");
		}
		string.append("]");
    	return string.toString();
	}

	public static String toString(ValueFunction vf) {
		StringBuilder string = new StringBuilder("[");
    	for (double value : vf.getValues()) {
    		string.append(String.format("%.2f", value)+", ");
    	}
		string.replace(string.length()-2, string.length(),"]");
    	return string.toString();
	}

}
