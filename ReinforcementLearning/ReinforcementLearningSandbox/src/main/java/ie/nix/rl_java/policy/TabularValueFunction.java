package ie.nix.rl_java.policy;

import ie.nix.rl_java.mdp.State;

public class TabularValueFunction implements ValueFunction {

	protected double[] values;
	
	public TabularValueFunction(int numberOfStates) {
		this.values = new double[numberOfStates];
		
	}
	
	public double getValue(State state) {
		return values[state.getId()];
		
	}
	public int getNumberOfValues() {
		return values.length;
	}

	public void updateValue(State state, double value) {
		values[state.getId()] = value;
		
	}

	public double[] getValues() {
		return values;
	}

	@Override
	public TabularValueFunction clone() {
		TabularValueFunction newTabularValueFunction = new TabularValueFunction(this.values.length);
		System.arraycopy(this.values, 0, newTabularValueFunction.values, 0, this.values.length);
		return newTabularValueFunction;
		
	}
	
}
