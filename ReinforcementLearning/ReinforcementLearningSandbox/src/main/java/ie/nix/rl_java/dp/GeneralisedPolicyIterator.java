package ie.nix.rl_java.dp;

import ie.nix.rl_java.Agent;
import ie.nix.rl_java.mdp.FiniteMDP;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.ValueFunction;

public class GeneralisedPolicyIterator  {
	
	protected PolicyEvaluator pe;
	protected PolicyImprover pi;

	public GeneralisedPolicyIterator(TabularFiniteMDP environment, double theta, double discount) {
		this(environment, 
				new PolicyEvaluator(environment, theta, discount), 
				new PolicyImprover(environment, discount));
	}
	
	public GeneralisedPolicyIterator(FiniteMDP environment, PolicyEvaluator pe, PolicyImprover pi) {
		this.pe = pe;
		this.pi = pi;
	}

	public void iterate(Agent agent, int numberOfSweeps) {
		iterate(agent.getPolicy(), agent.getValueFunction(), numberOfSweeps);
		
	}
	
	public void iterate(Agent agent, int numberOfIterations, int numberOfSweeps) {
		iterate(agent.getPolicy(), agent.getValueFunction(), numberOfSweeps, numberOfIterations);
		
	}
	
	public boolean iterate(Policy policy, ValueFunction valueFunction, int numberOfSweeps) {
		boolean policyUpdated = true;
		do {
			// Evaluate the policy
			pe.evaluate(policy, valueFunction, numberOfSweeps);
			
			// Improve the policy
			policyUpdated = pi.improve(policy, valueFunction);
			
		} while (policyUpdated);
		
		return !policyUpdated;
		
	}
	
	public boolean iterate(Policy policy, ValueFunction valueFunction, int numberOfSweeps, int numberOfIterations) {
		boolean policyUpdated = true;
		do {
			// Evaluate the policy
			pe.evaluate(policy, valueFunction, numberOfSweeps);
			
			// Improve the policy
			policyUpdated = pi.improve(policy, valueFunction);
			
			numberOfIterations--;
			
		} while ((policyUpdated) && (numberOfIterations > 0));
		
		return !policyUpdated;
	}

}
