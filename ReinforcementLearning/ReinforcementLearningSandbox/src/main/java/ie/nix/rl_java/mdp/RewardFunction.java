package ie.nix.rl_java.mdp;

public interface RewardFunction {
	
	public enum Type {
		Eposodic,
		Continuing
	};

	// TODO - used by DP, but MC one wuld do?
	public double getReward(State initialState, Action actionTaken, State finalState);

	
}
