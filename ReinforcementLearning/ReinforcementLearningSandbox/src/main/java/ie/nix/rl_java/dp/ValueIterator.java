package ie.nix.rl_java.dp;

import ie.nix.rl_java.Agent;
import ie.nix.rl_java.policy.ValueFunction;
import ie.nix.rl_java.mdp.FiniteMDP;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy;

public class ValueIterator {
	
	protected PolicyEvaluator pe;
	protected PolicyImprover pi;

	public ValueIterator(TabularFiniteMDP environment, double theta, double discount) {
		this(environment, 
				new PolicyEvaluator(environment, theta, discount), 
				new PolicyImprover(environment, discount));
	}
	
	public ValueIterator(FiniteMDP environment, PolicyEvaluator pe, PolicyImprover pi) {
		this.pe = pe;
		this.pi = pi;
	}
	
	public boolean iterate(Agent agent) {
		return iterate(agent.getPolicy(), agent.getValueFunction());
		
	}	
	
	public boolean iterate(Agent agent, int numberOfIterations) {
		return iterate(agent.getPolicy(), agent.getValueFunction(), numberOfIterations);
		
	}

	public boolean iterate(Policy policy, ValueFunction valueFunction) {
		boolean stablePolicy = false;
		do {
			// Evaluate the policy
			pe.evaluate(policy, valueFunction, 1);
			
			// Improve the policy
			stablePolicy = pi.improve(policy, valueFunction);
			
		} while (!stablePolicy);
		
		return stablePolicy;
		
	}
	
	public boolean iterate(Policy policy, ValueFunction valueFunction, int numberOfIterations) {
		boolean policyUpdated;
		do {
			// Evaluate the policy
			pe.evaluate(policy, valueFunction, 1);
			
			// Improve the policy
			policyUpdated = pi.improve(policy, valueFunction);
			
			numberOfIterations--;
			
		} while ((policyUpdated) && (numberOfIterations > 0));
		
		return !policyUpdated;
	}

}
