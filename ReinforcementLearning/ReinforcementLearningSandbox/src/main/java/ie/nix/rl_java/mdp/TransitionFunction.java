package ie.nix.rl_java.mdp;

public interface TransitionFunction {

	public State getNextState(State givenState, Action givenAction);
	
	public double probabilityOf(State movingToState, State givenState, Action givenAction);
	
}
