package ie.nix.rl_java.mc;

import ie.nix.rl_java.Episode;
import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.ValueFunction;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.policy.Policy;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;

public class PolicyEvaluator {
	
	public enum Type {
		FirstVisit,
		EveryVisit
	}
	
	protected double theta;
	protected double discount;
	protected Type type;

	public PolicyEvaluator(double theta, double discount) {
		this(theta, discount, Type.FirstVisit);
		
	}
	
	public PolicyEvaluator(double theta, double discount, Type type) {
		this.theta = theta;
		this.discount = discount;
		this.type = type;
		
	}
	
	public double evaluate(Policy policy, ActionValueFunction actionValueFunction, Episode episode, ArrayList<StateAction> vistedStateActions) {
		double delta = 0;
		double reward = Double.NaN; 
		
		reward = episode.runRecordStateActions(policy, vistedStateActions);

		if (type == Type.FirstVisit) {
			for (StateAction stateAction : new HashSet<StateAction>(vistedStateActions)) {
				double tempValue = actionValueFunction.getValue(stateAction);
				actionValueFunction.updateValue(stateAction, reward);
				delta = Math.max(delta, Math.abs(tempValue - actionValueFunction.getValue(stateAction)));

//			 	System.out.println("NDB::PolicyEvaluator.evaluate()~reward="+reward+", tempValue="+tempValue+
//			 			", new value="+actionValueFunction.getValue(stateAction)+", delta="+delta );
			
			}
		} else {
			for (StateAction stateAction : vistedStateActions) {
				double tempValue = actionValueFunction.getValue(stateAction);
				actionValueFunction.updateValue(stateAction, reward);
				delta = Math.max(delta, Math.abs(tempValue - actionValueFunction.getValue(stateAction)));
				
//			 	System.out.println("NDB::PolicyEvaluator.evaluate()~reward="+reward+", tempValue="+tempValue+
//			 			", new value="+actionValueFunction.getValue(stateAction)+", delta="+delta );
		
			
			}
		}
		return delta;

	}
	
	public double evaluate(Policy policy, ActionValueFunction actionValueFunction, Episode[] episodes, ArrayList<StateAction> vistedStateActions) {

		double delta = 0;
		double reward = Double.NaN; 
		
		ArrayList<StateAction> vistedStateActionsForEpisode = new ArrayList<StateAction>();
		
		for (Episode episode : episodes) {
			
			vistedStateActionsForEpisode.clear();
			reward = episode.runRecordStateActions(policy, vistedStateActionsForEpisode);

			if (type == Type.FirstVisit) {

				for (StateAction stateAction : new LinkedHashSet<StateAction>(vistedStateActionsForEpisode)) {

					double tempValue = actionValueFunction.getValue(stateAction);
					actionValueFunction.updateValue(stateAction, reward);
					delta = Math.max(delta, Math.abs(tempValue - actionValueFunction.getValue(stateAction)));

					vistedStateActions.add(stateAction);
				}

			} else {

				for (StateAction stateAction : vistedStateActionsForEpisode) {

					double tempValue = actionValueFunction.getValue(stateAction);
					actionValueFunction.updateValue(stateAction, reward);
					delta = Math.max(delta, Math.abs(tempValue - actionValueFunction.getValue(stateAction)));

					vistedStateActions.add(stateAction);
				}
			}
		}
		return delta;

	}

	public double evaluate(Policy policy, ValueFunction valueFunction, Episode episode, ArrayList<State> vistedStates) {
		double delta = 0;
		double reward = Double.NaN; 

		reward = episode.runRecordStates(policy, vistedStates);

		if (type == Type.FirstVisit) {
			for (State state : new HashSet<State>(vistedStates)) {
				double tempValue = valueFunction.getValue(state);
				valueFunction.updateValue(state, reward);
				delta = Math.max(delta, Math.abs(tempValue - valueFunction.getValue(state)));

//			 	System.out.println("NDB::PolicyEvaluator.evaluate()~reward="+reward+"tempValue="+tempValue+
//			 			", new value="+valueFunction.getValue(state)+", delta="+delta );
			 	
			}
		} else {
			for (State state : vistedStates) {
				double tempValue = valueFunction.getValue(state);
				valueFunction.updateValue(state, reward);
				delta = Math.max(delta, Math.abs(tempValue - valueFunction.getValue(state)));

//			 	System.out.println("NDB::PolicyEvaluator.evaluate()~reward="+reward+"tempValue="+tempValue+
//			 			", new value="+valueFunction.getValue(state)+", delta="+delta );
			 	
			}
		}
		return delta;

	}
	
	public double evaluate(Policy policy, ValueFunction valueFunction, Episode[] episodes, ArrayList<State> vistedStates) {
		double delta = 0;
		double reward = Double.NaN; 

		for (Episode episode : episodes) {
			reward = episode.runRecordStates(policy, vistedStates);
		}
		
		if (type == Type.FirstVisit) {
			for (State state : new HashSet<State>(vistedStates)) {
				double tempValue = valueFunction.getValue(state);
				valueFunction.updateValue(state, reward);
				delta = Math.max(delta, Math.abs(tempValue - valueFunction.getValue(state)));

//			 	System.out.println("NDB::PolicyEvaluator.evaluate()~reward="+reward+"tempValue="+tempValue+
//			 			", new value="+valueFunction.getValue(state)+", delta="+delta );
			 	
			}
		} else {
			for (State state : vistedStates) {
				double tempValue = valueFunction.getValue(state);
				valueFunction.updateValue(state, reward);
				delta = Math.max(delta, Math.abs(tempValue - valueFunction.getValue(state)));

//			 	System.out.println("NDB::PolicyEvaluator.evaluate()~reward="+reward+"tempValue="+tempValue+
//			 			", new value="+valueFunction.getValue(state)+", delta="+delta );
			 	
			}
		}
		return delta;

	}
	
}
