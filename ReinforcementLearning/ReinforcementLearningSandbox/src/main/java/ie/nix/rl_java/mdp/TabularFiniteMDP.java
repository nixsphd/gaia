package ie.nix.rl_java.mdp;

import java.util.Arrays;

import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.TabularActionValueFunction;
import ie.nix.rl_java.policy.TabularPolicy;
import ie.nix.rl_java.policy.TabularValueFunction;
import ie.nix.rl_java.policy.ValueFunction;

public class TabularFiniteMDP implements FiniteMDP {

	protected int numberOfStates = 0;
	protected int numberOfActions = 0;
	protected RewardFunction rewardFunction;
	protected State[] states;
	protected Action[] actions;
	protected StateActionFunction stateActionFunction;
	protected TransitionFunction stateTransitionFunction;
	
	public TabularFiniteMDP(int numberOfStates, int numberOfActions) {
		this.states = new State[numberOfStates];
		this.actions = new Action[numberOfActions];
	}

	public State newState() {
		return newState(State.Type.NonTerminal);
	}	
	
	public State newState(State.Type type) {
		State newState = new State(numberOfStates, type);
		addState(newState);
		return newState;
	}
	
	public void addState(State state) {
		this.states[numberOfStates] = state;
		numberOfStates++;
		
	}
	
	public void updateState(State state, int stateID) {
		this.states[stateID] = state;
		
	}
	
	public Action newAction() {
		Action newAction = new Action(numberOfActions);
		addAction(newAction);
		return newAction;
	}
	
	public void addAction(Action action) {
		this.actions[numberOfActions] = action;
		numberOfActions++;
		
	}
	
	public int getNumberOfStates() {
		return numberOfStates;
	}

	public int getNumberOfActions() {
		return numberOfActions;
	}

	public RewardFunction getRewardFunction() {
		return rewardFunction;
	}
	
	public void setRewardFunction(RewardFunction rewardFunction) {
		this.rewardFunction = rewardFunction;
	}
	
	public State[] getStates() {
		return states;
	}
	
	public Action[] getActions() {
		return actions;
	}

	public TransitionFunction getStateTransitionFunction() {
		return stateTransitionFunction;
	}
	
	public void setStateTransitionFunction(TransitionFunction stf) {
		this.stateTransitionFunction = stf;
	}
	
	public StateActionFunction getStateActionFunction() {
		return stateActionFunction;
	}
	
	public void setStateActionFunction(StateActionFunction stateActionFunction) {
		this.stateActionFunction = stateActionFunction;
	}
	
	public Policy createEquiprobablePolicy() {
		Policy policy = new TabularPolicy(this, Policy.Type.Equiprobable);
		for (State state : getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				Action[] possibleActions = getStateActionFunction().getActions(state);
				policy.updateActions(state, possibleActions);

			}

		}
		return policy;
		
	}
	public Policy createEquiprobableSoftPolicy() {
		Policy policy = new TabularPolicy(this, Policy.Type.Equiprobable, 0.1);
		for (State state : getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				Action[] possibleActions = getStateActionFunction().getActions(state);
				policy.updateActions(state, possibleActions);

			}

		}
		return policy;
		
	}
	
	public Policy createEquiprobableStochasticPolicy() {
		Policy policy = new TabularPolicy(this, Policy.Type.Stochastic);
		for (State state : getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				Action[] possibleActions = getStateActionFunction().getActions(state);
				double[] probabilities = new double[possibleActions.length];
				Arrays.fill(probabilities, 1.0/probabilities.length);
				policy.updateActions(state, possibleActions, probabilities);

			}

		}
		return policy;
		
	}

	public ValueFunction createValueFunction() {
		// Initialise the value function
		return new TabularValueFunction(getStates().length);
		
	}
	
	public ActionValueFunction createActionValueFunction() {
		// Initialise the value function
		return new TabularActionValueFunction(getStates().length, getActions().length);
		
	}
	
	// TODO - Generate Transition graph.
	
}
