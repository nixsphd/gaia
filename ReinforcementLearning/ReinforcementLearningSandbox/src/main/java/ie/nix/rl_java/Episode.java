package ie.nix.rl_java;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.Policy;

import java.util.ArrayList;

public class Episode {

	protected TabularFiniteMDP enviornment;
	protected State intialState;
	protected Action initialAction;
	
	public Episode(TabularFiniteMDP enviornment, State intialState, Action initialAction) {
		this.enviornment = enviornment;
		this.intialState = intialState;
		this.initialAction = initialAction;
	}

	public State getInitialState() {
		return intialState;
	}

	public Action getInitialAction() {
		return initialAction;
	}

	public double run(Policy policy) {
		return run(policy, null, null);
	}
	
	public double runRecordStates(Policy policy, ArrayList<State> vistedStates) {
		return run(policy, vistedStates, null);
	}

	public double runRecordStateActions(Policy policy, ArrayList<StateAction> vistedStateActions) {
		return run(policy, null, vistedStateActions);
		
	}
	
	public double run(Policy policy, ArrayList<State> vistedStates, ArrayList<StateAction> vistedStateActions) {

		State oldState = null;
		State state = intialState;
		Action action = initialAction;
		double reward = 0;
		
		// While state is not a terminal state and it's different from the last state
		while ((state.getType() != State.Type.Terminal) && (oldState != state)) {

			// Get the reward
			if (oldState != null) {
				reward += enviornment.getRewardFunction().getReward(oldState, action, state);
			}
			
			// Record this State action as visited.
			if (vistedStates != null) {
				vistedStates.add(state);
//		    	System.out.println("NDB::runRecordStateActions()~Visited state="+state);
			}
			if (vistedStateActions != null) {
				vistedStateActions.add(new StateAction(state, action));
//		    	System.out.println("NDB::runRecordStateActions()~Visited state="+state+", action="+action);
			}

			// Get the new state
			oldState = state;
			state = enviornment.getStateTransitionFunction().getNextState(state, action);
			
			// Agent selects and action based on it's policy
			if (state.getType() != State.Type.Terminal) {
				action = policy.selectAction(state);
//				System.out.println("NDB::runRecordStateActions()~Next state="+state+", next action="+action);
			}
			
		}

		// Get the reward
		reward += enviornment.getRewardFunction().getReward(oldState, action, state);
		
		// Do something with the reward...
		return reward;
	}
	
}