package ie.nix.rl_java.mc;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.policy.TabularActionValueFunction;

public class SampleAverageActionValueFunction extends TabularActionValueFunction {

	int[][] numberOfSamples;
	
	public SampleAverageActionValueFunction(int numberOfStates, int numberOfActions) {
		super(numberOfStates, numberOfActions);
		this.numberOfSamples = new int[numberOfStates][numberOfActions];
		
	}
	
	protected SampleAverageActionValueFunction(double[][] values, int[][] numberOfSamples) {
		super(values);
		this.numberOfSamples = numberOfSamples;
		    
	}
	
	public void updateValue(State forState, Action withAction, double value) {
		numberOfSamples[forState.getId()][withAction.getId()] += 1;
//	 	System.out.println("NDB::SampleAverageActionValueFunction.updateValue()~numberOfSamples["+forState.getId()+"]["+withAction.getId()+
//	 			"]="+numberOfSamples[forState.getId()][withAction.getId()]);
//		values[forState.getId()][withAction.getId()] += value;
		values[forState.getId()][withAction.getId()] += (value - values[forState.getId()][withAction.getId()]) / numberOfSamples[forState.getId()][withAction.getId()];
		
	}
	
	public void updateValue(StateAction stateAction, double value) {
		updateValue(stateAction.getState(), stateAction.getAction(), value);
		
	}
	
//	public double getValue(State state, Action action) {
//		if (numberOfSamples[state.getId()][action.getId()] != 0) {
//			return values[state.getId()][action.getId()]/numberOfSamples[state.getId()][action.getId()];
//		} else {
//			return 0;
//		}
//	}

	public int getNumberOfSamples(State state, Action action) {
		return numberOfSamples[state.getId()][action.getId()];
	}

	public int getNumberOfSamples(StateAction stateAction) {
		return numberOfSamples[stateAction.getState().getId()][stateAction.getAction().getId()];
	}

}
