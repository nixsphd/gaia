package ie.nix.rl_java.mc;

import ie.nix.util_java.Equals;

import ie.nix.rl_java.Agent;
import ie.nix.rl_java.Episode;
import ie.nix.rl_java.mdp.FiniteMDP;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.Policy;
import ie.nix.rl_java.policy.ValueFunction;

import java.util.ArrayList;

public class GeneralisedPolicyIterator  {

	protected PolicyEvaluator pe;
	protected PolicyImprover pi;
	protected double theta;

	public GeneralisedPolicyIterator(TabularFiniteMDP environment, double theta, double discount) {
		this(environment, 
				new PolicyEvaluator(theta, discount), 
				new PolicyImprover(environment, theta, discount),
				theta);
	}
	
	public GeneralisedPolicyIterator(FiniteMDP environment, PolicyEvaluator pe, PolicyImprover pi, double theta) {
		this.pe = pe;
		this.pi = pi;
		this.theta = theta;
	}

	public boolean iterate(Policy policy, ValueFunction valueFunction, Episode[] episodes) {
		System.out.println("ERROR GeneralisedPolicyIterator.iterate(Policy, ValueFunction, Episode[]) NOT IMPLEMENTED");
		return false;
	}
	
	public boolean iterate(Agent agent, Episode[] episodes) {
		return iterate(agent.getPolicy(), agent.getActionValueFunction(), episodes);
		
	}
	
	public boolean iterate(Agent agent, Episode[] episodes, int numberOfIterations) {
		return iterate(agent.getPolicy(), agent.getActionValueFunction(), episodes, numberOfIterations);
		
	}
	
	public boolean iterate(Policy policy, ActionValueFunction actionValueFunction, Episode[] episodes) {
		boolean policyUpdated = false;
		double totalDelta = 0;
		do {
//		 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~iteration="+iteration);
			policyUpdated = false;
			totalDelta = 0;
			for (int episodeNumber = 0; episodeNumber < episodes.length; episodeNumber++) {
				// Evaluate the policy
				ArrayList<StateAction> visitedStateActions = new ArrayList<StateAction> ();
				//double delta = 
				totalDelta += pe.evaluate(policy, actionValueFunction, episodes[episodeNumber], visitedStateActions);
				
				// Improve the policy
				if (pi.improve(policy, actionValueFunction, visitedStateActions)) {
					policyUpdated = true;
//				 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~policyUpdated="+policyUpdated);
				}
				
			}
//		 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~iteration="+iteration+", totalDelta="+totalDelta+", policyUpdated="+policyUpdated);
		 	
		} while (policyUpdated || Equals.greaterThan(totalDelta, 0.0, theta));
		
		return (!policyUpdated) && (Equals.equals(totalDelta, 0.0, theta));
		
	}

	public boolean iterate(Policy policy, ActionValueFunction actionValueFunction, Episode[] episodes, int numberOfIterations) {
		boolean policyUpdated = false;
		double totalDelta = 0;
		do {
//		 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~iteration="+iteration);
			policyUpdated = false;
			totalDelta = 0;
			for (int episodeNumber = 0; episodeNumber < episodes.length; episodeNumber++) {
				// Evaluate the policy
				ArrayList<StateAction> visitedStateActions = new ArrayList<StateAction> ();
				//double delta = 
				totalDelta += pe.evaluate(policy, actionValueFunction, episodes[episodeNumber], visitedStateActions);
//				pe.evaluate(policy, actionValueFunction, episodes[episodeNumber], visitedStateActions);
				
				// Improve the policy
				if (pi.improve(policy, actionValueFunction, visitedStateActions)) {
					policyUpdated = true;
//				 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~policyUpdated="+policyUpdated);
				}
				
			}
		 	System.out.println("NDB::GeneralisedPolicyIterator.iterate()~theta="+theta+", numberOfIterations="+numberOfIterations+
		 			", totalDelta="+totalDelta+", policyUpdated="+policyUpdated);

			numberOfIterations--;
		 	
		} while (numberOfIterations > 0 && Equals.greaterThan(totalDelta, 0.0, theta));

		return (!policyUpdated && Equals.equals(totalDelta, 0.0, theta));
		
		
	}

}
