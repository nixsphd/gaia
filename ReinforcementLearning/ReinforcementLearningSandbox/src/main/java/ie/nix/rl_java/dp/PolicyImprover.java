package ie.nix.rl_java.dp;

import ie.nix.util_java.Equals;
import java.util.ArrayList;
import java.util.Arrays;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.policy.ActionValueFunction;
import ie.nix.rl_java.policy.ValueFunction;
import ie.nix.rl_java.policy.Policy;

public class PolicyImprover {

	protected TabularFiniteMDP environment;
	protected double discount;
	protected double delta = 0.0000001;
	
	public PolicyImprover(TabularFiniteMDP environment, double discount) {
		this.environment = environment;
		this.discount = discount;
		
	}
	
	public boolean improve(Policy policy, ValueFunction valueFunction) {
		boolean policyUpdated = false;
		for (State state : environment.getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				if (policy.getType() == Policy.Type.Deterministic) {
					Action newAction = getBestAction(state, valueFunction);
					if (policy.getAction(state) != newAction) {
						policy.updateAction(state, newAction); 
						policyUpdated = true;
						
					}
				} else if (policy.getType() == Policy.Type.Equiprobable) {
					Action[] newActions = getBestActions(state, valueFunction);
					if (!Arrays.equals(policy.getActions(state), newActions)) {
						policy.updateActions(state, newActions); 
						policyUpdated = true;
						
					}
					
				} else if (policy.getType() == Policy.Type.Stochastic) {
					Action[] newActions = getBestActions(state, valueFunction);
					
					if (!Arrays.equals(policy.getActions(state), newActions)) {
						policy.updateActions(state, newActions); 
						policyUpdated = true;
						
					}
					
				} 
			}
		}
		return policyUpdated;
			
	}
	
	public boolean improve(Policy policy, ActionValueFunction actionValueFunction) {
		boolean policyUpdated = false;
		for (State state : environment.getStates()) {
			if (state.getType() == State.Type.NonTerminal) {
				if (policy.getType() == Policy.Type.Deterministic) {
					Action newAction = getBestAction(state, actionValueFunction);
					if (policy.getAction(state) != newAction) {
						policy.updateAction(state, newAction); 
						policyUpdated = true;
						
					}
				} else if (policy.getType() == Policy.Type.Equiprobable) {
//					Action[] newActions = getBestActions(state, actionValueFunction);
//					if (!Arrays.equals(policy.getActions(state), newActions)) {
//						policy.updateActions(state, newActions); 
//						policyUpdated = true;
//						
//					}
//					
				} else if (policy.getType() == Policy.Type.Stochastic) {
//					Action[] newActions = getBestActions(state, actionValueFunction);
//					
//					if (!Arrays.equals(policy.getActions(state), newActions)) {
//						policy.updateActions(state, newActions); 
//						policyUpdated = true;
//						
//					}
					
				} 
			}
		}
		return policyUpdated;
			
	}

	protected Action getBestAction(State forState, ValueFunction givenValueFunction) {
		
		// Get the actions for state s
		Action[] actions = environment.getStateActionFunction().getActions(forState);
	
		// Get the action values
		double[] actionValues = getActionValues(forState, givenValueFunction, actions);
		
		// Pick the best action based on the action values
		Action bestAction = null; 
		double bestActionValue = Double.NEGATIVE_INFINITY;
		for (int a = 1; a < actions.length; a++) {
			if (bestActionValue < actionValues[a]) {
				bestActionValue = actionValues[a];
				bestAction = actions[a];
			}
		}
		return bestAction;
	}

	protected Action getBestAction(State forState, ActionValueFunction actionValueFunction) {
		// Get the actions for state s
		Action[] actions = environment.getStateActionFunction().getActions(forState);
	
		// Get the action values
		double[] actionValues = getActionValues(forState, actionValueFunction, actions);
		
		// Pick the best action based on the action values
		Action bestAction = null; 
		double bestActionValue = Double.NEGATIVE_INFINITY;
		for (int a = 1; a < actions.length; a++) {
			if (bestActionValue < actionValues[a]) {
				bestActionValue = actionValues[a];
				bestAction = actions[a];
			}
		}
		return bestAction;
		
	}
	
	protected double[] getActionValues(State forState,
			ValueFunction givenValueFunction, Action[] forActions) {
		// Get all the action values for the possible actions.
		double[] actionValues = new double[forActions.length];
		for (int a = 0; a < forActions.length; a++) {
			Action action = forActions[a];
			for (State nextState : environment.getStates()) {
				double prob = environment.getStateTransitionFunction().probabilityOf(nextState, forState, action);
				if (prob != 0.0) {
					double reward = environment.getRewardFunction().getReward(forState, action, nextState);
					actionValues[a] += prob * (reward + (discount * givenValueFunction.getValue(nextState)));
				}
			}
		}
		return actionValues;
	}


	protected double[] getActionValues(State forState, ActionValueFunction givenActionValueFunction, Action[] forActions) {
		// Get all the action values for the possible actions.
		double[] actionValues = new double[forActions.length];
		for (int a = 0; a < forActions.length; a++) {
			Action action = forActions[a];
			givenActionValueFunction.getValue(forState, action);
		}
		return actionValues;
	}

	protected Action[] getBestActions(State forState, ValueFunction givenValueFunction) {
		
		// Get the actions for state s
		Action[] actions = environment.getStateActionFunction().getActions(forState);
	
		// Get the action values
		double[] actionValues = getActionValues(forState, givenValueFunction, actions);
		
		// Pick the best action based on the action values
		ArrayList<Action> bestActions = new ArrayList<Action>(actions.length);
		double bestActionValue = Double.NEGATIVE_INFINITY;
		for (int a = 0; a < actions.length; a++) {
//			if (((float)bestActionValue) < ((float)actionValues[a])) {
			if (Equals.lessThan(bestActionValue, actionValues[a], delta)) {
				bestActionValue = actionValues[a];
				bestActions = new ArrayList<Action>(actions.length);
				bestActions.add(actions[a]);
				
			} else if (Equals.equals(bestActionValue, actionValues[a], delta)) {
				bestActions.add(actions[a]);
				
			}
		}
		return bestActions.toArray(new Action[bestActions.size()]);
		
	}

}
