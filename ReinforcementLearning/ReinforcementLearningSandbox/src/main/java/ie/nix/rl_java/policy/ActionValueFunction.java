package ie.nix.rl_java.policy;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;

public interface ActionValueFunction extends Cloneable {

	// Used by MC
	public double getValue(StateAction stateAction);

	public void updateValue(StateAction stateAction, double value);
	
	// Used by PD
	public double getValue(State state, Action action);

	public void updateValue(State forState, Action withAction, double value);

	public ActionValueFunction clone();

	// TODO - Generate backup diagram
	
}
