package ie.nix.rl_java.gambler;

import ie.nix.rl_java.Episode;
import ie.nix.rl_java.RLFramework;
import ie.nix.rl_java.mc.GeneralisedPolicyIterator;
import ie.nix.rl_java.mc.OffPolicyIterator;
import ie.nix.rl_java.mc.PolicyEvaluator;
import ie.nix.rl_java.mc.PolicyEvaluator.Type;
import ie.nix.rl_java.mc.PolicyImprover;
import ie.nix.rl_java.mc.SampleAverageActionValueFunction;
import ie.nix.rl_java.mc.SampleAverageValueFunction;
import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;
import ie.nix.rl_java.policy.Policy;

import ie.nix.util_java.Randomness;
import java.io.PrintWriter;
import java.util.ArrayList;

public class MCGamblerProblem extends RLFramework {

	public static void main(String[] args) {

//		exploringStarts();
//		randomStarts();
//		
//		valueEstimates();
//		
//		actionValueEstimates();
		
//		actionValueImprovment();
		
//		policyImprovement();

//		generalisedPolicyIteration();
		
//		offPolicy();
		
		System.out.println("Done.");
		
	}

	public static void exploringStarts() {
		final int targetEuros = 100;
		final double probabilityHeads = 0.4;
		
		Randomness.setSeed(0);

		// Initialise the MDP
		Casino casino = new Casino(targetEuros, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		
		Episode[] exploringStarts = getExploringStarts(casino);
		System.out.println("NDB::GamblerProblemWithMC.exploringStarts()~Number of exploring Starts="+exploringStarts.length);
		
//		Episode[] exploringStarts = getRandomStarts(casino, 2599);

		// For each episode
		for (Episode episode : exploringStarts) {
			ArrayList<State> vistedStates = new ArrayList<State>();
			double reward = episode.runRecordStates(gambler.getPolicy(), vistedStates);
//			System.out.println("NDB::GamblerProblemWithMC.exploringStarts()~Initial state="+episode.getInitialState()+
//					", Initial action="+episode.getInitialAction()+
//					", visitingStates="+toString(vistedStates.toArray(new State[vistedStates.size()]))+" -> Reward="+reward);
			
		}

	}
	
	public static void randomStarts() {
		final int targetEuros = 100;
		final double probabilityHeads = 0.4;
		
		Randomness.setSeed(0);

		// Initialise the MDP
		Casino casino = new Casino(targetEuros, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		
		Episode[] randomStarts = getRandomStarts(casino, 2599);
		System.out.println("NDB::GamblerProblemWithMC.randomStarts()~Number of random Starts="+randomStarts.length);

		// For each episode
		for (Episode episode : randomStarts) {
			ArrayList<StateAction> vistedStateActions = new ArrayList<StateAction>();
			double reward = episode.runRecordStateActions(gambler.getPolicy(), vistedStateActions);
//			System.out.println("NDB::GamblerProblemWithMC.randomStarts()~Initial state="+episode.getInitialState()+
//					", Initial action="+episode.getInitialAction()+
//					", visitingStateActionss="+toString(vistedStateActions.toArray(new StateAction[vistedStateActions.size()]))+" -> Reward="+reward);
			
		}

	}

	public static void valueEstimates() {
		final int targetEuros = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;
		
		Randomness.setSeed(0);

		// Initialise the MDP
		Casino casino = new Casino(targetEuros, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		
//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("MCValueEstimates.csv");
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Episode", "State", "Value", "Visits", "Type"});
//		}
//		results.writeHeaders(writer);
		
		// Evaluate the policy
		int numberOfLoops = 50; //100;

		// For each episode
		int episodeNumber = 0;
		
		Episode[] exploringStarts = getExploringStarts(casino);
		
		// Initialise the policy evaluator
		PolicyEvaluator firstVisit = new PolicyEvaluator(theta, discount, Type.FirstVisit);

		SampleAverageValueFunction vf = new SampleAverageValueFunction(casino.getNumberOfStates());
		gambler.setValueFunction(vf);
		

		for (int loop = 0; loop < numberOfLoops; loop++) {
			for (Episode episode : exploringStarts) {
				episodeNumber++;

				ArrayList<State> vistedStateActions = new ArrayList<State>();
				double delta = firstVisit.evaluate(gambler.getPolicy(), gambler.getValueFunction(), episode, vistedStateActions);
//				System.out.println("NDB::GamblerProblemWithMC.valueEstimates()~state="+episode.getInitialState()+
//						", action="+episode.getInitialAction()+", visited ="+visited.length);
				
//				if (episodeNumber%(numberOfLoops*10) == 0) {
//					for (State state : casino.getStates()) {
//						if (state.getType() == State.Type.NonTerminal) {
////							results.addRow(episodeNumber, state, vf.getValue(state), vf.getNumberOfSamples(state), "First Visit");
//						}
//					}
//				}
			}
		}

		// For each episode
		episodeNumber = 0;
				
		// Initialise the policy evaluator
		PolicyEvaluator everyVisit = new PolicyEvaluator(theta, discount, Type.EveryVisit);
		vf = new SampleAverageValueFunction(casino.getNumberOfStates());
		gambler.setValueFunction(vf);

		for (int loop = 0; loop < numberOfLoops; loop++) {
			for (Episode episode : exploringStarts) {
				episodeNumber++;
				
				ArrayList<State> vistedStates = new ArrayList<State>();
				double delta = firstVisit.evaluate(gambler.getPolicy(), gambler.getValueFunction(), episode, vistedStates);
//				System.out.println("NDB::GamblerProblemWithMC.valueEstimates()~state="+episode.getInitialState()+
//						", action="+episode.getInitialAction()+", visited ="+visited.length);
				
//				if (episodeNumber%(numberOfLoops*10) == 0) {
//					for (State state : casino.getStates()) {
//						if (state.getType() == State.Type.NonTerminal) {
////							results.addRow(episodeNumber, state, vf.getValue(state), vf.getNumberOfSamples(state), "Every Visit");
//						}
//					}
//				}
			}
		}
		
//		results.writeRows(writer);
	}
	
	public static void actionValueEstimates() {
		final int targetEuros = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;
		
		Randomness.setSeed(0);

		// Initialise the MDP
		Casino casino = new Casino(targetEuros, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		gambler.setPolicy(gambler.createEquiprobablePolicy(casino));
		
//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("MCActionValueEstimates.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Episode", "State", "Action", "Value", "Visits", "Type"});
//		}
//		results.writeHeaders(writer);
		
		// Evaluate the policy
		int numberOfLoops = 100;

		// For each episode
		int episodeNumber = 0;
		
		// Initialise the policy evaluator
		PolicyEvaluator firstVisit = new PolicyEvaluator(theta, discount, Type.FirstVisit);

		SampleAverageActionValueFunction vavf = new SampleAverageActionValueFunction(casino.getNumberOfStates(), casino.getNumberOfActions());
		gambler.setActionValueFunction(vavf);
		
		Episode[] exploringStarts = getExploringStarts(casino);
		
		for (int loop = 0; loop < numberOfLoops; loop++) {
			for (Episode exploringStart : exploringStarts) {
				episodeNumber++;
				
				ArrayList<StateAction> vistedStateActions = new ArrayList<StateAction>();
				double delta = firstVisit.evaluate(gambler.getPolicy(), gambler.getActionValueFunction(), exploringStart, vistedStateActions);
//				System.out.println("NDB::GamblerProblemWithMC.episode()~state="+intialState+", action="+initialAction);
				
//				if (episodeNumber%(numberOfLoops*targetEuros) == 0) {
//					for (State state : casino.getStates()) {
//						for (Action action : casino.getActions()) {
//							if (state.getType() == State.Type.NonTerminal) {
//								results.addRow(episodeNumber, state, action, vavf.getValue(state, action), vavf.getNumberOfSamples(state, action), "First Visit");
////										System.out.println("NDB::GamblerProblemWithMC.episode()~state="+state+", action="+action+", value="+vavf.getValue(state, action)+", samples="+vavf.getNumberOfSamples(state, action));
//							}
//						}
//					}
//				}
			}
		}

		// For each episode
		episodeNumber = 0;
				
		// Initialise the policy evaluator
		PolicyEvaluator everyVisit = new PolicyEvaluator(theta, discount, Type.EveryVisit);

		vavf = new SampleAverageActionValueFunction(casino.getNumberOfStates(), casino.getNumberOfActions());
		gambler.setActionValueFunction(vavf);
		
		for (int loop = 0; loop < numberOfLoops; loop++) {
			for (Episode exploringStart : exploringStarts) {
				episodeNumber++;

				ArrayList<StateAction> vistedStateActions = new ArrayList<StateAction>();
				double delta = firstVisit.evaluate(gambler.getPolicy(), gambler.getActionValueFunction(), exploringStart, vistedStateActions);
//				System.out.println("NDB::GamblerProblemWithMC.episode()~state="+intialState+", action="+initialAction);
				
//				if (episodeNumber%(numberOfLoops*targetEuros) == 0) {
//					for (State state : casino.getStates()) {
//						for (Action action : casino.getActions()) {
//							if (state.getType() == State.Type.NonTerminal) {
//								results.addRow(episodeNumber, state, action, vavf.getValue(state, action), vavf.getNumberOfSamples(state, action), "Every Visit");
//							}
//						}
//					}
//				}
			}
		}
		
//		results.writeRows(writer);
	}

	public static void actionValueImprovment() {
		final int targetEuros = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;
		
		Randomness.setSeed(0);

		// Initialise the MDP
		Casino casino = new Casino(targetEuros, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		gambler.setPolicy(gambler.createSoftPolicy(casino));
//
//		DataFrame policyResults = new DataFrame();
//		PrintWriter policyWriter = policyResults.initWriter("MC_AVI_Policy.csv");
//		if (policyResults.getNumberOfColumns() == 0) {
//			policyResults.addColumns(new String[]{"Loop", "State", "Action"});
//		}
//		policyResults.writeHeaders(policyWriter);
//
//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("MCActionValueImprovment.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Loop", "State", "Action", "Value", "Visits"});
//		}
//		results.writeHeaders(writer);
		
		// Evaluate the policy
		int numberOfLoops = 11;

		// For each episode
		int episodeNumber = 0;
		
		// Initialise the policy evaluator
		PolicyEvaluator firstVisit = new PolicyEvaluator(theta, discount, Type.FirstVisit);

		SampleAverageActionValueFunction vavf = new SampleAverageActionValueFunction(
				casino.getNumberOfStates(), casino.getNumberOfActions());
		gambler.setActionValueFunction(vavf);

		PolicyImprover pi = new PolicyImprover(casino, theta, discount);
		
		Episode[] exploringStarts = getExploringStarts(casino);

//		int loop = 1;
		for (int loop = 1; loop <= numberOfLoops; loop++) {
			
			ArrayList<StateAction> vistedStateActions = new ArrayList<StateAction>();
			
			for (Episode exploringStart : exploringStarts) {
				episodeNumber++;
				
				double delta = firstVisit.evaluate(gambler.getPolicy(), 
						gambler.getActionValueFunction(), exploringStart, vistedStateActions);
//				System.out.println("NDB::GamblerProblemWithMC.episode()~state="+intialState+", action="+initialAction);
				
			}

//			if (loop%50 == 0) {
				pi.improve(gambler.getPolicy(), gambler.getActionValueFunction(), vistedStateActions);
//			}

			if (loop%(numberOfLoops/10) == 0) {
				for (State state : casino.getStates()) {
					if (state.getType() == State.Type.NonTerminal) {
						Action[] actions = gambler.getPolicy().getActions(state);
//						double[] probabilities = gambler.getPolicy().getProbabilities(state);
//						for (int a = 0; a < actions.length; a++) {
//							policyResults.addRow(loop, state, actions[a]);
//						}
//						for (Action action : casino.getStateActionFunction().getActions(state)) {
//							results.addRow(loop, state, action, vavf.getValue(state, action),
//									vavf.getNumberOfSamples(state, action));
//						}
					}
				}
			}
		}

//		policyResults.writeRows(policyWriter);
//		results.writeRows(writer);
	}
	
	public static void policyImprovement() {
		final int targetEuros = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;

		// Evaluate the policy
		int numberOfLoops = 100;
		
		Randomness.setSeed(0);
		
		// Initialise the MDP
		Casino casino = new Casino(targetEuros, probabilityHeads);
		
		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		gambler.setPolicy(gambler.createEquiprobablePolicy(casino));

		// Initialise the policy evaluator
		PolicyEvaluator pe = new PolicyEvaluator(theta, discount, Type.EveryVisit);
		
		// Set the ActionValueFunction to be the sample average required for episodes. 
		SampleAverageActionValueFunction vavf = new SampleAverageActionValueFunction(
				casino.getNumberOfStates(), casino.getNumberOfActions());
		gambler.setActionValueFunction(vavf);
		
		// Initialise the policy improver
		PolicyImprover pi = new PolicyImprover(casino, theta, discount);

//		DataFrame policyResults = new DataFrame();
//		PrintWriter policyWriter = policyResults.initWriter("MC_PI_Policy.csv");
//		if (policyResults.getNumberOfColumns() == 0) {
//			policyResults.addColumns(new String[]{"Episode", "State", "Action"});
//		}
//		policyResults.writeHeaders(policyWriter);
//
//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("MC_PI_ActionValueFunction.csv");
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"Episode", "State", "Action", "Value"});
//		}
//		results.writeHeaders(writer);
		
//		for (State state : casino.getStates()) {
//			if (state.getType() == State.Type.NonTerminal) {
//				Action[] actions = gambler.getPolicy().getActions(state);
//				for (int a = 0; a < actions.length; a++) {
//					policyResults.addRow(0, state, actions[a]);
//				}
//				for (Action action : casino.getStateActionFunction().getActions(state)) {
//					results.addRow(0, state, action, gambler.getActionValueFunction().getValue(state, action));
//				}
//			}
//		}
//
//		policyResults.writeRows(policyWriter);
//		results.writeRows(writer);
		
//		Episode[] episodes = getRandomStarts(casino, numberOfRandomStarts);
		Episode[] episodes = getExploringStarts(casino);
		
//		// For each episode
		int episodeNumber = 0;
//
//		for (int loop = 0; loop < numberOfLoops; loop++) {
			for (Episode episode : episodes) {
				// Evaluate the policy

				ArrayList<StateAction> vistedStateActions = new ArrayList<StateAction>();
				double delta = pe.evaluate(gambler.getPolicy(), gambler.getActionValueFunction(), 
						episode, vistedStateActions);
	
				// Improve the policy
				pi.improve(gambler.getPolicy(), gambler.getActionValueFunction(), vistedStateActions);
				episodeNumber++;
				
//				if (episodeNumber%(numberOfLoops*10) == 0) {
//				if (episodeNumber%500 == 0) {
//					for (State state : casino.getStates()) {
//						if (state.getType() == State.Type.NonTerminal) {
//							Action[] actions = gambler.getPolicy().getActions(state);
//							for (int a = 0; a < actions.length; a++) {
//								policyResults.addRow(episodeNumber, state, actions[a]);
//							}
//							for (Action action : casino.getStateActionFunction().getActions(state)) {
//								results.addRow(episodeNumber, state, action,
//										gambler.getActionValueFunction().getValue(state, action));
//							}
//						}
//					}
//				}
//				
			}
//		}

//		policyResults.writeRows(policyWriter);
//		results.writeRows(writer);

	}
	
	public static void generalisedPolicyIteration() {
		final int targetDollars = 100;
		final double probabilityHeads = 0.4;
		double theta = 0.0000001;
		double discount = 1.0;
		int numberOfIterations = 5000;
		
		Randomness.setSeed(0);

		// Initialise the MDP
		Casino casino = new Casino(targetDollars, probabilityHeads);

		// Initialise the policy and value function
		Gambler gambler = new Gambler(casino);
		gambler.setPolicy(gambler.createDeterministicPolicy(casino));
		
		// Set the ActionValueFunction to be the sample average required for episodes. 
		SampleAverageActionValueFunction vavf = new SampleAverageActionValueFunction(
				casino.getNumberOfStates(), casino.getNumberOfActions());
		gambler.setActionValueFunction(vavf);
		
		PolicyEvaluator policyEvaluator = new PolicyEvaluator(theta, discount);
		PolicyImprover policyImprover = new PolicyImprover(casino, 0.1, discount);
		GeneralisedPolicyIterator gpi = new GeneralisedPolicyIterator(casino, policyEvaluator, policyImprover, 0.001);

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("MCGeneralisedPolicyIteration.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"State", "Action", "IterationsNumber"});
//		}
//		results.writeHeaders(writer);
//
//		DataFrame avfResults = new DataFrame();
//		PrintWriter avfWriter = avfResults.initWriter("MC_GI_ActionValueFunction.csv");
//		if (avfResults.getNumberOfColumns() == 0) {
//			avfResults.addColumns(new String[]{"State", "Action", "Value", "IterationsNumber"});
//		}
//		avfResults.writeHeaders(avfWriter);
//
//		for (State state : casino.getStates()) {
//			if (state.getType() == State.Type.NonTerminal) {
//				if (gambler.getPolicy().getType() == Policy.Type.Deterministic) {
//					results.addRow(state, gambler.getPolicy().getAction(state), 0);
//
//				} else {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow(state, action, 0);
//					}
//				}
//				for (Action action : casino.getStateActionFunction().getActions(state)) {
//					avfResults.addRow(state, action, gambler.getActionValueFunction().getValue(state, action), 0);
//				}
//			}
//		}
		

		Episode[] episodes = getExploringStarts(casino);
		boolean stable = false;
		int iteration = 0;
		while (!stable && iteration < numberOfIterations) {
			stable = gpi.iterate(gambler, episodes, 3);
			iteration++;
		 	System.out.println("NDB::generalisedPolicyIteration()~iteration="+iteration+", stable="+stable);
		 	
//			if (iteration%(numberOfIterations/10) == 0) {
//				for (State state : casino.getStates()) {
//					if (state.getType() == State.Type.NonTerminal) {
//						if (gambler.getPolicy().getType() == Policy.Type.Deterministic) {
//							results.addRow(state, gambler.getPolicy().getAction(state), 0);
//
//						} else {
//							for (Action action : gambler.getPolicy().getActions(state)) {
//								results.addRow(state, action, 0);
//							}
//						}
//						for (Action action : casino.getStateActionFunction().getActions(state)) {
//							avfResults.addRow(state, action,
//									gambler.getActionValueFunction().getValue(state, action), iteration);
//						}
//					}
//				}
//			}
		}
		
//		for (State state : casino.getStates()) {
//			if (state.getType() == State.Type.NonTerminal) {
//				if (gambler.getPolicy().getType() == Policy.Type.Deterministic) {
//					results.addRow(state, gambler.getPolicy().getAction(state), 0);
//
//				} else {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow(state, action, 0);
//					}
//				}
//				for (Action action : casino.getStateActionFunction().getActions(state)) {
//					avfResults.addRow(state, action,
//							gambler.getActionValueFunction().getValue(state, action), iteration);
//				}
//			}
//		}
		
//		results.writeRows(writer);
//		avfResults.writeRows(avfWriter);
		
	}
	
//	public static Episode[] offPolicy() {
//		final int targetDollars = 100;
//		final double probabilityHeads = 0.4;
//		double theta = 0.0000001;
//		double discount = 1.0;
//		int numberOfIterations = 5000;
//
//		Randomness.setSeed(0);
//
//		// Initialise the MDP
//		Casino casino = new Casino(targetDollars, probabilityHeads);
//
//		// Initialise the policy and value function
//		Gambler gambler = new Gambler(casino);
////		gambler.setPolicy(gambler.createDeterministicPolicy(casino));
//
//		Policy behaviourPolicy = gambler.createEquiprobableSoftPolicy(casino);
//		Policy estimationPolicy = gambler.createEquiprobablePolicy(casino);
//
//		// Set the ActionValueFunction to be the sample average required for episodes.
//		SampleAverageActionValueFunction vavf = new SampleAverageActionValueFunction(
//				casino.getNumberOfStates(), casino.getNumberOfActions());
//		gambler.setActionValueFunction(vavf);
		
//		PolicyEvaluator policyEvaluator = new PolicyEvaluator(theta, discount);
//		PolicyImprover policyImprover = new PolicyImprover(casino, 0.1, discount);
//		OffPolicyIterator opi = new OffPolicyIterator();
//				casino, 
//				behaviourPolicy, estimationPolicy, 
//				policyEvaluator, policyImprover, 0.001);

//		DataFrame results = new DataFrame();
//		PrintWriter writer = results.initWriter("MCGeneralisedPolicyIteration.csv");
//
//		if (results.getNumberOfColumns() == 0) {
//			results.addColumns(new String[]{"State", "Action", "IterationsNumber"});
//		}
//		results.writeHeaders(writer);
//
//		DataFrame avfResults = new DataFrame();
//		PrintWriter avfWriter = avfResults.initWriter("MC_GI_ActionValueFunction.csv");
//		if (avfResults.getNumberOfColumns() == 0) {
//			avfResults.addColumns(new String[]{"State", "Action", "Value", "IterationsNumber"});
//		}
//		avfResults.writeHeaders(avfWriter);
		
//		for (State state : casino.getStates()) {
//			if (state.getType() == State.Type.NonTerminal) {
//				if (gambler.getPolicy().getType() == Policy.Type.Deterministic) {
//					results.addRow(state, gambler.getPolicy().getAction(state), 0);
//
//				} else {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow(state, action, 0);
//					}
//				}
//				for (Action action : casino.getStateActionFunction().getActions(state)) {
//					avfResults.addRow(state, action, gambler.getActionValueFunction().getValue(state, action), 0);
//				}
//			}
//		}

//		Episode[] episodes = getExploringStarts(casino);
//		boolean stable = false;
//		int iteration = 0;
//		while (!stable && iteration < numberOfIterations) {
//			stable = opi.iterate(behaviourPolicy, estimationPolicy, vavf, episodes, 3);
//			iteration++;
//		 	System.out.println("NDB::generalisedPolicyIteration()~iteration="+iteration+", stable="+stable);
		 	
//			if (iteration%(numberOfIterations/10) == 0) {
//				for (State state : casino.getStates()) {
//					if (state.getType() == State.Type.NonTerminal) {
//						if (gambler.getPolicy().getType() == Policy.Type.Deterministic) {
//							results.addRow(state, gambler.getPolicy().getAction(state), 0);
//
//						} else {
//							for (Action action : gambler.getPolicy().getActions(state)) {
//								results.addRow(state, action, 0);
//							}
//						}
//						for (Action action : casino.getStateActionFunction().getActions(state)) {
//							avfResults.addRow(state, action,
//									gambler.getActionValueFunction().getValue(state, action), iteration);
//						}
//					}
//				}
//			}
//		}
		
//		for (State state : casino.getStates()) {
//			if (state.getType() == State.Type.NonTerminal) {
//				if (gambler.getPolicy().getType() == Policy.Type.Deterministic) {
//					results.addRow(state, gambler.getPolicy().getAction(state), 0);
//
//				} else {
//					for (Action action : gambler.getPolicy().getActions(state)) {
//						results.addRow(state, action, 0);
//					}
//				}
//				for (Action action : casino.getStateActionFunction().getActions(state)) {
//					avfResults.addRow(state, action,
//							gambler.getActionValueFunction().getValue(state, action), iteration);
//				}
//			}
//		}
//
//		results.writeRows(writer);
//		avfResults.writeRows(avfWriter);
		
//	}

	public static Episode[] getExploringStarts(Casino casino) {
		ArrayList<Episode> exploringStarts = new ArrayList<Episode>();
		// Exploring States
		for (State intialState : casino.getStates()) {
			if (intialState.getType() == State.Type.NonTerminal) {
				for (Action initialAction : casino.getStateActionFunction().getActions(intialState)) {
					exploringStarts.add(new Episode(casino, intialState, initialAction));
				}
			}
		}
		return exploringStarts.toArray(new Episode[exploringStarts.size()]);
	}
	
	public static Episode[] getRandomStarts(Casino casino, int numberOfRandomStarts) {
		Episode[] randomStarts = new Episode[numberOfRandomStarts];
		for (int randomStart = 0; randomStart < numberOfRandomStarts; randomStart++) {
			int intialStateId = Randomness.nextInt(1, casino.getNumberOfStates()-1); // The first and last states are terminal, so no good.
			State intialState = casino.getStates()[intialStateId];
			Action[] possibleInitialAction = casino.getStateActionFunction().getActions(intialState);
			int intialActionLocation = Randomness.nextInt(possibleInitialAction.length);
			Action initialAction = possibleInitialAction[intialActionLocation];
			randomStarts[randomStart] = new Episode(casino, intialState, initialAction);
		}
		return randomStarts;
	}
}
