package ie.nix.rl_java.mc;

import ie.nix.rl_java.policy.TabularValueFunction;
import ie.nix.rl_java.mdp.State;

// I wrote this 6 years agi and I reckon now that it might be wrong.
public class WeightedAverageValueFunction extends TabularValueFunction {

	protected double[] weights;
	
	public WeightedAverageValueFunction(int numberOfStates) {
		super(numberOfStates);
		weights = new double[numberOfStates];
		
	}

	public void updateValue(State state, double value, double weight) {
		values[state.getId()] += value;
		weights[state.getId()] += weight;
		
	}
	
	public double getValue(State state) {
		if (weights[state.getId()] != 0) {
			return values[state.getId()]/weights[state.getId()];
		} else {
			return 0;
		}
	}

	public double getWeight(State state) {
		return weights[state.getId()];
	}

}
