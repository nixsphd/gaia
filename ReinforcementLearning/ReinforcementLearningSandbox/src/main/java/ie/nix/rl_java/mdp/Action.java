package ie.nix.rl_java.mdp;

public class Action {
	
	private int id;

	protected Action(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return Integer.toString(id);
	}
	
}
