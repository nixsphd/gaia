package ie.nix.rl_java.policy;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateAction;

public interface Policy {
	
	public enum Type {
		Deterministic,
		Equiprobable,
		Stochastic,
	}
	
	public Type getType();

	public boolean isSoft();

	public double getE();

	public State[] getStates();
	
	Action[] getPossibleActions(State forState);

	public Action selectAction(State state);

	Action getAction(State givenState);

	public Action[] getActions(State givenState);

	public double[] getProbabilities(State givenState);

	public double getProbability(StateAction visitedStateAction);

	public void updateAction(State forState, Action action);
	
	public void updateActions(State forState, Action[] actions);

	public void updateActions(State forState, Action[] actions, double[] probabilities);

}
