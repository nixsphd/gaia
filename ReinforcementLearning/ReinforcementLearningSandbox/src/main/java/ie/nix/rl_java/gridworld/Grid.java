package ie.nix.rl_java.gridworld;

import ie.nix.rl_java.mdp.Action;
import ie.nix.rl_java.mdp.RewardFunction;
import ie.nix.rl_java.mdp.State;
import ie.nix.rl_java.mdp.StateActionFunction;
import ie.nix.rl_java.mdp.TabularFiniteMDP;
import ie.nix.rl_java.mdp.TransitionFunction;
import java.util.HashMap;

public class Grid extends TabularFiniteMDP {

	public class Cell extends State {
		final int x;
		final int y;
		
		protected Cell(int x, int y) {
			this(x, y, numberOfStates, State.Type.NonTerminal);
		}
		
		protected Cell(int x, int y, int id, State.Type type) {
			super(id);
			this.type = type;
			this.x = x;
			this.y = y;
		}
	}
	
	protected enum Moves {
		North,
		East,
		South,
		West
	};
	
	public class Move extends Action {
		Moves type;
		protected Move(Moves type) {
			super(numberOfActions);
			this.type = type;
		}
	};
	
	private final int cellsInX;
	private final int cellsInY;
	private double defaultReward = 0;
	private double bumpReward = -1;
	private HashMap<Cell,Cell> specialStateTransitions;
	private HashMap<Cell,Double> specialStateRewards;
	
	public Grid(final int cellsInX, final int cellsInY) {
		super(cellsInX * cellsInY, 4);
		
		this.specialStateTransitions = new HashMap<Cell,Cell>();
		this.specialStateRewards = new HashMap<Cell,Double>();
		
		// Set the grid dimensions
		this.cellsInX = cellsInX;
		this.cellsInY = cellsInY;
		
		// Initialise the states
		for (int x = 0; x < cellsInX; x++) {
			for (int y = 0; y < cellsInY; y++) {
				addState(new Cell(x, y));
			}
		}

		// Initialise the actions: Moves
		for (Moves type: Moves.values()) {
			addAction(new Move(type));
		}
		
		// Initialise the state action function which gives the allowed
		// actions for the agent in a state.
		StateActionFunction stateActionFunction = new StateActionFunction() {
			public Action[] getActions(State forState) {
				return Grid.this.getActions();
			}
		};
		setStateActionFunction(stateActionFunction);
		
		// Initialise the state transition function
		TransitionFunction stf = new TransitionFunction() {
			
			public double probabilityOf(State movingToState, State givenState, Action givenAction) {
				State actualNextCell = (Cell)getNextState(givenState, givenAction);
				if (movingToState == actualNextCell) {
					return 1.0;
					
				} else {
					return 0.0;
					
				}
				
			}

			public State getNextState(State givenState, Action givenAction) {
				Cell givenCell = (Cell)givenState;
				Move givenMove = (Move)givenAction;
				
				if (specialStateTransitions.containsKey(givenCell)) {
					return specialStateTransitions.get(givenCell);
				}
				
				Cell nextCell = givenCell;
				switch (givenMove.type) {
					case North:
						if (givenCell.y != cellsInY - 1) {
							nextCell = getCell(givenCell.x, givenCell.y + 1);
						}
						break;
					case East:
						if (givenCell.x != cellsInX - 1) {
							nextCell = getCell(givenCell.x + 1, givenCell.y);
						}
						break;
					case South:
						if (givenCell.y != 0) {
							nextCell = getCell(givenCell.x, givenCell.y - 1);
						}
						break;
					case West:
						if (givenCell.x != 0) {
							nextCell = getCell(givenCell.x - 1, givenCell.y);
						}
						break;
					
				}
				return nextCell;
				
			}

		};
		setStateTransitionFunction(stf);
		
		// Initialise the reward states
		setRewardFunction(new RewardFunction() {

			public double getReward(State initialState, Action actionTaken, State finalState) {
				Cell givenCell = (Cell)initialState;
				Cell nextCell = (Cell)finalState;
				double reward = 0.0;
				if (specialStateRewards.containsKey(givenCell)) {
					reward = specialStateRewards.get(givenCell);
				}
				if (givenCell == nextCell) {
					reward =  bumpReward;
				} else {
					reward = defaultReward;
				}
				
				return reward;
			}
			
		});
	}

	public int getCellsInX() {
		return cellsInX;
	}

	public int getCellsInY() {
		return cellsInY;
	}

	public void setDefaultReward(double defaultReward) {
		this.defaultReward = defaultReward;
	}

	public void setBumpReward(double bumpReward) {
		this.bumpReward = bumpReward;
	}

	public Cell getCell(int x, int y) {
		return (Cell)states[(x * cellsInY) + y];
	}

	public void setTerminal(Cell cell) {
		updateState(new Cell(cell.x, cell.y, cell.getId(), State.Type.Terminal), (cell.x * cellsInY) + cell.y);
	}

	public void setSpecialCell(Cell cell, Cell cell1, double reward) {
		setSpecialTransition(cell, cell1);
		setSpecialReward(cell1, reward);
	}

	public void setSpecialTransition(Cell cell, Cell toCell) {
		specialStateTransitions.put(cell, toCell);
		
	}

	public void setSpecialReward(Cell cell, double reward) {
		specialStateRewards.put(cell, reward);
		
	}
	

}

