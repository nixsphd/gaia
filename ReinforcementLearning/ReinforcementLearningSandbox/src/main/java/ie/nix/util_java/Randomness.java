package ie.nix.util_java;

//import java.util.HashMap;

import java.util.Random;

//http://download.eclipse.org/eclipse/updates/4.3-P-builds/
//http://dist.springsource.org/snapshot/GRECLIPSE/e4.3-j8
	
public class Randomness {
	
	private static Random rngFromJava;
//	private static Normal normal;

//	private static HashMap<Double, PoissonDistribution> poissonDistributions;// (double p)
	
	public static void setSeed(int seed) {
		rngFromJava = new Random(seed);
//		poissonDistributions = new HashMap<Double, PoissonDistribution>();
//		randomEngine = new MersenneTwister(seed);
//		normal = new Normal(0, 1, randomEngine);
	}

	public static int nextBinary() {
//		System.out.println("NDB::nextRandomGene() ~Generated="+nextRandomGene);
		return nextInt(0,1);		
	}	
	
	public static double nextDouble() {
		return rngFromJava.nextDouble();
//		return nextDouble(0.0, 1.0);
	}

	public static double nextDouble(double start, double end) {
		double nextRandomDouble = start + (rngFromJava.nextDouble() * (end - start));
		return nextRandomDouble;
	}

	// Generates a uniformly distributed random integer between lower and upper (endpoints included).
	public static int nextInt(int start, int end) {
	    int nextInt = start + (rngFromJava.nextInt(end - start));
		return nextInt;
	}
	
	public static int nextInt(int positiveBound) {
		return nextInt(0, positiveBound);		
	}
	
	public static double nextGaussian() {
		return nextGaussian(0.0, 1.0);
	}
	
	public static double nextGaussian(double standardDeviation) {
		return nextGaussian(0.0, standardDeviation);
	}
	
	public static double nextGaussian(double mean, double standardDeviation) {
		double nextGaussian = mean + (rngFromJava.nextGaussian() * standardDeviation);
		return nextGaussian;
	}

//	public static long nextPoisson(double mean) {
//		return rngFromApache.nextPoisson(mean);
//	}
}