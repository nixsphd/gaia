package ie.nix.util_java;

public class Equals {

  public static boolean equals(double valueA, double valueB, double delta) {
    return (Math.abs(valueA - valueB) <= delta);

  }

  public static boolean greaterThan(double valueA, double valueB, double delta) {
    return ((valueA - valueB) >= delta);

  }

  public static boolean lessThan(double valueA, double valueB, double delta) {
    return ((valueB - valueA) >= delta);

  }

  public static boolean equals(double[] valueA, double[] valueB, double delta) {
    boolean equals = valueA.length == valueB.length;
    int d = valueA.length - 1;
    while (equals && d >= 0) {
      equals =  (Math.abs(valueA[d] - valueB[d]) <= delta);
      d--;
    }
    return equals;

  }

  public static boolean greaterThan(double[] valueA, double[] valueB, double delta) {
    boolean equals = valueA.length == valueB.length;
    int d = valueA.length - 1;
    while (equals && d >= 0) {
      equals =  (valueA[d] - valueB[d] >= delta);
      d--;
    }
    return equals;

  }

  public static boolean lessThan(double[] valueA, double[] valueB, double delta) {
    boolean equals = valueA.length == valueB.length;
    int d = valueA.length - 1;
    while (equals && d >= 0) {
      equals =  (valueA[d] - valueB[d] >=  delta);
      d--;
    }
    return equals;

  }

}
