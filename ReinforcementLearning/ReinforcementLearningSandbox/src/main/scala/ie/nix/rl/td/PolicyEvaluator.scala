package ie.nix.rl.td

import ie.nix.rl.Environment.TransitionReward
import ie.nix.rl.tabular.vf.{ConstantStepSize, ValueFunction}
import ie.nix.rl.{Episode, Episodes, FiniteMDP, Policy, State, StateAction}
import org.apache.logging.log4j.scala.Logging

import scala.annotation.tailrec

class PolicyEvaluator(environment: FiniteMDP with Episodes, initialValue: Double,
                      discount: Double, stepSize: Double)
    extends Logging {
    
    def getValueFunctionForPolicy(policy: Policy, episodes: Vector[Episode]):
    ValueFunction = {
        
        val initialValueFunction = ConstantStepSize(environment, initialValue, stepSize)
        getValueFunctionForPolicy(policy, episodes, initialValueFunction)
        
    }
    
    def getValueFunctionForPolicy(policy: Policy, episodes: Vector[Episode],
                                  initialValueFunction: ValueFunction): ValueFunction = {
        
        var valueFunction = initialValueFunction
        for (episode <- episodes) {
            valueFunction = runEpisodeSteps(policy, episode, valueFunction)
            logger.debug(s"~episode=$episode, valueFunction=$valueFunction")
        }
        valueFunction
    }
    
    def runEpisodeSteps(policy: Policy, episode: Episode, valueFunction: ValueFunction): ValueFunction = {
        
        @tailrec
        def runEpisodeStepsHelper(policy: Policy, episode: Episode, stepStateAction: StateAction, valueFunction: ValueFunction): ValueFunction = {
    
            val (transition, reward) = environment.getTransitionRewardForStateAction(stepStateAction)
            val updatedValueFunction = updateValueFunctionForEpisodeStep(valueFunction, (transition, reward))
            logger.trace(s"~transition=$transition, reward=$reward, updatedValueFunction=$updatedValueFunction")
    
            if (transition.isTerminal)
                updatedValueFunction
            else {
                val nextState = transition.finalState
                val nextStateAction = StateAction(nextState, policy.getActionForState(nextState))
                runEpisodeStepsHelper(policy, episode, nextStateAction, updatedValueFunction)
            }
        }
        
        runEpisodeStepsHelper(policy, episode, episode.initialStateAction, valueFunction)
    }
    
    def updateValueFunctionForEpisodeStep(valueFunction: ValueFunction, transitionReward: TransitionReward): ValueFunction = {
        valueFunction match {
            case constantStepSizeValueFunction: ConstantStepSize => {
                val state = transitionReward._1.stateAction.state
                val target: Double = getTarget(transitionReward, valueFunction)
                constantStepSizeValueFunction.updateValueForState(state, target)
            }
            case _ => {
                logger error (s"BatchUpdatingPolicyEvaluator expects a BatchUpdating, but got ${valueFunction.getClass}")
                valueFunction
            }
        }
    }
    
    def getTarget(transitionReward: TransitionReward, valueFunction: ValueFunction) = {
        val state = transitionReward._1.stateAction.state // S
        val finalState = transitionReward._1.finalState // S'
        val originalFinalStateValue = valueFunction.getValueForState(finalState) // V(S')
        val reward = transitionReward._2 // R
        val target = reward + (discount * originalFinalStateValue) // R + γV(S′)
        logger.trace(s"~state=$state, finalState=$finalState, " +
            s"originalFinalStateValue=$originalFinalStateValue, " +
            s"reward=$reward, target=$target")
        target
    }
    
}
