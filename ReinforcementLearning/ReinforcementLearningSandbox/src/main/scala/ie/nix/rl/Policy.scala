package ie.nix.rl

trait Policy {
    
    def getActionForState(state: State): Action
    
    def getActionsForState(state: State): Vector[Action]
    
    def getProbabilityForStateAction(stateAction: StateAction): Double
    
    def getMostProbableActionForState(state: State): Action
    
    def getActionAndProbabilitiesForState(state: State): Vector[(Action, Double)]
    
    def getStateActionsForState(state: State): Vector[StateAction]
    
    def getStateActionAndProbabilitiesForState(state: State): Vector[(StateAction, Double)]
    
    def updateStateActionProbability(stateAction: StateAction, probability: Double): Policy
    
}
