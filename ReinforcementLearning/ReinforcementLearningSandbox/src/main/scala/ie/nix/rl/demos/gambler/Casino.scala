package ie.nix.rl.demos.gambler

import ie.nix.rl.dp.FiniteMDPModel
import ie.nix.rl.{Action, Episodes, State, StateAction, Transition}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

case class Casino(targetCapitol: Int, probabilityHeads: Double) extends FiniteMDPModel with Episodes with  Logging {
    
    for (capitol <- 0 to targetCapitol) {
        val gamblersCapitol = GamblersCapitol(capitol)
        logger.debug(s"~gamblersCapitol=$gamblersCapitol, isTerminal=${gamblersCapitol.isTerminal}")
    
        if (gamblersCapitol.isTerminal) {
            addTerminalState(gamblersCapitol)
            
        } else {
            val highestBet = math.min(capitol, targetCapitol - capitol)
            logger.debug(s"~gamblersCapitol=$gamblersCapitol, highestBet=$highestBet")
            
            val noBet = GamblersBet(0)
            val noTransition = Transition(StateAction(gamblersCapitol, noBet), gamblersCapitol)
            setProbabilityForTransition(noTransition, 1d)
            logger.debug(s"~setProbabilityForTransition(noTransition=$noTransition, probability=1)")
            
            for (bet <- 1 to highestBet) {
                
                val gamblersBet = GamblersBet(bet)
                addStateAction(StateAction(gamblersCapitol, gamblersBet))
                logger.debug(s"~addStateAction(gamblersCapitol=$gamblersCapitol, highestBet=$gamblersBet)")
    
                val gamblersWinningCapitol = GamblersCapitol(capitol + bet)
                val winingTransition = Transition(StateAction(gamblersCapitol, gamblersBet), gamblersWinningCapitol)
                setProbabilityForTransition(winingTransition, probabilityHeads)
                logger.debug(s"~setProbabilityForTransition(winingTransition=$winingTransition, probability=$probabilityHeads)")
    
                val gamblersLoosingCapitol = GamblersCapitol(capitol - bet)
                val loosingTransition = Transition(StateAction(gamblersCapitol, gamblersBet), gamblersLoosingCapitol)
                setProbabilityForTransition(loosingTransition, 1d - probabilityHeads)
                logger.debug(s"~setProbabilityForTransition(loosingTransition=$loosingTransition, probability=${1d - probabilityHeads})")
    
                if (capitol + bet == targetCapitol) {
                    setRewardForTransition(winingTransition, 1)
                    logger.debug(s"~setRewardForTransition(winingTransition=$winingTransition, reward=1)")
        
                }
            }
        }
    }
    
    override def toString: String = {
        s"Casino(targetCapitol=$targetCapitol, probabilityHeads=$probabilityHeads)"
    }
    
    override def getInitialState(): GamblersCapitol = {
        // +1 to ensure that we don't get GamblersCapitol(0) which is a terminal state,
        // and -1 to compensate for the +1.
        GamblersCapitol(1+Random.nextInt(targetCapitol-1))
    }
    
    case class GamblersCapitol(capitol: Int) extends State {
        
        override def toString: String = s"$capitol"
        
        override def isTerminal: Boolean = capitol == 0 || capitol == targetCapitol
        
    }
    
    case class GamblersBet(bet: Int) extends Action {
        override def toString: String = s"$bet"
    }
    
}

