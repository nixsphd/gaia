package ie.nix.rl

import ie.nix.rl.Environment.TransitionReward
import org.apache.logging.log4j.scala.Logging

trait FiniteMDP extends Environment with Logging {
    
    private var terminalStateSet: Set[State] = Set()
    private var stateActionsSet: Set[StateAction] = Set()
    
    def getStates(): Vector[State] = {
        (stateActionsSet.map(_.state) ++ terminalStateSet).toVector
    }
    
    def getNonTerminalStates(): Vector[State] = {
        stateActionsSet.map(_.state).toVector
    }
    
    def getStateActions(): Vector[StateAction] = {
        stateActionsSet.toVector
    }
    
    def getActionsForState(state: State): Vector[Action] = {
        stateActionsSet.filter(_.state == state).map(_.action).toVector
    }
    
    def getStateActionsForState(state: State): Vector[StateAction] = {
        stateActionsSet.filter( _.state == state).toVector
    }
    
    def getTransitionRewardForStateAction(stateAction: StateAction): TransitionReward = {
        val transition = getTransitionForStateAction(stateAction)
        val reward = getRewardForTransition(transition)
        (transition, reward)
    }
    
    def addStateAction(stateAction: StateAction): FiniteMDP = {
        stateActionsSet += stateAction
        this
    }
    
    def addTerminalState(state: State): FiniteMDP = {
        terminalStateSet += state
        this
    }
    
}
