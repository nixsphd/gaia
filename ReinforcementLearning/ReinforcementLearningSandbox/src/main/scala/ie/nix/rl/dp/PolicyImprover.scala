package ie.nix.rl.dp

import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.Policy.difference
import ie.nix.rl.tabular.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging


object PolicyImprover {
    
    def apply(environment: FiniteMDPModel, discount: Double): PolicyImprover = {
        new PolicyImprover(environment, discount)
    }
    
}

class PolicyImprover(environment: FiniteMDPModel, discount: Double)
    extends Logging {
    
    def improve(policy: DeterministicPolicy, valueFunction: ValueFunction):
        (DeterministicPolicy, Double) = {
        
        val improvedPolicy = DeterministicPolicy(environment, discount, valueFunction)
        val policyDifference = difference(policy, improvedPolicy)
        logger.debug(s"~policyDifference=$policyDifference, improvedPolicy=$improvedPolicy")
        (improvedPolicy, policyDifference)

    }
    
}
