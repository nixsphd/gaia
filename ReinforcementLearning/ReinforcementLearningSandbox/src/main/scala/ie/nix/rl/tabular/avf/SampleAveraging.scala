package ie.nix.rl.tabular.avf

import ie.nix.rl.tabular.avf.ActionValueFunction.stateActionValueMap
import ie.nix.rl.{FiniteMDP, StateAction}
import org.apache.logging.log4j.scala.Logging

object SampleAveraging {
    
    def apply(environment: FiniteMDP, initialValue: Double): ActionValueFunction with SampleAveraging = {
        new SampleAveraging(stateActionValueMap(environment, initialValue), initialValue, Map())
    }
    
}

class SampleAveraging private[avf](stateActionValueMap: Map[StateAction, Double],
                                   initialValue: Double,
                                   val stateActionNumberOfSamplesMap: Map[StateAction, Int])
    extends ActionValueFunction(stateActionValueMap, initialValue) with Logging {
    
    override def toString: String = {
        stateActionValueMap.map(entry => "(" + entry._1 + "->" + entry._2 + "," +
            stateActionNumberOfSamplesMap.getOrElse(entry._1, "-") + ") ").mkString
    }
    
    override def getValueForStateAction(stateAction: StateAction): Double = {
        // Default to numberOfSample 1d to avoid division by 0.
        val sumOfValuesForState: Double = stateActionValueMap.getOrElse(stateAction, initialValue)
        val numberOfSamplesForState: Int = stateActionNumberOfSamplesMap.getOrElse(stateAction, 1)
        sumOfValuesForState / numberOfSamplesForState
    }
    
    override def setValueForStateAction(stateAction: StateAction, value: Double): SampleAveraging = {
        val newStateActionValue: Double = stateActionValueMap.getOrElse(stateAction, initialValue) + value
        val newNumberOfSamples: Int = stateActionNumberOfSamplesMap.getOrElse(stateAction, 0) + 1
        new SampleAveraging(stateActionValueMap + (stateAction -> newStateActionValue), initialValue,
            stateActionNumberOfSamplesMap + (stateAction -> newNumberOfSamples))
    }
    
    def getNumberOfSamples(stateAction: StateAction): Int =
        stateActionNumberOfSamplesMap(stateAction)
}
