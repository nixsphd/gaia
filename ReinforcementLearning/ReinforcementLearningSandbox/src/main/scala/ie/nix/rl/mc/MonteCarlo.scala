package ie.nix.rl.mc

import ie.nix.rl.Environment.TransitionReward
import ie.nix.rl.Policy
import ie.nix.rl.tabular.avf.{ActionValueFunction, WeightedAveraging}
import ie.nix.rl.tabular.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging

object MonteCarlo extends Logging {
    
    val DEFAULT_DISCOUNT: Double = 1d
    
    def updateActionValueFunction(originalActionValueFunction: ActionValueFunction, discount: Double,
                                  transitionRewards: Vector[TransitionReward]): ActionValueFunction = {
        
        var stateActionsReturn = 0d
        var actionValueFunction = originalActionValueFunction
        for ((transition, reward) <- transitionRewards.reverse) {
            val stateAction = transition.stateAction
            stateActionsReturn = (discount * stateActionsReturn) + reward
            actionValueFunction =
                actionValueFunction.setValueForStateAction(stateAction, stateActionsReturn)
        }
        actionValueFunction
    }
    
    def updateActionValueFunction(originalActionValueFunction: WeightedAveraging, discount: Double,
                                  visitedTransitionRewards: Vector[TransitionReward],
                                  behaviourPolicy: Policy): WeightedAveraging = {
        
        var stateActionReturn = 0d
        var weight = 1d
        var actionValueFunction = originalActionValueFunction
        for ((visitedTransition, reward) <- visitedTransitionRewards.reverse) {
            
            val stateAction = visitedTransition.stateAction
            stateActionReturn = (discount * stateActionReturn) + reward
            
            val behaviourPolicyProbability = behaviourPolicy.getProbabilityForStateAction(stateAction)
            weight = weight * (1d/behaviourPolicyProbability)
            logger.debug(s"~weight=$weight, behaviourPolicyProbability=$behaviourPolicyProbability")
            
            actionValueFunction = actionValueFunction.
                setValueForStateAction(stateAction, stateActionReturn, weight)
            
        }
        logger.debug(s"~visitedTransitionRewards=$visitedTransitionRewards, " +
            s"actionValueFunction=$actionValueFunction")
        actionValueFunction
    }
    
    def updateValueFunction(originalValueFunction: ValueFunction, discount: Double,
                            transitionRewards: Vector[TransitionReward]): ValueFunction = {
        
        var stateActionsReturn = 0d
        var valueFunction = originalValueFunction
        for ((transition, reward) <- transitionRewards.reverse) {
            val state = transition.stateAction.state
            stateActionsReturn = (discount * stateActionsReturn) + reward
            valueFunction = valueFunction.updateValueForState(state, stateActionsReturn)
            logger.info(s"~state=$state, return=$stateActionsReturn, valueFunction=$valueFunction")
        }
        valueFunction
    }
    
}