package ie.nix.rl.dp

import ie.nix.rl.dp.DynamicProgramming.{backupAllState}
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging

object PolicyEvaluator {
    
    def apply(environment: FiniteMDPModel, numberOfSweeps: Int, discount: Double): PolicyEvaluator = {
        new PolicyEvaluator(environment, numberOfSweeps, discount)
    }
    
}

class PolicyEvaluator(environment: FiniteMDPModel, numberOfSweeps: Int, discount: Double)
    extends Logging {
    
    
    def getValueFunctionForPolicy(policy: DeterministicPolicy): ValueFunction = {
        var valueFunction: ValueFunction = ValueFunction(environment, 0d)
        (1 to numberOfSweeps).foreach(_ => {
            valueFunction = backupAllState(environment, discount, policy, valueFunction)
        })
        logger.debug(s"~policy=$policy, valueFunction=$valueFunction")
        valueFunction
    }
    
}
