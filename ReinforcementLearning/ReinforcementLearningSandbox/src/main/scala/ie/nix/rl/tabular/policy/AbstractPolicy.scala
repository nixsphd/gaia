package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment.findItemGivenProbability
import ie.nix.rl.{Action, FiniteMDP, State, StateAction}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object AbstractPolicy {
    
    
    def getStateActionProbabilityMap(environment: FiniteMDP, initialValue: Double): Map[StateAction, Double] = {
        environment.getStateActions().map(stateAction => (stateAction, initialValue)).toMap
    }
    
    def getStateActionProbabilityMap(environment: FiniteMDP): Map[StateAction, Double] = {
        environment.getStateActions().map(stateAction => (stateAction, Random.nextDouble())).toMap
    }
    
}

abstract class AbstractPolicy(private val stateActionProbabilityMap: Map[StateAction, Double])
    extends ie.nix.rl.Policy with Logging {
    
    logger.trace(s"~stateActionProbabilityMap=$stateActionProbabilityMap")
    
    override def equals(obj: Any): Boolean = obj match {
        case policy: AbstractPolicy =>
            stateActionProbabilityMap.equals(policy.stateActionProbabilityMap)
    }
    
    override def toString: String = {
        stateActionProbabilityMap.toVector.sortBy(_._1.toString).toString()
    }
    
    override def getActionForState(state: State): Action = {
        val stateActionProbabilitiesForState = getStateActionAndProbabilitiesForState(state)
        val probability = Random.nextDouble()
        val stateAction = findItemGivenProbability[StateAction](stateActionProbabilitiesForState, probability)
        logger.debug(s"~probability=$probability, state=$state -> action=${stateAction.action}")
        stateAction.action
    }
    
    override def getMostProbableActionForState(state: State): Action = {
        getStateActionAndProbabilitiesForState(state).maxBy(_._2)._1.action
    }
    
    override def getActionsForState(state: State): Vector[Action] = {
        (for ((stateAction, _) <- stateActionProbabilityMap
              if stateAction.state == state) yield stateAction.action).toVector
    }
    
    override def getActionAndProbabilitiesForState(state: State): Vector[(Action, Double)] = {
        stateActionProbabilityMap.filterKeys(_.state == state).
            map(entry => (entry._1.action, entry._2)).toVector
    }
    
    override def getStateActionsForState(state: State): Vector[StateAction] = {
        (for ((stateAction, _) <- stateActionProbabilityMap
              if stateAction.state == state) yield stateAction).toVector
    }
    
    override def getProbabilityForStateAction(stateAction: StateAction) = {
        stateActionProbabilityMap.getOrElse(stateAction, 0d);
    }
    
    override def getStateActionAndProbabilitiesForState(state: State): Vector[(StateAction, Double)] = {
        stateActionProbabilityMap.filterKeys(_.state == state).toVector
    }
    
}