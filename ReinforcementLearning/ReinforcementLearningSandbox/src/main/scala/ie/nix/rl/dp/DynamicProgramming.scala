package ie.nix.rl.dp

import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.{Action, State, StateAction}
import ie.nix.rl.tabular.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging

object DynamicProgramming extends Logging {
    
    def backupAllState(environment: FiniteMDPModel, discount: Double,
                       policy: DeterministicPolicy, valueFunction: ValueFunction): ValueFunction = {
        
        var newValueFunction = valueFunction
        for (state <- environment.getNonTerminalStates) {
            val action = policy.getActionForState(state)
            val newStateValue = backupState(environment, discount, StateAction(state, action), newValueFunction)
            newValueFunction = newValueFunction.updateValueForState(state, newStateValue)
        }
        logger.trace(s"~policy=$policy, valueFunction=$valueFunction, newValueFunction=$newValueFunction")
        newValueFunction
        
    }
    
    def getBestAction(environment: FiniteMDPModel, discount: Double,
                      state: State, valueFunction: ValueFunction): Action = {
        
        val (bestStateAction, value) = environment.getStateActionsForState(state).
            map(stateAction => (stateAction, backupState(environment, discount, stateAction, valueFunction))).
            maxBy(_._2)
        logger.trace(s"~state=$state, bestStateAction=$bestStateAction, value=$value, valueFunction=$valueFunction")
        bestStateAction.action
        
    }
    
    def backupState(environment: FiniteMDPModel, discount: Double,
                    stateAction: StateAction, valueFunction: ValueFunction): Double = {
        var backupValue = 0d
        for ((transition, probabilityOfTransition) <- environment.getPossibleTransitionForStateAction(stateAction)) {
            val rewardForTransition = environment.getRewardForTransition(transition)
            val finalStateValue = valueFunction.getValueForState(transition.finalState)
            // v(s) = p(s'|a,s) * (reward(s,a,s') + (y * v(s')))
            backupValue += probabilityOfTransition * (rewardForTransition + (discount * finalStateValue))
            logger.trace(s"~transition=$transition, probabilityOfTransition=$probabilityOfTransition, " +
                s"rewardForTransition=$rewardForTransition, backupValue=$backupValue")
            backupValue
        }
        logger.trace(s"~stateAction=$stateAction, backupValue=$backupValue")
        backupValue
    }
    
}








