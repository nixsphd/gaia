package ie.nix.rl.tabular.policy

import ie.nix.rl.dp.DynamicProgramming.getBestAction
import ie.nix.rl.dp.FiniteMDPModel
import ie.nix.rl.tabular.avf.ActionValueFunction
import ie.nix.rl.tabular.policy.AbstractPolicy.getStateActionProbabilityMap
import ie.nix.rl.tabular.vf.ValueFunction
import ie.nix.rl.{Action, FiniteMDP, State, StateAction}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object DeterministicPolicy extends Logging {
    
    def apply() = new DeterministicPolicy(Map())
    
    def apply(environment: FiniteMDP) =
        new DeterministicPolicy(getStateActionProbabilityMap(environment, initialValue=0d))
    
    def apply(environment: FiniteMDPModel, discount: Double, valueFunction: ValueFunction): DeterministicPolicy = {
        var policy = DeterministicPolicy(environment)
        val updateForStates = environment.getNonTerminalStates
        policy = DeterministicPolicy(environment, discount, policy, valueFunction, updateForStates)
        logger.debug(s"~policy=$policy")
        policy
    }
    
    def apply(environment: FiniteMDPModel, discount: Double, originalPolicy: DeterministicPolicy,
              valueFunction: ValueFunction, updateForStates: Vector[State]): DeterministicPolicy = {
        var policy = originalPolicy
        for (state <- updateForStates) {
            val bestAction = getBestAction(environment, discount, state, valueFunction)
            policy = policy.updateActionForState(state, bestAction)
            logger.trace(s"~state=$state, bestAction=$bestAction")
        }
        logger.debug(s"~policy=$policy")
        policy
    }
    
    def apply(environment: FiniteMDP, actionValueFunction: ActionValueFunction): DeterministicPolicy = {
        var policy = DeterministicPolicy(environment)
        val updateForStates = environment.getNonTerminalStates
        policy = DeterministicPolicy(environment, policy, actionValueFunction, updateForStates)
        logger.debug(s"~policy=$policy")
        policy
    }
    
    def apply(environment: FiniteMDP, originalPolicy: DeterministicPolicy,
              actionValueFunction: ActionValueFunction, updateForStates: Vector[State]): DeterministicPolicy = {
        var policy = originalPolicy
        for (state <- updateForStates) {
            val bestAction = actionValueFunction.getBestAction(state)
            policy = policy.updateActionForState(state, bestAction)
            logger.trace(s"~state=$state, bestAction=$bestAction")
        }
        logger.debug(s"~policy=$policy")
        policy
    }
    
    def RandomDeterministicPolicy(environment: FiniteMDP): DeterministicPolicy = {
        environment.getNonTerminalStates().foldLeft(DeterministicPolicy(environment))((policy, state) => {
            val possibleStateActions = environment.getStateActionsForState(state)
            val randomlySelectedAction = possibleStateActions(Random.nextInt(possibleStateActions.length)).action
            
            policy.updateStateActionProbability(StateAction(state, randomlySelectedAction), 1d)
        })
    }
    
}

class DeterministicPolicy(private val stateActionProbabilityMap: Map[StateAction, Double])
    extends Policy(stateActionProbabilityMap) {
    
    override def getActionForState(state: State): Action = {
        val actionsWithProbabilityEquals1 = for ((action, probability) <- getActionAndProbabilitiesForState(state)
                                                 if (probability == 1d)) yield action
        logger.trace(s"~actionsWithProbabilityEquals1=$actionsWithProbabilityEquals1")
        
        actionsWithProbabilityEquals1 match {
            case action +: Vector() => action
            case actions if actions.length == 0 =>
                throw new RuntimeException(s"Deterministic policy does not have an action for state, $state")
            case actions if actions.length >= 2 =>
                throw new RuntimeException(s"Deterministic policy has multiple actions for state, $state. The are $actions")
        }
        
    }
    
    def updateActionForState(state: State, newAction: Action): DeterministicPolicy = {
        val updatedPolicy = getStateActionsForState(state).foldLeft(this)((policy, stateAction) => {
            if (stateAction.action == newAction) {
                policy.updateStateActionProbability(stateAction, 1d)
            } else {
                policy.updateStateActionProbability(stateAction, 0d)
            }
        })
        logger.debug(s"~state=$state -> newAction=$newAction, updatedPolicy=$updatedPolicy")
        updatedPolicy
    }
    
    override def updateStateActionProbability(stateAction: StateAction, probability: Double): DeterministicPolicy =
        new DeterministicPolicy(stateActionProbabilityMap + (stateAction -> probability))
    
}