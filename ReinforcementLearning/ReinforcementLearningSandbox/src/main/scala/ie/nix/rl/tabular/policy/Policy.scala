package ie.nix.rl.tabular.policy

import ie.nix.rl.{FiniteMDP, StateAction}
import ie.nix.rl.tabular.policy.AbstractPolicy.{getStateActionProbabilityMap}

import scala.util.Random

object Policy {
    
    def apply() = new Policy(Map())
    
    def apply(environment: FiniteMDP) =
        new Policy(getStateActionProbabilityMap(environment, initialValue=0d))
    
    def RandomPolicy(environment: FiniteMDP): Policy = {
        environment.getStateActions().
            foldLeft(Policy(environment))((policy, stateAction) => {
                val stateActionProbability = Random.nextDouble()
                policy.updateStateActionProbability(stateAction, stateActionProbability)
            })
    }
    
    def EquiprobablePolicy(environment: FiniteMDP): Policy = {
        environment.getNonTerminalStates().foldLeft(Policy(environment))((policy, state) => {
            val stateActions = environment.getStateActionsForState(state)
            stateActions.foldLeft(policy)((policy, stateAction) => {
                policy.updateStateActionProbability(stateAction, 1d / stateActions.size)
            })
        })
    }
    
    def difference(anAbstractPolicy: Policy, otherAbstractPolicy: Policy): Double = {
        val stateActions = (anAbstractPolicy.stateActionProbabilityMap.keySet ++ otherAbstractPolicy.stateActionProbabilityMap.keySet).toVector
        val difference = (for (stateAction <- stateActions) yield {
            val probability = anAbstractPolicy.getProbabilityForStateAction(stateAction)
            val otherProbability = otherAbstractPolicy.getProbabilityForStateAction(stateAction)
            math.abs(probability - otherProbability)
        }).sum
        difference
    }
    
}

class Policy(private val stateActionProbabilityMap: Map[StateAction, Double])
    extends AbstractPolicy(stateActionProbabilityMap) {
        
        override def updateStateActionProbability(stateAction: StateAction, probability: Double): Policy =
            new Policy(stateActionProbabilityMap + (stateAction -> probability))
            
    }