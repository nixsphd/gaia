package ie.nix.rl.dp

import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging

object GeneralisedPolicyIterator {
    
    def apply(policyEvaluator: PolicyEvaluator, policyImprover: PolicyImprover): GeneralisedPolicyIterator = {
        new GeneralisedPolicyIterator(policyEvaluator, policyImprover)
    }
    
}

class GeneralisedPolicyIterator(policyEvaluator: PolicyEvaluator, policyImprover: PolicyImprover) extends Logging {
    
    def iterate(policy: DeterministicPolicy): (DeterministicPolicy, ValueFunction) = {
        
        def iterateHelper(improvedPolicyValueFunctionAndIsImproved: (DeterministicPolicy, ValueFunction, Double)):
        (DeterministicPolicy, ValueFunction) = {
            improvedPolicyValueFunctionAndIsImproved match {
                case (improvedPolicy, valueFunction, difference) if (difference == 0) => (improvedPolicy, valueFunction)
                case (improvedPolicy, _, _) =>
                    iterateHelper(evaluateAndImprovePolicy(improvedPolicy))
            }
        }
        
        iterateHelper(policy, policyEvaluator.getValueFunctionForPolicy(policy), Double.MaxValue)
    }
    
    private def evaluateAndImprovePolicy(policy: DeterministicPolicy):
        (DeterministicPolicy, ValueFunction, Double) = {
        
        // Evaluate the policy
        val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy)
        // Improve the policy
        val (improvedPolicy, difference) = policyImprover.improve(policy, valueFunction)
        
        (improvedPolicy, valueFunction, difference)
    }
    
}
