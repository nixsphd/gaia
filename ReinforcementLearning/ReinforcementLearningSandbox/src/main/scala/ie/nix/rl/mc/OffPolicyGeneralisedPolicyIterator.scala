package ie.nix.rl.mc

import ie.nix.rl.mc.MonteCarlo.{DEFAULT_DISCOUNT, updateActionValueFunction}
import ie.nix.rl.tabular.avf.{ActionValueFunction, WeightedAveraging}
import ie.nix.rl.tabular.policy.{DeterministicPolicy, SoftPolicy}
import ie.nix.rl.tabular.policy.Policy.EquiprobablePolicy
import ie.nix.rl.tabular.policy.SoftPolicy.RandomSoftPolicy
import ie.nix.rl.{Episode, Episodes, FiniteMDP, Policy}
import org.apache.logging.log4j.scala.Logging

object OffPolicyGeneralisedPolicyIterator {
    
    def apply(environment: FiniteMDP with Episodes, discount: Double):
        OffPolicyGeneralisedPolicyIterator = {
        new OffPolicyGeneralisedPolicyIterator(environment, discount)
    }
    
    def apply(environment: FiniteMDP with Episodes): OffPolicyGeneralisedPolicyIterator = {
        new OffPolicyGeneralisedPolicyIterator(environment, DEFAULT_DISCOUNT)
    }
    
}

class OffPolicyGeneralisedPolicyIterator(environment: FiniteMDP with Episodes, discount: Double)
    extends Logging {
    
    def iterateWithRandomSoftPolicyBehaviourPolicy(targetPolicy: DeterministicPolicy, episodes: Vector[Episode]):
    (DeterministicPolicy, ActionValueFunction) = {
        val probabilityOfRandomAction = 0.1
        iterate(targetPolicy, episodes, getBehaviourPolicy =
            _ => RandomSoftPolicy(environment, probabilityOfRandomAction))
    }
    
    def iterateWithEquiprobableBehaviourPolicy(targetPolicy: DeterministicPolicy, episodes: Vector[Episode]):
        (DeterministicPolicy, ActionValueFunction) = {
        iterate(targetPolicy, episodes, getBehaviourPolicy = _ => EquiprobablePolicy(environment))
    }
    
    def iterateWithEGreedyBehaviourPolicy(targetPolicy: DeterministicPolicy, episodes: Vector[Episode]):
        (DeterministicPolicy, ActionValueFunction) = {
        val probabilityOfRandomAction = 0.1
        iterate(targetPolicy, episodes, getBehaviourPolicy = deterministicPolicy =>
            SoftPolicy(environment, probabilityOfRandomAction, deterministicPolicy))
    }
    
    private[mc] def iterate(targetPolicy: DeterministicPolicy, episodes: Vector[Episode],
                getBehaviourPolicy: DeterministicPolicy => Policy): (DeterministicPolicy, ActionValueFunction) = {
        
        var actionValueFunction = WeightedAveraging(environment, initialValue=0d)
        var improvedTargetPolicy = targetPolicy
        for (episode <- episodes) {
            
            val behaviourPolicy = getBehaviourPolicy(improvedTargetPolicy)
            logger.trace(s"~behaviourPolicy=$behaviourPolicy")
            val visitedTransitionRewards = episode.runEpisode(behaviourPolicy)
            
            actionValueFunction =
                updateActionValueFunction(actionValueFunction, discount, visitedTransitionRewards, behaviourPolicy)
            
            val visitedStates = visitedTransitionRewards.map(_._1.stateAction.state)
            val sumOfRewards = visitedTransitionRewards.map(_._2).sum
            logger.info(s"~episode=$episode, sumOfRewards=$sumOfRewards")
            
            improvedTargetPolicy = DeterministicPolicy(environment, improvedTargetPolicy, actionValueFunction, visitedStates)
    
        }
        (improvedTargetPolicy, actionValueFunction)
        
    }
}
