package ie.nix.rl.tabular.vf

import ie.nix.rl.{FiniteMDP, State}
import ie.nix.rl.tabular.vf.ConstantStepSize.StepSize
import ie.nix.rl.tabular.vf.ValueFunction.getStateValueMap
import org.apache.logging.log4j.scala.Logging

object BatchUpdating {
    
    def apply(environment: FiniteMDP, initialValue: Double, stepSize: StepSize) =
        new BatchUpdating(getStateValueMap(environment, initialValue), initialValue, stepSize, Map())
        
}

class BatchUpdating private[vf](stateValueMap: Map[State, Double], initialValue: Double,
                                val stepSize: StepSize,
                                val stateBatchUpdatesMap: Map[State, Double])
    extends ValueFunction(stateValueMap, initialValue) with Logging {
    
    logger.trace(s"~stateValueMap=$stateValueMap, stateBatchUpdatesMap=$stateBatchUpdatesMap, " +
        s"${stateValueMap.zip(stateBatchUpdatesMap)}")
    
//    def this(initialValue: Double, stepSize: StepSize) =
//        this(Map(), initialValue, stepSize, Map())
    
    override def toString: String = {
        s"$stateValueMap, $stateBatchUpdatesMap"
    }
    
    override def updateValueForState(state: State, target: Double): BatchUpdating = {
        logger.trace(s"~state=$state, stepSize=$stepSize, target=$target")
        new BatchUpdating(stateValueMap + (state -> target), initialValue, stepSize,
            stateBatchUpdatesMap + (state -> target))
    }
    
    def getBatchValueForState(state: State): Double = {
        stateBatchUpdatesMap.getOrElse(state, initialValue)
    }
    
    def addBatchValueForState(state: State, target: Double): BatchUpdating = {
        val originalBatchValue = getBatchValueForState(state)
        // V(St) ← V(St) + α􏰖[target − V(St)􏰗]
        val newValue = originalBatchValue + (stepSize * (target - originalBatchValue))
        logger.trace(s"~state=$state, stepSize=$stepSize, target=$target, " +
            s"originalBatchValue=$originalBatchValue, newValue=$newValue")
        new BatchUpdating(stateValueMap, initialValue, stepSize, stateBatchUpdatesMap + (state -> newValue))
    }
    
    def consolidateBatch: BatchUpdating = {
        stateBatchUpdatesMap.foldLeft(this)((batchUpdating, stateBatchValue) => {
            val state = stateBatchValue._1
            val batchValue = stateBatchValue._2
            val originalValue = stateValueMap.getOrElse(state, initialValue)
            
            logger.trace(s"~state=$state, originalValue=$originalValue, newValue=$batchValue")
            new BatchUpdating(batchUpdating.stateValueMap + (state -> batchValue),
                batchUpdating.initialValue, batchUpdating.stepSize,
                batchUpdating.stateBatchUpdatesMap)
        })
    }
}
