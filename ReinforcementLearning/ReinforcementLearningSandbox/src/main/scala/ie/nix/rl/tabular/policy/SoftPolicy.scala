package ie.nix.rl.tabular.policy

import ie.nix.rl.tabular.avf.ActionValueFunction
import ie.nix.rl.tabular.policy.AbstractPolicy.{getStateActionProbabilityMap}
import ie.nix.rl.{Action, FiniteMDP, State, StateAction}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random


object SoftPolicy extends Logging {
    
    def apply(probabilityOfRandomAction: Double) =
        new SoftPolicy(Map(), probabilityOfRandomAction)
    
    def apply(environment: FiniteMDP, probabilityOfRandomAction: Double) =
        new SoftPolicy(getStateActionProbabilityMap(environment, initialValue=0), probabilityOfRandomAction)
    
    def apply(environment: FiniteMDP, probabilityOfRandomAction: Double,
              actionValueFunction: ActionValueFunction): SoftPolicy = {
        var policy = SoftPolicy(environment, probabilityOfRandomAction)
        for (state <- environment.getNonTerminalStates) {
            val bestAction = actionValueFunction.getBestAction(state)
            policy = policy.updateActionForState(state, bestAction)
            logger.trace(s"~state=$state, bestAction=$bestAction")
        }
        logger.trace(s"~policy=$policy")
        policy
    }
    
    def apply(environment: FiniteMDP, originalPolicy: SoftPolicy, actionValueFunction: ActionValueFunction,
              visitedStates: Vector[State]): SoftPolicy = {
        var policy = originalPolicy
        for (state <- visitedStates) {
            val bestAction = actionValueFunction.getBestAction(state)
            policy = policy.updateActionForState(state, bestAction)
            logger.trace(s"~state=$state, bestAction=$bestAction")
        }
        logger.trace(s"~policy=$policy")
        policy
    }
    
    def apply(environment: FiniteMDP, probabilityOfRandomAction: Double,
                   deterministicPolicy: DeterministicPolicy): SoftPolicy = {

        environment.getNonTerminalStates().
            foldLeft(SoftPolicy(environment, probabilityOfRandomAction))((policy, state) => {
                val possibleStateActions = environment.getStateActionsForState(state)
                val actionFromDeterministicPolicy = deterministicPolicy.getActionForState(state)
                val probabilityOfNonGreedyAction = probabilityOfRandomAction / possibleStateActions.size
                val probabilityOfGreedyAction = 1d - probabilityOfRandomAction + probabilityOfNonGreedyAction
                
                possibleStateActions.foldLeft(policy)((policy, stateAction) => {
                    if (stateAction.action == actionFromDeterministicPolicy) {
                        logger.trace(s"~stateAction=$stateAction, $probabilityOfGreedyAction")
                        policy.updateStateActionProbability(stateAction, probabilityOfGreedyAction)
                    } else {
                        logger.trace(s"~stateAction=$stateAction, $probabilityOfNonGreedyAction")
                        policy.updateStateActionProbability(stateAction, probabilityOfNonGreedyAction)
                    }
                })
                
            })
    }
    
    def RandomSoftPolicy(environment: FiniteMDP, probabilityOfRandomAction: Double): SoftPolicy = {
        environment.getNonTerminalStates().foldLeft(SoftPolicy(environment, probabilityOfRandomAction))((policy, state) => {
            val possibleStateActions = environment.getStateActionsForState(state)
            val randomlySelectedAction = possibleStateActions(Random.nextInt(possibleStateActions.length)).action
            val probabilityOfNonGreedyAction = probabilityOfRandomAction / possibleStateActions.size
            val probabilityOfGreedyAction = 1d - probabilityOfRandomAction + probabilityOfNonGreedyAction
            possibleStateActions.foldLeft(policy)((policy, stateAction) => {
                if (stateAction.action == randomlySelectedAction) {
                    policy.updateStateActionProbability(stateAction, probabilityOfGreedyAction)
                } else {
                    policy.updateStateActionProbability(stateAction, probabilityOfNonGreedyAction)
                }
            })
        })
    }
}

class SoftPolicy(private val stateActionProbabilityMap: Map[StateAction, Double], probabilityOfRandomAction: Double)
    extends DeterministicPolicy(stateActionProbabilityMap) with Logging {
    
    override def updateActionForState(state: State, action: Action): SoftPolicy = {
        val newStateActionProbability: Seq[(StateAction, Double)] =
            getSoftStateActionsProbabilities(state, action)
        logger.trace(s"~newStateActionProbability=$newStateActionProbability")

        val updatedPolicy = new SoftPolicy(stateActionProbabilityMap ++ newStateActionProbability.toMap,
            probabilityOfRandomAction)
        logger.debug(s"~state=$state -> action=$action, updatedPolicy=$updatedPolicy")
        updatedPolicy
    }
    
    override def updateStateActionProbability(stateAction: StateAction, probability: Double): SoftPolicy =
        new SoftPolicy(stateActionProbabilityMap + (stateAction -> probability), probabilityOfRandomAction)
        
    private def getSoftStateActionsProbabilities(state: State, action: Action) = {
        val stateActions = getStateActionsForState(state)
        val probabilityOfNonGreedyAction = probabilityOfRandomAction / stateActions.size
        val probabilityOfGreedyAction = 1d - probabilityOfRandomAction + probabilityOfNonGreedyAction
        val newStateActionProbability: Vector[(StateAction, Double)] =
            (StateAction(state, action), probabilityOfGreedyAction) +:
                stateActions.filter(stateAction => stateAction.action != action).
                    map(stateAction => (stateAction, probabilityOfNonGreedyAction))
        newStateActionProbability
    }
    
    override def getActionForState(state: State): Action = {
        val actionWithProbability = getMostProbableActionForState(state)
        logger.trace(s"~actionWithProbability=$actionWithProbability")
        actionWithProbability
        
    }
    
}
