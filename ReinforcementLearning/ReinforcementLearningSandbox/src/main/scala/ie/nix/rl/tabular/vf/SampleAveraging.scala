package ie.nix.rl.tabular.vf

import ie.nix.rl.{FiniteMDP, State}
import ie.nix.rl.tabular.vf.ConstantStepSize.StepSize
import ie.nix.rl.tabular.vf.ValueFunction.getStateValueMap
import org.apache.logging.log4j.scala.Logging

object SampleAveraging {
    
    def apply(environment: FiniteMDP, initialValue: Double): SampleAveraging =
        new SampleAveraging(getStateValueMap(environment, initialValue), initialValue, Map())
}

class SampleAveraging private[vf](stateValueMap: Map[State, Double],
                                  initialValue: Double,
                                  val stateNumberOfSamplesMap: Map[State, Int])
    extends ValueFunction(stateValueMap, initialValue) with Logging {
    
//    def this(initialValue: Double) = this(Map(), initialValue, Map())
    
    def copy(stateValueMap: Map[State, StepSize],
             initialValue: Double,
             stateNumberOfSamplesMap: Map[State, Int]): SampleAveraging = {
        new SampleAveraging(stateValueMap, initialValue, stateNumberOfSamplesMap)
    }
    
    override def toString: String = {
        stateValueMap.zip(stateNumberOfSamplesMap).toString()
    }
    
    override def getValueForState(state: State): Double = {
        // Default to numberOfSample 1d to avoid division by 0.
        val sumOfValuesForState: Double = super.getValueForState(state)
        val numberOfSamplesForState: Int = stateNumberOfSamplesMap.getOrElse(state, 1)
        sumOfValuesForState / numberOfSamplesForState
    }
    
    override def updateValueForState(state: State, value: Double): SampleAveraging = {
        val newStateValue: Double = super.getValueForState(state) + value
        val newNumberOfSamples: Int = stateNumberOfSamplesMap.getOrElse(state, 0) + 1
        copy(stateValueMap + (state -> newStateValue), initialValue,
            stateNumberOfSamplesMap + (state -> newNumberOfSamples))
    }
    
    def getNumberOfSamples(state: State): Int = stateNumberOfSamplesMap(state)
}
