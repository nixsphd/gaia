package ie.nix.rl.mc

import ie.nix.rl.mc.MonteCarlo.{DEFAULT_DISCOUNT, updateActionValueFunction, updateValueFunction}
import ie.nix.rl.tabular.avf.{ActionValueFunction, SampleAveraging}
import ie.nix.rl.tabular.vf.{ConstantStepSize, ValueFunction}
import ie.nix.rl.{Episode, Episodes, FiniteMDP, Policy}
import org.apache.logging.log4j.scala.Logging

object PolicyEvaluator {
    
    def apply(environment: FiniteMDP with Episodes, discount: Double): PolicyEvaluator = {
        new PolicyEvaluator(environment, discount)
    }
    
    def apply(environment: FiniteMDP with Episodes): PolicyEvaluator = {
        new PolicyEvaluator(environment, DEFAULT_DISCOUNT)
    }
    
}

case class PolicyEvaluator(environment: FiniteMDP with Episodes, discount: Double)
    extends Logging {
    
    def getSampleAveragingValueFunctionForPolicy(policy: Policy, episodes: Seq[Episode]):
        ActionValueFunction = {

        val initialActionValueFunction = SampleAveraging(environment, initialValue = 0d)
        updateActionValueFunctionForPolicy(policy, episodes, initialActionValueFunction)
    }
    
    private def updateActionValueFunctionForPolicy(policy: Policy, episodes: Seq[Episode],
                                                   initialActionValueFunction: ActionValueFunction): ActionValueFunction = {
        
        var updatedActionValueFunction = initialActionValueFunction
        for (episode <- episodes) {
            val visitedTransitionRewards = episode.runEpisode(policy)
    
            updatedActionValueFunction =
                updateActionValueFunction(updatedActionValueFunction, discount, visitedTransitionRewards)
        }
        logger.trace(s"~updatedActionValueFunction=$updatedActionValueFunction")
        updatedActionValueFunction
    }

    def getConstantStepSizeValueFunctionForPolicy(policy: Policy, episodes: Seq[Episode],
                                                  initialValue: Double, stepSize: Double): ValueFunction = {

        val initialValueFunction = ConstantStepSize(environment, initialValue, stepSize)
        updateValueFunctionForPolicy(policy, episodes, initialValueFunction)

    }
    
    private def updateValueFunctionForPolicy(policy: Policy, episodes: Seq[Episode], initialValueFunction: ValueFunction):
        ValueFunction = {
        
        var updatedValueFunction = initialValueFunction
        for (episode <- episodes) {
            val visitedTransitionRewards = episode.runEpisode(policy)
            updatedValueFunction =
                updateValueFunction(updatedValueFunction, discount, visitedTransitionRewards)
            logger.info(s"~episode=$episode, updatedValueFunction=$updatedValueFunction")
        }
        logger.trace(s"~updatedValueFunction=$updatedValueFunction")
        updatedValueFunction
    }
    
}
