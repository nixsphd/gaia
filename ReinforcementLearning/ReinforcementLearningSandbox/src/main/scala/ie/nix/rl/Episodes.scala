package ie.nix.rl

import ie.nix.rl.Environment.TransitionReward
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

trait Episodes extends FiniteMDP with Logging {
    
    def getExploringStart(): Vector[Episode] = {
        getExploringStarts(1)
    }
    
    def getExploringStarts(numberOfRepeats: Int): Vector[Episode] = {
        val exploringStarts = for (_ <- 1 to numberOfRepeats;
                                   state <- getNonTerminalStates();
                                   stateAction <- getStateActionsForState(state))
            yield Episode(this, stateAction)
        exploringStarts.toVector
    }
    
    def getRandomStarts(numberOfRandomStarts: Int): Vector[Episode] = {
        val nonTerminalStates = getNonTerminalStates()
        (1 to numberOfRandomStarts).map(_ => {
            val state = nonTerminalStates(Random.nextInt(nonTerminalStates.length))
            val stateActionsForState = getStateActionsForState(state)
            val stateAction = stateActionsForState(Random.nextInt(stateActionsForState.length))
            Episode(this, stateAction)
        }).toVector
    }
    
    def getStarts(numberOfStarts: Int): Vector[Episode] = {
        (1 to numberOfStarts).map(_ => {
            val state = getInitialState()
            val stateActionsForState = getStateActionsForState(state)
            val stateAction = stateActionsForState(Random.nextInt(stateActionsForState.length))
            Episode(this, stateAction)
        }).toVector
    }
    
    def getInitialState(): State
    
}

case class Episode(environment: Environment, initialStateAction: StateAction) {
    
    def runEpisode(policy: Policy): (Vector[TransitionReward]) = {
        
        def runEpisodeHelper(transitionRewards: Vector[TransitionReward]): Vector[TransitionReward] = {
            val lastTransitionReward = transitionRewards.last
            if (lastTransitionReward._1.isTerminal)
                transitionRewards
            else {
                val nextState = lastTransitionReward._1.finalState
                val nextStateAction = StateAction(nextState, policy.getActionForState(nextState))
                val nextTransitionReward = environment.getTransitionRewardForStateAction(nextStateAction)
                runEpisodeHelper(transitionRewards :+ nextTransitionReward)
            }
        }
    
        val transition = environment.getTransitionForStateAction(initialStateAction)
        val transitionRewards = Vector((transition, environment.getRewardForTransition(transition)))
        runEpisodeHelper(transitionRewards)
    }

}
