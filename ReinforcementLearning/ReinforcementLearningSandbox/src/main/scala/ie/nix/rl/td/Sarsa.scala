package ie.nix.rl.td

import ie.nix.rl.tabular.avf.ActionValueFunction
import ie.nix.rl.{Episodes, FiniteMDP, StateAction}

class Sarsa(environment: FiniteMDP with Episodes, discount: Double) {
    
    def iterate(episodes: Vector[StateAction]) {
        
        // Initialize Q(s, a), ∀s ∈ S, a ∈ A(s), arbitrarily Q(terminal-state, ·) = 0
        val actionValueFunction = ActionValueFunction(environment, initialValue=0d)
        
        // Repeat (for each episode):
        for (episode <- episodes) {
            //  Initialize S
            //  Choose A from S using policy derived from Q (e.g., ε-greedy) Repeat (for each step of episode):
            //  Take action A, observe R, S′
            //  Choose A′ from S′ using policy derived from Q (e.g., ε-greedy) Q(S, A) ← Q(S, A) + α[R + γQ(S′, A′) − Q(S, A)]
            //  S ← S′; A ← A′;
            // until S is terminal
        }
        
    }
    
}
