package ie.nix.rl.demos.random_walk

import ie.nix.rl.Environment.Reward
import ie.nix.rl.{Action, Episodes, FiniteMDP, State, StateAction, Transition, ValueFunction}
import org.apache.logging.log4j.scala.Logging

import scala.math.{pow, sqrt}

object RandomWalk extends Logging {
    
    def rootMeanSquaredError(randomWalk: RandomWalk, valueFunction: ValueFunction): Double = {
        val errorSquared = for (randomWalk.Tile(timeNumber) <- randomWalk.getNonTerminalStates()) yield {
            val value = valueFunction.getValueForState(randomWalk.Tile(timeNumber))
            val idealValue = timeNumber.toDouble / randomWalk.numberOfTiles.toDouble
            val error = idealValue - value
            logger.info(s"~timeNumber=$timeNumber, value=$value, idealValue=$idealValue, error=$error")
            pow(error, 2d)
        }
        sqrt(errorSquared.sum)
    }
    
}

case class RandomWalk(numberOfTiles: Int) extends FiniteMDP with Episodes with Logging {
    
    addTerminalState(Tile(0))
    for (tileNumber <- 1 until numberOfTiles; action <- List(Right, Left)) {
        addStateAction(StateAction(Tile(tileNumber), action))
    }
    addTerminalState(Tile(numberOfTiles))
    
    
    override def getInitialState(): State = {
        val initialState = Tile(numberOfTiles/2d.toInt)
        logger.trace(s"~initialState=$initialState")
        initialState
    }
    
    override def getTransitionForStateAction(stateAction: StateAction): Transition =
        stateAction match {
            case StateAction(Tile(tileNumber), Right) => {
                val transition = Transition(stateAction, Tile(tileNumber + 1))
                logger.trace(s"~transition=$transition")
                transition
            }
            case StateAction(Tile(tileNumber), Left) => {
                val transition = Transition(stateAction, Tile(tileNumber - 1))
                logger.trace(s"~transition=$transition")
                transition
            }
            case _ => {
                val transition = Transition(stateAction, stateAction.state)
                logger.error(s"Unexpected stateAction, $stateAction, using transition $transition")
                transition
            }
        }
    
    override def getRewardForTransition(transition: Transition): Reward = {
        transition match {
            case Transition(_, Tile(tileNumber)) if (tileNumber == numberOfTiles) => 1
            case _ => 0
        }
    }
    
    
    case class Tile(tileNumber: Int) extends State {
        override def toString: String = s"Tile($tileNumber)"
        override def isTerminal: Boolean =  tileNumber == 0 || tileNumber == numberOfTiles
    }
    
    object Right extends Action {
        override def toString: String = "Right"
    }
    
    object Left extends Action {
        override def toString: String = "Left"
    }
    
}
