package ie.nix.rl.tabular.vf

import ie.nix.rl.{FiniteMDP, State}
import ie.nix.rl.tabular.vf.ConstantStepSize.StepSize
import ie.nix.rl.tabular.vf.ValueFunction.getStateValueMap
import org.apache.logging.log4j.scala.Logging

object ConstantStepSize {
    
    type StepSize = Double
    
    def apply(environment: FiniteMDP, initialValue: Double, stepSize: StepSize): ConstantStepSize =
        new ConstantStepSize(getStateValueMap(environment, initialValue), initialValue, stepSize)
        
}

class ConstantStepSize private[vf](stateValueMap: Map[State, Double], initialValue: Double,
                                   val stepSize: StepSize)
    extends ValueFunction(stateValueMap, initialValue) with Logging {
    
//    def this(initialValue: Double, stepSize: StepSize) =
//        this(Map(), initialValue, stepSize)
    
//    def copy(stateValueMap: Map[State, StepSize],
//             initialValue: Double,
//             stepSize: StepSize): ConstantStepSize = {
//        new ConstantStepSize(stateValueMap, initialValue, stepSize)
//    }
    
    override def updateValueForState(state: State, target: Double): ConstantStepSize = {
        val newValue = getNewValueForTarget(state, target)
        logger.info(s"~state=$state, stepSize=$stepSize, target=$target, newValue=$newValue")
        new ConstantStepSize(stateValueMap + (state -> newValue), initialValue, stepSize)
    }
    
    protected def getNewValueForTarget(state: State, target: StepSize): Double = {
        val originalValue = stateValueMap.getOrElse(state, initialValue)
        // V(St) ← V(St) + α􏰖[target − V(St)􏰗]
        originalValue + (stepSize * (target - originalValue))
    }
    
}
