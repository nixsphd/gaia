package ie.nix.rl.mc

import ie.nix.rl.mc.MonteCarlo.{DEFAULT_DISCOUNT, updateActionValueFunction}
import ie.nix.rl.tabular.avf.{ActionValueFunction, SampleAveraging}
import ie.nix.rl.tabular.policy.{DeterministicPolicy, SoftPolicy}
import ie.nix.rl.{Episode, Episodes, FiniteMDP}
import org.apache.logging.log4j.scala.Logging

object GeneralisedPolicyIterator {
    
    def apply(environment: FiniteMDP with Episodes, discount: Double): GeneralisedPolicyIterator = {
        new GeneralisedPolicyIterator(environment, discount)
    }
    
    def apply(environment: FiniteMDP with Episodes): GeneralisedPolicyIterator = {
        new GeneralisedPolicyIterator(environment, DEFAULT_DISCOUNT)
    }
    
}

class GeneralisedPolicyIterator(environment: FiniteMDP with Episodes, discount: Double)
    extends Logging {
    
    def iterate(policy: DeterministicPolicy, episodes: Vector[Episode]):
        (DeterministicPolicy, ActionValueFunction) = {
        
        var actionValueFunction: ActionValueFunction = SampleAveraging(environment, initialValue = 0d)
        var improvedPolicy = policy
        
        for (episode <- episodes) {
            logger.trace(s"~episode=$episode")
            val visitedTransitionRewards = episode.runEpisode(improvedPolicy)
            actionValueFunction =
                updateActionValueFunction(actionValueFunction, discount, visitedTransitionRewards)
            
            val visitedStates = visitedTransitionRewards.map(_._1.stateAction.state)
            logger.trace(s"~visitedStates=$visitedStates")
            improvedPolicy = DeterministicPolicy(environment, improvedPolicy, actionValueFunction, visitedStates)
            
        }
        
        logger.trace(s"~improvedPolicy=$improvedPolicy")
        logger.trace(s"~actionValueFunction=$actionValueFunction")
        (improvedPolicy, actionValueFunction)
    }
    
    def iterate(policy: SoftPolicy, episodes: Vector[Episode]):
        (SoftPolicy, ActionValueFunction) = {
        
        var actionValueFunction: ActionValueFunction = SampleAveraging(environment, initialValue = 0d)
        var improvedPolicy = policy
        
        for (episode <- episodes) {
            
            val visitedTransitionRewards = episode.runEpisode(improvedPolicy)
            actionValueFunction =
                updateActionValueFunction(actionValueFunction, discount, visitedTransitionRewards)
            
            val visitedStates = visitedTransitionRewards.map(_._1.stateAction.state)
            improvedPolicy = SoftPolicy(environment, improvedPolicy, actionValueFunction, visitedStates)
            
        }
        
        logger.trace(s"~improvedPolicy=$improvedPolicy")
        logger.trace(s"~actionValueFunction=$actionValueFunction")
        (improvedPolicy, actionValueFunction)
    }
}
