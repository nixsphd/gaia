package ie.nix.rl.tabular.avf

import ie.nix.rl.{Action, FiniteMDP, State, StateAction}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object ActionValueFunction {
    
    def apply(environment: FiniteMDP, initialValue: Double): ActionValueFunction =  {
        new ActionValueFunction(stateActionValueMap(environment, initialValue), initialValue)
    }
    
    private[avf] def stateActionValueMap(environment: FiniteMDP, initialValue: Double): Map[StateAction, Double] = {
        environment.getStateActions().map(stateAction => (stateAction, initialValue)).toMap
    }
    
    private[avf] def stateActionValueMap(environment: FiniteMDP): Map[StateAction, Double] = {
        environment.getStateActions().map(stateAction => (stateAction, Random.nextDouble())).toMap
    }
    
    def difference(one: ActionValueFunction, other: ActionValueFunction): Double = {
        val stateActions = (one.stateActionValueMap.keySet ++ other.stateActionValueMap.keySet).toVector
        val difference = (for (stateAction <- stateActions) yield {
            val value = one.getValueForStateAction(stateAction)
            val otherValue = other.getValueForStateAction(stateAction)
            math.abs(value - otherValue)
        }).sum
        difference
    }
    
}

class ActionValueFunction private[avf](val stateActionValueMap: Map[StateAction, Double],
                                       val initialValue: Double) extends ie.nix.rl.ActionValueFunction with Logging {
    
    override def toString: String = {
        stateActionValueMap.toVector.sortBy(_._1.toString).toString()
    }
    
    def getActionsForState(state: State): Vector[Action] = {
        stateActionValueMap.keySet.filter(stateAction => stateAction.state == state).
            map(stateAction => stateAction.action).toVector
    }
    
    def getStateActionsForState(state: State): Vector[StateAction] = {
        stateActionValueMap.keySet.filter(stateAction => stateAction.state == state).toVector
    }
    
    def getStateActionsValuesForState(state: State): Vector[(StateAction, Double)] = {
        stateActionValueMap.filterKeys(stateAction => stateAction.state == state).toVector
    }
    
    def getBestAction(state: State): Action = {
        val (bestStateAction, value) = getStateActionsValuesForState(state).maxBy(_._2)
        logger.trace(s"~state=$state, bestStateAction=$bestStateAction, value=$value, actionValueFunction=$this")
        bestStateAction.action
        
    }
    
    override def getValueForStateAction(stateAction: StateAction): Double = {
        stateActionValueMap.getOrElse(stateAction, initialValue)
    }
    
    override def setValueForStateAction(stateAction: StateAction, value: Double): ActionValueFunction = {
        new ActionValueFunction(stateActionValueMap + (stateAction -> value), initialValue)
    }
    
}





