package ie.nix.rl.demos.black_jack

import ie.nix.rl.demos.black_jack.CSVFileWriterHelper.{writeActionPolicyProbabilities, writeActionValueFunction, writePolicy}
import ie.nix.rl.{ActionValueFunction, Policy, StateAction}
import ie.nix.rl.mc.{GeneralisedPolicyIterator, OffPolicyGeneralisedPolicyIterator}
import ie.nix.rl.tabular.policy.DeterministicPolicy.RandomDeterministicPolicy
import ie.nix.rl.tabular.policy.SoftPolicy.RandomSoftPolicy
import ie.nix.util.CSVFileWriter
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object BlackJackProblemWithMC extends App with Logging {
    
    Random.setSeed(0)
    
    val blackJackTable: BlackJackTable = new BlackJackTable()
    val generalisedPolicyIterator = GeneralisedPolicyIterator(blackJackTable)
    val policy = RandomDeterministicPolicy(blackJackTable)
    val episodes = blackJackTable.getStarts(1000000)
    val (improvedPolicy, actionValueFunction)  = generalisedPolicyIterator.iterate(policy, episodes)
    
    writePolicy(improvedPolicy, blackJackTable, "BlackJackMC.csv")
    writeActionPolicyProbabilities(improvedPolicy, blackJackTable, "BlackJackMCPolicy.csv")
    writeActionValueFunction(actionValueFunction, blackJackTable, "BlackJackMCAVF.csv")
    
}

object BlackJackProblemWithSoftPolicy extends App with Logging {
    
    Random.setSeed(0)
    
    val blackJackTable: BlackJackTable = new BlackJackTable()
    val generalisedPolicyIterator = GeneralisedPolicyIterator(blackJackTable)
    val probabilityOfRandomAction = 0.1
    val policy = RandomSoftPolicy(blackJackTable, probabilityOfRandomAction)
    val episodes = blackJackTable.getStarts(1000000)
    val (improvedPolicy, actionValueFunction)  = generalisedPolicyIterator.iterate(policy, episodes)
    
    writePolicy(improvedPolicy, blackJackTable, "BlackJackMCSoft.csv")
    writeActionPolicyProbabilities(improvedPolicy, blackJackTable, "BlackJackMCSoftPolicy.csv")
    writeActionValueFunction(actionValueFunction, blackJackTable, "BlackJackMCSoftAVF.csv")
    
}

object BlackJackProblemWithOffPolicyMC extends App with Logging {
    
    Random.setSeed(0)
    
    val blackJackTable: BlackJackTable = new BlackJackTable()
    val generalisedPolicyIterator = OffPolicyGeneralisedPolicyIterator(blackJackTable)
    val probabilityOfRandomAction = 0.1
    val policy = RandomDeterministicPolicy(blackJackTable)
    val episodes = blackJackTable.getStarts(4000)
    val (improvedPolicy, actionValueFunction)  =
        generalisedPolicyIterator.iterateWithEGreedyBehaviourPolicy(policy, episodes)
    
    writePolicy(improvedPolicy, blackJackTable, "BlackJackProblemWithOffPolicyMC.csv")
    writeActionPolicyProbabilities(improvedPolicy, blackJackTable, "BlackJackProblemWithOffPolicyMCPolicy.csv")
    writeActionValueFunction(actionValueFunction, blackJackTable, "BlackJackProblemWithOffPolicyMCAVF.csv")
    
}

object CSVFileWriterHelper extends Logging {
    
    def writePolicy(policy: Policy, blackJackTable: BlackJackTable, fileName: String) = {
        val csvFileWriter: CSVFileWriter = CSVFileWriter("results", fileName)
        csvFileWriter.writeHeaders("Sum Of Players Cards", "Player Has Usable Ace", "Dealers Visible Card", "Action");
        
        blackJackTable.getNonTerminalStates().foreach(_ match {
            case state: blackJackTable.BlackJackState =>
                csvFileWriter.writeRow(state.sumOfPlayersCards, state.playerHasUsableAce, state.dealersVisibleCard,
                    policy.getMostProbableActionForState(state))
        })
    }
    
    def writeActionPolicyProbabilities(policy: Policy, blackJackTable: BlackJackTable, fileName: String) = {
        
        val csvFileWriter = CSVFileWriter("results", fileName)
        csvFileWriter.writeHeaders("Sum Of Players Cards", "Player Has Usable Ace", "Dealers Visible Card",
            "Action", "Probability")
        blackJackTable.getStateActions().foreach(_ match {
            case StateAction(state: blackJackTable.BlackJackState, action) =>
                val probability = policy.getProbabilityForStateAction(StateAction(state, action))
                csvFileWriter.writeRow(state.sumOfPlayersCards, state.playerHasUsableAce, state.dealersVisibleCard,
                    action, probability)
            case stateAction => logger.info(s"~No match for $stateAction")
        })
        
    }
    
    def writeActionValueFunction(actionValueFunction: ActionValueFunction, blackJackTable: BlackJackTable, fileName: String) = {
        val csvFileWriter = CSVFileWriter("results", fileName)
        csvFileWriter.writeHeaders("Sum Of Players Cards", "Player Has Usable Ace", "Dealers Visible Card",
            "Action", "Value");
        
        blackJackTable.getStateActions().foreach(_ match {
            case StateAction(state: blackJackTable.BlackJackState, action) =>
                val value = actionValueFunction.getValueForStateAction(StateAction(state, action))
                csvFileWriter.writeRow(state.sumOfPlayersCards, state.playerHasUsableAce, state.dealersVisibleCard, action, value)
            case stateAction => logger.info(s"~No match for $stateAction")
        })
    }
    
}
