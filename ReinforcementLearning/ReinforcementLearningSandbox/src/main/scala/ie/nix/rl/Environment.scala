package ie.nix.rl

import ie.nix.rl.Environment.{Reward, TransitionReward}
import org.apache.logging.log4j.scala.Logging

import scala.math.Ordered.orderingToOrdered
import scala.annotation.tailrec

object Environment extends Logging {
    
    type Reward = Double
    
    type TransitionReward = (Transition, Reward)
    
    def findItemGivenProbability[I](stateItemProbabilities: Seq[(I, Double)],
                                    selectProbability: Double): I = {
        @tailrec
        def findItemGivenProbability[I](itemProbabilities: Seq[(I, Double)],
                                        accumulativeProbability: Double,
                                        selectProbability: Double): I =
            itemProbabilities match {
                case (item, _) +: Nil => item
                case (item, probability) +: _
                    if accumulativeProbability + probability > selectProbability => item
                case (_, probability) +: tail =>
                    findItemGivenProbability(tail, accumulativeProbability + probability, selectProbability)
        }
        
        findItemGivenProbability(stateItemProbabilities, 0, selectProbability)
        
    }
    
}

trait Environment {
    
    def getTransitionForStateAction(stateAction: StateAction): Transition
    
    def getRewardForTransition(transition: Transition): Reward
    
    def getTransitionRewardForStateAction(stateAction: StateAction): TransitionReward
    
}

trait State {
    def isTerminal: Boolean = false
}

trait Action {
    def isTerminal: Boolean = false
}

case class StateAction(state: State, action: Action) {
    
    def isTerminal: Boolean =
        (state.isTerminal || action.isTerminal)
        
}

case class Transition(stateAction: StateAction, finalState: State) {
    def isTerminal: Boolean =
        stateAction.isTerminal || finalState.isTerminal
}



