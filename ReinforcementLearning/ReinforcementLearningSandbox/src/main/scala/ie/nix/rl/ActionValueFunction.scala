package ie.nix.rl

trait ActionValueFunction {
    
    def setValueForStateAction(stateAction: StateAction, value: Double): ActionValueFunction
    
    def getValueForStateAction(stateAction: StateAction): Double
    
}
