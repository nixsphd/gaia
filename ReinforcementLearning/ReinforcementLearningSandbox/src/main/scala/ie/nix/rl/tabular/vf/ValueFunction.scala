package ie.nix.rl.tabular.vf

import ie.nix.rl.Environment.{Reward, TransitionReward}
import ie.nix.rl.{FiniteMDP, State, StateAction, Transition}
import ie.nix.rl.tabular.vf.ConstantStepSize.StepSize
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object ValueFunction extends Logging {
    
    def apply(environment: FiniteMDP, initialValue: Double): ValueFunction =
        new ValueFunction(getStateValueMap(environment, initialValue), initialValue)
    
    private[vf] def getStateValueMap(environment: FiniteMDP, initialValue: Double): Map[State, Double] = {
        environment.getStates().map(state => if (state.isTerminal) (state, 0d) else (state, initialValue)).toMap
//        environment.getNonTerminalStates().map(state => (state, initialValue)).toMap
    }
    
    private[vf] def getStateValueMap(environment: FiniteMDP): Map[State, Double] = {
        environment.getStates().map(state => if (state.isTerminal) (state, 0d) else (state, Random.nextDouble())).toMap
//        environment.getNonTerminalStates().map(state => (state, Random.nextDouble())).toMap
    }
    
    
    
}

class ValueFunction private[vf](val stateValueMap: Map[State, Double],
                                val initialValue: Double)
    extends ie.nix.rl.ValueFunction with Logging {
    
    def copy(stateValueMap: Map[State, StepSize],
             initialValue: Double): ValueFunction = {
        new ValueFunction(stateValueMap, initialValue)
    }
    
    override def toString: String = {
        stateValueMap.toVector.sortBy(_._1.toString).toString()
    }
    
    override def getValueForState(state: State): Double = {
        if (state.isTerminal) 0d else stateValueMap.get(state).getOrElse(initialValue)
    }
    
    override def updateValueForState(state: State, value: Double): ValueFunction = {
        copy(stateValueMap + (state -> value), initialValue)
    }
    
    def getDifference(otherValueFunction: ValueFunction): Double = {
        val states = (stateValueMap.keySet ++ otherValueFunction.stateValueMap.keySet).toVector
        val difference = (for (state <- states) yield {
            val value = getValueForState(state)
            val otherValue = otherValueFunction.getValueForState(state)
            math.abs(value - otherValue)
        }).sum
        difference
    }
}