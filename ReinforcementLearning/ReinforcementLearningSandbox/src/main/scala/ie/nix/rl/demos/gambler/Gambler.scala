package ie.nix.rl.demos.gambler

import ie.nix.rl.tabular.policy
import ie.nix.rl.tabular.policy.DeterministicPolicy

object Gambler {
    
    def OstentatiousBetter(casino: Casino): DeterministicPolicy = {
        casino.getNonTerminalStates().foldLeft(policy.DeterministicPolicy(casino))((policy, state) => {
            state match {
                case (gamblersCapitol: casino.GamblersCapitol) => {
                    val maxBet = math.min(gamblersCapitol.capitol, casino.targetCapitol - gamblersCapitol.capitol)
                    policy.updateActionForState(state, casino.GamblersBet(maxBet))
                }
            }
        })
    }
    
    def ModestBetter(casino: Casino): DeterministicPolicy = {
        casino.getNonTerminalStates().foldLeft(policy.DeterministicPolicy(casino))((policy, state) => {
            policy.updateActionForState(state, casino.GamblersBet(1))
        })
    }
    
}
