package ie.nix.rl.demos.random_walk

import ie.nix.rl.mc
import ie.nix.rl.demos.random_walk.RandomWalk.rootMeanSquaredError
import ie.nix.rl.tabular.policy.Policy.EquiprobablePolicy
import ie.nix.rl.td
import ie.nix.rl.td.TemporalDifference.{BatchUpdatingPolicyEvaluator, PolicyEvaluator}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object RandomWalkProblemWithMCConstantStepSize extends App with Logging {

    Random.setSeed(0)

    val numberOfTiles = 6
    val randomWalk = RandomWalk(numberOfTiles)
    val policyEvaluator = mc.PolicyEvaluator(randomWalk)
    val policy = EquiprobablePolicy(randomWalk)
    val episodes = randomWalk.getStarts(250)
    val initialValue = 0.5
    val stepSize = 0.01

    val valueFunction =
        policyEvaluator.getConstantStepSizeValueFunctionForPolicy(
            policy, episodes, initialValue, stepSize)
    logger.info(s"~valueFunction=$valueFunction")

    val error = rootMeanSquaredError(randomWalk, valueFunction)
    logger.info(s"~rootMeanSquaredError=$error ~= 0.1")

}

object RandomWalkProblemWithTD extends App with Logging {

    Random.setSeed(0)

    val numberOfTiles = 6
    val randomWalk = RandomWalk(numberOfTiles)

    val initialValue = 0.5
    val discount = 1d
    val stepSize = 0.1
    val policyEvaluator = PolicyEvaluator(randomWalk, initialValue, discount, stepSize)

    val policy = EquiprobablePolicy(randomWalk)
    val episodes = randomWalk.getStarts(100)
    val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy, episodes)
    logger.info(s"~valueFunction=$valueFunction")

    val error = rootMeanSquaredError(randomWalk, valueFunction)
    logger.info(s"~rootMeanSquaredError=$error ~= 0.075")

}

object RandomWalkProblemWithBatchTD extends App with Logging {
    
    Random.setSeed(0)
    
    val randomWalk = RandomWalk(numberOfTiles=6)
    val policyEvaluator =
        BatchUpdatingPolicyEvaluator(randomWalk, initialValue=0.5, stepSize=0.01)
    val policy = EquiprobablePolicy(randomWalk)
    val episodes = randomWalk.getExploringStarts(numberOfRepeats=5)

    val valueFunction = policyEvaluator.getValueFunctionForPolicy(policy, episodes)
    logger.info(s"~valueFunction=$valueFunction")

    val error = rootMeanSquaredError(randomWalk, valueFunction)
    logger.info(s"~rootMeanSquaredError=$error ~= 0.04")
    
}



