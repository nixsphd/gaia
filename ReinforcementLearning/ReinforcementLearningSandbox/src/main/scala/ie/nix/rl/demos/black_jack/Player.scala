package ie.nix.rl.demos.black_jack

import ie.nix.rl.StateAction
import ie.nix.rl.tabular.policy
import ie.nix.rl.tabular.policy.DeterministicPolicy
import org.apache.logging.log4j.scala.Logging

object Player extends Logging {
    
    def Sticker(environment: BlackJackTable): DeterministicPolicy = {
        environment.getNonTerminalStates().foldLeft(policy.DeterministicPolicy(environment))((policy, state) => {
            policy.updateStateActionProbability(StateAction(state, environment.Stick), 1d)
        })
    }
    
    def Hitter(environment: BlackJackTable): DeterministicPolicy = {
        environment.getNonTerminalStates().foldLeft(policy.DeterministicPolicy(environment))((policy, state) => {
            logger.info(s"~policy=$policy, state=$state")
            policy.updateStateActionProbability(StateAction(state, environment.Hit), 1d)
        })
    }
    
}
