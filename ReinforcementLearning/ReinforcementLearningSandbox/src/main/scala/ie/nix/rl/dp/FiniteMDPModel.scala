package ie.nix.rl.dp

import ie.nix.rl.Environment.{Reward, findItemGivenProbability}
import ie.nix.rl.{FiniteMDP, State, StateAction, Transition}

import scala.util.Random

class FiniteMDPModel extends FiniteMDP {
    
    private var transitionProbabilityMap: Map[Transition, Double] = Map()
    private var transitionRewardMap: Map[Transition, Double] = Map()
    
    override def getTransitionForStateAction(stateAction: StateAction): Transition = {
        val probability = Random.nextDouble()
        val transition = findItemGivenProbability[Transition](
            getPossibleTransitionForStateAction(stateAction).toSeq, probability)
        transition
    }
    
    def getPossibleTransitionForState(state: State): Set[(Transition, Double)] = {
        transitionProbabilityMap.filter(entry =>
            (state == entry._1.stateAction.state && entry._2 > 0d)).toSet
    }
    
    def getPossibleTransitionForStateAction(stateAction: StateAction): Set[(Transition, Double)] = {
        transitionProbabilityMap.filter(entry =>
            (entry._1.stateAction == stateAction && entry._2 > 0d)).toSet
    }
    
    def setProbabilityForTransition(transition: Transition, probability: Double): Unit = {
        transitionProbabilityMap += (transition -> probability)
    }
    
    def getProbabilityOfTransition(transition: Transition): Double = {
        transitionProbabilityMap.get(transition).getOrElse(0)
    }
    
    
    override def getRewardForTransition(transition: Transition): Reward = {
        transitionRewardMap.get(transition).getOrElse(0)
    }
    
    def setRewardForTransition(transition: Transition, reward: Reward): Unit = {
        transitionRewardMap += (transition -> reward)
    }
    
}
