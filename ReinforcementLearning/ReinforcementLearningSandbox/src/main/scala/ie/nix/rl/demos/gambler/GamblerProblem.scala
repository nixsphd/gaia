package ie.nix.rl.demos.gambler

import ie.nix.rl.dp
import ie.nix.rl.mc
import ie.nix.rl.tabular.policy.DeterministicPolicy.RandomDeterministicPolicy
import ie.nix.rl.tabular.policy.SoftPolicy.RandomSoftPolicy
import ie.nix.util.CSVFileWriter
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object GamblerProblemWithDP extends App with Logging {
    
    Random.setSeed(0)
    val targetCapitol: Int = 15
    val probabilityHeads: Double = 0.4
    val casino: Casino = Casino(targetCapitol, probabilityHeads)
    val discount: Double = 1.0
    val numberOfSweeps: Int = 16
    val policyEvaluator = dp.PolicyEvaluator(casino, numberOfSweeps, discount)
    val policyImprover = dp.PolicyImprover(casino, discount)
    val gip = dp.GeneralisedPolicyIterator(policyEvaluator, policyImprover)
    val gamblerPolicy = RandomDeterministicPolicy(casino)
    val (improvedGamblerPolicy, valueFunction)  = gip.iterate(gamblerPolicy)
    logger.info(s"~improvedGamblerPolicy=$improvedGamblerPolicy")
    
    var csvFileWriter: CSVFileWriter =
        CSVFileWriter("results", "GamblerProblemDP.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads", "State", "Action");
    
    casino.getNonTerminalStates.foreach(state => {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,  state,
            improvedGamblerPolicy.getActionForState(state))
    })
    
    csvFileWriter = CSVFileWriter("results", "GamblerProblemDPVF.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads",
        "State", "Value")
    for (state <- casino.getNonTerminalStates) {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,  state,
            valueFunction.getValueForState(state))
    }
}

object GamblerProblemWithMC extends App with Logging {
    Random.setSeed(0)
    val targetCapitol: Int = 15
    val probabilityHeads: Double = 0.4
    val casino = Casino(targetCapitol, probabilityHeads)
    val generalisedPolicyIterator = mc.GeneralisedPolicyIterator(casino)
    val policy = RandomDeterministicPolicy(casino)
    val episodes = casino.getExploringStarts(100)
    val (improvedPolicy, actionValueFunction) = generalisedPolicyIterator.iterate(policy, episodes)
    logger.info(s"~improvedPolicy=$improvedPolicy")
    
    var csvFileWriter: CSVFileWriter = CSVFileWriter("results", "GamblerProblemMC.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads", "State", "Value")
    casino.getNonTerminalStates.foreach(state => {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,
            state, improvedPolicy.getActionForState(state))
    })
    
    csvFileWriter = CSVFileWriter("results", "GamblerProblemMCAVF.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads",
        "State", "Action", "Value")
    for (state <- casino.getNonTerminalStates;
         stateAction <- casino.getStateActionsForState(state)) {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,  state, stateAction.action,
            actionValueFunction.getValueForStateAction(stateAction))
    }
}

object GamblerProblemWithMCSoft extends App with Logging {
    Random.setSeed(0)
    val targetCapitol: Int = 15
    val probabilityHeads: Double = 0.4
    val casino = Casino(targetCapitol, probabilityHeads)
    val generalisedPolicyIterator = mc.GeneralisedPolicyIterator(casino)
    val probabilityOfRandomAction = 0.1
    val policy = RandomSoftPolicy(casino, probabilityOfRandomAction)
    val episodes = casino.getStarts(100000)
    val (improvedPolicy, actionValueFunction) = generalisedPolicyIterator.iterate(policy, episodes)
    logger.info(s"~improvedPolicy=$improvedPolicy")
    
    var csvFileWriter: CSVFileWriter = CSVFileWriter("results", "GamblerProblemMCSoft.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads", "State", "Value")
    casino.getNonTerminalStates.foreach(state => {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,
            state, improvedPolicy.getMostProbableActionForState(state))
    })
    
    csvFileWriter = CSVFileWriter("results", "GamblerProblemMCSoftAVF.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads",
        "State", "Action", "Value")
    for (state <- casino.getNonTerminalStates;
         stateAction <- casino.getStateActionsForState(state)) {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,  state, stateAction.action,
            actionValueFunction.getValueForStateAction(stateAction))
    }
}

object GamblerProblemWithOffPolicyMC extends App with Logging {
    Random.setSeed(0)
    val targetCapitol: Int = 15
    val probabilityHeads: Double = 0.8
    val casino = Casino(targetCapitol, probabilityHeads)
    val generalisedPolicyIterator = mc.OffPolicyGeneralisedPolicyIterator(casino)
    val probabilityOfRandomAction = 0.1
    val policy = RandomDeterministicPolicy(casino)
    val episodes= casino.getStarts(400000)
    val (improvedPolicy, actionValueFunction) =
        generalisedPolicyIterator.iterateWithEquiprobableBehaviourPolicy(policy, episodes)
    logger.info(s"~improvedPolicy=$improvedPolicy")
    
    var csvFileWriter: CSVFileWriter = CSVFileWriter("results", "GamblerProblemWithOffPolicyMC.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads", "State", "Value")
    casino.getNonTerminalStates.foreach(state => {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,
            state, improvedPolicy.getMostProbableActionForState(state))
    })
    
    csvFileWriter = CSVFileWriter("results", "GamblerProblemWithOffPolicyMCAVF.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads",
        "State", "Action", "Value")
    for (state <- casino.getNonTerminalStates;
         stateAction <- casino.getStateActionsForState(state)) {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,  state, stateAction.action,
            actionValueFunction.getValueForStateAction(stateAction))
    }
    
    csvFileWriter = CSVFileWriter("results", "GamblerProblemWithOffPolicyMCPolicy.csv")
    csvFileWriter.writeHeaders("Target Dollars", "Probability Heads",
        "State", "Action", "Probability")
    for (stateAction <- casino.getStateActions()) {
        csvFileWriter.writeRow(targetCapitol, probabilityHeads,  stateAction.state, stateAction.action,
            improvedPolicy.getProbabilityForStateAction(stateAction))
    }
}
