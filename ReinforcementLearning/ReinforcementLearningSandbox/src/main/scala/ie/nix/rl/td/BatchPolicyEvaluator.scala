package ie.nix.rl.td

import ie.nix.rl.Environment.TransitionReward
import ie.nix.rl.tabular.vf.{BatchUpdating, ValueFunction}
import ie.nix.rl.{Episode, Episodes, FiniteMDP, Policy, StateAction}
import org.apache.logging.log4j.scala.Logging

import scala.annotation.tailrec

class BatchPolicyEvaluator(environment: FiniteMDP with Episodes, initialValue: Double,
                           discount: Double, stepSize: Double, convergenceThreshold: Double)
    extends PolicyEvaluator(environment, initialValue, discount, stepSize) with Logging {
    
    override def getValueFunctionForPolicy(policy: Policy, episodes: Vector[Episode]): ValueFunction = {
        
        val initialValueFunction = BatchUpdating(environment, initialValue, stepSize)
        getValueFunctionForPolicy(policy, episodes, initialValueFunction)
        
    }
    
    override def getValueFunctionForPolicy(policy: Policy, episodes: Vector[Episode],
                                           valueFunction: ValueFunction): ValueFunction = {
        @tailrec
        def helper(batchOfEpisodes: Vector[Episode], valueFunction: ValueFunction): ValueFunction = {
            
            val updatedValueFunction = super.getValueFunctionForPolicy(policy, batchOfEpisodes, valueFunction)
            logger.trace(s"~updatedValueFunction=$updatedValueFunction")
            updatedValueFunction match {
                case batchUpdating: BatchUpdating => {
                    
                    val consolidatedBatchUpdating = batchUpdating.consolidateBatch
                    logger.trace(s"~Consolidated batch consolidatedBatchUpdating=$consolidatedBatchUpdating")
                    
                    val differenceBetweenValueFunctions = valueFunction.getDifference(consolidatedBatchUpdating)
                    logger.trace(s"~differenceBetweenValueFunctions=$differenceBetweenValueFunctions")
                    if (differenceBetweenValueFunctions >= convergenceThreshold) {
                        logger.trace(s"~No improvement updatedValueFunction=$updatedValueFunction")
                        consolidatedBatchUpdating
                    } else {
                        logger.trace(s"~Improvement continuing update updatedValueFunction=$updatedValueFunction")
                        helper(batchOfEpisodes, consolidatedBatchUpdating)
                    }
                }
            }
            
        }
        
        episodes.zipWithIndex.foldLeft(valueFunction)((valueFunction, episodeWithIndex) => {
            val index = episodeWithIndex._2
            val batchOfEpisodes = episodes.slice(0, index + 1) // +1 as slice second param is an until
            logger.debug(s"~index=$index, batchOfEpisodes=$batchOfEpisodes")
            helper(batchOfEpisodes, valueFunction)
        })
        
    }
    
    override def updateValueFunctionForEpisodeStep(valueFunction: ValueFunction, transitionReward: TransitionReward):
        ValueFunction = {
        
        valueFunction match {
            case batchUpdating: BatchUpdating => {
                val state = transitionReward._1.stateAction.state
                val target: Double = getTarget(transitionReward, batchUpdating)
                logger trace (s"~batchUpdating=$batchUpdating")
                val updatedBatchUpdating = batchUpdating.addBatchValueForState(state, target)
                logger trace (s"~state=$state, target=$target, updatedBatchUpdating=$updatedBatchUpdating")
                updatedBatchUpdating
            }
            case _ => {
                logger error (s"BatchUpdatingPolicyEvaluator expects a BatchUpdating, but got ${valueFunction.getClass}")
                valueFunction
            }
        }
    }
    
}
