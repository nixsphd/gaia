package ie.nix.rl.td

import ie.nix.rl.{Episodes, FiniteMDP}

object TemporalDifference {
    
    def Sarsa(environment: FiniteMDP with Episodes,
                                     discount: Double = 1): Sarsa = {
        new Sarsa(environment, discount)
    }
    
    def PolicyEvaluator(environment: FiniteMDP with Episodes,
                        initialValue: Double = 0,
                        discount: Double = 1,
                        stepSize: Double = 1): PolicyEvaluator = {
        new PolicyEvaluator(environment, initialValue, discount, stepSize)
    }
    
    def BatchUpdatingPolicyEvaluator(environment: FiniteMDP with Episodes,
                        initialValue: Double = 0,
                        discount: Double = 1,
                        stepSize: Double = 1,
                        convergenceThreshold: Double = 0d): BatchPolicyEvaluator = {
        new BatchPolicyEvaluator(environment, initialValue, discount, stepSize, convergenceThreshold)
    }
    
}