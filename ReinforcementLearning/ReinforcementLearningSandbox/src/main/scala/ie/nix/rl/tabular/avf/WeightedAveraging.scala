package ie.nix.rl.tabular.avf

import ie.nix.rl.tabular.avf.ActionValueFunction.stateActionValueMap
import ie.nix.rl.{FiniteMDP, StateAction}
import org.apache.logging.log4j.scala.Logging

object WeightedAveraging extends Logging {
    
    def apply(environment: FiniteMDP, initialValue: Double): WeightedAveraging = {
        new WeightedAveraging(stateActionValueMap(environment, initialValue), initialValue, Map())
    }
    
}

class WeightedAveraging private[avf](stateActionValueMap: Map[StateAction, Double],
                                     initialValue: Double,
                                     val stateActionWeightsMap: Map[StateAction, Double])
    extends ActionValueFunction(stateActionValueMap, initialValue) with Logging {
    
    override def toString: String = {
        stateActionValueMap.toVector.sortBy(_._1.toString).
            map(entry => "(" + entry._1 + "->" + entry._2 + "," +
            stateActionWeightsMap.getOrElse(entry._1, "n/a") + ") ").
            mkString
    }
    
    override def getValueForStateAction(stateAction: StateAction): Double = {
        (stateActionValueMap.get(stateAction), stateActionWeightsMap.get(stateAction)) match {
            case (Some(numerator), Some(denominator)) => {
                val valueForStateAction = numerator / denominator
                logger.trace(s"~stateAction=$stateAction, value=$valueForStateAction")
                valueForStateAction
            }
            case _ => 0
        }
    }
    
    override def setValueForStateAction(stateAction: StateAction, value: Double): WeightedAveraging = {
        setValueForStateAction(stateAction: StateAction, value: Double, weight = 1d)
    }
    
    def setValueForStateAction(stateAction: StateAction, value: Double, weight: Double): WeightedAveraging = {
        
        val initialStateActionValue = stateActionValueMap.getOrElse(stateAction, initialValue)
        val newStateActionValue = initialStateActionValue + (weight * value)
        
        val initialStateActionWeight = stateActionWeightsMap.getOrElse(stateAction, 0d)
        val newStateActionWeight = initialStateActionWeight + weight
        logger.info(s"~stateAction=$stateAction, newStateActionValue=$newStateActionValue, " +
            s"newStateActionWeight=$newStateActionWeight")
        
        new WeightedAveraging(stateActionValueMap + (stateAction -> newStateActionValue), initialValue,
            stateActionWeightsMap + (stateAction -> newStateActionWeight))
        
    }
    
}
