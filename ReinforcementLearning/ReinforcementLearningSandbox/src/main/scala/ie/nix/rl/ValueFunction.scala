package ie.nix.rl

trait ValueFunction {
    
    def updateValueForState(state: State, value: Double): ValueFunction
    
    def getValueForState(state: State): Double
    
}
