package ie.nix.rl.demos.random_walk

import ie.nix.rl.tabular.policy
import ie.nix.rl.tabular.policy.DeterministicPolicy

object Walker {
    
    def Lefty(randomWalk: RandomWalk): DeterministicPolicy = {
        randomWalk.getNonTerminalStates().foldLeft(policy.DeterministicPolicy(randomWalk))((policy, state) => {
            state match {
                case (tile: randomWalk.Tile) => {
                    policy.updateActionForState(tile, randomWalk.Left)
                }
            }
        })
    }
    
    def Righty(randomWalk: RandomWalk): DeterministicPolicy = {
        randomWalk.getNonTerminalStates().foldLeft(policy.DeterministicPolicy(randomWalk))((policy, state) => {
            state match {
                case (tile: randomWalk.Tile) => {
                    policy.updateActionForState(tile, randomWalk.Right)
                }
            }
        })
    }
}

