package ie.nix.rl.demos.black_jack

import ie.nix.rl.Environment.{Reward, findItemGivenProbability}
import ie.nix.rl.demos.black_jack.BlackJackTable.{Card, DRAW, LOOSE, SumOfCards, WIN}
import ie.nix.rl.{Action, Episodes, FiniteMDP, State, StateAction, Transition}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object BlackJackTable {
    
    type Card = Int
    type SumOfCards = Int
    
    private val WIN: Reward = 1
    private val DRAW: Reward = 0
    private val LOOSE: Reward = -1
    
    def apply(): BlackJackTable = {
        new BlackJackTable
    }
}

class BlackJackTable extends FiniteMDP with Episodes with Logging {
    
    for (sumOfPlayersCards: SumOfCards <- 12 to 21;
         playerHasUsableAce: Boolean <- List(true, false);
         dealersVisibleCard: Card <- 1 to 10;
         action: PlayerMove <- List(Hit, Stick)) {
        addStateAction(StateAction(BlackJackState(sumOfPlayersCards, playerHasUsableAce, dealersVisibleCard),action))
    }
    
    override def toString: String = {
        s"BlackJackTable"
    }
    
    override def getTransitionForStateAction(stateAction: StateAction): Transition = (stateAction) match {
        case StateAction(_, Stick) => Transition(stateAction, stateAction.state)
        case StateAction(blackJackState: BlackJackState, Hit) =>
            Transition(stateAction, getStateAfterPlayerHit(blackJackState))
        case _ =>
            logger.error(s"~Unexpected state ${stateAction.state.getClass}, require BlackJackState")
            Transition(stateAction, stateAction.state)
    }
    
    override def getInitialState(): BlackJackState = {
        
        def initTableHelper(currentState: BlackJackState): BlackJackState = currentState match {
            case BlackJackState(sumOfPlayersCards, _, _) if sumOfPlayersCards >= 12 =>
                currentState
            case _ =>
                initTableHelper(getStateAfterPlayerHit(currentState))
        }
        
        val dealerShownCard = dealNextCard
        val initialState = initTableHelper(BlackJackState(sumOfPlayersCards=0, playerHasUsableAce=false, dealerShownCard))
        initialState
        
    }
    
    def dealNextCard(): Card = {
        val cardProbabilities = Map((1 -> 1d / 13), (2 -> 1d / 13), (3 -> 1d / 13), (4 -> 1d / 13),
            (5 -> 1d / 13), (6 -> 1d / 13), (7 -> 1d / 13), (8 -> 1d / 13), (9 -> 1d / 13), (10 -> 4d / 13))
        val probability = Random.nextDouble()
        val nextCard = findItemGivenProbability[Int](cardProbabilities.toSeq, probability)
        logger.debug(s"~nextCard=$nextCard")
        nextCard
    }
    
    private def getStateAfterPlayerHit(currentState: BlackJackState): BlackJackState = {
        val nextPlayerCard: Card = dealNextCard()
        if (isUsableAce(nextPlayerCard, currentState.sumOfPlayersCards)) {
            getNextStateGivenUsableAce(currentState)
        } else {
            getNextStateGivenPlayerCard(currentState, nextPlayerCard)
        }
    }
    
    private def isUsableAce(nextPlayerCard: Card, sumOfPlayersCards: SumOfCards): Boolean = {
        // If the player holds an ace that he could count as 11 without going bust, then the ace is said to be
        // usable.
        nextPlayerCard == 1 && sumOfPlayersCards + 11 <= 21
    }
    
    private def getNextStateGivenPlayerCard(currentState: BlackJackState, playerCard: Card): BlackJackState = {
        if (isPlayerBustWithUsableAce(currentState, playerCard)) {
            currentState.copy(sumOfPlayersCards = currentState.sumOfPlayersCards + playerCard - 10,
                playerHasUsableAce = false)
    
        } else {
            currentState.copy(sumOfPlayersCards = currentState.sumOfPlayersCards + playerCard)
        }
    }
    
    private def isPlayerBustWithUsableAce(currentState: BlackJackState, playerCard: Card) = {
        currentState.sumOfPlayersCards + playerCard > 21 && currentState.playerHasUsableAce
    }
    
    private def getNextStateGivenUsableAce(currentState: BlackJackState): BlackJackState = {
        // In this case it is always counted as 11 because counting it as 1 would make the sum 11 or less, in which
        // case there is no decision to be made because, obviously, the player should always hit
        currentState.copy(sumOfPlayersCards = currentState.sumOfPlayersCards + 11, playerHasUsableAce = true)
    }
    
    
    override def getRewardForTransition(transition: Transition): Reward = transition match {
        case Transition(StateAction(_, Hit), newState: BlackJackState) if newState.sumOfPlayersCards > 21 => LOOSE
        case Transition(StateAction(_, Hit), _) => 0
        case Transition(StateAction(_, Stick), currentState: BlackJackState) => getRewardAfterDealerPlays(currentState)
    }
    
    private def getRewardAfterDealerPlays(currentState: BlackJackState): Reward = {
        // The dealer sticks on any sum of 17 or greater, and hits otherwise.
        // If the dealer goes bust, then the player wins; otherwise, the outcome—win, lose, or draw—is determined by
        // whose final sum is closer to 21.
        def dealerPlayHelper(sumOfPlayersCards: SumOfCards, sumOfDealersCards: SumOfCards, dealerHasUsableAce: Boolean): Reward = {
            logger.trace(s"~sumOfPlayersCards=$sumOfPlayersCards, sumOfDealersCards=$sumOfDealersCards");
            (sumOfPlayersCards, sumOfDealersCards) match {
                case _ if sumOfDealersCards > 21 && !dealerHasUsableAce => WIN
                case _ if sumOfDealersCards > 21 && dealerHasUsableAce =>
                    dealerPlayHelper(sumOfPlayersCards, sumOfDealersCards + dealNextCard - 10, false)
                case _ if sumOfDealersCards >= 17 && sumOfPlayersCards > sumOfDealersCards => WIN
                case _ if sumOfDealersCards >= 17 && sumOfPlayersCards == sumOfDealersCards => DRAW
                case _ if sumOfDealersCards >= 17 && sumOfPlayersCards < sumOfDealersCards => LOOSE
                case _ => dealerPlayHelper(sumOfPlayersCards, sumOfDealersCards + dealNextCard, dealerHasUsableAce)
            }
        }
        val dealerHiddenCard = dealNextCard
        val dealerHasUsableAce = currentState.dealersVisibleCard == 1 || dealerHiddenCard == 1
        if (dealerHasUsableAce) {
            val reward = dealerPlayHelper(currentState.sumOfPlayersCards,
                currentState.dealersVisibleCard + dealerHiddenCard + 10, dealerHasUsableAce)
            logger.trace(s"~reward=$reward");
            reward
        } else {
            val reward = dealerPlayHelper(currentState.sumOfPlayersCards,
                currentState.dealersVisibleCard + dealerHiddenCard, dealerHasUsableAce)
            logger.trace(s"~reward=$reward");
            reward
        }
        
    }
    
    case class BlackJackState(sumOfPlayersCards: SumOfCards, playerHasUsableAce: Boolean,
                              dealersVisibleCard: Card) extends State {
        
        override def toString: String = s"BlackJackState($sumOfPlayersCards, $playerHasUsableAce, $dealersVisibleCard)"
        
        override def isTerminal: Boolean = sumOfPlayersCards > 21
        
    }
    
    sealed trait PlayerMove extends Action
    
    case object Hit extends PlayerMove {
        override def toString: String = "Hit"
    }
    
    case object Stick extends PlayerMove {
        override def toString: String = "Stick"
        override def isTerminal: Boolean = true
    }
    
}