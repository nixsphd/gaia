package ie.nix.util

import java.nio.file.{FileSystems, Files}

import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class CSVFileWriterImplTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  val logDirName = "/tmp"
  val logFileName = "testWriteHeaders.csv"

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  override def afterEach(): Unit = {
    cleanUpLogFile
  }

  private def cleanUpLogFile = {
    val logFilePath = FileSystems.getDefault.getPath(logDirName).resolve(logFileName)
    Files.deleteIfExists(logFilePath)
  }

  behavior of "CSVFileWriterImpl"

  it should "write string headers" in {
    val headerString = "A, B, C"
    csvFileWriter.writeHeaders(headerString)
    assert(getLines.contains(headerString))
  }

  it should "write list Headers" in {
    val headerList = List("A", "B", "C")
    csvFileWriter.writeHeaders(headerList)
    assert(getLines.contains(getListAsString(headerList)))
  }

  it should "write Headers" in {
    csvFileWriter.writeHeaders("A", "B", "C")
    assert(getLines.contains(getListAsString(List("A", "B", "C"))))
  }

  private def csvFileWriter = {
    CSVFileWriter(logDirName, logFileName)
  }

  private def getLines = {
    val logFilePath = FileSystems.getDefault.getPath(logDirName).resolve(logFileName)
    Files.readAllLines(logFilePath)
  }

  private def getListAsString(list: List[String]) = {
    list.mkString(sep = ",")
  }

  it should "write string row" in {
    val rowString = "a, b, c"
    csvFileWriter.writeRow(rowString)
    assert(getLines.contains(rowString))
  }

  it should "write list row" in {
    val rowList = List("a", "b", "c")
    csvFileWriter.writeRow(rowList)
    assert(getLines.contains(getListAsString(rowList)))
  }

  it should "write row" in {
    csvFileWriter.writeRow("a", "b", "c")
    assert(getLines.contains(getListAsString(List("a", "b", "c"))))
  }

}
