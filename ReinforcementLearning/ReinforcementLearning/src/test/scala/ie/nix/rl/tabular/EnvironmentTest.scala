package ie.nix.rl.tabular

import ie.nix.rl.Environment.{Reward, StateAction, Transition}
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

object EnvironmentTest {

  import org.scalactic.TypeCheckedTripleEquals._

  val TARGET_ID = 4

  def environment: TestEnvironment = new TestEnvironment(stateActions, terminalStates)
  def terminalStates: Set[State] = Set[State](TestState(0), TestState(4))
  def stateActions: Set[StateAction] = Set[StateAction](
    StateAction(TestState(1), TestAction(1)),
    StateAction(TestState(2), TestAction(1)),
    StateAction(TestState(2), TestAction(2)),
    StateAction(TestState(3), TestAction(1))
  )

  case class TestState(id: Int) extends State {
    override def toString: String = s"TestState($id)"

    override def isTerminal: Boolean = id === 0 || id >= TARGET_ID
  }

  case class TestAction(id: Int) extends Action {
    override def toString: String = s"TestAction($id)"
  }

  class TestEnvironment(override val stateActionsSet: Set[StateAction], override val terminalStateSet: Set[State])
      extends Environment(stateActionsSet, terminalStateSet)
      with Episodes {

    def this() = {
      this(stateActions, terminalStates)
    }

    override def getTransitionForStateAction(stateAction: StateAction): Transition =
      stateAction match {
        case StateAction(state: TestState, action: TestAction) =>
          Transition(stateAction, TestState(state.id + action.id))
        case _ => Transition(stateAction, stateAction.state)
      }

    override def getRewardForTransition(transition: Transition): Reward =
      transition match {
        case Transition(_, finalState: TestState) if finalState.id >= 4 => 1d
        case _                                                          => 0d
      }

    override def getInitialState(): State = {
      TestState(1)
    }
  }

}

@RunWith(classOf[JUnitRunner])
class EnvironmentTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "EnvironmentTest"

  it should "get Actions For State" in {

    val actions = environment.getActionsForState(TestState(2))

    assert(actions.size === 2)
    assert(actions.contains(TestAction(1)))
    assert(actions.contains(TestAction(2)))

  }

  it should "get StateActions" in {

    val stateActions = environment.getStateActions

    assert(stateActions.size === 4)
    assert(stateActions.contains(StateAction(TestState(1), TestAction(1))))
    assert(stateActions.contains(StateAction(TestState(2), TestAction(1))))
    assert(stateActions.contains(StateAction(TestState(2), TestAction(2))))
    assert(stateActions.contains(StateAction(TestState(3), TestAction(1))))

  }

  it should "get StateActions For State" in {

    val stateActions = environment.getStateActionsForState(TestState(2))

    assert(stateActions.size === 2)
    assert(stateActions.contains(StateAction(TestState(2), TestAction(1))))
    assert(stateActions.contains(StateAction(TestState(2), TestAction(2))))

  }

  it should "get States" in {

    val states = environment.getStates

    assert(states.size === 5)
    assert(states.contains(TestState(0)))
    assert(states.contains(TestState(1)))
    assert(states.contains(TestState(2)))
    assert(states.contains(TestState(3)))
    assert(states.contains(TestState(4)))
  }

  it should "get NonTerminal States" in {

    val nonTerminalStates = environment.getNonTerminalStates

    assert(nonTerminalStates.size === 3)
    assert(nonTerminalStates.contains(TestState(1)))
    assert(nonTerminalStates.contains(TestState(2)))
    assert(nonTerminalStates.contains(TestState(3)))
  }

  it should "get TransitionReward with 0 reward For StateAction" in {

    val transitionReward = environment.getTransitionRewardForStateAction(StateAction(TestState(1), TestAction(1)))

    assert(transitionReward.transition.finalState === TestState(2))
    assert(transitionReward.reward === 0d)
  }

  it should "get TransitionReward with 1 reward For StateAction" in {

    val transitionReward = environment.getTransitionRewardForStateAction(StateAction(TestState(2), TestAction(2)))

    assert(transitionReward.transition.finalState === TestState(4))
    assert(transitionReward.reward === 1d)
  }

}
