package ie.nix.rl.tabular.examples.gambler

import ie.nix.rl.Environment.{StateAction, Transition}
import ie.nix.rl.tabular.examples.gambler.Casino.{GamblersBet, GamblersCapitol}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class CasinoTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "CasinoTest"

  it should "have 5 states" in {

    val states = casino.getStates
    logger.debug(s"~states=$states")

    assert(states.length === 5)
    assert(states.contains(GamblersCapitol(0, isTerminal = true)))
    assert(states.contains(GamblersCapitol(1)))
    assert(states.contains(GamblersCapitol(2)))
    assert(states.contains(GamblersCapitol(3)))
    assert(states.contains(GamblersCapitol(4, isTerminal = true)))

  }

  it should "have 2 terminal states" in {

    val terminalStates = casino.getTerminalStates
    logger.debug(s"~terminalStates=$terminalStates")

    assert(terminalStates.length === 2)
    assert(terminalStates.contains(GamblersCapitol(0, isTerminal = true)))
    assert(terminalStates.contains(GamblersCapitol(4, isTerminal = true)))

  }

  it should "have 3 non-terminal states" in {

    val nonTerminalStates = casino.getNonTerminalStates
    logger.debug(s"~nonTerminalStates=$nonTerminalStates")

    assert(nonTerminalStates.length === 3)
    assert(nonTerminalStates.contains(GamblersCapitol(1)))
    assert(nonTerminalStates.contains(GamblersCapitol(2)))
    assert(nonTerminalStates.contains(GamblersCapitol(3)))

  }

  it should "have 4 state actions states" in {

    val stateActions = casino.getStateActions
    logger.debug(s"~stateActions=$stateActions")

    assert(stateActions.length === 4)
    assert(stateActions.contains(StateAction(GamblersCapitol(1), GamblersBet(1))))
    assert(stateActions.contains(StateAction(GamblersCapitol(2), GamblersBet(1))))
    assert(stateActions.contains(StateAction(GamblersCapitol(2), GamblersBet(2))))
    assert(stateActions.contains(StateAction(GamblersCapitol(3), GamblersBet(1))))

  }

  private def casino: Casino = {
    val targetCapitol = 4
    val probabilityHeads = 0.4
    Casino(targetCapitol, probabilityHeads)
  }

  it should "get Transition For StateAction with heads toss" in {

    val transition = headTossingCasino.getTransitionForStateAction(StateAction(GamblersCapitol(1), GamblersBet(1)))
    logger.debug(s"~transition=$transition")

    assert(transition.finalState === GamblersCapitol(2))

  }

  private def headTossingCasino: Casino = {
    val targetCapitol = 4
    val probabilityHeads = 1d
    Casino(targetCapitol, probabilityHeads)
  }

  it should "get Transition For StateAction with tail toss" in {

    val transition = tailTossingCasino.getTransitionForStateAction(StateAction(GamblersCapitol(1), GamblersBet(1)))
    logger.trace(s"~transition=$transition")

    assert(transition.finalState === GamblersCapitol(0, isTerminal = true))

  }

  private def tailTossingCasino: Casino = {
    val targetCapitol = 4
    val probabilityHeads = 0d
    Casino(targetCapitol, probabilityHeads)
  }

  it should "get getReward of 0 For Transition" in {

    val transition = Transition(StateAction(GamblersCapitol(1), GamblersBet(1)), GamblersCapitol(2))

    val reward = casino.getRewardForTransition(transition)
    logger.debug(s"~transition=$transition, reward=$reward")

    assert(reward === 0)

  }

  it should "get getReward of 0 For Transition when final state is 0" in {

    val transition = Transition(StateAction(GamblersCapitol(1), GamblersBet(1)), GamblersCapitol(0))

    val reward = casino.getRewardForTransition(transition)
    logger.debug(s"~transition=$transition, reward=$reward")

    assert(reward === 0)

  }

  it should "get getReward of 1 For Transition when final state is 4, target capitol" in {

    val transition = Transition(StateAction(GamblersCapitol(2), GamblersBet(2)), GamblersCapitol(4, isTerminal = true))

    val reward = casino.getRewardForTransition(transition)
    logger.debug(s"~transition=$transition, reward=$reward")

    assert(reward === 1)

  }

  it should "have remain state transition probability of 1 for a no bet" in {
    val noBetTransition = Transition(StateAction(GamblersCapitol(1), GamblersBet(0)), GamblersCapitol(1))
    val noBetTransitionProbability = casino.getProbabilityOfTransition(noBetTransition)
    logger.info(s"~noBetTransition)=$noBetTransition, noBetTransitionProbability=$noBetTransitionProbability")
    assert(noBetTransitionProbability === 1d)
  }

  it should "change state transition probability of 1 for a no bet" in {
    val noBetTransition = Transition(StateAction(GamblersCapitol(1), GamblersBet(0)), GamblersCapitol(2))
    val noBetTransitionProbability = casino.getProbabilityOfTransition(noBetTransition)
    logger.trace(s"~noBetTransition)=$noBetTransition, noBetTransitionProbability=$noBetTransitionProbability")
    assert(noBetTransitionProbability === 1d)
  }

  it should "have state transition probability of 0.4 to the win state" in {
    val winTransition = Transition(StateAction(GamblersCapitol(1), GamblersBet(1)), GamblersCapitol(2))
    val winTransitionProbability = casino.getProbabilityOfTransition(winTransition)
    logger.trace(s"~winTransition)=$winTransition, winTransitionProbability=$winTransitionProbability")
    assert(winTransitionProbability === 0.4)
  }

  it should "have state transition probability of 0.6 to the loose state" in {
    val looseTransition = Transition(StateAction(GamblersCapitol(1), GamblersBet(1)), GamblersCapitol(0))
    val looseTransitionProbability = casino.getProbabilityOfTransition(looseTransition)
    logger.trace(s"~looseTransition)=$looseTransition, looseTransitionProbability=$looseTransitionProbability")
    assert(looseTransitionProbability === 0.6)
  }

  it should "have no state transition probability for any other state" in {
    val noTransition = Transition(StateAction(GamblersCapitol(1), GamblersBet(1)), GamblersCapitol(3))
    val noTransitionProbability = casino.getProbabilityOfTransition(noTransition)
    logger.trace(s"~noTransition)=$noTransition, noTransitionProbability=$noTransitionProbability")
    assert(noTransitionProbability === 0d)
  }

  it should "have no reward for a state less than the targetCapitol" in {
    val notTotargetCapitolTransition = Transition(StateAction(GamblersCapitol(1), GamblersBet(1)), GamblersCapitol(2))
    val notTotargetCapitolTransitionReward = casino.getRewardForTransition(notTotargetCapitolTransition)
    logger.trace(
      s"~notTotargetCapitolTransition)=$notTotargetCapitolTransition, notTotargetCapitolTransitionReward=$notTotargetCapitolTransitionReward")
    assert(notTotargetCapitolTransitionReward === 0d)
  }

  it should "have a reward of 1 for the targetCapitol state" in {
    val totargetCapitolTransition =
      Transition(StateAction(GamblersCapitol(2), GamblersBet(2)), GamblersCapitol(4, isTerminal = true))
    val totargetCapitolTransitionReward = casino.getRewardForTransition(totargetCapitolTransition)
    logger.trace(
      s"~totargetCapitolTransition)=$totargetCapitolTransition, totargetCapitolTransitionReward=$totargetCapitolTransitionReward")
    assert(totargetCapitolTransitionReward === 1d)
  }

}
