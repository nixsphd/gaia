package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ConstantStepSizeTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "ConstantStepSize"

  it should "create ConstantStepSize for a policy" in {

    val policy = randomPolicy
    val stepSize = 0.1
    val constantStepSize = ConstantStepSize(policy, stepSize)
    logger.info(s"policy=$policy, constantStepSize=$constantStepSize")

    assert(constantStepSize.getStateActions.size === policy.getStateActions.size)
    assert(constantStepSize.getValue(StateAction(TestState(1), TestAction(1))) !== 0d)
    assert(constantStepSize.getValue(StateAction(TestState(2), TestAction(1))) !== 0d)
    assert(constantStepSize.getValue(StateAction(TestState(2), TestAction(2))) !== 0d)

  }

  def randomPolicy: Policy = {
    RandomPolicy
  }

  it should "update Value" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(stateAction, 2d)
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(stateAction) === 0.2)

  }

  it should "update Value twice" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(stateAction, 2d)
      .updateValue(stateAction, 3d)
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    // V(St) ← V(St) + α􏰖[target − V(St)􏰗] = 0.48 <- 0.2 + 0.1[3 - 0.2]
    assert(updatedActionValueFunction.getValue(stateAction) === 0.48)

  }

  def actionValueFunction: ConstantStepSize = {
    val stepSize = 0.1
    val initialValue = 0d
    ConstantStepSize(randomPolicy, initialValue, stepSize)
  }

  def stateAction: StateAction = StateAction(TestState(1), TestAction(1))

}
