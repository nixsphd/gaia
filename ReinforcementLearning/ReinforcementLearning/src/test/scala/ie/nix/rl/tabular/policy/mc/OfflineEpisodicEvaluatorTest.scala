package ie.nix.rl.tabular.policy.mc

import ie.nix.rl.Environment.{StateAction, Transition, TransitionReward}
import ie.nix.rl.policy.{Determinism, Policy}
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.policy.EpisodicPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.avf.WeightedAveraging
import ie.nix.rl.tabular.policy.{DeterministicPolicy, SoftPolicy}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class OfflineEpisodicEvaluatorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes

  override def beforeEach(): Unit = Random.setSeed(0)

  private def behaviourPolicy = SoftPolicy(probabilityOfRandomAction = 0.1).updateAction(TestState(2), TestAction(1))

  private def offlineEpisodicEvaluator =
    OfflineEpisodicEvaluator[Policy with Determinism, SoftPolicy](initSoftBehaviourPolicy, discount = 1d)

  private def initSoftBehaviourPolicy: Policy with Determinism => SoftPolicy =
    targetPolicy => SoftPolicy(probabilityOfRandomAction = 0.1, targetPolicy)

  private def weightedAveraging = WeightedAveraging(targetPolicy)

  private def targetPolicy: Policy with Determinism = DeterministicPolicy.apply

  private def state1Action1 = StateAction(TestState(1), TestAction(1))

  private def state2Action1 = StateAction(TestState(2), TestAction(1))

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  private def state3Action1 = StateAction(TestState(3), TestAction(1))

  behavior of "OfflineEpisodicEvaluator"

  it should "get New Weight for hard action" in {

    val policy = behaviourPolicy
    val newWeight = offlineEpisodicEvaluator.getNewWeight(policy, state1Action1, 1d)
    logger info s"~policy=$policy, newWeight=$newWeight"

    assert(newWeight === 1d)
  }

  it should "get New Weight for soft action" in {

    val policy = behaviourPolicy
    val newWeight = offlineEpisodicEvaluator.getNewWeight(policy, state2Action2, 1d)
    logger info s"~policy=$policy, newWeight=$newWeight"

    assert(newWeight > 1d)
  }

  it should "get New Total Rewards with no discount" in {

    val newTotalRewards = offlineEpisodicEvaluator.getNewTotalRewards(totalRewards = 10d, reward = 2d)

    assert(newTotalRewards === 12d)

  }

  it should "get New Total Rewards with a discount of 0.9" in {

    val offlineEpisodicEvaluator =
      OfflineEpisodicEvaluator[Policy with Determinism, SoftPolicy](initSoftBehaviourPolicy, discount = 0.9)
    val newTotalRewards = offlineEpisodicEvaluator.getNewTotalRewards(totalRewards = 10d, reward = 2d)

    assert(newTotalRewards === 11d)

  }

  it should "update ActionValueFunction probably action" in {

    val transitionRewards =
      Vector(TransitionReward(Transition(state2Action2, TestState(4)), 2d))
    val newWeightedAveraging =
      offlineEpisodicEvaluator.updateActionValueFunction(targetPolicy,
                                                         behaviourPolicy,
                                                         weightedAveraging,
                                                         transitionRewards)
    logger info s"~newWeightedAveraging=$newWeightedAveraging"

    assert(newWeightedAveraging.getValue(state2Action2) === 2d)
    assert(newWeightedAveraging.valueMap.getOrElse(state2Action2, (0d, 0d))._1 === 2d)
    assert(newWeightedAveraging.valueMap.getOrElse(state2Action2, (0d, 0d))._2 === 20d)

  }

  it should "update ActionValueFunction less probably action" in {

    val transitionRewards =
      Vector(TransitionReward(Transition(state2Action1, TestState(3)), 2d))
    val newWeightedAveraging =
      offlineEpisodicEvaluator.updateActionValueFunction(targetPolicy,
                                                         behaviourPolicy,
                                                         weightedAveraging,
                                                         transitionRewards)
    logger info s"~newWeightedAveraging=$newWeightedAveraging"

    assert(newWeightedAveraging.getValue(state2Action1) === 2d)
    assert(newWeightedAveraging.valueMap.getOrElse(state2Action1, (0d, 0d))._1 === 2d)
    assert(newWeightedAveraging.valueMap.getOrElse(state2Action1, (0d, 0d))._2 === 1.0526315789473684)

  }

  it should "update ActionValueFunction twice" in {

    val transitionRewards = Vector(TransitionReward(Transition(state1Action1, TestState(2)), 0d),
                                   TransitionReward(Transition(state2Action2, TestState(4)), 2d))
    val newWeightedAveraging =
      offlineEpisodicEvaluator.updateActionValueFunction(targetPolicy,
                                                         behaviourPolicy,
                                                         weightedAveraging,
                                                         transitionRewards)
    logger info s"~newWeightedAveraging=$newWeightedAveraging"

    assert(newWeightedAveraging.valueMap.getOrElse(state2Action2, (0d, 0d))._1 === 2d)
    assert(newWeightedAveraging.valueMap.getOrElse(state2Action2, (0d, 0d))._2 === 20d)

  }

  it should "update ActionValueFunction twice only the tail" in {

    val transitionRewards =
      Vector(
        TransitionReward(Transition(state1Action1, TestState(2)), 0d),
        TransitionReward(Transition(state2Action1, TestState(3)), 0d),
        TransitionReward(Transition(state3Action1, TestState(4)), 2d),
      )
    val newWeightedAveraging =
      offlineEpisodicEvaluator.updateActionValueFunction(targetPolicy,
                                                         behaviourPolicy,
                                                         weightedAveraging,
                                                         transitionRewards)
    logger info s"~newWeightedAveraging=$newWeightedAveraging"

    assert(newWeightedAveraging.valueMap.getOrElse(state1Action1, (0d, 0d))._1 === 0d)
    assert(newWeightedAveraging.valueMap.getOrElse(state1Action1, (0d, 0d))._2 === 0d)

  }

  it should "evaluatePolicy with 1 episode" in {

    val policy = DeterministicPolicy.apply.updateAction(TestState(2), TestAction(2))

    val (newActionValueFunction, statesVisited) =
      offlineEpisodicEvaluator.evaluatePolicy(policy, weightedAveraging, state1Action1)
    logger trace s"~newActionValueFunction=$newActionValueFunction"
    logger trace s"~statesVisited=$statesVisited"

    assert(statesVisited.contains(TestState(1)))
    assert(statesVisited.contains(TestState(2)))

    assert(newActionValueFunction.getValue(state1Action1) === 1d)
    assert(newActionValueFunction.getValue(state2Action1) === 0d)
    assert(newActionValueFunction.getValue(state2Action2) === 1d)

  }

}
