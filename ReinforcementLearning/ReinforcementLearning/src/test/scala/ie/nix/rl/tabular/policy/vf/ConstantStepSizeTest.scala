package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ConstantStepSizeTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "ConstantStepSize"

  it should "create ConstantStepSize for a policy" in {

    val policy = randomPolicy
    val stepSize = 0.1
    val constantStepSize = ConstantStepSize(policy, stepSize)
    logger.info(s"policy=$policy, constantStepSize=$constantStepSize")

    assert(constantStepSize.getStates.size === policy.getStates.size)
    assert(constantStepSize.getValue(TestState(0)) === 0d)
    assert(constantStepSize.getValue(TestState(1)) !== 0d)
    assert(constantStepSize.getValue(TestState(2)) !== 0d)

  }

  def randomPolicy: Policy = {
    RandomPolicy
  }

  it should "update Value" in {

    val updatedValueFunction = valueFunction.updateValue(TestState(1), 2d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 0.2)

  }

  it should "update Value twice" in {

    val updatedValueFunction = valueFunction
      .updateValue(TestState(1), 2d)
      .updateValue(TestState(1), 3d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    // V(St) ← V(St) + α􏰖[target − V(St)􏰗] = 0.48 <- 0.2 + 0.1[3 - 0.2]
    assert(updatedValueFunction.getValue(TestState(1)) === 0.48)

  }

  def valueFunction: ConstantStepSize = {
    val stepSize = 0.1
    val initialValue = 0d
    ConstantStepSize(randomPolicy, initialValue, stepSize)
  }

}
