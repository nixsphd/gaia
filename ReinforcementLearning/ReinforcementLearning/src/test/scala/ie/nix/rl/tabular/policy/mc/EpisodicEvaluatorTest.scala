package ie.nix.rl.tabular.policy.mc

import ie.nix.rl.Environment.{StateAction, Transition, TransitionReward}
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.EpisodicPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.avf.SampleAveraging
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator.{ConstantStepSizeEpisodicEvaluator, runEpisode}
import ie.nix.rl.tabular.policy.vf.ConstantStepSize
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class EpisodicEvaluatorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes

  override def beforeEach(): Unit = Random.setSeed(0)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def episodicEvaluator = EpisodicEvaluator[DeterministicPolicy](discount = 1d)

  private def constantStepSizeEpisodicEvaluator = ConstantStepSizeEpisodicEvaluator[DeterministicPolicy](discount = 1d)

  private def sampleAveraging = SampleAveraging(deterministicPolicy)

  private def constantStepSize = ConstantStepSize(deterministicPolicy, initialValue = 0d, stepSize = 0.1)

  private def state1Action1 = StateAction(TestState(1), TestAction(1))

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  private def state3Action1 = StateAction(TestState(3), TestAction(1))

  behavior of "EpisodicEvaluator"

  it should "runEpisode with policy" in {

    val episode = state1Action1: Episode
    logger.info(s"~episode=$episode")

    val transitionRewards = runEpisode(episode, deterministicPolicy)
    logger.info(s"~transitionRewards=$transitionRewards")

    val visitedStateActions = transitionRewards.map(_.transition.stateAction)
    val rewards = transitionRewards.map(_.reward)
    assert(visitedStateActions.contains(episode))
    assert(visitedStateActions.contains(state2Action2))
    assert(rewards.sum === 1d)

  }

  it should "update ActionValueFunction with one transition" in {

    val transitionRewards: Vector[TransitionReward] =
      Vector(TransitionReward(Transition(StateAction(TestState(3), TestAction(1)), TestState(4)), 1d))

    val updatedActionValueFunction = episodicEvaluator.updateValueFunction(sampleAveraging, transitionRewards)
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(1))) === 1d)
  }

  it should "update ActionValueFunction with two transition" in {

    val transitionRewards: Vector[TransitionReward] =
      Vector(
        TransitionReward(Transition(StateAction(TestState(2), TestAction(1)), TestState(3)), 0d),
        TransitionReward(Transition(StateAction(TestState(3), TestAction(1)), TestState(4)), 1d)
      )

    val updatedActionValueFunction = episodicEvaluator.updateValueFunction(sampleAveraging, transitionRewards)
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 1d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(1))) === 1d)
  }

  it should "update ActionValueFunction with one transition with initial ActionValueFunction not 0" in {

    val initialActionValueFunction = sampleAveraging.updateValue(StateAction(TestState(3), TestAction(1)), 2d)
    val transitionRewards: Vector[TransitionReward] =
      Vector(TransitionReward(Transition(StateAction(TestState(3), TestAction(1)), TestState(4)), 1d))

    val updatedActionValueFunction =
      episodicEvaluator.updateValueFunction(initialActionValueFunction, transitionRewards)
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(1))) === 1.5)
  }

  it should "evaluate Policy from start 3,1" in {

    val (updatedActionValueFunction, visitedStates) =
      episodicEvaluator.evaluatePolicy(deterministicPolicy, sampleAveraging, state3Action1)
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"
    logger info s"~visitedStates=$visitedStates"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(1))) === 1d)
    assert(visitedStates.size === 1)
    assert(visitedStates.contains(TestState(3)))
  }

  it should "evaluate Policy from start 1,1" in {

    val (updatedActionValueFunction, visitedStates) =
      episodicEvaluator.evaluatePolicy(deterministicPolicy, sampleAveraging, state1Action1)
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"
    logger info s"~visitedStates=$visitedStates"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 1d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 0d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 1d)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(1))) === 0d)
    assert(visitedStates.size === 2)
    assert(visitedStates.contains(TestState(1)))
    assert(visitedStates.contains(TestState(2)))
  }

  it should "evaluatePolicy with an ConstantStepSize EpisodicEvaluator one step" in {

    val (updatedConstantStepSize, visitedStates) =
      constantStepSizeEpisodicEvaluator.evaluatePolicy(deterministicPolicy, constantStepSize, state2Action2)
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"
    logger info s"~visitedStates=$visitedStates"

    assert(updatedConstantStepSize.getValue(TestState(1)) === 0d)
    assert(updatedConstantStepSize.getValue(TestState(2)) === 0.1)
    assert(updatedConstantStepSize.getValue(TestState(3)) === 0d)
    assert(visitedStates.size === 1)
    assert(visitedStates.contains(TestState(2)))
  }

  it should "evaluatePolicy with an ConstantStepSize EpisodicEvaluator two steps" in {

    val (updatedConstantStepSize, visitedStates) =
      constantStepSizeEpisodicEvaluator.evaluatePolicy(deterministicPolicy, constantStepSize, state1Action1)
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"
    logger info s"~visitedStates=$visitedStates"

    assert(updatedConstantStepSize.getValue(TestState(1)) === 0.1)
    assert(updatedConstantStepSize.getValue(TestState(2)) === 0.1)
    assert(updatedConstantStepSize.getValue(TestState(3)) === 0d)
    assert(visitedStates.size === 2)
    assert(visitedStates.contains(TestState(1)))
    assert(visitedStates.contains(TestState(2)))
  }

  it should "evaluatePolicy with an ConstantStepSize EpisodicEvaluator one steps with initialValue 1" in {

    val constantStepSize = ConstantStepSize(deterministicPolicy, initialValue = 0.5, stepSize = 0.1)
    val (updatedConstantStepSize, visitedStates) =
      constantStepSizeEpisodicEvaluator.evaluatePolicy(deterministicPolicy, constantStepSize, state2Action2)
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"
    logger info s"~visitedStates=$visitedStates"

    assert(updatedConstantStepSize.getValue(TestState(1)) === 0.5)
    assert(updatedConstantStepSize.getValue(TestState(2)) === 0.55)
    assert(updatedConstantStepSize.getValue(TestState(3)) === 0.5)
    assert(visitedStates.size === 1)
    assert(visitedStates.contains(TestState(2)))
  }

  it should "evaluatePolicy with an ConstantStepSize EpisodicEvaluator two steps with initialValue 1" in {

    val constantStepSize = ConstantStepSize(deterministicPolicy, initialValue = 0.5, stepSize = 0.1)
    val (updatedConstantStepSize, visitedStates) =
      constantStepSizeEpisodicEvaluator.evaluatePolicy(deterministicPolicy, constantStepSize, state1Action1)
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"
    logger info s"~visitedStates=$visitedStates"

    assert(updatedConstantStepSize.getValue(TestState(1)) === 0.55)
    assert(updatedConstantStepSize.getValue(TestState(2)) === 0.55)
    assert(updatedConstantStepSize.getValue(TestState(3)) === 0.5)
    assert(visitedStates.size === 2)
    assert(visitedStates.contains(TestState(1)))
    assert(visitedStates.contains(TestState(2)))
  }

}
