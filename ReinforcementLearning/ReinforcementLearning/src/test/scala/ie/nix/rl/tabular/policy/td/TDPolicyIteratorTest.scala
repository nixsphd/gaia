package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment.{Reward, StateAction, Transition}
import ie.nix.rl.State
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestEnvironment, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.policy.avf.ConstantStepSize
import ie.nix.rl.tabular.policy.td.TDPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.{DeterministicPolicy, SoftPolicy, avf}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

object TDPolicyIteratorTest {

  def environmentWithEpisodes: TestEnvironmentWithEpisodes = {
    val terminalStates = Set[State](TestState(0), TestState(4), TestState(5))
    val stateActions = Set[StateAction](
      StateAction(TestState(1), TestAction(1)),
      StateAction(TestState(2), TestAction(1)),
      StateAction(TestState(2), TestAction(2)),
      StateAction(TestState(3), TestAction(1)),
      StateAction(TestState(3), TestAction(2))
    )
    new TestEnvironmentWithEpisodes(stateActions, terminalStates)
  }

  class TestEnvironmentWithEpisodes(override val stateActionsSet: Set[StateAction],
                                    override val terminalStateSet: Set[State])
      extends TestEnvironment(stateActionsSet, terminalStateSet)
      with Episodes {

    import org.scalactic.TypeCheckedTripleEquals._

    override def getInitialState(): State = TestState(1)

    override def getRewardForTransition(transition: Transition): Reward =
      transition match {
        case Transition(_, finalState: TestState) if finalState.id > 4   => -1d
        case Transition(_, finalState: TestState) if finalState.id === 4 => 1d
        case _                                                           => 0d
      }

  }

}

@RunWith(classOf[JUnitRunner])
class TDPolicyIteratorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes
  private val initialActionValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = policy =>
    avf.ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.1)

  override def beforeEach(): Unit = Random.setSeed(0)

  private def tdPolicyIterator =
    new TDPolicyIterator(episodes, initialActionValueFunction, discount = 1d) {
      override def updateActionValueFunction(actionActionValueFunction: ConstantStepSize,
                                             stateAction: StateAction,
                                             maybeNextStateAction: Option[StateAction],
                                             target: Double): ConstantStepSize =
        actionActionValueFunction.updateValue(stateAction, target)

      override def getTarget(reward: Reward,
                             actionValueFunction: ConstantStepSize,
                             stateAction: StateAction,
                             maybeNextStateAction: Option[StateAction]): Reward = {
        //   SARSA - Q(S, A) ← Q(S,A) + α[R + γQ(S′,A′) − Q(S, A)]
        val nextStateActionValue = maybeNextStateAction match {
          case Some(nextStateAction) => actionValueFunction.getValue(nextStateAction)
          case None                  => 0d
        }
        reward + (discount * nextStateActionValue) // R + γQ(S′,A′)
      }
    }

  private def episodes = environment.getStarts(2000)

  private def constantStepSize = ConstantStepSize(deterministicPolicy, initialValue = 0.5, stepSize = 0.1)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def softPolicy: SoftPolicy = SoftPolicy(probabilityOfRandomAction = 0.1)

  private def state1Action1 = StateAction(TestState(1), TestAction(1))

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  private def state3Action2 = StateAction(TestState(3), TestAction(2))

  behavior of "TDPolicyIterator"

  it should "getTarget with reward od 1" in {

    val target = tdPolicyIterator.getTarget(1d, constantStepSize, state1Action1, Some(state2Action2))
    logger info s"~target=$target"

    assert(target === 1.5)
  }

  it should "getTarget with reward of 0" in {

    val target = tdPolicyIterator.getTarget(0d, constantStepSize, state1Action1, Some(state2Action2))
    logger info s"~target=$target"

    assert(target === 0.5)
  }

  it should "evaluate And Improve Policy last step" in {

    val (improvedPolicy, updatedAVF) =
      tdPolicyIterator.evaluateAndImprovePolicy(softPolicy, constantStepSize, state2Action2)
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedAVF=$updatedAVF"

    assert(updatedAVF.getValue(state2Action2) === 0.55)
  }

  it should "evaluate And Improve Policy" in {

    val (improvedPolicy, updatedAVF) =
      tdPolicyIterator.evaluateAndImprovePolicy(softPolicy, constantStepSize, state1Action1)
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedAVF=$updatedAVF"

    assert(updatedAVF.getValue(state3Action2) === 0.35)
  }

  it should "iterate" in {

    val policy = softPolicy.updateAction(TestState(2), TestAction(1))
    val (improvedPolicy, updatedAVF) =
      tdPolicyIterator.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedAVF=$updatedAVF"

    assert(updatedAVF.getValue(state2Action2) >= 0.9)
    assert(updatedAVF.getValue(state3Action2) <= 0.5)
  }

}
