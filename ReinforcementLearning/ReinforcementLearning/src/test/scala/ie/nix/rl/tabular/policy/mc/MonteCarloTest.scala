package ie.nix.rl.tabular.policy.mc

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.{Determinism, Policy}
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.policy.EpisodicPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.{DeterministicPolicy, SoftPolicy}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class MonteCarloTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes

  override def beforeEach(): Unit = Random.setSeed(0)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def state2Action1 = StateAction(TestState(2), TestAction(1))

  private def episode22 = Vector(state2Action2)

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  private def initSoftBehaviourPolicy: Policy with Determinism => SoftPolicy =
    targetPolicy => SoftPolicy(probabilityOfRandomAction = 0.1, targetPolicy)

  behavior of "MonteCarlo"

  it should "online iterate" in {

    val monteCarlo = MonteCarlo(episode22, discount = 1d)
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val (improvedPolicy, actionValueFunction) = monteCarlo.iterate(policy)
    logger info s"~policy        =$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~actionValueFunction=$actionValueFunction"

    assert(!improvedPolicy.equals(policy))
    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 1d)

  }

  it should "offline iterate" in {

    val monteCarlo = MonteCarlo[SoftPolicy](episode22, initSoftBehaviourPolicy, discount = 1d)
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))

    val (updatedPolicy, updatedActionValueFunction) = monteCarlo.iterate(policy)
    logger info s"updatedPolicy=$updatedPolicy"
    logger info s"updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedPolicy.getAction(TestState(2)) === TestAction(2))
    assert(updatedActionValueFunction.getValue(state2Action2) > updatedActionValueFunction.getValue(state2Action1))

  }

}
