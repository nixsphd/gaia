package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment.{StateAction, Transition, TransitionReward}
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.EpisodicPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.avf.ConstantStepSize
import ie.nix.rl.tabular.policy.td.TemporalDifferenceEvaluatorQF.getStateAction
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class TemporalDifferenceEvaluatorQFTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes

  override def beforeEach(): Unit = Random.setSeed(0)

  private def constantStepSize = ConstantStepSize(deterministicPolicy, initialValue = 0.5, stepSize = 0.1)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def transitionRewards = Vector(transitionReward224, transitionReward112)

  private def transitionReward224 =
    TransitionReward(Transition(state2Action2, TestState(4)), 1d)

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  private def transitionReward112 =
    TransitionReward(Transition(state1Action1, TestState(2)), 0d)

  private def state1Action1: Episode = StateAction(TestState(1), TestAction(1))

  behavior of "TemporalDifferenceEvaluatorQF"

  it should "get Final StateAction only one choice" in {

    val finalStateAction = getStateAction(constantStepSize, TestState(3))
    logger info s"~finalStateAction=$finalStateAction"

    assert(finalStateAction.action === TestAction(1))
  }

  it should "get Final StateAction with two choices" in {

    val finalStateAction = getStateAction(constantStepSize, TestState(2))
    logger info s"~finalStateAction=$finalStateAction"

    assert(finalStateAction.action === TestAction(2))
  }

  it should "getTarget" in {

    val evaluator = TemporalDifferenceEvaluatorQF(discount = 1d)
    val target = evaluator.getTarget(constantStepSize, transitionReward112)
    logger info s"~target=$target"

    assert(target === 0.5)
  }

  it should "getTarget for ultimate step" in {

    val evaluator = TemporalDifferenceEvaluatorQF(discount = 1d)
    val target = evaluator.getTarget(constantStepSize, transitionReward224)
    logger info s"~target=$target"

    assert(target === 1d)
  }

  it should "updateValueFunction" in {
    val evaluator = TemporalDifferenceEvaluatorQF(discount = 1d)
    val updatedConstantStepSize = evaluator.updateValueFunction(constantStepSize, Vector(transitionReward112))

    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"

    assert(updatedConstantStepSize.getValue(StateAction(TestState(2), TestAction(2))) === 0.5)
  }

  it should "updateValueFunction for ultimate step" in {
    val evaluator = TemporalDifferenceEvaluatorQF(discount = 1d)
    val updatedConstantStepSize = evaluator.updateValueFunction(constantStepSize, Vector(transitionReward224))

    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"

    assert(updatedConstantStepSize.getValue(StateAction(TestState(2), TestAction(2))) === 0.55)
  }

  it should "updateValueFunction for two step" in {
    val evaluator = TemporalDifferenceEvaluatorQF(discount = 1d)
    val updatedConstantStepSize = evaluator.updateValueFunction(constantStepSize, transitionRewards)

    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"

    assert(updatedConstantStepSize.getValue(StateAction(TestState(1), TestAction(1))) === 0.505)
    assert(updatedConstantStepSize.getValue(StateAction(TestState(2), TestAction(2))) === 0.55)
  }

  it should "evaluatePolicy one step" in {
    val evaluator = TemporalDifferenceEvaluatorQF[DeterministicPolicy](discount = 1d)
    val (updatedConstantStepSize, visitedStates) =
      evaluator.evaluatePolicy(deterministicPolicy, constantStepSize, state1Action1)
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"
    logger info s"~visitedStates=$visitedStates"

    assert(updatedConstantStepSize.getValue(StateAction(TestState(1), TestAction(1))) === 0.5)
    assert(updatedConstantStepSize.getValue(StateAction(TestState(2), TestAction(1))) === 0.5)
    assert(updatedConstantStepSize.getValue(StateAction(TestState(2), TestAction(2))) === 0.55)
    assert(updatedConstantStepSize.getValue(StateAction(TestState(3), TestAction(1))) === 0.5)
    assert(visitedStates.size === 2)
    assert(visitedStates.contains(TestState(1)))
    assert(visitedStates.contains(TestState(2)))
  }

  it should "evaluatePolicy for twice" in {
    val evaluator = TemporalDifferenceEvaluatorQF[DeterministicPolicy](discount = 1d)
    val (updatedConstantStepSize, _) =
      evaluator.evaluatePolicy(deterministicPolicy, constantStepSize, state1Action1)
    val (updatedConstantStepSizeTwice, _) =
      evaluator.evaluatePolicy(deterministicPolicy, updatedConstantStepSize, state1Action1)
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"
    logger info s"~updatedConstantStepSizeTwice=$updatedConstantStepSizeTwice"

    assert(updatedConstantStepSizeTwice.getValue(StateAction(TestState(1), TestAction(1))) === 0.5)
    assert(updatedConstantStepSizeTwice.getValue(StateAction(TestState(2), TestAction(1))) === 0.5)
    assert(updatedConstantStepSizeTwice.getValue(StateAction(TestState(2), TestAction(2))) === 0.595 +- 0.0001)
    assert(updatedConstantStepSizeTwice.getValue(StateAction(TestState(3), TestAction(1))) === 0.5)
  }

}
