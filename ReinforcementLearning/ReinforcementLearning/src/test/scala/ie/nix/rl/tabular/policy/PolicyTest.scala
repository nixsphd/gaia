package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PolicyTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = Random.setSeed(0)

  def randomPolicy: Policy = RandomPolicy

  def equiprobablePolicy: Policy = Policy.apply

  behavior of "Policy"

  it should "create Policy with equiprobable values" in {

    val policy = Policy.apply

    for {
      state <- policy.getStates
      stateActions = policy.getStateActions(state)
      equiprobability = 1d / stateActions.size
      stateAction <- stateActions
      probability = policy.getProbability(stateAction)
    } {
      assert(probability === equiprobability)
    }

  }

  it should "create Policy with initial value" in {

    val initialValue = 1d

    val policy = Policy(initialValue)

    for (stateAction <- policy.getStateActions) {
      assert(policy.getProbability(stateAction) === initialValue)
    }

  }

  it should "create Policy with random value" in {

    val policy = randomPolicy

    val probabilities = (for {
      stateAction <- policy.getStateActions
      probability = policy.getProbability(stateAction)
    } yield probability).toSet

    assert(probabilities.size === testEnvironment.getStateActions.size)

  }

  it should "get an ordered toString" in {

    val randomPolicyAsString = randomPolicy.toString
    logger.info(s"~randomPolicyAsString=$randomPolicyAsString")

    assert(
      randomPolicyAsString ===
        "Policy(" +
          "(StateAction(TestState(1),TestAction(1)),0.730967787376657), " +
          "(StateAction(TestState(2),TestAction(1)),0.24053641567148587), " +
          "(StateAction(TestState(2),TestAction(2)),0.6374174253501083), " +
          "(StateAction(TestState(3),TestAction(1)),0.5504370051176339))")

  }

  it should "get the Action for a State with one possibility" in {

    val policyAction = equiprobablePolicy.getAction(TestState(1))
    logger.info(s"~policyAction=$policyAction")

    assert(policyAction === TestAction(1))

  }

  it should "get the Action for a State from two even choices" in {

    val policyAction = equiprobablePolicy.getAction(TestState(2))
    logger.info(s"~policyAction=$policyAction")

    assert(policyAction === TestAction(1) || policyAction === TestAction(2))

  }

  it should "get the Action for a State from two unbalanced choices" in {

    val policyAction = randomPolicy.getAction(TestState(2))
    logger.info(s"~policyAction=$policyAction")

    assert(policyAction === TestAction(2))

  }

  it should "get Most Probable Action For State" in {

    val policyAction = randomPolicy.getMostProbableAction(TestState(2))
    logger.info(s"~policyAction=$policyAction")

    assert(policyAction === TestAction(2))

  }

  it should "get Actions For State" in {

    val actions = randomPolicy.getActions(TestState(2))
    logger.info(s"~actions=$actions")

    assert(actions.size === 2)
    assert(actions.contains(TestAction(1)))
    assert(actions.contains(TestAction(2)))

  }

  it should "get Action And Probabilities For State" in {

    val actionsAndProbabilities = equiprobablePolicy.getActionAndProbabilities(TestState(2))
    logger.info(s"~actionsAndProbabilities=$actionsAndProbabilities")

    assert(actionsAndProbabilities.size === 2)
    assert(actionsAndProbabilities.contains((TestAction(1), 0.5)))
    assert(actionsAndProbabilities.contains((TestAction(2), 0.5)))

  }

}
