package ie.nix.rl.tabular.policy.mc

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.EpisodicPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.avf.SampleAveraging
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class GreedyEpisodicImproverTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes

  override def beforeEach(): Unit = Random.setSeed(0)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def greedyEpisodicImprover: GreedyEpisodicImprover[SampleAveraging] = GreedyEpisodicImprover.apply

  private def actionValueFunction = SampleAveraging(deterministicPolicy)

  private def state1Action1 = StateAction(TestState(1), TestAction(1))

  private def state2Action1 = StateAction(TestState(2), TestAction(1))

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  behavior of "GreedyEpisodicImprover"

  it should "improve Policy 1 state visited" in {

    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val actionValueFunction =
      SampleAveraging(deterministicPolicy).updateValue(state2Action1, 1d).updateValue(state2Action2, 2d)
    val statesToUpdate = Vector(TestState(2))
    val improvedPolicy = greedyEpisodicImprover.improvePolicy(policy, actionValueFunction, statesToUpdate)

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))

  }

  it should "improve Policy 2 state visited" in {

    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val actionValueFunction = SampleAveraging(deterministicPolicy)
      .updateValue(state1Action1, 0.5)
      .updateValue(state2Action1, 1d)
      .updateValue(state2Action2, 2d)
    val statesToUpdate = Vector(TestState(1), TestState(2))
    val improvedPolicy = greedyEpisodicImprover.improvePolicy(policy, actionValueFunction, statesToUpdate)

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))

  }

  it should "getBestAction" in {

    val actionValueFunction =
      SampleAveraging(deterministicPolicy).updateValue(state2Action1, 1d).updateValue(state2Action2, 2d)
    val bestAction = greedyEpisodicImprover.getBestAction(TestState(2), actionValueFunction)

    assert(bestAction === TestAction(2))
  }

  it should "get StateActions Values" in {

    val stateActionValues = greedyEpisodicImprover.getStateActionsValues(actionValueFunction, TestState(2))

    assert(stateActionValues.size === 2)
    assert(stateActionValues.contains((state2Action1, 0d)))
    assert(stateActionValues.contains((state2Action2, 0d)))

  }

}
