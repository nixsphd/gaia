package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment.{StateAction, Transition, TransitionReward}
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.EpisodicPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.td.TemporalDifferenceEvaluator.BatchTemporalDifferenceEvaluator
import ie.nix.rl.tabular.policy.vf.{Batch, ConstantStepSize}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class TemporalDifferenceEvaluatorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes

  override def beforeEach(): Unit = Random.setSeed(0)

  private def constantStepSize = ConstantStepSize(deterministicPolicy, initialValue = 0.5, stepSize = 0.1)

  private def batch = Batch(ConstantStepSize(deterministicPolicy, initialValue = 0.5, stepSize = 0.1))

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def transitionReward224 =
    TransitionReward(Transition(state2Action2, TestState(4)), 1d)

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  private def transitionReward112 =
    TransitionReward(Transition(state1Action1, TestState(2)), 0d)

  private def state1Action1: Episode = StateAction(TestState(1), TestAction(1))

  behavior of "TemporalDifferenceEvaluator"

  it should "getTarget" in {

    val evaluator = TemporalDifferenceEvaluator(discount = 1d)
    val target = evaluator.getTarget(constantStepSize, transitionReward112)
    logger info s"~target=$target"

    assert(target === 0.5)
  }

  it should "getTarget for ultimate step" in {

    val evaluator = TemporalDifferenceEvaluator(discount = 1d)
    val target = evaluator.getTarget(constantStepSize, transitionReward224)
    logger info s"~target=$target"

    assert(target === 1d)
  }

  it should "updateValueFunction" in {

    val evaluator = TemporalDifferenceEvaluator(discount = 1d)
    val updatedConstantStepSize = evaluator.updateValueFunction(constantStepSize, Vector(transitionReward112))
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"

    assert(updatedConstantStepSize.getValue(TestState(1)) === 0.5)
  }

  it should "updateValueFunction for ultimate step" in {

    val evaluator = TemporalDifferenceEvaluator(discount = 1d)
    val updatedConstantStepSize = evaluator.updateValueFunction(constantStepSize, Vector(transitionReward224))
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"

    assert(updatedConstantStepSize.getValue(TestState(2)) === 0.55)
  }

  it should "evaluatePolicy" in {

    val evaluator = TemporalDifferenceEvaluator[DeterministicPolicy](discount = 1d)
    val (updatedConstantStepSize, visitedStates) =
      evaluator.evaluatePolicy(deterministicPolicy, constantStepSize, state1Action1)
    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"
    logger info s"~visitedStates=$visitedStates"

    assert(updatedConstantStepSize.getValue(TestState(1)) === 0.5)
    assert(updatedConstantStepSize.getValue(TestState(2)) === 0.55)
    assert(updatedConstantStepSize.getValue(TestState(3)) === 0.5)
    assert(visitedStates.size === 2)
    assert(visitedStates.contains(TestState(1)))
    assert(visitedStates.contains(TestState(2)))

  }

  it should "evaluatePolicy twice" in {

    val evaluator = TemporalDifferenceEvaluator[DeterministicPolicy](discount = 1d)
    val (updatedConstantStepSize, _) =
      evaluator.evaluatePolicy(deterministicPolicy, constantStepSize, state2Action2)
    val (updatedConstantStepSizeTwice, _) =
      evaluator.evaluatePolicy(deterministicPolicy, updatedConstantStepSize, state2Action2)

    logger info s"~updatedConstantStepSize=$updatedConstantStepSize"
    logger info s"~updatedConstantStepSizeTwice=$updatedConstantStepSizeTwice"

    assert(updatedConstantStepSizeTwice.getValue(TestState(1)) === 0.5)
    assert(updatedConstantStepSizeTwice.getValue(TestState(2)) === 0.595 +- 0.0001)
    assert(updatedConstantStepSizeTwice.getValue(TestState(3)) === 0.5)

  }

  it should "evaluatePolicy using Batch" in {

    val evaluator = BatchTemporalDifferenceEvaluator[DeterministicPolicy](discount = 1d)
    val updatedBatch =
      evaluator.evaluatePolicy(deterministicPolicy, batch, state1Action1)._1.consolidateBatch
    logger info s"~updatedBatch=$updatedBatch"

    assert(updatedBatch.getValue(TestState(1)) === 0.5)
    assert(updatedBatch.getValue(TestState(2)) === 0.55)
    assert(updatedBatch.getValue(TestState(3)) === 0.5)

  }

}
