package ie.nix.rl.tabular.episodes

import ie.nix.rl.tabular.EnvironmentTest.{TestEnvironment, environment}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class EpisodesTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "Episodes"

  it should "get Starts" in {
    implicit val testEnvironment = environment
    val starts = testEnvironment.getStarts(numberOfStarts = 5)
    logger.info(s"~starts=$starts")

    assert(starts.length === 5)
    assert(starts.toSet.size === 1)

  }

  it should "get ExploringStarts" in {
    implicit val testEnvironment = new TestEnvironment() with ExploringStarts
    val exploringStarts = testEnvironment.getStarts(numberOfStarts = 10)
    logger.info(s"~exploringStarts=$exploringStarts")

    assert(exploringStarts.length === 10)
    assert(exploringStarts.toSet.size === 4)

  }

  it should "get RandomStarts" in {
    implicit val testEnvironment = new TestEnvironment() with RandomStarts
    val randomStarts = testEnvironment.getStarts(numberOfStarts = 10)
    logger.info(s"~randomStarts=$randomStarts")

    assert(randomStarts.length === 10)
    assert(randomStarts.toSet.size === 4)

  }
}
