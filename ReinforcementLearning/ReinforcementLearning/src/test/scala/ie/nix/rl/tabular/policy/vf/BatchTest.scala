package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BatchTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "Batch"

  it should "create ValueFunction for a policy" in {

    val policy = randomPolicy
    val valueFunction = ValueFunction(policy, initialValue = 2d)
    val batch = Batch(valueFunction)

    logger.info(s"policy=$policy, valueFunction=$valueFunction, batch=$batch")

    assert(batch.getStates.size === policy.getStates.size)
    assert(batch.getValue(TestState(0)) === 0d)
    assert(batch.getValue(TestState(1)) === 2d)
    assert(batch.getValue(TestState(2)) === 2d)

  }

  def randomPolicy: Policy = {
    RandomPolicy
  }

  it should "update Value" in {

    val updatedValueFunction = valueFunction
      .updateValue(TestState(1), 2d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 0.0)

  }

  it should "update Value and consolidate batch" in {

    val updatedValueFunction = valueFunction
      .updateValue(TestState(1), 2d)
      .consolidateBatch
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 2d)

  }

  it should "update Value, consolidate batch and update again" in {

    val updatedValueFunction = valueFunction
      .updateValue(TestState(1), 2d)
      .consolidateBatch
      .updateValue(TestState(1), 3d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 2d)

  }

  it should "update Value, consolidate batch, update again and consolidate again" in {

    val updatedValueFunction = valueFunction
      .updateValue(TestState(1), 2d)
      .consolidateBatch
      .updateValue(TestState(1), 3d)
      .consolidateBatch
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 3d)

  }

  def valueFunction: Batch = {
    val valueFunction = ValueFunction(randomPolicy, initialValue = 0d)
    Batch(valueFunction)
  }

}
