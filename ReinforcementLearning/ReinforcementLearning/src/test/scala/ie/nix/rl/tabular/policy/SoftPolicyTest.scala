package ie.nix.rl.tabular.policy

import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import ie.nix.rl.tabular.policy.SoftPolicy.isEquivalent
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class SoftPolicyTest extends AnyFlatSpec with BeforeAndAfterEach {

  private implicit val testEnvironment: Environment = environment
  val probabilityOfRandomAction = 0.1

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "SoftPolicy"

  it should "create SoftPolicy with random action" in {

    val policy = softPolicy

    assert(isSoft(policy))

  }

  def softPolicy: SoftPolicy = {
    SoftPolicy(probabilityOfRandomAction)
  }

  def isSoft(policy: SoftPolicy): Boolean = {
    (for {
      state <- policy.getStates
      actionForState = policy.getAction(state)
      stateActions = policy.getStateActions(state)
      stateAction <- stateActions
      probability = policy.getProbability(stateAction)
    } yield {
      val probabilityOfOtherActions = probabilityOfRandomAction / stateActions.size
      if (stateAction.action === actionForState) {
        probability === probabilityOfOtherActions + 1 - probabilityOfRandomAction
      } else {
        probability === probabilityOfOtherActions
      }
    }).reduce(_ && _)
  }

  it should "create SoftPolicy from a Deterministic Policy" in {

    val policy = SoftPolicy(probabilityOfRandomAction, DeterministicPolicy.apply)

    assert(isSoft(policy))
  }

  it should "create Policy from a SoftPolicy" in {

    val randomSoftPolicy = SoftPolicy(probabilityOfRandomAction = 0.1)
    val policy = SoftPolicy.Policy(randomSoftPolicy)

    assert(SoftPolicy.isEquivalent(randomSoftPolicy, policy))

  }

  it should "create SoftPolicy from a Policy" in {

    val softPolicy = SoftPolicy(probabilityOfRandomAction, RandomPolicy)

    assert(isSoft(softPolicy))

  }

  it should "create SoftPolicy equivalent to a Policy" in {

    val policy = RandomPolicy
    val softPolicy = SoftPolicy(probabilityOfRandomAction, policy)

    assert(isEquivalent(softPolicy, policy))
  }

  it should "create SoftPolicy equivalent to a DeterministicPolicy" in {

    val deterministicPolicy = DeterministicPolicy.apply
    val softPolicy = SoftPolicy(probabilityOfRandomAction, deterministicPolicy)

    assert(isEquivalent(softPolicy, deterministicPolicy))
  }

  it should "get Action For State" in {

    val action = softPolicy.getAction(TestState(2))

    assert(action === TestAction(2))
  }

}
