package ie.nix.rl.tabular.examples.gambler

import ie.nix.rl.tabular.examples.gambler.Casino.GamblersBet
import ie.nix.rl.tabular.examples.gambler.Gambler.{ModestBetter, OstentatiousBetter}
import ie.nix.rl.tabular.policy.DeterministicPolicy.isDeterministic
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class GamblerTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val environment: Casino = casino

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "Gambler"

  it should "create a ModestBetter" in {

    val policy = ModestBetter
    logger info s"~policy=$policy"

    assert(isDeterministic(policy))
    assert(
      policy.getStates
        .map(state => policy.getMostProbableAction(state))
        .filter(action => action !== GamblersBet(1))
        .size === 0)

  }

  it should "create an OstentatiousBetter" in {

    val policy = OstentatiousBetter
    logger info s"~policy=$policy"

    assert(isDeterministic(policy))
    assert(
      policy.getStates
        .map(state => policy.getMostProbableAction(state))
        .filter(action => action !== GamblersBet(1))
        .size === 1)

  }

  private def casino: Casino = {
    val targetCapitol = 4
    val probabilityHeads = 0.4
    Casino(targetCapitol, probabilityHeads)
  }
}
