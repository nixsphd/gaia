package ie.nix.rl.tabular.examples.blackjack

import ie.nix.rl.Environment.{Reward, StateAction, Transition}
import ie.nix.rl.tabular.examples.blackjack.BlackJack.{Hit, State, Stick}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.PrivateMethodTester._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BlackJackTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  // private methods to be tested
  private val getStateAfterPlayerHit =
    PrivateMethod[State]('getStateAfterPlayerHit)
  private val getRewardAfterDealerPlays =
    PrivateMethod[Reward]('getRewardAfterDealerPlays)
  private val getNextStateGivenPlayerCard =
    PrivateMethod[State]('getNextStateGivenPlayerCard)

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  private def blackJackWhereNextDealIsAnAce(): BlackJack = {
    Random.setSeed(1)
    blackJack.dealNextCard()
    blackJack
  }

  private def blackJack = BlackJack()

  behavior of "BlackJackTest"

  it should "getTransitionForStateAction for Stick" in {

    val state =
      BlackJack.State(sumOfPlayersCards = 11, playerHasUsableAce = false, dealersVisibleCard = 1)
    val action = BlackJack.Stick
    val stateAction = StateAction(state, action)
    val transition = blackJack.getTransitionForStateAction(stateAction)
    logger.debug(s"~transition=$transition")

    assert(state === transition.finalState)

  }

  it should "getTransitionForStateAction for Hit" in {

    val state = State(sumOfPlayersCards = 11, playerHasUsableAce = false, dealersVisibleCard = 1)
    val action = Hit
    val stateAction = StateAction(state, action)
    val transition: Transition = blackJack.getTransitionForStateAction(stateAction)
    logger.debug(s"~transition=$transition")

    assert(state !== transition.finalState)

  }

  it should "get State of 18 after Player Hit" in {

    val initialState =
      State(sumOfPlayersCards = 11, playerHasUsableAce = false, dealersVisibleCard = 1)

    val newState = blackJack invokePrivate getStateAfterPlayerHit(initialState)
    logger.debug(s"~newState=$newState")

    assert(newState.sumOfPlayersCards === 18)
  }

  it should "get usable ace State after Player Hit" in {

    val initialState = State(sumOfPlayersCards = 10, playerHasUsableAce = false, dealersVisibleCard = 1)

    val newState = blackJackWhereNextDealIsAnAce invokePrivate getStateAfterPlayerHit(initialState)
    logger.debug(s"~newState=$newState")

    assert(newState.sumOfPlayersCards === 21)
    assert(newState.playerHasUsableAce)
  }

  it should "get Next State Given Player Card" in {

    val initialState =
      BlackJack.State(sumOfPlayersCards = 18, playerHasUsableAce = true, dealersVisibleCard = 1)

    val newState = blackJack invokePrivate getNextStateGivenPlayerCard(initialState, 5)
    logger.debug(s"~newState=$newState")

    assert(newState.sumOfPlayersCards === 18 + 5 - 10)
    assert(!newState.playerHasUsableAce)
  }

  it should "get 0 Reward For Transition hit to 19" in {

    val transition: Transition = Transition(
      StateAction(State(sumOfPlayersCards = 11, playerHasUsableAce = false, dealersVisibleCard = 1), Hit),
      State(sumOfPlayersCards = 19, playerHasUsableAce = false, dealersVisibleCard = 1)
    )

    val reward: Reward = blackJack.getRewardForTransition(transition)
    logger.debug(s"~transition=$transition, reward=$reward")

    assert(0d === reward)
  }

  it should "get -1 Reward For Transition Hit to bust" in {

    val transition: Transition = Transition(
      StateAction(State(sumOfPlayersCards = 18, playerHasUsableAce = false, dealersVisibleCard = 1), Hit),
      State(sumOfPlayersCards = 22, playerHasUsableAce = false, dealersVisibleCard = 1)
    )

    val reward: Reward = blackJack.getRewardForTransition(transition)
    logger.debug(s"~transition=$transition, reward=$reward")

    assert(-1d === reward)
  }

  it should "get 1 Reward For Transition Stick at 21" in {

    val transition: Transition = Transition(
      StateAction(State(sumOfPlayersCards = 21, playerHasUsableAce = false, dealersVisibleCard = 1), Stick),
      State(sumOfPlayersCards = 21, playerHasUsableAce = false, dealersVisibleCard = 1)
    )

    val reward: Reward = blackJack.getRewardForTransition(transition)
    logger.debug(s"~transition=$transition, reward=$reward")

    assert(1d === reward)
  }

  it should "get -1 Reward For Transition Stick at 11" in {

    val transition: Transition = Transition(
      StateAction(State(sumOfPlayersCards = 11, playerHasUsableAce = false, dealersVisibleCard = 1), Stick),
      State(sumOfPlayersCards = 11, playerHasUsableAce = false, dealersVisibleCard = 1)
    )

    val reward: Reward = blackJack.getRewardForTransition(transition)
    logger.debug(s"~transition=$transition, reward=$reward")

    assert(-1d === reward)
  }

  it should "get -1 Reward For State " in {
    val state = State(sumOfPlayersCards = 11, playerHasUsableAce = false, dealersVisibleCard = 1)

    val reward: Reward = blackJack invokePrivate getRewardAfterDealerPlays(state)
    logger.debug(s"~state=$state, reward=$reward")

    assert(-1d === reward)
  }

  it should "get 1 Reward For State " in {
    val state = State(sumOfPlayersCards = 21, playerHasUsableAce = false, dealersVisibleCard = 1)

    val reward: Reward = blackJack invokePrivate getRewardAfterDealerPlays(state)
    logger.debug(s"~state=$state, reward=$reward")

    assert(1d === reward)
  }

  it should "get 0 Reward For State " in {
    val state = State(sumOfPlayersCards = 18, playerHasUsableAce = false, dealersVisibleCard = 1)

    val reward: Reward = blackJack invokePrivate getRewardAfterDealerPlays(state)
    logger.debug(s"~state=$state, reward=$reward")

    assert(0d === reward)
  }

  // TODO Episodes
//  it should "initialise the table" in {
//
//    val initialisedTable = blackJack.getInitialState()
//    logger.debug(s"~initialisedTable=$initialisedTable")
//
//    assertEquals(12, initialisedTable.sumOfPlayersCards)
//    assertEquals(false, initialisedTable.playerHasUsableAce)
//    assertEquals(7, initialisedTable.dealersVisibleCard)
//
//  }

}
