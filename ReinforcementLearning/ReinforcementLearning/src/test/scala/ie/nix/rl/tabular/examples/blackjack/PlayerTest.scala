package ie.nix.rl.tabular.examples.blackjack

import ie.nix.rl.tabular.examples.blackjack.BlackJack.{Hit, Stick}
import ie.nix.rl.tabular.examples.blackjack.Player.{Hitter, Sticker}
import ie.nix.rl.tabular.policy.DeterministicPolicy.isDeterministic
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PlayerTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val environment: BlackJack = BlackJack()

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "Gambler"

  it should "create a Hitter" in {

    val policy = Hitter
    logger info s"~policy=$policy"

    assert(isDeterministic(policy))
    assert(
      policy.getStates
        .map(state => policy.getMostProbableAction(state))
        .filter(action => action !== Hit)
        .size === 0)

  }

  it should "create a Sticker" in {

    val policy = Sticker
    logger info s"~policy=$policy"

    assert(isDeterministic(policy))
    assert(
      policy.getStates
        .map(state => policy.getMostProbableAction(state))
        .filter(action => action !== Stick)
        .size === 0)

  }
}
