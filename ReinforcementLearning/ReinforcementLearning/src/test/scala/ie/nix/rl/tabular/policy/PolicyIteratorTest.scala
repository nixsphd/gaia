package ie.nix.rl.tabular.policy

import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import ie.nix.rl.tabular.policy.PolicyIterator.policyDifference
import ie.nix.rl.tabular.policy.dp.DynamicProgrammingTest.{TestEnvironmentWithModel, environmentWithModel}
import ie.nix.rl.tabular.policy.dp.{BackupEvaluator, GreedyBackupImprover}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PolicyIteratorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: TestEnvironmentWithModel = environmentWithModel

  override def beforeEach(): Unit = Random.setSeed(0)

  def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply
  def randomPolicy: Policy = RandomPolicy
  def equiprobablePolicy: Policy = Policy.apply

  behavior of "PolicyIteratorTest"

  it should "policyDifference not 0 for two random policies" in {
    val differenceBetweenPolicies = policyDifference(randomPolicy, randomPolicy)

    logger.info(s"~differenceBetweenPolicies=$differenceBetweenPolicies")

    assert(differenceBetweenPolicies !== 0d)

  }

  it should "policyDifference is 0 for two equiprobable policies" in {

    val differenceBetweenPolicies = policyDifference(equiprobablePolicy, equiprobablePolicy)
    logger.info(s"~differenceBetweenPolicies=$differenceBetweenPolicies")

    assert(differenceBetweenPolicies === 0d)

  }

  it should "iterate with 1 sweep with discount 0.9" in {
    implicit val evaluator = BackupEvaluator(discount = 0.9, numberOfSweeps = 1)
    implicit val improver = GreedyBackupImprover(discount = 0.9)
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val iterator = new PolicyIterator(numberOfIterations = 1)
    logger info s"~policy=$policy"

    val (improvedPolicy, _) = iterator.iterate(policy)
    logger info s"~improvedPolicy=$improvedPolicy"

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))

  }

  it should "iterate with 2 sweeps with discount 0.9" in {
    implicit val evaluator = BackupEvaluator(discount = 0.9, numberOfSweeps = 2)
    implicit val improver = GreedyBackupImprover(discount = 0.9)
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val iterator = new PolicyIterator(numberOfIterations = 1)
    logger info s"~policy=$policy"

    val (improvedPolicy, _) = iterator.iterate(policy)
    logger info s"~improvedPolicy=$improvedPolicy"

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))

  }

}
