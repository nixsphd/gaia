package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class WeightedAveragingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = Random.setSeed(0)

  private def actionValueFunction = WeightedAveraging(randomPolicy)

  private def randomPolicy: Policy = RandomPolicy

  private def stateAction: StateAction = StateAction(TestState(1), TestAction(1))

  behavior of "WeightedAveraging AVF"

  it should "create WeightedAveraging for a policy" in {

    val policy = randomPolicy
    val weightedAveraging = WeightedAveraging(policy)
    logger.info(s"policy=$policy, weightedAveraging=$weightedAveraging")

    assert(weightedAveraging.getStateActions.size === policy.getStateActions.size)
    assert(weightedAveraging.getValue(StateAction(TestState(1), TestAction(1))) === 0d)
    assert(weightedAveraging.getValue(StateAction(TestState(2), TestAction(1))) === 0d)
    assert(weightedAveraging.getValue(StateAction(TestState(2), TestAction(2))) === 0d)

  }

  it should "update Value without a weight" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(stateAction, 2d)
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(stateAction) === 2d)

  }

  it should "update Value with a weight" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(stateAction, 2d, 3d)
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(stateAction) === 2d)

  }

  it should "update Value with a weight twice" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(stateAction, 2d, 3d)
      .updateValue(stateAction, 3d, 1d)
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(stateAction) === 9d / 4d)

  }

}
