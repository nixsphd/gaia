package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Determinism
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestEnvironment, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.policy.EpisodicPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.avf.{SampleAveraging, WeightedAveraging}
import ie.nix.rl.tabular.policy.mc.{EpisodicEvaluator, GreedyEpisodicImprover, MonteCarlo}
import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random

object EpisodicPolicyIteratorTest {

  def environmentWithEpisodes: TestEnvironmentWithEpisodes = {
    val terminalStates = Set[State](TestState(0), TestState(4))
    val stateActions = Set[StateAction](
      StateAction(TestState(1), TestAction(1)),
      StateAction(TestState(2), TestAction(1)),
      StateAction(TestState(2), TestAction(2)),
      StateAction(TestState(3), TestAction(1))
    )
    new TestEnvironmentWithEpisodes(stateActions, terminalStates)
  }

  class TestEnvironmentWithEpisodes(override val stateActionsSet: Set[StateAction],
                                    override val terminalStateSet: Set[State])
      extends TestEnvironment(stateActionsSet, terminalStateSet)
      with Episodes {

    override def getInitialState(): State = TestState(1)

  }

}

class EpisodicPolicyIteratorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes
  implicit val evaluator: EpisodicEvaluator[Policy with Determinism, SampleAveraging] =
    EpisodicEvaluator[Policy with Determinism](discount = 1d)
  implicit val improver: GreedyEpisodicImprover[SampleAveraging] =
    GreedyEpisodicImprover.apply

  override def beforeEach(): Unit = Random.setSeed(0)

  val episodes = Vector(state1Action1, state2Action2)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def initialValueFunction = targetPolicy => WeightedAveraging(targetPolicy)

  private def initSoftBehaviourPolicy: policy.Policy with Determinism => SoftPolicy =
    targetPolicy => SoftPolicy(probabilityOfRandomAction = 0.1, targetPolicy)

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  private def state1Action1 = StateAction(TestState(1), TestAction(1))

  private def state2Action1 = StateAction(TestState(2), TestAction(1))

  private def targetPolicy = DeterministicPolicy.apply.updateAction(TestState(2), TestAction(1))

  behavior of "OfflinePolicyIterator"

  it should "iterate Online Policy with 1 episode" in {

    val episodes = Vector(state2Action2)
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    logger info s"~policy=$policy"
    val onPolicyIterator = MonteCarlo(episodes, discount = 1d)
    val (improvedPolicy, actionValueFunction) = onPolicyIterator.iterate(policy)
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~actionValueFunction=$actionValueFunction"

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))
    assert(actionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 0d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 0d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 1d)
    assert(actionValueFunction.getValue(StateAction(TestState(3), TestAction(1))) === 0d)
  }

  it should "iterate Online Policy with 2 episode" in {

    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    logger info s"~policy=$policy"
    val onPolicyIterator = MonteCarlo(episodes, discount = 1d)
    val (improvedPolicy, actionValueFunction) = onPolicyIterator.iterate(policy)
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~actionValueFunction=$actionValueFunction"

    assert(actionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 1d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 1d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 1d)
    assert(actionValueFunction.getValue(StateAction(TestState(3), TestAction(1))) === 1d)
  }

  it should "evaluate And Improve Offline Policy" in {

    val offPolicyIterator = MonteCarlo(episodes, initSoftBehaviourPolicy, discount = 1d)

    val (updatedPolicy, updatedActionValueFunction) =
      offPolicyIterator.evaluateAndImprovePolicy(targetPolicy, initialValueFunction.apply(targetPolicy), state2Action2)
    logger info s"updatedPolicy=$updatedPolicy"
    logger info s"updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedPolicy.getAction(TestState(2)) === TestAction(2))
    assert(updatedActionValueFunction.getValue(state2Action2) > updatedActionValueFunction.getValue(state2Action1))
  }

  it should "iterate Offline Policy" in {

    val offPolicyIterator = MonteCarlo(Vector(state2Action2), initSoftBehaviourPolicy, discount = 1d)
    val (updatedPolicy, updatedActionValueFunction) = offPolicyIterator.iterate(targetPolicy)
    logger info s"updatedPolicy=$updatedPolicy"
    logger info s"updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedPolicy.getAction(TestState(2)) === TestAction(2))
    assert(updatedActionValueFunction.getValue(state2Action2) > updatedActionValueFunction.getValue(state2Action1))

  }

}
