package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ValueFunctionTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "ValueFunction"

  it should "create ValueFunction for a policy" in {

    val policy = randomPolicy
    val valueFunction = ValueFunction(policy)
    logger.info(s"policy=$policy, valueFunction=$valueFunction")

    assert(valueFunction.getStates.size === policy.getStates.size)
    assert(valueFunction.getValue(TestState(0)) === 0d)
    assert(valueFunction.getValue(TestState(1)) !== 0d)
    assert(valueFunction.getValue(TestState(2)) !== 0d)

  }

  it should "create ValueFunction for a policy with initial value" in {

    val policy = randomPolicy
    val valueFunction = ValueFunction(policy, initialValue = 2d)
    logger.info(s"policy=$policy, valueFunction=$valueFunction")

    assert(valueFunction.getValue(TestState(0)) === 0d)
    assert(valueFunction.getValue(TestState(1)) === 2d)
    assert(valueFunction.getValue(TestState(2)) === 2d)

  }

  def randomPolicy: Policy = {
    RandomPolicy
  }

  it should "get a Difference between two random functions" in {

    val policy = randomPolicy
    val aValueFunction = ValueFunction(policy)
    val anotherValueFunction = ValueFunction(policy)

    val theDifference = ValueFunction.difference(aValueFunction, anotherValueFunction)
    logger.info(
      s"theDifference=$theDifference, aValueFunction=$aValueFunction, " +
        s"anotherValueFunction=$anotherValueFunction")

    assert(theDifference !== 0d)

  }

  it should "get no Difference between two identical functions" in {

    val policy = randomPolicy
    val aValueFunction = ValueFunction(policy, initialValue = 2d)
    val anotherValueFunction = ValueFunction(policy, initialValue = 2d)

    val theDifference = ValueFunction.difference(aValueFunction, anotherValueFunction)
    logger.info(
      s"theDifference=$theDifference, aValueFunction=$aValueFunction, " +
        s"anotherValueFunction=$anotherValueFunction")

    assert(theDifference === 0d)

  }

  def equiprobablePolicy: Policy = {
    Policy.apply
  }

  it should "get Value" in {

    val value = valueFunction.getValue(TestState(1))
    logger.info(s"value=$value")

    assert(value === 2d)

  }

  it should "update Value" in {

    val updatedValueFunction = valueFunction.updateValue(TestState(1), 2d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 2d)

  }

  private def valueFunction = {
    ValueFunction(randomPolicy, initialValue = 2d)
  }

}
