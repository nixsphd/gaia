package ie.nix.rl.tabular.policy

import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.DeterministicPolicy.{isDeterministic, isEquivalent}
import ie.nix.rl.tabular.policy.Policy.{
  RandomPolicy,
  getEquiprobableStateActionProbabilitiesMap,
  getStateActionProbabilityMap
}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class DeterministicPolicyTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = Random.setSeed(0)

  def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  behavior of "DeterministicPolicy"

  it should "create DeterministicPolicy with random action" in {

    val policy = deterministicPolicy

    for {
      state <- policy.getStates
      actionForState = policy.getAction(state)
      stateActions = policy.getStateActions(state)
      stateAction <- stateActions
      probability = policy.getProbability(stateAction)
    } yield {
      if (stateAction.action === actionForState) {
        assert(probability === 1d)
      } else {
        assert(probability === 0d)
      }
    }

  }

  it should "create DeterministicPolicy from a Policy" in {

    val policy = RandomPolicy
    val deterministicPolicy = DeterministicPolicy(policy)

    assert(isDeterministic(deterministicPolicy))
  }

  it should "create DeterministicPolicy equivalent to a Policy" in {

    val policy = RandomPolicy
    val deterministicPolicy = DeterministicPolicy(policy)

    assert(isEquivalent(deterministicPolicy, policy))
  }

  it should "create Policy from a DeterministicPolicy" in {

    val randomDeterministicPolicy = DeterministicPolicy.apply
    val policy = DeterministicPolicy.Policy(randomDeterministicPolicy)

    assert(DeterministicPolicy.isEquivalent(randomDeterministicPolicy, policy))

  }

  it should "get Action For State" in {

    val action = deterministicPolicy.getAction(TestState(2))

    assert(action === TestAction(2))

  }

  it should "throw an exception when a state has no Action" in {

    val policy = new DeterministicPolicy(getStateActionProbabilityMap(initialValue = 0d))
    assertThrows[RuntimeException] {
      policy.getAction(TestState(1))
    }

  }

  it should "throw an exception when a state has mor than one Action" in {

    val policy = new DeterministicPolicy(getEquiprobableStateActionProbabilitiesMap)
    assertThrows[RuntimeException] {
      policy.getAction(TestState(2))
    }

  }

//  it should "update the Action for a State" in {
//    val state: State = TestState(1)
//    val action: Action = TestAction(1)
//    val policy = DeterministicPolicy().updateStateActionProbability(StateAction(state, action), 0d)
//
//    val updatedPolicy = policy.updateActionForState(state, action)
//
//    val policyAction = updatedPolicy.getActionForState(state)
//    logger.debug(s"~updatedPolicy=$updatedPolicy, policyAction=$policyAction")
//    assertEquals(action, policyAction)
//  }
//
//  it should "improve BlackJackTable policy" in {
//
//    val blackJackTable: BlackJackTable = new BlackJackTable()
//    val actionValueFunction = newTabularActionValueFunctionHitOver17(blackJackTable)
//
//    val improvedPolicy = DeterministicPolicy(blackJackTable, actionValueFunction)
//    logger.info(s"~improvedPolicy=$improvedPolicy")
//
//    val sumOfPlayersCards17State =
//      blackJackTable.BlackJackState(sumOfPlayersCards = 17, playerHasUsableAce = false, dealersVisibleCard = 1)
//    val sumOfPlayersCards18State =
//      blackJackTable.BlackJackState(sumOfPlayersCards = 18, playerHasUsableAce = false, dealersVisibleCard = 1)
//
//    assertEquals(blackJackTable.Hit, improvedPolicy.getActionForState(sumOfPlayersCards17State))
//    assertEquals(blackJackTable.Stick, improvedPolicy.getActionForState(sumOfPlayersCards18State))
//
//  }
//
//  it should "does not improve when conservative action value function and ModestBetter" in {
//
//    val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
//    val policy = ModestBetter(casino)
//    val actionValueFunction = newTabularActionValueFunctionConservative(casino)
//
//    val improvedPolicy = DeterministicPolicy(casino, actionValueFunction)
//    logger.info(s"~improvedPolicy=$improvedPolicy")
//
//    assertEquals(policy, improvedPolicy)
//
//  }
//
//  it should "does not improve when flaithulach action value function and OstentatiousBetter" in {
//
//    val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
//    val policy = OstentatiousBetter(casino)
//    val actionValueFunction = newTabularActionValueFunctionFlaithulach(casino)
//
//    val improvedPolicy = DeterministicPolicy(casino, actionValueFunction)
//    logger.info(s"~improvedPolicy=$improvedPolicy")
//
//    assertEquals(policy, improvedPolicy)
//
//  }
//
//  it should "does improve when flaithulach action value function and ModestBetter" in {
//
//    val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
//    val policy = ModestBetter(casino)
//    logger.info(s"~policy=$policy")
//
//    val actionValueFunction = newTabularActionValueFunctionFlaithulach(casino)
//    val improvedPolicy = DeterministicPolicy(casino, actionValueFunction)
//    logger.info(s"~improvedPolicy=$improvedPolicy")
//
//    assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(1)))
//    assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(2)))
//    assertEquals(casino.GamblersBet(2), improvedPolicy.getActionForState(casino.GamblersCapitol(3)))
//    assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(4)))
//
//  }
//
//  it should "does improve when conservative action value function and OstentatiousBetter" in {
//
//    val casino = Casino(targetCapitol = 5, probabilityHeads = 0.4)
//    val policy = OstentatiousBetter(casino)
//    logger.info(s"~policy=$policy")
//
//    val actionValueFunction = newTabularActionValueFunctionConservative(casino)
//    val improvedPolicy = DeterministicPolicy(casino, actionValueFunction)
//    logger.info(s"~improvedPolicy=$improvedPolicy")
//
//    assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(1)))
//    assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(2)))
//    assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(3)))
//    assertEquals(casino.GamblersBet(1), improvedPolicy.getActionForState(casino.GamblersCapitol(4)))
//
//  }

}

//object DeterministicPolicyTest {

//  def newTabularActionValueFunctionHitOver17(blackJackTable: BlackJackTable) = {
//    var actionValueFunction = ActionValueFunction(blackJackTable, initialValue = 0)
//    for (sumOfPlayersCards: SumOfCards <- 11 to 21;
//         playerHasUsableAce: Boolean <- List(true, false);
//         dealersVisibleCard: Card <- 1 to 10) {
//      val state = blackJackTable.BlackJackState(sumOfPlayersCards, playerHasUsableAce, dealersVisibleCard)
//      if (sumOfPlayersCards <= 17) {
//        actionValueFunction = actionValueFunction.setValueForStateAction(StateAction(state, blackJackTable.Hit), 1d)
//        actionValueFunction = actionValueFunction.setValueForStateAction(StateAction(state, blackJackTable.Stick), -1d)
//      } else {
//        actionValueFunction = actionValueFunction.setValueForStateAction(StateAction(state, blackJackTable.Hit), -1d)
//        actionValueFunction = actionValueFunction.setValueForStateAction(StateAction(state, blackJackTable.Stick), 1d)
//      }
//    }
//    actionValueFunction
//  }
//
//  def newTabularActionValueFunctionConservative(casino: Casino) = {
//    var actionValueFunction = ActionValueFunction(casino, initialValue = 0)
//    casino.getNonTerminalStates()
//    for (state <- casino.getNonTerminalStates(); action <- casino.getActionsForState(state)) {
//      (state, action) match {
//        case (gamblersCapitol: casino.GamblersCapitol, gamblerBet: casino.GamblersBet) => {
//          val value: Double = gamblersCapitol.capitol / gamblerBet.bet
//          actionValueFunction =
//            actionValueFunction.setValueForStateAction(StateAction(gamblersCapitol, gamblerBet), value)
//        }
//        case _ =>
//      }
//    }
//    actionValueFunction
//  }
//
//  def newTabularActionValueFunctionFlaithulach(casino: Casino) = {
//    var actionValueFunction = ActionValueFunction(casino, initialValue = 0)
//    casino.getNonTerminalStates()
//    for (state <- casino.getNonTerminalStates(); action <- casino.getActionsForState(state)) {
//      (state, action) match {
//        case (gamblersCapitol: casino.GamblersCapitol, gamblerBet: casino.GamblersBet) => {
//          val value: Double = gamblerBet.bet / gamblersCapitol.capitol
//          actionValueFunction =
//            actionValueFunction.setValueForStateAction(StateAction(gamblersCapitol, gamblerBet), value)
//        }
//        case _ =>
//      }
//    }
//    actionValueFunction
//  }
//
//}
