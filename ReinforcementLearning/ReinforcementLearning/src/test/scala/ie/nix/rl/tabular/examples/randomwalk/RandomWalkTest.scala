package ie.nix.rl.tabular.examples.randomwalk

import ie.nix.rl.Environment.{StateAction, Transition}
import ie.nix.rl.tabular.examples.randomwalk.RandomWalk.{Left, Right, Tile}
import org.apache.logging.log4j.scala.Logging
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random

class RandomWalkTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  override def beforeEach(): Unit = Random.setSeed(0)

  private def randomWalk: RandomWalk = RandomWalk(numberOfTiles = 5)

  behavior of "RandomWalk"

  it should "RandomWalk" in {

    val numberOfTiles = 4
    val newRandomWalk = RandomWalk(numberOfTiles)

    assert(newRandomWalk.numberOfTiles === 4)

  }

  it should "numberOfTiles" in {

    val numberOfTiles = randomWalk.numberOfTiles
    logger.debug(s"numberOfTiles=$numberOfTiles")

    assert(numberOfTiles === 5)

  }

  it should "get 2 terminal States" in {

    val terminalStates = randomWalk.getTerminalStates
    logger.debug(s"terminalStates=$terminalStates")

    assert(terminalStates.size === 2)
    assert(terminalStates.contains(Tile(0, isTerminal = true)))
    assert(terminalStates.contains(Tile(5, isTerminal = true)))

  }

  it should "get stateActionsSet" in {

    val stateActions = randomWalk.getStateActions
    logger.debug(s"stateActions=$stateActions")

    assert(stateActions.size === 8)
    assert(stateActions.contains(StateAction(Tile(1), Right)))
    assert(stateActions.contains(StateAction(Tile(1), Left)))
    assert(stateActions.contains(StateAction(Tile(2), Right)))
    assert(stateActions.contains(StateAction(Tile(2), Left)))
    assert(stateActions.contains(StateAction(Tile(3), Right)))
    assert(stateActions.contains(StateAction(Tile(3), Left)))
    assert(stateActions.contains(StateAction(Tile(4), Right)))
    assert(stateActions.contains(StateAction(Tile(4), Left)))
  }

  it should "get Transition For StateAction(2,Right)" in {

    val stateAction = StateAction(Tile(2), Right)

    val transition = randomWalk.getTransitionForStateAction(stateAction)
    logger.info(s"~transition=$transition")

    assert(transition.finalState === Tile(3))

  }

  it should "get Transition For StateAction(4,Right)" in {

    val stateAction = StateAction(Tile(4), Right)

    val transition = randomWalk.getTransitionForStateAction(stateAction)
    logger.info(s"~transition=$transition")

    assert(transition.finalState === Tile(5, isTerminal = true))

  }

  it should "get Transition For StateAction(1,Left)" in {

    val stateAction = StateAction(Tile(1), Left)

    val transition = randomWalk.getTransitionForStateAction(stateAction)
    logger.info(s"~transition=$transition")

    assert(transition.finalState.isTerminal)
    assert(transition.finalState === Tile(0, isTerminal = true))

  }

  it should "get Reward For Transition to 5" in {

    val transition = Transition(StateAction(Tile(4), Right), Tile(5, isTerminal = true))

    val reward = randomWalk.getRewardForTransition(transition)
    logger.info(s"~reward=$reward")

    assert(reward === 1d)

  }

  it should "get Reward For Transition to 0" in {

    val transition = Transition(StateAction(Tile(1), Left), Tile(0))

    val reward = randomWalk.getRewardForTransition(transition)
    logger.info(s"~reward=$reward")

    assert(reward === 0d)

  }

  it should "get Reward For Transition to 1" in {

    val transition = Transition(StateAction(Tile(2), Left), Tile(1))

    val reward = randomWalk.getRewardForTransition(transition)
    logger.info(s"~reward=$reward")

    assert(reward === 0d)

  }

}
