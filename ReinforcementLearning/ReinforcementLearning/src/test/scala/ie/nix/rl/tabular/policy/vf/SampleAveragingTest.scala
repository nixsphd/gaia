package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class SampleAveragingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  private def valueFunction = SampleAveraging(randomPolicy)

  private def randomPolicy: Policy = RandomPolicy

  behavior of "SampleAveraging"

  it should "create SampleAveraging for a policy" in {

    val policy = randomPolicy
    val sampleAveraging = SampleAveraging(policy)
    logger.info(s"policy=$policy, sampleAveraging=$sampleAveraging")

    assert(sampleAveraging.getStates.size === policy.getStates.size)
    assert(sampleAveraging.getValue(TestState(0)) === 0d)
    assert(sampleAveraging.getValue(TestState(1)) === 0d)
    assert(sampleAveraging.getValue(TestState(2)) === 0d)

  }

  it should "update Value" in {

    val updatedValueFunction = valueFunction.updateValue(TestState(1), 2d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 2d)

  }

  it should "update Value twice" in {

    val updatedValueFunction = valueFunction
      .updateValue(TestState(1), 2d)
      .updateValue(TestState(1), 3d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 2.5)

  }

}
