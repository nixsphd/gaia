package ie.nix.rl.tabular.examples.randomwalk

import ie.nix.rl.tabular.examples.randomwalk.RandomWalk.{Left, Right}
import ie.nix.rl.tabular.examples.randomwalk.Walker.{Lefty, Righty}
import ie.nix.rl.tabular.policy.DeterministicPolicy.isDeterministic
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class WalkerTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val environment: RandomWalk = randomWalk

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "Gambler"

  it should "create a Lefty" in {

    val policy = Lefty
    logger info s"~policy=$policy"

    assert(isDeterministic(policy))
    assert(
      policy.getStates
        .map(state => policy.getMostProbableAction(state))
        .filter(action => action !== Left)
        .size === 0)

  }

  it should "create a Righty" in {

    val policy = Righty
    logger info s"~policy=$policy"

    assert(isDeterministic(policy))
    assert(
      policy.getStates
        .map(state => policy.getMostProbableAction(state))
        .filter(action => action !== Right)
        .size === 0)

  }

  private def randomWalk: RandomWalk = {
    val numberOfTiles = 5
    RandomWalk(numberOfTiles)
  }

}
