package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.EpisodicPolicyImprover
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.policy.EpisodicPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.mc.IdleEpisodicImprover
import ie.nix.rl.tabular.policy.td.TemporalDifferenceEvaluator
import ie.nix.rl.tabular.policy.td.TemporalDifferenceEvaluator.BatchTemporalDifferenceEvaluator
import ie.nix.rl.tabular.policy.vf.{Batch, ConstantStepSize}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BatchedEpisodicPolicyIteratorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes
  implicit val evaluator: TemporalDifferenceEvaluator[Policy, Batch] =
    BatchTemporalDifferenceEvaluator[Policy](discount = 1d)
  implicit val improver: EpisodicPolicyImprover[Policy, Batch] = new IdleEpisodicImprover[Policy, Batch]

  override def beforeEach(): Unit = Random.setSeed(0)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def batch: ie.nix.rl.policy.Policy => Batch =
    policy => Batch(ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.1))

  private def state1Action1 = StateAction(TestState(1), TestAction(1))

  //  private def state2Action1 = StateAction(TestState(2), TestAction(1))
  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  behavior of "BatchedEpisodicPolicyIterator"

  it should "iterateBatch" in {

    val episodes = Vector(state2Action2)
    val policy = deterministicPolicy
    val batchIterator = new BatchedEpisodicPolicyIterator[Policy](episodes, batch)

    val updatedBatch =
      batchIterator.iterateBatch((policy, batch.apply(policy)), episodes)._2.consolidateBatch
    logger info s"~updatedBatch=$updatedBatch"

    assert(updatedBatch.getValue(TestState(1)) === 0.5)
    assert(updatedBatch.getValue(TestState(2)) === 0.55)
    assert(updatedBatch.getValue(TestState(3)) === 0.5)
  }

  it should "iterateBatch with 2 episodes" in {

    val episodes = Vector(state1Action1, state2Action2)
    val policy = deterministicPolicy
    val batchIterator = new BatchedEpisodicPolicyIterator[Policy](episodes, batch)

    val updatedBatch =
      batchIterator.iterateBatch((policy, batch.apply(policy)), episodes)._2.consolidateBatch
    logger info s"~updatedBatch=$updatedBatch"

    assert(updatedBatch.getValue(TestState(1)) === 0.5)
    assert(updatedBatch.getValue(TestState(2)) === 0.595 +- 0.0001)
    assert(updatedBatch.getValue(TestState(3)) === 0.5)
  }

  it should "iterate" in {
    val episodes = Vector(state2Action2)
    val policy = deterministicPolicy
    val batchIterator = new BatchedEpisodicPolicyIterator[Policy](episodes, batch)

    val (_, updatedBatch) =
      batchIterator.iterate(policy)
    logger info s"~updatedBatch=$updatedBatch"

    assert(updatedBatch.getValue(TestState(1)) === 0.5)
    assert(updatedBatch.getValue(TestState(2)) === 0.55)
    assert(updatedBatch.getValue(TestState(3)) === 0.5)
  }

  it should "iterate with 2 episodes" in {

    val episodes = Vector(state1Action1, state2Action2)
    val policy = deterministicPolicy
    val batchIterator = new BatchedEpisodicPolicyIterator[Policy](episodes, batch)

    val (_, updatedBatch) =
      batchIterator.iterate(policy)
    logger info s"~updatedBatch=$updatedBatch"

    assert(updatedBatch.getValue(TestState(1)) === 0.505)
    assert(updatedBatch.getValue(TestState(2)) === 0.6355 +- 0.0001)
    assert(updatedBatch.getValue(TestState(3)) === 0.5)
  }

}
