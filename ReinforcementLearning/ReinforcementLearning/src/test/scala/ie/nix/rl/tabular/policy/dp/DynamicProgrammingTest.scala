package ie.nix.rl.tabular.policy.dp

import ie.nix.rl.Environment.{StateAction, Transition}
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestEnvironment, TestState}
import ie.nix.rl.tabular.Model
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.dp.DynamicProgrammingTest.{TestEnvironmentWithModel, environmentWithModel}
import ie.nix.rl.tabular.policy.vf.ValueFunction
import ie.nix.rl.{Environment, State}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

object DynamicProgrammingTest {

  import org.scalactic.TypeCheckedTripleEquals._

  def environmentWithModel: TestEnvironmentWithModel = {
    val terminalStates = Set[State](TestState(0), TestState(4))
    val stateActions = Set[StateAction](
      StateAction(TestState(1), TestAction(1)),
      StateAction(TestState(2), TestAction(1)),
      StateAction(TestState(2), TestAction(2)),
      StateAction(TestState(3), TestAction(1))
    )
    new TestEnvironmentWithModel(stateActions, terminalStates)
  }

  class TestEnvironmentWithModel(override val stateActionsSet: Set[StateAction],
                                 override val terminalStateSet: Set[State])
      extends TestEnvironment(stateActionsSet, terminalStateSet)
      with Model {

    override def getProbabilityOfTransition(transition: Environment.Transition): Double = transition match {
      case Transition(StateAction(TestState(stateId), TestAction(actionId)), TestState(finalStateId))
          if stateId + actionId === finalStateId =>
        0.75
      case Transition(StateAction(TestState(stateId), TestAction(actionId)), TestState(finalStateId))
          if stateId - actionId === finalStateId =>
        0.25
      case _ => 0d
    }
  }

}

@RunWith(classOf[JUnitRunner])
class DynamicProgrammingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: TestEnvironmentWithModel = environmentWithModel

  override def beforeEach(): Unit = Random.setSeed(0)
  private def valueFunction0 = ValueFunction(deterministicPolicy, initialValue = 0d)
  private def valueFunction1 = ValueFunction(deterministicPolicy, initialValue = 1d)
  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply
  private def state2Action1 = StateAction(TestState(2), TestAction(1))
  private def state3Action1 = StateAction(TestState(3), TestAction(1))

  behavior of "DynamicProgramming"

  it should "backupState 31 with discount of 1" in {

    val backup = DynamicProgramming.backupState(state3Action1, discount = 1.0, valueFunction0)
    logger info s"~backup=$backup, valueFunction=$valueFunction0"

    assert(backup === 0.75)

  }

  it should "backupState 21 with discount of 0.9" in {

    val backup = DynamicProgramming.backupState(state2Action1, discount = 0.9, valueFunction1)
    logger info s"~backup=$backup, valueFunction=$valueFunction0"

    assert(backup === 0.9)

  }

  it should "iterate with 1 sweep and discount 0.9" in {
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val dynamicProgramming = DynamicProgramming(numberOfIterations = 1, discount = 0.9, numberOfSweeps = 1)
    logger info s"~policy=$policy"

    val (improvedPolicy, _) = dynamicProgramming.iterate(policy)
    logger info s"~improvedPolicy=$improvedPolicy"

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))

  }

}
