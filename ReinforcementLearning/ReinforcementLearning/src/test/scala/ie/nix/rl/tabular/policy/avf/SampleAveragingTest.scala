package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class SampleAveragingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = Random.setSeed(0)

  private def sampleAveraging = SampleAveraging(randomPolicy)
  private def randomPolicy = RandomPolicy
  private def stateAction = StateAction(TestState(1), TestAction(1))

  behavior of "SampleAveraging"

  it should "create SampleAveraging for a policy" in {

    val policy = randomPolicy
    val sampleAveraging = SampleAveraging(policy)
    logger.info(s"policy=$policy, sampleAveraging=$sampleAveraging")

    assert(sampleAveraging.getStateActions.size === policy.getStateActions.size)
    assert(sampleAveraging.getValue(StateAction(TestState(1), TestAction(1))) === 0d)
    assert(sampleAveraging.getValue(StateAction(TestState(2), TestAction(1))) === 0d)
    assert(sampleAveraging.getValue(StateAction(TestState(2), TestAction(2))) === 0d)

  }

  it should "update Value" in {

    val updatedSampleAveraging = sampleAveraging.updateValue(stateAction, 2d)
    logger.info(s"updatedSampleAveraging=$updatedSampleAveraging")

    assert(updatedSampleAveraging.getValue(stateAction) === 2d)

  }

  it should "update Value twice" in {

    val updatedSampleAveraging = sampleAveraging
      .updateValue(stateAction, 2d)
      .updateValue(stateAction, 3d)
    logger.info(s"updatedSampleAveraging=$updatedSampleAveraging")

    assert(updatedSampleAveraging.getValue(stateAction) === 2.5)

  }
}
