package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.examples.blackjack.BlackJack
import ie.nix.rl.tabular.policy.td.TDPolicyIteratorTest.environmentWithEpisodes
import ie.nix.rl.tabular.policy.td.TemporalDifference.{
  QLearning,
  SARSA,
  TemporalDifferenceBatchPredictor,
  TemporalDifferencePredictor
}
import ie.nix.rl.tabular.policy.td.TemporalDifferenceEvaluator.BatchTemporalDifferenceEvaluator
import ie.nix.rl.tabular.policy.{DeterministicPolicy, SoftPolicy, avf, vf}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class TemporalDifferenceTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  implicit val environment: Environment with Episodes = environmentWithEpisodes
  implicit val vfEvaluator: TemporalDifferenceEvaluator[Policy, vf.ConstantStepSize] =
    TemporalDifferenceEvaluator[Policy](discount = 1d)
  implicit val avfEvaluator: TemporalDifferenceEvaluatorQF[Policy, avf.ConstantStepSize] =
    TemporalDifferenceEvaluatorQF[Policy](discount = 1d)
  implicit val evaluator: TemporalDifferenceEvaluator[Policy, vf.Batch] =
    BatchTemporalDifferenceEvaluator[Policy](discount = 1d)
  private val initialActionValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = policy =>
    avf.ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.1)
  private val initialValueFunction: ie.nix.rl.policy.Policy => vf.ConstantStepSize = policy =>
    vf.ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.1)
  private val initialBatchValueFunction: ie.nix.rl.policy.Policy => vf.Batch = policy =>
    vf.Batch(vf.ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.1))

  override def beforeEach(): Unit = Random.setSeed(0)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def softPolicy: SoftPolicy = SoftPolicy(probabilityOfRandomAction = 0.1)

  private def episode21 = Vector(state2Action1)

  private def state2Action1 = StateAction(TestState(2), TestAction(1))

  private def episode22 = Vector(state2Action2)

  private def episode32 = Vector(state3Action2)

  private def episode21s = Vector.tabulate(70)(_ => state2Action1)

  private def episode22s = Vector.tabulate(70)(_ => state2Action2)

  private def state2Action2 = StateAction(TestState(2), TestAction(2))

  private def episode32s = Vector.tabulate(70)(_ => state3Action2)

  private def state3Action2 = StateAction(TestState(3), TestAction(2))

  private def state12F1Hit = StateAction(state12F1, BlackJack.Hit)

  private def state12F1 =
    BlackJack.State(sumOfPlayersCards = 12, playerHasUsableAce = false, dealersVisibleCard = 1)

  private def state12F1Stick = StateAction(state12F1, BlackJack.Stick)

  private def state20F1Hit = StateAction(state20F1, BlackJack.Hit)

  private def state20F1Stick = StateAction(state20F1, BlackJack.Stick)

  private def state20F1 =
    BlackJack.State(sumOfPlayersCards = 20, playerHasUsableAce = false, dealersVisibleCard = 1)

  behavior of "TemporalDifference"

  it should "TemporalDifferencePredictor predict valueFunction " in {

    val predictor = TemporalDifferencePredictor[Policy](episode22, initialValueFunction)
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val (_, updatedValueFunction) = predictor.iterate(policy)
    logger info s"~updatedValueFunction=$updatedValueFunction"

    assert(updatedValueFunction.getValue(TestState(2)) === 0.55)

  }

  it should "TemporalDifferencePredictor predict actionValueFunction " in {

    val predictor = TemporalDifferencePredictor[Policy](episode22, initialActionValueFunction)
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val (_, updatedActionValueFunction) = predictor.iterate(policy)
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 0.55)

  }

  it should "TemporalDifferenceBatchPredictor" in {

    val predictor = TemporalDifferenceBatchPredictor[Policy](episode22, initialBatchValueFunction)
    val policy = deterministicPolicy.updateAction(TestState(2), TestAction(1))
    val (_, updatedBatchValueFunction) = predictor.iterate(policy)
    logger info s"~updatedBatchValueFunction=$updatedBatchValueFunction"

    assert(updatedBatchValueFunction.getValue(TestState(2)) === 0.55)

  }

  it should "iterate a SARSA increase state 22" in {

    val sarsa = SARSA(episode22, initialActionValueFunction, discount = 1d)
    val policy = softPolicy.updateAction(TestState(2), TestAction(1))
    val (improvedPolicy, updatedActionValueFunction) = sarsa.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 0.5)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 0.55)

  }

  it should "iterate a SARSA increase many state 22" in {

    val sarsa = SARSA(episode22s, initialActionValueFunction, discount = 1d)
    val policy = softPolicy.updateAction(TestState(2), TestAction(1))
    val (improvedPolicy, updatedActionValueFunction) = sarsa.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 1d +- 0.01)

  }

  it should "iterate a SARSA increase state 32" in {

    val sarsa = SARSA(episode32, initialActionValueFunction, discount = 1d)
    val policy = softPolicy.updateAction(TestState(2), TestAction(1))
    val (improvedPolicy, updatedActionValueFunction) = sarsa.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(2))) === 0.35)

  }

  it should "iterate a SARSA increase many state 32" in {

    val sarsa = SARSA(episode32s, initialActionValueFunction, discount = 1d)
    val policy = softPolicy.updateAction(TestState(2), TestAction(1))
    val (improvedPolicy, updatedActionValueFunction) = sarsa.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(2))) === -1d +- 0.01)

  }

  it should "iterate a SARSA for BlackJack Hit" in {

    val blackJack: Environment with Episodes = BlackJack()
    val initialActionValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = { policy =>
      avf.ConstantStepSize(policy, initialValue = 0d, stepSize = 0.1)
    }
    val episodes = Vector.tabulate(100)(_ => state12F1Hit) ++ Vector.tabulate(100)(_ => state12F1Stick)
    val sarsa = SARSA(episodes, initialActionValueFunction, discount = 1d)(blackJack)
    val policy = SoftPolicy(probabilityOfRandomAction = 0.1)(blackJack)
    val (improvedPolicy, updatedActionValueFunction) = sarsa.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(state12F1Hit) >= -1d)
    assert(updatedActionValueFunction.getValue(state12F1Hit) <= 1d)
    assert(updatedActionValueFunction.getValue(state12F1Stick) >= -1d)
    assert(updatedActionValueFunction.getValue(state12F1Stick) <= 1d)
    assert(updatedActionValueFunction.getValue(state12F1Hit) > updatedActionValueFunction.getValue(state12F1Stick))

  }

  it should "iterate a SARSA for BlackJack Stick" in {

    val blackJack: Environment with Episodes = BlackJack()
    val initialActionValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = { policy =>
      avf.ConstantStepSize(policy, initialValue = 0d, stepSize = 0.1)
    }
    val episodes = Vector.tabulate(100)(_ => state20F1Hit) ++ Vector.tabulate(100)(_ => state20F1Stick)
    val sarsa = SARSA(episodes, initialActionValueFunction, discount = 1d)(blackJack)
    val policy = SoftPolicy(probabilityOfRandomAction = 0.1)(blackJack)
    val (improvedPolicy, updatedActionValueFunction) = sarsa.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(state20F1Hit) >= -1d)
    assert(updatedActionValueFunction.getValue(state20F1Hit) <= 1d)
    assert(updatedActionValueFunction.getValue(state20F1Stick) >= -1d)
    assert(updatedActionValueFunction.getValue(state20F1Stick) <= 1d)
    assert(updatedActionValueFunction.getValue(state20F1Hit) < updatedActionValueFunction.getValue(state20F1Stick))

  }

  it should "iterate a Q-Learning increase state 22" in {

    val qLearning = QLearning(episode22, initialActionValueFunction, discount = 1d)
    val policy = softPolicy.updateAction(TestState(2), TestAction(1))
    val (improvedPolicy, updatedActionValueFunction) = qLearning.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))
    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 0.55)

  }

  it should "iterate a Q-Learning increase many state 22" in {

    val qLearning = QLearning(episode22s, initialActionValueFunction, discount = 1d)
    val policy = softPolicy.updateAction(TestState(2), TestAction(1))
    val (improvedPolicy, updatedActionValueFunction) = qLearning.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 1d +- 0.1)

  }

  it should "iterate a Q-Learning decrease state 32" in {

    val qLearning = QLearning(episode32, initialActionValueFunction, discount = 1d)
    val policy = softPolicy
    val (improvedPolicy, updatedActionValueFunction) = qLearning.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(2))) === 0.35)

  }

  it should "iterate a Q-Learning decrease many state 32" in {

    val qLearning = QLearning(episode32s, initialActionValueFunction, discount = 1d)
    val policy = softPolicy
    val (improvedPolicy, updatedActionValueFunction) = qLearning.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(2))) === -1d +- 0.01)

  }

  it should "iterate a Q-Learning decrease state 21" in {

    val qLearning = QLearning(episode21, initialActionValueFunction, discount = 1d)
    val policy = softPolicy
    val (improvedPolicy, updatedActionValueFunction) = qLearning.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))
    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(2))) === 0.35)

  }

  it should "iterate a Q-Learning decrease many state 21" in {

    val qLearning = QLearning(episode21s, initialActionValueFunction, discount = 1d)
    val policy = softPolicy
    val (improvedPolicy, updatedActionValueFunction) = qLearning.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 1d +- 0.01)
    assert(updatedActionValueFunction.getValue(StateAction(TestState(3), TestAction(1))) === 1d +- 0.01)

  }

  it should "iterate a Q-Learning for BlackJack Hit" in {

    val blackJack: Environment with Episodes = BlackJack()
    val initialActionValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = { policy =>
      avf.ConstantStepSize(policy, initialValue = 0d, stepSize = 0.1)
    }
    val episodes = Vector.tabulate(100)(_ => state12F1Hit) ++ Vector.tabulate(100)(_ => state12F1Stick)
    val qLearning = QLearning(episodes, initialActionValueFunction, discount = 1d)(blackJack)
    val policy = SoftPolicy(probabilityOfRandomAction = 0.1)(blackJack)
    val (improvedPolicy, updatedActionValueFunction) = qLearning.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(state12F1Hit) >= -1d)
    assert(updatedActionValueFunction.getValue(state12F1Hit) <= 1d)
    assert(updatedActionValueFunction.getValue(state12F1Stick) >= -1d)
    assert(updatedActionValueFunction.getValue(state12F1Stick) <= 1d)
    assert(updatedActionValueFunction.getValue(state12F1Hit) > updatedActionValueFunction.getValue(state12F1Stick))

  }

  it should "iterate a Q-Learning for BlackJack Stick" in {

    val blackJack: Environment with Episodes = BlackJack()
    val initialActionValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = { policy =>
      avf.ConstantStepSize(policy, initialValue = 0d, stepSize = 0.1)
    }
    val episodes = Vector.tabulate(100)(_ => state20F1Hit) ++ Vector.tabulate(100)(_ => state20F1Stick)
    val qLearning = QLearning(episodes, initialActionValueFunction, discount = 1d)(blackJack)
    val policy = SoftPolicy(probabilityOfRandomAction = 0.1)(blackJack)
    val (improvedPolicy, updatedActionValueFunction) = qLearning.iterate(policy)
    logger info s"~policy=$policy"
    logger info s"~improvedPolicy=$improvedPolicy"
    logger info s"~updatedActionValueFunction=$updatedActionValueFunction"

    assert(updatedActionValueFunction.getValue(state20F1Hit) >= -1d)
    assert(updatedActionValueFunction.getValue(state20F1Hit) <= 1d)
    assert(updatedActionValueFunction.getValue(state20F1Stick) >= -1d)
    assert(updatedActionValueFunction.getValue(state20F1Stick) <= 1d)
    assert(updatedActionValueFunction.getValue(state20F1Hit) < updatedActionValueFunction.getValue(state20F1Stick))

  }

}
