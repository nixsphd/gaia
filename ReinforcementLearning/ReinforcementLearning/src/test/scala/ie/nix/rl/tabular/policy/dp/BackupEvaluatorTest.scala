package ie.nix.rl.tabular.policy.dp

import ie.nix.rl.Environment.{StateAction, Transition}
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestEnvironment, TestState}
import ie.nix.rl.tabular.Model
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.dp.BackupEvaluatorTest.{TestEnvironmentWithModel, environmentWithModel}
import ie.nix.rl.tabular.policy.vf.ValueFunction
import ie.nix.rl.{Environment, State}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

object BackupEvaluatorTest {

  import org.scalactic.TypeCheckedTripleEquals._

  def environmentWithModel: TestEnvironmentWithModel = {
    val terminalStates = Set[State](TestState(0), TestState(4))
    val stateActions = Set[StateAction](
      StateAction(TestState(1), TestAction(1)),
      StateAction(TestState(2), TestAction(1)),
      StateAction(TestState(3), TestAction(1))
    )
    new TestEnvironmentWithModel(stateActions, terminalStates)
  }

  class TestEnvironmentWithModel(override val stateActionsSet: Set[StateAction],
                                 override val terminalStateSet: Set[State])
      extends TestEnvironment(stateActionsSet, terminalStateSet)
      with Model {

    override def getProbabilityOfTransition(transition: Environment.Transition): Double = transition match {
      case Transition(StateAction(TestState(stateId), TestAction(actionId)), TestState(finalStateId))
          if stateId + actionId === finalStateId =>
        1d
      case _ => 0d
    }
  }

}

@RunWith(classOf[JUnitRunner])
class BackupEvaluatorTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: TestEnvironmentWithModel = environmentWithModel

  override def beforeEach(): Unit = Random.setSeed(0)

  private def valueFunction = ValueFunction(deterministicPolicy, initialValue = 0d)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def valueFunction1 = ValueFunction(deterministicPolicy, initialValue = 1d)

  private def backupEvaluator = BackupEvaluator(discount = 1d, numberOfSweeps = 1)

  private def backupEvaluator0p9 = BackupEvaluator(discount = 0.9, numberOfSweeps = 1)

  behavior of "BackupEvaluator"

  it should "backup All State once with no discount" in {

    val updatedValueFunction = backupEvaluator.backupAllState(deterministicPolicy, valueFunction)
    logger info s"~valueFunction=$valueFunction, updatedValueFunction=$updatedValueFunction"

    assert(updatedValueFunction.getValue(TestState(0)) === 0d)
    assert(updatedValueFunction.getValue(TestState(1)) === 0d)
    assert(updatedValueFunction.getValue(TestState(2)) === 0d)
    assert(updatedValueFunction.getValue(TestState(3)) === 1d)
    assert(updatedValueFunction.getValue(TestState(4)) === 0d)

  }

  it should "backup All State twice with no discount" in {

    val updatedValueFunction = backupEvaluator.backupAllState(deterministicPolicy, valueFunction)
    val twiceUpdatedValueFunction = backupEvaluator.backupAllState(deterministicPolicy, updatedValueFunction)
    logger info s"~valueFunction=$valueFunction, updatedValueFunction=$updatedValueFunction, " +
      s"twiceUpdatedValueFunction=$twiceUpdatedValueFunction"

    assert(twiceUpdatedValueFunction.getValue(TestState(0)) === 0d)
    assert(twiceUpdatedValueFunction.getValue(TestState(1)) === 0d)
    assert(twiceUpdatedValueFunction.getValue(TestState(2)) === 1d)
    assert(twiceUpdatedValueFunction.getValue(TestState(3)) === 1d)
    assert(twiceUpdatedValueFunction.getValue(TestState(4)) === 0d)

  }

  it should "backup All State trice with no discount" in {

    val updatedValueFunction = backupEvaluator.backupAllState(deterministicPolicy, valueFunction)
    val twiceUpdatedValueFunction = backupEvaluator.backupAllState(deterministicPolicy, updatedValueFunction)
    val triceUpdatedValueFunction = backupEvaluator.backupAllState(deterministicPolicy, twiceUpdatedValueFunction)
    logger info s"~valueFunction=$valueFunction, updatedValueFunction=$updatedValueFunction, " +
      s"twiceUpdatedValueFunction=$twiceUpdatedValueFunction, triceUpdatedValueFunction=$triceUpdatedValueFunction"

    assert(triceUpdatedValueFunction.getValue(TestState(0)) === 0d)
    assert(triceUpdatedValueFunction.getValue(TestState(1)) === 1d)
    assert(triceUpdatedValueFunction.getValue(TestState(2)) === 1d)
    assert(triceUpdatedValueFunction.getValue(TestState(3)) === 1d)
    assert(triceUpdatedValueFunction.getValue(TestState(4)) === 0d)

  }

  it should "backup All State once with discount of 0.9" in {

    val updatedValueFunction = backupEvaluator0p9.backupAllState(deterministicPolicy, valueFunction1)
    logger info s"~valueFunction1=$valueFunction1, updatedValueFunction=$updatedValueFunction"

    assert(updatedValueFunction.getValue(TestState(0)) === 0d)
    assert(updatedValueFunction.getValue(TestState(1)) === 0.9)
    assert(updatedValueFunction.getValue(TestState(2)) === 0.9)
    assert(updatedValueFunction.getValue(TestState(3)) === 1d)
    assert(updatedValueFunction.getValue(TestState(4)) === 0d)

  }

  it should "backup All State twice with discount of 0.9" in {

    val updatedValueFunction = backupEvaluator0p9.backupAllState(deterministicPolicy, valueFunction1)
    val twiceUpdatedValueFunction = backupEvaluator0p9.backupAllState(deterministicPolicy, updatedValueFunction)
    logger info s"~valueFunction1=$valueFunction1, updatedValueFunction=$updatedValueFunction, " +
      s"twiceUpdatedValueFunction=$twiceUpdatedValueFunction"

    assert(twiceUpdatedValueFunction.getValue(TestState(0)) === 0d)
    assert(twiceUpdatedValueFunction.getValue(TestState(1)) === 0.81)
    assert(twiceUpdatedValueFunction.getValue(TestState(2)) === 0.9)
    assert(twiceUpdatedValueFunction.getValue(TestState(3)) === 1d)
    assert(twiceUpdatedValueFunction.getValue(TestState(4)) === 0d)

  }

  it should "backup All State trice with discount of 0.9" in {

    val updatedValueFunction = backupEvaluator0p9.backupAllState(deterministicPolicy, valueFunction1)
    val twiceUpdatedValueFunction = backupEvaluator0p9.backupAllState(deterministicPolicy, updatedValueFunction)
    val triceUpdatedValueFunction = backupEvaluator0p9.backupAllState(deterministicPolicy, twiceUpdatedValueFunction)
    logger info s"~valueFunction1=$valueFunction1, updatedValueFunction=$updatedValueFunction, " +
      s"twiceUpdatedValueFunction=$twiceUpdatedValueFunction, triceUpdatedValueFunction=$triceUpdatedValueFunction"

    assert(triceUpdatedValueFunction.getValue(TestState(0)) === 0d)
    assert(twiceUpdatedValueFunction.getValue(TestState(1)) === 0.81)
    assert(twiceUpdatedValueFunction.getValue(TestState(2)) === 0.9)
    assert(twiceUpdatedValueFunction.getValue(TestState(3)) === 1d)
    assert(triceUpdatedValueFunction.getValue(TestState(4)) === 0d)

  }

  it should "evaluate Policy 1 sweep with no discount" in {
    val valueFunction = BackupEvaluator(discount = 1d, numberOfSweeps = 1).evaluatePolicy(deterministicPolicy)
    logger info s"~valueFunction=$valueFunction"

    assert(valueFunction.getValue(TestState(0)) === 0d)
    assert(valueFunction.getValue(TestState(1)) === 0d)
    assert(valueFunction.getValue(TestState(2)) === 0d)
    assert(valueFunction.getValue(TestState(3)) === 1d)
    assert(valueFunction.getValue(TestState(4)) === 0d)
  }

  it should "evaluate Policy 2 sweep with no discount" in {
    val valueFunction = BackupEvaluator(discount = 1d, numberOfSweeps = 2).evaluatePolicy(deterministicPolicy)
    logger info s"~valueFunction=$valueFunction"

    assert(valueFunction.getValue(TestState(0)) === 0d)
    assert(valueFunction.getValue(TestState(1)) === 0d)
    assert(valueFunction.getValue(TestState(2)) === 1d)
    assert(valueFunction.getValue(TestState(3)) === 1d)
    assert(valueFunction.getValue(TestState(4)) === 0d)
  }

  it should "evaluate Policy 3 sweep with no discount" in {
    val valueFunction = BackupEvaluator(discount = 1d, numberOfSweeps = 3).evaluatePolicy(deterministicPolicy)
    logger info s"~valueFunction=$valueFunction"

    assert(valueFunction.getValue(TestState(0)) === 0d)
    assert(valueFunction.getValue(TestState(1)) === 1d)
    assert(valueFunction.getValue(TestState(2)) === 1d)
    assert(valueFunction.getValue(TestState(3)) === 1d)
    assert(valueFunction.getValue(TestState(4)) === 0d)
  }

  it should "evaluate Policy 1 sweep with discount of 0.9" in {
    val valueFunction = BackupEvaluator(discount = 0.9, numberOfSweeps = 1).evaluatePolicy(deterministicPolicy)
    logger info s"~valueFunction=$valueFunction"

    assert(valueFunction.getValue(TestState(0)) === 0d)
    assert(valueFunction.getValue(TestState(1)) === 0d)
    assert(valueFunction.getValue(TestState(2)) === 0d)
    assert(valueFunction.getValue(TestState(3)) === 1d)
    assert(valueFunction.getValue(TestState(4)) === 0d)
  }

  it should "evaluate Policy 2 sweep with discount of 0.9" in {
    val valueFunction = BackupEvaluator(discount = 0.9, numberOfSweeps = 2).evaluatePolicy(deterministicPolicy)
    logger info s"~valueFunction=$valueFunction"

    assert(valueFunction.getValue(TestState(0)) === 0d)
    assert(valueFunction.getValue(TestState(1)) === 0d)
    assert(valueFunction.getValue(TestState(2)) === 0.9)
    assert(valueFunction.getValue(TestState(3)) === 1d)
    assert(valueFunction.getValue(TestState(4)) === 0d)
  }

  it should "evaluate Policy 3 sweep with discount of 0.9" in {
    val valueFunction = BackupEvaluator(discount = 0.9, numberOfSweeps = 3).evaluatePolicy(deterministicPolicy)
    logger info s"~valueFunction=$valueFunction"

    assert(valueFunction.getValue(TestState(0)) === 0d)
    assert(valueFunction.getValue(TestState(1)) === 0.81)
    assert(valueFunction.getValue(TestState(2)) === 0.9)
    assert(valueFunction.getValue(TestState(3)) === 1d)
    assert(valueFunction.getValue(TestState(4)) === 0d)
  }

}
