package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BatchTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "Batch"

  it should "create ValueFunction for a policy" in {

    val policy = randomPolicy
    val actionValueFunction = ActionValueFunction(policy, initialValue = 2d)
    val batch = Batch(actionValueFunction)

    logger.info(s"policy=$policy, actionValueFunction=$actionValueFunction, batch=$batch")

    assert(batch.getStateActions.size === policy.getStateActions.size)
    assert(batch.getValue(StateAction(TestState(1), TestAction(1))) === 2d)
    assert(batch.getValue(StateAction(TestState(2), TestAction(1))) === 2d)
    assert(batch.getValue(StateAction(TestState(2), TestAction(2))) === 2d)

  }

  def randomPolicy: Policy = {
    RandomPolicy
  }

  it should "update Value" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(StateAction(TestState(1), TestAction(1)), 2d)
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 0.0)

  }

  it should "update Value and consolidate batch" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(StateAction(TestState(1), TestAction(1)), 2d)
      .consolidateBatch
    logger.info(s"updatedValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 2d)

  }

  it should "update Value, consolidate batch and update again" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(StateAction(TestState(1), TestAction(1)), 2d)
      .consolidateBatch
      .updateValue(StateAction(TestState(1), TestAction(1)), 3d)
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 2d)

  }

  it should "update Value, consolidate batch, update again and consolidate again" in {

    val updatedActionValueFunction = actionValueFunction
      .updateValue(StateAction(TestState(1), TestAction(1)), 2d)
      .consolidateBatch
      .updateValue(StateAction(TestState(1), TestAction(1)), 3d)
      .consolidateBatch
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 3d)

  }

  def actionValueFunction: Batch = {
    val actionValueFunction = ActionValueFunction(randomPolicy, initialValue = 0d)
    Batch(actionValueFunction)
  }

}
