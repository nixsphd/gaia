package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class QDLambdaTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "QDLambda"

  it should "create QDLambda for a policy" in {

    val policy = randomPolicy
    val qdLambda = QDLambda(randomPolicy,
                            discount = 1d,
                            innovationStepSize = 0.1,
                            consensusStepSize = 0.3,
                            eligibilityDecay = 0.7,
                            initialValue = 2d)
    logger.info(s"policy=$policy, qdLambda=$qdLambda")

    assert(qdLambda.getStatesOrStateActions.size === policy.getStateActions.size)
    assert(qdLambda.getValue(stateAction01) === 0d)
    assert(qdLambda.getValue(stateAction11) !== 0d)
    assert(qdLambda.getValue(stateAction21) !== 0d)

  }

  it should "create QDLambda for a policy with initial value" in {

    val policy = randomPolicy
    val qdLambda = QDLambda(randomPolicy,
                            discount = 1d,
                            innovationStepSize = 0.1,
                            consensusStepSize = 0.3,
                            eligibilityDecay = 0.7,
                            initialValue = 2d)
    logger.info(s"policy=$policy, qdLambda=$qdLambda")

    assert(qdLambda.getValue(stateAction01) === 0d)
    assert(qdLambda.getValue(stateAction11) === 2d)
    assert(qdLambda.getValue(stateAction21) === 2d)

  }

  it should "get Value" in {

    val value = qdLambda.getValue(stateAction11)
    logger.info(s"qdLambda=$qdLambda")

    assert(value === 1d)

  }

  it should "update Value" in {

    val updatedQDLambda = qdLambda.updateValue(stateAction11, innovation = 2d, consensus = 3d)
    logger.info(s"updatedQDLambda=$updatedQDLambda")

    // V(s) ← V(s) + α*innovation*Z(s) - b*consensus*Z(s)
    // 0.3 ← 1 + 0.2 - 0.9 ← 1 + (0.1*2*1 - (0.3*3*1)
    assert(updatedQDLambda.getValue(stateAction11) === 0.3 +- 0.001)

  }

  it should "update Value innovation only" in {

    val updatedQDLambda = qdLambda.updateValue(stateAction11, 2d)
    logger.info(s"updatedQDLambda=$updatedQDLambda")

    assert(updatedQDLambda.getValue(stateAction11) === 1.2)

  }

  def stateAction01: StateAction = StateAction(TestState(0), TestAction(1))
  def stateAction11: StateAction = StateAction(TestState(1), TestAction(1))
  def stateAction21: StateAction = StateAction(TestState(2), TestAction(1))
  def randomPolicy: Policy = RandomPolicy
  def equiprobablePolicy: Policy = Policy.apply
  def qdLambda =
    QDLambda(randomPolicy,
             discount = 1d,
             innovationStepSize = 0.1,
             consensusStepSize = 0.3,
             eligibilityDecay = 0.7,
             initialValue = 1d)

}
