package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ActionValueFunctionTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "ActionValueFunction"

  it should "create ValueFunction for a policy" in {

    val policy = randomPolicy
    val actionValueFunction = ActionValueFunction(policy)
    logger.info(s"policy=$policy, actionValueFunction=$actionValueFunction")

    assert(actionValueFunction.getStateActions.size === policy.getStateActions.size)
    assert(actionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) !== 0d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) !== 0d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) !== 0d)

  }

  it should "create ValueFunction for a policy with initial value" in {

    val policy = randomPolicy
    val actionValueFunction = ActionValueFunction(policy, initialValue = 2d)
    logger.info(s"policy=$policy, actionValueFunction=$actionValueFunction")

    assert(actionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 2d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 2d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 2d)

  }

  def randomPolicy: Policy = {
    RandomPolicy
  }

  it should "get a Difference between two random functions" in {

    val policy = randomPolicy
    val anActionValueFunction = ActionValueFunction(policy)
    val anotherActionValueFunction = ActionValueFunction(policy)

    val theDifference = ActionValueFunction.difference(anActionValueFunction, anotherActionValueFunction)
    logger.info(
      s"theDifference=$theDifference, anActionValueFunction=$anActionValueFunction, " +
        s"anotherActionValueFunction=$anotherActionValueFunction")

    assert(theDifference !== 0d)

  }

  it should "get no Difference between two identical functions" in {

    val policy = randomPolicy
    val anActionValueFunction = ActionValueFunction(policy, initialValue = 2d)
    val anotherActionValueFunction = ActionValueFunction(policy, initialValue = 2d)

    val theDifference = ActionValueFunction.difference(anActionValueFunction, anotherActionValueFunction)
    logger.info(
      s"theDifference=$theDifference, anActionValueFunction=$anActionValueFunction, " +
        s"anotherActionValueFunction=$anotherActionValueFunction")

    assert(theDifference === 0d)

  }

  def equiprobablePolicy: Policy = {
    Policy.apply
  }

  it should "get Value" in {

    val actionValueFunction = ActionValueFunction(randomPolicy, initialValue = 2d)

    val value = actionValueFunction.getValue(StateAction(TestState(1), TestAction(1)))
    logger.info(s"value=$value")

    assert(value === 2d)

  }

  it should "update Value" in {

    val updatedValueFunction = actionValueFunction.updateValue(StateAction(TestState(1), TestAction(1)), 2d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 2d)

  }

  private def actionValueFunction = {
    ActionValueFunction(randomPolicy, initialValue = 2d)
  }

}
