package ie.nix.rl.tabular.policy.dp

import ie.nix.rl.tabular.EnvironmentTest.{TestAction, TestState}
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.dp.DynamicProgrammingTest.{TestEnvironmentWithModel, environmentWithModel}
import ie.nix.rl.tabular.policy.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class GreedyBackupImproverTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: TestEnvironmentWithModel = environmentWithModel

  override def beforeEach(): Unit = Random.setSeed(0)

  private def valueFunction = ValueFunction(deterministicPolicy)

  private def deterministicPolicy: DeterministicPolicy = DeterministicPolicy.apply

  private def greedyBackupImprover = GreedyBackupImprover(discount = 1d)

  behavior of "GreedyBackupImprover"

  it should "getBestAction" in {

    val randomValueFunction = valueFunction
    val bestAction = greedyBackupImprover.getBestAction(TestState(2), randomValueFunction)
    logger info s"~bestAction=$bestAction, randomValueFunction=$randomValueFunction"

    assert(bestAction === TestAction(2))

  }

  it should "improvePolicy" in {

    val policy = deterministicPolicy
      .updateAction(TestState(2), TestAction(1))
    val valueFunction = ValueFunction(policy)
      .updateValue(TestState(1), 0.25)
      .updateValue(TestState(2), 0.5)
      .updateValue(TestState(3), 0.75)
    val improvedPolicy = greedyBackupImprover.improvePolicy(policy, valueFunction)
    logger info s"~policy=$policy, improvedPolicy=$improvedPolicy, valueFunction=$valueFunction"

    assert(improvedPolicy.getAction(TestState(2)) === TestAction(2))
  }

}
