package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class QDLambdaTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "QDLambda"

  it should "create QDLambda for a policy" in {

    val policy = randomPolicy
    val qdLambda = QDLambda(randomPolicy,
                            discount = 1d,
                            innovationStepSize = 0.1,
                            consensusStepSize = 0.3,
                            eligibilityDecay = 0.7,
                            initialValue = 2d)
    logger.info(s"policy=$policy, qdLambda=$qdLambda")

    assert(qdLambda.getStates.size === policy.getStates.size)
    assert(qdLambda.getValue(TestState(0)) === 0d)
    assert(qdLambda.getValue(TestState(1)) !== 0d)
    assert(qdLambda.getValue(TestState(2)) !== 0d)

  }

  it should "create QDLambda for a policy with initial value" in {

    val policy = randomPolicy
    val qdLambda = QDLambda(randomPolicy,
                            discount = 1d,
                            innovationStepSize = 0.1,
                            consensusStepSize = 0.3,
                            eligibilityDecay = 0.7,
                            initialValue = 2d)
    logger.info(s"policy=$policy, qdLambda=$qdLambda")

    assert(qdLambda.getValue(TestState(0)) === 0d)
    assert(qdLambda.getValue(TestState(1)) === 2d)
    assert(qdLambda.getValue(TestState(2)) === 2d)

  }

  it should "get Value" in {

    val value = qdLambda.getValue(TestState(1))
    logger.info(s"qdLambda=$qdLambda")

    assert(value === 1d)

  }

  it should "update Value" in {

    val updatedQDLambda = qdLambda.updateValue(TestState(1), innovation = 2d, consensus = 3d)
    logger.info(s"updatedQDLambda=$updatedQDLambda")

    // V(s) ← V(s) + α*innovation*Z(s) - b*consensus*Z(s)
    // 0.3 ← 1 + 0.2 - 0.9 ← 1 + (0.1*2*1 - (0.3*3*1)
    assert(updatedQDLambda.getValue(TestState(1)) === 0.3 +- 0.0001)

  }

  it should "update Value innovation only" in {

    val updatedQDLambda = qdLambda.updateValue(TestState(1), 2d)
    logger.info(s"updatedQDLambda=$updatedQDLambda")

    assert(updatedQDLambda.getValue(TestState(1)) === 1.2)

  }

  def randomPolicy: Policy = RandomPolicy

  def equiprobablePolicy: Policy = Policy.apply

  def qdLambda = {
    QDLambda(randomPolicy,
             discount = 1d,
             innovationStepSize = 0.1,
             consensusStepSize = 0.3,
             eligibilityDecay = 0.7,
             initialValue = 1d)
  }

}
