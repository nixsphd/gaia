package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.Environment
import ie.nix.rl.tabular.EnvironmentTest.{TestState, environment}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.Policy.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class WeightedAveragingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = Random.setSeed(0)

  private def valueFunction = WeightedAveraging(randomPolicy)

  private def randomPolicy: Policy = RandomPolicy

  behavior of "WeightedAveraging VF"

  it should "create WeightedAveraging for a policy" in {

    val policy = randomPolicy
    val weightedAveraging = WeightedAveraging(policy)
    logger.info(s"policy=$policy, weightedAveraging=$weightedAveraging")

    assert(weightedAveraging.getStates.size === policy.getStates.size)
    assert(weightedAveraging.getValue(TestState(0)) === 0d)
    assert(weightedAveraging.getValue(TestState(1)) === 0d)
    assert(weightedAveraging.getValue(TestState(2)) === 0d)

  }

  it should "update Value without a weight" in {

    val updatedValueFunction = valueFunction.updateValue(TestState(1), 2d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 2d)

  }

  it should "update Value with a weight" in {

    val updatedValueFunction = valueFunction.updateValue(TestState(1), 2d, 3d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 2d)

  }

  it should "update Value with a weight twice" in {

    val updatedValueFunction = valueFunction
      .updateValue(TestState(1), 2d, 3d)
      .updateValue(TestState(1), 3d, 1d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(TestState(1)) === 9d / 4d)

  }

}
