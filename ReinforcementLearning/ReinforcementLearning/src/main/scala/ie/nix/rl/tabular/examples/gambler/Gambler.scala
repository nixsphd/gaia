package ie.nix.rl.tabular.examples.gambler

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.examples.gambler.Casino.{GamblersBet, GamblersCapitol}
import ie.nix.rl.tabular.policy
import ie.nix.rl.tabular.policy.DeterministicPolicy

object Gambler {

  def OstentatiousBetter(implicit casino: Casino): DeterministicPolicy = {
    casino.getNonTerminalStates
      .foldLeft(policy.DeterministicPolicy(casino))((policy, state) => {
        state match {
          case gamblersCapitol: GamblersCapitol =>
            val maxBet = math.min(gamblersCapitol.capitol, casino.targetCapitol - gamblersCapitol.capitol)
            policy updateAction StateAction(state, GamblersBet(maxBet))
        }
      })
  }

  def ModestBetter(implicit casino: Casino): DeterministicPolicy = {
    casino.getNonTerminalStates
      .foldLeft(policy.DeterministicPolicy(casino))((policy, state) => {
        policy updateAction StateAction(state, GamblersBet(1))
      })
  }

}
