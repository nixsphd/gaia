package ie.nix.rl.policy

import ie.nix.rl.State

trait EpisodicPolicyImprover[P <: Policy, -VF] {

  def improvePolicy(policy: P, valueFunction: VF, statesToUpdate: Vector[State]): P

}
