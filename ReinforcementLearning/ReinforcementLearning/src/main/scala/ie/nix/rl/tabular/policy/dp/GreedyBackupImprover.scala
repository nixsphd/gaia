package ie.nix.rl.tabular.policy.dp

import ie.nix.rl.policy.{PolicyImprover, ValueFunction}
import ie.nix.rl.tabular.Model
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.dp.DynamicProgramming.backupState
import ie.nix.rl.{Action, Environment, State}
import org.apache.logging.log4j.scala.Logging

object GreedyBackupImprover {

  def apply(discount: Double)(implicit environment: Environment with Model): GreedyBackupImprover = {
    new GreedyBackupImprover(discount)
  }

}

class GreedyBackupImprover private[GreedyBackupImprover] (discount: Double)(
    implicit environment: Environment with Model)
    extends PolicyImprover[DeterministicPolicy, ValueFunction]
    with Logging {

  override def improvePolicy(policy: DeterministicPolicy, valueFunction: ValueFunction): DeterministicPolicy = {

    policy.getStates.foldLeft(policy)((policy, state) => {
      val bestAction = getBestAction(state, valueFunction)
      logger.trace(s"~state=$state, bestAction=$bestAction")
      policy.updateAction(state, bestAction)
    })

  }

  def getBestAction(state: State, valueFunction: ValueFunction): Action = {

    val (bestStateAction, value) = environment
      .getStateActionsForState(state)
      .map(stateAction => (stateAction, backupState(stateAction, discount, valueFunction)))
      .maxBy(_._2)
    logger.trace(s"~state=$state, bestStateAction=$bestStateAction, value=$value, valueFunction=$valueFunction")
    bestStateAction.action

  }

}
