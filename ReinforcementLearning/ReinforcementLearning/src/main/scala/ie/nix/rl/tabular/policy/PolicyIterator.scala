package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.{PolicyEvaluator, PolicyImprover}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

object PolicyIterator extends Logging {

  def equals(aPolicy: Policy, anotherPolicy: Policy): Boolean = {
    logger.info(s"aPolicy=$aPolicy, anotherPolicy=$anotherPolicy")
    policyDifference(aPolicy, anotherPolicy) === 0d
  }

  def policyDifference[P <: ie.nix.rl.policy.Policy](aPolicy: P, anotherPolicy: P): Double = {

    def getAllStateActions: Set[StateAction] = {
      (aPolicy.getStateActions ++ anotherPolicy.getStateActions).toSet
    }

    val allStateActions = getAllStateActions
    (for {
      stateAction <- allStateActions
      probability = aPolicy.getProbability(stateAction)
      otherProbability = anotherPolicy.getProbability(stateAction)
    } yield {
      math.abs(probability - otherProbability)
    }).sum

  }

}

class PolicyIterator[P <: ie.nix.rl.policy.Policy, VF](val numberOfIterations: Int)(
    implicit val evaluator: PolicyEvaluator[P, VF],
    implicit val improver: PolicyImprover[P, VF])
    extends ie.nix.rl.policy.PolicyIterator[P, VF]
    with Logging {

  override def iterate(policy: P): (P, VF) = {

    (1 to numberOfIterations).foldLeft(evaluateAndImprovePolicy(policy))((policyAndValueFunction, _) => {
      val policy = policyAndValueFunction._1
      evaluateAndImprovePolicy(policy)
    })

  }

  def evaluateAndImprovePolicy(policy: P): (P, VF) = {

    // Evaluate the policy
    val valueFunction = evaluator.evaluatePolicy(policy)
    logger info s"~valueFunction=$valueFunction"
    // Improve the policy
    (improver.improvePolicy(policy, valueFunction), valueFunction)
  }

}
