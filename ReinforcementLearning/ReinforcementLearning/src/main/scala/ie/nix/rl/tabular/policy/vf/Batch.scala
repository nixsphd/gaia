package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging

object Batch {

  def apply(valueFunction: policy.ValueFunction): Batch = {
    new Batch(getStateValueMap(valueFunction), valueFunction)
  }

  def getStateValueMap(valueFunction: policy.ValueFunction): Map[State, Double] = {
    (for { state <- valueFunction.getStates } yield {
      (state, valueFunction.getValue(state))
    }).toMap
  }
}

class Batch private[vf] (valueMap: Map[State, Double], val valueFunction: policy.ValueFunction)
    extends AbstractBatch[State](valueMap)
    with policy.ValueFunction
    with Logging {

  override def getStates: Vector[State] = valueMap.keySet.toVector

  override def updateValue(state: State, value: Double): Batch =
    new Batch(valueMap, valueFunction.updateValue(state, value))

  def consolidateBatch: Batch = {

    valueMap.foldLeft(this)((batch, stateValue) => {
      val state = stateValue._1
      val valueToConsolidate = valueFunction.getValue(state)
      logger.trace(s"~state=$state, valueToConsolidate=$valueToConsolidate")
      new Batch(batch.valueMap + (state -> valueToConsolidate), valueFunction)
    })

  }

}

abstract class AbstractBatch[S] private[policy] (valueMap: Map[S, Double])
    extends AbstractValueFunction[S, Double](valueMap)
    with Logging {

  override def mapValue(value: Double): Double = value

  def consolidateBatch: AbstractBatch[S]

}
