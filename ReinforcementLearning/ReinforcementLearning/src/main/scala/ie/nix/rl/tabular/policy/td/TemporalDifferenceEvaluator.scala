package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment.TransitionReward
import ie.nix.rl.policy.{Policy, ValueFunction}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator.{getVisitedStates, runEpisode}
import ie.nix.rl.tabular.policy.vf
import ie.nix.rl.{Environment, State}
import org.apache.logging.log4j.scala.Logging

object TemporalDifferenceEvaluator {

  def apply[P <: Policy](discount: Double)(
      implicit environment: Environment with Episodes): TemporalDifferenceEvaluator[P, vf.ConstantStepSize] = {

    new TemporalDifferenceEvaluator[P, vf.ConstantStepSize](discount) {
      override def updateValueFunction(constantStepSize: vf.ConstantStepSize,
                                       stateAction: Episode,
                                       target: Double): vf.ConstantStepSize = {
        constantStepSize.updateValue(stateAction.state, target)
      }
    }

  }

  def BatchTemporalDifferenceEvaluator[P <: Policy](discount: Double)(
      implicit environment: Environment with Episodes): TemporalDifferenceEvaluator[P, vf.Batch] = {

    new TemporalDifferenceEvaluator[P, vf.Batch](discount) {
      override def updateValueFunction(batch: vf.Batch, stateAction: Episode, target: Double): vf.Batch = {
        batch.updateValue(stateAction.state, target)
      }
    }

  }

}

abstract class TemporalDifferenceEvaluator[P <: Policy, VF <: ValueFunction](override val discount: Double)(
    implicit environment: Environment with Episodes)
    extends EpisodicEvaluator[P, VF](discount)
    with Logging {

  override def evaluatePolicy(policy: P, valueFunction: VF, episode: Episode): (VF, Vector[State]) = {

    val transitionRewards = runEpisode(episode, policy)
    val updatedValueFunction = updateValueFunction(valueFunction, transitionRewards)
    val visitedStates = getVisitedStates(episode, transitionRewards)

    (updatedValueFunction, visitedStates)

  }

  override def updateValueFunction(valueFunction: VF, transitionRewards: Vector[TransitionReward]): VF = {

    val updatedValueFunction =
      transitionRewards.foldLeft(valueFunction)((valueFunction, transitionReward) => {

        val stateAction = transitionReward.transition.stateAction
        val target = getTarget(valueFunction, transitionReward)
        updateValueFunction(valueFunction, stateAction, target)

      })
    updatedValueFunction

  }

  def getTarget(valueFunction: VF, transitionReward: TransitionReward): Double = {

    val state = transitionReward.transition.stateAction.state // S
    val finalState = transitionReward.transition.finalState // S'
    val originalFinalStateValue = valueFunction.getValue(finalState) // V(S')
    val reward = transitionReward.reward
    val target = reward + (discount * originalFinalStateValue) // R + γV(S′)
    logger.trace(
      s"~state=$state, finalState=$finalState, originalFinalStateValue=$originalFinalStateValue, " +
        s"reward=$reward, target=$target")
    target

  }

}
