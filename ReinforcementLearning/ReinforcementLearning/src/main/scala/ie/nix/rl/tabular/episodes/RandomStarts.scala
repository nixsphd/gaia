package ie.nix.rl.tabular.episodes

import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.episodes.Episodes.Episode

import scala.util.Random

trait RandomStarts extends Episodes {

  environment: Environment with Episodes =>

  override def getStarts(numberOfStarts: Int): Vector[Episode] = {
    val nonTerminalStates = getNonTerminalStates
    (1 to numberOfStarts)
      .map(_ => {
        val state = nonTerminalStates(Random.nextInt(nonTerminalStates.length))
        val stateActionsForState = getStateActionsForState(state)
        val stateAction = stateActionsForState(Random.nextInt(stateActionsForState.length))
        stateAction: Episode
      })
      .toVector
  }

}
