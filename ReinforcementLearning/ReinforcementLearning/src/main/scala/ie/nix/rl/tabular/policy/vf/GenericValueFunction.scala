package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object GenericValueFunction {

  val randomValue: () => Double = () => Random.nextDouble()

  def getStateValueMap[V](policy: Policy)(initialValue: () => V): Map[State, V] = {
    (for { state <- policy.getStates } yield {
      (state, initialValue())
    }).toMap
  }

  def getStateActionValueMap[V](policy: Policy)(initialValue: () => V): Map[StateAction, V] = {
    (for { stateAction <- policy.getStateActions } yield {
      (stateAction, initialValue())
    }).toMap
  }

  def difference[S](aGenericValueFunction: GenericValueFunction[S, _],
                    anotherGenericValueFunction: GenericValueFunction[S, _]): Double = {
    val statesOrStateActions =
      (aGenericValueFunction.valueMap.keySet ++ anotherGenericValueFunction.valueMap.keySet).toVector
    val difference = (for { statesOrStateAction <- statesOrStateActions } yield {
      val aValue = aGenericValueFunction.getValue(statesOrStateAction)
      val otherValue = anotherGenericValueFunction.getValue(statesOrStateAction)
      math.abs(aValue - otherValue)
    }).sum
    difference
  }

}

abstract class GenericValueFunction[S, V] private[policy] (val valueMap: Map[S, V]) extends Logging {

  override def toString: String = {
    valueMap.toVector.sortBy(_._1.toString).toString()
  }

  def getStateOrStateAction: Vector[S] = valueMap.keySet.toVector

  def getValue(stateOrStateAction: S): Double =
    valueMap.get(stateOrStateAction: S) match {
      case Some(value) => mapValue(value)
      case None        => 0d
      // assume state not stored in the map are terminal, value is always = 0
    }

  def mapValue(value: V): Double

  def updateValue(stateOrStateAction: S, value: V): GenericValueFunction[S, V]

}

trait States extends policy.ValueFunction with Logging {

  self: GenericValueFunction[State, _] =>

  override def getStates: Vector[State] = self.valueMap.keySet.toVector

}

trait StateActions extends policy.ActionValueFunction with Logging {

  self: GenericValueFunction[StateAction, _] =>

  override def getStateActions: Vector[StateAction] = self.valueMap.keySet.toVector

}
