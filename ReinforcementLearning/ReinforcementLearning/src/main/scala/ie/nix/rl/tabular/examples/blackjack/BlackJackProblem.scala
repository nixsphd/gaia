package ie.nix.rl.tabular.examples.blackjack

import ie.nix.rl.policy.Determinism
import ie.nix.rl.tabular.episodes.ExploringStarts
import ie.nix.rl.tabular.examples.CSVFileWriterHelper.{writeBlackJackPolicy, writeBlackJackValueFunction}
import ie.nix.rl.tabular.policy.et.EligibilityTraces.{QLearningLambdaWatkins, SARSALambda}
import ie.nix.rl.tabular.policy.mc.MonteCarlo
import ie.nix.rl.tabular.policy.td.TemporalDifference.{QLearning, SARSA}
import ie.nix.rl.tabular.policy.{DeterministicPolicy, Policy, SoftPolicy, avf}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object BlackJackProblemWithMC extends App with Logging {

  Random.setSeed(0)

  implicit val blackJack: BlackJack with ExploringStarts =
    new BlackJack() with ExploringStarts
  //  implicit val blackJack = new BlackJack() with RandomStarts

  val starts = blackJack.getStarts(numberOfStarts = 500000) // 500,000
  val discount: Double = 1d
  val policy = DeterministicPolicy(blackJack)

  val (improvedPolicy, actionValueFunction) = MonteCarlo(starts, discount).iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~actionValueFunction=$actionValueFunction")

  writeBlackJackPolicy(improvedPolicy, "BlackJackMC.csv")
  writeBlackJackValueFunction(actionValueFunction, "BlackJackMCAVF.csv")

}

object BlackJackProblemWithMCSoft extends App with Logging {

  Random.setSeed(0)

  implicit val blackJack: BlackJack = BlackJack()

  val starts = blackJack.getStarts(numberOfStarts = 500000) // = 500,000
  val discount: Double = 1d
  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)

  val (improvedPolicy, actionValueFunction) = MonteCarlo(starts, discount).iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~actionValueFunction=$actionValueFunction")

  writeBlackJackPolicy(improvedPolicy, "BlackJackMCSoft.csv")
  writeBlackJackValueFunction(actionValueFunction, "BlackJackMCSoftAVF.csv")

}

object BlackJackProblemWithMCOffline extends App with Logging {

  Random.setSeed(0)

  implicit val blackJack: BlackJack with ExploringStarts =
    new BlackJack() with ExploringStarts

  val starts = blackJack.getStarts(numberOfStarts = 750000) // = 750000
  val discount: Double = 1d
  val policy: Policy with Determinism = DeterministicPolicy.apply
  val initSoftBehaviourPolicy: ie.nix.rl.policy.Policy with Determinism => SoftPolicy =
    targetPolicy => SoftPolicy(probabilityOfRandomAction = 0.1, targetPolicy)

  val (improvedPolicy, actionValueFunction) =
    MonteCarlo(starts, initSoftBehaviourPolicy, discount).iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~actionValueFunction=$actionValueFunction")

  writeBlackJackPolicy(improvedPolicy, "BlackJackMCOffline.csv")
  writeBlackJackValueFunction(actionValueFunction, "BlackJackMCOfflineAVF.csv")

}

object BlackJackProblemWithSARSA extends App with Logging {

  Random.setSeed(0)

  implicit val blackJack: BlackJack with ExploringStarts = new BlackJack() with ExploringStarts
  val episodes = blackJack.getStarts(numberOfStarts = 1000000) // 1,000,000

  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = policy =>
    avf.ConstantStepSize(policy, stepSize = 0.001)

  val sarsa = SARSA(episodes, initialValueFunction, discount = 1d)

  val (improvedPolicy, updatedValueFunction) = sarsa.iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  writeBlackJackPolicy(improvedPolicy, "BlackJackSARSA.csv")
  writeBlackJackValueFunction(updatedValueFunction, "BlackJackSARSAAVF.csv")

}

object BlackJackProblemWithQLearning extends App with Logging {

  Random.setSeed(0)

  implicit val blackJack: BlackJack with ExploringStarts = new BlackJack() with ExploringStarts
  val episodes = blackJack.getStarts(numberOfStarts = 1000000) // 1,000,000

  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = policy =>
    avf.ConstantStepSize(policy, stepSize = 0.005)

  val qLearning = QLearning(episodes, initialValueFunction, discount = 1d)

  val (improvedPolicy, updatedValueFunction) = qLearning.iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  writeBlackJackPolicy(improvedPolicy, "BlackJackQLearning.csv")
  writeBlackJackValueFunction(updatedValueFunction, "BlackJackQLearningAVF.csv")

}

object BlackJackProblemWithSARSALambda extends App with Logging {

  Random.setSeed(0)

  implicit val blackJack: BlackJack with ExploringStarts = new BlackJack() with ExploringStarts
  val episodes = blackJack.getStarts(numberOfStarts = 1000000) // 1,000,000

  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)
  val discount = 1d
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.EligibilityTracing = policy =>
    avf.EligibilityTracing(policy, discount, stepSize = 0.01, eligibilityDecay = 0.9)

  val sarsaLambda = SARSALambda(episodes, initialValueFunction, discount)

  val (improvedPolicy, updatedValueFunction) = sarsaLambda.iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  writeBlackJackPolicy(improvedPolicy, "BlackJackSARSALambda.csv")
  writeBlackJackValueFunction(updatedValueFunction, "BlackJackSARSALambdaAVF.csv")

}

object BlackJackProblemWithQLearningLambdaWatkins extends App with Logging {

  Random.setSeed(0)

  implicit val blackJack: BlackJack with ExploringStarts = new BlackJack() with ExploringStarts
  val episodes = blackJack.getStarts(numberOfStarts = 2000000) // 1,000,000

  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)
  val discount = 1d
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.EligibilityTracing = policy =>
    avf.EligibilityTracing(policy, discount, stepSize = 0.001, eligibilityDecay = 0.8)

  val qLearningLambdaWatkins = QLearningLambdaWatkins(episodes, initialValueFunction, discount)

  val (improvedPolicy, updatedValueFunction) = qLearningLambdaWatkins.iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  writeBlackJackPolicy(improvedPolicy, "BlackJackQLearningLambdaWatkins.csv")
  writeBlackJackValueFunction(updatedValueFunction, "BlackJackQLearningLambdaWatkinsAVF.csv")

}
