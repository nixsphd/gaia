package ie.nix.rl.tabular.examples

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.{ActionValueFunction, Determinism, Policy, ValueFunction}
import ie.nix.rl.tabular.examples.blackjack.BlackJack
import ie.nix.util.CSVFileWriter
import org.apache.logging.log4j.scala.Logging

object CSVFileWriterHelper extends Logging {

  def writePolicyWithDeterminism(policy: Policy with Determinism, filename: String)(
      implicit environment: Environment): Unit = {
    val csvFileWriter: CSVFileWriter = CSVFileWriter("results", filename)
    csvFileWriter.writeHeaders("State", "Action")
    environment.getNonTerminalStates.foreach(state => {
      csvFileWriter.writeRow(state, policy.getAction(state))
    })
  }

  def writeBlackJackPolicy(policy: Policy, filename: String)(implicit environment: Environment): Unit = {
    val csvFileWriter = CSVFileWriter("results", filename)
    csvFileWriter.writeHeaders("sumOfPlayersCards", "playerHasUsableAce", "dealersVisibleCard", "Action")
    for { state <- environment.getNonTerminalStates } state match {
      case state: BlackJack.State =>
        csvFileWriter.writeRow(state.sumOfPlayersCards,
                               state.playerHasUsableAce,
                               state.dealersVisibleCard,
                               policy.getMostProbableAction(state))
    }
  }

  def writeValueFunction(valueFunction: ValueFunction, filename: String): Unit = {
    val csvFileWriter = CSVFileWriter("results", filename)
    csvFileWriter.writeHeaders("State", "Value")
    for (state <- valueFunction.getStates) {
      csvFileWriter.writeRow(state, valueFunction.getValue(state))
    }
  }

  def writeActionValueFunction(actionValueFunction: ActionValueFunction, filename: String): Unit = {
    val csvFileWriter = CSVFileWriter("results", filename)
    csvFileWriter.writeHeaders("State", "Action", "Value")
    for (stateAction <- actionValueFunction.getStateActions) {
      csvFileWriter.writeRow(stateAction.state, stateAction.action, actionValueFunction.getValue(stateAction))
    }
  }

  def writeBlackJackValueFunction(actionValueFunction: ActionValueFunction, filename: String): Unit = {
    val csvFileWriter = CSVFileWriter("results", filename)
    csvFileWriter.writeHeaders("sumOfPlayersCards", "playerHasUsableAce", "dealersVisibleCard", "Action", "Value")
    for { StateAction(state: BlackJack.State, action) <- actionValueFunction.getStateActions } {
      csvFileWriter.writeRow(state.sumOfPlayersCards,
                             state.playerHasUsableAce,
                             state.dealersVisibleCard,
                             action,
                             actionValueFunction.getValue(StateAction(state, action)))
    }
  }

}
