package ie.nix.rl.tabular.examples.blackjack

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.examples.blackjack.BlackJack.{Hit, Stick}
import ie.nix.rl.tabular.policy
import ie.nix.rl.tabular.policy.DeterministicPolicy
import org.apache.logging.log4j.scala.Logging

object Player extends Logging {

  def Sticker(implicit environment: BlackJack): DeterministicPolicy = {
    environment.getNonTerminalStates
      .foldLeft(policy.DeterministicPolicy(environment))((policy, state) => {
        policy updateAction StateAction(state, Stick)
      })
  }

  def Hitter(implicit environment: BlackJack): DeterministicPolicy = {
    environment.getNonTerminalStates
      .foldLeft(policy.DeterministicPolicy(environment))((policy, state) => {
        policy updateAction StateAction(state, Hit)
      })
  }

}
