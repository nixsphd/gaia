package ie.nix.rl.tabular.policy.et

import ie.nix.rl.Environment.{StateAction, Transition}
import ie.nix.rl.policy.{EpisodicPolicyEvaluator, Policy}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.avf
import ie.nix.rl.tabular.policy.td.TemporalDifferenceEvaluatorQF.getStateActionValue
import ie.nix.rl.{Environment, State}
import org.apache.logging.log4j.scala.Logging

class EligibilityTracesEvaluatorQF[P <: Policy](val discount: Double)(implicit environment: Environment with Episodes)
    extends EpisodicPolicyEvaluator[P, avf.EligibilityTracing]
    with Logging {

  override def evaluatePolicy(policy: P,
                              eligibilityTraces: avf.EligibilityTracing,
                              episode: Episode): (avf.EligibilityTracing, Vector[State]) = {

    val transition = environment.getTransitionForStateAction(episode)
    val updatedEligibilityTraces = runEpisodeStep(policy, eligibilityTraces, transition)
    (updatedEligibilityTraces, Vector[State]())
  }

  def runEpisodeStep(policy: Policy,
                     eligibilityTraces: avf.EligibilityTracing,
                     transition: Transition): avf.EligibilityTracing = {
    val error = getError(eligibilityTraces, transition)
    logger trace s"transition=$transition, error=$error"

    val updatedEligibilityTraces = eligibilityTraces.updateValue(transition.stateAction, error)
    logger info s"updatedEligibilityTraces=$updatedEligibilityTraces"

    nextEpisodeStep(policy, updatedEligibilityTraces, transition)
  }

  def getError(eligibilityTraces: avf.EligibilityTracing, transition: Transition): Double = {
    val reward = environment.getRewardForTransition(transition)
    val stateActionValue = eligibilityTraces.getValue(transition.stateAction)
    val nextStateActionValue = getStateActionValue(eligibilityTraces, transition.finalState)
    // δ ← R + γV(S′) − V(S)
    reward + (discount * nextStateActionValue) - stateActionValue
  }

  def nextEpisodeStep(policy: Policy,
                      eligibilityTracing: avf.EligibilityTracing,
                      transition: Transition): avf.EligibilityTracing = {

    logger trace s"transition=$transition, transition.isTerminal=${transition.isTerminal}"
    if (transition.isTerminal) {
      eligibilityTracing
    } else {
      val nextState = transition.finalState
      val nextStateAction = StateAction(nextState, policy.getAction(nextState))
      val nextTransition = environment.getTransitionForStateAction(nextStateAction)
      runEpisodeStep(policy, eligibilityTracing, nextTransition)
    }
  }

}
