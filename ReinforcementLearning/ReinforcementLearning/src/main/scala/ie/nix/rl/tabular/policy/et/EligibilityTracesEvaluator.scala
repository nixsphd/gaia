package ie.nix.rl.tabular.policy.et

import ie.nix.rl.Environment.{StateAction, Transition}
import ie.nix.rl.policy.{EpisodicPolicyEvaluator, Policy}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.vf
import ie.nix.rl.{Environment, State}
import org.apache.logging.log4j.scala.Logging

class EligibilityTracesEvaluator[P <: Policy](val discount: Double)(implicit environment: Environment with Episodes)
    extends EpisodicPolicyEvaluator[P, vf.EligibilityTracing]
    with Logging {

  override def evaluatePolicy(policy: P,
                              eligibilityTraces: vf.EligibilityTracing,
                              episode: Episode): (vf.EligibilityTracing, Vector[State]) = {

    val transition = environment.getTransitionForStateAction(episode)
    val updatedEligibilityTraces = runEpisodeStep(policy, eligibilityTraces, transition)
    (updatedEligibilityTraces, Vector[State]())
  }

  def runEpisodeStep(policy: Policy,
                     eligibilityTraces: vf.EligibilityTracing,
                     transition: Transition): vf.EligibilityTracing = {
    val state = transition.stateAction.state
    val nextState = transition.finalState
    val reward = environment.getRewardForTransition(transition)
    val error = getError(eligibilityTraces, state, nextState, reward)
    logger trace s"transition=$transition, error=$error"

    val updatedEligibilityTraces = eligibilityTraces.updateValue(state, error)
    logger info s"updatedEligibilityTraces=$updatedEligibilityTraces"

    nextEpisodeStep(policy, updatedEligibilityTraces, transition)
  }

  def getError(eligibilityTraces: vf.EligibilityTracing, state: State, nextState: State, reward: Double): Double = {
    val stateValue = eligibilityTraces.getValue(state)
    val nextStateValue = eligibilityTraces.getValue(nextState)
    // δ ← R + γV(S′) − V(S)
    reward + (discount * nextStateValue) - stateValue
  }

  def nextEpisodeStep(policy: Policy,
                      eligibilityTracing: vf.EligibilityTracing,
                      transition: Transition): vf.EligibilityTracing = {
    if (transition.isTerminal) {
      eligibilityTracing
    } else {
      val nextState = transition.finalState
      val nextStateAction = StateAction(nextState, policy.getAction(nextState))
      val nextTransition = environment.getTransitionForStateAction(nextStateAction)
      runEpisodeStep(policy, eligibilityTracing, nextTransition)
    }
  }

}
