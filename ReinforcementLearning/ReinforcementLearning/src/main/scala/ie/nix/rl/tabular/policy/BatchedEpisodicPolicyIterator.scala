package ie.nix.rl.tabular.policy

import ie.nix.rl.policy.{EpisodicPolicyEvaluator, EpisodicPolicyImprover}
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.vf.Batch
import org.apache.logging.log4j.scala.Logging

class BatchedEpisodicPolicyIterator[P <: ie.nix.rl.policy.Policy](episodes: Vector[Episode],
                                                                  initialValueFunction: P => Batch)(
    implicit evaluator: EpisodicPolicyEvaluator[P, Batch],
    improver: EpisodicPolicyImprover[P, Batch])
    extends EpisodicPolicyIterator[P, Batch](episodes, initialValueFunction)
    with Logging {

  override def iterate(policy: P): (P, Batch) = {
    val valueFunction: Batch = initialValueFunction(policy)
    episodes.zipWithIndex.foldLeft((policy, valueFunction))((policyAndValueFunction, episodeWithIndex) => {
      val index = episodeWithIndex._2
      val batchOfEpisodes = episodes.slice(0, index + 1) // +1 as slice's second param is an "until"
      logger.debug(s"~index=$index, batchOfEpisodes=$batchOfEpisodes")
      val (updatedPolicy, updatedValueFunction) = iterateBatch(policyAndValueFunction, batchOfEpisodes)
      (updatedPolicy, updatedValueFunction.consolidateBatch)
    })
  }

  def iterateBatch(policyAndValueFunction: (P, Batch), batchOfEpisodes: Vector[Episode]): (P, Batch) = {

    val policy = policyAndValueFunction._1
    val valueFunction = policyAndValueFunction._2

    batchOfEpisodes.tail.foldLeft(evaluateAndImprovePolicy(policy, valueFunction, episodes.head))(
      (policyAndValueFunction, episode) => {
        val targetPolicy = policyAndValueFunction._1
        val valueFunction = policyAndValueFunction._2
        evaluateAndImprovePolicy(targetPolicy, valueFunction, episode)
      })

  }

}
