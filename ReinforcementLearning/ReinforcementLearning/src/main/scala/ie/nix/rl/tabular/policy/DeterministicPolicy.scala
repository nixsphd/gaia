package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Determinism
import ie.nix.rl.tabular.EnvironmentView
import ie.nix.rl.tabular.policy.Policy.getStateActionProbabilityMap
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

object DeterministicPolicy extends Logging {

  def apply(implicit environment: EnvironmentView): DeterministicPolicy = {
    new DeterministicPolicy(getRandomStateActionProbabilityMap(environment))
  }

  def apply(policy: Policy): DeterministicPolicy = {
    new DeterministicPolicy(getDeterministicStateActionProbabilityMap(policy))
  }

  def apply(stateActionProbabilityMap: Map[StateAction, Double]): DeterministicPolicy = {
    new DeterministicPolicy(stateActionProbabilityMap)
  }

  def getRandomStateActionProbabilityMap(implicit environment: EnvironmentView): Map[StateAction, Double] = {
    (for {
      state <- environment.getNonTerminalStates
      actions = environment.getActionsForState(state)
      randomAction = actions(Random.nextInt(actions.length))
      action <- actions
    } yield {
      if (action === randomAction) {
        (StateAction(state, action), 1d)
      } else {
        (StateAction(state, action), 0d)
      }
    }).toMap
  }

  def getDeterministicStateActionProbabilityMap(policy: Policy): Map[StateAction, Double] = {
    (for {
      state <- policy.getStates
      actions = policy.getActions(state)
      selectedAction = policy.getMostProbableAction(state)
      action <- actions
    } yield {
      if (action === selectedAction) {
        (StateAction(state, action), 1d)
      } else {
        (StateAction(state, action), 0d)
      }
    }).toMap
  }

  def Policy(deterministicPolicy: DeterministicPolicy): Policy = {
    new Policy(getStateActionProbabilityMap(deterministicPolicy))
  }

  def isDeterministic(policy: Policy): Boolean = {
    (for {
      state <- policy.getStates
      actionForState = policy.getAction(state)
      stateActions = policy.getStateActions(state)
      stateAction <- stateActions
      probability = policy.getProbability(stateAction)
    } yield {
      if (stateAction.action === actionForState) {
        probability === 1d
      } else {
        probability === 0d
      }
    }).reduce(_ && _)
  }

  def isEquivalent(deterministicPolicy: DeterministicPolicy, policy: Policy): Boolean = {
    (for {
      state <- deterministicPolicy.getStates
      actionForDeterministicPolicy = deterministicPolicy.getAction(state)
      actionForPolicy = policy.getMostProbableAction(state)
    } yield {
      actionForDeterministicPolicy === actionForPolicy
    }).reduce(_ && _)
  }

}

class DeterministicPolicy private[policy] (stateActionProbabilityMap: Map[StateAction, Double])
    extends Policy(stateActionProbabilityMap)
    with Determinism
    with Logging {

  override def getAction(state: State): Action = {

    val actionsWithProbabilityEquals1 = for {
      (action, probability) <- getActionAndProbabilities(state)
      if probability === 1d
    } yield action
    logger.trace(s"~actionsWithProbabilityEquals1=$actionsWithProbabilityEquals1")

    actionsWithProbabilityEquals1 match {
      case action +: Vector() => action
      case actions if actions.length === 0 =>
        throw new RuntimeException(s"Deterministic policy does not have an action for state, $state")
      case actions if actions.length >= 2 =>
        throw new RuntimeException(s"Deterministic policy has multiple actions for state, $state. The are $actions")
    }

  }

  override def updateProbability(stateAction: StateAction, probability: Double): DeterministicPolicy = {
    logger debug s"~stateAction=$stateAction, probability=$probability"
    new DeterministicPolicy(stateActionProbabilityMap + (stateAction -> probability))
  }

  override def updateAction(state: State, action: Action): DeterministicPolicy = {
    updateAction(StateAction(state, action))
  }

  def updateAction(stateAction: StateAction): DeterministicPolicy = {
    logger trace s"~stateAction=$stateAction"
    getStateActions(stateAction.state)
      .foldLeft(this)((policy, stateActionForState) => {
        if (stateActionForState.action === stateAction.action) {
          policy.updateProbability(stateActionForState, 1d)
        } else {
          policy.updateProbability(stateActionForState, 0d)
        }
      })

  }

}
