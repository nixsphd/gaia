package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment.{StateAction, Transition, TransitionReward}
import ie.nix.rl.policy._
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.{Action, Environment, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.annotation.tailrec

abstract class TDPolicyIterator[P <: Policy with Determinism, QF <: ActionValueFunction](
    val episodes: Vector[Episode],
    val initialActionValueFunction: Policy with Determinism => QF,
    val discount: Double)(implicit environment: Environment with Episodes)
    extends PolicyIterator[Policy with Determinism, QF]
    with Logging {

  override def iterate(policy: Policy with Determinism): (Policy with Determinism, QF) = {
    episodes.tail.foldLeft(evaluateAndImprovePolicy(policy, initialActionValueFunction(policy), episodes.head))(
      (policyAndActionValueFunction, episode) => {
        val targetPolicy = policyAndActionValueFunction._1
        val actionValueFunction = policyAndActionValueFunction._2
        val preparedActionValueFunction = prepareActionValueFunction(actionValueFunction)
        evaluateAndImprovePolicy(targetPolicy, preparedActionValueFunction, episode)
      })

  }

  @tailrec
  final def evaluateAndImprovePolicy(policy: Policy with Determinism,
                                     actionValueFunction: QF,
                                     episodeStep: StateAction): (Policy with Determinism, QF) = {

    //   Take action A, observe R, S′
    val TransitionReward(Transition(stateAction, nextState), reward) =
      environment.getTransitionRewardForStateAction(episodeStep)
    logger trace s"~stateAction=$stateAction nextState=$nextState, reward=$reward"

    if (stateAction.isTerminal || nextState.isTerminal) {
      val target =
        getTarget(reward, actionValueFunction, stateAction, None)
      val updatedActionValueFunction =
        updateActionValueFunction(actionValueFunction, stateAction, None, target)
      val updatedPolicy = improvePolicy(stateAction.state, policy, updatedActionValueFunction)
      logger trace s"~updatedPolicy=$updatedPolicy"
      logger trace s"~updatedActionValueFunction=$updatedActionValueFunction"

      (updatedPolicy, updatedActionValueFunction)

    } else {

      val nextStateAction = getNextStateAction(policy, nextState)
      val target =
        getTarget(reward, actionValueFunction, stateAction, Some(nextStateAction))
      logger trace s"~stateAction=$stateAction nextState=$nextState, nextEpisodeStep=$nextStateAction, reward=$reward, target=$target"

      val updatedActionValueFunction =
        updateActionValueFunction(actionValueFunction, stateAction, Some(nextStateAction), target)
      val updatedPolicy = improvePolicy(stateAction.state, policy, updatedActionValueFunction)
      logger trace s"~updatedPolicy=$updatedPolicy"
      logger trace s"~updatedActionValueFunction=$updatedActionValueFunction"

      evaluateAndImprovePolicy(updatedPolicy, updatedActionValueFunction, nextStateAction)
    }
  }

  def improvePolicy(state: State,
                    policy: Policy with Determinism,
                    updatedActionValueFunction: QF): Policy with Determinism = {
    val bestAction = getBestAction(state, updatedActionValueFunction)
    policy.updateAction(state, bestAction)

  }

  def getNextStateAction(policy: Policy with Determinism, nextState: State): StateAction = {
    StateAction(nextState, policy.getAction(nextState))
  }

  def getTarget(reward: Double,
                actionValueFunction: QF,
                stateAction: StateAction,
                maybeNextStateAction: Option[StateAction]): Double

  def getBestAction(state: State, actionActionValueFunction: QF): Action = {
    val (bestStateAction, _) = getStateActionsValues(actionActionValueFunction, state).maxBy(_._2)
    bestStateAction.action

  }

  def getStateActionsValues(actionValueFunction: QF, state: State): Vector[(StateAction, Double)] = {
    actionValueFunction.getStateActions
      .filter(stateAction => stateAction.state === state)
      .map(stateAction => (stateAction, actionValueFunction.getValue(stateAction)))

  }

  def getMaxStateActionValue(actionValueFunction: QF, maybeStateAction: Option[StateAction]): Double = {
    maybeStateAction match {
      case Some(stateAction) =>
        actionValueFunction.getStateActions
          .filter(_.state === stateAction.state)
          .map(stateAction => actionValueFunction.getValue(stateAction))
          .max
      case None => 0d
    }
  }

  def prepareActionValueFunction(actionValueFunction: QF): QF = actionValueFunction

  def updateActionValueFunction(actionActionValueFunction: QF,
                                stateAction: StateAction,
                                maybeNextStateAction: Option[StateAction],
                                target: Double): QF

}
