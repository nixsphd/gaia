package ie.nix.rl.tabular.examples.gambler

import ie.nix.rl.Environment.{Reward, StateAction, Transition}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.examples.gambler.Casino.{GamblersBet, GamblersCapitol}
import ie.nix.rl.tabular.{Environment, Model}
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

object Casino {

  case class GamblersCapitol(capitol: Int, override val isTerminal: Boolean = false)
      extends ie.nix.rl.State
      with Logging {
    override def toString: String = s"$capitol"
  }

  case class GamblersBet(bet: Int) extends Action {
    override def toString: String = s"$bet"
  }

  def apply(targetCapitol: Int, probabilityHeads: Double): Casino = {
    new Casino(stateActions(targetCapitol), terminalStates(targetCapitol), targetCapitol, probabilityHeads: Double)
  }

  private def terminalStates(targetCapitol: Int) = {
    Vector[State](GamblersCapitol(0, isTerminal = true), GamblersCapitol(targetCapitol, isTerminal = true)).toSet
  }

  private def stateActions(targetCapitol: Int) = {
    (for {
      capitol <- 1 until targetCapitol
      highestBet = math.min(capitol, targetCapitol - capitol)
      bet <- 1 to highestBet
    } yield {
      StateAction(GamblersCapitol(capitol), GamblersBet(bet))
    }).toSet
  }

}

class Casino private[gambler] (override val stateActionsSet: Set[StateAction],
                               override val terminalStateSet: Set[State],
                               val targetCapitol: Int,
                               val probabilityHeads: Double)
    extends Environment(stateActionsSet, terminalStateSet)
    with Model
    with Episodes
    with Logging {

  override def getTransitionForStateAction(stateAction: StateAction): Transition = {
    Transition(stateAction, getStateAfterToss(stateAction))
  }

  private def getStateAfterToss(stateAction: StateAction): State = {
    (stateAction, toss) match {
      case (StateAction(state: GamblersCapitol, action: GamblersBet), true) =>
        val newCapitol = state.capitol + action.bet
        if (newCapitol === 0 || newCapitol === targetCapitol) {
          GamblersCapitol(newCapitol, isTerminal = true)
        } else {
          GamblersCapitol(newCapitol)
        }

      case (StateAction(state: GamblersCapitol, action: GamblersBet), false) =>
        val newCapitol = state.capitol - action.bet
        if (newCapitol === 0 || newCapitol === targetCapitol) {
          GamblersCapitol(newCapitol, isTerminal = true)
        } else {
          GamblersCapitol(newCapitol)
        }

      case _ =>
        logger.error(s"Unexpected stateAction, $stateAction")
        stateAction.state
    }
  }

  private def toss: Boolean = Random.nextDouble() <= probabilityHeads

  override def getRewardForTransition(transition: Transition): Reward = {
    transition match {
      case Transition(_, GamblersCapitol(capitol, true)) if capitol === targetCapitol => 1d
      case Transition(_, GamblersCapitol(_, _))                                       => 0d
      case _ =>
        logger.error(s"Unexpected transition, $transition")
        0
    }
  }

  override def getProbabilityOfTransition(transition: Transition): Reward = transition match {
    case Transition(StateAction(_, action: GamblersBet), _) if action.bet === 0 =>
      1d
    case Transition(StateAction(state: GamblersCapitol, action: GamblersBet), finalState: GamblersCapitol)
        if state.capitol + action.bet === finalState.capitol =>
      probabilityHeads
    case Transition(StateAction(state: GamblersCapitol, action: GamblersBet), finalState: GamblersCapitol)
        if state.capitol - action.bet === finalState.capitol =>
      1d - probabilityHeads
    case _ => 0d
  }

  override def getInitialState(): State = {
    GamblersCapitol(1 + Random.nextInt(targetCapitol - 1))
  }

}
