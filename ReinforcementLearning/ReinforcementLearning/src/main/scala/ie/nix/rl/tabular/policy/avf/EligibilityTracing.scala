package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy
import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.avf.ActionValueFunction.getValueMap
import ie.nix.rl.tabular.policy.vf.AbstractEligibilityTracing
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object EligibilityTracing {

  val randomValue: () => (Double, Double) = () => (Random.nextDouble(), 0d)

  def apply(policy: Policy,
            discount: Double,
            stepSize: StepSize,
            eligibilityDecay: Double,
            initialValue: Double): EligibilityTracing = {
    new EligibilityTracing(getValueMap[(Double, Double)](policy)(() => (initialValue, 0d)),
                           discount,
                           stepSize,
                           eligibilityDecay)
  }

  def apply(policy: Policy, discount: Double, stepSize: StepSize, eligibilityDecay: Double): EligibilityTracing = {
    new EligibilityTracing(getValueMap[(Double, Double)](policy)(randomValue), discount, stepSize, eligibilityDecay)
  }

}

class EligibilityTracing private[avf] (stateValueMap: Map[StateAction, (Double, Double)],
                                       discount: Double,
                                       stepSize: StepSize,
                                       eligibilityDecay: Double)
    extends AbstractEligibilityTracing[StateAction](stateValueMap, discount, stepSize, eligibilityDecay)
    with policy.ActionValueFunction
    with Logging {

  override def getStateActions: Vector[StateAction] = valueMap.keySet.toVector

  override def updateValue(stateAction: StateAction, error: Double): EligibilityTracing = {
    incrementEligibilityForState(stateAction)
      .updateErrorForAllStates(error)
      .decayEligibilityForAllStates
  }

  def updateValueAndEligibilityTrace(stateAction: StateAction,
                                     value: Double,
                                     eligibilityTrace: Double): EligibilityTracing = {
    val newValue = (value, eligibilityTrace)
    new EligibilityTracing(stateValueMap + (stateAction -> newValue), discount, stepSize, eligibilityDecay)

  }

  def incrementEligibilityForState(stateAction: StateAction): EligibilityTracing = {
    val (value, _) = getValueAndEligibility(stateAction)
    // Z(S) ← Z(S) + 1
//    val newEligibilityOfState = eligibility + 1d
    // Z(S) ← 1
    val newEligibilityOfState = 1d
    updateValueAndEligibilityTrace(stateAction, value, newEligibilityOfState)
  }

  def updateErrorForAllStates(error: Double): EligibilityTracing = {
    getStatesOrStateActions.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, eligibility) = getValueAndEligibility(stateOrStateAction)
      // V(s) ← V(s) + αδZ(s)
      val newValue = value + (stepSize * error * eligibility)
//      if (eligibility != 0d && error != 0d)
//        logger info s"stateOrStateAction=$stateOrStateAction, value=$value ->  newValue=$newValue"
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, newValue, eligibility)
    })
  }

  def decayEligibilityForAllStates: EligibilityTracing = {
    getStatesOrStateActions.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, eligibility) = getValueAndEligibility(stateOrStateAction)
      // Z(s) ← γλZ(s)
      val newEligibility = discount * eligibilityDecay * eligibility
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, value, newEligibility)
    })
  }

  def clearEligibilityForAllStates(): EligibilityTracing = {
    getStatesOrStateActions.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, _) = getValueAndEligibility(stateOrStateAction)
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, value, 0d)
    })
  }

}
