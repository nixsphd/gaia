package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment.{StateAction, TransitionReward}
import ie.nix.rl.policy.{ActionValueFunction, Policy}
import ie.nix.rl.tabular.Selector.selectItem
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.avf
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator.{getVisitedStates, runEpisode}
import ie.nix.rl.tabular.policy.td.TemporalDifferenceEvaluatorQF.getStateActionValue
import ie.nix.rl.{Environment, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

object TemporalDifferenceEvaluatorQF {

  def apply[P <: Policy](discount: Double)(
      implicit environment: Environment with Episodes): TemporalDifferenceEvaluatorQF[P, avf.ConstantStepSize] = {

    new TemporalDifferenceEvaluatorQF[P, avf.ConstantStepSize](discount) {
      override def updateValueFunction(constantStepSize: avf.ConstantStepSize,
                                       stateAction: Episode,
                                       target: Double): avf.ConstantStepSize = {
        constantStepSize.updateValue(stateAction, target)
      }
    }
  }

  def getStateActionValue(actionValueFunction: ActionValueFunction, state: State): Double = {
    if (state.isTerminal) {
      0d
    } else {
      val finalStateAction = getStateAction(actionValueFunction, state)
      actionValueFunction.getValue(finalStateAction) // V(S')
    }
  }

  def getStateAction(actionValueFunction: ActionValueFunction, state: State): StateAction = {
    val possibleStateActionAndProbabilities = actionValueFunction.getStateActions
      .filter(_.state === state)
      .map(stateAction => (stateAction, actionValueFunction.getValue(stateAction)))
    val sumOfProbabilities = possibleStateActionAndProbabilities.map(_._2).sum
    val selectProbability = Random.nextDouble() * sumOfProbabilities
    val finalStateAction = selectItem[StateAction](possibleStateActionAndProbabilities, selectProbability)
    finalStateAction
  }

}

abstract class TemporalDifferenceEvaluatorQF[P <: Policy, QF <: ActionValueFunction](override val discount: Double)(
    implicit environment: Environment with Episodes)
    extends EpisodicEvaluator[P, QF](discount)
    with Logging {

  override def evaluatePolicy(policy: P, actionValueFunction: QF, episode: Episode): (QF, Vector[State]) = {
    val transitionRewards = runEpisode(episode, policy)
    val updatedValueFunction = updateValueFunction(actionValueFunction, transitionRewards)
    val visitedStates = getVisitedStates(episode, transitionRewards)

    (updatedValueFunction, visitedStates)

  }

  override def updateValueFunction(actionValueFunction: QF, transitionRewards: Vector[TransitionReward]): QF = {
    val updatedValueFunction =
      transitionRewards.foldLeft(actionValueFunction)((actionValueFunction, transitionReward) => {

        val stateAction = transitionReward.transition.stateAction
        val target = getTarget(actionValueFunction, transitionReward)
        updateValueFunction(actionValueFunction, stateAction, target)

      })
    updatedValueFunction

  }

  def getTarget(actionValueFunction: QF, transitionReward: TransitionReward): Double = {
    val state = transitionReward.transition.stateAction.state // S
    val finalState = transitionReward.transition.finalState // S'
    val finalStateActionValue = getStateActionValue(actionValueFunction, finalState)
    val reward = transitionReward.reward
    val target = reward + (discount * finalStateActionValue) // R + γV(S′)
    logger.trace(
      s"~state=$state, finalState=$finalState, finalStateActionValue=$finalStateActionValue, " +
        s"reward=$reward, target=$target")
    target

  }

  def getNextTransitionReward(policy: P, transitionReward: TransitionReward): TransitionReward = {
    val stateAction =
      StateAction(transitionReward.transition.finalState, policy.getAction(transitionReward.transition.finalState))
    getTransitionReward(stateAction)

  }

  def getTransitionReward(stateAction: StateAction): TransitionReward = {
    environment.getTransitionRewardForStateAction(stateAction)

  }
}
