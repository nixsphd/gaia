package ie.nix.rl.policy

trait PolicyIterator[P <: Policy, +VF] {

  def iterate(policy: P): (P, VF)

}
