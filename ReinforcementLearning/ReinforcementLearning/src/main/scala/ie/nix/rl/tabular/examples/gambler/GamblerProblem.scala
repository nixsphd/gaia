package ie.nix.rl.tabular.examples.gambler

import ie.nix.rl.tabular.{Environment, Model}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.examples.CSVFileWriterHelper.{
  writeActionValueFunction,
  writePolicyWithDeterminism,
  writeValueFunction
}
import ie.nix.rl.tabular.policy.dp.DynamicProgramming
import ie.nix.rl.tabular.policy.td.TemporalDifference.SARSA
import ie.nix.rl.tabular.policy.{DeterministicPolicy, SoftPolicy, avf}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object GamblerProblemWithDP extends App with Logging {

  Random.setSeed(0)

  val targetCapitol: Int = 15
  val probabilityHeads: Double = 0.4
  implicit val casino: Environment with Model = Casino(targetCapitol, probabilityHeads)

  val policy = DeterministicPolicy.apply

  val numberOfIterations = 1
  val discount: Double = 1.0
  val numberOfSweeps: Int = 10
  val dynamicProgramming = DynamicProgramming(numberOfIterations, numberOfSweeps, discount)

  val (improvedPolicy, valueFunction) = dynamicProgramming.iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~valueFunction=$valueFunction")

  writePolicyWithDeterminism(improvedPolicy, "GamblerProblemDP.csv")
  writeValueFunction(valueFunction, "GamblerProblemDPVF.csv")

}

object GamblerProblemWithSARSA extends App with Logging {

  Random.setSeed(0)

  val targetCapitol: Int = 16
  val probabilityHeads: Double = 0.9
  implicit val casino: Environment with Episodes = Casino(targetCapitol, probabilityHeads)
  val episodes = casino.getStarts(numberOfStarts = 1000)

  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = policy =>
    avf.ConstantStepSize(policy, initialValue = 0d, stepSize = 0.01)

  val sarsa = SARSA(episodes, initialValueFunction, discount = 1d)

  val (improvedPolicy, valueFunction) = sarsa.iterate(policy)
  logger.info(s"~improvedPolicy=$improvedPolicy")
  logger.info(s"~valueFunction=$valueFunction")

  writePolicyWithDeterminism(improvedPolicy, "GamblerProblemSARSA.csv")
  writeActionValueFunction(valueFunction, "GamblerProblemSARSAAVF.csv")

}
