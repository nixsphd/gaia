package ie.nix.rl.tabular.policy.mc

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.{ActionValueFunction, Determinism, EpisodicPolicyImprover, Policy}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.{Action, Environment, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

object GreedyEpisodicImprover {

  def apply[AVF <: ActionValueFunction](
      implicit environment: Environment with Episodes): GreedyEpisodicImprover[AVF] = {
    new GreedyEpisodicImprover
  }

}

class GreedyEpisodicImprover[AVF <: ActionValueFunction] private[GreedyEpisodicImprover] (
    implicit val environment: Environment with Episodes)
    extends EpisodicPolicyImprover[Policy with Determinism, AVF]
    with Logging {

  override def improvePolicy(policy: Policy with Determinism,
                             actionValueFunction: AVF,
                             statesToUpdate: Vector[State]): Policy with Determinism = {

    statesToUpdate.foldLeft(policy)((policy, state) => {
      val bestAction = getBestAction(state, actionValueFunction)
      val updatedPolicy = policy.updateAction(state, bestAction)
      updatedPolicy
    })

  }

  def getBestAction(state: State, actionValueFunction: AVF): Action = {

    val (bestStateAction, _) = getStateActionsValues(actionValueFunction, state).maxBy(_._2)
    bestStateAction.action

  }

  def getStateActionsValues(actionValueFunction: AVF, state: State): Vector[(StateAction, Double)] = {

    actionValueFunction.getStateActions
      .filter(stateAction => stateAction.state === state)
      .map(stateAction => (stateAction, actionValueFunction.getValue(stateAction)))

  }

}
