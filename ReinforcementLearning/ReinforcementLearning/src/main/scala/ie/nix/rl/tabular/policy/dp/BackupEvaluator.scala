package ie.nix.rl.tabular.policy.dp

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.{Policy, PolicyEvaluator}
import ie.nix.rl.tabular.Model
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.dp.DynamicProgramming.backupState
import ie.nix.rl.tabular.policy.vf.ValueFunction
import org.apache.logging.log4j.scala.Logging

object BackupEvaluator {

  def apply(discount: Double, numberOfSweeps: Int)(implicit environment: Environment with Model): BackupEvaluator = {
    new BackupEvaluator(discount, numberOfSweeps)
  }

}

class BackupEvaluator private[BackupEvaluator] (val discount: Double, val numberOfSweeps: Int)(
    implicit environment: Environment with Model)
    extends PolicyEvaluator[DeterministicPolicy, ie.nix.rl.policy.ValueFunction]
    with Logging {

  override def evaluatePolicy(policy: DeterministicPolicy): ValueFunction = {

    (1 to numberOfSweeps).foldLeft(ValueFunction(policy, initialValue = 0d))((valueFunction, _) =>
      backupAllState(policy, valueFunction))

  }

  def backupAllState(policy: Policy, valueFunction: ValueFunction): ValueFunction = {

    val newValueFunction = environment.getNonTerminalStates
      .foldLeft(ValueFunction(policy))((newValueFunction, state) => {
        val action = policy.getAction(state)
        val newStateValue = backupState(StateAction(state, action), discount, valueFunction)
        logger.info(s"~state=$state, newStateValue=$newStateValue")
        newValueFunction.updateValue(state, newStateValue)
      })
    logger.debug(s"~policy=$policy, valueFunction=$valueFunction, newValueFunction=$newValueFunction")
    newValueFunction

  }

}
