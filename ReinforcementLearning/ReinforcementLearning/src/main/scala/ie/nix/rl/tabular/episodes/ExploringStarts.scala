package ie.nix.rl.tabular.episodes

import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.episodes.Episodes.Episode

trait ExploringStarts {

  environment: Environment with Episodes =>

  override def getStarts(numberOfStarts: Int): Vector[Episode] = {

    val exploringStarts = getExploringStarts
    val starts = for {
      index <- 1 to numberOfStarts
      exploringStart = exploringStarts(index % exploringStarts.length)
    } yield exploringStart: Episode
    starts.toVector

  }

  def getExploringStarts: Vector[Episode] = {
    val exploringStarts = for {
      state <- getNonTerminalStates
      stateAction <- getStateActionsForState(state)
    } yield stateAction: Episode
    exploringStarts.toVector
  }

}
