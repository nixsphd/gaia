package ie.nix.rl.tabular.policy.mc

import ie.nix.rl.Environment.{Transition, TransitionReward}
import ie.nix.rl.policy.{Determinism, EpisodicPolicyEvaluator, Policy}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.avf.WeightedAveraging
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator.{getVisitedStates, runEpisode}
import ie.nix.rl.{Environment, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.annotation.tailrec

object OfflineEpisodicEvaluator {

  def apply[TP <: Policy with Determinism, BP <: Policy](initBehaviourPolicy: TP => BP, discount: Double)(
      implicit environment: Environment with Episodes): OfflineEpisodicEvaluator[TP, BP, WeightedAveraging] = {
    new OfflineEpisodicEvaluator[TP, BP, WeightedAveraging](initBehaviourPolicy, discount) {
      override def updateValueFunction(weightedAveraging: WeightedAveraging,
                                       stateAction: Episode,
                                       newTotalRewards: Double,
                                       newWeight: Double): WeightedAveraging = {
        weightedAveraging.updateValue(stateAction, newTotalRewards, newWeight)
      }
    }
  }

}

abstract class OfflineEpisodicEvaluator[TP <: Policy with Determinism, BP <: Policy, VF] private[OfflineEpisodicEvaluator] (
    val initBehaviourPolicy: TP => BP,
    discount: Double)(implicit environment: Environment with Episodes)
//    extends EpisodicEvaluator[BP, VF](discount)(environment)
    extends EpisodicPolicyEvaluator[TP, VF]
    with Logging {

  //  override def evaluatePolicy(target: BP,
  //                              actionValueFunction: VF,
  //                              episode: Episode): (VF, Vector[State]) = actionValueFunction match {
  //    case weightedAveraging: WeightedAveraging => evaluatePolicy(target, weightedAveraging, episode)
  //    case _ =>
  //      logger error "~OfflineEpisodicEvaluator, call evaluatePolicy with a WeightedAveraging actionValueFunction"
  //      (actionValueFunction, Vector[State]())
  //  }

  def evaluatePolicy(targetPolicy: TP, actionValueFunction: VF, episode: Episode): (VF, Vector[State]) = {

    val behaviourPolicy = initBehaviourPolicy(targetPolicy)
    logger.trace(s"~targetPolicy=$targetPolicy")
    logger.trace(s"~behaviourPolicy=$behaviourPolicy")

    val transitionRewards = runEpisode(episode, behaviourPolicy)
    logger trace s"~transitionRewards=$transitionRewards"
    val updatedActionValueFunction =
      updateActionValueFunction(targetPolicy, behaviourPolicy, actionValueFunction, transitionRewards)
    val visitedStates = getVisitedStates(episode, transitionRewards)

    (updatedActionValueFunction, visitedStates)

  }

  def updateActionValueFunction(targetPolicy: TP,
                                behaviourPolicy: BP,
                                actionValueFunction: VF,
                                transitionRewards: Vector[TransitionReward]): VF = {

    @tailrec
    def helper(transitionRewards: Vector[TransitionReward],
               actionValueFunction: VF,
               totalRewards: Double,
               weight: Double): VF = transitionRewards match {
      case transitionRewards if transitionRewards.isEmpty => actionValueFunction
      case TransitionReward(Transition(stateAction, _), reward) +: remainingTransitionRewards =>
        val newTotalRewards = getNewTotalRewards(totalRewards, reward)
        val newWeight = getNewWeight(behaviourPolicy, stateAction, weight)
        val newActionValueFunction = updateValueFunction(actionValueFunction, stateAction, newTotalRewards, newWeight)
        //        val newActionValueFunction = actionValueFunction.updateValue(stateAction, newTotalRewards, newWeight)
        logger.trace(
          s"~stateAction=$stateAction, totalRewards=$totalRewards, reward=$reward, " +
            s"newTotalRewards=$newTotalRewards")
        logger.trace(s"~stateAction=$stateAction, weight=$weight, newWeight=$newWeight")

        if (stateAction.action !== targetPolicy.getAction(stateAction.state)) {
          // If At != (St) then ExitForLoop
          logger.trace(
            s"~exiting at stateAction=$stateAction, behaviour action=${stateAction.action} != " +
              s"targetPolicy action=${targetPolicy.getAction(stateAction.state)}")
          newActionValueFunction
        } else {
          helper(remainingTransitionRewards, newActionValueFunction, newTotalRewards, newWeight)
        }
    }

    logger.trace(s"~transitionRewards=$transitionRewards")
    helper(transitionRewards.reverse, actionValueFunction, totalRewards = 0d, weight = 1d)

  }

  //  override def updateValueFunction(actionValueFunction: VF, stateAction: Episode, newTotalRewards: Double): VF = {
  //    updateValueFunction(actionValueFunction, stateAction, newTotalRewards, newWeight = 1d)
  //  }

  def updateValueFunction(actionValueFunction: VF, stateAction: Episode, newTotalRewards: Double, newWeight: Double): VF

  def getNewTotalRewards(totalRewards: Double, reward: Double): Double =
    (discount * totalRewards) + reward

  def getNewWeight(behaviourPolicy: BP, stateAction: Episode, weight: Double): Double =
    weight * (1d / behaviourPolicy.getProbability(stateAction))

}
