package ie.nix.rl.tabular.policy.et

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.{Determinism, Policy}
import ie.nix.rl.tabular.episodes
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.avf.EligibilityTracing
import ie.nix.rl.tabular.policy.mc.IdleEpisodicImprover
import ie.nix.rl.tabular.policy.td.TDPolicyIterator
import ie.nix.rl.tabular.policy.{EpisodicPolicyIterator, avf, vf}
import org.scalactic.TypeCheckedTripleEquals._

object EligibilityTraces {

  def EligibilityTracesEvaluator[P <: Policy](discount: Double)(
      implicit environment: Environment with Episodes): EligibilityTracesEvaluator[P] =
    new EligibilityTracesEvaluator[P](discount)

  def EligibilityTracesEvaluatorQF[P <: Policy](discount: Double)(
      implicit environment: Environment with Episodes): EligibilityTracesEvaluatorQF[P] =
    new EligibilityTracesEvaluatorQF[P](discount)

  // Prediction only
  def EligibilityTracesPredictor[P <: Policy](episodes: Vector[Episode],
                                              initialValueFunction: Policy => vf.EligibilityTracing)(
      implicit environment: Environment with Episodes,
      evaluator: EligibilityTracesEvaluator[P]): EpisodicPolicyIterator[P, vf.EligibilityTracing] = {

    implicit val improver: IdleEpisodicImprover[P, vf.EligibilityTracing] =
      new IdleEpisodicImprover[P, vf.EligibilityTracing]

    new EpisodicPolicyIterator[P, vf.EligibilityTracing](episodes, initialValueFunction)(evaluator, improver)

  }

  def EligibilityTracesPredictorQF[P <: Policy](episodes: Vector[Episode],
                                                initialValueFunction: Policy => avf.EligibilityTracing)(
      implicit environment: Environment with Episodes,
      evaluator: EligibilityTracesEvaluatorQF[P]): EpisodicPolicyIterator[P, avf.EligibilityTracing] = {

    implicit val improver: IdleEpisodicImprover[P, avf.EligibilityTracing] =
      new IdleEpisodicImprover[P, avf.EligibilityTracing]

    new EpisodicPolicyIterator[P, avf.EligibilityTracing](episodes, initialValueFunction)(evaluator, improver)

  }

  // SARSA Lambda
  def SARSALambda(episodes: Vector[Episode],
                  initialActionValueFunction: Policy with Determinism => avf.EligibilityTracing,
                  discount: Double)(implicit environment: Environment with Episodes)
    : TDPolicyIterator[Policy with Determinism, avf.EligibilityTracing] = {

    new TDPolicyIterator[Policy with Determinism, avf.EligibilityTracing](episodes,
                                                                          initialActionValueFunction,
                                                                          discount) {

      override def prepareActionValueFunction(eligibilityTraces: avf.EligibilityTracing): avf.EligibilityTracing =
        eligibilityTraces.clearEligibilityForAllStates()

      override def updateActionValueFunction(eligibilityTraces: avf.EligibilityTracing,
                                             episodeStep: StateAction,
                                             maybeNextStateAction: Option[StateAction],
                                             error: Double): avf.EligibilityTracing =
        eligibilityTraces.updateValue(episodeStep, error)

      override def getTarget(reward: Double,
                             eligibilityTraces: avf.EligibilityTracing,
                             stateAction: StateAction,
                             maybeNextStateAction: Option[StateAction]): Double = {
        val stateActionValue = eligibilityTraces.getValue(stateAction)
        val nextStateActionValue = getStateActionValue(eligibilityTraces, maybeNextStateAction)
        // δ ← R + γV(S′) − V(S)
        val error = reward + (discount * nextStateActionValue) - stateActionValue
//        if (reward > 0)
//          logger info s"error=$error = reward=$reward + (discount=$discount * nextStateActionValue=$nextStateActionValue) - stateActionValue=$stateActionValue"
        error
      }
    }
  }

  def getStateActionValue(eligibilityTraces: EligibilityTracing,
                          maybeNextStateAction: Option[episodes.Episodes.Episode]): Double = {
    maybeNextStateAction match {
      case Some(nextStateAction) => eligibilityTraces.getValue(nextStateAction)
      case None                  => 0d
    }
  }

  // Q-Learning Lambda Watkins
  def QLearningLambdaWatkins(episodes: Vector[Episode],
                             initialActionValueFunction: Policy with Determinism => avf.EligibilityTracing,
                             discount: Double)(implicit environment: Environment with Episodes)
    : TDPolicyIterator[Policy with Determinism, avf.EligibilityTracing] = {

    new TDPolicyIterator[Policy with Determinism, avf.EligibilityTracing](episodes,
                                                                          initialActionValueFunction,
                                                                          discount) {

      override def prepareActionValueFunction(eligibilityTraces: avf.EligibilityTracing): avf.EligibilityTracing =
        eligibilityTraces.clearEligibilityForAllStates()

      override def updateActionValueFunction(eligibilityTraces: avf.EligibilityTracing,
                                             episodeStep: StateAction,
                                             maybeNextStateAction: Option[StateAction],
                                             error: Double): avf.EligibilityTracing = {

        val updatedEligibilityTraces = eligibilityTraces.updateValue(episodeStep, error)

        val nextStateActionValue = getStateActionValue(eligibilityTraces, maybeNextStateAction)
        val maxNextStateActionValue = getMaxStateActionValue(eligibilityTraces, maybeNextStateAction)
        // If A′ = A∗
        if (nextStateActionValue === maxNextStateActionValue) {
          // then Z(s,a) ← γλZ(s,a)
          updatedEligibilityTraces
        } else {
          logger trace s"nextStateActionValue=$nextStateActionValue != maxNextStateActionValue=$maxNextStateActionValue, clearEligibilityForAllStates"
          // else Z(s,a) ← 0
          eligibilityTraces.clearEligibilityForAllStates()
        }
      }

      override def getTarget(reward: Double,
                             eligibilityTraces: avf.EligibilityTracing,
                             stateAction: StateAction,
                             maybeNextStateAction: Option[StateAction]): Double = {
        val stateActionValue = eligibilityTraces.getValue(stateAction)
        // A∗ ← arg maxa Q(S′, a) (if A′ ties for the max, then A∗ ← A′)
        val maxStateActionValue = getMaxStateActionValue(eligibilityTraces, maybeNextStateAction)
        // δ ← R + γV(S′) − V(S)
        val error = reward + (discount * maxStateActionValue) - stateActionValue
        error
      }

    }
  }

}
