package ie.nix.rl.tabular.episodes

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.State
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.episodes.Episodes.Episode

import scala.util.Random

object Episodes {

  type Episode = StateAction
}

trait Episodes {

  environment: Environment =>

  def getStarts(numberOfStarts: Int): Vector[Episode] = {
    (1 to numberOfStarts)
      .map(_ => {
        val state = getInitialState()
        val stateActionsForState = getStateActionsForState(state)
        val stateAction = stateActionsForState(Random.nextInt(stateActionsForState.length))
        stateAction: Episode
      })
      .toVector
  }

  def getInitialState(): State

}
