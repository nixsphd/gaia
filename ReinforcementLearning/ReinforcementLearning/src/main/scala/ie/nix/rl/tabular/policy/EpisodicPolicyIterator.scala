package ie.nix.rl.tabular.policy

import ie.nix.rl.policy._
import ie.nix.rl.tabular.episodes.Episodes.Episode
import org.apache.logging.log4j.scala.Logging

class EpisodicPolicyIterator[P <: ie.nix.rl.policy.Policy, VF](val episodes: Vector[Episode],
                                                               val initialValueFunction: P => VF)(
    implicit val evaluator: EpisodicPolicyEvaluator[P, VF],
    implicit val improver: EpisodicPolicyImprover[P, VF])
    extends ie.nix.rl.policy.PolicyIterator[P, VF]
    with Logging {

  override def iterate(policy: P): (P, VF) = {

    episodes.tail.foldLeft(evaluateAndImprovePolicy(policy, initialValueFunction(policy), episodes.head))(
      (policyAndValueFunction, episode) => {
        val targetPolicy = policyAndValueFunction._1
        val valueFunction = policyAndValueFunction._2
        evaluateAndImprovePolicy(targetPolicy, valueFunction, episode)
      })

  }

  def evaluateAndImprovePolicy(policy: P, valueFunction: VF, episode: Episode): (P, VF) = {

    // Evaluate the policy
    val (updatedValueFunction, statesVisited) =
      evaluator.evaluatePolicy(policy, valueFunction, episode)
    logger trace s"~updatedValueFunction=$updatedValueFunction, statesVisited=$statesVisited"
    // Improve the policy
    (improver.improvePolicy(policy, updatedValueFunction, statesVisited), updatedValueFunction)

  }

}
