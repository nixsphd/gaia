package ie.nix.rl.tabular

import org.apache.logging.log4j.scala.Logging

import scala.annotation.tailrec

object Selector extends Logging {

  @tailrec
  def selectItem[I](itemProbabilities: Seq[(I, Double)],
                    selectProbability: Double,
                    accumulativeProbability: Double = 0d): I = itemProbabilities match {
    case (item, _) +: Nil                                                                      => item
    case (item, probability) +: _ if accumulativeProbability + probability > selectProbability => item
    case (_, probability) +: tail =>
      selectItem(tail, selectProbability, accumulativeProbability + probability)
  }

}
