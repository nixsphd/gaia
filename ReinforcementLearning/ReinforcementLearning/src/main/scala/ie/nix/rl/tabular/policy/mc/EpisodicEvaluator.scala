package ie.nix.rl.tabular.policy.mc

import ie.nix.rl.Environment.{StateAction, TransitionReward}
import ie.nix.rl.policy.{EpisodicPolicyEvaluator, Policy}
import ie.nix.rl.tabular.episodes
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.avf.SampleAveraging
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator.{getVisitedStates, runEpisode}
import ie.nix.rl.tabular.policy.vf
import ie.nix.rl.{Environment, State}

import scala.annotation.tailrec

object EpisodicEvaluator {

  def apply[P <: Policy](discount: Double)(
      implicit environment: Environment with Episodes): EpisodicEvaluator[P, SampleAveraging] = {
    new EpisodicEvaluator[P, SampleAveraging](discount) {
      override def updateValueFunction(sampleAveraging: SampleAveraging,
                                       stateAction: Episode,
                                       newTotalRewards: Double): SampleAveraging = {
        sampleAveraging.updateValue(stateAction, newTotalRewards)
      }
    }
  }

  def ConstantStepSizeEpisodicEvaluator[P <: Policy](discount: Double)(
      implicit environment: Environment with Episodes): EpisodicEvaluator[P, vf.ConstantStepSize] = {
    new EpisodicEvaluator[P, vf.ConstantStepSize](discount) {
      override def updateValueFunction(constantStepSize: vf.ConstantStepSize,
                                       stateAction: Episode,
                                       newTotalRewards: Double): vf.ConstantStepSize = {
        constantStepSize.updateValue(stateAction.state, newTotalRewards)
      }

    }
  }

  def runEpisode[P <: Policy](episode: Episode, policy: P)(
      implicit environment: Environment with Episodes): Vector[TransitionReward] = {

    @tailrec
    def runEpisodeHelper(transitionRewards: Vector[TransitionReward]): Vector[TransitionReward] = {
      val lastTransitionReward = transitionRewards.last
      if (lastTransitionReward.transition.isTerminal) {
        transitionRewards
      } else {
        val nextState = lastTransitionReward.transition.finalState
        val nextStateAction = StateAction(nextState, policy.getAction(nextState))
        val nextTransitionReward = environment.getTransitionRewardForStateAction(nextStateAction)
        runEpisodeHelper(transitionRewards :+ nextTransitionReward)
      }
    }

    val transition = environment.getTransitionForStateAction(episode)
    val transitionRewards = Vector(TransitionReward(transition, environment.getRewardForTransition(transition)))
    runEpisodeHelper(transitionRewards)

  }

  def getVisitedStates(episode: Episode, transitionRewards: Vector[TransitionReward]): Vector[State] = {
    episode.state +: transitionRewards
      .map(transitionRewards => transitionRewards.transition.finalState)
      .filter(state => !state.isTerminal)
  }

}

abstract class EpisodicEvaluator[P <: Policy, VF] protected[EpisodicEvaluator] (val discount: Double)(
    implicit environment: Environment with Episodes)
    extends EpisodicPolicyEvaluator[P, VF] {

  override def evaluatePolicy(policy: P, actionValueFunction: VF, episode: Episode): (VF, Vector[State]) = {

    val transitionRewards = runEpisode(episode, policy)
    val updatedActionValueFunction = updateValueFunction(actionValueFunction, transitionRewards)
    val visitedStates = getVisitedStates(episode, transitionRewards)

    (updatedActionValueFunction, visitedStates)

  }

  def updateValueFunction(valueFunction: VF, transitionRewards: Vector[TransitionReward]): VF = {

    val (updatedValueFunction, _) =
      transitionRewards.foldRight((valueFunction, 0d))((transitionReward, valueFunctionAndTotalRewards) => {

        val valueFunction = valueFunctionAndTotalRewards._1
        val stateAction = transitionReward.transition.stateAction
        val totalRewards = valueFunctionAndTotalRewards._2
        val target = getTarget(transitionReward, totalRewards)
        val newValueFunction = updateValueFunction(valueFunction, stateAction, target)

        (newValueFunction, target)

      })
    updatedValueFunction

  }

  def getTarget(transitionReward: TransitionReward, totalRewards: Double): Double = {
    val reward = transitionReward.reward
    (discount * totalRewards) + reward
  }

  def updateValueFunction(actionValueFunction: VF, stateAction: episodes.Episodes.Episode, target: Double): VF

}
