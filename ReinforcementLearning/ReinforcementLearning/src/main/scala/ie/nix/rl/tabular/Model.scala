package ie.nix.rl.tabular

import ie.nix.rl.Environment.Transition

trait Model {

  environment: Environment =>

  def getProbabilityOfTransition(transition: Transition): Double

}
