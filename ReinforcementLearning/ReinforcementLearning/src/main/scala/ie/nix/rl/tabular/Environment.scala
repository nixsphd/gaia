package ie.nix.rl.tabular

import ie.nix.rl.Environment.{StateAction, TransitionReward}
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

abstract class EnvironmentView(val stateActionsSet: Set[StateAction], val terminalStateSet: Set[State])
    extends ie.nix.rl.EnvironmentView
    with Logging {

  override def getStates: Vector[State] = {
    (stateActionsSet.map(_.state) ++ terminalStateSet).toVector
  }

  override def getTerminalStates: Vector[State] = {
    terminalStateSet.toVector
  }

  override def getNonTerminalStates: Vector[State] = {
    stateActionsSet.map(_.state).toVector
  }

  override def getStateActions: Vector[StateAction] = {
    stateActionsSet.toVector
  }

  override def getActionsForState(state: State): Vector[Action] = {
    stateActionsSet.filter(_.state === state).map(_.action).toVector
  }

  override def getStateActionsForState(state: State): Vector[StateAction] = {
    stateActionsSet.filter(_.state === state).toVector
  }
}

abstract class Environment(override val stateActionsSet: Set[StateAction], override val terminalStateSet: Set[State])
    extends EnvironmentView(stateActionsSet, terminalStateSet)
    with ie.nix.rl.Environment
    with Logging {

  override def getTransitionRewardForStateAction(stateAction: StateAction): TransitionReward = {
    val transition = getTransitionForStateAction(stateAction)
    val reward = getRewardForTransition(transition)
    TransitionReward(transition, reward)
  }

}
