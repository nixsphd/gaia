package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import ie.nix.rl.tabular.policy.vf.ValueFunction.getValueMap
import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging

object QDLambda {

  def apply(policy: Policy,
            discount: Double,
            innovationStepSize: StepSize,
            consensusStepSize: StepSize,
            eligibilityDecay: Double,
            initialValue: Double): QDLambda = {
    new QDLambda(getValueMap[(Double, Double)](policy)(() => (initialValue, 0d)),
                 discount,
                 innovationStepSize,
                 consensusStepSize,
                 eligibilityDecay)
  }

  def apply(policy: Policy,
            discount: Double,
            innovationStepSize: StepSize,
            consensusStepSize: StepSize,
            eligibilityDecay: Double): QDLambda = {
    apply(policy, discount, innovationStepSize, consensusStepSize, eligibilityDecay, 0d)
  }

}

class QDLambda private[vf] (stateValueMap: Map[State, (Double, Double)],
                            discount: Double,
                            innovationStepSize: StepSize,
                            consensusStepSize: StepSize,
                            eligibilityDecay: Double)
    extends AbstractQDLambda[State](stateValueMap, discount, innovationStepSize, consensusStepSize, eligibilityDecay)
    with policy.ValueFunction
    with Logging {

  override def getStates: Vector[State] = valueMap.keySet.toVector

  override def updateValue(state: State, value: Double): QDLambda = {
    logger warn s"Can not call updateValue with single value $value for AbstractQDLambda, " +
      s"expect two terms, innovation and consensus"
    updateValue(state: State, value, 0d)
  }

  def updateValue(state: State, innovation: Double, consensus: Double): QDLambda = {
    incrementEligibilityForState(state)
      .updateValueForAllStates(innovation, consensus)
      .decayEligibilityForAllStates
  }

  override def updateValueAndEligibilityTrace(state: State, value: Double, eligibilityTrace: Double): QDLambda = {
    val newValue = (value, eligibilityTrace)
    new QDLambda(stateValueMap + (state -> newValue), discount, innovationStepSize, consensusStepSize, eligibilityDecay)
  }

  def incrementEligibilityForState(state: State): QDLambda = {
    val (value, eligibility) = getValueAndEligibility(state)
    // Z(S) ← Z(S) + 1
    val newEligibilityOfState = eligibility + 1d
    updateValueAndEligibilityTrace(state, value, newEligibilityOfState)
  }

  def updateValueForAllStates(innovation: Double, consensus: Double): QDLambda = {
    getStates.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, eligibility) = getValueAndEligibility(stateOrStateAction)
      // V(s) ← V(s) + α*innovation*Z(s) - b*consensus*Z(s)
      val newValue = value + (innovationStepSize * innovation * eligibility) - (consensusStepSize * consensus * eligibility)
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, newValue, eligibility)
    })
  }

  def decayEligibilityForAllStates(): QDLambda = {
    getStatesOrStateActions.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, eligibility) = getValueAndEligibility(stateOrStateAction)
      // Z(s) ← γλZ(s)
      val newEligibility = discount * eligibilityDecay * eligibility
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, value, newEligibility)
    })
  }

  def clearEligibilityForAllStates(): QDLambda = {
    getStatesOrStateActions.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, _) = getValueAndEligibility(stateOrStateAction)
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, value, 0d)
    })
  }

}

abstract class AbstractQDLambda[S] private[policy] (valueMap: Map[S, (Double, Double)],
                                                    val discount: Double,
                                                    val innovationStepSize: StepSize,
                                                    val consensusStepSize: StepSize,
                                                    val eligibilityDecay: Double)
    extends AbstractValueFunction[S, (Double, Double)](valueMap)
    with Logging {

  override def mapValue(value: (Double, Double)): Double = value._1

  def getValueAndEligibility(stateOrStateAction: S): (Double, Double) = {
    valueMap.getOrElse(stateOrStateAction, (0d, 0d))
  }

  def updateValueAndEligibilityTrace(stateOrStateAction: S,
                                     value: Double,
                                     eligibilityTrace: Double): AbstractQDLambda[S]

}
