package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.{Determinism, Policy}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy._
import ie.nix.rl.tabular.policy.avf.ConstantStepSize
import ie.nix.rl.tabular.policy.mc.IdleEpisodicImprover

object TemporalDifference {

  // Prediction only
  def TemporalDifferencePredictor[P <: Policy](episodes: Vector[Episode],
                                               initialValueFunction: Policy => vf.ConstantStepSize)(
      implicit environment: Environment with Episodes,
      evaluator: TemporalDifferenceEvaluator[P, vf.ConstantStepSize])
    : EpisodicPolicyIterator[P, vf.ConstantStepSize] = {

    implicit val improver: IdleEpisodicImprover[P, vf.ConstantStepSize] = new mc.IdleEpisodicImprover

    new EpisodicPolicyIterator[P, vf.ConstantStepSize](episodes, initialValueFunction)

  }

  def TemporalDifferencePredictor[P <: Policy](episodes: Vector[Episode],
                                               initialValueFunction: Policy => avf.ConstantStepSize)(
      implicit environment: Environment with Episodes,
      evaluator: TemporalDifferenceEvaluatorQF[P, avf.ConstantStepSize])
    : EpisodicPolicyIterator[P, avf.ConstantStepSize] = {

    implicit val improver: IdleEpisodicImprover[P, avf.ConstantStepSize] = new mc.IdleEpisodicImprover

    new EpisodicPolicyIterator[P, avf.ConstantStepSize](episodes, initialValueFunction)

  }

  // Prediction only, using Batches
  def TemporalDifferenceBatchPredictor[P <: Policy](episodes: Vector[Episode],
                                                    initialValueFunction: Policy => vf.Batch)(
      implicit environment: Environment with Episodes,
      evaluator: TemporalDifferenceEvaluator[P, vf.Batch]): EpisodicPolicyIterator[P, vf.Batch] = {

    implicit val improver: IdleEpisodicImprover[P, vf.Batch] = new mc.IdleEpisodicImprover

    new BatchedEpisodicPolicyIterator[P](episodes, initialValueFunction)

  }

  // SARSA
  def SARSA(episodes: Vector[Episode],
            initialActionValueFunction: Policy with Determinism => avf.ConstantStepSize,
            discount: Double)(implicit environment: Environment with Episodes)
    : TDPolicyIterator[Policy with Determinism, avf.ConstantStepSize] = {

    new TDPolicyIterator[Policy with Determinism, avf.ConstantStepSize](episodes, initialActionValueFunction, discount) {

      override def updateActionValueFunction(actionActionValueFunction: ConstantStepSize,
                                             stateAction: StateAction,
                                             maybeNextStateAction: Option[StateAction],
                                             target: Double): ConstantStepSize = {
        actionActionValueFunction.updateValue(stateAction, target)
      }

      override def getTarget(reward: Double,
                             actionValueFunction: ConstantStepSize,
                             stateAction: StateAction,
                             maybeNextStateAction: Option[StateAction]): Double = {
        // Q-Learning - Q(S,A) ← Q(S,A) + α[R + γ max a Q(S′,a) − Q(S,A)]
        val nextStateActionValue = maybeNextStateAction match {
          case Some(nextStateAction) => actionValueFunction.getValue(nextStateAction)
          case None                  => 0d
        }
        //   SARSA - Q(S, A) ← Q(S,A) + α[R + γQ(S′,A′) − Q(S, A)]
        reward + (discount * nextStateActionValue) // R + γQ(S′,A′)
      }

    }
  }

  // Q-Learning
  def QLearning(episodes: Vector[Episode],
                initialActionValueFunction: Policy with Determinism => avf.ConstantStepSize,
                discount: Double)(implicit environment: Environment with Episodes)
    : TDPolicyIterator[Policy with Determinism, avf.ConstantStepSize] = {

    new TDPolicyIterator[Policy with Determinism, avf.ConstantStepSize](episodes, initialActionValueFunction, discount) {

      override def updateActionValueFunction(actionActionValueFunction: ConstantStepSize,
                                             stateAction: StateAction,
                                             maybeNextStateAction: Option[StateAction],
                                             target: Double): ConstantStepSize = {
        actionActionValueFunction.updateValue(stateAction, target)
      }

      override def getTarget(reward: Double,
                             actionValueFunction: ConstantStepSize,
                             stateAction: StateAction,
                             maybeNextStateAction: Option[StateAction]): Double = {
        // Q-Learning - Q(S,A) ← Q(S,A) + α[R + γ max a Q(S′,a) − Q(S,A)]
        val maxStateActionValue = getMaxStateActionValue(actionValueFunction, maybeNextStateAction)
        reward + (discount * maxStateActionValue) // R + γ max a Q(S′,a)
      }

    }
  }
}
