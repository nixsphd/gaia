package ie.nix.rl.tabular.examples.randomwalk

import ie.nix.rl.tabular.episodes.{Episodes, ExploringStarts}
import ie.nix.rl.tabular.examples.randomwalk.RandomWalk.rootMeanSquaredError
import ie.nix.rl.tabular.examples.randomwalk.Walker.Righty
import ie.nix.rl.tabular.policy.PolicyIterator.policyDifference
import ie.nix.rl.tabular.policy.et.EligibilityTraces._
import ie.nix.rl.tabular.policy.et.{EligibilityTracesEvaluator, EligibilityTracesEvaluatorQF}
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator
import ie.nix.rl.tabular.policy.mc.EpisodicEvaluator.ConstantStepSizeEpisodicEvaluator
import ie.nix.rl.tabular.policy.mc.MonteCarlo.MonteCarloPredictor
import ie.nix.rl.tabular.policy.td.TemporalDifference.{
  QLearning,
  SARSA,
  TemporalDifferenceBatchPredictor,
  TemporalDifferencePredictor
}
import ie.nix.rl.tabular.policy.td.TemporalDifferenceEvaluator.BatchTemporalDifferenceEvaluator
import ie.nix.rl.tabular.policy.td.{TemporalDifferenceEvaluator, TemporalDifferenceEvaluatorQF}
import ie.nix.rl.tabular.policy.{Policy, SoftPolicy, avf, vf}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object RandomWalkProblemWithMCConstantStepSize extends App with Logging {

  Random.setSeed(0)

  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 6) with Episodes
  implicit val evaluator: EpisodicEvaluator[Policy, vf.ConstantStepSize] = ConstantStepSizeEpisodicEvaluator(
    discount = 1d)
  val episodes = randomWalk.getStarts(numberOfStarts = 200)

  val policy = Policy(randomWalk)
  val initialValueFunction: ie.nix.rl.policy.Policy => vf.ConstantStepSize = policy =>
    vf.ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.01)

  val mcPredictor = MonteCarloPredictor(episodes, initialValueFunction)

  val (updatedPolicy, valueFunction) = mcPredictor.iterate(policy)
  logger.info(s"~valueFunction=$valueFunction")

  val error = rootMeanSquaredError(randomWalk, valueFunction)
  logger.info(s"~rootMeanSquaredError=$error ~= 0.1")

}

object RandomWalkProblemWithTD extends App with Logging {

  Random.setSeed(0)

  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 6) with Episodes
  implicit val evaluator: TemporalDifferenceEvaluator[Policy, vf.ConstantStepSize] = TemporalDifferenceEvaluator(
    discount = 1d)
  val episodes = randomWalk.getStarts(numberOfStarts = 200)

  val policy = Policy(randomWalk)
  val initialValueFunction: ie.nix.rl.policy.Policy => vf.ConstantStepSize = policy =>
    vf.ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.1)

  val tdPredictor = TemporalDifferencePredictor(episodes, initialValueFunction)

  val (updatedPolicy, updatedValueFunction) = tdPredictor.iterate(policy)
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  val error = rootMeanSquaredError(randomWalk, updatedValueFunction)
  logger.info(s"~rootMeanSquaredError=$error ~= 0.1")

}

object RandomWalkProblemWithBatchTD extends App with Logging {

  Random.setSeed(0)

  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 6) with ExploringStarts
  implicit val evaluator: TemporalDifferenceEvaluator[Policy, vf.Batch] =
    BatchTemporalDifferenceEvaluator[Policy](discount = 1d)
  val episodes = randomWalk.getStarts(numberOfStarts = 25)

  val policy = Policy(randomWalk)
  val initialValueFunction: ie.nix.rl.policy.Policy => vf.Batch = policy =>
    vf.Batch(vf.ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.02))

  val batchPredictor = TemporalDifferenceBatchPredictor(episodes, initialValueFunction)

  val (updatedPolicy, updatedValueFunction) = batchPredictor.iterate(policy)
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  val error = rootMeanSquaredError(randomWalk, updatedValueFunction)
  logger.info(s"~rootMeanSquaredError=$error ~= 0.04")

}

object RandomWalkProblemWithTDQF extends App with Logging {

  Random.setSeed(0)

  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 6) with Episodes
  implicit val evaluator: TemporalDifferenceEvaluatorQF[Policy, avf.ConstantStepSize] =
    TemporalDifferenceEvaluatorQF[Policy](discount = 1d)
  val episodes = randomWalk.getStarts(numberOfStarts = 100)

  val policy = Policy(randomWalk)
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = policy =>
    avf.ConstantStepSize(policy, initialValue = 0.5, stepSize = 0.1)

  val tdPredictor = TemporalDifferencePredictor[Policy](episodes, initialValueFunction)

  val (updatedPolicy, updatedValueFunction) = tdPredictor.iterate(policy)
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  val error = rootMeanSquaredError(randomWalk, updatedValueFunction)
  logger.info(s"~rootMeanSquaredError=$error ~= 0.1")

}

object RandomWalkProblemWithSARSA extends App with Logging {

  Random.setSeed(0)

  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 6) with ExploringStarts
  val episodes = randomWalk.getStarts(numberOfStarts = 20)

  val initialValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = policy =>
    avf.ConstantStepSize(policy, initialValue = 0d, stepSize = 0.01)
  val sarsa = SARSA(episodes, initialValueFunction, discount = 1d)

  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)
  val (updatedPolicy, updatedValueFunction) = sarsa.iterate(policy)
  logger.info(s"~updatedPolicy=$updatedPolicy")
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  val difference = policyDifference(updatedPolicy, Righty(randomWalk))
  logger.info(s"~difference=$difference ~= 0.1 due to Softness")

}

object RandomWalkProblemWithQLearning extends App with Logging {

  Random.setSeed(0)

  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 6) with ExploringStarts
  implicit val evaluator: TemporalDifferenceEvaluatorQF[Policy, avf.ConstantStepSize] =
    TemporalDifferenceEvaluatorQF[Policy](discount = 1d)
  val episodes = randomWalk.getStarts(numberOfStarts = 1000)

  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.ConstantStepSize = policy =>
    avf.ConstantStepSize(policy, initialValue = 0d, stepSize = 0.01)

  val qLearning = QLearning(episodes, initialValueFunction, discount = 1d)

  val (updatedPolicy, updatedValueFunction) = qLearning.iterate(policy)
  logger.info(s"~updatedPolicy=$updatedPolicy")
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  val error = rootMeanSquaredError(randomWalk, updatedValueFunction)
  logger.info(s"~rootMeanSquaredError=$error ~= 0.1")

}

object RandomWalkProblemWithTDLambda extends App with Logging {

  Random.setSeed(0)

  val discount = 1d
  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 19) with ExploringStarts
  implicit val evaluator: EligibilityTracesEvaluator[Policy] =
    EligibilityTracesEvaluator[Policy](discount)
  val episodes = randomWalk.getStarts(numberOfStarts = 10)

  val policy = Policy(randomWalk)
  val initialValueFunction: ie.nix.rl.policy.Policy => vf.EligibilityTracing = policy =>
    vf.EligibilityTracing(policy, discount, stepSize = 0.25, eligibilityDecay = 0.8, initialValue = 0.5)

  val tdPredictor = EligibilityTracesPredictor[Policy](episodes, initialValueFunction)

  val (updatedPolicy, updatedValueFunction) = tdPredictor.iterate(policy)
  logger.info(s"updatedValueFunction=$updatedValueFunction")

  val error = rootMeanSquaredError(randomWalk, updatedValueFunction)
  logger.info(s"rootMeanSquaredError=$error ~= 0.3")

}

object RandomWalkProblemWithTDLambdaQF extends App with Logging {

  Random.setSeed(0)

  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 19) with Episodes
  implicit val evaluator: EligibilityTracesEvaluatorQF[Policy] =
    EligibilityTracesEvaluatorQF[Policy](discount = 1d)
  val episodes = randomWalk.getStarts(numberOfStarts = 10)

  val policy = Policy(randomWalk)
  val discount = 1d
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.EligibilityTracing = policy =>
    avf.EligibilityTracing(policy, discount, stepSize = 0.25, eligibilityDecay = 0.8, initialValue = 0.5)

  val tdPredictor = EligibilityTracesPredictorQF[Policy](episodes, initialValueFunction)(randomWalk, evaluator)

  val (updatedPolicy, updatedValueFunction) = tdPredictor.iterate(policy)
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  val error = rootMeanSquaredError(randomWalk, updatedValueFunction)
  logger.info(s"~rootMeanSquaredError=$error ~= 0.1")

}

object RandomWalkProblemWithSARSALambda extends App with Logging {

  Random.setSeed(0)

  implicit val randomWalk: RandomWalk = new RandomWalk(numberOfTiles = 19, winReward = 1d, looseReward = -1d)
  with Episodes
  val episodes = randomWalk.getStarts(numberOfStarts = 10)

  val policy = SoftPolicy(probabilityOfRandomAction = 0.1)
  val discount = 1d
  val initialValueFunction: ie.nix.rl.policy.Policy => avf.EligibilityTracing = policy =>
    avf.EligibilityTracing(policy, discount, stepSize = 0.25, eligibilityDecay = 0.6, initialValue = 0.0)

  val sarsaLambda = SARSALambda(episodes, initialValueFunction, discount)

  val (updatedPolicy, updatedValueFunction) = sarsaLambda.iterate(policy)
  logger.info(s"~updatedPolicy=$updatedPolicy")
  logger.info(s"~updatedValueFunction=$updatedValueFunction")

  val difference = policyDifference(updatedPolicy, Righty(randomWalk))
  logger.info(s"~difference=$difference ~= 0.1 due to Softness")

}
