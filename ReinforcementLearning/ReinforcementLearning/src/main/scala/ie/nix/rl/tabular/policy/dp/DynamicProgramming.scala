package ie.nix.rl.tabular.policy.dp

import ie.nix.rl.Environment
import ie.nix.rl.Environment.{StateAction, Transition}
import ie.nix.rl.policy.ValueFunction
import ie.nix.rl.tabular.Model
import ie.nix.rl.tabular.policy.{DeterministicPolicy, PolicyIterator}
import org.apache.logging.log4j.scala.Logging

object DynamicProgramming extends Logging {

  def apply(numberOfIterations: Int, numberOfSweeps: Int, discount: Double)(
      implicit environment: Environment with Model): PolicyIterator[DeterministicPolicy, ValueFunction] = {

    implicit val evaluator = BackupEvaluator(discount, numberOfSweeps)
    implicit val improver = GreedyBackupImprover(discount)

    new PolicyIterator[DeterministicPolicy, ValueFunction](numberOfIterations)

  }

  def backupState(stateAction: StateAction, discount: Double, valueFunction: ValueFunction)(
      implicit environment: Environment with Model): Double = {

    def getPossibleTransitionForStateAction(stateAction: StateAction): Vector[(Transition, Double)] = {
      environment.getStates
        .map(state => Transition(stateAction, state))
        .filter(transition => environment.getProbabilityOfTransition(transition) > 0d)
        .map(transition => (transition, environment.getProbabilityOfTransition(transition)))
    }

    val newBackUpValue = getPossibleTransitionForStateAction(stateAction)
      .foldLeft(0d)((backupValue, transitionAndProbability) => {
        val transition = transitionAndProbability._1
        val probabilityOfTransition = transitionAndProbability._2
        val rewardForTransition = environment.getRewardForTransition(transition)
        val finalStateValue = valueFunction.getValue(transition.finalState)
        logger.info(
          s"~transition=$transition, " +
            s"probabilityOfTransition=$probabilityOfTransition, " +
            s"rewardForTransition=$rewardForTransition, " +
            s"finalStateValue=$finalStateValue")
        // v(s) = p(s'|a,s) * (reward(s,a,s') + (y * v(s')))
        val newBackUpValue = backupValue + probabilityOfTransition * (rewardForTransition + (discount * finalStateValue))
        logger.info(s"~stateAction=$stateAction, backupValue=$backupValue, newBackUpValue=$newBackUpValue")
        newBackUpValue
      })
    logger.trace(s"~stateAction=$stateAction, newBackUpValue=$newBackUpValue")
    newBackUpValue

  }
}
