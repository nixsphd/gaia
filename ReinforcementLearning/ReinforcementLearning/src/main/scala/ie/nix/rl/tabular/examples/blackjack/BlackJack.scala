package ie.nix.rl.tabular.examples.blackjack

import ie.nix.rl.Action
import ie.nix.rl.Environment.{Reward, StateAction, Transition}
import ie.nix.rl.tabular.Environment
import ie.nix.rl.tabular.Selector.selectItem
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.examples.blackjack.BlackJack._
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

object BlackJack {

  type Card = Int
  type SumOfCards = Int
  val WIN: Reward = 1
  val DRAW: Reward = 0
  val LOOSE: Reward = -1

  case class State(sumOfPlayersCards: SumOfCards, playerHasUsableAce: Boolean, dealersVisibleCard: Card)
      extends ie.nix.rl.State {
    override def toString: String = s"BlackJackState($sumOfPlayersCards, $playerHasUsableAce, $dealersVisibleCard)"
    override def isTerminal: Boolean = sumOfPlayersCards > 21

  }

  case object Hit extends Action {
    override def toString: String = "Hit"
  }

  case object Stick extends Action {
    override def toString: String = "Stick"
    override def isTerminal: Boolean = true
  }

  def apply(): BlackJack = {
    new BlackJack(stateActions, Set())
  }

  private def stateActions: Set[StateAction] = {
    (for {
      sumOfPlayersCards <- 12 to 21
      playerHasUsableAce <- List(true, false)
      dealersVisibleCard <- 1 to 10
      action <- List(Hit, Stick)
    } yield {
      StateAction(State(sumOfPlayersCards, playerHasUsableAce, dealersVisibleCard), action)
    }).toSet
  }

}

class BlackJack private[blackjack] (override val stateActionsSet: Set[StateAction],
                                    override val terminalStateSet: Set[ie.nix.rl.State])
    extends Environment(stateActionsSet, terminalStateSet)
    with Episodes
    with Logging {

  def this() = {
    this(stateActions, Set())
  }

  override def toString: String = "BlackJack"

  override def getTransitionForStateAction(stateAction: StateAction): Transition = stateAction match {
    case StateAction(_, Stick) => Transition(stateAction, stateAction.state)
    case StateAction(blackJackState: State, Hit) =>
      Transition(stateAction, getStateAfterPlayerHit(blackJackState))
    case _ =>
      logger.error(s"~Unexpected state ${stateAction.state.getClass}, require BlackJackState")
      Transition(stateAction, stateAction.state)
  }

  private def getStateAfterPlayerHit(currentState: State): State = {
    val nextPlayerCard = dealNextCard()
    if (isUsableAce(nextPlayerCard, currentState.sumOfPlayersCards)) {
      getNextStateGivenUsableAce(currentState)
    } else {
      getNextStateGivenPlayerCard(currentState, nextPlayerCard)
    }
  }

  private def isUsableAce(nextPlayerCard: Card, sumOfPlayersCards: SumOfCards): Boolean = {
    // If the player holds an ace that he could count as 11 without going bust, then the ace is said to be
    // usable.
    nextPlayerCard === 1 && sumOfPlayersCards + 11 <= 21
  }

  private def getNextStateGivenPlayerCard(currentState: State, playerCard: Card): State = {
    if (isPlayerBustWithUsableAce(currentState, playerCard)) {
      currentState.copy(sumOfPlayersCards = currentState.sumOfPlayersCards + playerCard - 10,
                        playerHasUsableAce = false)

    } else {
      currentState.copy(sumOfPlayersCards = currentState.sumOfPlayersCards + playerCard)
    }
  }

  private def isPlayerBustWithUsableAce(currentState: State, playerCard: Card) = {
    currentState.sumOfPlayersCards + playerCard > 21 && currentState.playerHasUsableAce
  }

  private def getNextStateGivenUsableAce(currentState: State): State = {
    // In this case it is always counted as 11 because counting it as 1 would make the sum 11 or less, in which
    // case there is no decision to be made because, obviously, the player should always hit
    currentState.copy(sumOfPlayersCards = currentState.sumOfPlayersCards + 11, playerHasUsableAce = true)
  }

  override def getRewardForTransition(transition: Transition): Reward = transition match {
    case Transition(StateAction(_, Hit), newState: State) if newState.sumOfPlayersCards > 21 => LOOSE
    case Transition(StateAction(_, Hit), _)                                                  => 0 // still playing
    case Transition(StateAction(_, Stick), currentState: State)                              => getRewardAfterDealerPlays(currentState)
  }

  private def getRewardAfterDealerPlays(currentState: State): Reward = {
    // The dealer sticks on any sum of 17 or greater, and hits otherwise.
    // If the dealer goes bust, then the player wins; otherwise, the outcome—win, lose, or draw—is determined by
    // whose final sum is closer to 21.
    def dealerPlayHelper(sumOfPlayersCards: SumOfCards,
                         sumOfDealersCards: SumOfCards,
                         dealerHasUsableAce: Boolean): Reward = {
      val reward = (sumOfPlayersCards, sumOfDealersCards) match {
        case _ if sumOfDealersCards > 21 && !dealerHasUsableAce                      => WIN
        case _ if sumOfDealersCards >= 17 && sumOfPlayersCards > sumOfDealersCards   => WIN
        case _ if sumOfDealersCards >= 17 && sumOfPlayersCards === sumOfDealersCards => DRAW
        case _ if sumOfDealersCards >= 17 && sumOfPlayersCards < sumOfDealersCards   => LOOSE
        case _ if sumOfDealersCards > 21 && dealerHasUsableAce =>
          dealerPlayHelper(sumOfPlayersCards, sumOfDealersCards + dealNextCard - 10, dealerHasUsableAce = false)
        case _ =>
          dealerPlayHelper(sumOfPlayersCards, sumOfDealersCards + dealNextCard, dealerHasUsableAce)
      }
      logger trace s"reward=$reward, sumOfPlayersCards=$sumOfPlayersCards, sumOfDealersCards=$sumOfDealersCards"
      reward
    }
    val dealerHiddenCard = dealNextCard()
    val dealerHasUsableAce = currentState.dealersVisibleCard === 1 || dealerHiddenCard === 1
    if (dealerHasUsableAce) {
      val reward = dealerPlayHelper(currentState.sumOfPlayersCards,
                                    currentState.dealersVisibleCard + dealerHiddenCard + 10,
                                    dealerHasUsableAce)
      logger.trace(s"~reward=$reward")
      reward
    } else {
      val reward = dealerPlayHelper(currentState.sumOfPlayersCards,
                                    currentState.dealersVisibleCard + dealerHiddenCard,
                                    dealerHasUsableAce)
      logger.trace(s"~reward=$reward")
      reward
    }

  }

  def dealNextCard(): Card = {
    val cardProbabilities = Map(1 -> 1d / 13,
                                2 -> 1d / 13,
                                3 -> 1d / 13,
                                4 -> 1d / 13,
                                5 -> 1d / 13,
                                6 -> 1d / 13,
                                7 -> 1d / 13,
                                8 -> 1d / 13,
                                9 -> 1d / 13,
                                10 -> 4d / 13)
    val probability = Random.nextDouble()
    val nextCard = selectItem[Int](cardProbabilities.toSeq, probability)
    logger.debug(s"~nextCard=$nextCard")
    nextCard
  }

  // Episodes
  override def getInitialState(): State = {

    def initTableHelper(currentState: State): State = currentState match {
      case State(sumOfPlayersCards, _, _) if sumOfPlayersCards >= 12 =>
        currentState
      case _ =>
        initTableHelper(getStateAfterPlayerHit(currentState))
    }

    val dealerShownCard = dealNextCard
    val initialState = initTableHelper(State(sumOfPlayersCards = 0, playerHasUsableAce = false, dealerShownCard))
    initialState

  }
}
