package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.Selector.selectItem
import ie.nix.rl.{Action, Environment, State, policy}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

object Policy {

  def apply(implicit environment: Environment): Policy = {
    new Policy(getEquiprobableStateActionProbabilitiesMap(environment))
  }

  def apply(initialValue: Double)(implicit environment: Environment): Policy = {
    new Policy(getStateActionProbabilityMap(initialValue))
  }

  def RandomPolicy(implicit environment: Environment): Policy = {
    new Policy(getRandomStateActionProbabilityMap(environment))
  }

  def getEquiprobableStateActionProbabilitiesMap(implicit environment: Environment): Map[StateAction, Double] = {
    (for {
      state <- environment.getNonTerminalStates
      actions = environment.getActionsForState(state)
      action <- actions
    } yield {
      (StateAction(state, action), 1d / actions.size)
    }).toMap
  }

  def getStateActionProbabilityMap(initialValue: Double)(
      implicit environment: Environment): Map[StateAction, Double] = {
    environment.getStateActions
      .map(stateAction => (stateAction, initialValue))
      .toMap
  }

  def getRandomStateActionProbabilityMap(implicit environment: Environment): Map[StateAction, Double] = {
    environment.getStateActions
      .map(stateAction => (stateAction, Random.nextDouble()))
      .toMap
  }

  def getStateActionProbabilityMap(policy: Policy): Map[StateAction, Double] = {
    (for {
      state <- policy.getStates
      stateActionAndProbabilities = policy.getStateActionAndProbabilities(state)
      stateActionAndProbability <- stateActionAndProbabilities
    } yield {
      stateActionAndProbability
    }).toMap
  }

  def difference(aPolicy: Policy, anotherPolicy: Policy): Int = {
    (for {
      state <- aPolicy.getStates
      actionForPolicy = aPolicy.getMostProbableAction(state)
      actionForOtherPolicy = anotherPolicy.getMostProbableAction(state)
    } yield {
      actionForPolicy === actionForOtherPolicy
    }).count(_ == false)
  }

}

class Policy(private val stateActionProbabilityMap: Map[StateAction, Double]) extends policy.Policy with Logging {

  override def toString: String = {
    stateActionProbabilityMap.toVector
      .sortBy(_._1.toString)
      .mkString(start = getClass.getSimpleName + "(", sep = ", ", end = ")")
  }

  override def getStates: Vector[State] = {
    stateActionProbabilityMap.keySet
      .map(stateAction => stateAction.state)
      .toVector
  }

  override def getStateActions: Vector[StateAction] = {
    stateActionProbabilityMap.keySet.toVector
  }

  override def getAction(state: State): Action = {
    val stateActionProbabilitiesForState = getStateActionAndProbabilities(state)
    val probability = Random.nextDouble()
    val stateAction = selectItem[StateAction](stateActionProbabilitiesForState, probability)
    logger.debug(s"~probability=$probability, state=$state -> action=${stateAction.action}")
    stateAction.action
  }

  override def getMostProbableAction(state: State): Action = {
    getStateActionAndProbabilities(state).maxBy(_._2)._1.action
  }

  override def getActions(state: State): Vector[Action] = {
    (for {
      (stateAction, _) <- stateActionProbabilityMap
      if stateAction.state === state
    } yield stateAction.action).toVector
  }

  override def getStateActions(state: State): Vector[StateAction] = {
    (for {
      (stateAction, _) <- stateActionProbabilityMap
      if stateAction.state === state
    } yield stateAction).toVector
  }

  override def getActionAndProbabilities(state: State): Vector[(Action, Double)] = {
    stateActionProbabilityMap.filterKeys(_.state === state).map(entry => (entry._1.action, entry._2)).toVector
  }

  override def getProbability(stateAction: StateAction): Double = {
    stateActionProbabilityMap.getOrElse(stateAction, 0d)
  }

  override def getStateActionAndProbabilities(state: State): Vector[(StateAction, Double)] = {
    stateActionProbabilityMap.filterKeys(_.state === state).toVector
  }

  override def updateProbability(stateAction: StateAction, probability: Double): Policy = {
    new Policy(stateActionProbabilityMap + (stateAction -> probability))
  }

}
