package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy
import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.avf.ActionValueFunction.getValueMap
import ie.nix.rl.tabular.policy.vf.AbstractWeightedAveraging
import org.apache.logging.log4j.scala.Logging

object WeightedAveraging {

  def apply(policy: Policy): WeightedAveraging = {
    new WeightedAveraging(getValueMap[(Double, Double)](policy)(() => (0d, 0d)))
  }

}

class WeightedAveraging private[avf] (stateValueMap: Map[StateAction, (Double, Double)])
    extends AbstractWeightedAveraging[StateAction](stateValueMap)
    with policy.ActionValueFunction
    with Logging {

  override def getStateActions: Vector[StateAction] = valueMap.keySet.toVector

  override def updateValue(stateAction: StateAction, value: Double): WeightedAveraging =
    updateValue(stateAction, value, 1d)

  def updateValue(stateAction: StateAction, valueNumerator: Double, valueDenominator: Double): WeightedAveraging = {
    new WeightedAveraging(valueMap + (stateAction -> newValue(stateAction, valueNumerator, valueDenominator)))

  }

}
