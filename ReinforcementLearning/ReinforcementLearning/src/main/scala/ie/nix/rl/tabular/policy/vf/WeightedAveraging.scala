package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.vf.ValueFunction.getValueMap
import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging

object WeightedAveraging {

  def apply(policy: Policy): WeightedAveraging = {
    new WeightedAveraging(getValueMap[(Double, Double)](policy)(() => (0d, 0d)))
  }

}

class WeightedAveraging private[vf] (stateValueMap: Map[State, (Double, Double)])
    extends AbstractWeightedAveraging[State](stateValueMap)
    with policy.ValueFunction
    with Logging {

  override def getStates: Vector[State] = valueMap.keySet.toVector

  override def updateValue(stateOrStateAction: State, value: Double): WeightedAveraging =
    updateValue(stateOrStateAction, value, 1d)

  def updateValue(state: State, value: Double, weight: Double): WeightedAveraging = {
    new WeightedAveraging(valueMap + (state -> newValue(state, value, weight)))

  }

}

abstract class AbstractWeightedAveraging[S] private[policy] (valueMap: Map[S, (Double, Double)])
    extends AbstractValueFunction[S, (Double, Double)](valueMap)
    with Logging {

  override def mapValue(value: (Double, Double)): Double = value._1

  def newValue(stateOrStateAction: S, value: Double, weight: Double): (Double, Double) = {
    val (initialValue, initialWeight) = valueMap.getOrElse(stateOrStateAction, (0d, 0d))
    // C(St,At) = C(St,At) + W
    val newWeight = initialWeight + weight
    // Q(St,At) = Q(St,At) + W/C(St,At) [G - Q(St,At)]
    val newValue = initialValue + ((weight / newWeight) * (value - initialValue))
    logger trace s"~stateOrStateAction=$stateOrStateAction, newValue=$newValue, newWeight=$newWeight"
    (newValue, newWeight)

  }

}
