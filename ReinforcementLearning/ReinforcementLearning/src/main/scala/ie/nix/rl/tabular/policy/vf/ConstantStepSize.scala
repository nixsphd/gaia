package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import ie.nix.rl.tabular.policy.vf.ValueFunction.{getValueMap, randomValue}
import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging

object ConstantStepSize {

  type StepSize = Double

  def apply(policy: Policy, stepSize: StepSize): ConstantStepSize = {
    new ConstantStepSize(getValueMap[Double](policy)(randomValue), stepSize)
  }

  def apply(policy: Policy, initialValue: Double, stepSize: StepSize): ConstantStepSize = {
    new ConstantStepSize(getValueMap[Double](policy)(() => initialValue), stepSize)
  }

}

class ConstantStepSize private[policy] (valueMap: Map[State, Double], stepSize: StepSize)
    extends AbstractConstantStepSize[State](valueMap, stepSize)
    with policy.ValueFunction {

  override def getStates: Vector[State] = valueMap.keySet.toVector

  override def updateValue(state: State, target: Double): ConstantStepSize = {
    new ConstantStepSize(valueMap + (state -> newValue(state, target)), stepSize)
  }

}

abstract class AbstractConstantStepSize[S] private[policy] (valueMap: Map[S, Double], val stepSize: StepSize)
    extends AbstractValueFunction[S, Double](valueMap)
    with Logging {

  override def mapValue(value: Double): Double = value

  def newValue(state: S, target: Double): Double = {
    val originalValue = getValue(state)
    // V(St) ← V(St) + α􏰖[target − V(St)􏰗]
    originalValue + (stepSize * (target - originalValue))
  }
}
