package ie.nix.rl.policy

trait PolicyImprover[P <: Policy, -VF] {

  def improvePolicy(policy: P, valueFunction: VF): P

}
