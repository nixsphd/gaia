package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import ie.nix.rl.tabular.policy.vf.ValueFunction.getValueMap
import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging

object EligibilityTracing {

  def apply(policy: Policy,
            discount: Double,
            stepSize: StepSize,
            eligibilityDecay: Double,
            initialValue: Double): EligibilityTracing = {
    new EligibilityTracing(getValueMap[(Double, Double)](policy)(() => (initialValue, 0d)),
                           discount,
                           stepSize,
                           eligibilityDecay)
  }

  def apply(policy: Policy, discount: Double, stepSize: StepSize, eligibilityDecay: Double): EligibilityTracing = {
    apply(policy, discount, stepSize, eligibilityDecay, 0d)
  }

}

class EligibilityTracing private[vf] (stateValueMap: Map[State, (Double, Double)],
                                      discount: Double,
                                      stepSize: StepSize,
                                      eligibilityDecay: Double)
    extends AbstractEligibilityTracing[State](stateValueMap, discount, stepSize, eligibilityDecay)
    with policy.ValueFunction
    with Logging {

  override def getStates: Vector[State] = valueMap.keySet.toVector

  override def updateValue(state: State, error: Double): EligibilityTracing = {
    incrementEligibilityForState(state)
      .updateErrorForAllStates(error)
      .decayEligibilityForAllStates
  }

  override def updateValueAndEligibilityTrace(state: State,
                                              value: Double,
                                              eligibilityTrace: Double): EligibilityTracing = {
    val newValue = (value, eligibilityTrace)
    new EligibilityTracing(stateValueMap + (state -> newValue), discount, stepSize, eligibilityDecay)
  }

  def incrementEligibilityForState(state: State): EligibilityTracing = {
    val (value, eligibility) = getValueAndEligibility(state)
    // Z(S) ← Z(S) + 1
    val newEligibilityOfState = eligibility + 1d
    updateValueAndEligibilityTrace(state, value, newEligibilityOfState)
  }

  def updateErrorForAllStates(error: Double): EligibilityTracing = {
    getStates.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, eligibility) = getValueAndEligibility(stateOrStateAction)
      // V(s) ← V(s) + αδZ(s)
      val newValue = value + (stepSize * error * eligibility)
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, newValue, eligibility)
    })
  }

  def decayEligibilityForAllStates(): EligibilityTracing = {
    getStatesOrStateActions.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, eligibility) = getValueAndEligibility(stateOrStateAction)
      // Z(s) ← γλZ(s)
      val newEligibility = discount * eligibilityDecay * eligibility
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, value, newEligibility)
    })
  }

  def clearEligibilityForAllStates(): EligibilityTracing = {
    getStatesOrStateActions.foldLeft(this)((eligibilityTraces, stateOrStateAction) => {
      val (value, _) = getValueAndEligibility(stateOrStateAction)
      eligibilityTraces.updateValueAndEligibilityTrace(stateOrStateAction, value, 0d)
    })
  }

}

abstract class AbstractEligibilityTracing[S] private[policy] (valueMap: Map[S, (Double, Double)],
                                                              val discount: Double,
                                                              val stepSize: StepSize,
                                                              val eligibilityDecay: Double)
    extends AbstractValueFunction[S, (Double, Double)](valueMap)
    with Logging {

  override def mapValue(value: (Double, Double)): Double = value._1

  def getValueAndEligibility(stateOrStateAction: S): (Double, Double) = {
    valueMap.getOrElse(stateOrStateAction, (0d, 0d))
  }

  def updateValueAndEligibilityTrace(stateOrStateAction: S,
                                     value: Double,
                                     eligibilityTrace: Double): AbstractEligibilityTracing[S]

}
