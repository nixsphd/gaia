package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy
import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.avf.ActionValueFunction.getValueMap
import ie.nix.rl.tabular.policy.vf.AbstractQDLambda
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import org.apache.logging.log4j.scala.Logging

object QDLambda {

  def apply(policy: Policy,
            discount: Double,
            innovationStepSize: StepSize,
            consensusStepSize: StepSize,
            eligibilityDecay: Double,
            initialValue: Double): QDLambda = {
    new QDLambda(getValueMap[(Double, Double)](policy)(() => (initialValue, 0d)),
                 discount,
                 innovationStepSize,
                 consensusStepSize,
                 eligibilityDecay)
  }

  def apply(policy: Policy,
            discount: Double,
            innovationStepSize: StepSize,
            consensusStepSize: StepSize,
            eligibilityDecay: Double): QDLambda = {
    apply(policy, discount, innovationStepSize, consensusStepSize, eligibilityDecay, 0d)
  }

}

class QDLambda private[avf] (stateValueMap: Map[StateAction, (Double, Double)],
                             discount: Double,
                             innovationStepSize: StepSize,
                             consensusStepSize: StepSize,
                             eligibilityDecay: Double)
    extends AbstractQDLambda[StateAction](stateValueMap,
                                          discount,
                                          innovationStepSize,
                                          consensusStepSize,
                                          eligibilityDecay)
    with policy.ActionValueFunction
    with Logging {

  override def getStateActions: Vector[StateAction] = valueMap.keySet.toVector

  override def updateValue(stateAction: StateAction, value: Double): QDLambda = {
    logger warn s"Can not call updateValue with single value $value for AbstractQDLambda, " +
      s"expect two terms, innovation and consensus"
    updateValue(stateAction, value, 0d)
  }

  def updateValue(stateAction: StateAction, innovation: Double, consensus: Double): QDLambda = {
    incrementEligibilityForState(stateAction)
      .updateValueForAllStates(innovation, consensus)
      .decayEligibilityForAllStates
  }

  override def updateValueAndEligibilityTrace(stateAction: StateAction,
                                              value: Double,
                                              eligibilityTrace: Double): QDLambda = {
    val newValue = (value, eligibilityTrace)
    new QDLambda(stateValueMap + (stateAction -> newValue),
                 discount,
                 innovationStepSize,
                 consensusStepSize,
                 eligibilityDecay)
  }

  def incrementEligibilityForState(stateAction: StateAction): QDLambda = {
    val (value, eligibility) = getValueAndEligibility(stateAction)
    // Z(S) ← Z(S) + 1
    val newEligibilityOfState = eligibility + 1d
    updateValueAndEligibilityTrace(stateAction, value, newEligibilityOfState)
  }

  def updateValueForAllStates(innovation: Double, consensus: Double): QDLambda = {
    getStatesOrStateActions.foldLeft(this)((qdLambda, stateOrStateAction) => {
      val (value, eligibility) = getValueAndEligibility(stateOrStateAction)
      // V(s) ← V(s) + α*innovation*Z(s) - b*consensus*Z(s)
      val newValue = value + (innovationStepSize * innovation * eligibility) - (consensusStepSize * consensus * eligibility)
      logger trace s"value=$value, eligibility=$eligibility, " +
        s"innovationStepSize=$innovationStepSize, innovation=$innovation, " +
        s"consensusStepSize=$consensusStepSize, consensus=$consensus, newValue=$newValue"
      qdLambda.updateValueAndEligibilityTrace(stateOrStateAction, newValue, eligibility)
    })
  }

  def decayEligibilityForAllStates(): QDLambda = {
    getStatesOrStateActions.foldLeft(this)((qdLambda, stateOrStateAction) => {
      val (value, eligibility) = getValueAndEligibility(stateOrStateAction)
      // Z(s) ← γλZ(s)
      val newEligibility = discount * eligibilityDecay * eligibility
      qdLambda.updateValueAndEligibilityTrace(stateOrStateAction, value, newEligibility)
    })
  }

  def clearEligibilityForAllStates(): QDLambda = {
    getStatesOrStateActions.foldLeft(this)((qdLambda, stateOrStateAction) => {
      val (value, _) = getValueAndEligibility(stateOrStateAction)
      qdLambda.updateValueAndEligibilityTrace(stateOrStateAction, value, 0d)
    })
  }

}
