package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy
import ie.nix.rl.tabular.policy.vf.AbstractBatch
import org.apache.logging.log4j.scala.Logging

object Batch {

  def apply(actionValueFunction: ActionValueFunction): Batch = {
    new Batch(getStateActionValueMap(actionValueFunction), actionValueFunction)
  }

  def getStateActionValueMap(actionValueFunction: policy.ActionValueFunction): Map[StateAction, Double] = {
    (for { stateAction <- actionValueFunction.getStateActions } yield {
      (stateAction, actionValueFunction.getValue(stateAction))
    }).toMap
  }
}

class Batch private[avf] (valueMap: Map[StateAction, Double], val actionValueFunction: policy.ActionValueFunction)
    extends AbstractBatch[StateAction](valueMap)
    with policy.ActionValueFunction
    with Logging {

  override def getStateActions: Vector[StateAction] = valueMap.keySet.toVector

  override def updateValue(stateAction: StateAction, value: Double): Batch =
    new Batch(valueMap, actionValueFunction.updateValue(stateAction, value))

  override def consolidateBatch: Batch = {

    valueMap.foldLeft(this)((batch, stateActionValue) => {
      val stateAction = stateActionValue._1
      val valueToConsolidate = actionValueFunction.getValue(stateAction)
      logger.trace(s"~stateAction=$stateAction, valueToConsolidate=$valueToConsolidate")
      new Batch(batch.valueMap + (stateAction -> valueToConsolidate), actionValueFunction)
    })

  }

}
