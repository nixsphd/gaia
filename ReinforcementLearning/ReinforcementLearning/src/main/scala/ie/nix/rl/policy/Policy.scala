package ie.nix.rl.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.{Action, State}

trait Policy {

  def getStates: Vector[State]

  def getStateActions: Vector[StateAction]

  def getAction(state: State): Action

  def getActions(state: State): Vector[Action]

  def getProbability(stateAction: StateAction): Double

  def getMostProbableAction(state: State): Action

  def getActionAndProbabilities(state: State): Vector[(Action, Double)]

  def getStateActions(state: State): Vector[StateAction]

  def getStateActionAndProbabilities(state: State): Vector[(StateAction, Double)]

  def updateProbability(stateAction: StateAction, probability: Double): Policy

}
