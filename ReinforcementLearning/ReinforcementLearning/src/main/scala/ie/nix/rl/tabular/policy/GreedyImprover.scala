package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.{ActionValueFunction, PolicyImprover}
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

object GreedyImprover {

  def apply(): GreedyImprover = {
    new GreedyImprover()
  }

}

class GreedyImprover private[GreedyImprover] () extends PolicyImprover[SoftPolicy, ActionValueFunction] with Logging {

  def improvePolicy(policy: DeterministicPolicy, actionValueFunction: ActionValueFunction): DeterministicPolicy = {

    policy.getStates.foldLeft(policy)((policy, state) => {

      val policyAction = policy.getMostProbableAction(state)
      val policyActionValue = actionValueFunction.getValue(StateAction(state, policyAction))

      val bestAction = getBestAction(state, actionValueFunction)
      val bestActionValue = actionValueFunction.getValue(StateAction(state, bestAction))

      if (policyAction != bestAction && policyActionValue < bestActionValue) {
        val updatedPolicy = policy.updateAction(state, bestAction)
        logger info s"Improved action for $state to $bestAction value=$bestActionValue " +
          s"from $policyAction value=$policyActionValue"
        updatedPolicy

      } else {
        policy
      }
    })

  }

  def improvePolicy(policy: SoftPolicy, actionValueFunction: ActionValueFunction): SoftPolicy = {

    policy.getStates.foldLeft(policy)((policy, state) => {

      val policyAction = policy.getMostProbableAction(state)
      val policyActionValue = actionValueFunction.getValue(StateAction(state, policyAction))

      val bestAction = getBestAction(state, actionValueFunction)
      val bestActionValue = actionValueFunction.getValue(StateAction(state, bestAction))

      logger trace s"state=$state, policyAction=$policyAction, bestAction=$bestAction"
      logger trace s"state=$state, policyActionValue=$policyActionValue, bestActionValue=$bestActionValue"

      if (policyAction != bestAction && policyActionValue < bestActionValue) {
        val updatedPolicy = policy.updateAction(state, bestAction)
        logger trace s"Improved action for $state to $bestAction value=$bestActionValue " +
          s"from $policyAction value=$policyActionValue"
        updatedPolicy

      } else {
        policy
      }
    })

  }

  def getBestAction(state: State, actionValueFunction: ActionValueFunction): Action = {
    val (bestStateAction, _) = getStateActionsValues(actionValueFunction, state).maxBy(_._2)
    bestStateAction.action

  }

  def getStateActionsValues(actionValueFunction: ActionValueFunction, state: State): Vector[(StateAction, Double)] = {
    actionValueFunction.getStateActions
      .filter(stateAction => stateAction.state === state)
      .map(stateAction => (stateAction, actionValueFunction.getValue(stateAction)))

  }

}
