package ie.nix.rl.policy

import ie.nix.rl.State

trait ValueFunction {

  def getStates: Vector[State]

  def getValue(state: State): Double

  def updateValue(state: State, value: Double): ValueFunction

}
