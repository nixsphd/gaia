package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.vf.ValueFunction.getValueMap
import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging

object SampleAveraging {

  def apply(policy: Policy): SampleAveraging = {
    new SampleAveraging(getValueMap[(Double, Int)](policy)(() => (0d, 0)))
  }

}

class SampleAveraging private[policy] (valueMap: Map[State, (Double, Int)])
    extends AbstractSampleAveraging[State](valueMap)
    with policy.ValueFunction {

  override def getStates: Vector[State] = valueMap.keySet.toVector

  override def updateValue(state: State, value: Double): SampleAveraging = {
    new SampleAveraging(valueMap + (state -> newValue(state, value)))
  }

}

abstract class AbstractSampleAveraging[S] private[policy] (valueMap: Map[S, (Double, Int)])
    extends AbstractValueFunction[S, (Double, Int)](valueMap)
    with Logging {

  override def mapValue(value: (Double, Int)): Double = value._2 match {
    case 0 => 0d
    case _ => value._1 / value._2
  }

  def newValue(stateOrStateAction: S, value: Double): (Double, Int) = {
    val (originalSumOfValues, originalNumberOfValue) = valueMap.getOrElse(stateOrStateAction, (0d, 0))
    val newSumOfValues = originalSumOfValues + value
    val newNumberOfValue = originalNumberOfValue + 1
    (newSumOfValues, newNumberOfValue)
  }

}
