package ie.nix.rl.policy

import ie.nix.rl.Environment.StateAction

trait ActionValueFunction {

  def getStateActions: Vector[StateAction]

  def getValue(stateAction: StateAction): Double

  def updateValue(stateAction: StateAction, value: Double): ActionValueFunction

}
