package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy
import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.vf.AbstractValueFunction
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object ActionValueFunction extends Logging {

  def apply(policy: Policy): ActionValueFunction = {
    new ActionValueFunction(getValueMap[Double](policy)(() => Random.nextDouble()))
  }

  def apply(policy: Policy, initialValue: Double): ActionValueFunction = {
    new ActionValueFunction(getValueMap[Double](policy)(() => initialValue))
  }

  def getValueMap[V](policy: Policy)(initialValue: () => V): Map[StateAction, V] = {
    (for { stateAction <- policy.getStateActions } yield {
      (stateAction, initialValue())
    }).toMap
  }

  def difference(anValueFunction: AbstractValueFunction[StateAction, _],
                 anotherValueFunction: AbstractValueFunction[StateAction, _]): Double = {
    val states = (anValueFunction.valueMap.keySet ++ anotherValueFunction.valueMap.keySet).toVector
    val difference = (for { state <- states } yield {
      val aValue = anValueFunction.getValue(state)
      val otherValue = anotherValueFunction.getValue(state)
      math.abs(aValue - otherValue)
    }).sum
    difference
  }

}

class ActionValueFunction private[policy] (valueMap: Map[StateAction, Double])
    extends AbstractValueFunction[StateAction, Double](valueMap)
    with policy.ActionValueFunction
    with Logging {

  override def getStateActions: Vector[StateAction] = valueMap.keySet.toVector

  override def mapValue(value: Double): Double = value

  override def updateValue(stateAction: StateAction, value: Double): ActionValueFunction = {
    new ActionValueFunction(valueMap + (stateAction -> value))
  }

}
