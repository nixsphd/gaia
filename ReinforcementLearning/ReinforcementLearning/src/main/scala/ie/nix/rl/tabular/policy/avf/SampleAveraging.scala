package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy
import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.avf.ActionValueFunction.getValueMap
import ie.nix.rl.tabular.policy.vf.AbstractSampleAveraging

object SampleAveraging {

  def apply(policy: Policy): SampleAveraging = {
    new SampleAveraging(getValueMap[(Double, Int)](policy)(() => (0d, 0)))
  }

}

class SampleAveraging private[policy] (valueMap: Map[StateAction, (Double, Int)])
    extends AbstractSampleAveraging[StateAction](valueMap)
    with policy.ActionValueFunction {

  override def getStateActions: Vector[StateAction] = valueMap.keySet.toVector

  override def updateValue(stateAction: StateAction, value: Double): SampleAveraging = {
    new SampleAveraging(valueMap + (stateAction -> newValue(stateAction, value)))
  }

}
