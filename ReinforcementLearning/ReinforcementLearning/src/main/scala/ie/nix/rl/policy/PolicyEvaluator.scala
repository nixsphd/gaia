package ie.nix.rl.policy

trait PolicyEvaluator[-P <: Policy, +VF] {

  def evaluatePolicy(policy: P): VF

}
