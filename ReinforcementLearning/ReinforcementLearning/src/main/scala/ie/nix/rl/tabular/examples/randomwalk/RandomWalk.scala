package ie.nix.rl.tabular.examples.randomwalk

import ie.nix.rl.Environment.{Reward, StateAction, Transition}
import ie.nix.rl.policy.{ActionValueFunction, ValueFunction}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.examples.randomwalk.RandomWalk.{
  DEFAULT_LOOSE_REWARD,
  DEFAULT_WIN_REWARD,
  Left,
  Right,
  Tile,
  stateActions,
  terminalStates
}
import ie.nix.rl.tabular.{Environment, Model}
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.math.{pow, sqrt}

object RandomWalk extends Logging {

  case class Tile(tileNumber: Int, override val isTerminal: Boolean = false) extends State {
    override def toString: String = s"Tile($tileNumber)"
  }

  case object Right extends Action {
    override def toString: String = "Right"
  }

  case object Left extends Action {
    override def toString: String = "Left"
  }

  val DEFAULT_WIN_REWARD: Reward = 1d
  val DEFAULT_LOOSE_REWARD: Reward = 0d

  def apply(numberOfTiles: Int,
            winReward: Double = DEFAULT_WIN_REWARD,
            looseReward: Double = DEFAULT_LOOSE_REWARD): RandomWalk = {
    new RandomWalk(stateActions(numberOfTiles), terminalStates(numberOfTiles), numberOfTiles, winReward, looseReward)
  }

  private def terminalStates(numberOfTiles: Int): Set[State] = {
    Vector[State](Tile(0, isTerminal = true), Tile(numberOfTiles, isTerminal = true)).toSet
  }

  private def stateActions(numberOfTiles: Int): Set[StateAction] = {
    (for {
      tileNumber <- 1 until numberOfTiles
      action <- List(Right, Left)
    } yield {
      StateAction(Tile(tileNumber), action)
    }).toSet
  }

  def rootMeanSquaredError(randomWalk: RandomWalk, valueFunction: ValueFunction): Double = {
    val errorSquared = for { Tile(timeNumber, _) <- randomWalk.getNonTerminalStates } yield {
      val value = valueFunction.getValue(Tile(timeNumber))
      val idealValue = timeNumber.toDouble / randomWalk.numberOfTiles.toDouble
      val error = idealValue - value
      logger.info(s"~timeNumber=$timeNumber, value=$value, idealValue=$idealValue, error=$error")
      pow(error, 2d)
    }
    sqrt(errorSquared.sum)
  }

  def rootMeanSquaredError(randomWalk: RandomWalk, actionValueFunction: ActionValueFunction): Double = {
    val errorSquared = for { Tile(timeNumber, _) <- randomWalk.getNonTerminalStates } yield {
      val valueR = actionValueFunction.getValue(StateAction(Tile(timeNumber), Right))
      val valueL = actionValueFunction.getValue(StateAction(Tile(timeNumber), Left))
      val idealValue = timeNumber.toDouble / randomWalk.numberOfTiles.toDouble
      val error = idealValue - ((valueR + valueL) / 2d) //idealValue - value
      logger.info(s"~timeNumber=$timeNumber, valueR=$valueR, valueL=$valueL, idealValue=$idealValue, error=$error")
      pow(error, 2d)
    }
    sqrt(errorSquared.sum)
  }
}

class RandomWalk private[randomwalk] (override val stateActionsSet: Set[StateAction],
                                      override val terminalStateSet: Set[State],
                                      val numberOfTiles: Int,
                                      val winReward: Double,
                                      val looseReward: Double)
    extends Environment(stateActionsSet, terminalStateSet)
    with Model
    with Episodes
    with Logging {

  def this(numberOfTiles: Int, winReward: Double = DEFAULT_WIN_REWARD, looseReward: Double = DEFAULT_LOOSE_REWARD) =
    this(stateActions(numberOfTiles), terminalStates(numberOfTiles), numberOfTiles, winReward, looseReward)

  override def getTransitionForStateAction(stateAction: StateAction): Transition = {
    stateAction match {
      case StateAction(Tile(tileNumber, _), Left) if tileNumber - 1 === 0 =>
        Transition(stateAction, Tile(tileNumber - 1, isTerminal = true))
      case StateAction(Tile(tileNumber, _), Right) if tileNumber + 1 === numberOfTiles =>
        Transition(stateAction, Tile(tileNumber + 1, isTerminal = true))
      case StateAction(Tile(tileNumber, _), Left)  => Transition(stateAction, Tile(tileNumber - 1))
      case StateAction(Tile(tileNumber, _), Right) => Transition(stateAction, Tile(tileNumber + 1))
      case _ =>
        logger.error(s"~Unexpected stateAction, $stateAction")
        Transition(stateAction, stateAction.state)

    }
  }

  override def getRewardForTransition(transition: Transition): Reward =
    transition match {
      case Transition(_, Tile(tileNumber, true)) if tileNumber === numberOfTiles => winReward
      case Transition(_, Tile(tileNumber, true)) if tileNumber === 0             => looseReward
      case _                                                                     => 0d
    }

  override def getProbabilityOfTransition(transition: Transition): Reward =
    transition match {
      case Transition(StateAction(Tile(initialTileNumber, _), Right), Tile(finalTileNumber, _))
          if initialTileNumber + 1 === finalTileNumber =>
        1
      case Transition(StateAction(Tile(initialTileNumber, _), Left), Tile(finalTileNumber, _))
          if initialTileNumber - 1 === finalTileNumber =>
        1
      case _ => 0
    }

  // Episodes
  override def getInitialState(): State = {
    val initialState = Tile(numberOfTiles / 2d.toInt)
    logger.trace(s"~initialState=$initialState")
    initialState
  }
}
