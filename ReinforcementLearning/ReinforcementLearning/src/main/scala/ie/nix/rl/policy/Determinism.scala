package ie.nix.rl.policy

import ie.nix.rl.{Action, State}

trait Determinism {

  policy: Policy =>

  def updateAction(state: State, action: Action): Policy with Determinism

}
