package ie.nix.rl

import ie.nix.rl.Environment.{Reward, StateAction, Transition, TransitionReward}
import org.apache.logging.log4j.scala.Logging

trait EnvironmentView extends Logging {

  def getStates: Vector[State]

  def getTerminalStates: Vector[State]

  def getNonTerminalStates: Vector[State]

  def getStateActions: Vector[StateAction]

  def getActionsForState(state: State): Vector[Action]

  def getStateActionsForState(state: State): Vector[StateAction]

}

trait Environment extends EnvironmentView with Logging {

  def getTransitionForStateAction(stateAction: StateAction): Transition

  def getRewardForTransition(transition: Environment.Transition): Reward

  def getTransitionRewardForStateAction(stateAction: StateAction): TransitionReward

}

object Environment extends Logging {

  type Reward = Double

  case class StateAction(state: State, action: Action) {
    def isTerminal: Boolean = state.isTerminal || action.isTerminal
  }

  case class Transition(stateAction: StateAction, finalState: State) {
    def isTerminal: Boolean = stateAction.isTerminal || finalState.isTerminal
  }

  case class TransitionReward(transition: Transition, reward: Reward)

}
