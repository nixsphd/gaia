package ie.nix.rl.tabular.policy.mc

import ie.nix.rl.policy.{Determinism, EpisodicPolicyImprover, Policy}
import ie.nix.rl.tabular.episodes.Episodes
import ie.nix.rl.tabular.episodes.Episodes.Episode
import ie.nix.rl.tabular.policy.avf.{SampleAveraging, WeightedAveraging}
import ie.nix.rl.tabular.policy.{EpisodicPolicyIterator, mc}
import ie.nix.rl.{Environment, State}
import org.apache.logging.log4j.scala.Logging

object MonteCarlo {

  // Prediction only
  def MonteCarloPredictor[P <: Policy, VF](episodes: Vector[Episode], initialValueFunction: Policy => VF)(
      implicit environment: Environment with Episodes,
      evaluator: EpisodicEvaluator[P, VF]): EpisodicPolicyIterator[P, VF] = {

    implicit val improver: IdleEpisodicImprover[P, VF] = new mc.IdleEpisodicImprover

    new EpisodicPolicyIterator[P, VF](episodes, initialValueFunction)

  }

  // Online
  def apply(episodes: Vector[Episode], discount: Double)(implicit environment: Environment with Episodes)
    : EpisodicPolicyIterator[Policy with Determinism, SampleAveraging] = {

    implicit val evaluator: EpisodicEvaluator[Policy with Determinism, SampleAveraging] =
      EpisodicEvaluator[Policy with Determinism](discount: Double)
    implicit val improver: GreedyEpisodicImprover[SampleAveraging] =
      GreedyEpisodicImprover.apply

    val initialValueFunction = policy => SampleAveraging(policy)

    new EpisodicPolicyIterator[Policy with Determinism, SampleAveraging](episodes, initialValueFunction)

  }

  // Offline
  def apply[BP <: Policy](episodes: Vector[Episode],
                          initBehaviourPolicy: (Policy with Determinism) => BP,
                          discount: Double)(implicit environment: Environment with Episodes)
    : EpisodicPolicyIterator[Policy with Determinism, WeightedAveraging] = {

    implicit val evaluator: OfflineEpisodicEvaluator[Policy with Determinism, BP, WeightedAveraging] =
      OfflineEpisodicEvaluator[Policy with Determinism, BP](initBehaviourPolicy, discount)
    implicit val improver: GreedyEpisodicImprover[WeightedAveraging] = GreedyEpisodicImprover.apply

    val initialValueFunction = targetPolicy => WeightedAveraging(targetPolicy)

    new EpisodicPolicyIterator[Policy with Determinism, WeightedAveraging](episodes, initialValueFunction)

  }

}

class IdleEpisodicImprover[P <: ie.nix.rl.policy.Policy, VF](implicit val environment: Environment with Episodes)
    extends EpisodicPolicyImprover[P, VF]
    with Logging {

  override def improvePolicy(policy: P, valueFunction: VF, statesToUpdate: Vector[State]): P = policy
}
