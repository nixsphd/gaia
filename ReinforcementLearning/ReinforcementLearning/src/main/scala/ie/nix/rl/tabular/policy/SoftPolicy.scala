package ie.nix.rl.tabular.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Determinism
import ie.nix.rl.tabular.EnvironmentView
import ie.nix.rl.tabular.Selector.selectItem
import ie.nix.rl.tabular.policy.Policy.getStateActionProbabilityMap
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

object SoftPolicy extends Logging {

  def apply(probabilityOfRandomAction: Double)(implicit environment: EnvironmentView): SoftPolicy = {
    new SoftPolicy(getRandomStateActionProbabilityMap(probabilityOfRandomAction)(environment),
                   probabilityOfRandomAction)
  }

  def apply(probabilityOfRandomAction: Double, policy: ie.nix.rl.policy.Policy): SoftPolicy = {
    new SoftPolicy(getSoftStateActionProbabilityMap(probabilityOfRandomAction, policy), probabilityOfRandomAction)
  }

  def getRandomStateActionProbabilityMap(probabilityOfRandomAction: Double)(
      implicit environment: EnvironmentView): Map[StateAction, Double] = {
    (for {
      state <- environment.getNonTerminalStates
      actions = environment.getActionsForState(state)
      randomlySelectedAction = actions(Random.nextInt(actions.length))
      probabilityOfOtherActions = probabilityOfRandomAction / actions.size
      probabilityOfRandomlySelectedAction = 1d - probabilityOfRandomAction + probabilityOfOtherActions
      action <- actions
    } yield {
      if (action === randomlySelectedAction) {
        (StateAction(state, action), probabilityOfRandomlySelectedAction)
      } else {
        (StateAction(state, action), probabilityOfOtherActions)
      }
    }).toMap
  }

  def getSoftStateActionProbabilityMap(probabilityOfRandomAction: Double,
                                       policy: ie.nix.rl.policy.Policy): Map[StateAction, Double] = {
    (for {
      state <- policy.getStates
      actions = policy.getActions(state)
      selectedAction = policy.getMostProbableAction(state)
      probabilityOfOtherActions = probabilityOfRandomAction / actions.size
      probabilityOfSelectedAction = 1d - probabilityOfRandomAction + probabilityOfOtherActions
      action <- actions
    } yield {
      if (action === selectedAction) {
        (StateAction(state, action), probabilityOfSelectedAction)
      } else {
        (StateAction(state, action), probabilityOfOtherActions)
      }
    }).toMap
  }

  def Policy(softPolicy: SoftPolicy): Policy = {
    new Policy(getStateActionProbabilityMap(softPolicy))
  }

  def isEquivalent(softPolicy: SoftPolicy, policy: Policy): Boolean = {
    (for {
      state <- softPolicy.getStates
      actionForDeterministicPolicy = softPolicy.getAction(state)
      actionForSoftPolicy = policy.getMostProbableAction(state)
    } yield {
      actionForDeterministicPolicy === actionForSoftPolicy
    }).reduce(_ && _)
  }

  def isEquivalent(softPolicy: SoftPolicy, deterministicPolicy: DeterministicPolicy): Boolean = {
    (for {
      state <- deterministicPolicy.getStates
      actionForDeterministicPolicy = deterministicPolicy.getAction(state)
      actionForSoftPolicy = softPolicy.getMostProbableAction(state)
    } yield {
      actionForDeterministicPolicy === actionForSoftPolicy
    }).reduce(_ && _)
  }

}

class SoftPolicy private[policy] (stateActionProbabilityMap: Map[StateAction, Double],
                                  val probabilityOfRandomAction: Double)
    extends Policy(stateActionProbabilityMap)
    with Determinism
    with Logging {

  def this(probabilityOfRandomAction: Double) = {
    this(Map(), probabilityOfRandomAction)
  }

  override def getAction(state: State): Action = {
    val stateActionProbabilitiesForState = getStateActionAndProbabilities(state)
    logger.trace(s"~state=$state, stateActionProbabilitiesForState=$stateActionProbabilitiesForState")
    val probability = Random.nextDouble()
    val stateAction = selectItem[StateAction](stateActionProbabilitiesForState, probability)
    logger.debug(s"~probability=$probability, state=$state -> action=${stateAction.action}")
    stateAction.action
  }

  override def updateProbability(stateAction: StateAction, probability: Double): SoftPolicy = {
    new SoftPolicy(stateActionProbabilityMap + (stateAction -> probability), probabilityOfRandomAction)
  }

  override def updateAction(state: State, action: Action): SoftPolicy = {
    updateAction(StateAction(state, action))
  }

  def updateAction(stateAction: StateAction): SoftPolicy = {
    logger trace s"~stateAction=$stateAction"
    val stateActions = getStateActions(stateAction.state)
    val probabilityOfNonGreedyAction = probabilityOfRandomAction / stateActions.size
    val probabilityOfGreedyAction = 1d - probabilityOfRandomAction + probabilityOfNonGreedyAction
    stateActions
      .foldLeft(this)((policy, stateActionForState) => {
        if (stateActionForState.action === stateAction.action) {
          policy.updateProbability(stateActionForState, probabilityOfGreedyAction)
        } else {
          policy.updateProbability(stateActionForState, probabilityOfNonGreedyAction)
        }
      })

  }

}
