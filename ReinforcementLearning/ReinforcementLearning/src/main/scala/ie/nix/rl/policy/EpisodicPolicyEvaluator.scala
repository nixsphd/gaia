package ie.nix.rl.policy

import ie.nix.rl.State
import ie.nix.rl.tabular.episodes.Episodes.Episode

trait EpisodicPolicyEvaluator[P <: Policy, VF] {

  def evaluatePolicy(policy: P, valueFunction: VF, episode: Episode): (VF, Vector[State])

}
