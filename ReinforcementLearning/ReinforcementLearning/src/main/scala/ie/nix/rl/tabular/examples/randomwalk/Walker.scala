package ie.nix.rl.tabular.examples.randomwalk

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.examples.randomwalk.RandomWalk.{Left, Right, Tile}
import ie.nix.rl.tabular.policy
import ie.nix.rl.tabular.policy.DeterministicPolicy

object Walker {

  def Lefty(implicit randomWalk: RandomWalk): DeterministicPolicy = {
    randomWalk.getNonTerminalStates
      .foldLeft(policy.DeterministicPolicy(randomWalk))((policy, state) => {
        state match {
          case tile: Tile =>
            policy updateAction StateAction(tile, Left)
        }
      })
  }

  def Righty(implicit randomWalk: RandomWalk): DeterministicPolicy = {
    randomWalk.getNonTerminalStates
      .foldLeft(policy.DeterministicPolicy(randomWalk))((policy, state) => {
        state match {
          case tile: Tile =>
            policy.updateAction(StateAction(tile, Right))
        }
      })
  }
}
