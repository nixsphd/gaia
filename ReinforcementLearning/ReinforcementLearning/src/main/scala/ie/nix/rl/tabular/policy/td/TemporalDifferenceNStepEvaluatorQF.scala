package ie.nix.rl.tabular.policy.td

import ie.nix.rl.Environment
import ie.nix.rl.policy.{ActionValueFunction, Policy}
import ie.nix.rl.tabular.episodes.Episodes
import org.apache.logging.log4j.scala.Logging

abstract class TemporalDifferenceNStepEvaluatorQF[P <: Policy, QF <: ActionValueFunction](discount: Double, n: Int)(
    implicit environment: Environment with Episodes)
    extends TemporalDifferenceEvaluatorQF[P, QF](discount)
    with Logging {

  val useNToMakeItCompile = n

//  override def updateValueFunction(actionValueFunction: QF, transitionRewards: Vector[TransitionReward]): QF = {
//    val lastTimeStep = transitionRewards.size
//    val transitionRewardsMap = transitionRewards.zipWithIndex
//      .map(transitionRewardsAndIndex => (transitionRewardsAndIndex._2, transitionRewardsAndIndex._1))
//      .toMap
//    val timeSteps = transitionRewardsMap.keys + Range(transitionRewards.size)
//    val updatedValueFunction =
//      timeSteps.foldLeft(actionValueFunction)((actionValueFunction, timeStep) => {
//        val stateAction = transitionRewardsMap.get(timeStep).get.transition.stateAction
//        val target = getTarget(actionValueFunction, transitionRewardsMap, timeStep, lastTimeStep)
//        updateValueFunction(actionValueFunction, stateAction, target)
//      })
//    updatedValueFunction
//
//  }
//
//  def getTarget(actionValueFunction: QF,
//                transitionRewardsMap: Map[Int, TransitionReward],
//                timeStep: Int,
//                lastTimeStep: Int): Double = {
//
//    val timeStepToUpdate = timeStep - n + 1
//
//    if (timeStepToUpdate >= 0) {
//      val maxTimeStepToUpdate = math.min(timeStepToUpdate + n, lastTimeStep)
//      for (1 <- timeStepToUpdate + 1 to maxTimeStepToUpdate) yield {}
//    }
//
//    val transitionReward = transitionRewardsMap.get(timeStep).get
//    val state = transitionReward.transition.stateAction.state // S
//    val finalState = transitionReward.transition.finalState // S'
//    val finalStateActionValue = getStateActionValue(actionValueFunction, finalState)
//    val reward = transitionReward.reward
//    val target = reward + (discount * finalStateActionValue) // R + γV(S′)
//    logger.trace(
//      s"~state=$state, finalState=$finalState, finalStateActionValue=$finalStateActionValue, " +
//        s"reward=$reward, target=$target")
//    target
//
//  }

}
