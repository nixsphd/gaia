package ie.nix.rl.tabular.policy.vf

import ie.nix.rl.policy.Policy
import ie.nix.rl.{State, policy}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object ValueFunction extends Logging {

  val randomValue: () => Double = () => Random.nextDouble()

  def apply(policy: Policy): ValueFunction = {
    new ValueFunction(getValueMap[Double](policy)(randomValue))
  }

  def apply(policy: Policy, initialValue: Double): ValueFunction = {
    new ValueFunction(getValueMap[Double](policy)(() => initialValue))
  }

  def getValueMap[V](policy: Policy)(initialValue: () => V): Map[State, V] = {
    (for { state <- policy.getStates } yield {
      (state, initialValue())
    }).toMap
  }

  def difference(anValueFunction: AbstractValueFunction[State, _],
                 anotherValueFunction: AbstractValueFunction[State, _]): Double = {
    val states = (anValueFunction.valueMap.keySet ++ anotherValueFunction.valueMap.keySet).toVector
    val difference = (for { state <- states } yield {
      val aValue = anValueFunction.getValue(state)
      val otherValue = anotherValueFunction.getValue(state)
      math.abs(aValue - otherValue)
    }).sum
    difference
  }

}

class ValueFunction private[vf] (valueMap: Map[State, Double])
    extends AbstractValueFunction[State, Double](valueMap)
    with policy.ValueFunction
    with Logging {

  override def getStates: Vector[State] = valueMap.keySet.toVector

  override def updateValue(state: State, value: Double): ValueFunction =
    new ValueFunction(valueMap + (state -> value))

  override def mapValue(value: Double): Double = value

}

abstract class AbstractValueFunction[S, V] private[policy] (val valueMap: Map[S, V]) extends Logging {

  override def toString: String = {
    valueMap.toVector.sortBy(_._1.toString).toString()
  }

  def getStatesOrStateActions: Vector[S] = valueMap.keySet.toVector

  def getValue(stateOrStateAction: S): Double =
    valueMap.get(stateOrStateAction: S) match {
      case Some(value) => mapValue(value)
      case None        => 0d // assume state not stored in the map are terminal, value is always = 0
    }

  def mapValue(value: V): Double

}
