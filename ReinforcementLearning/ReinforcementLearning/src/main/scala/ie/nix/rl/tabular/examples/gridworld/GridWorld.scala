package ie.nix.rl.tabular.examples.gridworld

/*
a standard gridworld, with start and goal states, but with one difference: there is a crosswind upward through the
middle of the grid. The actions are the standard four—up, down, right, and left—but in the middle region the
resultant next states are shifted upward by a “wind,” the strength of which varies from column to column. The
 strength of the wind is given below each column, in number of cells shifted upward. For example, if you are one
 cell to the right of the goal, then the action left takes you to the cell just above the goal.
 */
class GridWorld
