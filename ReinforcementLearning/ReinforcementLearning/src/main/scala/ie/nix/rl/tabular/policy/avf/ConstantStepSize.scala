package ie.nix.rl.tabular.policy.avf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy
import ie.nix.rl.policy.Policy
import ie.nix.rl.tabular.policy.avf.ActionValueFunction.getValueMap
import ie.nix.rl.tabular.policy.vf.AbstractConstantStepSize
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import ie.nix.rl.tabular.policy.vf.ValueFunction.randomValue

object ConstantStepSize {

  def apply(policy: Policy, stepSize: StepSize): ConstantStepSize = {
    new ConstantStepSize(getValueMap[Double](policy)(randomValue), stepSize)
  }

  def apply(policy: Policy, initialValue: Double, stepSize: StepSize): ConstantStepSize = {
    new ConstantStepSize(getValueMap[Double](policy)(() => initialValue), stepSize)
  }

}

class ConstantStepSize private[policy] (valueMap: Map[StateAction, Double], stepSize: StepSize)
    extends AbstractConstantStepSize[StateAction](valueMap, stepSize)
    with policy.ActionValueFunction {

  override def getStateActions: Vector[StateAction] = valueMap.keySet.toVector

  override def updateValue(stateAction: StateAction, target: Double): ConstantStepSize = {
    new ConstantStepSize(valueMap + (stateAction -> newValue(stateAction, target)), stepSize)
  }

}
