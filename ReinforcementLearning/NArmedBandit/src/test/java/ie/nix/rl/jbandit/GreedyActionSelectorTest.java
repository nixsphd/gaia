package ie.nix.rl.jbandit;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class GreedyActionSelectorTest {

  private void testSelect(double[] estimates, int expectedAction) {
    GreedyActionSelector greedyActionSelector = new GreedyActionSelector();
    int actualAction = greedyActionSelector.select(estimates);
    Assertions.assertEquals(expectedAction, actualAction);
  }

  @Test
  void testSelect() {
    double[] estimates = {1, 2, 3};
    int expectedAction = 2;
    testSelect(estimates, expectedAction);
  }

  @Test
  void testSelectWithTie() {
    double[] estimates = {1, 3, 0, 4, 0, 4};
    int expectedAction = 3;
    testSelect(estimates, expectedAction);
  }

}