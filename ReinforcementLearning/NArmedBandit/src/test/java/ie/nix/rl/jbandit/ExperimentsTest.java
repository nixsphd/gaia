package ie.nix.rl.jbandit;

import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ExperimentsTest {

  @Test
  void testExperiments() {
    Experiments.main(new String[] {});

    assertLogFileExists(Experiments.ACTION_SELECTION_EXPERIMENT_LOG_FILE_NAME);
    assertLogFileExists(Experiments.ACTION_VALUE_EXPERIMENT_LOG_FILE_NAME);
    assertLogFileExists(Experiments.ACTION_SELECTION_EXPERIMENT_LOG_FILE_NAME);

  }

  private void assertLogFileExists(String logFileName) {
    Path logfilePath = FileSystems.getDefault().getPath(Experiments.RESULTS_DIR).resolve(
        logFileName);
    boolean actualLogfileExists = Files.exists(logfilePath);
    Assertions.assertTrue(actualLogfileExists, "The logfile " + logfilePath + " does not exist.");
  }
}