package ie.nix.rl.jbandit;

import static ie.nix.rl.jbandit.Experiments.RANDOM;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

// Allow random number generator seed to be set to a fixed value for reproducible testing.
@SuppressWarnings("squid:S4347")
public class GibbsSoftMaxActionSelectorTests {

  private static final Logger LOGGER =
      LogManager.getLogger();

  @Test
  public void testLastIsBest() {
    RANDOM.setSeed(1L);

    double temperature = 0.1;
    GibbsSoftMaxActionSelector selectorUnderTest =
        new GibbsSoftMaxActionSelector(temperature);
    double[] estimates = {0.1, 0.1, 0.1, 0.1, 0.5};
    int expectedArm = 4;
    int actualArm = selectorUnderTest.select(estimates);

    Assert.assertEquals(expectedArm, actualArm);
    LOGGER.info("~Done.");
  }

  @Test
  public void testFirstIsBest() {
    RANDOM.setSeed(1L);

    double temperature = 0.1;
    GibbsSoftMaxActionSelector selectorUnderTest =
        new GibbsSoftMaxActionSelector(temperature);
    double[] estimates = {0.5, 0.1, 0.1, 0.1, 0.1};
    int expectedArm = 0;
    int actualArm = selectorUnderTest.select(estimates);

    Assert.assertEquals(expectedArm, actualArm);
    LOGGER.info("~Done.");
  }

}
