package ie.nix.rl.bandit.player {

  import org.apache.logging.log4j.scala.Logging
  import org.junit.Assert.assertEquals
  import org.junit.runner.RunWith
  import org.scalatest.flatspec.AnyFlatSpec
  import org.scalatestplus.junit.JUnitRunner

  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  @RunWith(classOf[JUnitRunner])
  class GibbsSoftMaxActionSelectorTest extends AnyFlatSpec with Logging {

    behavior of "GibbsSoftMaxActionSelector"

    it should "select" in {

      GibbsSoftMaxActionSelector.randomNumberGenerator.setSeed(1L)

      val temperature       = 0.1
      val selectorUnderTest = new GibbsSoftMaxActionSelector(temperature)
      val estimates         = Array(0.1, 0.1, 0.1, 0.1, 0.5)
      val expectedArm       = 4
      val actualArm         = selectorUnderTest.select(estimates)

      assertEquals(expectedArm, actualArm)
      logger.trace("~Done.")

    }

  }

}
