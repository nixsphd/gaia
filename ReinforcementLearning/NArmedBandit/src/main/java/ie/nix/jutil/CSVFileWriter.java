package ie.nix.jutil;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.StringJoiner;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * CSVFileWriter helps write experiment data to CSV files.
 */
public class CSVFileWriter {

  private static final Logger LOGGER = LogManager.getLogger();

  protected static final String DOUBLE_FORMAT = "%.2f";

  protected final String logFile;

  public CSVFileWriter(String logDirName, String logFileName) {
    Path logDirPath = FileSystems.getDefault().getPath(logDirName);
    if (!Files.exists(logDirPath)) {
      try {
        Files.createDirectories(logDirPath);
        LOGGER.trace("~Created {}", logDirPath);
      } catch (IOException e) {
        LOGGER.error("~Error creating logfile path {}", logDirPath, e);
      }
    }
    this.logFile = logDirPath.resolve(logFileName).toString();

    truncateLogFile();

    LOGGER.info("~Logging to {}", logFile);
  }

  protected void truncateLogFile() {
    try {
      Files.write(Paths.get(logFile), "".getBytes(),
          StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    } catch (IOException e) {
      LOGGER.error("~Error truncating logfile path {}", logFile, e);
    }
    LOGGER.debug("~Truncated {}", logFile);
  }

  public void writeHeaders(String headersString) {
    try {
      Files.write(Paths.get(logFile), (headersString + '\n').getBytes(),
          StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    } catch (IOException e) {
      LOGGER.error("~Error writing headers to logfile {}", logFile, e);
    }
    LOGGER.debug("~{}", headersString);

  }

  public void writeHeaders(Stream<Object> headersList) {
    writeHeaders(generateCSVString(headersList));
  }

  public void writeHeaders(Object... headers) {
    writeHeaders(Arrays.stream(headers));
  }

  public void writeRow(String rowString) {
    try {
      Files.write(Paths.get(logFile), (rowString + '\n').getBytes(),
          StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    } catch (IOException e) {
      LOGGER.error("~Error writing row to logfile {}", logFile, e);
    }
    LOGGER.debug("~{}", rowString);

  }

  public void writeRow(Object... rowItems) {
    writeRow(Arrays.stream(rowItems));
  }

  public void writeRow(Stream<Object> rowItemsList) {
    String rowString = generateCSVString(rowItemsList);
    writeRow(rowString);
  }

  protected String generateCSVString(Stream<Object> row) {
    StringJoiner rowString = new StringJoiner(",", "", "");
    row.forEachOrdered(entry -> {
      if (entry instanceof Double) {
        rowString.add(format((Double) entry));
      } else {
        rowString.add(entry.toString());
      }
    });
    return rowString.toString();
  }

  protected String format(double value) {
    return String.format(DOUBLE_FORMAT, value);
  }
}