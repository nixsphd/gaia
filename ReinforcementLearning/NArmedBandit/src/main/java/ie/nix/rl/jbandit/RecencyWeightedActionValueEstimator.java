package ie.nix.rl.jbandit;

public class RecencyWeightedActionValueEstimator extends AverageActionValueEstimator {

  protected final double alpha;

  public RecencyWeightedActionValueEstimator(int numberOfEstimates, double initialEstimate,
                                             double alpha) {
    super(numberOfEstimates, initialEstimate);
    this.alpha = alpha;
  }

  @Override
  public void updateEstimates(int arm, double reward) {
    numberOfEstimater[arm] += 1;
    estimates[arm] += (alpha) * (reward - estimates[arm]);
  }

  @Override
  public String toString() {
    return "Recency Weighted (alpha=" + alpha + ")";
  }

}