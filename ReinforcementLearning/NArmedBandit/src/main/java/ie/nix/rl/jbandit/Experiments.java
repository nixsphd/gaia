package ie.nix.rl.jbandit;

import ie.nix.jutil.CSVFileWriter;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Experiments {

  public static final Random RANDOM = new Random();

  private static final Logger LOGGER = LogManager.getLogger();

  protected static final int NUMBER_OF_ARMS = 10;
  protected static final double INITIAL_VALUE = 0.0;
  protected static final int NUMBER_OF_TIME_STEPS = 1000;
  protected static final String RESULTS_DIR = "results";
  protected static final String ACTION_SELECTION_EXPERIMENT_LOG_FILE_NAME = "ActionSelection.csv";
  protected static final String ACTION_VALUE_EXPERIMENT_LOG_FILE_NAME = "ActiomValue.csv";
  protected static final String OPTIMISTIC_INITIAL_VALUE_EXPERIMENT_LOG_FILE_NAME =
      "OptimisticInitialValues.csv";

  private static final Object[] HEADERS =
      new Object[] {"TimeStep", "AverageReward", "ActionSelection", "ActionValue", "InitialValue"};


  @SuppressWarnings("squid:S4347") // Allow seed to be set to a fixed value.
  public static void main(String[] args) {
    RANDOM.setSeed(1L);
    runActionSelectionExperiment();
    runActionValueEstimatorExperiment();
    runInitialValuesExperiment();

    LOGGER.info("~Done.");

  }

  /**
   * Rus the Actions Selection experiment.
   */
  public static void runActionSelectionExperiment() {

    CSVFileWriter csvFileWriter = new CSVFileWriter(RESULTS_DIR,
        ACTION_SELECTION_EXPERIMENT_LOG_FILE_NAME);
    csvFileWriter.writeHeaders(HEADERS);

    Player player = new Player(new GreedyActionSelector(),

        new AverageActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE));
    runExperiment(player, csvFileWriter);

    player = new Player(new EGreedyActionSelector(0.01),
        new AverageActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE));
    runExperiment(player, csvFileWriter);

    player = new Player(new EGreedyActionSelector(0.1),
        new AverageActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE));
    runExperiment(player, csvFileWriter);

    player = new Player(new GibbsSoftMaxActionSelector(0.05),
        new AverageActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE));
    runExperiment(player, csvFileWriter);

    player = new Player(new GibbsSoftMaxActionSelector(0.1),
        new AverageActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE));
    runExperiment(player, csvFileWriter);

    player = new Player(new GibbsSoftMaxActionSelector(0.5),
        new AverageActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE));
    runExperiment(player, csvFileWriter);
  }

  /**
   * Runs the Action Value experiment.
   */
  public static void runActionValueEstimatorExperiment() {

    CSVFileWriter csvFileWriter =
        new CSVFileWriter(RESULTS_DIR, ACTION_VALUE_EXPERIMENT_LOG_FILE_NAME);
    csvFileWriter.writeHeaders(HEADERS);

    Player player = new Player(new GreedyActionSelector(),
        new AverageActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE));
    runExperiment(player, csvFileWriter);

    double alpha = 0.1;
    player = new Player(new GreedyActionSelector(),
        new RecencyWeightedActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE, alpha));
    runExperiment(player, csvFileWriter);

    alpha = 0.5;
    player = new Player(new GreedyActionSelector(),
        new RecencyWeightedActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE, alpha));
    runExperiment(player, csvFileWriter);

    alpha = 1.0;
    player = new Player(new GreedyActionSelector(),
        new RecencyWeightedActionValueEstimator(NUMBER_OF_ARMS, INITIAL_VALUE, alpha));
    runExperiment(player, csvFileWriter);
  }

  /**
   * Runs the optimistic initial value experiment.
   */
  public static void runInitialValuesExperiment() {

    CSVFileWriter csvFileWriter =
        new CSVFileWriter(RESULTS_DIR, OPTIMISTIC_INITIAL_VALUE_EXPERIMENT_LOG_FILE_NAME);
    csvFileWriter.writeHeaders(HEADERS);

    Player player = new Player(new GreedyActionSelector(),
        new RecencyWeightedActionValueEstimator(NUMBER_OF_ARMS, 0.0, 0.1));
    runExperiment(player, csvFileWriter);

    player = new Player(new GreedyActionSelector(),
        new RecencyWeightedActionValueEstimator(NUMBER_OF_ARMS, 5.0, 0.1));
    runExperiment(player, csvFileWriter);

  }

  private static void runExperiment(Player player, CSVFileWriter csvFileWriter) {

    double[] accumulativeRewardsForTimeSteps = new double[NUMBER_OF_TIME_STEPS];

    int numberOfRuns = 2000;
    for (int b = 0; b < numberOfRuns; b++) {
      player.actionValueEstimator.reset();
      NArmedBandit bandit = new NArmedBandit(NUMBER_OF_ARMS);

      for (int t = 0; t < NUMBER_OF_TIME_STEPS; t++) {
        double reward = player.playAndUpdateActionValueEstimates(bandit);
        accumulativeRewardsForTimeSteps[t] += reward;
      }
    }

    for (int t = 0; t < NUMBER_OF_TIME_STEPS; t++) {
      csvFileWriter
          .writeRow(t, accumulativeRewardsForTimeSteps[t] / numberOfRuns, player.actionSelector,
              player.actionValueEstimator, INITIAL_VALUE);
    }

  }

}
