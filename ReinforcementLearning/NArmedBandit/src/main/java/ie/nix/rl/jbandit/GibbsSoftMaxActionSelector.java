package ie.nix.rl.jbandit;

import static ie.nix.rl.jbandit.Experiments.RANDOM;

import ie.nix.rl.jbandit.Player.ActionSelector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class GibbsSoftMaxActionSelector implements ActionSelector {

  private static final Logger LOGGER =
      LogManager.getLogger();

  protected final double temperature;

  public GibbsSoftMaxActionSelector(double temperature) {
    this.temperature = temperature;
  }

  public int select(double[] estimates) {
    double[] softMaxDistributions = getSoftMaxDistributions(estimates);
    double selectProbability = RANDOM.nextDouble();
    LOGGER.debug("~softMaxDistributions={}, selectProbability={}",
        softMaxDistributions, selectProbability);
    return selectArmWithProbability(softMaxDistributions, selectProbability);

  }

  public int selectArmWithProbability(double[] softMaxDistributions, double selectProbability) {

    LOGGER.debug("~selectProbability={}", selectProbability);
    int arm = 0;
    double accumulativeProbability = softMaxDistributions[arm];
    while (accumulativeProbability < selectProbability) {
      LOGGER.debug("~accumulativeProbability={}", accumulativeProbability);
      arm++;
      accumulativeProbability += softMaxDistributions[arm];
    }
    return arm;
  }

  @SuppressWarnings("squid:S3518")
  public double[] getSoftMaxDistributions(double[] estimates) {

    LOGGER.debug("~temperature={}", temperature);
    double[] softMaxDistributions = new double[estimates.length];
    for (int i = 0; i < softMaxDistributions.length; i++) {
      softMaxDistributions[i] = Math.exp(estimates[i] / temperature);
    }

    double totalSoftMaxDistribution = 0;
    for (double softMaxDistribution : softMaxDistributions) {
      totalSoftMaxDistribution += softMaxDistribution;
    }

    for (int i = 0; i < softMaxDistributions.length; i++) {
      softMaxDistributions[i] /= totalSoftMaxDistribution;
    }
    LOGGER.debug("~softMaxDistributions={}", softMaxDistributions);
    return softMaxDistributions;
  }

  @Override
  public String toString() {
    return "GibbsSoftMax (t=" + temperature + ")";
  }

}