package ie.nix.rl.jbandit;

import ie.nix.rl.jbandit.Player.ActionSelector;

public class GreedyActionSelector implements ActionSelector {

  public int select(double[] estimates) {
    int maxIndex = 0;
    double max = estimates[maxIndex];
    for (int i = 1; i < estimates.length; i++) {
      if (estimates[i] > max) {
        max = estimates[i];
        maxIndex = i;
      }
    }
    return maxIndex;
  }

  @Override
  public String toString() {
    return "Greedy";
  }

}