package ie.nix.rl.jbandit;

import static ie.nix.rl.jbandit.Experiments.RANDOM;

public class EGreedyActionSelector extends GreedyActionSelector {

  protected final double probabilityOfRandomAction;

  public EGreedyActionSelector(double probabilityOfRandomAction) {
    this.probabilityOfRandomAction = probabilityOfRandomAction;
  }

  @Override
  public int select(double[] estimates) {
    int greedyChoice = super.select(estimates);
    if (RANDOM.nextDouble() < probabilityOfRandomAction) {
      int egreedyChoice = RANDOM.nextInt(estimates.length - 1);
      if (egreedyChoice < greedyChoice) {
        return egreedyChoice;

      } else {
        return egreedyChoice + 1;

      }
    } else {
      return greedyChoice;
    }
  }

  @Override
  public String toString() {
    return "e-Greedy (e=" + probabilityOfRandomAction + ")";
  }

}