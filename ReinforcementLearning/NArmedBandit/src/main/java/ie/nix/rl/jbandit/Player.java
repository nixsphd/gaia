package ie.nix.rl.jbandit;

/**
 * The player who is learning how to play best against the n-armed bandit.
 */
public class Player {

  public interface ActionSelector {

    int select(double[] estimates);

  }

  public interface ActionValueEstimator {

    void reset();

    double[] getEstimates();

    void updateEstimates(int arm, double reward);

  }

  protected final ActionSelector actionSelector;
  protected final ActionValueEstimator actionValueEstimator;

  public Player(ActionSelector actionSelector, ActionValueEstimator actionValueEstimator) {
    this.actionSelector = actionSelector;
    this.actionValueEstimator = actionValueEstimator;
  }

  public double playAndUpdateActionValueEstimates(NArmedBandit bandit) {
    int arm = actionSelector.select(actionValueEstimator.getEstimates());
    double reward = bandit.getRewardForArm(arm);
    actionValueEstimator.updateEstimates(arm, reward);
    return reward;
  }

}
