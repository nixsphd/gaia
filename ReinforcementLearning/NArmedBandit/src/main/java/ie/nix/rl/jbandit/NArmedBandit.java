package ie.nix.rl.jbandit;

import static ie.nix.rl.jbandit.Experiments.RANDOM;

public class NArmedBandit {

  protected final double[] meanReward;

  public NArmedBandit(int numberOfArms) {
    this.meanReward = new double[numberOfArms];
    initMeanRewards();
  }

  protected void initMeanRewards() {
    for (int arm = 0; arm < meanReward.length; arm++) {
      meanReward[arm] = RANDOM.nextGaussian();
    }

  }

  public double getRewardForArm(int arm) {
    return meanReward[arm] + RANDOM.nextGaussian();
  }

}
