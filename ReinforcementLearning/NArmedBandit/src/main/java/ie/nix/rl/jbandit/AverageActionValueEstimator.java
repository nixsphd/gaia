package ie.nix.rl.jbandit;

import ie.nix.rl.jbandit.Player.ActionValueEstimator;

public class AverageActionValueEstimator implements ActionValueEstimator {

  protected final double initialEstimate;
  protected final double[] estimates;
  protected final int[] numberOfEstimater;

  public AverageActionValueEstimator(int numberOfEstimates, double initialEstimate) {
    this.initialEstimate = initialEstimate;
    this.estimates = new double[numberOfEstimates];
    this.numberOfEstimater = new int[numberOfEstimates];

    reset();

  }

  public void reset() {
    for (int arm = 0; arm < estimates.length; arm++) {
      estimates[arm] = initialEstimate;
      numberOfEstimater[arm] = 0;
    }

  }

  public double[] getEstimates() {
    return estimates;
  }

  public void updateEstimates(int arm, double reward) {
    numberOfEstimater[arm] += 1;
    estimates[arm] += (reward - estimates[arm]) / numberOfEstimater[arm];
  }


  @Override
  public String toString() {
    return "Sample Average";
  }

}