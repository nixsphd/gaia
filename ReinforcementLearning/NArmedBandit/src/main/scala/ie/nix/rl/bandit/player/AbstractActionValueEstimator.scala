package ie.nix.rl.bandit.player {

  import ie.nix.rl.bandit.ActionValueEstimator

  abstract class AbstractActionValueEstimator(
      val numberOfEstimates: Int,
      val initialEstimate: Double
  ) extends ActionValueEstimator {

    protected val estimates: Array[Double] =
      new Array[Double](numberOfEstimates)
    protected val k: Array[Int] = new Array[Int](numberOfEstimates)

    override def getEstimates: Array[Double] = estimates

  }

}
