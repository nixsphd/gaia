package ie.nix.rl.bandit.player {

  import ie.nix.rl.bandit.ActionSelector

  case class GreedyActionSelector() extends ActionSelector {

    override def toString: String = "Greedy"

    override def select(estimates: Array[Double]): Int =
      estimates.indexOf(estimates.max)

  }

}
