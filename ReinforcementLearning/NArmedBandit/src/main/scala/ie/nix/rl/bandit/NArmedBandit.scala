package ie.nix.rl.bandit {

  import ie.nix.rl.bandit.NArmedBandit.randomNumberGenerator

  import scala.util.Random

  case class NArmedBandit(numberOfArms: Int) {

    private val meanReward: Array[Double] =
      Array.fill(numberOfArms) {
        randomNumberGenerator.nextGaussian
      }

    def getRewardForArm(arm: Int): Double =
      meanReward(arm) + randomNumberGenerator.nextGaussian

  }

  object NArmedBandit {
    val randomNumberGenerator = new Random
    randomNumberGenerator.setSeed(1)
  }

}
