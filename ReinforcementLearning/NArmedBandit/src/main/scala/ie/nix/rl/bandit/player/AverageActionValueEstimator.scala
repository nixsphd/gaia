package ie.nix.rl.bandit.player {

  case class AverageActionValueEstimator(
      override val numberOfEstimates: Int,
      override val initialEstimate: Double
  ) extends AbstractActionValueEstimator(numberOfEstimates, initialEstimate) {

    override def updateEstimates(arm: Int, reward: Double): Unit = {
      k(arm) += 1
      estimates(arm) += (reward - estimates(arm)) / k(arm)
    }

    override def toString: String = "Sample Average"
  }

}
