package ie.nix.rl.bandit.player {

  case class RecencyWeightedActionValueEstimator(
      override val numberOfEstimates: Int,
      override val initialEstimate: Double,
      alpha: Double
  ) extends AbstractActionValueEstimator(numberOfEstimates, initialEstimate) {

    override def toString: String = s"Recency Weighted (alpha=$alpha)"

    override def updateEstimates(arm: Int, reward: Double): Unit = {
      k(arm) += 1
      estimates(arm) += alpha * (reward - estimates(arm))
    }
  }

}
