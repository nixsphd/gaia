package ie.nix.rl.bandit {

  trait ActionSelector {

    def select(estimates: Array[Double]): Int

  }

  trait ActionValueEstimator {

    val initialEstimate: Double

    def getEstimates: Array[Double]

    def updateEstimates(arm: Int, reward: Double): Unit
  }

  case class Player(
      actionSelector: ActionSelector,
      actionValueEstimator: ActionValueEstimator
  ) {

    def playAndUpdateActionValueEstimates(bandit: NArmedBandit): Double = {
      val arm    = actionSelector.select(actionValueEstimator.getEstimates)
      val reward = bandit.getRewardForArm(arm)
      actionValueEstimator.updateEstimates(arm, reward)
      reward
    }

  }

}
