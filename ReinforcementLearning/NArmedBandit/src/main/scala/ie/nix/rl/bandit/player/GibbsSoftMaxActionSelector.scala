package ie.nix.rl.bandit.player {

  import ie.nix.rl.bandit.ActionSelector
  import ie.nix.rl.bandit.player.GibbsSoftMaxActionSelector.randomNumberGenerator
  import org.apache.logging.log4j.scala.Logging

  import scala.annotation.tailrec
  import scala.util.Random

  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  case class GibbsSoftMaxActionSelector(temperature: Double)
      extends ActionSelector
      with Logging {

    override def toString: String = s"GibbsSoftMax (t=$temperature)"

    override def select(estimates: Array[Double]): Int = {
      val softMaxDistributions = getSoftMaxDistributions(estimates)
      logger.debug(
        s"~softMaxDistributions=${softMaxDistributions.mkString("Array(", ", ", ")")}")
      getArmWithProbability(
        softMaxDistributions,
        randomNumberGenerator.nextDouble
      )
    }

    def getArmWithProbability(
        softMaxDistributions: Array[Double],
        selectProbability: Double
    ): Int = {
      logger.debug(s"~selectProbability=$selectProbability")

      @tailrec
      def findArmWithProbability(
          distributionsAndArms: List[(Double, Int)],
          accumulativeDistribution: Double,
          selectProbability: Double
      ): Int = {
        distributionsAndArms match {
          case (_, arm) :: Nil => arm
          case (distribution, arm) :: _
              if accumulativeDistribution + distribution > selectProbability =>
            arm
          case (distribution, _) :: tail =>
            findArmWithProbability(
              tail,
              accumulativeDistribution + distribution,
              selectProbability
            )
        }
      }

      findArmWithProbability(
        softMaxDistributions.zipWithIndex.toList,
        0,
        selectProbability
      )

    }

    def getSoftMaxDistributions(estimates: Array[Double]): Array[Double] = {

      val unNormalizedSoftMaxDistribution =
        estimates.map(estimate => Math.exp(estimate / temperature))
      logger.debug(
        s"~unNormalizedSoftMaxDistribution=${unNormalizedSoftMaxDistribution
          .mkString("Array(", ", ", ")")}")

      val totalSoftMaxDistribution: Double = unNormalizedSoftMaxDistribution.sum
      logger.debug(s"~totalSoftMaxDistribution=$totalSoftMaxDistribution")

      unNormalizedSoftMaxDistribution.map(element =>
        element / totalSoftMaxDistribution)
    }
  }

  object GibbsSoftMaxActionSelector {
    val randomNumberGenerator = new Random
    randomNumberGenerator.setSeed(1L)
  }

}
