package ie.nix.rl.bandit.player {

  import ie.nix.rl.bandit.ActionSelector
  import ie.nix.rl.bandit.player.EGreedyActionSelector.randomNumberGenerator

  import scala.util.Random

  case class EGreedyActionSelector(e: Double) extends ActionSelector {

    override def toString: String = s"e-Greedy (e=$e)"

    override def select(estimates: Array[Double]): Int = {
      val greedyChoice = estimates.indexOf(estimates.max)
      if (randomNumberGenerator.nextDouble < e) {
        val eGreedyChoice = randomNumberGenerator.nextInt(estimates.length - 1)
        if (eGreedyChoice < greedyChoice) eGreedyChoice
        else eGreedyChoice + 1
      } else greedyChoice
    }

  }

  object EGreedyActionSelector {
    val randomNumberGenerator = new Random
    randomNumberGenerator.setSeed(1)
  }

}
