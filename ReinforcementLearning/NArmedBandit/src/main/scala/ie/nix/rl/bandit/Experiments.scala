package ie.nix.rl.bandit {

  import ie.nix.rl.bandit.player._
  import ie.nix.util.CSVFileWriter
  import org.apache.logging.log4j.scala.Logging

  case class Experiment(
      playerPrototype: Player,
      banditPrototype: NArmedBandit,
      numberOfRuns: Int,
      numberOfTimeSteps: Int
  ) extends Logging {

    def runAndWriteRewards(csvFileWriter: CSVFileWriter): Unit = {
      writeRewards(run(), csvFileWriter)
    }

    def run(): Array[Double] = {

      val accumulativeRewardsForTimeSteps: Array[Double] =
        Array.fill(numberOfTimeSteps) {
          0d
        }

      // Smell - This could be fine better I'm sure, using yield some how,
      // and maybe using only 1 for loop?
      for (_ <- 0 until numberOfRuns) {
        val player: Player = playerPrototype.copy()
        val bandit         = banditPrototype.copy()
        for (t <- 0 until numberOfTimeSteps) {
          accumulativeRewardsForTimeSteps(t) +=
            player.playAndUpdateActionValueEstimates(bandit)
        }
      }

      accumulativeRewardsForTimeSteps.map(_ / numberOfRuns)
    }

    def writeRewards(
        averageRewardsForTimeSteps: Array[Double],
        csvFileWriter: CSVFileWriter
    ): Unit = {

      for ((timeStep, averageRewardsForTimeStep) <- averageRewardsForTimeSteps.zipWithIndex) {
        csvFileWriter.writeRow(
          timeStep,
          averageRewardsForTimeStep,
          playerPrototype.actionSelector,
          playerPrototype.actionValueEstimator,
          playerPrototype.actionValueEstimator.initialEstimate
        )
      }

    }

  }

  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  object Experiments extends App with Logging {

    ActionSelectionExperiment.main(Array(""))
    ActionValueEstimatorExperiment.main(Array(""))
    InitialValuesExperiment.main(Array(""))
    logger.info("~Done")

  }

  object ActionSelectionExperiment extends App {

    val csvFileWriter: CSVFileWriter =
      CSVFileWriter("results", "ActionSelection.csv")
    csvFileWriter.writeHeaders(
      "TimeStep",
      "AverageReward",
      "ActionSelection",
      "ActionValue",
      "InitialValue"
    )

    Experiment(
      playerPrototype =
        Player(GreedyActionSelector(), AverageActionValueEstimator(10, 0.0)),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        EGreedyActionSelector(0.01),
        AverageActionValueEstimator(numberOfEstimates = 10,
                                    initialEstimate = 0.0)
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        EGreedyActionSelector(0.1),
        AverageActionValueEstimator(numberOfEstimates = 10,
                                    initialEstimate = 0.0)
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        GibbsSoftMaxActionSelector(temperature = 0.05),
        AverageActionValueEstimator(numberOfEstimates = 10,
                                    initialEstimate = 0.0)
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        GibbsSoftMaxActionSelector(temperature = 0.1),
        AverageActionValueEstimator(numberOfEstimates = 10,
                                    initialEstimate = 0.0)
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        GibbsSoftMaxActionSelector(temperature = 0.5),
        AverageActionValueEstimator(numberOfEstimates = 10,
                                    initialEstimate = 0.0)
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

  }

  object ActionValueEstimatorExperiment extends App {

    val csvFileWriter: CSVFileWriter =
      CSVFileWriter("results", "ActionSelection.csv")
    csvFileWriter.writeHeaders(
      "TimeStep",
      "AverageReward",
      "ActionSelection",
      "ActionValue",
      "InitialValue"
    )

    Experiment(
      playerPrototype = Player(
        GreedyActionSelector(),
        AverageActionValueEstimator(numberOfEstimates = 10,
                                    initialEstimate = 0.0)
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        GreedyActionSelector(),
        RecencyWeightedActionValueEstimator(
          numberOfEstimates = 10,
          initialEstimate = 0.0,
          alpha = 0.1
        )
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        GreedyActionSelector(),
        RecencyWeightedActionValueEstimator(
          numberOfEstimates = 10,
          initialEstimate = 0.0,
          alpha = 0.5
        )
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        GreedyActionSelector(),
        RecencyWeightedActionValueEstimator(
          numberOfEstimates = 10,
          initialEstimate = 0.0,
          alpha = 1.0
        )
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

  }

  object InitialValuesExperiment extends App {

    val csvFileWriter: CSVFileWriter =
      CSVFileWriter("results", "OptimisticInitialValues.csv")
    csvFileWriter.writeHeaders(
      "TimeStep",
      "AverageReward",
      "ActionSelection",
      "ActionValue",
      "InitialValue"
    )

    Experiment(
      playerPrototype = Player(
        GreedyActionSelector(),
        RecencyWeightedActionValueEstimator(
          numberOfEstimates = 10,
          initialEstimate = 0.0,
          alpha = 0.1
        )
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)

    Experiment(
      playerPrototype = Player(
        GreedyActionSelector(),
        RecencyWeightedActionValueEstimator(
          numberOfEstimates = 10,
          initialEstimate = 5.0,
          alpha = 0.1
        )
      ),
      banditPrototype = NArmedBandit(10),
      numberOfRuns = 1000,
      numberOfTimeSteps = 1000
    ).runAndWriteRewards(csvFileWriter)
  }

}
