package ie.nix.peersim

import ie.nix.peersim.Protocol.Message
import peersim.core.CommonState

class TestProtocol(override val prefix: String) extends Protocol(prefix) {

  logger trace s"[${CommonState.getTime}] prefix=$prefix"

  override def toString = if (Option(getNode).isDefined) { s"TestProtocol-$getNode" } else { "TestProtocol-?" }

  def nextCycle(node: Node): Unit = {
    logger trace s"[${CommonState.getTime}] $this"
  }

  def processEvent(node: Node, message: Message): Unit = {
    logger trace s"[${CommonState.getTime}] $this"
  }

}

object TestProtocol {

  class TestMessage() {
    override def toString: String = "TestMessage"
  }

}
