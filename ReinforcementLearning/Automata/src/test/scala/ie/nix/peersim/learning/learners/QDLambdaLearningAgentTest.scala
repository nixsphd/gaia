package ie.nix.peersim.learning.learners

import ie.nix.peersim.Utils.{getProtocolAs, getProtocolsAs, node}
import ie.nix.peersim.learning.TestProtocolWithLearner
import ie.nix.peersim.learning.TestProtocolWithLearner.{TestAction1, TestAction2, TestState}
import ie.nix.rl.Environment.StateAction
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, PrivateMethodTester}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class QDLambdaLearningAgentTest
    extends AnyFlatSpec
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with PrivateMethodTester
    with Logging {

  override def beforeAll(): Unit = initPeerSim

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "QDLearningAgent"

  it should "agent of right type" in {
    val testLearningProtocols = getProtocolsAs[TestProtocolWithLearner](testProtocolID)

    assert(testLearningProtocols.count(testLearningProtocol =>
      testLearningProtocol.learner.agent.isInstanceOf[QDLambdaLearningAgent]) === numberOfNodesFromConfig)
  }

  it should "learning protocol index matched node index" in {
    val testLearningProtocols = getProtocolsAs[TestProtocolWithLearner](testProtocolID)

    assert(testLearningProtocols.count(testLearningProtocol =>
      testLearningProtocol.getIndex === testLearningProtocol.getNode().getIndex) === numberOfNodesFromConfig)
  }

  it should "there only one environment" in {
    val testLearningProtocols = getProtocolsAs[TestProtocolWithLearner](testProtocolID)
    val environment = testLearningProtocols(0).learner.agent.environment

    assert(
      testLearningProtocols
        .count(testLearningProtocol => testLearningProtocol.learner.agent.environment eq environment) === numberOfNodesFromConfig)
  }

  it should "getMaxStateActionValue" in {
    learner0 invokePrivate updateAgent(
      agent0.copy(
        actionValueFunction = agent0.actionValueFunction
          .updateValueAndEligibilityTrace(stateAction01, 0.2, 0.64)
          .updateValueAndEligibilityTrace(stateAction02, 0.4, 0.8)))
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"

    val maxStateActionValue = agent0 invokePrivate getMaxStateActionValue(stateAction01)
    logger info s"maxStateActionValue=$maxStateActionValue"

    assert(maxStateActionValue === 0.4)

  }

  it should "getInnovation" in {
    val innovation = agent0 invokePrivate getInnovation(0.2, 1d, 0.4)
    logger info s"innovation=$innovation"

    assert(innovation === 1.2)
  }

  it should "getConsensus" in {
    val neighboursValues: Vector[Double] = Vector(0.4, 0.4, 0.4)
    val consensus = agent0 invokePrivate getConsensus(0.2, neighboursValues)
    logger info s"consensus=$consensus"

    assert(consensus === -0.6 +- 0.0001)
  }

  it should "learn updates maybePreviousStateAction when None" in {
    learner0 invokePrivate resetPreviousStateAction()

    agent0.getStateAction(state1)

    val actualMaybePreviousStateAction = learner0 invokePrivate maybePreviousStateAction()
    logger info s"actualMaybePreviousStateAction=$actualMaybePreviousStateAction"

    assert(actualMaybePreviousStateAction === None)
  }

  it should "learn qdLambda for greedy action" in {
    learner0 invokePrivate updatePreviousStateAction(stateAction01)
    learner0 invokePrivate updateAgent(
      agent0.copy(
        actionValueFunction = agent0.actionValueFunction
          .updateValueAndEligibilityTrace(stateAction01, 0.2, 0d)
          .updateValueAndEligibilityTrace(stateAction11, 0.4, 0d)
          .updateValueAndEligibilityTrace(stateAction12, 0.1, 0d)))
    logger info s"qdLambda=${agent0.actionValueFunction}"

    val updatedAgent = learner0 invokePrivate learn(stateAction11)

    logger info s"eligibilityTraces=${updatedAgent.actionValueFunction}"
    val (newValue, newEligibility) = updatedAgent.actionValueFunction.getValueAndEligibility(stateAction01)
    logger info s"newValue=$newValue, newEligibility=$newEligibility"

    assert(newValue === 0.204 +- 0.0001)
    assert(newEligibility === eligibilityDecayFromConfig +- 0.0001)
  }

  private def initPeerSim(): Unit = {
    logger info s"Start"
    val TEST_PROPERTIES = "src/test/resources/TestProtocolWithQDLambdaLearning.properties"
    val testProperties = Array[String]("-file", TEST_PROPERTIES)
    peersim.Simulator.main(testProperties)
    logger info s"End"
  }

  def numberOfNodesFromConfig = peersim.config.Configuration.getInt("network.size")

  def eligibilityDecayFromConfig =
    peersim.config.Configuration.getDouble("protocol.learner.agent.eligibility_decay")

  def innovationStepSizeFromConfig =
    peersim.config.Configuration.getDouble("protocol.learner.agent.innovation_step_size")

  def state0: TestState = TestState(0)

  def state1: TestState = TestState(1)

  def stateAction01: StateAction = StateAction(state0, TestAction1)

  def stateAction02: StateAction = StateAction(state0, TestAction2)

  def stateAction11: StateAction = StateAction(state1, TestAction1)

  def stateAction12: StateAction = StateAction(state1, TestAction2)

  def node0 = node(index = 0)

  def testProtocolID = 2

  def learner0 = as[NetworkedLearner](getProtocolAs[TestProtocolWithLearner](node0, testProtocolID).learner)

  def agent0: QDLambdaLearningAgent = as[QDLambdaLearningAgent](learner0.agent)

  // Agent
  val getInnovation = PrivateMethod[Double]('getInnovation)
  val getConsensus = PrivateMethod[Double]('getConsensus)
  val getTarget = PrivateMethod[Double]('getTarget)
  val getMaxStateActionValue = PrivateMethod[Double]('getMaxStateActionValue)
  val updateAgent = PrivateMethod[Unit]('updateAgent)
  val maybePreviousStateAction = PrivateMethod[Option[StateAction]]('maybePreviousStateAction)
  val updatePreviousStateAction = PrivateMethod[Unit]('updatePreviousStateAction)
  val resetPreviousStateAction = PrivateMethod[Unit]('resetPreviousStateAction)
  val learn = PrivateMethod[QDLambdaLearningAgent]('learn)
  val clearEligibilityTraces = PrivateMethod[Unit]('clearEligibilityTraces)

}
