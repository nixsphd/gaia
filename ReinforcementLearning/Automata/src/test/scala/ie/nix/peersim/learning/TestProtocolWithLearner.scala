package ie.nix.peersim.learning

import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.LEARNER
import ie.nix.peersim.Protocol.Message
import ie.nix.peersim.Utils.getProtocolAs
import ie.nix.peersim.learning.learners.IndependentLearner
import ie.nix.peersim.{Node, TestProtocol}
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

class TestProtocolWithLearner(prefix: String) extends TestProtocol(prefix) with Logging {

  logger trace s"[${CommonState.getTime}] prefix=$prefix"

  private val learningProtocolID = Configuration.getPid(s"$prefix.$LEARNER")
  logger trace s"[${CommonState.getTime}] protocolID=$protocolID learningProtocolID=$learningProtocolID"

  def learner: Learner =
    getProtocolAs[IndependentLearner](getNode(), learningProtocolID)

  override def toString: String =
    if (Option(getNode()).isDefined) { s"TestLearningProtocol-$getNode()" } else { "TestLearningProtocol-?" }

  override def nextCycle(node: Node): Unit = {
    logger trace s"[${CommonState.getTime}] $this"
  }

  override def processEvent(node: Node, message: Message): Unit = {
    logger trace s"[${CommonState.getTime}] $this"
  }

}

object TestProtocolWithLearner {

  case class TestState(value: Int) extends State {
    override def toString: String = s"TestState($value)"
    override def isTerminal: Boolean = false
  }

  case object TestAction1 extends Action {
    override def toString: String = "TestAction1"
  }
  case object TestAction2 extends Action {
    override def toString: String = "TestAction2"
  }

}
