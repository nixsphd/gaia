package ie.nix.peersim

import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

class TestNode(override val prefix: String) extends Node(prefix) with Logging {

  logger trace s"[${CommonState.getTime}] prefix=$prefix => $this"

  override def toString = s"TestNode-$getID"

}

object TestNode {

  implicit val toTestNode: peersim.core.Node => TestNode = node => as[TestNode](node)

}
