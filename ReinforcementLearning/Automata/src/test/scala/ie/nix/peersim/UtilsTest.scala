package ie.nix.peersim

import ie.nix.peersim.Utils._
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class UtilsTest extends AnyFlatSpec with BeforeAndAfterEach with BeforeAndAfterAll with Logging {

  import ie.nix.peersim.TestNode.toTestNode

  override def beforeAll(): Unit = initPeerSim

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "PeerSimUtilsTest"

  it should "numberOfNodes" in {
    val numberOfTestNodes = numberOfNodes
    assert(numberOfTestNodes === numberOfNodesFromConfig)
  }

  it should "node" in {
    val testNode = node
    assert(testNode.getID === 3)
  }

  it should "nodeAs" in {
    val testNode = nodeAs[TestNode]
    assert(testNode.toString === "TestNode-3")
  }

  it should "nodes" in {
    val testNodes = nodes
    assert(testNodes.length === numberOfNodesFromConfig)
  }

  it should "nodesAs" in {
    val testNodes = nodesAs[TestNode]
    assert(testNodes.toString === "Vector(TestNode-0, TestNode-1, TestNode-2, TestNode-3, TestNode-4)")
  }

  it should "getProtocol" in {
    val testProtocol = getProtocol(node, testProtocolID)
    assert(testProtocol.toString === "TestProtocol-TestNode-3")
  }

  it should "getProtocolAs" in {
    val testProtocol = getProtocolAs[TestProtocol](node, testProtocolID)
    assert(testProtocol.getClass.toString === "class ie.nix.peersim.TestProtocol")
  }

  it should "getProtocols" in {
    val testProtocols = getProtocols(testProtocolID)
    assert(
      testProtocols.toString === "Vector(TestProtocol-TestNode-0, TestProtocol-TestNode-1, " +
        "TestProtocol-TestNode-2, TestProtocol-TestNode-3, TestProtocol-TestNode-4)")
  }

  it should "getProtocolsAs" in {
    val testProtocols = getProtocols(testProtocolID)
    assert(testProtocols.count(_.isInstanceOf[TestProtocol]) === numberOfNodesFromConfig)
  }

  private def initPeerSim(): Unit = {
    val TEST_PROPERTIES = "src/test/resources/Test.properties"
    val testProperties = Array[String]("-file", TEST_PROPERTIES)
    peersim.Simulator.main(testProperties)
  }

  def numberOfNodesFromConfig = peersim.config.Configuration.getInt("network.size")

  def testProtocolID = 1
}
