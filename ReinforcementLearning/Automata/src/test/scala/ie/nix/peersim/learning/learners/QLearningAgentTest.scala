package ie.nix.peersim.learning.learners

import ie.nix.peersim.Utils.{getProtocolAs, getProtocolsAs, node}
import ie.nix.peersim.learning.TestProtocolWithLearner
import ie.nix.peersim.learning.TestProtocolWithLearner.{TestAction1, TestAction2, TestState}
import ie.nix.rl.Environment.StateAction
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, PrivateMethodTester}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class QLearningAgentTest
    extends AnyFlatSpec
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with PrivateMethodTester
    with Logging {

  override def beforeAll(): Unit = initPeerSim

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "QLearningAgent"

  it should "agent of right type" in {
    val testLearningProtocols = getProtocolsAs[TestProtocolWithLearner](testProtocolID)

    assert(testLearningProtocols.count(testLearningProtocol =>
      testLearningProtocol.learner.agent.isInstanceOf[QLearningAgent]) === numberOfNodesFromConfig)
  }

  it should "learning protocol index matched node index" in {
    val testLearningProtocols = getProtocolsAs[TestProtocolWithLearner](testProtocolID)

    assert(testLearningProtocols.count(testLearningProtocol =>
      testLearningProtocol.getIndex === testLearningProtocol.getNode().getIndex) === numberOfNodesFromConfig)
  }

  it should "there only one environment" in {
    val testLearningProtocols = getProtocolsAs[TestProtocolWithLearner](testProtocolID)
    val environment = testLearningProtocols(0).learner.agent.environment

    assert(
      testLearningProtocols
        .count(testLearningProtocol => testLearningProtocol.learner.agent.environment eq environment) === numberOfNodesFromConfig)
  }

  it should "getMaxStateActionValue" in {
    learner0 invokePrivate updateAgent(
      agent0.copy(
        actionValueFunction = agent0.actionValueFunction
          .updateValue(stateAction01, 0.2)
          .updateValue(stateAction02, 0.4)))
    logger info s"actionValueFunction=${agent0.actionValueFunction}"

    val maxStateActionValue = agent0 invokePrivate getMaxStateActionValue(stateAction01)
    logger info s"maxStateActionValue=$maxStateActionValue"

    assert(maxStateActionValue === 0.004)

  }

  it should "getTarget" in {
    val target = agent0 invokePrivate getTarget(0.2, 1d)
    logger info s"target=$target"

    assert(target === 1.2)
  }

//  it should "getError" in {
//    val error = agent0 invokePrivate getError(0.2, 1d, 0.4)
//    logger info s"error=$error"
//
//    assert(error === 1.2)
//  }

  private def initPeerSim(): Unit = {
    logger info s"Start"
    val TEST_PROPERTIES = "src/test/resources/TestProtocolWithQLearning.properties"
    val testProperties = Array[String]("-file", TEST_PROPERTIES)
    peersim.Simulator.main(testProperties)
    logger info s"End"
  }

  def numberOfNodesFromConfig = peersim.config.Configuration.getInt("network.size")
  def eligibilityDecayFromConfig =
    peersim.config.Configuration.getDouble("protocol.learner.agent.eligibility_decay")
  def stepSizeFromConfig = peersim.config.Configuration.getDouble("protocol.learner.agent.step_size")

  def state0: TestState = TestState(0)
  def state1: TestState = TestState(1)
  def stateAction01: StateAction = StateAction(state0, TestAction1)
  def stateAction02: StateAction = StateAction(state0, TestAction2)
  def stateAction11: StateAction = StateAction(state1, TestAction1)
  def stateAction12: StateAction = StateAction(state1, TestAction2)
  def node0 = node(index = 0)
  def testProtocolID = 2
  def learner0 = as[IndependentLearner](getProtocolAs[TestProtocolWithLearner](node0, testProtocolID).learner)
  def agent0: QLearningAgent = as[QLearningAgent](learner0.agent)

  // Agent
  val getError = PrivateMethod[Double]('getError)
  val getTarget = PrivateMethod[Double]('getTarget)
  val getMaxStateActionValue = PrivateMethod[Double]('getMaxStateActionValue)
  val updateAgent = PrivateMethod[Unit]('updateAgent)

}
