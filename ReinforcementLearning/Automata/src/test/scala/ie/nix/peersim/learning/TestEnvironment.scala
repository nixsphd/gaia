package ie.nix.peersim.learning

import ie.nix.peersim.learning.Environment.{GetStateActions, GetTerminalStates}
import ie.nix.peersim.learning.TestEnvironment.{getStateActions, getTerminalStates}
import ie.nix.peersim.learning.TestProtocolWithLearner.{TestAction1, TestAction2, TestState}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.State
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

class TestEnvironment(prefix: String) extends Environment(prefix, getStateActions, getTerminalStates) {

  logger trace s"prefix=$prefix"

  override def getReward(state: State): Double = 1d

}

object TestEnvironment extends Logging {

  val MAX_VALUE = "max_value"

  val getStateActions: GetStateActions = prefix => {
    val maxValue = Configuration.getInt(s"$prefix.$MAX_VALUE")
    (for {
      value <- 0 to maxValue
      action <- Vector(TestAction1, TestAction2)
    } yield {
      StateAction(TestState(value), action)
    }).toSet
  }

  val getTerminalStates: GetTerminalStates = prefix => {
    logger trace s"$prefix"
    Set[State]()
  }

}
