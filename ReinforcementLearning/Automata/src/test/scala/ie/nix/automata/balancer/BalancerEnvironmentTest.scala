package ie.nix.automata.balancer

import ie.nix.automata.balancer.BalancerEnvironmentTest.{
  DoNothingRejectSoftPolicy,
  ELIGIBILITY_DECAY,
  MAX_VALUE,
  MEAN_VALUE,
  NUMBER_OF_AUTOMATA,
  REWARD,
  STEP_SIZE,
  TEST_PROPERTIES
}
import ie.nix.automata.balancer.learning.BalancerEnvironment
import ie.nix.automata.balancer.learning.BalancerProtocolWithLearning.{Accept, BalancerState, DoNothing, Offer, Reject}
import ie.nix.peersim.Utils.{getProtocolAs, getProtocolsAs, node, nodes}
import ie.nix.peersim.learning.Agent
import ie.nix.peersim.learning.ProtocolLearning.{NextCycle, ProcessEvent}
import ie.nix.peersim.learning.learners.{IndependentLearner, QLambdaLearningAgent}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.SoftPolicy
import ie.nix.rl.tabular.policy.avf.EligibilityTracing
import ie.nix.rl.{Action, State}
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, PrivateMethodTester}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BalancerEnvironmentTest
    extends AnyFlatSpec
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with PrivateMethodTester
    with Logging {

  override def beforeAll(): Unit = initPeerSim

  override def beforeEach(): Unit = Random.setSeed(0)

  override def afterEach(): Unit = resetAgents()

  behavior of "BalancerEnvironment"

  it should "getReward for BalancerEnvironment" in {
    assert(balancerEnvironment.getReward(automataNC0) === -1)
    assert(balancerEnvironment.getReward(automataNC1) === 5)
    assert(balancerEnvironment.getReward(automataNC2) === -1)
    assert(balancerEnvironment.getReward(automataPE0) === -1)
    assert(balancerEnvironment.getReward(automataPE1) === 5)
    assert(balancerEnvironment.getReward(automataPE2) === -1)
  }

  behavior of "ProtocolLearning"

  it should "get negative reward for automata2DN" in {
    learner0 invokePrivate updatePreviousStateAction(automata2DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(policy = DoNothingRejectSoftPolicy(probabilityOfRandomAction)(balancerEnvironment)))
    logger info s"nextCycleAgent eligibilityTraces=${agent0.actionValueFunction}"

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata2DN))
    logger info s"nextCycleAgent eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"nextCycleAgent policy=${agent0.policy}"

    assertValueAndEligibility(agent0, automata2DN, -0.1, 0.8)
    assertPolicy(agent0, automataNC2, Offer)
  }

  it should "get positive reward for automata1DN" in {
    learner0 invokePrivate updatePreviousStateAction(automata1DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(policy = DoNothingRejectSoftPolicy(probabilityOfRandomAction)(balancerEnvironment)))
    logger info s"nextCycleAgent eligibilityTraces=${agent0.actionValueFunction}"

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata1DN))
    logger info s"nextCycleAgent eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"nextCycleAgent policy=${agent0.policy}"

    assertValueAndEligibility(agent0, automata1DN, 0.5, 0.8)
    assertPolicy(agent0, automataNC2, DoNothing)
  }

  it should "clear eligibilityTraces for automata1DN as this is soft action" in {
    learner0 invokePrivate updatePreviousStateAction(automata1DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(
        policy = DoNothingRejectSoftPolicy(probabilityOfRandomAction)(balancerEnvironment),
        actionValueFunction = agent0.actionValueFunction
          .updateValueAndEligibilityTrace(automata0DN, -0.1, 0.8)
          .updateValueAndEligibilityTrace(automata1O, 0.5, 0.8)
          .updateValueAndEligibilityTrace(automata2O, -0.1, 0.8)
      ))
    logger info s"nextCycleAgent eligibilityTraces=${agent0.actionValueFunction}"

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata1DN))
    logger info s"nextCycleAgent eligibilityTraces=${agent0.actionValueFunction}"

    assert(
      getProtocolsAs[IndependentLearner](protocolLearnerProtocolID)
        .flatMap(learner => as[QLambdaLearningAgent](learner.agent).actionValueFunction.valueMap.values)
        .count(_._2 > 0) === 0)
  }

  it should "get negative reward for automata0R" in {
    learner0 invokePrivate updatePreviousStateAction(automata0R)
    learner0 invokePrivate updateAgent(
      agent0.copy(policy = DoNothingRejectSoftPolicy(probabilityOfRandomAction)(balancerEnvironment)))
    logger info s"processEventAgent eligibilityTraces=${agent0.actionValueFunction}"

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata0R))
    logger info s"processEventAgent eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"processEventAgent policy=${agent0.policy}"

    assertValueAndEligibility(agent0, automata0R, -0.1, 0.8)
    assertPolicy(agent0, automataPE0, Accept)
  }

  it should "get positive reward for automata1R" in {
    learner0 invokePrivate updatePreviousStateAction(automata1R)
    learner0 invokePrivate updateAgent(
      agent0.copy(policy = DoNothingRejectSoftPolicy(probabilityOfRandomAction)(balancerEnvironment)))
    logger info s"processEventAgent eligibilityTraces=${agent0.actionValueFunction}"

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata1R))
    logger info s"processEventAgent eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"processEventAgent policy=${agent0.policy}"

    assertValueAndEligibility(agent0, automata1R, 0.5, 0.8)
    assertPolicy(agent0, automataPE1, Reject)
  }

  it should "clear eligibilityTraces for automata0R as this is soft action" in {
    learner0 invokePrivate updatePreviousStateAction(automata0R)
    learner0 invokePrivate updateAgent(
      agent0.copy(
        policy = DoNothingRejectSoftPolicy(probabilityOfRandomAction)(balancerEnvironment),
        actionValueFunction = agent0.actionValueFunction
          .updateValueAndEligibilityTrace(automata0R, -0.1, 0.8)
          .updateValueAndEligibilityTrace(automata1A, 0.5, 0.8)
      ))
    logger info s"processEventAgent eligibilityTraces=${agent0.actionValueFunction}"

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata0R))
    logger info s"processEventAgent eligibilityTraces=${agent0.actionValueFunction}"

    assert(
      getProtocolsAs[IndependentLearner](protocolLearnerProtocolID)
        .flatMap(learner => as[QLambdaLearningAgent](learner.agent).actionValueFunction.valueMap.values)
        .count(_._2 > 0) === 0)
  }

  private def assertValueAndEligibility(agent: Agent[EligibilityTracing],
                                        stateAction: StateAction,
                                        expectedValue: Double,
                                        expectedEligibility: Double): Unit = {
    val (actualValue, actualEligibility) = agent.actionValueFunction.getValueAndEligibility(stateAction)
    logger info s"expectedValue=$expectedValue, actualValue=$actualValue, " +
      s"expectedEligibility=$expectedEligibility, actualEligibility=$actualEligibility"
    assert(actualValue === expectedValue +- 0.0001)
    assert(actualEligibility === expectedEligibility +- 0.0001)
  }

  private def assertPolicy(agent: Agent[EligibilityTracing], state: State, action: Action) =
    assert(agent.policy.getMostProbableAction(state) == action)

  private def initPeerSim(): Unit = {
    val testProperties =
      Array[String](
        "-file",
        TEST_PROPERTIES,
        s"network.size=$NUMBER_OF_AUTOMATA",
        s"network.node.max_value=$MAX_VALUE",
        s"control.nodes.mean_value=$MEAN_VALUE",
        s"protocol.learner.environment.max_value=$MAX_VALUE",
        s"protocol.learner.environment.mean_value=$MEAN_VALUE",
        s"protocol.learner.environment.reward=$REWARD",
        s"protocol.learner.agent.eligibility_decay=$ELIGIBILITY_DECAY",
        s"protocol.learner.agent.step_size=$STEP_SIZE",
        "simulation.endtime=1"
      )
    peersim.Simulator.main(testProperties)
  }

  def balancerProtocolID: Int = peersim.config.Configuration.getPid("balancer")
  def protocolLearnerProtocolID: Int =
    peersim.config.Configuration.getPid("protocol.balancer.learner")
  def probabilityOfRandomAction: Double =
    peersim.config.Configuration.getDouble("protocol.learner.agent.probability_of_random_action")

  def automataNC0: BalancerState = BalancerState(NextCycle, 0)
  def automataNC1: BalancerState = BalancerState(NextCycle, 1)
  def automataNC2: BalancerState = BalancerState(NextCycle, 2)

  def automata0DN: StateAction = StateAction(automataNC0, DoNothing)
  def automata1DN: StateAction = StateAction(automataNC1, DoNothing)
  def automata1O: StateAction = StateAction(automataNC1, Offer)
  def automata2DN: StateAction = StateAction(automataNC2, DoNothing)
  def automata2O: StateAction = StateAction(automataNC2, Offer)

  def automataPE0: BalancerState = BalancerState(ProcessEvent, 0)
  def automataPE1: BalancerState = BalancerState(ProcessEvent, 1)
  def automataPE2: BalancerState = BalancerState(ProcessEvent, 2)

  def automata0A: StateAction = StateAction(automataPE0, Accept)
  def automata0R: StateAction = StateAction(automataPE0, Reject)
  def automata1A: StateAction = StateAction(automataPE1, Accept)
  def automata1R: StateAction = StateAction(automataPE1, Reject)
  def automata2R: StateAction = StateAction(automataPE2, Reject)

  def node0 = node(index = 0)

  def learner0 = getProtocolAs[IndependentLearner](node0, protocolLearnerProtocolID)
  def agent0 = as[QLambdaLearningAgent](learner0.agent)
  def balancerEnvironment: BalancerEnvironment = as[BalancerEnvironment](agent0.environment)

  def resetAgents(): Unit = {
    logger info s"resetAgents"
    for (node <- nodes;
         protocolLearner = getProtocolAs[IndependentLearner](node, protocolLearnerProtocolID)) {
      protocolLearner.resetAgent()
    }
  }

  // Learner
  private val updatePreviousStateAction = PrivateMethod[Unit]('updatePreviousStateAction)
  private val updateAgent = PrivateMethod[Unit]('updateAgent)
  private val learn = PrivateMethod[Agent[_]]('learn)

}

object BalancerEnvironmentTest extends Logging {

  val NUMBER_OF_AUTOMATA = 3
  val MAX_VALUE = 2
  val MEAN_VALUE = 1
  val REWARD = 5d
  val STEP_SIZE = 0.1
  val ELIGIBILITY_DECAY = 0.8
  val TEST_PROPERTIES = "src/main/resources/BalancerProblemWithQLambdaLearning.properties"

  def DoNothingRejectSoftPolicy(probabilityOfRandomAction: Double)(
      implicit environment: BalancerEnvironment): SoftPolicy = {
    environment.getNonTerminalStates.foldLeft(SoftPolicy(probabilityOfRandomAction)(environment))((policy, state) => {
      state match {
        case BalancerState(NextCycle, _) =>
          policy updateAction StateAction(state, DoNothing)
        case BalancerState(ProcessEvent, _) =>
          policy updateAction StateAction(state, Reject)
        case _ =>
          logger error s"Expected $state to be of type AutomataState"
          policy
      }
    })
  }

}
