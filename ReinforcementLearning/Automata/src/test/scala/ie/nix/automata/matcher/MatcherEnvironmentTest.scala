package ie.nix.automata.matcher

import ie.nix.automata.matcher.MatcherEnvironmentTest.{
  DoNothingSoftPolicy,
  ELIGIBILITY_DECAY,
  MAX_VALUE,
  NUMBER_OF_AUTOMATA,
  STEP_SIZE,
  TARGET_VALUE,
  TEST_PROPERTIES
}
import ie.nix.automata.matcher.learning.MatcherEnvironment
import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.{AutomataState, DoNothing, TakeAction}
import ie.nix.peersim.Utils.{getProtocolAs, getProtocolsAs, node, nodes}
import ie.nix.peersim.learning.learners.{IndependentLearner, QLambdaLearningAgent}
import ie.nix.peersim.learning.Agent
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.SoftPolicy
import ie.nix.rl.{Action, State}
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, PrivateMethodTester}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class MatcherEnvironmentTest
    extends AnyFlatSpec
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with PrivateMethodTester
    with Logging {

  override def beforeAll(): Unit = initPeerSim

  override def beforeEach(): Unit = Random.setSeed(0)

  override def afterEach(): Unit = resetAgents()

  behavior of "MatchEnvironment"

  it should "getReward" in {
    assert(environment.getReward(automata01) === -1)
    assert(environment.getReward(automata11) === 5)
    assert(environment.getReward(automata21) === -1)
  }

  behavior of "Learned"

  it should "get negative reward for automata01DN" in {
    learner0 invokePrivate updatePreviousStateAction(automata01DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(policy = DoNothingSoftPolicy(probabilityOfRandomAction)(environment)))

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata01DN))
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"policy=${agent0.policy}"

    assertValueAndEligibility(automata01DN, -0.1, 0.8)
    assertPolicy(automata01, TakeAction)
  }

  it should "get negative reward for automata2m1DN" in {
    learner0 invokePrivate updatePreviousStateAction(automata2m1DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(policy = DoNothingSoftPolicy(probabilityOfRandomAction)(environment)))

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata2m1DN))
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"policy=${agent0.policy}"

    assertValueAndEligibility(automata2m1DN, -0.1, 0.8)
    assertPolicy(automata2m1, TakeAction)
  }

  it should "get positive reward for automata01TA" in {
    learner0 invokePrivate updatePreviousStateAction(automata01TA)
    learner0 invokePrivate updateAgent(
      agent0.copy(policy = DoNothingSoftPolicy(probabilityOfRandomAction)(environment)))

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata11DN))
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"policy=${agent0.policy}"

    assertValueAndEligibility(automata01TA, 0.5, 0.8)
    assertPolicy(automata01, TakeAction)
  }

  it should "update eligibilityTraces for automata01DN, automata01TA, automata11DN" in {
    learner0 invokePrivate updatePreviousStateAction(automata01DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(
        policy = DoNothingSoftPolicy(probabilityOfRandomAction)(environment)
          .updateAction(automata01, TakeAction)
      ))

    val action = learner0.getAction(automata01)
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"policy=${agent0.policy}"

    assert(action === TakeAction)
    assertValueAndEligibility(automata01DN, -0.1, 0.8)
    assertPolicy(automata01, TakeAction)
    logger info s"automata01DN=${agent0.actionValueFunction.getValueAndEligibility(automata01DN)}"
    logger info s"automata01=${agent0.policy.getAction(automata01)}"

    val nextAction = learner0.getAction(automata11)
    assert(nextAction === DoNothing)
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"policy=${agent0.policy}"

    logger info s"automata01DN=${agent0.actionValueFunction.getValueAndEligibility(automata01DN)}"
    logger info s"automata01TA=${agent0.actionValueFunction.getValueAndEligibility(automata01TA)}"
    logger info s"automata01=${agent0.policy.getAction(automata01)}"

    assertValueAndEligibility(automata01TA, 0.5, 0.8)
    assertValueAndEligibility(automata01DN, 0.3, 0.8 * 0.8)
    assertPolicy(automata01, TakeAction)
  }

  it should "update eligibilityTraces for automata01DN, automata01DN, automata01TA, automata11DN" in {
    learner0 invokePrivate updatePreviousStateAction(automata01DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(
        policy = DoNothingSoftPolicy(probabilityOfRandomAction)(environment)
      ))

    val action = learner0.getAction(automata01)
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"policy=${agent0.policy}"
    assert(action === DoNothing)
    assertValueAndEligibility(automata01DN, -0.1, 0.8)
    assertPolicy(automata01, TakeAction)

    val nextAction = learner0.getAction(automata01)
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"policy=${agent0.policy}"
    assert(nextAction === TakeAction)
    assertValueAndEligibility(automata01DN, -0.19, 0.8)
    assertPolicy(automata01, TakeAction)

    val lastAction = learner0.getAction(automata11)
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"
    logger info s"policy=${agent0.policy}"
    assert(lastAction === DoNothing)
    assertValueAndEligibility(automata01DN, 0.21, 0.8 * 0.8)
    assertValueAndEligibility(automata01TA, 0.5, 0.8)
    assertPolicy(automata01, TakeAction)

  }

  it should "update eligibilityTraces for automata11DN, automata11DN" in {
    learner0 invokePrivate updatePreviousStateAction(automata11DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(policy = DoNothingSoftPolicy(probabilityOfRandomAction)(environment)))

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata11DN))
    assertValueAndEligibility(automata11DN, 0.5, 0.8)

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata11DN))
    assertValueAndEligibility(automata11DN, 1.0, 0.8)

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata11DN))
    assertValueAndEligibility(automata11DN, 1.5, 0.8)

    logger info s"eligibilityTraces=${agent0.actionValueFunction}"

  }

  it should "clear eligibilityTraces for automata01TA" in {
    learner0 invokePrivate updatePreviousStateAction(automata01DN)
    learner0 invokePrivate updateAgent(
      agent0.copy(
        policy = DoNothingSoftPolicy(probabilityOfRandomAction)(environment),
        actionValueFunction = agent0.actionValueFunction
          .updateValueAndEligibilityTrace(automata01DN, -0.1, 0.8)
          .updateValueAndEligibilityTrace(automata01TA, 0.5, 0.8)
      ))

    learner0 invokePrivate updateAgent(learner0 invokePrivate learn(automata01DN))
    logger info s"eligibilityTraces=${agent0.actionValueFunction}"

    assert(
      getProtocolsAs[IndependentLearner](learnerProtocolID)
        .flatMap(learner => as[QLambdaLearningAgent](learner.agent).actionValueFunction.valueMap.values)
        .count(_._2 > 0) === 0)
  }

  private def assertValueAndEligibility(stateAction: StateAction,
                                        expectedValue: Double,
                                        expectedEligibility: Double): Unit = {
    val (actualValue, actualEligibility) = agent0.actionValueFunction.getValueAndEligibility(stateAction)
    logger info s"expectedValue=$expectedValue, actualValue=$actualValue, " +
      s"expectedEligibility=$expectedEligibility, actualEligibility=$actualEligibility"
    assert(actualValue === expectedValue +- 0.0001)
    assert(actualEligibility === expectedEligibility +- 0.0001)
  }

  private def assertPolicy(state: State, action: Action) = {
    assert(agent0.policy.getMostProbableAction(state) == action)
  }

  private def initPeerSim(): Unit = {
    val testProperties =
      Array[String](
        "-file",
        TEST_PROPERTIES,
        s"network.size=$NUMBER_OF_AUTOMATA",
        s"network.node.max_value=$MAX_VALUE",
        s"protocol.matcher.target_value=$TARGET_VALUE",
        s"protocol.learner.environment.max_value=$MAX_VALUE",
        s"protocol.learner.environment.target_value=$TARGET_VALUE",
        s"protocol.learner.agent.eligibility_decay=$ELIGIBILITY_DECAY",
        s"protocol.learner.agent.step_size=$STEP_SIZE",
        "simulation.endtime=1"
      )
    peersim.Simulator.main(testProperties)
  }

  def matcherProtocolID: Int = peersim.config.Configuration.getPid("matcher")
  def learnerProtocolID: Int = peersim.config.Configuration.getPid("protocol.matcher.learner")
  def probabilityOfRandomAction: Double =
    peersim.config.Configuration.getDouble("protocol.learner.agent.probability_of_random_action")

  def automata01: AutomataState = AutomataState(0, 1)
  def automata11: AutomataState = AutomataState(1, 1)
  def automata21: AutomataState = AutomataState(2, 1)
  def automata0m1: AutomataState = AutomataState(0, -1)
  def automata1m1: AutomataState = AutomataState(1, -1)
  def automata2m1: AutomataState = AutomataState(2, -1)

  def automata01DN: StateAction = StateAction(automata01, DoNothing)
  def automata01TA: StateAction = StateAction(automata01, TakeAction)
  def automata11DN: StateAction = StateAction(automata11, DoNothing)
  def automata2m1DN: StateAction = StateAction(automata2m1, DoNothing)

  def node0 = node(index = 0)
  def learner0 = getProtocolAs[IndependentLearner](node0, learnerProtocolID)
  def agent0: QLambdaLearningAgent = as[QLambdaLearningAgent](learner0.agent)
  def environment: MatcherEnvironment = as[MatcherEnvironment](agent0.environment)

  def resetAgents(): Unit = {
    logger info s"resetAgents"
    for (node <- nodes;
         learner = getProtocolAs[IndependentLearner](node, learnerProtocolID)) {
      learner.resetAgent()
    }
  }

  // Learner
  private val updatePreviousStateAction = PrivateMethod[Unit]('updatePreviousStateAction)
  private val updateAgent = PrivateMethod[Unit]('updateAgent)
  private val learn = PrivateMethod[Agent[_]]('learn)

}

object MatcherEnvironmentTest extends Logging {

  val NUMBER_OF_AUTOMATA = 3
  val MAX_VALUE = 2
  val TARGET_VALUE = 1
  val STEP_SIZE = 0.1
  val ELIGIBILITY_DECAY = 0.8
  val TEST_PROPERTIES = "src/main/resources/MatcherProblemWithQLambdaLearning.properties"

  def DoNothingSoftPolicy(probabilityOfRandomAction: Double)(implicit environment: MatcherEnvironment): SoftPolicy = {
    environment.getNonTerminalStates.foldLeft(SoftPolicy(probabilityOfRandomAction)(environment))((policy, state) => {
      state match {
        case automataState: AutomataState =>
          policy updateAction StateAction(automataState, DoNothing)
        case _ =>
          logger error s"Expected $state to be of type AutomataState"
          policy
      }
    })
  }

}
