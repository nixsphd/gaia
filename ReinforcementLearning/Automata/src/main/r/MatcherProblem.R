library(plyr)
library(tidyr)
library(reshape2)
library(ggplot2)

setwd("~/Desktop/Gaia/ReinforcementLearning/Automata/src/main/r")

cycles <- 15

getDataFile <- function(filename) {
    paste(resultsDir, filename, sep="/")
}

# 
# MatchProblemWithIndependentLearning
# 
resultsDir <- "../../../results/MatcherProblemWithQLearning"
resultsDir <- "../../../results/MatcherProblemWithQLambdaLearning"
resultsDir <- "../../../results/MatcherProblemWithQDLambdaLearning"

data <- read.csv(getDataFile("Policy.csv"),strip.white = TRUE)
data$Episode <- data$Time/(100*cycles)
lastData <- data[data$Time == max(data$Time),]
lastData <- extract(lastData, col=State, 
                    into=c("MessageValue", "AutomataValue"), 
                    regex="Automata\\[(Inc|Dec) (.*)\\]")
lastData$MessageValue <- as.factor(lastData$MessageValue)
ggplot(lastData, aes(AutomataValue, MessageValue, fill=Action)) +
    geom_tile() + facet_wrap(~Learner)
ggsave(getDataFile("Policy.png"))

data <- read.csv(getDataFile("PolicyDiff.csv"),strip.white = TRUE)
data$Episode <- data$Time/(100*cycles)
ggplot(data, aes(Episode, Diff, color=Learner)) + geom_line()
ggsave(getDataFile("PolicyDiff.png"))

data <- read.csv(getDataFile("AVF.csv"),strip.white = TRUE)
data$Episode <- data$Time/(100*cycles)
ggplot(data, aes(Episode, Value, color=Action)) +
    geom_point(size=0.01) + 
    facet_wrap(.~State, ncol=5)
ggsave(getDataFile("AVF.png"))

lastData <- data[data$Time == max(data$Time),]
lastData <- extract(lastData, col=State, 
                    into=c("MessageValue", "AutomataValue"), 
                    regex="Automata\\[(Inc|Dec) (.*)\\]")
lastData$MessageValue <- as.factor(lastData$MessageValue)
ggplot(lastData, aes(AutomataValue, MessageValue, fill= Value)) +
    geom_tile() +
    scale_fill_gradient(low="white", high="blue") +
    facet_wrap(Action~Learner)
ggsave(getDataFile("LastAVF.png"))

