package ie.nix.peersim.learning.learners

import ie.nix.peersim.learning.Agent._
import ie.nix.peersim.learning.learners.QLambdaLearningAgent.{getActionValueFunction, getDiscount, getPolicy}
import ie.nix.peersim.learning.{Agent, Environment}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.ActionValueFunction
import ie.nix.rl.tabular.policy.SoftPolicy
import ie.nix.rl.tabular.policy.avf.EligibilityTracing
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

// TODO - Make this a private constructor.
class QLambdaLearningAgent(val policy: SoftPolicy, val actionValueFunction: EligibilityTracing, val discount: Double)(
    implicit val environment: Environment)
    extends Agent[EligibilityTracing]
    with Logging {

  def this(prefix: String)(implicit environment: Environment) = {
    this(getPolicy(prefix), getActionValueFunction(prefix), getDiscount(prefix))
  }

  override def toString: String = s"QLambdaLearningAgent"

  def copy(policy: SoftPolicy = policy,
           actionValueFunction: EligibilityTracing = actionValueFunction,
           discount: Double = discount): QLambdaLearningAgent =
    new QLambdaLearningAgent(policy, actionValueFunction, discount)

  def learn(previousStateAction: StateAction, nextStateAction: StateAction): QLambdaLearningAgent = {
    logger trace s"[${CommonState.getTime}] $this previousStateAction=$previousStateAction, nextStateAction=$nextStateAction"

    val nextStateActionValue = getStateActionValue(nextStateAction)
    val maxNextStateActionValue = getMaxStateActionValue(nextStateAction)
    logger trace s"[${CommonState.getTime}] $this nextStateActionValue=$nextStateActionValue, maxNextStateActionValue=$maxNextStateActionValue"

    val updatedEligibilityTraces =
      updateEligibilityTraces(actionValueFunction, previousStateAction, nextStateAction, maxNextStateActionValue)
    logger trace s"[${CommonState.getTime}] $this updatedEligibilityTraces=$updatedEligibilityTraces"

    // if took exploratory action zero traces. The state-action pairs you visited before the random exploratory step
    // deserve no credit/blame for future rewards, hence you delete the whole eligibility trace.
    val newEligibilityTraces =
      clearEligibilityIfExploratoryStep(updatedEligibilityTraces, nextStateActionValue, maxNextStateActionValue)

    logger debug s"[${CommonState.getTime}] $this previousStateAction=$previousStateAction, " +
      s"state=${nextStateAction.state}, reward=${environment.getReward(nextStateAction.state)}"
    // Improve the policy
    val newPolicy = greedyImprover.improvePolicy(policy, newEligibilityTraces)

    copy(policy = newPolicy, actionValueFunction = newEligibilityTraces)
  }

  private def clearEligibilityIfExploratoryStep(eligibilityTraces: EligibilityTracing,
                                                nextStateActionValue: Double,
                                                maxNextStateActionValue: Double): EligibilityTracing =
    if (nextStateActionValue != maxNextStateActionValue) eligibilityTraces.clearEligibilityForAllStates()
    else eligibilityTraces

  private def updateEligibilityTraces(eligibilityTraces: EligibilityTracing,
                                      previousStateAction: StateAction,
                                      nextStateAction: StateAction,
                                      maxNextStateActionValue: StepSize): EligibilityTracing = {
    val previousStateActionValue = eligibilityTraces.getValue(previousStateAction)
    logger trace s"[${CommonState.getTime}] $this previousStateAction=$previousStateAction, nextStateAction=$nextStateAction"

    // Get the reward for the nes state
    val reward = environment.getReward(nextStateAction.state)

    // Get the error
    val error = getError(previousStateActionValue, reward, maxNextStateActionValue)
    logger trace s"[${CommonState.getTime}] $this error=$error <= " +
      s"previousStateAction=$previousStateAction with " +
      s"reward=$reward + maxNextStateActionValue=$maxNextStateActionValue - " +
      s"previousStateActionValue=$previousStateActionValue"

    // Update the error for all states
    val updatedEligibilityTraces = eligibilityTraces.updateValue(previousStateAction, error)
    logger trace s"[${CommonState.getTime}] $this previousStateAction=$previousStateAction => ${updatedEligibilityTraces
      .getValueAndEligibility(previousStateAction)}"
    updatedEligibilityTraces
  }

  private def getError(stateActionValue: Double, reward: Double, nextStateActionValue: Double): Double = {
    // δ ← R + γV(S′) − V(S)
    reward + (discount * nextStateActionValue) - stateActionValue
  }
//
//  def clearEligibilityTraces(): WatkinsLambdaAgent =
//    copy(actionValueFunction = actionValueFunction.clearEligibilityForAllStates())

  override def newEpisode(): Agent[ActionValueFunction] =
    copy(actionValueFunction = actionValueFunction.clearEligibilityForAllStates())
}

object QLambdaLearningAgent {

  def probabilityOfRandomAction(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$PROBABILITY_OF_RANDOM_ACTION")
  def stepSize(prefix: String): StepSize = Configuration.getDouble(s"$prefix.$STEP_SIZE")
  def eligibilityDecay(prefix: String): Double = Configuration.getDouble(s"$prefix.$ELIGIBILITY_DECAY")
  def initialValue(prefix: String): Double = Configuration.getDouble(s"$prefix.$INITIAL_VALUE")
  def getDiscount(prefix: String): Double = Configuration.getDouble(s"$prefix.$DISCOUNT")

  def getPolicy(prefix: String)(implicit environment: Environment) = SoftPolicy(probabilityOfRandomAction(prefix))
  def getActionValueFunction(prefix: String)(implicit environment: Environment) =
    EligibilityTracing(getPolicy(prefix),
                       getDiscount(prefix),
                       stepSize(prefix),
                       eligibilityDecay(prefix),
                       initialValue(prefix))

  def apply(prefix: String)(implicit environment: Environment): QLambdaLearningAgent = {
    new QLambdaLearningAgent(getPolicy(prefix), getActionValueFunction(prefix), getDiscount(prefix))
  }

}
