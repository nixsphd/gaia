package ie.nix.peersim.learning.observers

import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.LEARNER
import ie.nix.peersim.Observer
import ie.nix.peersim.Utils.getProtocolsAs
import ie.nix.peersim.learning.{Agent, Learner}
import ie.nix.rl.policy.ActionValueFunction
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

class AVFObserver(prefix: String) extends Observer(prefix) with Logging {

  private val learningProtocolID = Configuration.getPid(s"$prefix.$LEARNER")
  logger trace s"[${CommonState.getTime}] learningProtocolID=$learningProtocolID"

  override def init(): Unit = {
    val headerString = "Time, Learner, State, Action, Value"
    csvFileWriter.writeHeaders(headerString)
    logger trace s"[${CommonState.getTime}] headerString=$headerString"
  }

  override def observe(): Unit = {
    val time = CommonState.getTime
    learners.foreach(learner => {
      (toAgent(learner).actionValueFunction) match {
        case avf: ActionValueFunction => {
          val stateActions = avf.getStateActions.sortBy(_.toString)
          stateActions.foreach(stateAction => {
            val value = avf.getValue(stateAction)
            csvFileWriter.writeRow(time, learner, stateAction.state, stateAction.action, value)
          })
        }
      }
    })
  }

  def learners: Vector[Learner] =
    getProtocolsAs[Learner](learningProtocolID)

  def toAgent: Learner => Agent[_] = (learner: Learner) => learner.agent

//  def toAVF: Agent[_] => Any = agent => agent.actionValueFunction

}
