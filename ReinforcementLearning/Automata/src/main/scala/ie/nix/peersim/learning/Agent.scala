package ie.nix.peersim.learning

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.State
import ie.nix.rl.policy.ActionValueFunction
import ie.nix.rl.tabular.policy.{GreedyImprover, SoftPolicy}

trait Agent[+AVF <: ActionValueFunction] {
  implicit val environment: Environment

  val policy: SoftPolicy
  val actionValueFunction: AVF
  val discount: Double

  protected val greedyImprover: GreedyImprover = GreedyImprover()

  override def toString: String = s"Agent"

  def learn(previousStateAction: StateAction, nextStateAction: StateAction): Agent[AVF]

  def newEpisode(): Agent[ActionValueFunction]

  def getStateAction(state: State): StateAction = StateAction(state, policy.getAction(state))

  def getStateActionValue(stateAction: StateAction): Double = actionValueFunction.getValue(stateAction)

  def getMaxStateActionValue(stateAction: StateAction): Double =
    actionValueFunction.getStateActions
      .filter(_.state == stateAction.state)
      .map(stateAction => actionValueFunction.getValue(stateAction))
      .max

}

object Agent {

  val PROBABILITY_OF_RANDOM_ACTION: String = "probability_of_random_action"
  val INITIAL_VALUE: String = "initial_value"
  val DISCOUNT: String = "discount"
  val STEP_SIZE: String = "step_size"
  val ELIGIBILITY_DECAY: String = "eligibility_decay"

}
