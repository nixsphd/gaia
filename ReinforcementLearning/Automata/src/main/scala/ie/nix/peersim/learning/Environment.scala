package ie.nix.peersim.learning

import ie.nix.peersim.learning.Environment.{GetStateActions, GetTerminalStates}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.State
import ie.nix.rl.tabular.EnvironmentView

abstract class Environment(prefix: String, getStateActions: GetStateActions, getTerminalStates: GetTerminalStates)
    extends EnvironmentView(getStateActions(prefix), getTerminalStates(prefix)) {

  def getReward(state: State): Double

}

object Environment {
  type GetStateActions = (String) => Set[StateAction]
  type GetTerminalStates = (String) => Set[State]
}
