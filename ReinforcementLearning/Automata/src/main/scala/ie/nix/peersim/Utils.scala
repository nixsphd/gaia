package ie.nix.peersim

import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

object Utils extends Logging {

  def numberOfNodes: Int = peersim.core.Network.size

  def node: Node = as[Node](CommonState.getNode)

  def nodeAs[T](implicit toT: Node => T): T = toT(node)

  def node(index: Int): Node = as[Node](peersim.core.Network.get(index))

  def nodeAs[T](index: Int): T = as[T](peersim.core.Network.get(index))

  def nodes: Vector[Node] =
    (0 until numberOfNodes).toVector
      .map(id => as[Node](peersim.core.Network.get(id)))

  def nodesAs[T](implicit toT: Node => T): Vector[T] = nodes.map(toT)

  def getProtocol(node: Node, protocolID: Int): Protocol =
    as[Protocol](node.getProtocol(protocolID))

  def getProtocols(protocolID: Int): Vector[Protocol] =
    nodes.map(getProtocol(_, protocolID))

  def getProtocolAs[T](node: Node, protocolID: Int): T =
    as[T](node.getProtocol(protocolID))

  def getProtocolsAs[T](protocolID: Int): Vector[T] =
    nodes.map(getProtocolAs[T](_, protocolID))

}
