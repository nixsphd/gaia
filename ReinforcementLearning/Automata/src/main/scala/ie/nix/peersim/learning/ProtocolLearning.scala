package ie.nix.peersim.learning

import ie.nix.peersim.Protocol.Message
import ie.nix.peersim.Utils.getProtocolAs
import ie.nix.peersim.learning.ProtocolLearning.PROTOCOL_LEARNER
import ie.nix.peersim.{Node, Protocol}
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

trait ProtocolLearning extends Logging {

  learningProtocol: Protocol =>

  private val protocolLearnerProtocolID = Configuration.getPid(s"$prefix.$PROTOCOL_LEARNER")
  logger trace s"[${CommonState.getTime}] protocolID=$protocolID protocolLearnerProtocolID=$protocolLearnerProtocolID"

  def protocolLearner: Learner =
    getProtocolAs[Learner](getNode, protocolLearnerProtocolID)

  def nextCycle(node: Node): Unit = {
    logger trace s"[${CommonState.getTime}] $this $node"

    val state = getNextCycleState(node)
    logger trace s"[${CommonState.getTime}] $this $node state=$state"

    val action = protocolLearner.getAction(state)
    logger trace s"[${CommonState.getTime}] $this $node state=$state, action=$action"

    takeNextCycleAction(node, action)
  }

  def getNextCycleState(node: Node): State

  def takeNextCycleAction(node: Node, action: Action): Unit

  def processEvent(node: Node, message: Message): Unit = {
    logger trace s"[${CommonState.getTime}] $this $node received message $message"

    val state = getProcessEventState(node, message)
    logger trace s"[${CommonState.getTime}] $this $node state=$state"

    val action = protocolLearner.getAction(state)
    logger debug s"[${CommonState.getTime}] $this $node state=$state, action=$action"

    takeProcessEventAction(node, message, action)
  }

  def getProcessEventState(node: Node, message: Message): State

  def takeProcessEventAction(node: Node, message: Message, action: Action): Unit

}

object ProtocolLearning extends Logging {

  val PROTOCOL_LEARNER = "learner"

  sealed trait ProtocolState
  object NextCycle extends ProtocolState {
    override def toString: String = "NextCycle"
  }
  object ProcessEvent extends ProtocolState {
    override def toString: String = "ProcessEvent"
  }
  object UnknownState extends ProtocolState {
    override def toString: String = "UnknownStates"
  }

}
