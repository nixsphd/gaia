package ie.nix.peersim.learning.observers

import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.LEARNER
import ie.nix.peersim.Observer
import ie.nix.peersim.Utils.getProtocolsAs
import ie.nix.peersim.learning.{Agent, Learner}
import ie.nix.rl.policy.Policy
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

class PolicyObserver(prefix: String) extends Observer(prefix) with Logging {

  private val learningProtocolID = Configuration.getPid(s"$prefix.$LEARNER")
  logger trace s"[${CommonState.getTime}] learningProtocolID=$learningProtocolID"

  override def init(): Unit = {
    val headerString = "Time, Learner, State, Action"
    csvFileWriter.writeHeaders(headerString)
    logger trace s"[${CommonState.getTime}] headerString=$headerString"
  }

  override def observe(): Unit = {
    val time = CommonState.getTime
    learners.foreach(learner => {
      val policy = toPolicy(toAgent(learner))
      val states = policy.getStates.sortBy(_.toString)
      states.foreach(state => {
        val action = policy.getMostProbableAction(state)
        csvFileWriter.writeRow(time, learner, state, action)
      })
    })
  }

  def learners: Vector[Learner] =
    getProtocolsAs[Learner](learningProtocolID)

  def toAgent: Learner => Agent[_] = (learner: Learner) => learner.agent

  def toPolicy: Agent[_] => Policy = agent => agent.policy

}
