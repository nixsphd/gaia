package ie.nix.peersim.learning.learners

import ie.nix.peersim.learning.Agent.{DISCOUNT, ELIGIBILITY_DECAY, INITIAL_VALUE, PROBABILITY_OF_RANDOM_ACTION}
import ie.nix.peersim.learning.learners.QDLambdaLearningAgent.{getActionValueFunction, getDiscount, getPolicy}
import ie.nix.peersim.learning.{Agent, Environment}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.SoftPolicy
import ie.nix.rl.tabular.policy.avf.QDLambda
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

class QDLambdaLearningAgent(val policy: SoftPolicy, val actionValueFunction: QDLambda, val discount: Double)(
    implicit val environment: Environment)
    extends Agent[QDLambda]
    with Logging {

  val consensusWeight = 1.0

  def this(prefix: String)(implicit environment: Environment) =
    this(getPolicy(prefix), getActionValueFunction(prefix), getDiscount(prefix))

  override def toString: String = s"QDLambdaLearningAgent"

  def copy(policy: SoftPolicy = policy,
           actionValueFunction: QDLambda = actionValueFunction,
           discount: Double = discount): QDLambdaLearningAgent = {
    new QDLambdaLearningAgent(policy, actionValueFunction, discount)
  }

  override def learn(previousStateAction: StateAction, nextStateAction: StateAction): QDLambdaLearningAgent =
    learn(previousStateAction, nextStateAction, Vector())

  def learn(previousStateAction: StateAction,
            nextStateAction: StateAction,
            neighboursStateActionValues: Vector[Double]): QDLambdaLearningAgent = {
    val updatedQDLambda =
      updateQDLambda(actionValueFunction, previousStateAction, nextStateAction, neighboursStateActionValues)

    // if took exploratory action zero traces. The state-action pairs you visited before the random exploratory step
    // deserve no credit/blame for future rewards, hence you delete the whole eligibility trace.
    val newQDLambda =
      if (isExploratoryStep(nextStateAction)) {
        updatedQDLambda.clearEligibilityForAllStates()
      } else updatedQDLambda

    // Improve the policy
    val newPolicy = greedyImprover.improvePolicy(policy, newQDLambda)

    copy(policy = newPolicy, actionValueFunction = newQDLambda)

  }

  private def isExploratoryStep(nextStateAction: StateAction) =
    getStateActionValue(nextStateAction) != getMaxStateActionValue(nextStateAction)

  private def updateQDLambda(qdLambda: QDLambda,
                             stateAction: StateAction,
                             nextStateAction: StateAction,
                             neighboursStateActionValues: Vector[Double]): QDLambda = {

    // Get the reward for the nes state
    val reward = environment.getReward(nextStateAction.state)

    // Get the innovation term
    val innovation =
      getInnovation(getStateActionValue(stateAction), reward, getMaxStateActionValue(nextStateAction))

    // Get the consensus term
    val consensus = getConsensus(getStateActionValue(stateAction), neighboursStateActionValues)

    // Update the error for all states
    val updatedQDLambda = qdLambda.updateValue(stateAction, innovation, consensus)
    logger trace s"stateAction=$stateAction, innovation=$innovation, consensus=$consensus, updatedQDLambda=$updatedQDLambda"
    updatedQDLambda
  }

  private def getInnovation(stateActionValue: Double, reward: Double, nextStateActionValue: Double): Double = {
    // δ ← R + γV(S′) − V(S)
    reward + (discount * nextStateActionValue) - stateActionValue
  }

  private def getConsensus(previousStateActionValue: Double, neighboursStateActionValues: Vector[Double]): Double = {
    // i ← ∑ V(S) − Vn(S)
    neighboursStateActionValues
      .map(neighboursStateActionValue => previousStateActionValue - neighboursStateActionValue)
      .sum
  }

  override def newEpisode(): QDLambdaLearningAgent =
    copy(actionValueFunction = actionValueFunction.clearEligibilityForAllStates())

}

object QDLambdaLearningAgent {

  val INNOVATION_STEP_SIZE = "innovation_step_size"
  val CONSENSUS_STEP_SIZE = "consensus_step_size"

  def probabilityOfRandomAction(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$PROBABILITY_OF_RANDOM_ACTION")
  def getDiscount(prefix: String): Double = Configuration.getDouble(s"$prefix.$DISCOUNT")
  def innovationStepSize(prefix: String): StepSize = Configuration.getDouble(s"$prefix.$INNOVATION_STEP_SIZE")
  def consensusStepSize(prefix: String): StepSize = Configuration.getDouble(s"$prefix.$CONSENSUS_STEP_SIZE")
  def eligibilityDecay(prefix: String): Double = Configuration.getDouble(s"$prefix.$ELIGIBILITY_DECAY")
  def initialValue(prefix: String): Double = Configuration.getDouble(s"$prefix.$INITIAL_VALUE")

  def getPolicy(prefix: String)(implicit environment: Environment) = SoftPolicy(probabilityOfRandomAction(prefix))
  def getActionValueFunction(prefix: String)(implicit environment: Environment) =
    QDLambda(getPolicy(prefix),
             getDiscount(prefix),
             innovationStepSize(prefix),
             consensusStepSize(prefix),
             eligibilityDecay(prefix),
             initialValue(prefix))

  def apply(prefix: String)(implicit environment: Environment): QDLambdaLearningAgent = {
    new QDLambdaLearningAgent(getPolicy(prefix), getActionValueFunction(prefix), getDiscount(prefix))
  }

}
