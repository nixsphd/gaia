package ie.nix.peersim.learning

import ie.nix.peersim.Protocol
import ie.nix.peersim.learning.Learner.{AGENT, getEnvironment}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.ActionValueFunction
import ie.nix.rl.{Action, State}
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import scala.util.Random

trait Learner extends Logging {

  learner: Protocol =>

  implicit val environment: Environment = getEnvironment(prefix)

  protected var maybeAgent: Option[Agent[ActionValueFunction]] = None
  protected var maybePreviousStateAction: Option[StateAction] = None

  def agent: Agent[ActionValueFunction] = {
    if (!maybeAgent.isDefined) {
      val agentPrefix: String = s"$prefix.$AGENT"
      val agent: Agent[ActionValueFunction] = getAgentForPrefix(agentPrefix).asInstanceOf[Agent[ActionValueFunction]]
      updateAgent(agent)
      logger trace s"$this getAgentForPrefix => ${maybeAgent.get}"
    }
    maybeAgent.get
  }

  protected def updateAgent(agent: Agent[ActionValueFunction]): Unit = {
    maybeAgent = Some(agent)
    logger trace s"$this ${maybeAgent.get}"
  }

  protected def updatePreviousStateAction(previousStateAction: StateAction): Unit =
    maybePreviousStateAction = Some(previousStateAction)

  def resetAgent(): Unit = maybeAgent = None

  def resetPreviousStateAction(): Unit = maybePreviousStateAction = None

  def getAction(state: State): Action

  def getAgentForPrefix(agentPrefix: String): Any = {
    val agentClassName = Configuration.getString(agentPrefix)
    val agentClass = Class.forName(agentClassName)
    val agentConstructor = agentClass.getConstructor(classOf[String], classOf[Environment])
    agentConstructor.newInstance(agentPrefix, environment)
  }

}

object Learner {

  val ENVIRONMENT: String = "environment"
  val SEED: String = "random.seed"
  val AGENT: String = "agent"

  // Setting the Scala Random Number Generator used by the RL classes.
  Random.setSeed(seed)

  def getEnvironment(prefix: String): Environment =
    as[Environment](Configuration.getInstance(s"$prefix.$ENVIRONMENT"))

  def seed: Long = Configuration.getLong(s"$SEED")

}
