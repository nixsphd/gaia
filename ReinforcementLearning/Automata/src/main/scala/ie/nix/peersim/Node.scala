package ie.nix.peersim

import ie.nix.peersim.Utils.nodes
import org.apache.logging.log4j.scala.Logging
import peersim.core.{CommonState, Control, GeneralNode}

class Node(val prefix: String) extends GeneralNode(prefix) with Logging {

  logger trace s"[${CommonState.getTime}] prefix=$prefix => $this"

  override def clone: AnyRef = {
    val cloned = super.clone
    logger trace s"[${CommonState.getTime}] Cloned $cloned"
    cloned
  }

  override def toString = s"TestNode-$getID"

}

object Node {

  class Init(val prefix: String) extends Control with Logging {

    def execute: Boolean = {
      for {
        node <- nodes
        protocolId <- 0 until node.protocolSize()
        protocol = node.getProtocol(protocolId)
      } (node, protocol) match {
        case (node: Node, testProtocol: Protocol) =>
          testProtocol.setNode(node)
        case _ => // Do nothings
      }
      false
    }
  }

}
