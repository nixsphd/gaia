package ie.nix.peersim

import ie.nix.peersim.Protocol.Message
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.cdsim.CDProtocol
import peersim.config.{Configuration, FastConfig}
import peersim.core.{CommonState, Linkable}
import peersim.edsim.EDProtocol
import peersim.transport.Transport

import scala.util.Random

abstract class Protocol(val prefix: String) extends CDProtocol with EDProtocol with Logging {

  val protocolName = prefix.replaceAll("protocol\\.", "")
  val protocolID: Int = Configuration.lookupPid(protocolName)
  logger trace s"[${CommonState.getTime}] prefix=$prefix, protocolID=$protocolID"

  private var node: Node = _

  override def clone: AnyRef = {
    val cloned = super.clone
    logger trace s"[${CommonState.getTime}] Cloned $cloned"
    cloned
  }

  override def toString = if (Option(getNode).isDefined) { s"$protocolName-$getIndex" } else { s"$protocolName-?" }

  override def nextCycle(node: peersim.core.Node, protocolID: Int): Unit = node match {
    case (node: Node) => nextCycle(node)
    case _            => // Do Nothing
  }

  override def processEvent(node: peersim.core.Node, protocolID: Int, event: Any): Unit = (node, event) match {
    case (_: Node, runnable: Runnable)  => runnable.run()
    case (node: Node, message: Message) => processEvent(node, message)
    case _                              => // Do Nothing
  }

  def nextCycle(node: Node): Unit

  def processEvent(node: Node, message: Message): Unit

  import peersim.edsim.EDSimulator

  protected def addCallback(delay: Long, runnable: Runnable): Unit = {
    logger info s"[${CommonState.getTime}]~delay=$delay, node=$node"
    EDSimulator.add(delay, runnable, node, protocolID)
  }

  def getNode(): Node = node

  def setNode(node: Node): Unit = this.node = node

  def getIndex: Int = node.getIndex

  def transport: Transport = as[Transport](getNode.getProtocol(FastConfig.getTransport(protocolID)))

  def neighbourhood: Linkable = as[Linkable](getNode.getProtocol(FastConfig.getLinkable(protocolID)))

  def neighbour(index: Int): Node = as[Node](neighbourhood.getNeighbor(index))

  def numberOfNeighbours: Int = neighbourhood.degree()

  def neighbourAs[T](index: Int): T = as[T](neighbour(index))

  def neighbours: Vector[Node] = neighboursAs[Node]

  def neighboursAs[T]: Vector[T] =
    (0 until neighbourhood.degree())
      .map(neighbourIndex => as[T](neighbourhood.getNeighbor(neighbourIndex)))
      .toVector

  def randomNeighbour: Node = neighbours(Random.nextInt(numberOfNeighbours))

  def randomNeighbourAs[T]: T = as[T](neighbours(Random.nextInt(numberOfNeighbours)))

  def sendMessage(message: Message, toNode: Node): Unit = {
    transport.send(getNode, toNode, message, protocolID)
    logger debug s"[${CommonState.getTime}]~$getNode sent $message to $toNode protocolID=$protocolID"
  }
}

object Protocol extends Logging {

  class Message(val from: Node) {
    override def toString: String = "Message"
  }

}
