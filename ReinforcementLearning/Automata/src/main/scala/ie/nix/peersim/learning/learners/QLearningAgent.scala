package ie.nix.peersim.learning.learners

import ie.nix.peersim.learning.Agent._
import ie.nix.peersim.learning.Learner.getEnvironment
import ie.nix.peersim.learning.learners.QLearningAgent.{getActionValueFunction, getDiscount, getPolicy}
import ie.nix.peersim.learning.{Agent, Environment}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.SoftPolicy
import ie.nix.rl.tabular.policy.avf.ConstantStepSize
import ie.nix.rl.tabular.policy.vf.ConstantStepSize.StepSize
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

// TODO - Make this a private constructor.
class QLearningAgent(val policy: SoftPolicy, val actionValueFunction: ConstantStepSize, val discount: Double)(
    implicit val environment: Environment)
    extends Agent[ConstantStepSize]
    with Logging {

  def this(prefix: String)(implicit environment: Environment) =
    this(getPolicy(prefix), getActionValueFunction(prefix), getDiscount(prefix))

  override def toString: String = s"QLearningAgent"

  def copy(policy: SoftPolicy = policy,
           actionValueFunction: ConstantStepSize = actionValueFunction,
           discount: Double = discount): QLearningAgent =
    new QLearningAgent(policy, actionValueFunction, discount)

  def learn(previousStateAction: StateAction, nextStateAction: StateAction): QLearningAgent = {
    logger trace s"[${CommonState.getTime}] $this previousStateAction=$previousStateAction, nextStateAction=$nextStateAction"

    val nextStateActionValue = getStateActionValue(nextStateAction)
    val maxNextStateActionValue = getMaxStateActionValue(nextStateAction)
    logger trace s"[${CommonState.getTime}] $this nextStateActionValue=$nextStateActionValue, maxNextStateActionValue=$maxNextStateActionValue"

    val updatedActionValueFunction =
      updateActionValueFunction(previousStateAction, nextStateAction, maxNextStateActionValue)
    logger trace s"[${CommonState.getTime}] $this updatedActionValueFunction=$updatedActionValueFunction"

    // Improve the policy
    val newPolicy = greedyImprover.improvePolicy(policy, updatedActionValueFunction)

    copy(policy = newPolicy, actionValueFunction = updatedActionValueFunction)
  }

  override def newEpisode(): Agent[ConstantStepSize] = this

  private def updateActionValueFunction(previousStateAction: StateAction,
                                        nextStateAction: StateAction,
                                        maxNextStateActionValue: Double): ConstantStepSize = {

    val previousStateActionValue = actionValueFunction.getValue(previousStateAction)
    logger trace s"[${CommonState.getTime}] $this previousStateAction=$previousStateAction, nextStateAction=$nextStateAction"

    // Get the reward for the nes state
    val reward = environment.getReward(nextStateAction.state)

    // Get the error
    val target = getTarget(reward, maxNextStateActionValue)
    logger trace s"[${CommonState.getTime}] $this target=$target <= " +
      s"previousStateAction=$previousStateAction with " +
      s"reward=$reward + maxNextStateActionValue=$maxNextStateActionValue - " +
      s"previousStateActionValue=$previousStateActionValue"

    // Update the error for all states
    val updatedConstantStepSize = actionValueFunction.updateValue(previousStateAction, target)
    logger trace s"[${CommonState.getTime}] $this previousStateAction=$previousStateAction => ${updatedConstantStepSize
      .getValue(previousStateAction)}"
    updatedConstantStepSize
  }

  def getTarget(reward: Double, maxNextStateActionValue: Double): Double = {
    // Q-Learning - Q(S,A) ← Q(S,A) + α[R + γ max a Q(S′,a) − Q(S,A)]
    reward + (discount * maxNextStateActionValue) // R + γ max a Q(S′,a)
  }

}

object QLearningAgent {

  def probabilityOfRandomAction(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$PROBABILITY_OF_RANDOM_ACTION")
  def stepSize(prefix: String): StepSize = Configuration.getDouble(s"$prefix.$STEP_SIZE")
  def eligibilityDecay(prefix: String): Double = Configuration.getDouble(s"$prefix.$ELIGIBILITY_DECAY")
  def initialValue(prefix: String): Double = Configuration.getDouble(s"$prefix.$INITIAL_VALUE")
  def getDiscount(prefix: String): Double = Configuration.getDouble(s"$prefix.$DISCOUNT")

  def getPolicy(prefix: String)(implicit environment: Environment): SoftPolicy =
    SoftPolicy(probabilityOfRandomAction(prefix))
  def getActionValueFunction(prefix: String)(implicit environment: Environment): ConstantStepSize =
    ConstantStepSize(getPolicy(prefix), initialValue(prefix), stepSize(prefix))

  def environment(prefix: String): Environment = getEnvironment(prefix)

  def apply(prefix: String)(implicit environment: Environment): QLearningAgent = {
    new QLearningAgent(getPolicy(prefix), getActionValueFunction(prefix), getDiscount(prefix))
  }

}
