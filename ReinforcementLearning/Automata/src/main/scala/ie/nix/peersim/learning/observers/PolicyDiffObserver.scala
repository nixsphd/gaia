package ie.nix.peersim.learning.observers

import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.LEARNER
import ie.nix.peersim.Observer
import ie.nix.peersim.Utils.getProtocolsAs
import ie.nix.peersim.learning.{Agent, Environment, Learner}
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.rl.tabular.policy.Policy.difference
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

abstract class PolicyDiffObserver(prefix: String) extends Observer(prefix) with Logging {

  private val learningProtocolID = Configuration.getPid(s"$prefix.$LEARNER")
  logger trace s"[${CommonState.getTime}] learningProtocolID=$learningProtocolID"

  override def init(): Unit = {
    val headerString = "Time, Learner, Diff"
    csvFileWriter.writeHeaders(headerString)
    logger trace s"[${CommonState.getTime}] headerString=$headerString"
  }

  override def observe(): Unit = {
    val time = CommonState.getTime
    learners.foreach(learner => {
      val environment = learner.agent.environment
      val policy = toPolicy(toAgent(learner))
      val policyDifference = difference(policy, idealPolicy(environment))
      csvFileWriter.writeRow(time, learner, policyDifference)
    })
  }

  def learners: Vector[Learner] =
    getProtocolsAs[Learner](learningProtocolID)

  def toAgent: Learner => Agent[_] = (learner: Learner) => learner.agent

  def toPolicy: Agent[_] => Policy = agent => agent.policy

  def idealPolicy(environment: Environment): DeterministicPolicy

}
