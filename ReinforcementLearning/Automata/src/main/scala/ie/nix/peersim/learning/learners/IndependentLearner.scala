package ie.nix.peersim.learning.learners

import ie.nix.peersim.Protocol.Message
import ie.nix.peersim.learning.{Agent, Learner}
import ie.nix.peersim.{Node, Protocol}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.ActionValueFunction
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

class IndependentLearner(prefix: String) extends Protocol(prefix) with Learner with Logging {

  override def toString =
    if (Option(getNode).isDefined) { s"IndependentLearner-$getIndex" } else { "IndependentLearner-?" }

  override def resetPreviousStateAction(): Unit = {
    super.resetPreviousStateAction()
    updateAgent(agent.newEpisode())
  }

  override def nextCycle(node: Node): Unit = {
    logger trace s"[${CommonState.getTime}]"
    resetPreviousStateAction()
  }

  override def processEvent(node: Node, message: Message): Unit = {}

  override def getAction(state: State): Action = {
    val nextStateAction = agent.getStateAction(state)
    logger trace s"[${CommonState.getTime}] => $this, nextStateAction=$nextStateAction"
    val newAgent = learn(nextStateAction)
    logger trace s"[${CommonState.getTime}] => $this, newAgent=$newAgent"
    updateAgent(newAgent)
    updatePreviousStateAction(nextStateAction)
    nextStateAction.action
  }

  def learn(nextStateAction: StateAction): Agent[ActionValueFunction] =
    maybePreviousStateAction match {
      case Some(previousStateAction) => agent.learn(previousStateAction, nextStateAction)
      case None                      => agent // can only learn is there is a previous StateAction to learn about
    }

}
