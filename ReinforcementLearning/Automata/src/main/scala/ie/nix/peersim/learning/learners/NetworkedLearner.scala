package ie.nix.peersim.learning.learners

import ie.nix.peersim.Protocol.Message
import ie.nix.peersim.Utils.getProtocolAs
import ie.nix.peersim.learning.Learner
import ie.nix.peersim.{Node, Protocol}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.{Action, State}
import ie.nix.util.Utils.as

class NetworkedLearner(prefix: String) extends Protocol(prefix) with Learner {

  override def toString =
    if (Option(getNode).isDefined) { s"NetworkedLearner-$getIndex" } else { "NetworkedLearner-?" }

  override def resetPreviousStateAction(): Unit = {
    super.resetPreviousStateAction()
    updateAgent(agent.newEpisode())
  }

  override def nextCycle(node: Node): Unit = resetPreviousStateAction

  override def processEvent(node: Node, message: Message): Unit = {}

  override def agent: QDLambdaLearningAgent = {
    as[QDLambdaLearningAgent](super.agent)
  }

  def getAction(state: State): Action = {
    val nextStateAction = agent.getStateAction(state)
    val newAgent = learn(nextStateAction)
    updateAgent(newAgent)
    updatePreviousStateAction(nextStateAction)
    nextStateAction.action
  }

  def learn(nextStateAction: StateAction): QDLambdaLearningAgent = {
    maybePreviousStateAction match {
      case Some(previousStateAction) => {
        //    val message = new QDLearnerRequest(nextStateAction)
        //    neighbours.foreach(sendMessage(message, _))
        val neighboursPreviousStateActionValues = neighbours
          .map(getProtocolAs[NetworkedLearner](_, protocolID))
          .map(_.agent.actionValueFunction.getValue(previousStateAction))
        agent.learn(previousStateAction, nextStateAction, neighboursPreviousStateActionValues)
      }
      case None => agent //can only learn is there is a previous StateAction to learn about
    }
  }

}

object NetworkedLearner {

  class QDLearnerRequest(from: Node, val stateAction: StateAction) extends Message(from) {
    override def toString: String = "QDLearnerRequest [" + stateAction + "]"
  }

}
