package ie.nix.peersim

import ie.nix.peersim.Observer.{LOG_DIR, LOG_FILE}
import ie.nix.util.CSVFileWriter.PreciseCSVFileWriter
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.{CommonState, Control}

class Observer(prefix: String) extends Control with Logging {

  val logDirName =
    Configuration.getString(s"$prefix.$LOG_DIR", Configuration.getString(Observer.LOG_DIR, "."))
  logger trace s"[${CommonState.getTime}]~$prefix.$LOG_DIR => logDirName=$logDirName"

  val logFileName =
    Configuration.getString(s"$prefix.$LOG_FILE", s"$getName")
  logger trace s"[${CommonState.getTime}]~logFileName=$logFileName"

  val csvFileWriter = PreciseCSVFileWriter(logDirName, logFileName, 4)

  protected def getName: String = this.getClass.getSimpleName

  override final def execute: Boolean = {
    logger trace s"[${CommonState.getTime}]"
    if (CommonState.getTime == 0) init() else observe()
    false
  }

  protected def init(): Unit = {}

  protected def observe(): Unit = {}

}

object Observer extends Logging {
  val LOG_DIR = "log_dir"
  val LOG_FILE = "log_file"
}
