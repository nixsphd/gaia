package ie.nix.automata.matcher.learning

import ie.nix.automata.matcher.learning.MatcherEnvironment.{
  MAX_VALUE,
  TARGET_HIT_REWARD,
  TARGET_MISS_PENALTY,
  TARGET_VALUE,
  getStateActions,
  getTerminalStates
}
import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.{AutomataState, DoNothing, TakeAction}
import ie.nix.peersim.learning.Environment
import ie.nix.peersim.learning.Environment.{GetStateActions, GetTerminalStates}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.State
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

class MatcherEnvironment(prefix: String) extends Environment(prefix, getStateActions, getTerminalStates) with Logging {

  logger trace s"prefix=$prefix"

  val maxValue = Configuration.getInt(s"$prefix.$MAX_VALUE")
  val targetValue = Configuration.getInt(s"$prefix.$TARGET_VALUE")
  val hitTargetReward = Configuration.getDouble(s"$prefix.$TARGET_HIT_REWARD")

  override def toString: String =
    "MatchLearnerEnvironment[" + stateActionsSet.toVector.sortBy(_.toString) + "]"

  def getReward(state: State): Double = {
    state match {
      case automataState: AutomataState if (automataState.automataValue == targetValue) => {
        logger debug s"automataState=$automataState => $hitTargetReward"
        hitTargetReward
      }
      case automataState: AutomataState => {
        logger debug s"automataState=$automataState => $TARGET_MISS_PENALTY"
        TARGET_MISS_PENALTY
      }
      case _ => {
        logger error s"expected $state to be of type AutomataState."
        0d
      }
    }
  }

}

object MatcherEnvironment extends Logging {

  val MAX_VALUE: String = "max_value"
  val TARGET_VALUE: String = "target_value"

  val TARGET_HIT_REWARD: String = "reward"
  val TARGET_MISS_PENALTY: Double = -1d

  val getStateActions: GetStateActions = prefix => {
    val maxValue = Configuration.getInt(s"$prefix.$MAX_VALUE")
    val stateActions = Set[StateAction]() +
      StateAction(AutomataState(0, 1), DoNothing) +
      StateAction(AutomataState(0, 1), TakeAction) +
      StateAction(AutomataState(0, -1), DoNothing) +
      StateAction(AutomataState(maxValue, 1), DoNothing) +
      StateAction(AutomataState(maxValue, -1), DoNothing) +
      StateAction(AutomataState(maxValue, -1), TakeAction) ++
      (for {
        value <- 1 to maxValue - 1
        messageValue <- Vector(-1, 1)
        action <- List(DoNothing, TakeAction)
      } yield {
        StateAction(AutomataState(value, messageValue), action)
      }).toSet
    stateActions
  }

  val getTerminalStates: GetTerminalStates = prefix => {
    logger trace s"$prefix"
    Set[State]()
  }

}
