package ie.nix.automata

import ie.nix.automata.AutomataProtocol.AutomataMessage
import ie.nix.peersim.Protocol.Message
import ie.nix.peersim.{Node, Protocol}
import ie.nix.util.Utils.as
import peersim.core.CommonState

class AutomataProtocol(prefix: String) extends Protocol(prefix) {

  override def nextCycle(node: Node): Unit = node match {
    case automata: Automata => nextCycle(automata)
    case _                  => logger error s"Expected $node to be of type Automata"
  }

  override def processEvent(node: Node, message: Message): Unit = (node, message) match {
    case (automata: Automata, message: AutomataMessage) => processEvent(automata, message)
    case _ =>
      logger error s"Expected $node to be of type Automata and $message to be of type AutomataMessage"
  }

  def automata: Automata = as[Automata](getNode)

  def leftAutomata: Automata = neighbour(0)

  def rightAutomata: Automata = neighbour(1)

  def nextCycle(automata: Automata): Unit = {
    logger debug s"[${CommonState.getTime}] $automata sending messages"
    sendMessages(automata, new AutomataMessage(automata))
  }

  def sendMessages(automata: Automata, message: Message) = {
    logger trace s"[${CommonState.getTime}] $automata sent $message"

    sendMessage(message, rightAutomata)
    logger trace s"[${CommonState.getTime}] $automata sent $message to $rightAutomata"

    sendMessage(message, leftAutomata)
    logger trace s"[${CommonState.getTime}] $automata sent $message to $leftAutomata"
  }

  def processEvent(automata: Automata, message: AutomataMessage): Unit = {
    logger trace s"[${CommonState.getTime}]~$automata received $message"
  }

  def getAutomataValue: Int = automata.getValue

  def setAutomataValue(value: Int): Unit = automata.setValue(value)

  def incrementAutomataValue(): Unit = automata.incrementValue()

  def decrementAutomataValue(): Unit = automata.decrementValue()

  def getAutomataIndex = automata.getIndex

}

object AutomataProtocol {

  class AutomataMessage(from: Automata) extends Message(from) {
    override def toString: String = "AutomataMessage"
  }

}
