package ie.nix.automata.balancer.learning

import ie.nix.automata.balancer.learning.BalancerPolicyDiffObserver.getIdealPolicy
import ie.nix.automata.balancer.learning.BalancerProtocolWithLearning._
import ie.nix.peersim.learning.Environment
import ie.nix.peersim.learning.ProtocolLearning.{NextCycle, ProcessEvent}
import ie.nix.peersim.learning.observers.PolicyDiffObserver
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

class BalancerPolicyDiffObserver(prefix: String) extends PolicyDiffObserver(prefix) {

  override def idealPolicy(environment: Environment): DeterministicPolicy = getIdealPolicy(environment)

}

object BalancerPolicyDiffObserver extends Logging {

  def getIdealPolicy(environment: Environment): DeterministicPolicy = {
    val balancerEnvironment: BalancerEnvironment = as[BalancerEnvironment](environment)
    balancerEnvironment.getNonTerminalStates.foldLeft(DeterministicPolicy(balancerEnvironment))((policy, state) => {
      state match {
        case BalancerState(NextCycle, automataValue) if (automataValue <= balancerEnvironment.meanValue) =>
          policy updateAction StateAction(state, DoNothing)
        case BalancerState(NextCycle, automataValue) if (automataValue > balancerEnvironment.meanValue) =>
          policy updateAction StateAction(state, Offer)
        case BalancerState(ProcessEvent, automataValue) if (automataValue < balancerEnvironment.meanValue) =>
          policy updateAction StateAction(state, Accept)
        case BalancerState(ProcessEvent, automataValue) if (automataValue >= balancerEnvironment.meanValue) =>
          policy updateAction StateAction(state, Reject)
        case _ =>
          logger error s"Expected $state to be of type AutomataState"
          policy
      }
    })
  }

}
