package ie.nix.automata.matcher

import ie.nix.automata.Automata
import ie.nix.automata.matcher.Utils.runPeerSim
import ie.nix.automata.matcher.learning.MatcherPolicyDiffObserver.getIdealPolicy
import ie.nix.peersim.Utils.{getProtocolsAs, nodes, nodesAs, numberOfNodes}
import ie.nix.peersim.learning.Learner
import ie.nix.rl.tabular.policy.Policy.difference
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

object MatcherProblem extends App with Logging {

  val AUTOMATA_PARAMS = "src/main/resources/MatcherProblem.properties"
  val properties = Array[String]("-file", AUTOMATA_PARAMS)

  peersim.Simulator.main(properties)

  val maxValue = peersim.config.Configuration.getInt("network.node.max_value")
  val targetValue = peersim.config.Configuration.getInt("protocol.matcher.target_value")
  val numberCorrect = nodesAs[Automata].count(_.getValue == targetValue)

  logger info nodes.mkString(",")
  logger info s"targetValue=$targetValue, numberCorrect=$numberCorrect, numberOfNodes=$numberOfNodes"

}

object MatcherProblemWithQLearning extends App {
  runPeerSim("src/main/resources/MatcherProblemWithQLearning.properties")
}

object MatcherProblemWithQLambdaLearning extends App {
  runPeerSim("src/main/resources/MatcherProblemWithQLambdaLearning.properties")
}

object MatcherProblemWithQDLambdaLearning extends App {
  runPeerSim("src/main/resources/MatcherProblemWithQDLambdaLearning.properties")
}

object Utils extends Logging {
  def learners: Vector[Learner] = getProtocolsAs[Learner](learningProtocolID)
  def learningProtocolID = Configuration.getPid("protocol.matcher.learner")
  def idealPolicy = getIdealPolicy(learners(0).agent.environment)
  def learnerDifferences = learners.map(learner => (learner, difference(learner.agent.policy, idealPolicy)))
  def totalLearnerDifferences = learnerDifferences.map(_._2).sum
  def numberCorrect = learnerDifferences.count(_._2 == 0)

  def runPeerSim(propertiesFile: String) = {
    val properties = Array[String]("-file", propertiesFile)
    peersim.Simulator.main(properties)
    logger info learnerDifferences.mkString(",")
    logger info s"differences=$totalLearnerDifferences, numberCorrect=$numberCorrect, numberOfNodes=$numberOfNodes"
  }

}
