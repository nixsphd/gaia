package ie.nix.automata.drifting_balancer

import ie.nix.automata.Automata
import ie.nix.automata.balancer.BalancerProblemControl
import ie.nix.automata.drifting_balancer.DriftingBalancerProblemControl.ADD_ITEMS_STEP
import ie.nix.automata.drifting_balancer.Util.runPeerSim
import ie.nix.peersim.Utils.{nodesAs, numberOfNodes}
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

import scala.util.Random

object DriftingBalancerProblemWithQLearning extends App with Logging {
  runPeerSim("src/main/resources/DriftingBalancerProblemWithQLearning.properties")
}

object DriftingBalancerProblemWithQLambdaLearning extends App with Logging {
  runPeerSim("src/main/resources/DriftingBalancerProblemWithQLambdaLearning.properties")
}

object DriftingBalancerProblemWithQDLambdaLearning extends App with Logging {
  runPeerSim("src/main/resources/DriftingBalancerProblemWithQDLambdaLearning.properties")
}

object Util extends Logging {

  def runPeerSim(propertiesFile: String) = {
    val properties = Array[String]("-file", propertiesFile)
    peersim.Simulator.main(properties)
  }

}

class DriftingBalancerProblemControl(prefix: String) extends BalancerProblemControl(prefix) {

  lazy val addItemsStep = Configuration.getInt(s"$prefix.$ADD_ITEMS_STEP")
  logger info s"[${CommonState.getTime}] seed=$seed, maxValue=$maxValue, addItemsStep=$addItemsStep"

  override def execute: Boolean = {

    // Initialise to Automata values to 0
    if (shouldInitialise) {
      initialiseAutomataToZero()
      addItemsToAutomataRandomly(numberOfNodes)
      shuffleItems()
      logger info s"[${CommonState.getTime}] Initialised Automata added ${numberOfNodes} items realMean=$realMean, ${nodesAs[
        Automata].mkString(", ")}"
    } else {
      // Add numberOfNodes new items randomly to Automata...
      if (shouldAddItems) {
        addItemsToAutomataRandomly(numberOfNodes)
        shuffleItems()
        logger info s"[${CommonState.getTime}] Added ${numberOfNodes} items realMean=$realMean, ${nodesAs[Automata].mkString(", ")}"
      } else {
        // Shuffle items between Automata
        shuffleItems()
        logger debug s"[${CommonState.getTime}] Shuffled items, ${nodesAs[Automata].mkString(", ")}"
      }
    }
    false
  }

  protected def shouldInitialise: Boolean = CommonState.getTime == 0

  protected def initialiseAutomataToZero(): Unit = nodesAs[Automata].foreach(_.setValue(0))

  protected def shouldAddItems: Boolean = CommonState.getTime % addItemsStep == 0

  protected def addItemsToAutomataRandomly(numberOfItemsToAdd: Int): Unit =
    (0 until numberOfItemsToAdd).foreach(_ => addItem())

  protected def addItem(): Unit = {
    val nonFullAutomata = nodesAs[Automata].filter(_.getValue < maxValue)
    nonFullAutomata(Random.nextInt(nonFullAutomata.length)).incrementValue()
  }

  protected def realMean: Double = nodesAs[Automata].map(_.getValue).sum.toDouble / numberOfNodes
}

object DriftingBalancerProblemControl extends Logging {
  val SEED = "random.seed"
  val ADD_ITEMS_STEP = "add_items_step";
}
