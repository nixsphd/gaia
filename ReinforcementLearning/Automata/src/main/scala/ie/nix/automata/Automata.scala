package ie.nix.automata

import ie.nix.automata.Automata.MAX_VALUE
import ie.nix.peersim.Node
import ie.nix.peersim.Utils.nodesAs
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.{CommonState, Control}

class Automata(prefix: String) extends Node(prefix) with Logging {

  val maxValue = Configuration.getInt(prefix + "." + MAX_VALUE)

  private var value: Int = randomValue

  logger trace s"[${CommonState.getTime}] => $this"

  override def toString: String = s"Automata-$getIndex{$value}"

  def getValue: Int = this.value

  def setValue(value: Int): Unit = this.value = clamp(value)

  def incrementValue(): Unit = setValue(getValue + 1)

  def decrementValue(): Unit = setValue(getValue - 1)

  def reset(): Unit = this.value = randomValue

  private def randomValue: Int = Automata.randomValue(maxValue)

  private def clamp(x: Int): Int = 0 max x min maxValue

}

object Automata extends Logging {

  implicit val toAutomata: peersim.core.Node => Automata = node => as[Automata](node)

  val MAX_VALUE = "max_value"

  class Init(val prefix: String) extends Control with Logging {

    logger trace s"[${CommonState.getTime}] => $this"

    def execute: Boolean = {
      nodesAs[Automata].foreach(_.reset)
      logger debug s"[${CommonState.getTime}] Reset Automata values"
      false
    }
  }

  def randomValue(maxValue: Int): Int = {
    // we want to allow maxValue, but nextInt bound is exclusive hence the "+ 1"
    CommonState.r.nextInt(maxValue + 1)
  }

}
