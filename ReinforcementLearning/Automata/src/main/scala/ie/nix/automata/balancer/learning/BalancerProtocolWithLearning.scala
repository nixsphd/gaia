package ie.nix.automata.balancer.learning

import ie.nix.automata.Automata
import ie.nix.automata.balancer.BalancerProtocol
import ie.nix.automata.balancer.BalancerProtocol.{AcceptMessage, OfferMessage}
import ie.nix.automata.balancer.learning.BalancerProtocolWithLearning.{
  Accept,
  BadBalancerState,
  BalancerState,
  DoNothing,
  Offer,
  Reject
}
import ie.nix.peersim.Node
import ie.nix.peersim.Protocol.Message
import ie.nix.peersim.learning.ProtocolLearning
import ie.nix.peersim.learning.ProtocolLearning.{NextCycle, ProcessEvent, ProtocolState, UnknownState}
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

class BalancerProtocolWithLearning(prefix: String) extends BalancerProtocol(prefix) with ProtocolLearning with Logging {

  logger info s"[${CommonState.getTime}]"

  override def toString =
    if (Option(getNode).isDefined) s"BalancerProtocolWithLearning-$getIndex"
    else "BalancerProtocolWithLearning-?"

  /*
   * Next Action
   */
  override def nextCycle(node: Node): Unit = super[ProtocolLearning].nextCycle(node)

  override def getNextCycleState(node: Node): State = node match {
    case automata: Automata => BalancerState(NextCycle, automata.getValue)
    case _                  => logger error s"Unexpected node, $node, expected Automata"; BadBalancerState
  }

  override def takeNextCycleAction(node: Node, action: Action): Unit = (node, action) match {
    case (_: Automata, Offer)     => sendMessage(new OfferMessage(automata), randomNeighbour)
    case (_: Automata, DoNothing) => // Do Nothing
    case (_: Automata, action) =>
      logger error s"Unexpected action, $action, expected Offer or DoNothing"; BadBalancerState
    case _ => logger error s"Unexpected node, $node, expected Automata"; BadBalancerState
  }

  /*
   * Process Event
   */
  override def processEvent(node: Node, message: Message): Unit = {
    logger trace s"[${CommonState.getTime}] $automata message $message"
    message match {
      case (_: AcceptMessage) => {
        decrementAutomataValue()
        logger trace s"[${CommonState.getTime}] $automata decrementAutomataValue"
      }
      case _ => super[ProtocolLearning].processEvent(node, message)
    }
  }

  override def getProcessEventState(node: Node, message: Message): BalancerState = node match {
    case automata: Automata => BalancerState(ProcessEvent, automata.getValue)
    case _                  => logger error s"Unexpected node, $node, expected Automata"; BadBalancerState
  }

  override def takeProcessEventAction(node: Node, message: Message, action: Action): Unit = {
    logger trace s"[${CommonState.getTime}] $automata action $action"
    (node, action) match {
      case (automata: Automata, Accept) => {
        incrementAutomataValue()
        logger trace s"[${CommonState.getTime}] $automata incrementedAutomataValue"
        sendMessage(new AcceptMessage(automata), message.from)
      }
      case (_: Automata, Reject) => // Do Nothing
      case (_: Automata, _)      => logger error s"Unexpected action, $action, expected Accept or Reject"
      case _                     => logger error s"Unexpected node, $node, expected Automata"; BadBalancerState
    }
  }

}

object BalancerProtocolWithLearning extends Logging {

  case class BalancerState(protocolState: ProtocolState, automataValue: Int) extends State {
    override def toString: String = s"$protocolState-$automataValue"
    override def isTerminal: Boolean = false
  }

  object BadBalancerState extends BalancerState(UnknownState, -1) {
    override def toString: String = "BadBalancerState"
  }

  case object Offer extends Action {
    override def toString: String = "Offer"
  }

  case object DoNothing extends Action {
    override def toString: String = "DoNothing"
  }

  case object Accept extends Action {
    override def toString: String = "Accept"
  }

  case object Reject extends Action {
    override def toString: String = "Reject"
  }

}
