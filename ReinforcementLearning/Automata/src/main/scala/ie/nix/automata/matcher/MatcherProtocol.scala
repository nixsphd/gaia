package ie.nix.automata.matcher

import ie.nix.automata.AutomataProtocol.AutomataMessage
import ie.nix.automata.matcher.MatcherProtocol.{MatcherMessage, TARGET_VALUE}
import ie.nix.automata.{Automata, AutomataProtocol}
import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

class MatcherProtocol(prefix: String) extends AutomataProtocol(prefix) with Logging {

  logger trace s"[${CommonState.getTime}] => $this"

  def targetValueFromConfig = peersim.config.Configuration.getInt(s"$prefix.$TARGET_VALUE")

  override def toString = if (Option(getNode).isDefined) s"MatchProtocol-$getNode" else "MatchProtocol-?"

  override def nextCycle(automata: Automata): Unit = {
    logger debug s"[${CommonState.getTime}] $automata sending messages"
    sendMessages(automata, new MatcherMessage(automata, messageValue))
  }

  def messageValue: Int = if (CommonState.r.nextBoolean()) -1 else 1

  override def processEvent(automata: Automata, message: AutomataMessage): Unit = {
    logger trace s"[${CommonState.getTime}] $this $automata $message"
    message match {
      case matcherMessage: MatcherMessage => processEvent(automata, matcherMessage)
      case _                              => logger error s"Expected MatcherMessage but got ${message.getClass.getSimpleName}"
    }
  }

  def processEvent(automata: Automata, matcherMessage: MatcherMessage): Unit = {
    logger trace s"[${CommonState.getTime}] $this $automata received matcherMessage $matcherMessage"
    (automata.getValue, matcherMessage.value) match {
      case (automataValue, 1) if (automataValue < targetValueFromConfig) =>
        setAutomataValue(automata.getValue + matcherMessage.value)
      case (automataValue, -1) if (automataValue > targetValueFromConfig) =>
        setAutomataValue(automata.getValue + matcherMessage.value)
      case _ => // Do Nothing
    }
  }
}

object MatcherProtocol {

  var TARGET_VALUE = "target_value"

  class MatcherMessage(from: Automata, val value: Int) extends AutomataMessage(from) {
    override def toString: String = "Message [" + value + "]"
  }

}
