package ie.nix.automata.drifting_balancer.learning

import ie.nix.automata.Automata
import ie.nix.automata.balancer.learning.BalancerProtocolWithLearning.BalancerState
import ie.nix.automata.balancer.learning.BalancerEnvironment.{
  HIT_REWARD,
  MAX_VALUE,
  MISS_PENALTY,
  getStateActions,
  getTerminalStates
}
import ie.nix.peersim.Utils.{nodesAs, numberOfNodes}
import ie.nix.peersim.learning.Environment
import ie.nix.rl.State
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

class DriftingBalancerEnvironment(prefix: String)
    extends Environment(prefix, getStateActions, getTerminalStates)
    with Logging {

  logger trace s"prefix=$prefix"

  val maxValue = Configuration.getInt(s"$prefix.$MAX_VALUE")
  val hitReward = Configuration.getDouble(s"$prefix.$HIT_REWARD")
  val missPenalty = Configuration.getDouble(s"$prefix.$MISS_PENALTY")

  override def toString: String =
    "BalancerEnvironment[" + stateActionsSet.toVector.sortBy(_.toString) + "]"

  def meanValue: Double = nodesAs[Automata].map(_.getValue).sum.toDouble / numberOfNodes

  def getReward(state: State): Double = {
    state match {
      case balancerState: BalancerState
          if ((balancerState.automataValue == math.floor(meanValue).toInt) ||
            (balancerState.automataValue == math.ceil(meanValue).toInt)) => {
        logger trace s"balancerState=$balancerState => $hitReward"
        hitReward
      }
      case balancerState: BalancerState => {
        logger trace s"balancerState=$balancerState => $missPenalty"
        missPenalty
      }
      case _ => {
        logger error s"expected $state to be of type AutomataState."
        0d
      }
    }
  }

}
