package ie.nix.automata.balancer

import ie.nix.automata.Automata
import ie.nix.automata.Automata.MAX_VALUE
import ie.nix.automata.balancer.BalancerProblemControl.{MEAN_VALUE, SEED}
import ie.nix.automata.balancer.Util.runPeerSim
import ie.nix.automata.balancer.learning.BalancerPolicyDiffObserver.getIdealPolicy
import ie.nix.peersim.Utils.{getProtocolsAs, nodes, nodesAs, numberOfNodes}
import ie.nix.peersim.learning.Learner
import ie.nix.rl.tabular.policy.Policy.difference
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.{CommonState, Control}

import scala.util.Random

object BalancerProblem extends App with Logging {

  val AUTOMATA_PARAMS = "src/main/resources/BalancerProblem.properties"
  val properties = Array[String]("-file", AUTOMATA_PARAMS)

  peersim.Simulator.main(properties)

  val meanValue = peersim.config.Configuration.getInt("control.nodes.mean_value")
  val numberCorrect = nodesAs[Automata].count(_.getValue == meanValue)
  logger info nodes.mkString(",")
  logger info s"meanValue=$meanValue, numberCorrect=$numberCorrect, numberOfNodes=$numberOfNodes"

}

object BalancerProblemWithQLearning extends App {
  runPeerSim("src/main/resources/BalancerProblemWithQLearning.properties")
}

object BalancerProblemWithQLambdaLearning extends App with Logging {
  runPeerSim("src/main/resources/BalancerProblemWithQLambdaLearning.properties")
}

object BalancerProblemWithQDLambdaLearning extends App with Logging {
  runPeerSim("src/main/resources/BalancerProblemWithQDLambdaLearning.properties")
}

object Util extends Logging {

  def learners: Vector[Learner] =
    getProtocolsAs[Learner](learningProtocolID)

  def learningProtocolID: Int = Configuration.lookupPid("learner")
  def idealPolicy = getIdealPolicy(learners(0).agent.environment)
  def learnerDifferences = learners.map(learner => (learner, difference(learner.agent.policy, idealPolicy)))
  def totalLearnerDifferences: Int = learnerDifferences.map(_._2).sum
  def numberCorrect: Int = learnerDifferences.count(_._2 == 0)

  def runPeerSim(propertiesFile: String) = {
    val properties = Array[String]("-file", propertiesFile)
    peersim.Simulator.main(properties)
    logger info learnerDifferences.mkString(",")
    logger info s"differences=$totalLearnerDifferences, numberCorrect=$numberCorrect, numberOfNodes=$numberOfNodes"
  }
}

class BalancerProblemControl(val prefix: String) extends Control with Logging {

  lazy val seed: Long = Configuration.getLong(s"$SEED")
  lazy val maxValue = Configuration.getInt(s"network.node.$MAX_VALUE")
  lazy val meanValue = Configuration.getInt(s"$prefix.$MEAN_VALUE")

  Random.setSeed(Configuration.getLong("random.seed"))
  logger debug s"[${CommonState.getTime}] seed=$seed, maxValue=$maxValue, meanValue=$meanValue"

  def execute: Boolean = {
    nodesAs[Automata].foreach(_.setValue(meanValue))
    shuffleItems()

    val realMean = nodesAs[Automata].map(_.getValue).sum / numberOfNodes
    logger debug s"[${CommonState.getTime}] Reset Automata values realMean=$realMean, ${nodesAs[Automata].mkString(", ")}"
    false
  }

  protected def shuffleItems(): Unit = shuffleItems(Random.shuffle(nodesAs[Automata].toList))

  protected def shuffleItems(automata: List[Automata]): Unit = automata match {
    case Nil                                    => // Do nothing
    case _ :: Nil                               => // Do nothing
    case anAutomaton :: anotherAutomaton :: Nil => shuffleItemsBetweenTwoAutomata(anAutomaton, anotherAutomaton)
    case anAutomaton :: anotherAutomaton :: tail => {
      shuffleItemsBetweenTwoAutomata(anAutomaton, anotherAutomaton)
      shuffleItems(tail)
    }
  }

  protected def shuffleItemsBetweenTwoAutomata(anAutomaton: Automata, anotherAutomaton: Automata): Unit = {
    val maxNumberToShuffle = Math.min(anAutomaton.getValue, maxValue - anotherAutomaton.getValue)
    // nextInt is exclusive so we +1 the result also ensuring the the shuffle number is never 0

    if (maxNumberToShuffle > 0) {
      val numberToShuffle = Random.nextInt(maxNumberToShuffle) + 1
      logger trace s"maxValue=$maxValue, maxNumberToShuffle=$maxNumberToShuffle numberToShuffle=$numberToShuffle"

      anAutomaton.setValue(anAutomaton.getValue - numberToShuffle)
      anotherAutomaton.setValue(anotherAutomaton.getValue + numberToShuffle)

    }
  }

}

object BalancerProblemControl extends Logging {

  val SEED = "random.seed"
  var MEAN_VALUE = "mean_value"

}
