package ie.nix.automata.balancer.learning

import ie.nix.automata.balancer.learning.BalancerEnvironment.{
  HIT_REWARD,
  MAX_VALUE,
  MEAN_VALUE,
  MISS_PENALTY,
  getStateActions,
  getTerminalStates
}
import ie.nix.automata.balancer.learning.BalancerProtocolWithLearning.{Accept, BalancerState, DoNothing, Offer, Reject}
import ie.nix.peersim.learning.Environment
import ie.nix.peersim.learning.Environment.{GetStateActions, GetTerminalStates}
import ie.nix.peersim.learning.ProtocolLearning.{NextCycle, ProcessEvent}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.State
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

class BalancerEnvironment(prefix: String) extends Environment(prefix, getStateActions, getTerminalStates) with Logging {

  logger trace s"prefix=$prefix"

  val maxValue = Configuration.getInt(s"$prefix.$MAX_VALUE")
  val meanValue = Configuration.getInt(s"$prefix.$MEAN_VALUE")
  val hitReward = Configuration.getDouble(s"$prefix.$HIT_REWARD")
  val missPenalty = Configuration.getDouble(s"$prefix.$MISS_PENALTY")

  override def toString: String =
    "BalancerEnvironment[" + stateActionsSet.toVector.sortBy(_.toString) + "]"

  def getReward(state: State): Double = {
    state match {
      case balancerState: BalancerState if (balancerState.automataValue == meanValue) => {
        logger debug s"balancerState=$balancerState => $hitReward"
        hitReward
      }
      case balancerState: BalancerState => {
        logger debug s"balancerState=$balancerState => $missPenalty"
        missPenalty
      }
      case _ => {
        logger error s"expected $state to be of type AutomataState."
        0d
      }
    }
  }

}

object BalancerEnvironment extends Logging {

  val MAX_VALUE: String = "max_value"
  val MEAN_VALUE: String = "mean_value"

  val HIT_REWARD: String = "reward"
  val MISS_PENALTY: String = "penalty"

  val getStateActions: GetStateActions = prefix => {
    val maxValue = Configuration.getInt(s"$prefix.$MAX_VALUE")
    logger trace s"$prefix], maxValue=$maxValue"
    val stateActions = Set[StateAction]() +
      StateAction(BalancerState(NextCycle, 0), DoNothing) ++
      (for {
        value <- 1 to maxValue
        action <- List(Offer, DoNothing)
      } yield {
        StateAction(BalancerState(NextCycle, value), action)
      }) ++
      (for {
        value <- 0 to maxValue - 1
        action <- List(Accept, Reject)
      } yield {
        StateAction(BalancerState(ProcessEvent, value), action)
      }) +
      StateAction(BalancerState(ProcessEvent, maxValue), Reject)
    stateActions
  }

  val getTerminalStates: GetTerminalStates = prefix => {
    logger trace s"$prefix"
    Set[State]()
  }

}
