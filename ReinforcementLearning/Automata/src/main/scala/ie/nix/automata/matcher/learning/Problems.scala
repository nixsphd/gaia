package ie.nix.automata.matcher.learning

import ie.nix.automata.Automata
import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.{AutomataState, LEARNER}
import ie.nix.peersim.Utils.{getProtocolAs, nodesAs}
import ie.nix.peersim.learning.Learner
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.Policy
import ie.nix.rl.tabular.policy.avf.{ActionValueFunction, EligibilityTracing}
import ie.nix.util.CSVFileWriter
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

object Problems extends App with Logging {

  private def learningProtocolID: Int = Configuration.getPid(s"protocol.matcher.$LEARNER")

  def writePolicies(filename: String): Unit = {
    val csvFileWriter = CSVFileWriter("results", filename)
    csvFileWriter.writeHeaders("Id", "AutomataValue", "MessageValue", "Action")
    nodesAs[Automata]
      .map(automata => (automata.getIndex, getProtocolAs[Learner](automata, learningProtocolID).agent.policy))
      .foreach(indexAndPolicy => writeAutomataPolicy(csvFileWriter, indexAndPolicy._1, indexAndPolicy._2))
  }

  def writeAutomataPolicy(csvFileWriter: CSVFileWriter, id: Int, policy: Policy): Unit = {
    for {
      state <- policy.getStates
    } state match {
      case automataState: AutomataState =>
        csvFileWriter.writeRow(id,
                               automataState.automataValue,
                               automataState.messageValue,
                               policy.getMostProbableAction(state))
    }
  }

  def writeActionValueFunctions(filename: String): Unit = {
    val csvFileWriter = CSVFileWriter("results", filename)
    csvFileWriter.writeHeaders("Id", "AutomataValue", "MessageValue", "Action", "Value", "Eligibility")
    nodesAs[Automata]
      .map(automata =>
        (automata.getIndex, getProtocolAs[Learner](automata, learningProtocolID).agent.actionValueFunction))
      .foreach(indexAndActionValueFunction =>
        indexAndActionValueFunction match {
          case (index, actionValueFunction: ActionValueFunction) =>
            writeActionValueFunction(csvFileWriter, index, actionValueFunction)
          case (index, eligibilityTracings: EligibilityTracing) =>
            writeEligibilityTracing(csvFileWriter, index, eligibilityTracings)
          case _ =>
      })
  }

  def writeEligibilityTracing(csvFileWriter: CSVFileWriter, id: Int, eligibilityTracing: EligibilityTracing): Unit = {
    for {
      stateAction <- eligibilityTracing.getStateActions
    } stateAction match {
      case StateAction(automataState: AutomataState, action) =>
        val (value, eligibility) = eligibilityTracing.getValueAndEligibility(stateAction)
        csvFileWriter.writeRow(id, automataState.automataValue, automataState.messageValue, action, value, eligibility)
      case _ => ()
    }
  }

  def writeActionValueFunction(csvFileWriter: CSVFileWriter,
                               id: Int,
                               actionValueFunction: ActionValueFunction): Unit = {
    for {
      stateAction <- actionValueFunction.getStateActions
    } stateAction match {
      case StateAction(automataState: AutomataState, action) =>
        val value = actionValueFunction.getValue(stateAction)
        csvFileWriter.writeRow(id, automataState.automataValue, automataState.messageValue, action, value)
      case _ => ()
    }
  }

}
