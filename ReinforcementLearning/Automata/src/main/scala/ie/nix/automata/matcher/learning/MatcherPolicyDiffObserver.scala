package ie.nix.automata.matcher.learning

import ie.nix.automata.matcher.learning.MatcherPolicyDiffObserver.getIdealPolicy
import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.{AutomataState, DoNothing, TakeAction}
import ie.nix.peersim.learning.Environment
import ie.nix.peersim.learning.observers.PolicyDiffObserver
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

class MatcherPolicyDiffObserver(prefix: String) extends PolicyDiffObserver(prefix) {

  override def idealPolicy(environment: Environment): DeterministicPolicy = getIdealPolicy(environment)

}

object MatcherPolicyDiffObserver extends Logging {

  def getIdealPolicy(environment: Environment): DeterministicPolicy = {
    val matcherEnvironment: MatcherEnvironment = as[MatcherEnvironment](environment)
    matcherEnvironment.getNonTerminalStates.foldLeft(DeterministicPolicy(matcherEnvironment))((policy, state) => {
      state match {
        case AutomataState(automataValue, 1, false) if (automataValue < matcherEnvironment.targetValue) =>
          policy updateAction StateAction(state, TakeAction)
        case AutomataState(automataValue, -1, false) if (automataValue > matcherEnvironment.targetValue) =>
          policy updateAction StateAction(state, TakeAction)
        case _: AutomataState =>
          policy updateAction StateAction(state, DoNothing)
        case _ =>
          logger error s"Expected $state to be of type AutomataState"
          policy
      }
    })
  }

}
