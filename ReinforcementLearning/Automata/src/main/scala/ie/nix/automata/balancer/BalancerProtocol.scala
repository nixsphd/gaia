package ie.nix.automata.balancer

import ie.nix.automata.AutomataProtocol.AutomataMessage
import ie.nix.automata.balancer.BalancerProtocol.{BalancerMessage, MEAN_VALUE, AcceptMessage, OfferMessage}
import ie.nix.automata.{Automata, AutomataProtocol}
import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

class BalancerProtocol(prefix: String) extends AutomataProtocol(prefix) with Logging {

  def meanValueFromConfig = peersim.config.Configuration.getInt(s"$prefix.$MEAN_VALUE")

  override def toString = if (Option(getNode).isDefined) s"BalancerProtocol-$getNode" else "BalancerProtocol-?"

  override def nextCycle(automata: Automata): Unit = {
    if (automata.getValue > meanValueFromConfig) {
      val randomNeighbour = randomNeighbourAs[Automata]
      logger debug s"[${CommonState.getTime}] $automata sending OfferMessage messages to $randomNeighbour"
      sendMessage(new OfferMessage(automata), randomNeighbour)
    }
  }

  override def processEvent(automata: Automata, message: AutomataMessage): Unit = {
    logger trace s"[${CommonState.getTime}] $this $automata received message $message"
    message match {
      case balancerMessage: BalancerMessage => processEvent(automata, balancerMessage)
      case _                                =>
    }
  }

  def processEvent(automata: Automata, balancerMessage: BalancerMessage): Unit = {
    balancerMessage match {
      case offerMessage: OfferMessage if (automata.getValue < meanValueFromConfig) => {
        logger trace s"[${CommonState.getTime}] $this $offerMessage accepted by $automata"
        incrementAutomataValue()
        logger trace s"[${CommonState.getTime}] $this AcceptMessage ${automata.getValue}"
        sendMessage(new AcceptMessage(automata), offerMessage.from)
      }
      case offerMessage: OfferMessage => {
        logger trace s"[${CommonState.getTime}] $this $offerMessage ignored by $automata"
      }
      case _: AcceptMessage => {
        decrementAutomataValue()
        logger trace s"[${CommonState.getTime}] $this AcceptMessage"
      }
    }

  }

}

object BalancerProtocol {

  var MEAN_VALUE = "mean_value"

  class BalancerMessage(from: Automata) extends AutomataMessage(from) {
    override def toString: String = "BalancerMessage"
  }

  class OfferMessage(from: Automata) extends BalancerMessage(from) {
    override def toString: String = "OfferMessage"
  }

  class AcceptMessage(from: Automata) extends BalancerMessage(from) {
    override def toString: String = "AcceptMessage"
  }

}
