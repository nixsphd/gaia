package ie.nix.automata.drifting_balancer.learning

import ie.nix.automata.balancer.learning.BalancerProtocolWithLearning._
import ie.nix.automata.drifting_balancer.learning.DriftingBalancerPolicyDiffObserver.getIdealPolicy
import ie.nix.peersim.learning.Environment
import ie.nix.peersim.learning.ProtocolLearning.{NextCycle, ProcessEvent}
import ie.nix.peersim.learning.observers.PolicyDiffObserver
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.tabular.policy.DeterministicPolicy
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

class DriftingBalancerPolicyDiffObserver(prefix: String) extends PolicyDiffObserver(prefix) {

  override def idealPolicy(environment: Environment): DeterministicPolicy = getIdealPolicy(environment)

}

object DriftingBalancerPolicyDiffObserver extends Logging {

  def getIdealPolicy(environment: Environment): DeterministicPolicy = {
    val driftingBalancerEnvironment: DriftingBalancerEnvironment = as[DriftingBalancerEnvironment](environment)
    val meanValue = driftingBalancerEnvironment.meanValue
    val idealPolicy =
      driftingBalancerEnvironment.getNonTerminalStates.foldLeft(DeterministicPolicy(environment))((policy, state) => {
        state match {
          case BalancerState(NextCycle, automataValue) if (automataValue > math.ceil(meanValue).toInt) =>
            policy updateAction StateAction(state, Offer)
          case BalancerState(NextCycle, _) =>
            policy updateAction StateAction(state, DoNothing)
          case BalancerState(ProcessEvent, automataValue) if (automataValue < math.floor(meanValue).toInt) =>
            policy updateAction StateAction(state, Accept)
          case BalancerState(ProcessEvent, _) =>
            policy updateAction StateAction(state, Reject)
          case _ =>
            logger error s"Expected $state to be of type AutomataState"
            policy
        }
      })
    logger trace s"meanValue=$meanValue, idealPolicy=$idealPolicy"
    idealPolicy
  }

}
