package ie.nix.automata.matcher.learning

import ie.nix.automata.Automata
import ie.nix.automata.matcher.MatcherProtocol
import ie.nix.automata.matcher.MatcherProtocol.MatcherMessage
import ie.nix.automata.matcher.learning.MatcherProtocolWithLearning.{AutomataState, DoNothing, LEARNER, TakeAction}
import ie.nix.peersim.Utils.getProtocolAs
import ie.nix.peersim.learning.Learner
import ie.nix.peersim.learning.learners.IndependentLearner
import ie.nix.rl.{Action, State}
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

class MatcherProtocolWithLearning(prefix: String) extends MatcherProtocol(prefix) with Logging {

  logger trace s"[${CommonState.getTime}] => $this"

  private val learningProtocolID = Configuration.getPid(s"$prefix.$LEARNER")
  logger trace s"[${CommonState.getTime}] protocolID=$protocolID learningProtocolID=$learningProtocolID"

  def learner: Learner =
    getProtocolAs[IndependentLearner](getNode, learningProtocolID)

  override def toString =
    if (Option(getNode).isDefined) s"MatcherProtocolWithIndependentLearner-$getIndex"
    else "MatcherProtocolWithIndependentLearner-?"

  override def processEvent(automata: Automata, message: MatcherMessage): Unit = {
    logger trace s"[${CommonState.getTime}] $this $automata received message $message"
    val state = getState(automata, message)
    logger trace s"[${CommonState.getTime}] $this $automata state=$state"
    val action = learner.getAction(state)
    logger trace s"[${CommonState.getTime}] $this $automata state=$state, action=$action"
    takeAction(automata, message, action)
  }

  def getState(automata: Automata, message: MatcherMessage): AutomataState = {
    AutomataState(automata.getValue, message.value)
  }

  def takeAction(automata: Automata, message: MatcherMessage, action: Action): Unit = {
    logger trace s"[${CommonState.getTime}] $this $action"
    action match {
      case TakeAction => setAutomataValue(automata.getValue + message.value)
      case DoNothing  =>
      case _ =>
        logger error s"[${CommonState.getTime}] expected ${action} to be either Increment or DoNothing or Decrement"
    }
    logger trace s"[${CommonState.getTime}] $this $action => $automata"
  }

}

object MatcherProtocolWithLearning extends Logging {
  val LEARNER = "learner"

  case class AutomataState(automataValue: Int, messageValue: Int, override val isTerminal: Boolean = false)
      extends State {
    override def toString: String = messageValue match {
      case 1  => s"Automata[Inc $automataValue]"
      case -1 => s"Automata[Dec $automataValue]"
    }
  }

  case object DoNothing extends Action {
    override def toString: String = "DoNothing"
  }

  case object TakeAction extends Action {
    override def toString: String = "TakeAction"
  }

}
