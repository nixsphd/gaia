package ie.nix.util

import java.lang.Double.parseDouble

import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

object DataFrame {

  def apply(headers: Vector[String]): DataFrame = {
    new DataFrame(Vector(headers))
  }

  implicit val asDouble: String => Double = parseDouble

}

class DataFrame(data: Vector[Vector[String]]) extends Logging {

  override def toString: String = {
    val dataAsAString = for { row <- data } yield {
      row.mkString("Vector(", ",", ")")
    }
    s"DataFrame data=$dataAsAString"
  }

  def numberOfRows: Int = {
    data.size
  }

  def numberOfRowsOfData: Int = {
    data.size - 1
  }

  def numberOfColumns: Int = {
    data(0).size
  }

  def getRow(row: Int): Vector[String] = {
    data(row)
  }

  def getRowAs[T](row: Int)(implicit asT: String => T): Vector[T] = {
    data(row).map(value => asT.apply(value))
  }

  def getColumns: Vector[Vector[String]] = {
    headers.map(header => getColumn(header))
  }

  def getColumn(header: String): Vector[String] = {
    getColumn(headers.indexOf(header))
  }

  def getColumn(column: Int): Vector[String] = {
    data.tail.map(row => row(column))
  }

  def getColumnsAs[T]()(implicit asT: String => T): Vector[Vector[T]] = {
    headers.map(header => getColumnAs[T](header))
  }

  def getColumnAs[T](header: String)(implicit asT: String => T): Vector[T] = {
    getColumnAs[T](headers.indexOf(header))(asT)
  }

  def getColumnAs[T](column: Int)(implicit asT: String => T): Vector[T] = {
    data.tail.map(row => asT.apply(row(column)))
  }

  def headers: Vector[String] = {
    data(0)
  }

  def get(row: Int, column: Int): String = {
    data(row)(column)
  }

  def getAs[T](row: Int, column: Int)(implicit asT: String => T): T = {
    asT.apply(data(row)(column))
  }

  def addRow(row: Vector[String]): DataFrame = {
    require(row.size === headers.size)
    new DataFrame(data :+ row)
  }

  def dropColumn(columnHeader: String): DataFrame = {
    subset(headers.filter(_ !== columnHeader))
  }

  def subset(headersToSubset: Vector[String]): DataFrame = {
    val columnsToSubset = headersToSubset.map(header => headers.indexOf(header))
    logger trace s"~headersToSubset=$headersToSubset, columnsToSubset=$columnsToSubset"
    subsetColumns(columnsToSubset)
  }

  def subsetColumns(columnsToSubset: Vector[Int]): DataFrame = {

    def subsetRow(row: Vector[String], columnsToSubset: Vector[Int]): Vector[String] = {
      for {
        (column, index) <- row.zipWithIndex if columnsToSubset.contains(index)
      } yield {
        column
      }
    }

    val subsetData = data.map(row => subsetRow(row, columnsToSubset))
    logger trace s"~subsetData=$subsetData"
    new DataFrame(subsetData)
  }

  def filter(header: String, value: String): DataFrame = {
    val column = headers.indexOf(header)
    new DataFrame(data.filter(row => row === data(0) || row(column) === value))
  }

}
