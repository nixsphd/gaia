package ie.nix.util

import java.nio.file.{FileSystems, Files, Path, StandardOpenOption}
import java.util.StringJoiner

import org.apache.logging.log4j.scala.Logging

import scala.util.{Failure, Success, Try}

trait CSVFileWriter {

  def writeHeaders(headersString: String): Unit

  def writeHeaders(headersList: List[Any]): Unit

  def writeHeaders(headers: Any*): Unit

  def writeRow(rowString: String): Unit

  def writeRow(rowItemsList: List[Any]): Unit

  def writeRow(rowItems: Any*): Unit

}

object CSVFileWriter {

  def apply(logDirName: String, logFileName: String): CSVFileWriter = {
    new CSVFileWriterImpl(logDirName, logFileName)
  }

  def apply(logFileName: String): CSVFileWriter = {
    new CSVFileWriterImpl(".", logFileName)
  }

  def PreciseCSVFileWriter(logDirName: String, logFileName: String, precision: Int): CSVFileWriter = {
    new CSVFileWriterImpl(logDirName, logFileName) {
      override def doublePrecision: Int = precision
    }
  }

}

@SuppressWarnings(Array("org.wartremover.warts.Null"))
class CSVFileWriterImpl(val logDirName: String, val logFileName: String) extends CSVFileWriter with Logging {

  private val logDir: Path = FileSystems.getDefault.getPath(logDirName)
  ensureDirExists(logDir)

  private val logFile: Path = logDir.resolve(logFileName)
  // Smell - Is this really necessary, the write headers function will truncate the file.
  ensureFileTruncated(logFile)

  logger.info(s"Logging to $logFile")

  def writeHeaders(headers: Any*): Unit = {
    writeHeaders(headers.toList)
  }

  def writeHeaders(headersList: List[Any]): Unit = {
    writeHeaders(generateCSVString(headersList))
  }

  def writeHeaders(headersString: String): Unit = {
    Try[Path] {
      logger.debug(s"Writing headers ~$headersString")
      Files.write(
        logFile,
        s"$headersString\n".getBytes,
        StandardOpenOption.CREATE,
        StandardOpenOption.TRUNCATE_EXISTING
      )
    } match {
      case Success(_) => ()
      case Failure(exception) =>
        logger.error(s"~Error writing headers to logfile $logFile", exception)
    }
  }

  def writeRow(rowItems: Any*): Unit = {
    writeRow(rowItems.toList)
  }

  def writeRow(rowItemsList: List[Any]): Unit = {
    val rowString = generateCSVString(rowItemsList)
    writeRow(rowString)
  }

  def writeRow(rowString: String): Unit = {
    Try[Path] {
      logger.debug(s"Writing row ~$rowString")
      Files.write(
        logFile,
        s"$rowString\n".getBytes,
        StandardOpenOption.CREATE,
        StandardOpenOption.APPEND
      )
    } match {
      case Success(_) => ()
      case Failure(exception) =>
        logger.error(s"~Error writing row to logfile $logFile", exception)
    }
  }

  protected def getName: String = this.getClass.getSimpleName

  protected def ensureDirExists(logDir: Path): Unit = {
    if (!Files.exists(logDir)) {
      Try[Path] {
        logger.trace(s"~Creating $logDir")
        Files.createDirectories(logDir)
      } match {
        case Success(_) => ()
        case Failure(exception) =>
          logger.error(s"~Error creating logDir $logDir", exception)
      }
    }
  }

  protected def ensureFileTruncated(logFile: Path): Unit = {
    if (Files.exists(logFile)) {
      Try[Path] {
        logger.debug(s"~Truncating $logFile")
        Files.write(
          logFile,
          "".getBytes,
          StandardOpenOption.CREATE,
          StandardOpenOption.TRUNCATE_EXISTING
        )
      } match {
        case Success(_) => ()
        case Failure(exception) =>
          logger.error(s"~Error truncating logfile $logDir", exception)
      }
    }
  }

  def generateCSVString(row: List[Any]): String = {

    val rowString = new StringJoiner(",", "", "")
    row.foreach((any: Any) =>
      any match {
        case double: Double => rowString.add(formatDouble(double))
        case _              => rowString.add(any.toString)
    })
    rowString.toString
  }

  def formatDouble(value: Double): String = s"%.${doublePrecision}f".format(value)

  def doublePrecision: Int = 2

}
