#!/bin/sh 

#SBATCH --time=12:00:00
#SBATCH --nodes=1
#SBATCH -A ngcom014c
#SBATCH -p ProdQ


module load r/3.4.4 java/8 taskfarm

export EXP=exp3
export MODEL=Sercon
export PROJECT_ROOT=/ichec/home/users/nix/gaia/Cloud/DVMC/
export RESULTS_ROOT=$PROJECT_ROOT/results/$EXP/$MODEL/

cd $PROJECT_ROOT
taskfarm kay/tasks
