#!/bin/sh 

#SBATCH --time=00:20:00
#SBATCH --nodes=1
#SBATCH -A nuig02
#SBATCH -p DevQ


module load r/3.4.4 java/8 taskfarm

export EXP=exp2
export MODEL=GC
export PROJECT_ROOT=/ichec/home/users/nix/gaia/Cloud/DVMC/
export RESULTS_ROOT=$PROJECT_ROOT/results/$EXP/$MODEL/

cd $PROJECT_ROOT
taskfarm kay/tasks