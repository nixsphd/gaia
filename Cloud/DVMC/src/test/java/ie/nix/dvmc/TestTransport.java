package ie.nix.dvmc;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;

public class TestTransport implements Transport {

	static final Logger LOGGER = LogManager.getLogger();
	
	public class Event {
		
		public Node src;
		public Node dest;
		public Object msg;
		public int pid;
		
		public Event(Node src, Node dest, Object msg, int pid) {
			super();
			this.src = src;
			this.dest = dest;
			this.msg = msg;
			this.pid = pid;
		}
		
	}
	
	public static List<Event> events;
	
	public TestTransport(String prefix) {
		events = new ArrayList<Event>();
	}
	
	@Override
	public TestTransport clone() {
		TestTransport clone = null;
		try {
			clone = (TestTransport)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	    LOGGER.trace("[0]");	
		return clone;
	}

	@Override
	public String toString() {
		return "TestTransport";
	}

	@Override
	public void send(Node src, Node dest, Object msg, int pid) {
	    LOGGER.debug("~src={}, dest={}, msg={}, pid={}", src, dest, msg, pid);	
	    events.add(new Event(src, dest, msg, pid));
	    
		EDProtocol edProtocol = (EDProtocol)dest.getProtocol(pid);
		edProtocol.processEvent(dest, pid, msg);
		
	}

	@Override
	public long getLatency(Node src, Node dest) {
		return 0;
	}
}