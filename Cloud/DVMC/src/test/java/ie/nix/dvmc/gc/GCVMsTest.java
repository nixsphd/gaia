package ie.nix.dvmc.gc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.TestTransport;
import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.gc.BidMessage;
import ie.nix.gc.TenderMessage;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class GCVMsTest {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	protected static final String DVMC_PROTOCOL = "dvmc_strategy";

	protected static int protocolID;
	
	protected static CloudDataCenter cdc;
	
	int random_seed = 1;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/exp.properties",
				"src/test/java/ie/nix/dvmc/gc/exp.properties",
				"src/main/resources/GC_VMs.properties"});
		
		// get the GC Protocol id.
		protocolID = Configuration.lookupPid(DVMC_PROTOCOL);
		cdc = CloudDataCenter.getCloudDataCenter();
	}

	@BeforeEach
	void beforeEach() {
		TestTransport.events.clear();
		cdc.provisionVMs();
		CommonState.r.setSeed(1);
	}

	@AfterEach
	void afterEach() {}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	/*
	 * boolean amIOverloaded(Host host)
	 */
	@Test
    @DisplayName("Am, I overloaded True")
    public void amIOverloadedTrueTest() {

		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		double overloadedUtilizationThreshold = 0.4;
		boolean expectedOverloaded = true;
		
		amIOverloadedTest(hostToCheck, overloadedUtilizationThreshold, expectedOverloaded);
		
		overloadedUtilizationThreshold = 0.5;
		expectedOverloaded = true;
		
		amIOverloadedTest(hostToCheck, overloadedUtilizationThreshold, expectedOverloaded);
		
    }
	
	@Test
    @DisplayName("Am I overloaded False")
    public void amIOverloadedFalseTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationHighThreshold = 0.9;
		boolean expectedOverloaded = false;
		
		amIOverloadedTest(hostToCheck, migrationHighThreshold, expectedOverloaded);
		
    }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void amIOverloadedTest(Host hostToCheck, double overloadedUtilizationThreshold, boolean expectedOverloaded) {
		GCDVMC gcnet = (GCDVMC)hostToCheck.getProtocol(protocolID);
		gcnet.overloadedThreshold = overloadedUtilizationThreshold;
		
		boolean actualOverloaded = gcnet.amIOverloaded(hostToCheck);
		LOGGER.debug("[{}]~actualOverloaded={}", CommonState.getTime(), actualOverloaded);

		Assertions.assertEquals(expectedOverloaded, actualOverloaded);
	}
	
	/*
	 * boolean amIBalanced(Host host)
	 */
	@Test
    @DisplayName("Am I underloaded True")
    public void amIBalancedTrueTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		double underloadedThreshold = 0.4;
		double overloadedThreshold = 0.9;
		boolean expectedBalanced = true;
		
		amIBalancedTest(hostToCheck, underloadedThreshold, overloadedThreshold, expectedBalanced);
		
    }	
	
	@Test
    @DisplayName("Am I underloaded True Low RAM")
    public void amIBalancedTrueTrueLowRAM() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		double underloadedThreshold = 0.5;
		double overloadedThreshold = 0.9;
		boolean expectedBalanced = true;
		
		amIBalancedTest(hostToCheck, underloadedThreshold, overloadedThreshold, expectedBalanced);
		
    }
	
	@Test
    @DisplayName("Am I underloaded True High RAM")
    public void amIBalancedTrueTrueHighRAM() {

		Host hostToCheck = cdc.getHosts().get(3);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		double underloadedThreshold = 0.3;
		double overloadedThreshold = 0.35;
		boolean expectedBalanced = true;
		
		amIBalancedTest(hostToCheck, underloadedThreshold, overloadedThreshold, expectedBalanced);
		
    }
	
	@Test
    @DisplayName("Am I underloaded True Low CPU")
    public void amIBalancedTrueTrueLowCPU() {

		Host hostToCheck = cdc.getHosts().get(3);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		double underloadedThreshold = 0.3;
		double overloadedThreshold = 0.4;
		boolean expectedBalanced = true;
		
		amIBalancedTest(hostToCheck, underloadedThreshold, overloadedThreshold, expectedBalanced);
		
    }
	
	@Test
    @DisplayName("Am I underloaded False High CPU")
    public void amIBalancedTrueFalseHighCPU() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		double underloadedThreshold = 0.4;
		double overloadedThreshold = 0.5;
		boolean expectedBalanced = false;
		
		amIBalancedTest(hostToCheck, underloadedThreshold, overloadedThreshold, expectedBalanced);
		
    }
	
	@Test
    @DisplayName("Am I underloaded True Edge")
    public void amIBalancedTrueEdge() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		double underloadedThreshold = hostToCheck.getRAMUtil();
		double overloadedThreshold = hostToCheck.getCPUUtil();
		boolean expectedBalanced = true;
		
		amIBalancedTest(hostToCheck, underloadedThreshold, overloadedThreshold, expectedBalanced);
		
    }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void amIBalancedTest(Host hostToCheck, double underloadedThreshold, 
			double overloadedThreshold, boolean expectedBalanced) {
		GCDVMC gcnet = (GCDVMC)hostToCheck.getProtocol(protocolID);
		gcnet.underloadedThreshold = underloadedThreshold;
		gcnet.overloadedThreshold = overloadedThreshold;
		
		boolean actualBalanced = gcnet.amIBalanced(hostToCheck);

		Assertions.assertEquals(expectedBalanced, actualBalanced);
	}
	
	/*
	 * boolean amIUnderloaded(Host host)
	 */
	@Test
    @DisplayName("Am I underloaded True")
    public void amIUnderloadedTrueTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double underloadedUtilizationThreshold = 0.6;
		boolean expectedUnderloaded = true;
		
		amIUnderloadedTest(hostToCheck, underloadedUtilizationThreshold, expectedUnderloaded);
		
    }
	
	@Test
    @DisplayName("Am I underloaded False")
    public void amIUnderloadedFalseTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double underloadedUtilizationThreshold = 0.4;
		boolean expectedUnderloaded = false;
		
		amIUnderloadedTest(hostToCheck, underloadedUtilizationThreshold, expectedUnderloaded);
		
    }
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void amIUnderloadedTest(Host hostToCheck, double underloadedUtilizationThreshold, boolean expectedUnderloaded) {
		GCDVMC gcnet = (GCDVMC)hostToCheck.getProtocol(protocolID);
		gcnet.underloadedThreshold = underloadedUtilizationThreshold;
		
		boolean actualUnderloaded = gcnet.amIUnderloaded(hostToCheck);

		Assertions.assertEquals(expectedUnderloaded, actualUnderloaded);
	}
	
	/*
	 * List<VM> selectVMsToMigrate(Host host) {
	 */
	@Test
    @DisplayName("Select VMs to Migrate One")
    public void selectVMsToMigrateOneTest() {
		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, vms={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.vmList());
		
		double overloadedUtilizationThreshold = 0.5;
		
		List<VM> expectedVMsToMigrate = new ArrayList<VM>();
		expectedVMsToMigrate.add(cdc.getVMs().get(0));
		
		selectVMsToMigrateTest(hostToCheck, overloadedUtilizationThreshold, expectedVMsToMigrate);
	}
	
//	@Test
//    @DisplayName("Select VMs to Migrate One Big One")
//    public void selectVMsToMigrateOneBigOneTest() {
//		// [cpu=0.58, ram=0.44]
//		Host hostToCheck = cdc.getHosts().get(0);
//		LOGGER.debug("[{}]~hostToCheck={}, vms={}", 
//				CommonState.getTime(), hostToCheck, hostToCheck.vmList());
//		
//		double overloadedUtilizationThreshold = 0.40;
//		
//		List<VM> expectedVMsToMigrate = new ArrayList<VM>();
//		expectedVMsToMigrate.add(cdc.getVMs().get(2));
//		
//		selectVMsToMigrateTest(hostToCheck, overloadedUtilizationThreshold, expectedVMsToMigrate);
//	}
	
	@Test
    @DisplayName("Select VMs to Migrate two")
    public void selectVMsToMigrateTwoTest() {
		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, vms={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.vmList());
		
		double overloadedUtilizationThreshold = 0.4;
		
		List<VM> expectedVMsToMigrate = new ArrayList<VM>();
		expectedVMsToMigrate.add(cdc.getVMs().get(0));
		expectedVMsToMigrate.add(cdc.getVMs().get(1));
		
		selectVMsToMigrateTest(hostToCheck, overloadedUtilizationThreshold, expectedVMsToMigrate);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void selectVMsToMigrateTest(Host hostToCheck, double overloadedUtilizationThreshold,
			List<VM> expectedVMsToMigrate) {
		GCDVMC gcnet = (GCDVMC)hostToCheck.getProtocol(protocolID);
		gcnet.overloadedThreshold = overloadedUtilizationThreshold;
		
		List<VM> actualVMsToMigrate = gcnet.selectVMsToMigrate(hostToCheck);
		LOGGER.debug("[{}]~actualVMsToMigrate={}", CommonState.getTime(), actualVMsToMigrate);

		Assertions.assertEquals(expectedVMsToMigrate, actualVMsToMigrate);
	}
	
	/*
	 * Comparator<? super TaskAnnounceMessage> getTaskComparator(Node node, int protocolID)
	 */
	@Test
    @DisplayName("Power Delta Tender Comparator Test")
    public void powerDeltaTenderComparatorTest() {
		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
//		public TenderMessage(Node from, Node to, long expirationTime, T task)
		// Sort all the tasks again.
		// VM1-1 [cpu=1000.00, ram=0.25], normalizedPowerDelta2=0.01625
		TenderMessage<VM> vm1 = new TenderMessage<VM>(hostToCheck, hostToCheck, 0, cdc.getVMs().get(1));
		// VM2-2 [cpu=1500.00, ram=1.00], normalizedPowerDelta1=0.02
		TenderMessage<VM> vm2 = new TenderMessage<VM>(hostToCheck, hostToCheck, 0, cdc.getVMs().get(2));
		// VM3-4 [cpu=1200.00, ram=0.50], normalizedPowerDelta1=0.0181
		TenderMessage<VM> vm4 = new TenderMessage<VM>(hostToCheck, hostToCheck, 0, cdc.getVMs().get(4));
		
		List<TenderMessage<VM>> actualSortedTasks = new ArrayList<TenderMessage<VM>>();
		actualSortedTasks.add(vm1);
		actualSortedTasks.add(vm2);
		actualSortedTasks.add(vm4);

		List<TenderMessage<VM>> expectedSortedTasks = new ArrayList<TenderMessage<VM>>();
		expectedSortedTasks.add(vm1);
		expectedSortedTasks.add(vm4);
		expectedSortedTasks.add(vm2);
		
		Comparator<? super TenderMessage<VM>> tenderSorter = GCDVMC_VMs.lowestPowerDelta;
		
		Collections.sort(actualSortedTasks, tenderSorter);
		LOGGER.debug("[{}]~expectedSortedTasks={}", CommonState.getTime(), expectedSortedTasks);
		LOGGER.debug("[{}]~actualSortedTasks={}", CommonState.getTime(), actualSortedTasks);

		Assertions.assertEquals(expectedSortedTasks, actualSortedTasks);
		
	}
	
	@Test
    @DisplayName("EcoCloud Tender Comparator Test")
    public void ecoCloudTenderComparatorTest() {
		Host hostToCheckOverloaded0 = cdc.getHosts().get(0);
		Host hostToCheckUnderloaded1 = cdc.getHosts().get(1);
		Host hostToCheckUnderloaded2 = cdc.getHosts().get(2);
		Host hostToCheckUnderloaded3 = cdc.getHosts().get(3);
		Host hostToCheckUnderloaded4 = cdc.getHosts().get(4);
		Host hostToCheckOverloaded5 = cdc.getHosts().get(5);
		
		hostToCheckUnderloaded1.migrateVM(cdc.getVMs().get(3), hostToCheckOverloaded0);
		hostToCheckUnderloaded1.migrateVM(cdc.getVMs().get(4), hostToCheckOverloaded0);
		hostToCheckUnderloaded4.migrateVM(cdc.getVMs().get(9), hostToCheckOverloaded5);
		hostToCheckUnderloaded4.migrateVM(cdc.getVMs().get(10), hostToCheckOverloaded5);
		
		LOGGER.debug("[{}]~hostToCheckOverloaded0={}", CommonState.getTime(), hostToCheckOverloaded0);
		LOGGER.debug("[{}]~hostToCheckUnderloaded1={}", CommonState.getTime(), hostToCheckUnderloaded1);
		LOGGER.debug("[{}]~hostToCheckUnderloaded2={}", CommonState.getTime(), hostToCheckUnderloaded2);
		LOGGER.debug("[{}]~hostToCheckUnderloaded3={}", CommonState.getTime(), hostToCheckUnderloaded3);
		LOGGER.debug("[{}]~hostToCheckUnderloaded4={}", CommonState.getTime(), hostToCheckUnderloaded4);
		LOGGER.debug("[{}]~hostToCheckOverloaded5={}", CommonState.getTime(), hostToCheckOverloaded5);
		GCDVMC_VMs gcnet= (GCDVMC_VMs)hostToCheckOverloaded0.getProtocol(protocolID);
	
//		INFO  1000, Host-0 [cpu=0.95, ram=0.63] guide=1.4999999999999996, gossipCounter=3
//		INFO  1001, Host-1 [cpu=0.17, ram=0.25] guide=1.0833333333333333, gossipCounter=3
//		INFO  1002, Host-2 [cpu=0.08, ram=0.13] guide=1.0416666666666667, gossipCounter=3
//		INFO  1003, Host-3 [cpu=0.25, ram=0.38] guide=1.125, gossipCounter=3
//		INFO  1004, Host-5 [cpu=1.00, ram=0.94] guide=2.0, gossipCounter=3
		
		// Sort all the tasks again.
		// guide=1.4999999999999996,
		TenderMessage<VM> tender0 = new TenderMessage<VM>(null, null, 0, null, gcnet.getEcoCloudGuide(hostToCheckOverloaded0, null));
		// guide=1.0833333333333333
		TenderMessage<VM> tender1 = new TenderMessage<VM>(null, null, 0, null, gcnet.getEcoCloudGuide(hostToCheckUnderloaded1, null));
		// guide=1.0416666666666667
		TenderMessage<VM> tender2 = new TenderMessage<VM>(null, null, 0, null, gcnet.getEcoCloudGuide(hostToCheckUnderloaded2, null));
		// guide=1.125
		TenderMessage<VM> tender3 = new TenderMessage<VM>(null, null, 0, null, gcnet.getEcoCloudGuide(hostToCheckUnderloaded3, null));
		// guide=2.0
		TenderMessage<VM> tender5 = new TenderMessage<VM>(null, null, 0, null, gcnet.getEcoCloudGuide(hostToCheckOverloaded5, null));
		
		List<TenderMessage<VM>> actualSortedTasks = new ArrayList<TenderMessage<VM>>();
		actualSortedTasks.add(tender0);
		actualSortedTasks.add(tender1);
		actualSortedTasks.add(tender2);
		actualSortedTasks.add(tender3);
		actualSortedTasks.add(tender5);

		List<TenderMessage<VM>> expectedSortedTasks = new ArrayList<TenderMessage<VM>>();
		expectedSortedTasks.add(tender5);
		expectedSortedTasks.add(tender0);
		expectedSortedTasks.add(tender2);
		expectedSortedTasks.add(tender1);
		expectedSortedTasks.add(tender3);
		
		Comparator<? super TenderMessage<VM>> tenderSorter = TenderMessage.highestGuide;
		
		Collections.sort(actualSortedTasks, tenderSorter);
		LOGGER.debug("[{}]~expectedSortedTasks={}", CommonState.getTime(), expectedSortedTasks);
		LOGGER.debug("[{}]~actualSortedTasks={}", CommonState.getTime(), actualSortedTasks);

		Assertions.assertEquals(expectedSortedTasks, actualSortedTasks);
		
	}
	
	@Test
    @DisplayName("Linear Tender Comparator Test")
    public void linearTenderComparatorTest() {
		Host hostToCheckOverloaded0 = cdc.getHosts().get(0);
		Host hostToCheckUnderloaded1 = cdc.getHosts().get(1);
		Host hostToCheckUnderloaded2 = cdc.getHosts().get(2);
		
		hostToCheckUnderloaded1.migrateVM(cdc.getVMs().get(3), hostToCheckOverloaded0);
		hostToCheckUnderloaded1.migrateVM(cdc.getVMs().get(4), hostToCheckOverloaded0);
		
		LOGGER.debug("[{}]~hostToCheckOverloaded0={}", CommonState.getTime(), hostToCheckOverloaded0);
		LOGGER.debug("[{}]~hostToCheckUnderloaded1={}", CommonState.getTime(), hostToCheckUnderloaded1);
		LOGGER.debug("[{}]~hostToCheckUnderloaded2={}", CommonState.getTime(), hostToCheckUnderloaded2);
		
		GCDVMC_VMs gcnet = (GCDVMC_VMs)hostToCheckOverloaded0.getProtocol(protocolID);
		gcnet.overloadedThreshold = 0.9;
		gcnet.underloadedThreshold = 0.5;
		
		VM vm1 = cdc.getVMs().get(1);
		
		// Sort all the tasks again.
//		INFO  TenderMessage.<init>()[0] -> TenderMessage contractID=1000, Host-0 [cpu=0.95, ram=0.63] -> null, expirationTime=0, task=null, guide=1.5, gossipCounter=0
//		INFO  TenderMessage.<init>()[0] -> TenderMessage contractID=1001, Host-1 [cpu=0.17, ram=0.25] -> null, expirationTime=0, task=null, guide=0.6666666666666667, gossipCounter=0
//		INFO  TenderMessage.<init>()[0] -> TenderMessage contractID=1002, Host-2 [cpu=0.08, ram=0.13] -> null, expirationTime=0, task=null, guide=0.8333333333333334, gossipCounter=0
				
		// guide=1.5
		TenderMessage<VM> tender0 = new TenderMessage<VM>(hostToCheckOverloaded0, null, 0, null, gcnet.getLinearGuide(hostToCheckOverloaded0, vm1));
		// guide=0.6666666666666667
		TenderMessage<VM> tender1 = new TenderMessage<VM>(hostToCheckUnderloaded1, null, 0, null, gcnet.getLinearGuide(hostToCheckUnderloaded1, vm1));
		// guide=0.8333333333333334,
		TenderMessage<VM> tender2 = new TenderMessage<VM>(hostToCheckUnderloaded2, null, 0, null, gcnet.getLinearGuide(hostToCheckUnderloaded2, vm1));
		
		List<TenderMessage<VM>> actualSortedTasks = new ArrayList<TenderMessage<VM>>();
		actualSortedTasks.add(tender1);
		actualSortedTasks.add(tender0);
		actualSortedTasks.add(tender2);

		List<TenderMessage<VM>> expectedSortedTasks = new ArrayList<TenderMessage<VM>>();
		expectedSortedTasks.add(tender0);
		expectedSortedTasks.add(tender2);
		expectedSortedTasks.add(tender1);
		
		Comparator<? super TenderMessage<VM>> tenderSorter = TenderMessage.highestGuide;
		
		Collections.sort(actualSortedTasks, tenderSorter);
		LOGGER.debug("[{}]~expectedSortedTasks={}", CommonState.getTime(), expectedSortedTasks);
		LOGGER.debug("[{}]~actualSortedTasks={}", CommonState.getTime(), actualSortedTasks);

		Assertions.assertEquals(expectedSortedTasks, actualSortedTasks);
		
	}
	
	@Test
    @DisplayName("Underloaded Linear Guide Test 0.0")
    public void underloadedlinearGuideTest0p0() {
		double util = 0.0;
		double expectedGuide = 1.0;
		underloadedlinearGuideTest(util, expectedGuide);
		
	}
	
	@Test
    @DisplayName("Underloaded Linear Guide Test 0.25")
    public void underloadedlinearGuideTest0p25() {
		double util = 0.25;
		double expectedGuide = 0.5;
		underloadedlinearGuideTest(util, expectedGuide);
		
	}
	
	@Test
    @DisplayName("Underloaded Linear Guide Test 0.5")
    public void underloadedlinearGuideTest0p5() {
		double util = 0.5;
		double expectedGuide = 0.0;
		underloadedlinearGuideTest(util, expectedGuide);
		
	}

	public void underloadedlinearGuideTest(double util, double expectedGuide) {
		
		Host hostToCheck = cdc.getHosts().get(0);
		GCDVMC_VMs gcnet = (GCDVMC_VMs)hostToCheck.getProtocol(protocolID);
		gcnet.overloadedThreshold = 0.9;
		gcnet.underloadedThreshold = 0.5;
		
		double actualGuide = gcnet.getLinearUnderloadedGuide(util);
		
		LOGGER.debug("[{}]~util={}, expectedGuide={}", 
				CommonState.getTime(), util, expectedGuide);
		LOGGER.debug("[{}]~util={}, actualGuide={}", 
				CommonState.getTime(), util, actualGuide);

		Assertions.assertEquals(expectedGuide, actualGuide);
	}
	
	@Test
    @DisplayName("Overloaded Linear Guide Test 0.9")
    public void overloadedLinearGuideTest0p9() {
		double util = 0.9;
		double expectedGuide = 0.0;
		overloadedLinearGuideTest(util, expectedGuide);
		
	}
	
	@Test
    @DisplayName("Overloaded Linear Guide Test 0.95")
    public void overloadedLinearGuideTest0p95() {
		double util = 0.95;
		double expectedGuide = 1.5;
		overloadedLinearGuideTest(util, expectedGuide);
		
	}
	
	@Test
    @DisplayName("Overloaded Linear Guide Test 1.0")
    public void overloadedLinearGuideTest1p0() {
		double util = 1.0;
		double expectedGuide = 2.0;
		overloadedLinearGuideTest(util, expectedGuide);
		
	}
	
	public void overloadedLinearGuideTest(double util, double expectedGuide) {
		
		Host hostToCheck = cdc.getHosts().get(0);
		GCDVMC_VMs gcnet = (GCDVMC_VMs)hostToCheck.getProtocol(protocolID);
		gcnet.overloadedThreshold = 0.9;
		gcnet.underloadedThreshold = 0.5;
		
		double actualGuide = gcnet.getLinearOverloadedGuide(util);
		
		LOGGER.debug("[{}]~util={}, expectedGuide={}", 
				CommonState.getTime(), util, expectedGuide);
		LOGGER.debug("[{}]~util={}, actualGuide={}", 
				CommonState.getTime(), util, actualGuide);

		Assertions.assertEquals(expectedGuide, actualGuide);
	}
	
	/*
	 * Comparator<BidMessage> getBidComparator(Node node, int protocolID)
	 */
	@Test
    @DisplayName("Power Delta Bid Comparator Test")
    public void powerDeltaBidComparatorTest() {
		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		// Sort all the tasks again, BidMessage<VM,VM>(Node to, Node from, int contractID, double powerDelta)
//		TenderMessage(Node from, Node to, long expirationTime)
		TenderMessage<VM> tenderMessage = new TenderMessage<VM>(null, null, 0, null); 
//		BidMessage(TenderMessage<T> tenderMessage, long expirationTime, double offer) 
		BidMessage<VM,VM> vm0 = new BidMessage<VM,VM>(tenderMessage, 0.05);
		// VM1-1 [cpu=1000.00, ram=0.25] normalizedPowerDelta1=0.11875
		BidMessage<VM,VM> vm1 = new BidMessage<VM,VM>(tenderMessage, 0.01);
		// VM2-2 [cpu=1500.00, ram=1.00] normalizedPowerDelta2=0.08333
		BidMessage<VM,VM> vm2 = new BidMessage<VM,VM>(tenderMessage, 0.03);
		
		List<BidMessage<VM,VM>> actualSortedBids = new ArrayList<BidMessage<VM,VM>>();
		actualSortedBids.add(vm2);
		actualSortedBids.add(vm1);
		actualSortedBids.add(vm0);
		
		Comparator<? super BidMessage<VM,VM>> bidSorter = BidMessage.lowestOffer;
		
		List<BidMessage<VM,VM>> expectedSortedBids = new ArrayList<BidMessage<VM,VM>>();
		expectedSortedBids.add(vm1);
		expectedSortedBids.add(vm2);
		expectedSortedBids.add(vm0);
		
		Collections.sort(actualSortedBids, bidSorter);
		LOGGER.debug("[{}]~expectedSortedBids={}", CommonState.getTime(), expectedSortedBids);
		LOGGER.debug("[{}]~actualSortedBids={}", CommonState.getTime(), actualSortedBids);

		Assertions.assertEquals(expectedSortedBids, actualSortedBids);
		
	}
	
	@Test
    @DisplayName("EcoCloud Offer Bid Comparator Test")
    public void ecoCloudOfferBidComparatorTest() {
		
		CommonState.r.setSeed(1);
		
		Host hostToCheckOverloaded0 = cdc.getHosts().get(0);
		TenderMessage<VM> tenderMessage0 = new TenderMessage<VM>(null, cdc.getHosts().get(0), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage1 = new TenderMessage<VM>(null, cdc.getHosts().get(1), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage2 = new TenderMessage<VM>(null, cdc.getHosts().get(2), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage3 = new TenderMessage<VM>(null, cdc.getHosts().get(3), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage4 = new TenderMessage<VM>(null, cdc.getHosts().get(4), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage5 = new TenderMessage<VM>(null, cdc.getHosts().get(5), 0, cdc.getVMs().get(0)); 
		
		GCDVMC_VMs gcnet = (GCDVMC_VMs)hostToCheckOverloaded0.getProtocol(protocolID);
		gcnet.overloadedThreshold = 0.9;
		gcnet.underloadedThreshold = 0.5;
		gcnet.shape = 5;
		
		BidMessage<VM,VM> bid0 = new BidMessage<VM,VM>(tenderMessage0, gcnet.getEcoCloudOffer(tenderMessage0));
		BidMessage<VM,VM> bid1 = new BidMessage<VM,VM>(tenderMessage1, gcnet.getEcoCloudOffer(tenderMessage1));
		BidMessage<VM,VM> bid2 = new BidMessage<VM,VM>(tenderMessage2, gcnet.getEcoCloudOffer(tenderMessage2));
		BidMessage<VM,VM> bid3 = new BidMessage<VM,VM>(tenderMessage3, gcnet.getEcoCloudOffer(tenderMessage3));
		BidMessage<VM,VM> bid4 = new BidMessage<VM,VM>(tenderMessage4, gcnet.getEcoCloudOffer(tenderMessage4));
		BidMessage<VM,VM> bid5 = new BidMessage<VM,VM>(tenderMessage5, gcnet.getEcoCloudOffer(tenderMessage5));
			
//		INFO  GCDVMC.createBidMessage()[0]~bidMessage=BidMessage contractID=1000, Host-0 [cpu=0.58, ram=0.44] offer=1.0
//		INFO  GCDVMC.createBidMessage()[0]~bidMessage=BidMessage contractID=1001, Host-1 [cpu=0.53, ram=0.44] offer=0.9443274622770917
//		INFO  GCDVMC.createBidMessage()[0]~bidMessage=BidMessage contractID=1002, Host-2 [cpu=0.08, ram=0.13] offer=0.01783264746227709
//		INFO  GCDVMC.createBidMessage()[0]~bidMessage=BidMessage contractID=1003, Host-3 [cpu=0.25, ram=0.38] offer=0.17052692584877716
//		INFO  GCDVMC.createBidMessage()[0]~bidMessage=BidMessage contractID=1004, Host-4 [cpu=0.33, ram=0.31] offer=0.35116598079561046
//		INFO  GCDVMC.createBidMessage()[0]~bidMessage=BidMessage contractID=1005, Host-5 [cpu=0.67, ram=0.63] offer=0.7526705692635691
					
		List<BidMessage<VM,VM>> actualSortedBids = new ArrayList<BidMessage<VM,VM>>();
		actualSortedBids.add(bid0);
		actualSortedBids.add(bid1);
		actualSortedBids.add(bid2);
		actualSortedBids.add(bid3);
		actualSortedBids.add(bid4);
		actualSortedBids.add(bid5);
		
		Comparator<? super BidMessage<VM,VM>> bidSorter = BidMessage.highestOffer;
		
		List<BidMessage<VM,VM>> expectedSortedBids = new ArrayList<BidMessage<VM,VM>>();
		expectedSortedBids.add(bid0);
		expectedSortedBids.add(bid1);
		expectedSortedBids.add(bid5);
		expectedSortedBids.add(bid4);
		expectedSortedBids.add(bid3);
		expectedSortedBids.add(bid2);
		
		Collections.sort(actualSortedBids, bidSorter);
		LOGGER.debug("[{}]~expectedSortedBids={}", CommonState.getTime(), expectedSortedBids);
		LOGGER.debug("[{}]~  actualSortedBids={}", CommonState.getTime(), actualSortedBids);

		Assertions.assertEquals(expectedSortedBids, actualSortedBids);
		
	}

	@Test
    @DisplayName("Linear Offer Bid Comparator Test")
    public void linearOfferBidComparatorTest() {

		Host hostToCheckOverloaded0 = cdc.getHosts().get(0);
		TenderMessage<VM> tenderMessage0 = new TenderMessage<VM>(null, cdc.getHosts().get(0), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage1 = new TenderMessage<VM>(null, cdc.getHosts().get(1), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage2 = new TenderMessage<VM>(null, cdc.getHosts().get(2), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage3 = new TenderMessage<VM>(null, cdc.getHosts().get(3), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage4 = new TenderMessage<VM>(null, cdc.getHosts().get(4), 0, cdc.getVMs().get(0)); 
		TenderMessage<VM> tenderMessage5 = new TenderMessage<VM>(null, cdc.getHosts().get(5), 0, cdc.getVMs().get(0)); 
						
		GCDVMC_VMs gcnet = (GCDVMC_VMs)hostToCheckOverloaded0.getProtocol(protocolID);
		gcnet.overloadedThreshold = 0.9;
		gcnet.underloadedThreshold = 0.5;
		
		BidMessage<VM,VM> bid0 = new BidMessage<VM,VM>(tenderMessage0, gcnet.getLinearOffer(tenderMessage0));
		BidMessage<VM,VM> bid1 = new BidMessage<VM,VM>(tenderMessage1, gcnet.getLinearOffer(tenderMessage1));
		BidMessage<VM,VM> bid2 = new BidMessage<VM,VM>(tenderMessage2, gcnet.getLinearOffer(tenderMessage2));
		BidMessage<VM,VM> bid3 = new BidMessage<VM,VM>(tenderMessage3, gcnet.getLinearOffer(tenderMessage3));
		BidMessage<VM,VM> bid4 = new BidMessage<VM,VM>(tenderMessage4, gcnet.getLinearOffer(tenderMessage4));
		BidMessage<VM,VM> bid5 = new BidMessage<VM,VM>(tenderMessage5, gcnet.getLinearOffer(tenderMessage5));
					
//		INFO  BidMessage.<init>()[0]~BidMessage contractID=1000, Host-0 [cpu=0.58, ram=0.44] , offer=0.8333333333333334
//		INFO  BidMessage.<init>()[0]~BidMessage contractID=1001, Host-1 [cpu=0.53, ram=0.44] , offer=0.7777777777777778
//		INFO  BidMessage.<init>()[0]~BidMessage contractID=1002, Host-2 [cpu=0.08, ram=0.13] , offer=0.2777777777777778
//		INFO  BidMessage.<init>()[0]~BidMessage contractID=1003, Host-3 [cpu=0.25, ram=0.38] , offer=0.46296296296296297
//		INFO  BidMessage.<init>()[0]~BidMessage contractID=1004, Host-4 [cpu=0.33, ram=0.31] , offer=0.5555555555555556
//		INFO  BidMessage.<init>()[0]~BidMessage contractID=1005, Host-5 [cpu=0.67, ram=0.63] , offer=0.9259259259259259

		List<BidMessage<VM,VM>> actualSortedBids = new ArrayList<BidMessage<VM,VM>>();
		actualSortedBids.add(bid0);
		actualSortedBids.add(bid1);
		actualSortedBids.add(bid2);
		actualSortedBids.add(bid3);
		actualSortedBids.add(bid4);
		actualSortedBids.add(bid5);
		
		Comparator<? super BidMessage<VM,VM>> bidSorter = BidMessage.highestOffer;
		
		List<BidMessage<VM,VM>> expectedSortedBids = new ArrayList<BidMessage<VM,VM>>();
		expectedSortedBids.add(bid5);
		expectedSortedBids.add(bid0);
		expectedSortedBids.add(bid1);
		expectedSortedBids.add(bid4);
		expectedSortedBids.add(bid3);
		expectedSortedBids.add(bid2);
		
		Collections.sort(actualSortedBids, bidSorter);
		LOGGER.debug("[{}]~expectedSortedBids={}", CommonState.getTime(), expectedSortedBids);
		LOGGER.debug("[{}]~  actualSortedBids={}", CommonState.getTime(), actualSortedBids);

		Assertions.assertEquals(expectedSortedBids, actualSortedBids);
		
	}
}