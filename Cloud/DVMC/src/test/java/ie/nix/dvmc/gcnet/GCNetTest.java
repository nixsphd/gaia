package ie.nix.dvmc.gcnet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import ie.nix.dvmc.TestTransport;
import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.gcnet.GCNetDVMC.VMBidMessage;
import ie.nix.dvmc.gcnet.GCNetDVMC.VMTenderMessage;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class GCNetTest {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	protected static final String DVMC_PROTOCOL = "dvmc_strategy";

	protected static int protocolID;
	
	protected static CloudDataCenter cdc;
	
	int random_seed = 1;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/exp.properties",
				"src/test/java/ie/nix/dvmc/gcnet/exp.properties",
				"src/main/resources/GCNet.properties"});
		
		// get the GCNet Protocol id.
		protocolID = Configuration.lookupPid(DVMC_PROTOCOL);
		cdc = CloudDataCenter.getCloudDataCenter();
	}

	@BeforeEach
	void beforeEach() {
		TestTransport.events.clear();
		cdc.provisionVMs();
		CommonState.r.setSeed(1);
	}

	@AfterEach
	void afterEach() {}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	/*
	 * boolean amIOverloaded(Host host)
	 */
	@Test
    @DisplayName("Am, I overloaded True")
    public void amIOverloadedTrueTest() {

		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		double overloadedUtilizationThreshold = 0.4;
		boolean expectedOverloaded = true;
		
		amIOverloadedTest(hostToCheck, overloadedUtilizationThreshold, expectedOverloaded);
		
		overloadedUtilizationThreshold = 0.5;
		expectedOverloaded = true;
		
		amIOverloadedTest(hostToCheck, overloadedUtilizationThreshold, expectedOverloaded);
		
    }
	
	@Test
    @DisplayName("Am I overloaded False")
    public void amIOverloadedFalseTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationHighThreshold = 0.9;
		boolean expectedOverloaded = false;
		
		amIOverloadedTest(hostToCheck, migrationHighThreshold, expectedOverloaded);
		
    }
	
	public void amIOverloadedTest(Host hostToCheck, double overloadedUtilizationThreshold, boolean expectedOverloaded) {
		GCNetDVMC gcnet = (GCNetDVMC)hostToCheck.getProtocol(protocolID);
		gcnet.overloadedThreshold = overloadedUtilizationThreshold;
		
		boolean actualOverloaded = gcnet.amIOverloaded(hostToCheck);
		LOGGER.debug("[{}]~actualOverloaded={}", CommonState.getTime(), actualOverloaded);

		Assertions.assertEquals(expectedOverloaded, actualOverloaded);
	}
	
	/*
	 * boolean amIUnderloaded(Host host)
	 */
	@Test
    @DisplayName("Am I underloaded True")
    public void amIUnderloadedTrueTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double underloadedUtilizationThreshold = 0.6;
		boolean expectedUnderloaded = true;
		
		amIUnderloadedTest(hostToCheck, underloadedUtilizationThreshold, expectedUnderloaded);
		
    }
	
	@Test
    @DisplayName("Am I underloaded False")
    public void amIUnderloadedFalseTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double underloadedUtilizationThreshold = 0.4;
		boolean expectedUnderloaded = false;
		
		amIUnderloadedTest(hostToCheck, underloadedUtilizationThreshold, expectedUnderloaded);
		
    }

	public void amIUnderloadedTest(Host hostToCheck, double underloadedUtilizationThreshold, boolean expectedUnderloaded) {
		GCNetDVMC gcnet = (GCNetDVMC)hostToCheck.getProtocol(protocolID);
		gcnet.underloadedThreshold = underloadedUtilizationThreshold;
		
		boolean actualUnderloaded = gcnet.amIUnderloaded(hostToCheck);

		Assertions.assertEquals(expectedUnderloaded, actualUnderloaded);
	}
	
	/*
	 * List<VM> selectVMsToMigrate(Host host) {
	 */
	@Test
    @DisplayName("Select VMs to Migrate One")
    public void selectVMsToMigrateOneTest() {
		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, vms={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.vmList());
		
		double overloadedUtilizationThreshold = 0.5;
		
		List<VM> expectedVMsToMigrate = new ArrayList<VM>();
		expectedVMsToMigrate.add(cdc.getVMs().get(0));
		
		selectVMsToMigrateTest(hostToCheck, overloadedUtilizationThreshold, expectedVMsToMigrate);
	}
	
//	@Test
//    @DisplayName("Select VMs to Migrate One Big One")
//    public void selectVMsToMigrateOneBigOneTest() {
//		// [cpu=0.58, ram=0.44]
//		Host hostToCheck = cdc.getHosts().get(0);
//		LOGGER.debug("[{}]~hostToCheck={}, vms={}", 
//				CommonState.getTime(), hostToCheck, hostToCheck.vmList());
//		
//		double overloadedUtilizationThreshold = 0.40;
//		
//		List<VM> expectedVMsToMigrate = new ArrayList<VM>();
//		expectedVMsToMigrate.add(cdc.getVMs().get(2));
//		
//		selectVMsToMigrateTest(hostToCheck, overloadedUtilizationThreshold, expectedVMsToMigrate);
//	}
	
	@Test
    @DisplayName("Select VMs to Migrate two")
    public void selectVMsToMigrateTwoTest() {
		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, vms={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.vmList());
		
		double overloadedUtilizationThreshold = 0.4;
		
		List<VM> expectedVMsToMigrate = new ArrayList<VM>();
		expectedVMsToMigrate.add(cdc.getVMs().get(0));
		expectedVMsToMigrate.add(cdc.getVMs().get(1));
		
		selectVMsToMigrateTest(hostToCheck, overloadedUtilizationThreshold, expectedVMsToMigrate);
	}

	public void selectVMsToMigrateTest(Host hostToCheck, double overloadedUtilizationThreshold,
			List<VM> expectedVMsToMigrate) {
		GCNetDVMC gcnet = (GCNetDVMC)hostToCheck.getProtocol(protocolID);
		gcnet.overloadedThreshold = overloadedUtilizationThreshold;
		
		List<VM> actualVMsToMigrate = gcnet.selectVMsToMigrate(hostToCheck);
		LOGGER.debug("[{}]~actualVMsToMigrate={}", CommonState.getTime(), actualVMsToMigrate);

		Assertions.assertEquals(expectedVMsToMigrate, actualVMsToMigrate);
	}
	
	/*
	 * Comparator<? super TaskAnnounceMessage> getTaskComparator(Node node, int protocolID)
	 */
	@Test
    @DisplayName("Task Comparator Test")
    public void taskComparatorTest() {
		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		// Sort all the tasks again.
		// VM1-1 [cpu=1000.00, ram=0.25], normalizedPowerDelta2=0.01625
		VMTenderMessage vm1 = new VMTenderMessage(hostToCheck, hostToCheck, 1, 0, 0, cdc.getVMs().get(1));
		// VM2-2 [cpu=1500.00, ram=1.00], normalizedPowerDelta1=0.02
		VMTenderMessage vm2 = new VMTenderMessage(hostToCheck, hostToCheck, 2, 0, 0, cdc.getVMs().get(2));
		// VM3-4 [cpu=1200.00, ram=0.50], normalizedPowerDelta1=0.0181
		VMTenderMessage vm4 = new VMTenderMessage(hostToCheck, hostToCheck, 0, 0, 0, cdc.getVMs().get(4));
		
		List<TaskAnnounceMessage> actualSortedTasks = new ArrayList<TaskAnnounceMessage>();
		actualSortedTasks.add(vm1);
		actualSortedTasks.add(vm2);
		actualSortedTasks.add(vm4);

		List<VMTenderMessage> expectedSortedTasks = new ArrayList<VMTenderMessage>();
		expectedSortedTasks.add(vm1);
		expectedSortedTasks.add(vm4);
		expectedSortedTasks.add(vm2);
		
		GCNetDVMC gcnet = (GCNetDVMC)hostToCheck.getProtocol(protocolID);
		Comparator<? super TaskAnnounceMessage> tenderSorter = 
				gcnet.getContractor(hostToCheck).getTaskComparator(hostToCheck, protocolID);
		
		Collections.sort(actualSortedTasks, tenderSorter);
		LOGGER.debug("[{}]~expectedSortedTasks={}", CommonState.getTime(), expectedSortedTasks);
		LOGGER.debug("[{}]~actualSortedTasks={}", CommonState.getTime(), actualSortedTasks);

		Assertions.assertEquals(expectedSortedTasks, actualSortedTasks);
		
	}
	
	/*
	 * Comparator<BidMessage> getBidComparator(Node node, int protocolID)
	 */
	@Test
    @DisplayName("Bid Comparator Test")
    public void bidComparatorTest() {
		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}", CommonState.getTime(), hostToCheck);
		
		// Sort all the tasks again, VMBidMessage(Node to, Node from, int contractID, double powerDelta)
		VMBidMessage vm0 = new VMBidMessage(hostToCheck, hostToCheck, 0, 0.05);
		// VM1-1 [cpu=1000.00, ram=0.25] normalizedPowerDelta1=0.11875
		VMBidMessage vm1 = new VMBidMessage(hostToCheck, hostToCheck, 0, 0.01);
		// VM2-2 [cpu=1500.00, ram=1.00] normalizedPowerDelta2=0.08333
		VMBidMessage vm2 = new VMBidMessage(hostToCheck, hostToCheck, 0, 0.03);
		
		List<VMBidMessage> actualSortedBids = new ArrayList<VMBidMessage>();
		actualSortedBids.add(vm2);
		actualSortedBids.add(vm1);
		actualSortedBids.add(vm0);
		
		GCNetDVMC gcnet = (GCNetDVMC)hostToCheck.getProtocol(protocolID);
		Comparator<? super VMBidMessage> bidSorter = 
				gcnet.getManager(hostToCheck).getBidComparator(hostToCheck, protocolID);
		
		List<VMBidMessage> expectedSortedBids = new ArrayList<VMBidMessage>();
		expectedSortedBids.add(vm1);
		expectedSortedBids.add(vm2);
		expectedSortedBids.add(vm0);
		
		Collections.sort(actualSortedBids, bidSorter);
		LOGGER.debug("[{}]~expectedSortedBids={}", CommonState.getTime(), expectedSortedBids);
		LOGGER.debug("[{}]~actualSortedBids={}", CommonState.getTime(), actualSortedBids);

		Assertions.assertEquals(expectedSortedBids, actualSortedBids);
		
	}

}