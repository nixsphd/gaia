package ie.nix.dvmc.gc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.TestTransport;
import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.gc.GCDVMC_Resources.Resources;
import ie.nix.gc.BidMessage;
import ie.nix.gc.TenderMessage;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;

public class GCResourcesTest {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	protected static final String DVMC_PROTOCOL = "dvmc_strategy";

	protected static int protocolID;
	
	protected static CloudDataCenter cdc;
	
	int random_seed = 1;
	double overloadedThreshold = 0.9;
	double underloadedThreshold = 0.5;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/exp.properties",
				"src/test/java/ie/nix/dvmc/gc/exp.properties",
				"src/main/resources/GC.properties"});
		
		// get the GC Protocol id.
		protocolID = Configuration.lookupPid(DVMC_PROTOCOL);
		cdc = CloudDataCenter.getCloudDataCenter();
	}

	@BeforeEach
	void beforeEach() {
		TestTransport.events.clear();
		cdc.provisionVMs();
		CommonState.r.setSeed(1);
	}

	@AfterEach
	void afterEach() {}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	/*
	 * Available Resources
	 */
	@Test
	@DisplayName("GC Reources")
	public void gcResourcesTest() {
		// [cpu=0.58, ram=0.44], Spec [6,000 GHz, 4 GB], availableResurces [cpu=1900.0, ram=2.25]
		Host hostToCheck = cdc.getHosts().get(0);
		GCDVMC_Resources gcResources = (GCDVMC_Resources)hostToCheck.getProtocol(protocolID);
		gcResources.overloadedThreshold = overloadedThreshold;
		gcResources.underloadedThreshold = underloadedThreshold;
		
		Resources expectedResources = new Resources(1900.0, 2.25);
		
		Resources actualResources = gcResources.getAvailableResources(hostToCheck);
		LOGGER.debug("[{}]~hostToCheck={}, expectedResources={}, actualResources={}", 
				CommonState.getTime(), hostToCheck, expectedResources, actualResources);
		
		Assertions.assertEquals(expectedResources, actualResources);
	}
	
	/*
	 * Can Fit
	 */
	@Test
	@DisplayName("GC Can Fit")
	public void canFitTest() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);

		GCDVMC_Resources gcResources = (GCDVMC_Resources)host.getProtocol(protocolID);
		gcResources.overloadedThreshold = overloadedThreshold;
		gcResources.underloadedThreshold = underloadedThreshold;
		
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(3200.0, 2.5);
		boolean expectedCanFit = true;
		
		canFitIntoTest(availableResurces, resources, expectedCanFit);
	}
	
	@Test
	@DisplayName("GC Can Fit Edge")
	public void canFitEdgeTest() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);

		GCDVMC_Resources gcResources = (GCDVMC_Resources)host.getProtocol(protocolID);
		gcResources.overloadedThreshold = overloadedThreshold;
		gcResources.underloadedThreshold = underloadedThreshold;
		
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(3400.0, 2.75);
		boolean expectedCanFit = true;
		
		canFitIntoTest(availableResurces, resources, expectedCanFit);
	}
	
	@Test
	@DisplayName("GC Can't Fit RAM")
	public void canFitTestRAM() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(3400.0, 2.76);
		boolean expectedCanFit = false;
		
		canFitIntoTest(availableResurces, resources, expectedCanFit);
	}
	
	@Test
	@DisplayName("GC Can't Fit CPU")
	public void canFitTestCPU() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
		
		Resources resources = new Resources(3500.0, 2.75);
		boolean expectedCanFit = false;
		
		canFitIntoTest(availableResurces, resources, expectedCanFit);
	}
	
	@Test
	@DisplayName("GC Can't Fit Both")
	public void canFitTestBoth() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host = cdc.getHosts().get(4);
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);

		Resources resources = new Resources(3500.0, 2.76);
		boolean expectedCanFit = false;
		
		canFitIntoTest(availableResurces, resources, expectedCanFit);
	}

	public void canFitIntoTest(Resources availableResurces, Resources resources,
			boolean expectedCanFit) {
		boolean actualCanFit = availableResurces.canFit(resources);
		LOGGER.debug("[{}]~availableResurces={}, resources={}, expectedCanFit={}, actualCanFit={}", 
				CommonState.getTime(), availableResurces, resources, expectedCanFit, actualCanFit);
		Assertions.assertEquals(expectedCanFit, actualCanFit);
	}
	
	/*
	 * Fit
	 */
	@Test
	@DisplayName("GC Fit")
	public void fitTest() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(3400.0, 2.75);
		double expectedFit = 1.0;
		
		fitTest(availableResurces, resources, expectedFit);
	}
	
	@Test
	@DisplayName("GC Fit Doesnt Fit Both")
	public void fitTestDoesntFitBoth() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(3500.0, 2.85);
		double expectedFit = Resources.DOESNT_FIT;
		
		fitTest(availableResurces, resources, expectedFit);
	}
	
	@Test
	@DisplayName("GC Fit Doesnt Fit RAM")
	public void fitTestDoesntFitRAM() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(3400.0, 2.85);
		double expectedFit = Resources.DOESNT_FIT;
		
		fitTest(availableResurces, resources, expectedFit);
	}
	
	@Test
	@DisplayName("GC Fit Doesnt Fit CPU")
	public void fitTestDoesntFitCPU() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(3500.0, 2.75);
		double expectedFit = Resources.DOESNT_FIT;
		
		fitTest(availableResurces, resources, expectedFit);
	}
	
	@Test
	@DisplayName("GC Fit Bad Fit CPU")
	public void fitTestBadFitCPU() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);

		GCDVMC_Resources gcResources = (GCDVMC_Resources)host.getProtocol(protocolID);
		gcResources.overloadedThreshold = overloadedThreshold;
		gcResources.underloadedThreshold = underloadedThreshold;
		
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(2500.0, 2.75);
		double expectedFit = 0.7353;
		
		fitTest(availableResurces, resources, expectedFit);
	}
	
	@Test
	@DisplayName("GC Fit Bad Fit RAM")
	public void fitTestBadFitRAM() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2], Spec [6,000 GHz, 4 GB], availableResurces [cpu=3400.0, ram=2.75]
		Host host= cdc.getHosts().get(4);
		Resources availableResurces = ((GCDVMC_Resources)host.getProtocol(protocolID)).
				getAvailableResources(host);
	
		Resources resources = new Resources(3400.0, 1.75);
		double expectedFit = 0.6363;
		
		fitTest(availableResurces, resources, expectedFit);
	}
	
	public void fitTest(Resources availableResurces, Resources resources,
			double expectedFit) {
		double actualFit = availableResurces.fit(resources);
		LOGGER.debug("[{}]~availableResurces={}, resources={}, expectedFit={}, actualFit={}", 
				CommonState.getTime(), availableResurces, resources, expectedFit, actualFit);
		Assertions.assertEquals(expectedFit, actualFit, 0.001);
	}
	
	/*
	 * double getGuide(Node node, Resources task) {
	 */
	@Test
    @DisplayName("Guide Test 0")
    public void getGuideTest0() {
		// Host-2 [cpu=0.00, ram=0.00, vms=0]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));
		
		double overloadedThreshold = 0.9;
		double expectedGuide = 0.0;
		
		getGuideTest(host, overloadedThreshold, expectedGuide);
	}
	
	
	@Test
    @DisplayName("Guide Test 0.5")
    public void getGuideTest0p5() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2]
		Host host = cdc.getHosts().get(4);

		double overloadedThreshold = 0.66;
		double expectedGuide = 0.5;
		
		getGuideTest(host, overloadedThreshold, expectedGuide);
	}
	
	@Test
    @DisplayName("Guide Test 1")
    public void getGuideTest1() {
		// Host-0 [cpu=0.58, ram=0.44, vms=3]
		Host host = cdc.getHosts().get(0);
		
		double overloadedThreshold = 0.58;
		double expectedGuide = 1.0;
		
		getGuideTest(host, overloadedThreshold, expectedGuide);
	}
	
	public void getGuideTest(Host host, double overloadedThreshold, double expectedGuide) {
		GCDVMC_Resources gcResources = (GCDVMC_Resources)host.getProtocol(protocolID);
		gcResources.overloadedThreshold = overloadedThreshold;
		double actualGuide = gcResources.getGuide(host, null);
		LOGGER.debug("[{}]~host={}, expectedGuide={}, actualGuide={}", 
				CommonState.getTime(), host, expectedGuide, actualGuide);
		Assertions.assertEquals(expectedGuide, actualGuide, 0.1);
	}
	
	/*
	 * double getOffer(TenderMessage<Resources> tenderMessage)
	 */
	
	@Test
    @DisplayName("Offer Test Zero")
    public void getOfferTestZero() {
		// Host-0 [cpu=0.58, ram=0.44, vms=3]
		Host host = cdc.getHosts().get(0);
		
		// TenderMessage(Node from, Node to, long expirationTime, T task)
		TenderMessage<Resources> tenderMessage = new TenderMessage<Resources>(null, host, 0, null);
		
		double overloadedThreshold = 0.59;
		double expectedOffer = 0.0;
		
		getOfferTest(host, tenderMessage, overloadedThreshold, underloadedThreshold, expectedOffer);
	}
	
	@Test
    @DisplayName("Offer Test 0.5")
    public void getOfferTest0p5() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2]
		Host host = cdc.getHosts().get(4);
		
		// TenderMessage(Node from, Node to, long expirationTime, T task)
		TenderMessage<Resources> tenderMessage = new TenderMessage<Resources>(null, host, 0, null);
		
		double overloadedThreshold = 0.66;
		double expectedOffer = 0.5;
		
		getOfferTest(host, tenderMessage, overloadedThreshold, underloadedThreshold, expectedOffer);
	}
	
	@Test
    @DisplayName("Offer Test One")
    public void getOfferTestOne() {
		// Host-4 [cpu=0.33, ram=0.31, vms=2]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));
		
		// TenderMessage(Node from, Node to, long expirationTime, T task)
		TenderMessage<Resources> tenderMessage = new TenderMessage<Resources>(null, host, 0, null);
		
		double expectedOffer = 1.0;
		
		getOfferTest(host, tenderMessage, overloadedThreshold, underloadedThreshold, expectedOffer);
	}
	
	public void getOfferTest(Host host, TenderMessage<Resources> tenderMessage, 
			double overloadedThreshold, double underloadedThreshold, double expectedOffer) {
		GCDVMC_Resources gcResources = (GCDVMC_Resources)host.getProtocol(protocolID);
		gcResources.overloadedThreshold = overloadedThreshold;
		gcResources.underloadedThreshold = underloadedThreshold;
		double actualOffer = gcResources.getOffer(tenderMessage);
		LOGGER.debug("[{}]~host={}, tenderMessage={}, expectedOffer={}, actualOffer={}", 
				CommonState.getTime(), host, tenderMessage, expectedOffer, actualOffer);
		Assertions.assertEquals(expectedOffer, actualOffer, 0.1);
	}
	
	/*
	 * Tender Comparator
	 */
	@Test
    @DisplayName("Best Fit Tender Comparator Test")
    public void bestFitTenderComparatorTest() {
		
		// Host-3 [cpu=0.25, ram=0.38, vms=2], Spec [6,000 GHz, 4 GB], CanFitInto [1,500, 1.52]
		Host host = cdc.getHosts().get(3);
		
		LOGGER.debug("[{}]~host={}", CommonState.getTime(), host);

		Resources resources1 = new Resources(1600.0, 1.6); // Can Fit Best
		Resources resources2 = new Resources(2600.0, 2.6); // Can Fit Worse
		Resources resources3 = new Resources(1400.0, 1.6); // Can't Fit RAM
		Resources resources4 = new Resources(1600.0, 1.4); // Can't Fit CPU
		
		//public TenderMessage(Node from, Node to, long expirationTime, T task, double guide) {			
		TenderMessage<Resources> tender1 = new TenderMessage<Resources>(null, host, 0, resources1, 0);
		TenderMessage<Resources> tender2 = new TenderMessage<Resources>(null, host, 0, resources2, 0);
		TenderMessage<Resources> tender3 = new TenderMessage<Resources>(null, host, 0, resources3, 0);
		TenderMessage<Resources> tender4 = new TenderMessage<Resources>(null, host, 0, resources4, 0);
		
		LOGGER.debug("[{}]~tender1={}", CommonState.getTime(), tender1);
		LOGGER.debug("[{}]~tender2={}", CommonState.getTime(), tender2);
		LOGGER.debug("[{}]~tender3={}", CommonState.getTime(), tender3);
		LOGGER.debug("[{}]~tender4={}", CommonState.getTime(), tender3);
		
//		INFO  GCResourcesTest.comparatorTest()[0]~tender1=TenderMessage contractID=1000, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.6], guide=0.33333333333333337, gossipCounter=0
//		INFO  GCResourcesTest.comparatorTest()[0]~tender2=TenderMessage contractID=1001, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1400.0, ram=1.6], guide=0.33333333333333337, gossipCounter=0
//		INFO  GCResourcesTest.comparatorTest()[0]~tender3=TenderMessage contractID=1002, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.4], guide=0.33333333333333337, gossipCounter=0
//		INFO  GCResourcesTest.comparatorTest()[0]~tender4=TenderMessage contractID=1003, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.6], guide=0.8333333333333334, gossipCounter=0
		
		List<TenderMessage<Resources>> actualSortedTasks = new ArrayList<TenderMessage<Resources>>();
		actualSortedTasks.add(tender3);
		actualSortedTasks.add(tender1);
		actualSortedTasks.add(tender4);
		actualSortedTasks.add(tender2);

//		TenderMessage contractID=1000, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.6], guide=0.33333333333333337, gossipCounter=0, 
//		TenderMessage contractID=1003, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.6], guide=0.8333333333333334, gossipCounter=0, 
//		TenderMessage contractID=1001, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1400.0, ram=1.6], guide=0.33333333333333337, gossipCounter=0, 
//		TenderMessage contractID=1002, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.4], guide=0.33333333333333337, gossipCounter=0]

		List<TenderMessage<Resources>> expectedSortedTasks = new ArrayList<TenderMessage<Resources>>();
		expectedSortedTasks.add(tender1);
		expectedSortedTasks.add(tender2);
		expectedSortedTasks.add(tender3);
		expectedSortedTasks.add(tender4);
		
		Comparator<? super TenderMessage<Resources>> tenderSorter = GCDVMC_Resources.bestFitTenderComparator;
		
		Collections.sort(actualSortedTasks, tenderSorter);
		LOGGER.debug("[{}]~expectedSortedTasks={}", CommonState.getTime(), expectedSortedTasks);
		LOGGER.debug("[{}]~actualSortedTasks={}", CommonState.getTime(), actualSortedTasks);

		Assertions.assertEquals(expectedSortedTasks, actualSortedTasks);
		
	}
	
	@Test
    @DisplayName("Best Fit By Guide Tender Comparator Test")
    public void bestFitByGuideTenderComparatorTest() {
		
		// Host-3 [cpu=0.25, ram=0.38, vms=2], Spec [6,000 GHz, 4 GB], CanFitInto [1,500, 1.52]
		Host host = cdc.getHosts().get(3);
		
		LOGGER.debug("[{}]~host={}", CommonState.getTime(), host);

		Resources resources1 = new Resources(1600.0, 1.6); // Can Fit Best
		Resources resources2 = new Resources(2600.0, 2.6); // Can Fit Worse
		Resources resources3 = new Resources(1400.0, 1.6); // Can't Fit RAM
		Resources resources4 = new Resources(1600.0, 1.4); // Can't Fit CPU
		
		//public TenderMessage(Node from, Node to, long expirationTime, T task, double guide) {			
		TenderMessage<Resources> tender1 = new TenderMessage<Resources>(null, host, 0, resources1, 0.1);
		TenderMessage<Resources> tender11 = new TenderMessage<Resources>(null, host, 0, resources1, 0.9);
		TenderMessage<Resources> tender2 = new TenderMessage<Resources>(null, host, 0, resources2, 0.1);
		TenderMessage<Resources> tender22 = new TenderMessage<Resources>(null, host, 0, resources2, 0.9);
		TenderMessage<Resources> tender3 = new TenderMessage<Resources>(null, host, 0, resources3, 0.1);
		TenderMessage<Resources> tender4 = new TenderMessage<Resources>(null, host, 0, resources4, 0.1);
		
		LOGGER.debug("[{}]~tender1={}", CommonState.getTime(), tender1);
		LOGGER.debug("[{}]~tender11={}", CommonState.getTime(), tender11);
		LOGGER.debug("[{}]~tender2={}", CommonState.getTime(), tender2);
		LOGGER.debug("[{}]~tender22={}", CommonState.getTime(), tender22);
		LOGGER.debug("[{}]~tender3={}", CommonState.getTime(), tender3);
		LOGGER.debug("[{}]~tender4={}", CommonState.getTime(), tender3);
		
//		INFO  GCResourcesTest.comparatorTest()[0]~tender1=TenderMessage contractID=1000, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.6], guide=0.33333333333333337, gossipCounter=0
//		INFO  GCResourcesTest.comparatorTest()[0]~tender2=TenderMessage contractID=1001, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1400.0, ram=1.6], guide=0.33333333333333337, gossipCounter=0
//		INFO  GCResourcesTest.comparatorTest()[0]~tender3=TenderMessage contractID=1002, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.4], guide=0.33333333333333337, gossipCounter=0
//		INFO  GCResourcesTest.comparatorTest()[0]~tender4=TenderMessage contractID=1003, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.6], guide=0.8333333333333334, gossipCounter=0
		
		List<TenderMessage<Resources>> actualSortedTasks = new ArrayList<TenderMessage<Resources>>();
		actualSortedTasks.add(tender3);
		actualSortedTasks.add(tender1);
		actualSortedTasks.add(tender4);
		actualSortedTasks.add(tender2);
		actualSortedTasks.add(tender11);
		actualSortedTasks.add(tender22);

//		TenderMessage contractID=1000, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.6], guide=0.33333333333333337, gossipCounter=0, 
//		TenderMessage contractID=1003, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.6], guide=0.8333333333333334, gossipCounter=0, 
//		TenderMessage contractID=1001, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1400.0, ram=1.6], guide=0.33333333333333337, gossipCounter=0, 
//		TenderMessage contractID=1002, null -> Host-3 [cpu=0.25, ram=0.38, vms=2], expirationTime=0, task=Resources [cpu=1600.0, ram=1.4], guide=0.33333333333333337, gossipCounter=0]

		List<TenderMessage<Resources>> expectedSortedTasks = new ArrayList<TenderMessage<Resources>>();
		expectedSortedTasks.add(tender11);
		expectedSortedTasks.add(tender22);
		expectedSortedTasks.add(tender1);
		expectedSortedTasks.add(tender2);
		expectedSortedTasks.add(tender3);
		expectedSortedTasks.add(tender4);
		
		Comparator<? super TenderMessage<Resources>> tenderSorter = GCDVMC_Resources.bestFitByGuideTenderComparator;
		
		Collections.sort(actualSortedTasks, tenderSorter);
		LOGGER.debug("[{}]~expectedSortedTasks={}", CommonState.getTime(), expectedSortedTasks);
		LOGGER.debug("[{}]~  actualSortedTasks={}", CommonState.getTime(), actualSortedTasks);

		Assertions.assertEquals(expectedSortedTasks, actualSortedTasks);
		
	}

	@Test
    @DisplayName("Best Fit Bid Comparator Test")
    public void bestFitbidComparatorTest() {
		Resources availableResources = new Resources(3400.0, 2.75); // Can Fit Best
		LOGGER.debug("[{}]~availableResources={}", CommonState.getTime(), availableResources);
		
//		TenderMessage(Node from, Node to, long expirationTime)
		TenderMessage<Resources> tenderMessage = new TenderMessage<Resources>(null, null, 0, availableResources); 

		List<VM> vms1 = Arrays.asList(new VM[]{cdc.getVMs().get(0), cdc.getVMs().get(1), cdc.getVMs().get(2)});
		List<VM> vms2 = Arrays.asList(new VM[]{cdc.getVMs().get(0)});
		List<VM> vms3 = Arrays.asList(new VM[]{cdc.getVMs().get(0), cdc.getVMs().get(1)});
		
		LOGGER.debug("[{}]~vms1={}", CommonState.getTime(), Resources.getResources(vms1)); // Worst, Can't Fit
		LOGGER.debug("[{}]~vms2={}", CommonState.getTime(), Resources.getResources(vms2)); // Next best
		LOGGER.debug("[{}]~vms3={}", CommonState.getTime(), Resources.getResources(vms3)); // Best

//		Available Resources [cpu=3400.0, ram=2.75]
//		INFO  GCResourcesTest.bestFitbidComparatorTest()[0]~vms1=Resources [cpu=3500.0, ram=1.75]
//		INFO  GCResourcesTest.bestFitbidComparatorTest()[0]~vms2=Resources [cpu=1000.0, ram=0.5]
//		INFO  GCResourcesTest.bestFitbidComparatorTest()[0]~vms3=Resources [cpu=2000.0, ram=0.75]
		
//		BidMessage(TenderMessage<T> tenderMessage, P proposal, double offer) {
		BidMessage<Resources,List<VM>> bid1 = new BidMessage<Resources,List<VM>>(tenderMessage, vms1, 0);
		BidMessage<Resources,List<VM>> bid2 = new BidMessage<Resources,List<VM>>(tenderMessage, vms2, 0);
		BidMessage<Resources,List<VM>> bid3 = new BidMessage<Resources,List<VM>>(tenderMessage, vms3, 0);
		
		List<BidMessage<Resources,List<VM>>> actualSortedBids = new ArrayList<BidMessage<Resources,List<VM>>>();
		actualSortedBids.add(bid1);
		actualSortedBids.add(bid2);
		actualSortedBids.add(bid3);
		
//		task=Resources [cpu=3400.0, ram=2.75], 
//		proposal=[VM0-0 [cpu=1000.00, ram=0.50], VM1-1 [cpu=1000.00, ram=0.25], VM2-2 [cpu=1500.00, ram=1.00]]
//		proposal=[VM0-0 [cpu=1000.00, ram=0.50], VM1-1 [cpu=1000.00, ram=0.25]]
//		proposal=[VM0-0 [cpu=1000.00, ram=0.50]
		
		Comparator<? super BidMessage<Resources,List<VM>>> bidSorter = GCDVMC_Resources.bestFitBidComparator;

		List<BidMessage<Resources,List<VM>>> expectedSortedBids = new ArrayList<BidMessage<Resources,List<VM>>>();
		expectedSortedBids.add(bid3);
		expectedSortedBids.add(bid2);
		expectedSortedBids.add(bid1);
		
		Collections.sort(actualSortedBids, bidSorter);
		LOGGER.debug("[{}]~expectedSortedBids={}", CommonState.getTime(), expectedSortedBids);
		LOGGER.debug("[{}]~  actualSortedBids={}", CommonState.getTime(), actualSortedBids);

		Assertions.assertEquals(expectedSortedBids, actualSortedBids);
		
	}
	
	@Test
    @DisplayName("Best Fit By Offer Bid Comparator Test")
    public void bestFitByOfferbidComparatorTest() {
		Resources availableResources = new Resources(3400.0, 2.75); // Can Fit Best
		LOGGER.debug("[{}]~availableResources={}", CommonState.getTime(), availableResources);
		
//		TenderMessage(Node from, Node to, long expirationTime)
		TenderMessage<Resources> tenderMessage = new TenderMessage<Resources>(null, null, 0, availableResources); 

		List<VM> vms1 = Arrays.asList(new VM[]{cdc.getVMs().get(0), cdc.getVMs().get(1), cdc.getVMs().get(2)});
		List<VM> vms2 = Arrays.asList(new VM[]{cdc.getVMs().get(0)});
		List<VM> vms3 = Arrays.asList(new VM[]{cdc.getVMs().get(0), cdc.getVMs().get(1)});
		
		LOGGER.debug("[{}]~vms1={}", CommonState.getTime(), Resources.getResources(vms1)); // Worst, Can't Fit
		LOGGER.debug("[{}]~vms2={}", CommonState.getTime(), Resources.getResources(vms2)); // Next best
		LOGGER.debug("[{}]~vms3={}", CommonState.getTime(), Resources.getResources(vms3)); // Best

//		Available Resources [cpu=3400.0, ram=2.75]
//		INFO  GCResourcesTest.bestFitbidComparatorTest()[0]~vms1=Resources [cpu=3500.0, ram=1.75]
//		INFO  GCResourcesTest.bestFitbidComparatorTest()[0]~vms2=Resources [cpu=1000.0, ram=0.5]
//		INFO  GCResourcesTest.bestFitbidComparatorTest()[0]~vms3=Resources [cpu=2000.0, ram=0.75]
		
//		BidMessage(TenderMessage<T> tenderMessage, P proposal, double offer) {
		BidMessage<Resources,List<VM>> bid1 = new BidMessage<Resources,List<VM>>(tenderMessage, vms1, 0.1);
		BidMessage<Resources,List<VM>> bid2 = new BidMessage<Resources,List<VM>>(tenderMessage, vms2, 0.1);
		BidMessage<Resources,List<VM>> bid22 = new BidMessage<Resources,List<VM>>(tenderMessage, vms2, 0.9);
		BidMessage<Resources,List<VM>> bid3 = new BidMessage<Resources,List<VM>>(tenderMessage, vms3, 0.1);
		BidMessage<Resources,List<VM>> bid4 = new BidMessage<Resources,List<VM>>(tenderMessage, vms3, 0.9);
		
		List<BidMessage<Resources,List<VM>>> actualSortedBids = new ArrayList<BidMessage<Resources,List<VM>>>();
		actualSortedBids.add(bid1);
		actualSortedBids.add(bid2);
		actualSortedBids.add(bid22);
		actualSortedBids.add(bid3);
		actualSortedBids.add(bid4);
		
//		task=Resources [cpu=3400.0, ram=2.75], 
//		proposal=[VM0-0 [cpu=1000.00, ram=0.50], VM1-1 [cpu=1000.00, ram=0.25], VM2-2 [cpu=1500.00, ram=1.00]]
//		proposal=[VM0-0 [cpu=1000.00, ram=0.50], VM1-1 [cpu=1000.00, ram=0.25]]
//		proposal=[VM0-0 [cpu=1000.00, ram=0.50]
		
		Comparator<? super BidMessage<Resources,List<VM>>> bidSorter = GCDVMC_Resources.bestFitByOfferBidComparator;

		List<BidMessage<Resources,List<VM>>> expectedSortedBids = new ArrayList<BidMessage<Resources,List<VM>>>();
		expectedSortedBids.add(bid4);
		expectedSortedBids.add(bid22);
		expectedSortedBids.add(bid3);
		expectedSortedBids.add(bid2);
		expectedSortedBids.add(bid1);
		
		Collections.sort(actualSortedBids, bidSorter);
		LOGGER.debug("[{}]~expectedSortedBids={}", CommonState.getTime(), expectedSortedBids);
		LOGGER.debug("[{}]~  actualSortedBids={}", CommonState.getTime(), actualSortedBids);

		Assertions.assertEquals(expectedSortedBids, actualSortedBids);
		
	}
}