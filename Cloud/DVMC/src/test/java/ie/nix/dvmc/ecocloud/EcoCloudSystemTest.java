package ie.nix.dvmc.ecocloud;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.observer.AverageWorkloadAndRAM;
import ie.nix.dvmc.cdc.observer.Observer;
import peersim.config.Configuration;

public class EcoCloudSystemTest {
	
	private static final Logger LOGGER = LogManager.getLogger();

	protected static final String DVMC_PROTOCOL = "dvmc_strategy";
	protected static final String UTIUL_OBSERVER = "average_workloads";

	protected static int dvmcProtocolID;
	protected static AverageWorkloadAndRAM util;
	
	CloudDataCenter cdc;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/exp.properties",
				"src/main/resources/ecoCloud.properties",
				"src/test/java/ie/nix/dvmc/ecocloud/exp-system.properties"});
		
		// get the ecoCloud Protocol id.
		dvmcProtocolID = Configuration.lookupPid(DVMC_PROTOCOL);

		// Add the observers to make them accessible for testing.
		util = (AverageWorkloadAndRAM)Observer.observers.get("AverageWorkloadAndRAM");
	}

	@BeforeEach
	void beforeEach() {
		cdc = CloudDataCenter.getCloudDataCenter();
	}

	@AfterEach
	void afterEach() {}

	@AfterAll
	static void afterAll() {
		LOGGER.info("~Done.");
	}

	@Test
    @DisplayName("System")
    public void systemTest() {
		cdc.getHosts().stream().forEachOrdered(host -> {
			System.out.println(host);
			host.vmStream().forEachOrdered(vm -> {
				System.out.println("\t"+vm);
			});
		});
		
		Assertions.assertNotNull(util);
		Assertions.assertEquals(util.avgCPUUtil, 0.8, 0.1);
		Assertions.assertEquals(util.avgRAMUtil, 0.8, 0.1);
	}
}