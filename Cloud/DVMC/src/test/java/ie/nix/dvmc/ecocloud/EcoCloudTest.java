package ie.nix.dvmc.ecocloud;

import java.util.OptionalDouble;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.TestTransport;
import ie.nix.dvmc.TestTransport.Event;
import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.ecocloud.EcoCloudDVMC.AssignMessage;
import ie.nix.dvmc.ecocloud.EcoCloudDVMC.Manager;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class EcoCloudTest {
	
	private static final Logger LOGGER = LogManager.getLogger();

	protected static final String DVMC_PROTOCOL = "dvmc_strategy";

	protected static int protocolID;
	
	protected static CloudDataCenter cdc;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/exp.properties",
				"src/main/resources/ecoCloud.properties",
				"src/test/java/ie/nix/dvmc/ecocloud/exp.properties",});
		// get the ecoCloud Protocol id.
		protocolID = Configuration.lookupPid(DVMC_PROTOCOL);
		cdc = CloudDataCenter.getCloudDataCenter();
	}

	@BeforeEach
	void beforeEach() {
		TestTransport.events.clear();
		cdc.provisionVMs();
		CommonState.r.setSeed(1);
	}

	@AfterEach
	void afterEach() {}

	@AfterAll
	static void afterAll() {
		LOGGER.info("~Done.");
	}
		
	/*
	 * boolean amIOverloaded(Host host)
	 */
	@Test
    @DisplayName("Am, I overloaded True")
    public void amIOverloadedTrueTest() {

		// [cpu=0.58, ram=0.44]
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationHighThreshold = 0.4;
		boolean expectedOverloaded = true;
		
		amIOverloadedTest(hostToCheck, migrationHighThreshold, expectedOverloaded);
		
		migrationHighThreshold = 0.5;
		expectedOverloaded = true;
		
		amIOverloadedTest(hostToCheck, migrationHighThreshold, expectedOverloaded);
		
    }
	
	@Test
    @DisplayName("Am I overloaded False")
    public void amIOverloadedFalseTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationHighThreshold = 0.9;
		boolean expectedOverloaded = false;
		
		amIOverloadedTest(hostToCheck, migrationHighThreshold, expectedOverloaded);
		
    }

	public void amIOverloadedTest(Host hostToCheck, double migrationHighThreshold, boolean expectedOverloaded) {
		EcoCloudDVMC ecocloud = (EcoCloudDVMC)hostToCheck.getProtocol(protocolID);
		ecocloud.migrationHighThreshold = migrationHighThreshold;
		
		boolean actualOverloaded = ecocloud.amIOverloaded(hostToCheck);

		Assertions.assertEquals(expectedOverloaded, actualOverloaded);
	}
	
	/*
	 * boolean amIUnderloaded(Host host)
	 */
	@Test
    @DisplayName("Am I underloaded True")
    public void amIUnderloadedTrueTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationLowThreshold = 0.6;
		boolean expectedUnderloaded = true;
		
		amIUnderloadedTest(hostToCheck, migrationLowThreshold, expectedUnderloaded);
		
    }
	
	@Test
    @DisplayName("Am I underloaded False")
    public void amIUnderloadedFalseTest() {

		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationLowThreshold = 0.4;
		boolean expectedUnderloaded = false;
		
		amIUnderloadedTest(hostToCheck, migrationLowThreshold, expectedUnderloaded);
		
    }

	public void amIUnderloadedTest(Host hostToCheck, double migrationLowThreshold, boolean expectedUnderloaded) {
		EcoCloudDVMC ecocloud = (EcoCloudDVMC)hostToCheck.getProtocol(protocolID);
		ecocloud.migrationLowThreshold = migrationLowThreshold;
		
		boolean actualUnderloaded = ecocloud.amIUnderloaded(hostToCheck);

		Assertions.assertEquals(expectedUnderloaded, actualUnderloaded);
	}
	
	/*
	 * double getHighMigrationSuccessProbability(double util)
	 */
	@Test
    @DisplayName("High Migration Probability 0.0")
    public void getHighMigrationSuccessProbability0Test() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationHighThreshold = 0.7;
		double util = 0.7;
		double expectedProbability = 0.0;
		
		getHighMigrationSuccessProbabilityTest(hostToCheck, migrationHighThreshold, util, expectedProbability);
		
	}
	
	@Test
    @DisplayName("High Migration Probability 1.0")
    public void getHighMigrationSuccessProbability1Test() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationHighThreshold = 0.7;
		double util = 1.0;
		double expectedProbability = 1.0;
		
		getHighMigrationSuccessProbabilityTest(hostToCheck, migrationHighThreshold, util, expectedProbability);
		
	}
	
	@Test
    @DisplayName("High Migration Probability 0.5")
    public void getHighMigrationSuccessProbability0p5Test() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationHighThreshold = 0.7;
		double util = 0.85;
		double expectedProbability = 0.5;
		
		getHighMigrationSuccessProbabilityTest(hostToCheck, migrationHighThreshold, util, expectedProbability);
		
	}

	public void getHighMigrationSuccessProbabilityTest(Host hostToCheck, double migrationHighThreshold, double util,
			double expectedProbability) {
		EcoCloudDVMC ecocloud = (EcoCloudDVMC)hostToCheck.getProtocol(protocolID);
		ecocloud.beta = 1.0;
		ecocloud.migrationHighThreshold = migrationHighThreshold;
		double actualProbability = ecocloud.getHighMigrationSuccessProbability(util);

		Assertions.assertEquals(expectedProbability, actualProbability);
	}
	
	/*
	 * double getLowMigrationSuccessProbability(double util)
	 */
	@Test
    @DisplayName("Low Migration Probability 0.0")
    public void getLowMigrationSuccessProbability0Test() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationLowThreshold = 0.3;
		double util = 0.3;
		double expectedProbability = 0.0;
		
		getLowMigrationSuccessProbabilityTest(hostToCheck, migrationLowThreshold, util, expectedProbability);
		
	}
	
	@Test
    @DisplayName("Low Migration Probability 1.0")
    public void getLowMigrationSuccessProbability1Test() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationLowThreshold = 0.3;
		double util = 0.0;
		double expectedProbability = 1.0;
		
		getLowMigrationSuccessProbabilityTest(hostToCheck, migrationLowThreshold, util, expectedProbability);
		
	}
	
	@Test
    @DisplayName("Low Migration Probability 0.5")
    public void getLowMigrationSuccessProbability0p5Test() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		
		double migrationLowThreshold = 0.3;
		double util = 0.15;
		double expectedProbability = 0.5;
		
		getLowMigrationSuccessProbabilityTest(hostToCheck, migrationLowThreshold, util, expectedProbability);
		
	}

	public void getLowMigrationSuccessProbabilityTest(Host hostToCheck, double migrationLowThreshold, double util,
			double expectedProbability) {
		EcoCloudDVMC ecocloud = (EcoCloudDVMC)hostToCheck.getProtocol(protocolID);
		ecocloud.alpha = 1.0;
		ecocloud.migrationLowThreshold = migrationLowThreshold;
		double actualProbability = ecocloud.getLowMigrationSuccessProbability(util);

		Assertions.assertEquals(expectedProbability, actualProbability);
	}
	
	/*
	 * static boolean bernoulliTrial(double successProbability)
	 */
	@Test
    @DisplayName("bernoulli Trial 0.5")
    public void bernoulliTrialTest0p5() {
		
		double successProbability = 0.5;
		final int numberOfTrials = 100;
		double expectedAverageSuccess = 0.5;
		
		bernoulliTrialTest(successProbability, numberOfTrials, expectedAverageSuccess);
		
	}
	
	@Test
    @DisplayName("bernoulli Trial 0.0")
    public void bernoulliTrialTest0p0() {
		
		double successProbability = 0.0;
		final int numberOfTrials = 100;
		double expectedAverageSuccess = 0.0;
		
		bernoulliTrialTest(successProbability, numberOfTrials, expectedAverageSuccess);
		
	}
	
	@Test
    @DisplayName("bernoulli Trial 1.0")
    public void bernoulliTrialTest1p0() {
		
		double successProbability = 1.0;
		final int numberOfTrials = 100;
		double expectedAverageSuccess = 1.0;
		
		bernoulliTrialTest(successProbability, numberOfTrials, expectedAverageSuccess);
		
	}

	public void bernoulliTrialTest(double successProbability, final int numberOfTrials, double expectedAverageSuccess) {
		OptionalDouble actualAverageSuccess = IntStream.range(0, numberOfTrials).
			mapToDouble(i -> (EcoCloudDVMC.bernoulliTrial(successProbability)? 1.0 : 0.0)).average();
		
		Assertions.assertTrue(actualAverageSuccess.isPresent());
		Assertions.assertEquals(expectedAverageSuccess, actualAverageSuccess.getAsDouble(), 0.05);
	}
	
	/*
	 * protected boolean amIUnderThreshold(Host host) 
	 */
	@Test
    @DisplayName("Am I Under Threshold 0.9")
    public void amIUnderThresholdTest0p9() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		double assignmentThreshold = 0.9;
		boolean expectedUnderThreshold = true;
		
		amIUnderThresholdTest(hostToCheck, assignmentThreshold, expectedUnderThreshold);
		
	}
	
	@Test
    @DisplayName("Am I Under Threshold 0.5")
    public void amIUnderThresholdTest0p5() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		double assignmentThreshold = 0.5;
		boolean expectedUnderThreshold = false;
		
		amIUnderThresholdTest(hostToCheck, assignmentThreshold, expectedUnderThreshold);
		
	}
	
	@Test
    @DisplayName("Am I Under Threshold 0.3")
    public void amIUnderThresholdTest0p3() {
		
		Host hostToCheck = cdc.getHosts().get(0);
		LOGGER.debug("[{}]~hostToCheck={}, cpuUtil={}, ramUtil={}", 
				CommonState.getTime(), hostToCheck, hostToCheck.getCPUUtil(), hostToCheck.getRAMUtil());
		double assignmentThreshold = 0.3;
		boolean expectedUnderThreshold = false;
		
		amIUnderThresholdTest(hostToCheck, assignmentThreshold, expectedUnderThreshold);
		
	}

	public void amIUnderThresholdTest(Host hostToCheck, double assignmentThreshold, boolean expectedUnderThreshold) {
		EcoCloudDVMC ecocloud = (EcoCloudDVMC)hostToCheck.getProtocol(protocolID);
		ecocloud.assignmentThreshold = assignmentThreshold;
		
		boolean actualUnderThreshold = ecocloud.amIUnderThreshold(hostToCheck);
		Assertions.assertEquals(expectedUnderThreshold, actualUnderThreshold);
	}
	
	/*
	 * double getSuccessProbability(double util)
	 */
	@Test
    @DisplayName("Get Success Probability 0.0")
    public void getSuccessProbabilityTest0p0() {
		double hostUtil = 0.0;
		double assignmentThreshold = 0.9;
		double shape = 2;
		double expectedSuccessProbability = 0.0;
		
		getSuccessProbabilityTest(hostUtil, assignmentThreshold, shape, expectedSuccessProbability);
		
	}
	
	@Test
    @DisplayName("Get Success Probability 1.0")
    public void getSuccessProbabilityTest1p0() {
		double hostUtil = 1.0;
		double assignmentThreshold = 0.9;
		double shape = 2;
		double expectedSuccessProbability = 0.0;
		
		getSuccessProbabilityTest(hostUtil, assignmentThreshold, shape, expectedSuccessProbability);
		
	}
	
	@Test
    @DisplayName("Get Success Probability 0.9")
    public void getSuccessProbabilityTest0p9() {
		double hostUtil = 0.9;
		double assignmentThreshold = 0.9;
		double shape = 2;
		double expectedSuccessProbability = 0.0;
		
		getSuccessProbabilityTest(hostUtil, assignmentThreshold, shape, expectedSuccessProbability);
		
	}
	
	@Test
    @DisplayName("Get Success Probability 0.625")
    public void getSuccessProbabilityTest0p625() {
		double hostUtil = 0.625;
		double assignmentThreshold = 0.9;
		double shape = 2;
		double expectedSuccessProbability = 1.0;
		
		getSuccessProbabilityTest(hostUtil, assignmentThreshold, shape, expectedSuccessProbability);
		
	}
	
	@Test
    @DisplayName("Get Success Probability 0.415")
    public void getSuccessProbabilityTest0p415() {
		double hostUtil = 0.415;
		double assignmentThreshold = 0.9;
		double shape = 3;
		double expectedSuccessProbability = 0.5;
		
		getSuccessProbabilityTest(hostUtil, assignmentThreshold, shape, expectedSuccessProbability);
		
	}
	
	public void getSuccessProbabilityTest(double hostUtil, double assignmentThreshold, 
			double shape, double expectedSuccessProbability) {
		
		EcoCloudDVMC ecocloud = (EcoCloudDVMC)cdc.getHosts().get(0).getProtocol(protocolID);
		ecocloud.assignmentThreshold = assignmentThreshold;
		ecocloud.shape = shape;
		
		double actualSuccessProbability = ecocloud.getSuccessProbability(hostUtil);
		Assertions.assertEquals(expectedSuccessProbability, actualSuccessProbability, 0.01);
	}
	
	/*
	 * void Manager.assignVM(VM vm, boolean allowWakeUp)
	 */
	@Test
    @DisplayName("Assign VM")
    public void assignVM() {

		// Remove it from the host it's in
		VM vm = cdc.getVMs().get(0);
		Host host = vm.getHost();
		host.removeVM(vm);
		Assertions.assertTrue(vm.getHost() == null);
		
		boolean allowWakeUp = false;
		
		Manager manager = EcoCloudDVMC.getManager();
		manager.assignDelay = 0;
		manager.assignVM(vm, allowWakeUp);

		Assertions.assertTrue(TestTransport.events.size() == 2);
		
		Event actualEvent = TestTransport.events.get(0);
		Assertions.assertTrue(((AssignMessage)actualEvent.msg).isAssignRequest());
		
		actualEvent = TestTransport.events.get(1);
		Assertions.assertTrue(((AssignMessage)actualEvent.msg).isAssignResponse());
		
		Assertions.assertTrue(vm.getHost() != null);
	}
	
	/*
	 * void nextCycle(Node node, int protocolID)
	 */
	@Test
    @DisplayName("nextCycle Underloaded Fail")
    public void nextCycleUnderloadedFail() {

		Host host = cdc.getHosts().get(4);
		int numberOfVMs = host.vmList().size();
		
		Manager manager = EcoCloudDVMC.getManager();
		manager.assignDelay = 0;
		
		((CDProtocol)host.getProtocol(protocolID)).nextCycle(host, protocolID);

		Assertions.assertTrue(TestTransport.events.size() == 2);

		AssignMessage actualMessage = (AssignMessage)TestTransport.events.get(0).msg;
		Assertions.assertTrue(actualMessage.isAssignRequest());
		
		actualMessage = (AssignMessage)TestTransport.events.get(1).msg;
		Assertions.assertTrue(actualMessage.isAssignResponse());
		Assertions.assertFalse(actualMessage.accept.isPresent() && actualMessage.accept.get());

		Assertions.assertTrue(host.vmList().size() == numberOfVMs);
	}
	
	@Test
    @DisplayName("nextCycle Underloaded Succeed")
    public void nextCycleUnderloadedSucceed() {
		// Have to move a VM into Host 3 to make it more porbable the migration will succeed.
		cdc.getHosts().get(0).migrateVM(cdc.getVMs().get(0), cdc.getHosts().get(3));
		
		Host host = cdc.getHosts().get(4);
		int numberOfVMs = host.vmList().size();
		
		Manager manager = EcoCloudDVMC.getManager();
		manager.assignDelay = 0;
		
		((CDProtocol)host.getProtocol(protocolID)).nextCycle(host, protocolID);

		Assertions.assertTrue(TestTransport.events.size() == 2);

		AssignMessage actualMessage = (AssignMessage)TestTransport.events.get(0).msg;
		Assertions.assertTrue(actualMessage.isAssignRequest());
		
		actualMessage = (AssignMessage)TestTransport.events.get(1).msg;
		Assertions.assertTrue(actualMessage.isAssignResponse());
		Assertions.assertTrue(actualMessage.accept.isPresent() && actualMessage.accept.get());

		Assertions.assertTrue(host.vmList().size() == numberOfVMs-1);
	}

	@Test
    @DisplayName("nextCycle Overloaded Succeed")
    public void nextCycleOverloaded() {
		Host host = cdc.getHosts().get(5);
		int numberOfVMs = host.vmList().size();
		
		EcoCloudDVMC ecocloud = (EcoCloudDVMC)host.getProtocol(protocolID);
		ecocloud.migrationHighThreshold = 0.5;
		
		Manager manager = EcoCloudDVMC.getManager();
		manager.assignDelay = 0;
		
		((CDProtocol)host.getProtocol(protocolID)).nextCycle(host, protocolID);

		Assertions.assertTrue(TestTransport.events.size() == 2);

		AssignMessage actualMessage = (AssignMessage)TestTransport.events.get(0).msg;
		Assertions.assertTrue(actualMessage.isAssignRequest());
		
		actualMessage = (AssignMessage)TestTransport.events.get(1).msg;
		Assertions.assertTrue(actualMessage.isAssignResponse());
		Assertions.assertTrue(actualMessage.accept.isPresent() && actualMessage.accept.get());

		Assertions.assertTrue(host.vmList().size() == numberOfVMs-1);
	}
}