package ie.nix.dvmc.cdc.protocols;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.TestProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.GeneralNode;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDSimulator;
import peersim.transport.Transport;

public class LockProtocolTest {
	
	static final Logger LOGGER = LogManager.getLogger();

	public static class TestNode extends GeneralNode {

		public static final String NETWORK_PROTOCOL = "network";
		public static final String TRANSPORT_PROTOCOL = "transport";
		public static final String LOCK_PROTOCOL = "lock";

		private int networkProtocolID;
		private int transportProtocolID;
		private int lockProtocolID;
	
		public TestNode(String prefix) {
			super(prefix);
			networkProtocolID = Configuration.lookupPid(NETWORK_PROTOCOL);
			transportProtocolID = Configuration.lookupPid(TRANSPORT_PROTOCOL);
			lockProtocolID = Configuration.lookupPid(LOCK_PROTOCOL);
	        LOGGER.trace("[0] prefix={}", prefix);	
		}
		
		@Override
		public TestNode clone() {
			TestNode clone = (TestNode)super.clone();
		    LOGGER.trace("[0]");	
			return clone;
		}

		@Override
		public String toString() {
			return "TestNode [ID="+this.getID()+"]";
		}
		
		public Linkable getNetworkProtocol() {
			return (Linkable)this.getProtocol(getNetworkProtocolID());
		}

		public int getNetworkProtocolID() {
			return networkProtocolID;
		}
		
		public Transport getTransportProtocol() {
			return (Transport)this.getProtocol(getTransportProtocolID());
		}

		public int getTransportProtocolID() {
			return transportProtocolID;
		}
		public LockProtocol getLockProtocol() {
			return (LockProtocol)this.getProtocol(getLockProtocolID());
		}

		public int getLockProtocolID() {
			return lockProtocolID;
		}
	}
	
	int random_seed= 1;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/java/ie/nix/dvmc/cdc/protocols/LockProtocolTest.properties"});
		
	}

	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to "+random_seed);
	}

	@AfterEach
	void afterEach() {
		clearAll();
		System.out.println();
	}

	private void clearAll() {
		TestProtocol.nextCycle = null;
		TestProtocol.processEvent = null;
	}

	@AfterAll
	static void afterAll() {
		LOGGER.info("[{}]~Done.", CommonState.getTime());
	}
	
	@Test
    @DisplayName("Basic Lock Test")
    public void basicLockTest() {
		TestProtocol.nextCycle = (Node node, int protocolID) -> {
			LOGGER.trace("[{}]~nextCycle() node={}, protocolID={}", 
					CommonState.getTime(), node, protocolID);
			if (CommonState.getTime() < 300 && node.getIndex() == 0) {
				int lockProtocolID = ((TestNode)node).getLockProtocolID();
				LockProtocol lockProtocol = (LockProtocol)node.getProtocol(lockProtocolID);
				lockProtocol.lockNeignbours(node, lockProtocolID);
				LOGGER.info("[{}]~nextCycle() node={}, protocolID={}, Lock neighbours", 
						CommonState.getTime(), node, protocolID);
			}
		};
		
		EDSimulator.nextExperiment();
		
		Set<Node> setOfLockedNodes = getLockedNodes();
		Set<Node> setOfNeignbourNodes = getNeignbourNodes(0);
      
        Assertions.assertTrue(setOfLockedNodes.equals(setOfNeignbourNodes));
    }
	
	@Test
    @DisplayName("Basic Lock Unlock Test")
    public void basicLockUnlockTest() {
		TestProtocol.nextCycle = (Node node, int protocolID) -> {
			LOGGER.trace("[{}]~nextCycle() node={}, protocolID={}", 
					CommonState.getTime(), node, protocolID);
			if (CommonState.getTime() < 300 && node.getIndex() == 0) {
				int lockProtocolID = ((TestNode)node).getLockProtocolID();
				LockProtocol lockProtocol = (LockProtocol)node.getProtocol(lockProtocolID);
				lockProtocol.lockNeignbours(node, lockProtocolID);
				LOGGER.info("[{}]~nextCycle() node={}, protocolID={}, Lock neighbours", 
						CommonState.getTime(), node, protocolID);
			}
			
			if (CommonState.getTime() >= 300 && node.getIndex() == 0) {
				int lockProtocolID = ((TestNode)node).getLockProtocolID();
				LockProtocol lockProtocol = (LockProtocol)node.getProtocol(lockProtocolID);
				lockProtocol.unlockNeignbours(node, lockProtocolID);
				LOGGER.info("[{}]~nextCycle() node={}, protocolID={}, Unlock neighbours", 
						CommonState.getTime(), node, protocolID);
			}
		};
		
		EDSimulator.nextExperiment();
		
		Set<Node> setOfLockedNodes = getLockedNodes();

        Assertions.assertTrue(setOfLockedNodes.isEmpty());
    }
	
	@Test
    @DisplayName("Basic Lock ALL Test")
    public void basicLockALLTest() {
		TestProtocol.nextCycle = (Node node, int protocolID) -> {
			LOGGER.trace("[{}]~nextCycle() node={}, protocolID={}", 
					CommonState.getTime(), node, protocolID);
			if (CommonState.getTime() < 300) {
				int lockProtocolID = ((TestNode)node).getLockProtocolID();
				LockProtocol lockProtocol = (LockProtocol)node.getProtocol(lockProtocolID);
				lockProtocol.lockNeignbours(node, lockProtocolID);
				LOGGER.info("[{}]~nextCycle() node={}, protocolID={}, Lock neighbours", 
						CommonState.getTime(), node, protocolID);
			}
		};
		
		EDSimulator.nextExperiment();
		
		int numberOfNodes = Network.size();
		for (int n = 0; n < numberOfNodes; n++) {
			TestNode testNode = (TestNode)Network.get(n);
	        Assertions.assertTrue(testNode.getLockProtocol().isLocked());
	        Assertions.assertTrue(locksAreConsistent(testNode));
		}
    }
	
	@Test
    @DisplayName("Basic Lock Unlock ALL Test")
    public void basicLockUnlockALLTest() {
		TestProtocol.nextCycle = (Node node, int protocolID) -> {
			LOGGER.trace("[{}]~nextCycle() node={}, protocolID={}", 
					CommonState.getTime(), node, protocolID);
			if (CommonState.getTime() < 300) {
				int lockProtocolID = ((TestNode)node).getLockProtocolID();
				LockProtocol lockProtocol = (LockProtocol)node.getProtocol(lockProtocolID);
				lockProtocol.lockNeignbours(node, lockProtocolID);
				LOGGER.info("[{}]~nextCycle() node={}, protocolID={}, Lock neighbours", 
						CommonState.getTime(), node, protocolID);
			}
			if (CommonState.getTime() >= 300) {
				int lockProtocolID = ((TestNode)node).getLockProtocolID();
				LockProtocol lockProtocol = (LockProtocol)node.getProtocol(lockProtocolID);
				
				lockProtocol.unlockNeignbours(node, lockProtocolID);
				LOGGER.info("[{}]~nextCycle() node={}, protocolID={}, Unlock neighbours", 
						CommonState.getTime(), node, protocolID);
			}
		};
		
		EDSimulator.nextExperiment();
		
		int numberOfNodes = Network.size();
		for (int n = 0; n < numberOfNodes; n++) {
			TestNode testNode = (TestNode)Network.get(n);
	        Assertions.assertFalse(testNode.getLockProtocol().isLocked());
	        Assertions.assertTrue(lockedNodesIsEmpty(testNode));
		}
    }

	private boolean lockedNodesIsEmpty(TestNode testNode) {
		LockProtocol lockProtocol = testNode.getLockProtocol();
		return lockProtocol.lockedNeighbours.size() == 0;
	}

	private boolean locksAreConsistent(TestNode testNode) {
		boolean locksAreConsistent = true;
		LockProtocol lockProtocol = testNode.getLockProtocol();
		
		TestNode lockedBy = (TestNode)lockProtocol.lockedBy;
		LockProtocol lockedByLockProtocol = lockedBy.getLockProtocol();
		if (testNode != lockedBy) {
			if (!lockedByLockProtocol.lockedNeighbours.contains(testNode)) {
				locksAreConsistent = false;
				LOGGER.error("[{}]~{} lockedNeighbours does not contins {}, it has {}", 
						CommonState.getTime(), lockedBy, testNode, 
						lockedByLockProtocol.lockedNeighbours);
			}
		}
		return locksAreConsistent;
	}

	private Set<Node> getNeignbourNodes(int i) {
		Set<Node> setOfNeignbourNodes = new HashSet<Node>();
		TestNode node = (TestNode)Network.get(i);
		setOfNeignbourNodes.add(node);
		Linkable network = (Linkable)node.getNetworkProtocol();
		for (int n = 0; n < network.degree(); n++) {
			Node neighbourNode = network.getNeighbor(n);
			setOfNeignbourNodes.add(neighbourNode);
		}
		return setOfNeignbourNodes;
	}

	private Set<Node> getLockedNodes() {
		int numberOfNodes = Network.size();
		Set<Node> setOfLockedNodes = new HashSet<Node>();
		for (int n = 0; n < numberOfNodes; n++) {
			TestNode testNode = (TestNode)Network.get(n);
			if (testNode.getLockProtocol().isLocked()) {
				setOfLockedNodes.add(testNode);
			}
		}
		return setOfLockedNodes;
	}

}