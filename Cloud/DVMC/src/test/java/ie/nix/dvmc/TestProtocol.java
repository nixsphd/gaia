package ie.nix.dvmc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.cdsim.CDProtocol;
import peersim.core.CommonState;
import peersim.core.Node;
import peersim.edsim.EDProtocol;

public class TestProtocol implements CDProtocol , EDProtocol {

	static final Logger LOGGER = LogManager.getLogger();
	
	public interface ProcessEvent {
		public void processEvent(Node node, int pid, Object event);
	}

	public interface NextCycle {
		public void nextCycle(Node node, int protocolID);
	}

	public static TestProtocol.ProcessEvent processEvent;
	public static TestProtocol.NextCycle nextCycle;
	
	public TestProtocol(String prefix) {
		LOGGER.trace("[0] prefix={}", prefix);	
	}
	
	@Override
	public TestProtocol clone() {
		TestProtocol clone = null;
		try {
			clone = (TestProtocol)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	    LOGGER.trace("[0]");	
		return clone;
	}

	@Override
	public String toString() {
		return "TestProtocol";
	}

	@Override
	public void processEvent(Node node, int protocolID, Object event) {
		LOGGER.trace("[{}]~node={}, protocolID={}, event={}", 
				CommonState.getTime(), node, protocolID, event);
		if (processEvent != null) {
			processEvent.processEvent(node, protocolID, event);
		}
	}

	@Override
	public void nextCycle(Node node, int protocolID) {
		LOGGER.trace("[{}]~node={}, protocolID={}", 
				CommonState.getTime() , node, protocolID);
		if (nextCycle != null) {
			nextCycle.nextCycle(node, protocolID);
		}
	}
}