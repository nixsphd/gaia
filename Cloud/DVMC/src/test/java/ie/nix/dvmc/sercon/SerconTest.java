package ie.nix.dvmc.sercon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;

public class SerconTest {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	CloudDataCenter cdc;
	Map<VM, Host> vmMigrationMap;
	Sercon sercon;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/exp.properties",
				"src/main/resources/Sercon.properties"});
		
	}

	@BeforeEach
	void beforeEach() {
		cdc = CloudDataCenter.getCloudDataCenter();
	}

	@AfterEach
	void afterEach() {}

	@AfterAll
	static void afterAll() {
		LOGGER.info("~Done.");
	}
		
	/*
	 * getVMsIngludingMigrations()
	 */
	@Test
    @DisplayName("Get VMs including Migrations Test with VMs migrating in")
    public void getVMsIngludingMigrationsTestWithVMsMigratingIn() {
		
		Host hostToCheck = cdc.getHosts().get(0);

		vmMigrationMap = getVMMigrationMap(cdc);
		
		VM[] expectedVMsIngludingMigration = {
				cdc.getVMs().get(0),
				cdc.getVMs().get(1),
				cdc.getVMs().get(2),
				cdc.getVMs().get(6)
		};
		
		getVMsIngludingMigrationsTest(vmMigrationMap, hostToCheck, expectedVMsIngludingMigration);
		
    }
	
	@Test
    @DisplayName("Get VMs including Migrations Test with VMs migrating out")
    public void getVMsIngludingMigrationsTestWithVMsMigratingOut() {

		Host hostToCheck = cdc.getHosts().get(2);
		VM[] expectedVMsIngludingMigration = {};

		vmMigrationMap = getVMMigrationMap(cdc);
		
		getVMsIngludingMigrationsTest(vmMigrationMap, hostToCheck, expectedVMsIngludingMigration);
		
    }

	protected void getVMsIngludingMigrationsTest(Map<VM, Host> vmMigrationMap, Host host, 
			VM[] expectedVMsIngludingMigration) {
		
		List<VM> actualVMsIngludingMigration = Sercon.getVMsIngludingMigrations(host, vmMigrationMap);

		Assertions.assertTrue(actualVMsIngludingMigration.size() == expectedVMsIngludingMigration.length);
		
		Arrays.stream(expectedVMsIngludingMigration).forEach(vm -> {
			Assertions.assertTrue(actualVMsIngludingMigration.contains(vm));
		});
	}

	/*
	 * getCPUUtilIncludingMigrations()
	 */
	@Test
    @DisplayName("Get CPU Util for a host considerin VM Migrations with migrations in.")
	public void getCPUUtilIncludingMigrationsTestWithVMsMigratingIn() {
	
		Host hostToCheck = cdc.getHosts().get(0);
		double expectedCPUUtilIncludingMigrations = 4.0/6;

		vmMigrationMap = getVMMigrationMap(cdc);
		
		getCPUUtilIncludingMigrationsTest(vmMigrationMap, hostToCheck, expectedCPUUtilIncludingMigrations);
		
	}

	@Test
    @DisplayName("Get CPU Util for a host considerin VM Migrations with migrations out.")
	public void getCPUUtilIncludingMigrationsTestWithVMsMigratingOut() {
		
		Host hostToCheck = cdc.getHosts().get(2);
		double expectedCPUUtilIncludingMigrations = 0.0;

		vmMigrationMap = getVMMigrationMap(cdc);
		
		getCPUUtilIncludingMigrationsTest(vmMigrationMap, hostToCheck, expectedCPUUtilIncludingMigrations);
		
	}

	protected void getCPUUtilIncludingMigrationsTest(Map<VM, Host> vmMigrationMap, Host hostToCheck,
			double expectedCPUUtilIncludingMigrations) {
		
		double actualCPUUtilIncludingMigrations = 
				Sercon.getCPUUtilIncludingMigrations(hostToCheck, vmMigrationMap);
		
		Assertions.assertEquals(actualCPUUtilIncludingMigrations, expectedCPUUtilIncludingMigrations);
	}

	/*
	 * getRAMUtilIncludingMigrations()
	 */
	@Test
    @DisplayName("Get RAM Util for a host considerin VM Migrations with migrations in.")
	public void getRAMUtilIncludingMigrationsTestWithVMsMigratingIn() {
		
		Host hostToCheck = cdc.getHosts().get(0);

		vmMigrationMap = getVMMigrationMap(cdc);
		
		double expectedRAMUtilIncludingMigrations = 2.25/4;
	
		getRAMUtilIncludingMigrationsTest(vmMigrationMap, hostToCheck, expectedRAMUtilIncludingMigrations);
		
	}

	@Test
    @DisplayName("Get RAM Util for a host considerin VM Migrations with migrations out.")
	public void getRAMUtilIncludingMigrationsTestWithVMsMigratingOut() {
		
		Host hostToCheck = cdc.getHosts().get(2);

		vmMigrationMap = getVMMigrationMap(cdc);
		
		double expectedRAMUtilIncludingMigrations = 0.0;
	
		getRAMUtilIncludingMigrationsTest(vmMigrationMap, hostToCheck, expectedRAMUtilIncludingMigrations);
		
	}

	protected void getRAMUtilIncludingMigrationsTest(Map<VM, Host> vmMigrationMap, Host hostToCheck,
			double expectedRAMUtilIncludingMigrations) {
		
		double actualRAMUtilIncludingMigrations = 
				Sercon.getRAMUtilIncludingMigrations(hostToCheck, vmMigrationMap);
		
		Assertions.assertEquals(actualRAMUtilIncludingMigrations, expectedRAMUtilIncludingMigrations);
	}
	
	/* 
	 * sortHostsBySurrogateWeight()
	 */
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight")
	public void sortVMsBySurrogateWeightIncludingMigrationsTest() {
		
		Host hostToCheck = cdc.getHosts().get(0);

		vmMigrationMap = getVMMigrationMap(cdc);
		
		List<VM> expecedOrder = new ArrayList<VM>();
		expecedOrder.add(cdc.getVMs().get(6));
		expecedOrder.add(cdc.getVMs().get(1));
		expecedOrder.add(cdc.getVMs().get(0));
		expecedOrder.add(cdc.getVMs().get(2));
	
		sortVMsBySurrogateWeightIncludingMigrationsTest(hostToCheck, expecedOrder);
		
	}

	protected void sortVMsBySurrogateWeightIncludingMigrationsTest(Host host, List<VM> expecedOrder) {
		
		List<VM> actualOrder = 
				Sercon.sortVMsBySurrogateWeightIncludingMigrations(host, vmMigrationMap);
		LOGGER.debug("~actualOrder={} compared to expecedOrder={}", actualOrder, expecedOrder);
		
		Iterator<VM> actualOrderIterator = actualOrder.iterator();
		expecedOrder.stream().forEach(vm -> {
			Assertions.assertTrue(actualOrderIterator.hasNext());
			VM nextVM = actualOrderIterator.next();
			Assertions.assertTrue(nextVM.equals(vm));
		});
		Assertions.assertFalse(actualOrderIterator.hasNext());
	}
	
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight with no migrations")
	public void sortHostsBySurrogateWeightNoMigrationsTest() {
		
		List<Host> hosts = cdc.getHosts();
		
		// No migrations
		vmMigrationMap = new HashMap<VM, Host>();
		
		// 3, 4, 5, 2, 1, 6 => 2, 3, 4, 1, 0, 5
		List<Host> expecedOrder = new ArrayList<Host>();
		expecedOrder.add(cdc.getHosts().get(2));
		expecedOrder.add(cdc.getHosts().get(3));
		expecedOrder.add(cdc.getHosts().get(4));
		expecedOrder.add(cdc.getHosts().get(1));
		expecedOrder.add(cdc.getHosts().get(0));
		expecedOrder.add(cdc.getHosts().get(5));
	
		sortHostsBySurrogateWeightTest(hosts, expecedOrder);
		
	}
	
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight with one migration")
	public void sortHostsBySurrogateWeightOneMigrationsTest() {
		
		List<Host> hosts = cdc.getHosts();
		
		// VM7 to Host 1 => VM6 to Host 0
		vmMigrationMap = new HashMap<VM, Host>();
		vmMigrationMap.put(cdc.getVMs().get(6), cdc.getHosts().get(0));
		
		// 3, 4, 5, 2, 1, 6 => 2, 3, 4, 1, 0, 5
		List<Host> expecedOrder = new ArrayList<Host>();
		expecedOrder.add(cdc.getHosts().get(2));
		expecedOrder.add(cdc.getHosts().get(3));
		expecedOrder.add(cdc.getHosts().get(4));
		expecedOrder.add(cdc.getHosts().get(1));
		expecedOrder.add(cdc.getHosts().get(0));
		expecedOrder.add(cdc.getHosts().get(5));
	
		sortHostsBySurrogateWeightTest(hosts, expecedOrder);
		
	}
	
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight")
	public void sortHostsBySurrogateWeightIncludingMigrationsTest() {
		
		List<Host> hosts = cdc.getHosts();

		vmMigrationMap = getVMMigrationMap(cdc);
		
		// 3, 4, 5, 1, 6, 2
		List<Host> expecedOrder = new ArrayList<Host>();
		expecedOrder.add(cdc.getHosts().get(2));
		expecedOrder.add(cdc.getHosts().get(3));
		expecedOrder.add(cdc.getHosts().get(4));
		expecedOrder.add(cdc.getHosts().get(0));
		expecedOrder.add(cdc.getHosts().get(5));
		expecedOrder.add(cdc.getHosts().get(1));
	
		sortHostsBySurrogateWeightTest(hosts, expecedOrder);
		
	}

	protected void sortHostsBySurrogateWeightTest(List<Host> hosts, List<Host> expecedOrder) {
		
		List<Host> actualOrder = 
				Sercon.sortHostsBySurrogateWeight(hosts, vmMigrationMap);
		LOGGER.debug("~actualOrder={} compared to expecedOrder={}", actualOrder, expecedOrder);
		
		Iterator<Host> actualOrderIterator = actualOrder.iterator();
		expecedOrder.stream().forEach(vm -> {
			Assertions.assertTrue(actualOrderIterator.hasNext());
			Host nextHost = actualOrderIterator.next();
			Assertions.assertTrue(nextHost.equals(vm));
		});
		Assertions.assertFalse(actualOrderIterator.hasNext());
	}
	
	/*
	 * getHostToBeReleased()
	 */
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight No Migrations")
	public void getHostToBeReleasedTestNoMigrations() {
		
		List<Host> hosts = cdc.getHosts();
		
		// No migrations
		vmMigrationMap = new HashMap<VM, Host>();
		List<Host> sortedHosts = Sercon.sortHostsBySurrogateWeight(hosts, vmMigrationMap);
		
		// Host 3 => 2
		Optional<Host> expectedOptionalHost = Optional.of(hosts.get(2));
		int unsuccessfulMigrationAttempts = 0;
	
		getHostToBeReleasedTest(sortedHosts, expectedOptionalHost, unsuccessfulMigrationAttempts);
			
	}
	
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight No Migrations One unsucessful")
	public void getHostToBeReleasedTestNoMigrationsOneUnsucessful() {
		
		List<Host> hosts = cdc.getHosts();
		
		// No migrations
		vmMigrationMap = new HashMap<VM, Host>();
		List<Host> sortedHosts = Sercon.sortHostsBySurrogateWeight(hosts, vmMigrationMap);
		
		// Host 4 => 3
		Optional<Host> expectedOptionalHost = Optional.of(hosts.get(3));
		int unsuccessfulMigrationAttempts = 1;
	
		getHostToBeReleasedTest(sortedHosts, expectedOptionalHost, unsuccessfulMigrationAttempts);
			
	}
	
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight One Migrations")
	public void getHostToBeReleasedTestOneMigrations() {
		
		List<Host> hosts = cdc.getHosts();
		
		// One migrations
		vmMigrationMap = new HashMap<VM, Host>();
		vmMigrationMap.put(cdc.getVMs().get(6), cdc.getHosts().get(0));
		List<Host> sortedHosts = Sercon.sortHostsBySurrogateWeight(hosts, vmMigrationMap);
		
		// Host 4 => 3
		Optional<Host> expectedOptionalHost = Optional.of(hosts.get(3));
		int unsuccessfulMigrationAttempts = 0;
	
		getHostToBeReleasedTest(sortedHosts, expectedOptionalHost, unsuccessfulMigrationAttempts);

	}
	
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight One Migrations One unsucessfukl")
	public void getHostToBeReleasedTestOneMigrationsOneUnsucessful() {
		
		List<Host> hosts = cdc.getHosts();
		
		// One migrations
		vmMigrationMap = new HashMap<VM, Host>();
		vmMigrationMap.put(cdc.getVMs().get(6), cdc.getHosts().get(0));
		List<Host> sortedHosts = Sercon.sortHostsBySurrogateWeight(hosts, vmMigrationMap);
		
		// Host 5 => 4
		Optional<Host> expectedOptionalHost = Optional.of(hosts.get(4));
		int unsuccessfulMigrationAttempts = 1;
	
		getHostToBeReleasedTest(sortedHosts, expectedOptionalHost, unsuccessfulMigrationAttempts);

	}
	
	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight")
	public void getHostToBeReleasedTest() {
		
		List<Host> hosts = cdc.getHosts();

		vmMigrationMap = getVMMigrationMap(cdc);
		
		List<Host> sortedHosts = Sercon.sortHostsBySurrogateWeight(hosts, vmMigrationMap);

		// Host 5 => 4
		Optional<Host> expectedOptionalHost = Optional.of(hosts.get(4));
		
		int unsuccessfulMigrationAttempts = 0;
	
		getHostToBeReleasedTest(sortedHosts, expectedOptionalHost, unsuccessfulMigrationAttempts);
		
	}

	@Test
    @DisplayName("Get the VMs sorted by Surrogate Weight One unsucessful")
	public void getHostToBeReleasedTestOneUnsucessful() {
		
		List<Host> hosts = cdc.getHosts();

		vmMigrationMap = getVMMigrationMap(cdc);
		
		List<Host> sortedHosts = Sercon.sortHostsBySurrogateWeight(hosts, vmMigrationMap);

		// Host 1 => 0
		Optional<Host> expectedOptionalHost = Optional.of(hosts.get(0));
		
		int unsuccessfulMigrationAttempts = 1;
	
		getHostToBeReleasedTest(sortedHosts, expectedOptionalHost, unsuccessfulMigrationAttempts);
		
	}
	
	protected void getHostToBeReleasedTest(List<Host> sortedHosts, 
			Optional<Host> expectedOptionalHost, int unsuccessfulMigrationAttempts) {
		
		Optional<Host> actualOptionalHost = 
				Sercon.getHostToBeReleased(sortedHosts, vmMigrationMap, unsuccessfulMigrationAttempts);
		LOGGER.debug("~expectedOptionalHost={}, actualOptionalHost={}", 
				expectedOptionalHost, actualOptionalHost);
		Assertions.assertTrue(expectedOptionalHost.equals(actualOptionalHost));
	}
		
	/*
	 * serconDVMC
	 */
	@Test
    @DisplayName("Sercon from the paper")
	public void serconDVMCTest() {

		sercon = newSercon();
		
		Map<VM, Host> expectedVMMigrationMap = getVMMigrationMap(cdc);

		serconDVMCMasAllowedMigrationsTest(expectedVMMigrationMap);

	}
	
	@Test
    @DisplayName("Sercon maxAllowedNumberOfMigrations=1")
	public void serconDVMCMaxAllowedMigrationsTest() {

		sercon = newSercon();
		sercon.maxAllowedNumberOfMigrations = 1;

		Map<VM, Host> expectedVMMigrationMap = new HashMap<VM, Host>();
		expectedVMMigrationMap.put(cdc.getVMs().get(6), cdc.getHosts().get(0));
		
		serconDVMCMasAllowedMigrationsTest(expectedVMMigrationMap);
	}

	@Test
    @DisplayName("Sercon minAllowedMigrationEfficiency=0.9")
	public void serconDVMCMinAllowedMigrationEfficiencyTest() {

		sercon = newSercon();
		sercon.minAllowedMigrationEfficiency = 0.9;

		Map<VM, Host> expectedVMMigrationMap = new HashMap<VM, Host>();
		expectedVMMigrationMap.put(cdc.getVMs().get(6), cdc.getHosts().get(0));
		
		serconDVMCMasAllowedMigrationsTest(expectedVMMigrationMap);
	}
	
	protected void serconDVMCMasAllowedMigrationsTest(Map<VM, Host> expectedVMMigrationMap) {
		HashMap<VM, Host> actualVMMigrationMap = new HashMap<VM, Host>();
		sercon.serconDVMC(actualVMMigrationMap);
		
		LOGGER.debug("~actualVMMigrationMap={}", actualVMMigrationMap);
		LOGGER.debug("~expectedVMMigrationMap={}", expectedVMMigrationMap);
		
		Iterator<Entry<VM, Host>> actualVMMigrationMapIterator = actualVMMigrationMap.entrySet().iterator();
		expectedVMMigrationMap.entrySet().stream().forEach(mapping -> {
			Assertions.assertTrue(actualVMMigrationMapIterator.hasNext());
			Entry<VM, Host> nextMapping = actualVMMigrationMapIterator.next();
			Assertions.assertTrue(nextMapping.getKey().equals(mapping.getKey()));
			Assertions.assertTrue(nextMapping.getValue().equals(mapping.getValue()));
		});
		Assertions.assertFalse(actualVMMigrationMapIterator.hasNext());
	}
	
	protected Sercon newSercon() {
		int maxAllowedNumberOfMigrations = 0;
		double minAllowedMigrationEfficiency = 0.1;
		double threshold = 0.7;
		Sercon sercon = new Sercon(maxAllowedNumberOfMigrations, minAllowedMigrationEfficiency, threshold);
		return sercon;
	}

	protected Map<VM, Host> getVMMigrationMap(CloudDataCenter cdc) {
		Map<VM, Host> vmMigrationMap = new HashMap<VM, Host>();
		// 8 -> 5, 9 -> 2 => 7 -> 4, 8 -> 1
		vmMigrationMap.put(cdc.getVMs().get(6), cdc.getHosts().get(0));
		vmMigrationMap.put(cdc.getVMs().get(7), cdc.getHosts().get(4));
		vmMigrationMap.put(cdc.getVMs().get(8), cdc.getHosts().get(1));
		return vmMigrationMap;
	}

}