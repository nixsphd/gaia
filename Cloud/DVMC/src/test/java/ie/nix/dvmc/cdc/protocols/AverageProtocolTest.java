package ie.nix.dvmc.cdc.protocols;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.TestProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.GeneralNode;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDSimulator;

public class AverageProtocolTest {
	
	private static final Logger LOGGER = LogManager.getLogger();

	public static class TestNode extends GeneralNode {

		public static final String AVERAGE_PROTOCOL = "average";

		private int averageProtocolID;
		
		private double value;

		public TestNode(String prefix) {
			super(prefix);
			value = (double)CommonState.r.nextInt(10);
			averageProtocolID = Configuration.lookupPid(AVERAGE_PROTOCOL);
	        LOGGER.info("[0] prefix={}, this={}, value={}", prefix, this, value);	
		}
		
		@Override
		public TestNode clone() {
			TestNode clone = (TestNode)super.clone();
			value = (double)CommonState.r.nextInt(10);
		    LOGGER.info("[0] clone={}, value={}", clone, value);	
			return clone;
		}
 
		public double getValue() {
			return value;
		}

		public void setValue(double value) {
			this.value = value;		
		}
		
		public AverageProtocol getAverageProtocolProtocol() {
			return (AverageProtocol)this.getProtocol(getAverageProtocolID());
		}

		public int getAverageProtocolID() {
			return averageProtocolID;
		}

		@Override
		public String toString() {
			return "TestNode [ID="+this.getID()+", value=" + value + "]";
		}
	}
	
	int random_seed= 1;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/java/ie/nix/dvmc/cdc/protocols/AverageProtocolTest.properties"});
		
	}

	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to "+random_seed);
	}

	@AfterEach
	void afterEach() {
//		TestNode.clearAll();
//		System.out.println();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	@Test
    @DisplayName("Basic Static Average Test")
    public void basicStaticAverageTest() {
		EDSimulator.nextExperiment();
		
        double actualAverage = getActualAverage();
        LOGGER.info("~actualAverage={}", actualAverage);	
        double protocolAverage = getProtocolAverage();
        LOGGER.info("~protocolAverage={}", protocolAverage);	
        double protocolStandardDeviation = getProtocolStandardDeviation(protocolAverage);
        LOGGER.info("~protocolStandardDeviation={}", protocolStandardDeviation);	
        
        Assertions.assertEquals(actualAverage, getProtocolAverage(0), 0.1);
        Assertions.assertEquals(actualAverage, protocolAverage, 0.1);
        Assertions.assertEquals(0.0, protocolStandardDeviation, 0.1);
    }
	
	@Test
	@DisplayName("Basic Dynamic Average Test")
    public void basicDynamicAverageTest() {
		TestProtocol.nextCycle = (Node node, int protocolID) -> {
			LOGGER.trace("[{}]~nextCycle() node={}, protocolID={}", 
					CommonState.getTime(), node, protocolID);
			if (CommonState.getTime() == 300) {
				TestNode testNode = (TestNode)node;
				double newValue = (double)CommonState.r.nextInt(20);
				LOGGER.info("[{}]~nextCycle() node={}, protocolID={}, Updating value {} -> {}", 
						CommonState.getTime(), node, protocolID, testNode.getValue(), newValue);
				testNode.setValue(newValue);
			}
		};
		
		EDSimulator.nextExperiment();
		
		double actualAverage = getActualAverage();
        LOGGER.info("~actualAverage={}", actualAverage);	
        double protocolAverage = getProtocolAverage();
        LOGGER.info("~protocolAverage={}", protocolAverage);	
        double protocolStandardDeviation = getProtocolStandardDeviation(protocolAverage);
        LOGGER.info("~protocolStandardDeviation={}", protocolStandardDeviation);	
        
        Assertions.assertEquals(actualAverage, getProtocolAverage(0), 0.1);
        Assertions.assertEquals(actualAverage, protocolAverage, 0.1);
        Assertions.assertEquals(0.0, protocolStandardDeviation, 0.1);
    }
	
	protected double getActualAverage() {
		int numberOfNodes = Network.size();
		double sum = 0;
		int numberOfNodesInSum = 0;
		for (int n = 0; n < numberOfNodes; n++) {
			TestNode testNode = (TestNode)Network.get(n);
			double testNodeValue = testNode.getValue();
			if (testNodeValue != AverageProtocol.IGNORE_VALUE) {
				sum += testNode.getValue();
				numberOfNodesInSum++;
		        LOGGER.debug("~value={}, sum={}, n={}, working average={}", 
		        		testNode.getValue(), sum, n, sum/numberOfNodesInSum);
			} else {
		        LOGGER.debug("~value={}, not including n={}, working average={}", 
		        		testNode.getValue(), n, sum/numberOfNodesInSum);
				
			}
		}
		return sum/numberOfNodesInSum;
	}
	
	protected double getProtocolAverage() {
		int numberOfNodes = Network.size();
		double sum = 0;
		int numberOfNodesInSum = 0;
		for (int n = 0; n < numberOfNodes; n++) {
			TestNode testNode = (TestNode)Network.get(n);
			double testNodeValue = testNode.getValue();
			if (testNodeValue != AverageProtocol.IGNORE_VALUE) {
				sum += getProtocolAverage(n);
				numberOfNodesInSum++;
		        LOGGER.debug("~sum={}, n={}, working average={}", 
		        		sum, n, sum/numberOfNodesInSum);
			} else {
		        LOGGER.debug("~sum={}, not including n={}, working average={}", 
		        		sum, n, sum/numberOfNodesInSum);
				
			}
		}
		return sum/numberOfNodesInSum;
	}
	
	protected double getProtocolStandardDeviation(double average) {
		int numberOfNodes = Network.size();
		double sum = 0;
		int numberOfNodesInSD = 0;
		for (int n = 0; n < numberOfNodes; n++) {
			TestNode testNode = (TestNode)Network.get(n);
			double testNodeValue = testNode.getValue();
			if (testNodeValue != AverageProtocol.IGNORE_VALUE) {
				sum += Math.pow((getProtocolAverage(n) - average), 2);
				numberOfNodesInSD++;
		        LOGGER.debug("~sum={}, n={}, working sd={}", 
		        		sum, n, sum/numberOfNodesInSD);
			} else {
		        LOGGER.debug("~sum={}, not including n={}, working sd={}", 
		        		sum, n, sum/numberOfNodesInSD);
				
			}
		}
		return sum/numberOfNodesInSD;
	}

	private double getProtocolAverage(int n) {
		TestNode testNode = (TestNode)Network.get(n);
		AverageProtocol averageProtocol = (AverageProtocol)testNode.getProtocol(0);
        LOGGER.debug("~averageProtocol={}, n={}, average={}", averageProtocol, n, averageProtocol.getAverage());
		return averageProtocol.getAverage();
	}

}