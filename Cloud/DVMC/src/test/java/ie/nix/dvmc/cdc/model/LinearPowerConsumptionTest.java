package ie.nix.dvmc.cdc.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import peersim.core.CommonState;

public class LinearPowerConsumptionTest {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LogManager.getLogger();
	
	protected static CloudDataCenter cdc;
	protected static double emptyPower;
	protected static double halfPower;
	protected static double fullPower;
	
	int random_seed = 1;
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/exp.properties"});
		
	}

	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to "+random_seed);
		
		cdc = CloudDataCenter.getCloudDataCenter();
		
		/*
		 * "Name",   "0%", 25%, "100%"
		 * "Host", 	  175, 225,  250 
		 */
		emptyPower = 175.0;
		halfPower = 225.0;
		fullPower = 250.0;
		
	}

	@AfterEach
	void afterEach() {
		cdc.provisionVMs();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}

	@Test
    @DisplayName("Power Consumption Test Empty")
    public void powerConsumptionEmptyTest() {
		
		//Added vm=VM5-6 [cpu=500.00, ram=0.50] to host=Host-2 [cpu=0.08, ram=0.13]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));
		
		double expectedPowerConsumption = 0.0;
		powerConsumptionTest(host, expectedPowerConsumption);
    }

	public void powerConsumptionTest(Host host, double expectedPowerConsumption) {
		double actualPowerConsumption = host.getPowerConsumption();
		Assertions.assertEquals(expectedPowerConsumption, actualPowerConsumption);
	}
	
	@Test
    @DisplayName("Power Consumption for Util Test Empty")
    public void powerConsumptionForUtilTestEmpty() {
		
		//Added vm=VM5-6 [cpu=500.00, ram=0.50] to host=Host-2 [cpu=0.08, ram=0.13]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));

		double utilization = 0.0;
		double expectedPowerConsumption = emptyPower;
		
		powerConsumptionForUtilTest(host, utilization, expectedPowerConsumption);
		
    }
	
	@Test
    @DisplayName("Power Consumption for Util Test 0p25")
    public void powerConsumptionForUtilTest0p25() {
		
		//Added vm=VM5-6 [cpu=500.00, ram=0.50] to host=Host-2 [cpu=0.08, ram=0.13]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));

		double utilization = 0.25;
		double expectedPowerConsumption = ((halfPower - emptyPower) / 2) + emptyPower;
		
		powerConsumptionForUtilTest(host, utilization, expectedPowerConsumption);
		
    }
	
	@Test
    @DisplayName("Power Consumption for Util Test 0p5")
    public void powerConsumptionForUtilTest0p5() {
		
		//Added vm=VM5-6 [cpu=500.00, ram=0.50] to host=Host-2 [cpu=0.08, ram=0.13]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));

		double utilization = 0.5;
		double expectedPowerConsumption = halfPower;
		
		powerConsumptionForUtilTest(host, utilization, expectedPowerConsumption);
		
    }
	
	@Test
    @DisplayName("Power Consumption for Util Test 0p75")
    public void powerConsumptionForUtilTest0p75() {
		
		//Added vm=VM5-6 [cpu=500.00, ram=0.50] to host=Host-2 [cpu=0.08, ram=0.13]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));

		double utilization = 0.75;
		double expectedPowerConsumption = ((fullPower - halfPower) / 2) + halfPower;
		
		powerConsumptionForUtilTest(host, utilization, expectedPowerConsumption);
		
    }
	
	@Test
    @DisplayName("Power Consumption for Util Test 0p95")
    public void powerConsumptionForUtilTest0p95() {
		
		//Added vm=VM5-6 [cpu=500.00, ram=0.50] to host=Host-2 [cpu=0.08, ram=0.13]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));

		double utilization = 0.95;
		double expectedPowerConsumption = ((fullPower - halfPower) * ((0.95 - 0.5) / 0.5)) + halfPower;
		
		powerConsumptionForUtilTest(host, utilization, expectedPowerConsumption);
		
    }
	
	@Test
    @DisplayName("Power Consumption for Util Test Full")
    public void powerConsumptionForUtilTestFull() {
		
		//Added vm=VM5-6 [cpu=500.00, ram=0.50] to host=Host-2 [cpu=0.08, ram=0.13]
		Host host = cdc.getHosts().get(2);
		host.removeVM(cdc.getVMs().get(6));

		double utilization = 1.0;
		double expectedPowerConsumption = fullPower;
		
		powerConsumptionForUtilTest(host, utilization, expectedPowerConsumption);
		
    }
	
	public void powerConsumptionForUtilTest(Host host, double utilization, double expectedPowerConsumption) {
		double actualPowerConsumption = host.getPowerConsumptionForUtil(utilization);
		Assertions.assertEquals(expectedPowerConsumption, actualPowerConsumption);
	}
	
	@Test
    @DisplayName("Linear Power Consumption Test 0")
    public void linearPowerConsumptionTest0p0() {
		Host host = cdc.getHosts().get(2);
		double utilization = 0.0;
		double expectedPowerConsumption = emptyPower;
		
		linearPowerConsumptionTest(host, utilization, expectedPowerConsumption);
	}
	
	@Test
    @DisplayName("Linear Power Consumption Test 0p5")
    public void linearPowerConsumptionTest0p5() {
		Host host = cdc.getHosts().get(2);
		double utilization = 0.5;
		double expectedPowerConsumption = halfPower;
		
		linearPowerConsumptionTest(host, utilization, expectedPowerConsumption);
	}
	
	@Test
    @DisplayName("Linear Power Consumption Test 1")
    public void linearPowerConsumptionTest1p0() {
		Host host = cdc.getHosts().get(2);
		double utilization = 1.0;
		double expectedPowerConsumption = fullPower;
		
		linearPowerConsumptionTest(host, utilization, expectedPowerConsumption);
	}
	
	@Test
    @DisplayName("Linear Power Consumption Test Under")
    public void linearPowerConsumptionTestUnder() {
		Host host = cdc.getHosts().get(2);
		double utilization = -0.2;
		double expectedPowerConsumption = emptyPower;
		
		linearPowerConsumptionTest(host, utilization, expectedPowerConsumption);
	}
	
	@Test
    @DisplayName("Linear Power Consumption Test Over")
    public void linearPowerConsumptionTestOver() {
		Host host = cdc.getHosts().get(2);
		double utilization = 1.2;
		double expectedPowerConsumption = fullPower;
		
		linearPowerConsumptionTest(host, utilization, expectedPowerConsumption);
	}

	public void linearPowerConsumptionTest(Host host, double utilization, double expectedPowerConsumption) {
		LinearPowerConsumption lpc = (LinearPowerConsumption)cdc.getHostPowerModel();
		double actualPowerConsumption = lpc.powerConsumption(host, utilization);
		Assertions.assertEquals(expectedPowerConsumption, actualPowerConsumption);
	}
	
//	@Test
//    @DisplayName("Linear Power Consumption Test Over")
//    public void linearPowerConsumptionTestOver() {
//		Host host = cdc.getHosts().get(2);
//		double utilization = 1.2;
//		double expectedPowerConsumption = fullPower;
//
//		LinearPowerConsumption lpc = (LinearPowerConsumption)cdc.getHostPowerModel();
//		
//		lpc.
//		
//		double actualPowerConsumption = lpc.powerConsumption(host, utilization);
//		Assertions.assertEquals(expectedPowerConsumption, actualPowerConsumption);
//	}
	
//	@Test
//    @DisplayName("Basic Static Average Test")
//    public void basicStaticAverageTest() {
//		EDSimulator.nextExperiment();
//		
//        double actualAverage = getActualAverage();
//        LOGGER.info("~actualAverage={}", actualAverage);	
//        double protocolAverage = getProtocolAverage();
//        LOGGER.info("~protocolAverage={}", protocolAverage);	
//        double protocolStandardDeviation = getProtocolStandardDeviation(protocolAverage);
//        LOGGER.info("~protocolStandardDeviation={}", protocolStandardDeviation);	
//        
//        Assertions.assertEquals(actualAverage, getProtocolAverage(0), 0.1);
//        Assertions.assertEquals(actualAverage, protocolAverage, 0.1);
//        Assertions.assertEquals(0.0, protocolStandardDeviation, 0.1);
//    }
//	
//	@Test
//	@DisplayName("Basic Dynamic Average Test")
//    public void basicDynamicAverageTest() {
//		TestProtocol.nextCycle = (Node node, int protocolID) -> {
//			LOGGER.trace("[{}]~nextCycle() node={}, protocolID={}", 
//					CommonState.getTime(), node, protocolID);
//			if (CommonState.getTime() == 300) {
//				TestNode testNode = (TestNode)node;
//				double newValue = (double)CommonState.r.nextInt(20);
//				LOGGER.info("[{}]~nextCycle() node={}, protocolID={}, Updating value {} -> {}", 
//						CommonState.getTime(), node, protocolID, testNode.getValue(), newValue);
//				testNode.setValue(newValue);
//			}
//		};
//		
//		EDSimulator.nextExperiment();
//		
//		double actualAverage = getActualAverage();
//        LOGGER.info("~actualAverage={}", actualAverage);	
//        double protocolAverage = getProtocolAverage();
//        LOGGER.info("~protocolAverage={}", protocolAverage);	
//        double protocolStandardDeviation = getProtocolStandardDeviation(protocolAverage);
//        LOGGER.info("~protocolStandardDeviation={}", protocolStandardDeviation);	
//        
//        Assertions.assertEquals(actualAverage, getProtocolAverage(0), 0.1);
//        Assertions.assertEquals(actualAverage, protocolAverage, 0.1);
//        Assertions.assertEquals(0.0, protocolStandardDeviation, 0.1);
//    }
//	
//	protected double getActualAverage() {
//		int numberOfNodes = Network.size();
//		double sum = 0;
//		int numberOfNodesInSum = 0;
//		for (int n = 0; n < numberOfNodes; n++) {
//			TestNode testNode = (TestNode)Network.get(n);
//			double testNodeValue = testNode.getValue();
//			if (testNodeValue != AverageProtocol.IGNORE_VALUE) {
//				sum += testNode.getValue();
//				numberOfNodesInSum++;
//		        LOGGER.debug("~value={}, sum={}, n={}, working average={}", 
//		        		testNode.getValue(), sum, n, sum/numberOfNodesInSum);
//			} else {
//		        LOGGER.debug("~value={}, not including n={}, working average={}", 
//		        		testNode.getValue(), n, sum/numberOfNodesInSum);
//				
//			}
//		}
//		return sum/numberOfNodesInSum;
//	}
//	
//	protected double getProtocolAverage() {
//		int numberOfNodes = Network.size();
//		double sum = 0;
//		int numberOfNodesInSum = 0;
//		for (int n = 0; n < numberOfNodes; n++) {
//			TestNode testNode = (TestNode)Network.get(n);
//			double testNodeValue = testNode.getValue();
//			if (testNodeValue != AverageProtocol.IGNORE_VALUE) {
//				sum += getProtocolAverage(n);
//				numberOfNodesInSum++;
//		        LOGGER.debug("~sum={}, n={}, working average={}", 
//		        		sum, n, sum/numberOfNodesInSum);
//			} else {
//		        LOGGER.debug("~sum={}, not including n={}, working average={}", 
//		        		sum, n, sum/numberOfNodesInSum);
//				
//			}
//		}
//		return sum/numberOfNodesInSum;
//	}
//	
//	protected double getProtocolStandardDeviation(double average) {
//		int numberOfNodes = Network.size();
//		double sum = 0;
//		int numberOfNodesInSD = 0;
//		for (int n = 0; n < numberOfNodes; n++) {
//			TestNode testNode = (TestNode)Network.get(n);
//			double testNodeValue = testNode.getValue();
//			if (testNodeValue != AverageProtocol.IGNORE_VALUE) {
//				sum += Math.pow((getProtocolAverage(n) - average), 2);
//				numberOfNodesInSD++;
//		        LOGGER.debug("~sum={}, n={}, working sd={}", 
//		        		sum, n, sum/numberOfNodesInSD);
//			} else {
//		        LOGGER.debug("~sum={}, not including n={}, working sd={}", 
//		        		sum, n, sum/numberOfNodesInSD);
//				
//			}
//		}
//		return sum/numberOfNodesInSD;
//	}
//
//	private double getProtocolAverage(int n) {
//		TestNode testNode = (TestNode)Network.get(n);
//		AverageProtocol averageProtocol = (AverageProtocol)testNode.getProtocol(0);
//        LOGGER.debug("~averageProtocol={}, n={}, average={}", averageProtocol, n, averageProtocol.getAverage());
//		return averageProtocol.getAverage();
//	}

}