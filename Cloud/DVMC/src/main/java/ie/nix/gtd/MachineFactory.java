package ie.nix.gtd;

import ie.nix.dvmc.cdc.model.FileBasedHostFactory;

public class MachineFactory extends FileBasedHostFactory {

	public MachineFactory(String prefix) {
		// HostSpec.csv:      	"Name", "Number of Cores", "CPU (GHz)", "RAM (GB)"
		// MachineTypes.csv: 	"Name", "Number of Cores", "CPUs", "Memory"
		
		// Hosts.csv:  			"Host", "Host Spec"
		// Machines.csv 		"Machine","Machine Type"
		super(prefix, "Machine Type","Number of Cores","CPU (GHz)", "RAM (GB)", "Machine","Machine Type"); 
	}

}
