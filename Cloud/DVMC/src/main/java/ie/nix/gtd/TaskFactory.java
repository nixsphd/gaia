package ie.nix.gtd;

import ie.nix.dvmc.cdc.model.FileBasedVMFactory;

public class TaskFactory extends FileBasedVMFactory {

	public TaskFactory(String prefix) {

		// VMSpec.csv:      	"Name", "Number of Cores", "CPU (GHz)", "RAM (GB)"
		// TaskTypes.csv: 		"Name", "MPIS", "memory request"
		
		// VMs.csv:  			"VM", "VM Spec"
		// Tasks.csv 			"Task", "Task Type"
		super(prefix, "Task Type", "MIPS", "RAM (GB)", "Task","Task Type");
	}

}
