package ie.nix.gtd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMWorkloadModel;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.cdc.model.CSVFileControl;
import ie.nix.dvmc.cdc.protocols.CallbackProtocol;
import ie.nix.dvmc.cdc.protocols.CallbackProtocol.Callback;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class TaskUsage extends CSVFileControl implements VMWorkloadModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static final String CSV_FILE = "task-usage-file";
	public static final String START_TIME = "start-time";
	public static final String END_TIME = "end-time";

	private static final int DEFAULT_START_TIME = 0;
	private static final int DEFAULT_END_TIME = 60 * 60;
	 
	private int startTime;
	private int endTime;
	private double[] workloads;
	
	public TaskUsage(String prefix) {
		super(prefix);
		startTime = Configuration.getInt(prefix + "." + START_TIME, DEFAULT_START_TIME);
		endTime = Configuration.getInt(prefix + "." + END_TIME, DEFAULT_END_TIME);
		LOGGER.info("[{}]~startTime {}, endTime {}", CommonState.getTime(), startTime, endTime);
	}

	@Override
	public boolean isDynamic() {
		return true;
	}
	
	@Override
	public double getWorkload(VM vm) {
        LOGGER.trace("[{}]~{} -> {}", CommonState.getTime(), 
        		vm.id, workloads[vm.id]);
		return workloads[vm.id];
	}
	
	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		int numberOfVMs = cdc.getNumberOfVMs();
		int ignoredTasks = 0;
		int ignoredTime = 0;
		workloads = new double[cdc.getNumberOfVMs()];
        LOGGER.info("[{}] tracking workload for {} VMs", CommonState.getTime(), workloads.length);
		
		for (int row = 0; row < df.getNumberOfRows(); row++) {
			// "time","Task","CPU rate"
			int time		= Integer.valueOf(df.getData(row, "time").toString());
			int task 		= Integer.valueOf(df.getData(row, "Task").toString());
			double workload = Double.valueOf(df.getData(row, "MIPS usage").toString());
			if (task < numberOfVMs) {
		        if (time <= startTime) {
		        	workloads[task] = workload;
			        LOGGER.debug("[{}]~Init time={}, task={}, workload={}", 
			        		CommonState.getTime(),time, task, workload);
		        } else if (time <= endTime) {
		        	CallbackProtocol.addCallback((time - startTime), new Callback(() -> {
			        	workloads[task] = workload;
				        LOGGER.debug("[{}]~Update time={}, task={}, workload={}", 
				        		CommonState.getTime(),(time - startTime), task, workload);
		        	}));
		        } else {
		        	ignoredTime++;
		        }
			} else {
				ignoredTasks++;
			}
	        
		}
		CloudDataCenter.getCloudDataCenter().setVMWorkloadModel(this);

        LOGGER.info("[{}]~ignoredTime={} ignoredTasks={} ", CommonState.getTime(), ignoredTime, ignoredTasks);
		return false;
	}
		
	public boolean step() {
        LOGGER.trace("[{}]~Doing nothing", CommonState.getTime());
		return false;
	}
}