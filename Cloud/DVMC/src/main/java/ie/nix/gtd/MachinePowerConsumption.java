package ie.nix.gtd;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.model.LinearPowerConsumption;
import peersim.core.CommonState;

public class MachinePowerConsumption extends LinearPowerConsumption {

	private static final Logger LOGGER = LogManager.getLogger();

	public MachinePowerConsumption(String prefix) {
		super(prefix, "Machine Type");
		LOGGER.trace("[{}]", CommonState.getTime());
	}

}