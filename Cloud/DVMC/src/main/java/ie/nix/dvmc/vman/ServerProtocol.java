package ie.nix.dvmc.vman;

import peersim.cdsim.CDProtocol;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.edsim.EDSimulator;
import peersim.transport.Transport;
import peersim.vector.SingleValueHolder;

public class ServerProtocol extends SingleValueHolder implements CDProtocol, EDProtocol {

	public static class Callback {
		final Runnable action;

		public Callback(Runnable action) {
			this.action = action;
		}

	}
	
	public static class Message {

		final Node sender;
		final int numberOfVMs;
		
		public Message(Node sender, int numberOfVMs) {
			this.sender = sender;
			this.numberOfVMs = numberOfVMs;
		}
		
	}
	
	public static class UpdateMessage {

		final Node sender;
		final int newNumberOfVMs;
		
		public UpdateMessage(Node sender, int newNumberOfVMs) {
			this.sender = sender;
			this.newNumberOfVMs = newNumberOfVMs;
		}
		
	}
	
	public static int vmCapacity = 8; // C
	
	public int numberOfVMs = 4; // Hi

	private UpdateMessage updateMessage; 
	
	public ServerProtocol(String prefix) {
		super(prefix);
//		System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol()");
	}

	@Override
	public void nextCycle(Node node, int protocolID) {
//		System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol.nextCycle("+node.getID()+")");
		// This is server i
		int linkableID = FastConfig.getLinkable(protocolID);
        Linkable linkable = (Linkable)node.getProtocol(linkableID);
        Transport transport = ((Transport)node.getProtocol(FastConfig.getTransport(protocolID)));
		// for all j in getPeers(i) do
        	for (int j = 0; j < linkable.degree(); j++) {
        		Node neirgbour = linkable.getNeighbor(j);
        		ServerProtocol neghbourServerProtocol = (ServerProtocol) neirgbour.getProtocol(protocolID);
                
    			// Check the neighbour is up
    			if(neirgbour.isUp() && neghbourServerProtocol != this) {
    				
    				// Send (Hi) to j.
    				Message message = new Message(node, numberOfVMs);
    				transport.send(node, neirgbour, message, protocolID);
    				System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol.nextCycle("+node.getID()+") sending "+numberOfVMs+" to neirgbour "+neirgbour.getID());

        			int delay = 10;
        			EDSimulator.add(delay, new Callback(() -> {
        				System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol[" + node.getID()+ "].nextCycle()");
   				
        				if (updateMessage == null) {
        					EDSimulator.add(delay, this, node, protocolID);
        					
        				} else {
        					
        					// Hi <- Hi'
                			numberOfVMs = updateMessage.newNumberOfVMs;
                			System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol.nextCycle("+node.getID()+")~setting numberOfVMs to "+numberOfVMs);

        				}
        				
        			 }), node, protocolID);

    			}
        } // end for
		
	}

	@Override
	public void processEvent(Node node, int protocolID, Object event) {
//		System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol.processEvent("+node.getID()+")");

		// This is server i getting a message from server j
        Transport transport = ((Transport)node.getProtocol(FastConfig.getTransport(protocolID)));
        
        if (event instanceof Callback) {
			((Callback)event).action.run();
        } else if (event instanceof Message) {
        	
			handleMessage(node, protocolID, event, transport);
			
        } else if (event instanceof UpdateMessage) {

			handleUpdateMessage(node, protocolID, event, transport);
			
        }
		
	}

	public void handleMessage(Node node, int protocolID, Object event, Transport transport) {
		Message message = (Message)event;
		Node sender = message.sender; // J
		int remoteNumberOfVMs = message.numberOfVMs; // Hj
		
		// if (Hi > Hj) then
		if (numberOfVMs > remoteNumberOfVMs) {
			
			// D <- Min(Hj,C - Hi)
			int d = Math.min(remoteNumberOfVMs, vmCapacity - numberOfVMs);
			
			// Send Hj - D to j
			UpdateMessage replyMessage = new UpdateMessage(node, remoteNumberOfVMs - d);
			transport.send(node, sender, replyMessage, protocolID);
			
			// Hi <- Hi + D
			numberOfVMs += d;

			System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol.processEvent("+node.getID()+")~"
					+ "Migrating "+d+" from "+node.getID()+"->"+sender.getID());
			System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol.processEvent("+node.getID()+")~"
					+ "Setting numberOfVMs to "+numberOfVMs);
			
		} else {
			// D <- Min(Hi,C - Hj)
			int d = Math.min(numberOfVMs, vmCapacity - remoteNumberOfVMs);
			
			// Send Hj + D to j
			UpdateMessage replyMessage = new UpdateMessage(node, remoteNumberOfVMs + d);
			transport.send(node, message.sender, replyMessage, protocolID);
			
			// Hi <- Hi - D
			numberOfVMs -= d;

			System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol.processEvent("+node.getID()+")~"
					+ "Migrating "+(-d)+" from "+node.getID()+"->"+sender.getID());
			System.out.println("NDB::[" + CommonState.getTime() + "]ServerProtocol.processEvent("+node.getID()+")~setting numberOfVMs to "+numberOfVMs);
		
		}
	}

	public void handleUpdateMessage(Node node, int protocolID, Object event, Transport transport) {
		updateMessage = (UpdateMessage)event;
		
	}

}
