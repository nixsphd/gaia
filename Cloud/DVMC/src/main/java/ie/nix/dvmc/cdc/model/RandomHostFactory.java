package ie.nix.dvmc.cdc.model;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter.HostFactory;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.Host.HostSpec;
import peersim.core.CommonState;

public class RandomHostFactory extends CSVFileControl implements HostFactory {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	private final HostSpec[] hostSpecs;
	
	public RandomHostFactory(String prefix) {
		this(prefix, "Name", "Number of Cores", "CPU (GHz)", "RAM (GB)");
	}

	public RandomHostFactory(String prefix, String hostSpecNameHeader, 
			String numberOfCoresHeader, String cpuHeader,String ramHeader) {
		super(prefix);
		this.hostSpecs = new HostSpec[df.getNumberOfRows()];
		for (int row = 0; row < df.getNumberOfRows(); row++) {
			// "Name",  "Number of Cores",  "CPU (GHz)"
			this.hostSpecs[row] = new HostSpec(
					removeQuotes((String)df.getData(row, hostSpecNameHeader)), 
					Integer.valueOf(df.getData(row, numberOfCoresHeader).toString()), 
					Double.valueOf(df.getData(row, cpuHeader).toString()), 
					Double.valueOf(df.getData(row, ramHeader).toString()));
	        LOGGER.trace("[{}]~Added HostSpec={}", 
	        		CommonState.getTime(), hostSpecs[row]);
	        
		}
        LOGGER.debug("[{}]", CommonState.getTime());
	}
	
	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
        LOGGER.debug("[{}]", CommonState.getTime());
		return false;
	}

	public boolean step() { return false; }

	public Host newHost() {
		return new Host(hostSpecs[CommonState.r.nextInt(hostSpecs.length)]);
	}

	public Stream<HostSpec> getHostSpecs() {
		return Stream.of(hostSpecs);
	}
	
}