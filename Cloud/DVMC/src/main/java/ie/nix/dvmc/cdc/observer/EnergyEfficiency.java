package ie.nix.dvmc.cdc.observer;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import peersim.core.CommonState;

public class EnergyEfficiency extends Observer {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	public EnergyEfficiency(String prefix) {
		super(prefix);
		writeHeaders(Stream.of("time", "power"));
	}

	public void observe(CloudDataCenter cdc) {
        LOGGER.debug("[{}]", CommonState.getTime());

		double powerConsumption = cdc.getHosts().stream().
				mapToDouble(host -> host.getPowerConsumption()).sum();
		writeRows(Stream.of(CommonState.getTime(), powerConsumption));

        LOGGER.info("[{}]~powerConsumption={}", CommonState.getTime(),
        		String.format("%.0f", powerConsumption));

	}

}