package ie.nix.dvmc.cdc.protocols;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;

public class LockProtocol implements EDProtocol {

	private static final Logger LOGGER = LogManager.getLogger();
	
	/**
	 * The type of a message. It contains a value of type double and the sender node
	 * of type {@link peersim.core.Node}.
	 */
	public static class LockMessage {

		public static enum Type {
			REQUEST, RESPONSE
		};

		/** If not null, this has to be answered, otherwise this is the answer. */
		final Node sender;
		final Type type;
		final boolean lock;

		public LockMessage(Node sender, Type type, boolean lock) {
			this.sender = sender;
			this.type = type;
			this.lock = lock;
	        LOGGER.trace("[{}]~sender={}, type={}, lock={}", CommonState.getTime(), sender, type, lock);
		}

		@Override
		public String toString() {
			return "LockMessage [sender=" + sender + ", type=" + type + ", lock=" + lock + "]";
		}

	}

	// Node that this node is locked by.
	protected Node lockedBy;

	protected List<Node> lockedNeighbours;

	public LockProtocol(String prefix) {
		this.lockedNeighbours = new ArrayList<Node>();
        LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);
	}

	@Override
	public LockProtocol clone() {
		LockProtocol foo = null;
		try {
			foo = (LockProtocol) super.clone();
			foo.lockedNeighbours = new ArrayList<Node>();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}

	public List<Node> getLockedNeighbours() {
		return lockedNeighbours;
	}

	public void lockNeignbours(Node node, int protocolID) {
	    LOGGER.trace("[{}]~node={}, protocolID={}", CommonState.getTime(), node, protocolID);
		Linkable neighbourhood = (Linkable) node.getProtocol(FastConfig.getLinkable(protocolID));
		Transport transport = (Transport) node.getProtocol(FastConfig.getTransport(protocolID));
	
		if (!isLocked()) {
	
			// First lock yourself
			lockBy(node);
	        LOGGER.debug("[{}]~node={}, protocolID={}, lock self", CommonState.getTime(), node, protocolID);
	
			for (int n = 0; n < neighbourhood.degree(); n++) {
				Node neighbourNode = neighbourhood.getNeighbor(n);
	
				// Failure handling
				if (!neighbourNode.isUp()) {
					// Do nothing...sure we'll try the next one.
				}
	
				transport.send(node, neighbourNode, new LockMessage(node, LockMessage.Type.REQUEST, true), protocolID);
		        LOGGER.debug("[{}]~node={}, protocolID={}, send REQUEST to {}, lock={}", 
		        		CommonState.getTime(), node, protocolID, neighbourNode, true);
	
			}
		} else {
			LOGGER.debug("[{}]~node={}, protocolID={}, already locked by {}", 
					CommonState.getTime(), node, protocolID, lockedBy.getID());
			
		}
	}

	public void unlockNeignbours(Node node, int protocolID) {
	    LOGGER.trace("[{}]~node={}, protocolID={}", 
	    		CommonState.getTime(), node, protocolID);
	
		if (isLocked() && lockedBy == node) {
			Transport transport = (Transport) node.getProtocol(FastConfig.getTransport(protocolID));
	
			for (Node lockedNeighbour : lockedNeighbours) {
	
				// Either there were not locked hosts or they were not up.
				if (lockedNeighbour != null && !lockedNeighbour.isUp()) {
					// Do nothing...sure we'll try the next one.
				}
	
				transport.send(node, lockedNeighbour, new LockMessage(node, LockMessage.Type.REQUEST, false),
						protocolID);
		        LOGGER.debug("[{}]~node={}, protocolID={}, send REQUEST to {}, lock={}", 
		        		CommonState.getTime(), node, protocolID, lockedNeighbour, false);
			}
	
			// Lastly unlock this host
			unlock();
	        LOGGER.debug("[{}]~node={}, protocolID={}, unlock self", 
	        		CommonState.getTime(), node, protocolID);
		}
	}

	public void processEvent(Node node, int protocolID, Object event) {
        LOGGER.trace("[{}]~node={}, protocolID={}, event={}", 
        		CommonState.getTime(), node, protocolID, event);
		if (event instanceof LockMessage) {
			handleLockMessage(node, protocolID, event);
		}
	}

	protected void handleLockMessage(Node node, int protocolID, Object event) { 
		LOGGER.trace("[{}]~node={}, protocolID={}, event={}", 
				CommonState.getTime(), node, protocolID, event);
		LockMessage message = (LockMessage) event;
		// If there is a sender then they want a reply, so send them back the value,
		// before it's updated.
		if (message.type == LockMessage.Type.REQUEST) {
			// if the request is to lock and not already locked
			if (message.lock && !isLocked()) {
				lockBy(message.sender);
				respondToLockMessage(node, protocolID, message);
	
			// if the request is to lock and are already locked and if lockedBy same
			} else if (message.lock && isLocked() && lockedBy == message.sender) {
				lockBy(message.sender);
				respondToLockMessage(node, protocolID, message);
				
			// if the request is to unlock and are already locked by the sender
			} else if (!message.lock && isLocked() && lockedBy == message.sender) {
				unlock();
				respondToLockMessage(node, protocolID, message);
		
			// if the request is to unlock and are already unlocked
			} else if (!message.lock && !isLocked()) {
				// if locked by sender, unlock and send an unlock reply, else ignore
				unlock();
				respondToLockMessage(node, protocolID, message);
			} else {
				// otherwise ignore
				LOGGER.debug("[{}]~node={}, protocolID={}, event={}, IGNORING, lockedBy={}, message.lock={}", 
						CommonState.getTime(), node, protocolID, event, 
						lockedBy, message.lock);
			}
			
		} else if (message.type == LockMessage.Type.RESPONSE) {
			if (message.lock) {
				lockedNeighbours.add(message.sender);
				LOGGER.debug("[{}]~node={}, protocolID={}, adding to locked neighbours.", 
						CommonState.getTime(), node, protocolID, message.sender);
	
			} else {
				lockedNeighbours.remove(message.sender);
				LOGGER.debug("[{}]~node={}, protocolID={}, adding to locked neighbours.", 
						CommonState.getTime(), node, protocolID, message.sender);
	
			}
		}
	}

	protected void respondToLockMessage(Node node, int protocolID, LockMessage message) {
		Transport transport = (Transport) node.getProtocol(FastConfig.getTransport(protocolID));
		transport.send(node, message.sender, new LockMessage(node, LockMessage.Type.RESPONSE, isLocked()),
				protocolID);
		LOGGER.debug("[{}]~node={}, protocolID={}, send RESPONSE to {} lock={}", 
				CommonState.getTime(), node, protocolID, message.sender, isLocked());
	}

	/*
	 * Lock Mechanism APIs
	 */
	protected boolean isLocked() {
		return lockedBy != null;
	}

	protected void lockBy(Node node) {
		LOGGER.trace("[{}]~node={}", CommonState.getTime(), node);	
		lockedBy = node;
	}

	protected void unlock() { 
		LOGGER.trace("[{}]", CommonState.getTime());
		lockedBy = null;
	}

}