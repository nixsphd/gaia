package ie.nix.dvmc.cdc.protocols;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.Host;
import peersim.core.CommonState;
import peersim.core.Fallible;

public class FailStateSleep extends AutoSleep {

	private static final Logger LOGGER = LogManager.getLogger();

	public FailStateSleep(String prefix) {
		super(prefix);
        LOGGER.trace("[{}]", CommonState.getTime());
	}

	@Override
	public FailStateSleep clone() {
		FailStateSleep foo = null;
		foo = (FailStateSleep) super.clone();
        LOGGER.debug("[{}]", CommonState.getTime());
		return foo;
	}

	/*
	 * Host Awake / Sleep protocol
	 */
	public boolean isAwake(Host host) {
		boolean isAwake = host.isUp();
        LOGGER.trace("[{}]~Host={} -> {}", CommonState.getTime(), host, isAwake);
		return isAwake;
	}

	public void sleep(Host host) {
        assert(!host.hasVMs()) :
        	"Putting host "+host+" to sleep even though it has VMs!!!";
        
        // This seems odd I agree. I need to make sure that Host
        // with index 0 is not put to sleep because if it is asleep 
        // then the callback will stop working. 
        // The proper fix for this is to implement a new scheduler 
        // strategy which does not use FAILSTATE to put the Hosts to 
        // sleep.
		if (host.getIndex() != 0) {
			host.setFailState(Fallible.DOWN);
	        LOGGER.debug("[{}]~Host-{}", CommonState.getTime(), host);
		} else {
	        LOGGER.debug("[{}]~Host-{} not sleeping as it needs to process the Callbacks", CommonState.getTime(), host);
		}

	}

	public void wakeUp(Host host) {
		host.setFailState(Fallible.OK);
        LOGGER.debug("[{}]~Host-{}", CommonState.getTime(), host);

	}

}