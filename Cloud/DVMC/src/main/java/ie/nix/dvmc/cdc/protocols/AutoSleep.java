package ie.nix.dvmc.cdc.protocols;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter.HostSleepModel;
import ie.nix.dvmc.cdc.Host;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;

public class AutoSleep implements HostSleepModel {

	private static final Logger LOGGER = LogManager.getLogger();

	public static final String FIND_MAS_HOPS = "find-max-hops";
	public static final int DEFAULT_FIND_MAS_HOPS = 10;

	private final int protocolID;

	private int maxHops;
	
	public AutoSleep(String prefix) {
        LOGGER.debug("[{}]", CommonState.getTime());
		this.protocolID = Configuration.lookupPid(prefix.replaceFirst("^protocol.", ""));
		this.maxHops = Configuration.getInt(prefix + "." + FIND_MAS_HOPS, DEFAULT_FIND_MAS_HOPS);
	}

	@Override
	public AutoSleep clone() {
		AutoSleep foo = null;
		try {
			foo = (AutoSleep) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.debug("[{}]", CommonState.getTime());
		return foo;
	}

	/*
	 * Host Awake / Sleep protocol
	 */
	public boolean isAwake(Host host) {
		return host.hasVMs();
	}

	public void sleep(Host host) {}

	public void wakeUp(Host host) {}
	
	@Override
	public Optional<Host> findSleepingHost(Host host) {
		int hops = 0;
		Host hostToCheck = host;
		while (hops < maxHops) {
			Linkable neighbourhood = 
					(Linkable) hostToCheck.getProtocol(FastConfig.getLinkable(protocolID));
			Builder<Host> builder = Stream.builder();
			for (int n = 0; n < neighbourhood.degree(); n++) {
				Host neighbourHost = (Host)neighbourhood.getNeighbor(n);
				if (!neighbourHost.isAwake()) {
					LOGGER.trace("[{}] found sleeping host={}", 
							CommonState.getTime(), neighbourHost);
					return Optional.of(neighbourHost);
				} else {
					builder.add(neighbourHost);
				} 
				LOGGER.trace("[{}] checking n={}, neighbourHost={}, hops={}", 
						CommonState.getTime(), n, neighbourHost, hops);
			}
			LOGGER.trace("[{}] hostToCheck={} has no sleeping neighbours of the {}", 
					CommonState.getTime(), hostToCheck, neighbourhood.degree());
			Stream<Host> neighbourHosts = builder.build();
			int nextHostIndex = CommonState.r.nextInt(neighbourhood.degree());
			hostToCheck = neighbourHosts.skip(nextHostIndex).findFirst().get();
			hops++;
			LOGGER.trace("[{}] next hostToCheck={}, hops={}", 
					CommonState.getTime(), hostToCheck, hops);
		}
		return Optional.empty();
	}

	@Override
	public void processEvent(Node node, int pid, Object event) {
		// Do nothing, no real events.
	}

}