package ie.nix.dvmc.cdc;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter.VMWorkloadModel;
import peersim.core.CommonState;

public class VM implements Cloneable {

	private static final Logger LOGGER = LogManager.getLogger();

	public static class VMSpec {

		// Name of the VM tyoe
		protected String name;

		// Number of cores
//		protected int cores;

		// MIPS
		protected int mips;
		
		// RAM (GB)
		protected final double ram;
		
		public VMSpec(String name, int mips) {
			this(name, mips, 0);
		}
		
		public VMSpec(String name, int mips, double ram) {
			super();
			this.name = name;
			this.mips = mips;
			this.ram = ram;
			LOGGER.trace("VMSpec({}, {}, {})", name, mips, ram);
		}

		@Override
		public String toString() {
			return name;
		}

		public String getName() {
			return name;
		}

//		public int getCores() {
//			return cores;
//		}

		public int getMIPs() {
			return mips;
		}

		public double getRam() {
			return ram;
		}
	}

	// Unique ID for the next VM
	private static int NEXT_UNIQUE_ID = 0;

	// The specification of the host
	public final VMSpec spec;

	// The unique id
	public int id;

	// when the VM will terminate
	private double terminationTime;

	// Records the time that the VM was first created
	private long startTime;

	// Host where the VM lives.
	private Optional<Host> optionalHost;

	public VM(VMSpec spec) {
		super();
		this.spec = spec;
		this.id = NEXT_UNIQUE_ID++;
		this.startTime = CommonState.getTime();
		this.terminationTime = -1; // Never terminate
		this.optionalHost = Optional.empty();
        LOGGER.trace("[{}]~spec={}", CommonState.getTime(), spec);
	}

	@Override
	public VM clone() {
		VM foo;
		try {
			foo = (VM) super.clone();
			foo.id = NEXT_UNIQUE_ID++;
			this.startTime = CommonState.getTime();
	        LOGGER.trace("[{}]~spec={}", CommonState.getTime(), spec);
		} catch (CloneNotSupportedException e) {
			throw new Error();
		}

		return foo;
	}

	@Override
	public String toString() {
		return spec.name + "-" +  + id
				+ " ["
				+ "cpu=" + String.format("%.2f", getWorkload())  + ", "
				+ "ram=" + String.format("%.2f", getRAM()) + "]";
	}

	public double getUptime() {
		double uptime = CommonState.getTime() - startTime;
        LOGGER.trace("[{}]~{}", CommonState.getTime(), uptime);
		return uptime;
	}

	public void setHost(Host host) {
        LOGGER.trace("[{}]~{}", CommonState.getTime(), host);
		this.optionalHost = Optional.ofNullable(host);
	}
	
	public Host getHost() {
		return optionalHost.orElse(null);
	}
	
	public Optional<Host> getOptionalHost() {
		return optionalHost;
	}

	/*
	 * Get the workload data form the workload model
	 */
	public double getWorkload() {
		VMWorkloadModel vmWorkloadModel = CloudDataCenter.getCloudDataCenter().getVMorkloadModel();
		double workload = spec.mips;
		if (vmWorkloadModel != null) {
			workload = vmWorkloadModel.getWorkload(this);
		}
        LOGGER.trace("[{}]~{}", CommonState.getTime(), workload);
		return workload;
	}

	public double getRAM() {
		return this.spec.ram;
	}

	public double getTerminationTime() {
        LOGGER.trace("[{}]~ -> {}", CommonState.getTime(), terminationTime);
		return terminationTime;
	}

	public void setTerminationTime(double terminationTime) {
        LOGGER.trace("[{}]~{}", CommonState.getTime(), terminationTime);
		this.terminationTime = terminationTime;
	}

}