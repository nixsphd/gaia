package ie.nix.dvmc.cdc.model;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMProvisionModel;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import peersim.core.CommonState;

public class RandomCanFit implements VMProvisionModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public RandomCanFit(String prefix) {
        LOGGER.trace("[{}]", CommonState.getTime());
		
	}

	@Override
	public boolean execute() {
	    LOGGER.trace("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	@Override
	public boolean init() {
		LOGGER.trace("[{}]", CommonState.getTime());
		return false;
	}

	public boolean step() { return false; }

	@Override
	public void provisionVM(VM vm) {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<Host> hosts = cdc.getHosts();
		
		// Seem odd, but this way we try hosts.size() times to add a vm to a host.
		for (int h = 0; h < hosts.size(); h++) {
			Host host = (Host) hosts.get(CommonState.r.nextInt(hosts.size()));
			if (host.canFitVM(vm)) {
				host.addVM(vm);
				LOGGER.debug("[{}]~provisioning vm {} on host {}", 
						CommonState.getTime(), vm, host);
				return;
			}
		}
		LOGGER.debug("[{}]~can't provisioning vm {}", 
				CommonState.getTime(), vm);
	
	}
	
}