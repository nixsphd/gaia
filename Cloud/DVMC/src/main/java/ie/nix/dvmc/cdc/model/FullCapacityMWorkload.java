package ie.nix.dvmc.cdc.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMWorkloadModel;
import ie.nix.dvmc.cdc.VM;
import peersim.core.CommonState;

// TODO - Rename this, FullCapacityMWorkload to FullCapacityVMWorkload
public class FullCapacityMWorkload implements VMWorkloadModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private Map<VM, Double> workloads; 
	
	public FullCapacityMWorkload(String prefix) {
		workloads = new HashMap<VM, Double>();
        LOGGER.debug("[{}]", CommonState.getTime());
        init();
	}
	
	@Override
	public boolean isDynamic() {
		return false;
	}
	
	@Override
	public double getWorkload(VM vm) {
        LOGGER.trace("[{}]~vm.id={} -> {}",
        		CommonState.getTime(), vm.id, workloads.get(vm));
		return workloads.get(vm);
	}
	
	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
        LOGGER.info("[{}]", CommonState.getTime());
		CloudDataCenter.getCloudDataCenter().getVMs().stream().forEach(vm -> {
			workloads.put(vm, (double)vm.spec.getMIPs());
	        LOGGER.debug("[{}]~VM={} workload init->{}", CommonState.getTime(), vm, 
	        		String.format("%.2f", workloads.get(vm)));
		});
        CloudDataCenter.getCloudDataCenter().setVMWorkloadModel(this);
		return false;
	}
		
	public boolean step() {
        LOGGER.trace("[{}]~Doing nothing", CommonState.getTime());
		return false;
	}
}