package ie.nix.dvmc.cdc.observer;

import java.util.DoubleSummaryStatistics;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import peersim.core.CommonState;

public class AverageWorkloadAndRAM extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public DoubleSummaryStatistics cpuUtilSummaryStatistics;
	public double minCPUUtil;
	public double avgCPUUtil;
	public double maxCPUUtil;
	
	public DoubleSummaryStatistics ramUtilSummaryStatistics;
	public double minRAMUtil;
	public double avgRAMUtil;
	public double maxRAMUtil;
	
	public double cpuDemand;
	public double ramDemand;
	
	public AverageWorkloadAndRAM(String prefix) {
		super(prefix);
		writeHeaders(Stream.of("time", 
				"CPU Demand", "Min CPU Util", "Average CPU Util", "Max CPU Util",
				"RAM Demand", "Min RAM Util", "Average RAM Util", "Max RAM Util"));
	}

	public void observe(CloudDataCenter cdc) {
        LOGGER.debug("[{}]", CommonState.getTime());

		cpuUtilSummaryStatistics = 
				cdc.getHosts().stream().
					mapToDouble(host -> host.getCPUUtil()).
					filter(cpuUtil -> cpuUtil != 0.0).
					summaryStatistics();

		cpuDemand = cdc.getHosts().stream().mapToDouble(host -> host.getCPUUsed()).sum();
		
		minCPUUtil = cpuUtilSummaryStatistics.getMin();
		avgCPUUtil = cpuUtilSummaryStatistics.getAverage();
		maxCPUUtil = cpuUtilSummaryStatistics.getMax();
		
		ramUtilSummaryStatistics = 
				cdc.getHosts().stream().
					mapToDouble(host -> host.getRAMUtil()).
					filter(ramUtil -> ramUtil != 0.0).
					summaryStatistics();
		
		ramDemand = cdc.getHosts().stream().mapToDouble(host -> host.getRAMUsed()).sum();
		minRAMUtil = ramUtilSummaryStatistics.getMin();
		avgRAMUtil = ramUtilSummaryStatistics.getAverage();
		maxRAMUtil = ramUtilSummaryStatistics.getMax();
		
		writeRows(Stream.of(CommonState.getTime(),
        		String.format("%.2f", cpuDemand),
        		String.format("%.2f", minCPUUtil),
        		String.format("%.2f", avgCPUUtil),
        		String.format("%.2f", maxCPUUtil),
        		String.format("%.2f", ramDemand),
        		String.format("%.2f", minRAMUtil),
        		String.format("%.2f", avgRAMUtil),
        		String.format("%.2f", maxRAMUtil)));
		
        LOGGER.info("[{}]~CPU[demand={}, min={}%, average={}%, max={}%], "
        		       + "RAM[demand={}, min={}%, average={}%, max={}%]\"", 
        		CommonState.getTime(),
        		String.format("%.0f", cpuDemand*100),
        		String.format("%.0f", minCPUUtil*100),
        		String.format("%.0f", avgCPUUtil*100),
        		String.format("%.0f", maxCPUUtil*100),
        		String.format("%.0f", ramDemand*100),
        		String.format("%.0f", minRAMUtil*100),
        		String.format("%.0f", avgRAMUtil*100),
        		String.format("%.0f", maxRAMUtil*100));

	}

}
