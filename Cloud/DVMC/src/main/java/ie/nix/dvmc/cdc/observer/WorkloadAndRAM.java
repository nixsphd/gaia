package ie.nix.dvmc.cdc.observer;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import peersim.core.CommonState;

public class WorkloadAndRAM extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public WorkloadAndRAM(String prefix) {
		super(prefix);
	}

	public void observe(CloudDataCenter cdc) {
        LOGGER.trace("[{}]", CommonState.getTime());
        
		if (CommonState.getTime() == 0) {
			
			Stream.Builder<String> builder = Stream.builder();
			builder.add(String.valueOf("time"));
			cdc.getHosts().stream().forEachOrdered(host -> {
				builder.add(String.valueOf("Host-"+host.getIndex()+"-CPU"));
			});
			cdc.getHosts().stream().forEachOrdered(host -> {
				builder.add(String.valueOf("Host-"+host.getIndex()+"-RAM"));
			});
			Stream<String> headers = builder.build();
			writeHeaders(headers);
		}
        
        Stream.Builder<String> builder = Stream.builder();
        builder.add(String.valueOf(CommonState.getTime()));
        
		// Add the CPU Data
        cdc.getHosts().stream().forEachOrdered(host -> {
			builder.add(String.valueOf(host.getCPUUtil()));
		});
        // Add the RAM Data
        cdc.getHosts().stream().forEachOrdered(host -> {
			builder.add(String.valueOf(host.getRAMUtil()));
		});
	
        Stream<String> hostsWorkload = builder.build();
        writeRow(hostsWorkload);
		
        LOGGER.info("[{}]~Logging CPU Workloads & RAM", CommonState.getTime());

	}

}
