package ie.nix.dvmc.vman;

import peersim.config.Configuration;
import peersim.core.Control;
import peersim.core.Network;

public class VMAN {
	
	int numberOfVMs = 4;
	
	// M sum of all the VM on servers - sumOf(Hi)
	
	public double fractionServersWithKVMs(int K) {
		return -1;
	}
	
	public double optimalFractionOfEmptyServers(int K) {
		return -1;
		
	}
	
	public static class Observer implements Control {

	    private static final String PAR_PROT = "protocol";

	    private final String name;
	    public String getName() {
			return name;
		}
		private final int pid;

	    public Observer(String name) {
	        this.name = name;
	        pid = Configuration.getPid(name + "." + PAR_PROT);
	    }
	    public boolean execute() {
	    	
	    		int numberOfSleepingHosts = 0;
	    		int totalLoad = 0;
	    		
	    		for (int i = 0; i < Network.size(); i++) {
	            ServerProtocol protocol = (ServerProtocol) Network.get(i).getProtocol(pid);
	            if (protocol.numberOfVMs == 0) {
	            		numberOfSleepingHosts++;
	            } else {
	            		totalLoad += protocol.numberOfVMs;
	            }
	        }
			System.err.println("Observer.execute()~totalLoad="+totalLoad);	
			System.err.println("Observer.execute()~numberOfSleepingHosts="+numberOfSleepingHosts);	
			System.err.println("Observer.execute()~averageLoad="+totalLoad/(Network.size() - numberOfSleepingHosts));	
	        return false;
	    }

	}
	
	public static void main(String[] args) {
		args = new String[] {
			"src/ie/nix/dvmc/vman/PeerSim.properties"
		};
		peersim.Simulator.main(args);
		System.err.println("Done!");	
				
	}
	
}
