package ie.nix.dvmc.cdc.protocols;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;
import peersim.vector.SingleValueHolder;

public class AverageProtocol extends SingleValueHolder implements CDProtocol, EDProtocol {

	/**
	* The type of a message. It contains a value of type double and the
	* sender node of type {@link peersim.core.Node}.
	*/
	public static class AverageMessage {
		
		/** If not null,
		this has to be answered, otherwise this is the answer. */
		final Node sender;
		final double value;
		
		public AverageMessage(Node sender, double value) {
			this.sender = sender;
			this.value = value;
		}

		@Override
		public String toString() {
			return "AverageMessage [sender=" + sender + ", value=" + value + "]";
		}
		
	}
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	public static final String VALUE = "value";
	public static final String DEFAULT_VALUE = "Value";
	public static final int IGNORE_VALUE = 0;
	public static final String REFRESH_STEP = "refresh_step";

	protected String valueName;
	protected Method valueGetter;

	private double nodeValue;
	private int refreshStep;
	private int nextRefreshStep;
	private int swaps;
	
	public AverageProtocol(String prefix) {
		super(prefix);
		swaps = 0;
		valueName = Configuration.getString(prefix + "." + VALUE, DEFAULT_VALUE);
		refreshStep = Configuration.getInt(prefix + "." + REFRESH_STEP, 
				Configuration.getInt(prefix + ".step"));
		nextRefreshStep = 0;
        LOGGER.trace("[{}]~prefix={}, valueName={}", CommonState.getTime(), prefix, valueName);
	}

	@Override
	public AverageProtocol clone() {
		AverageProtocol foo = null;
	    foo = (AverageProtocol)super.clone();
        LOGGER.trace("[{}]", CommonState.getTime());
	    return foo;
	}

	/*
	 * Return the average CPU utilization as calculated by the gossip protocol.
	 * 
	 */
	public double getAverage() {
	    LOGGER.trace("[{}] -> {}, swaps={}", CommonState.getTime(), value, swaps);
		return value;
	}
	
	/**
	 * This is the standard method the define periodic activity.
	 * The frequency of execution of this method is defined by a
	 * {@link peersim.edsim.CDScheduler} component in the configuration.
	 */
	public void nextCycle(Node node, int protocolID) {
	    LOGGER.trace("[{}]~node={}, nextRefreshStep={}", 
	    		CommonState.getTime(), node, nextRefreshStep);
	    
	    if (CommonState.getTime() > nextRefreshStep) {
	    	refreshValue(node);
	    }
	    
		if (nodeValue != IGNORE_VALUE) {
			Linkable neighbourhood = (Linkable)node.getProtocol(FastConfig.getLinkable(protocolID));
			Transport transport = (Transport)node.getProtocol(FastConfig.getTransport(protocolID));
					
			if (neighbourhood.degree() > 0) {
				Node neighbourHost = (Node) neighbourhood.getNeighbor(CommonState.r.nextInt(neighbourhood.degree()));
		
				// Failure handling
				if (!neighbourHost.isUp()) {
					// Do nothing...
					return;
				}
				transport.send(node, neighbourHost, new AverageMessage(node, value), protocolID);
				LOGGER.trace("[{}]~node={}, protocolID={}, send {} to {}", 
						CommonState.getTime(), node, protocolID, value, neighbourHost);
		
			}
		}
		
	}

	/**
		* This is the standard method to define to process incoming messages.
		*/
		public void processEvent(Node node, int protocolID, Object event) {
		    LOGGER.trace("[{}]~node={}, protocolID={}, event={}", CommonState.getTime(), node, protocolID, event);
		    
			if (nodeValue != IGNORE_VALUE) {
				AverageMessage message = (AverageMessage)event;
				
				// If there is a sender then they want a reply, so send them back the value, before it's updated.
				if (message.sender != null) {
					Transport transport = (Transport)node.getProtocol(FastConfig.getTransport(protocolID));
					transport.send(node, message.sender, new AverageMessage(null, value), protocolID);
					LOGGER.trace("[{}]~node={}, send {} to {}", 
							CommonState.getTime(), node, value, message.sender);
				}
			
				if (value != message.value) {
					double initialValue = value;
					value = (value + message.value)/2;
					swaps++;
					LOGGER.debug("[{}]~node={}, value {} -> {}, swaps={}", 
							CommonState.getTime(), node, initialValue, value, swaps);
				}
			}
			
		}

	protected void refreshValue(Node node) {
		LOGGER.trace("[{}]~node={}, nodeValue {}", 
				CommonState.getTime(), node, nodeValue);
		try {
			double newValue = (double)getValueGetter().invoke(node);
			swaps = 0;
			if (nodeValue != newValue) {
				LOGGER.debug("[{}]~node={}, newValue {} -> {}, swaps={}", 
						CommonState.getTime(), node, nodeValue, newValue, swaps);
			} else  {
				LOGGER.debug("[{}]~node={}, newValue {} -> {}, swaps={}", 
						CommonState.getTime(), node, nodeValue, newValue, swaps);
			}
			nodeValue = newValue;
			value = nodeValue;
			nextRefreshStep += refreshStep;
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			LOGGER.error("[{}]~Exception {}", CommonState.getTime(), e);
			e.printStackTrace();
		}
		
	}

	protected Method getValueGetter() {
		if (valueGetter == null) {
			try {
				valueGetter = Network.prototype.getClass().getMethod("get"+valueName);
			} catch (NoSuchMethodException e) {
		        LOGGER.error("[{}]~valueName={} Can not find method {}", 
		        		CommonState.getTime(), valueName, 
		        		Network.prototype.getClass().getName()+".get"+valueName+"()");
				e.printStackTrace();
			} catch (SecurityException e) {
				LOGGER.error("[{}]~valueName={} Exception {}", 
	        		CommonState.getTime(), valueName, e);
				e.printStackTrace();
			}
		} 
		return valueGetter;
	}
}
