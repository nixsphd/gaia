package ie.nix.dvmc.cdc.protocols;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.edsim.EDSimulator;

public class CallbackProtocol implements EDProtocol {

	private static final Logger LOGGER = LogManager.getLogger();

	protected static final String CALLBACK_PROTOCOL = "callback";
	
	public static void addCallback(int delay, Callback callback) {
		EDSimulator.add(delay, callback, Network.get(0), 
				Configuration.lookupPid(CALLBACK_PROTOCOL));
	}
	
	public static class Callback {
		final Runnable action;

		public Callback(Runnable action) {
			this.action = action;
	        LOGGER.debug("[{}]~action={}", CommonState.getTime(), action);
		}

	}

	public CallbackProtocol(String prefix) {
        LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);
	}

	@Override
	public CallbackProtocol clone() {
		CallbackProtocol foo = null;
		try {
			foo = (CallbackProtocol) super.clone();
	        LOGGER.trace("[{}]", CommonState.getTime());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return foo;
	}

	public void processEvent(Node node, int protocolID, Object event) {
        LOGGER.trace("[{}]~node={}, protocolID={}, event={}", CommonState.getTime(), node, protocolID, event);
		if (event instanceof Callback) {
			handleCallback(event);

		}
	}

	public void handleCallback(Object event) {
        LOGGER.trace("[{}]~event={}", CommonState.getTime(), event);
		Callback callback = (Callback) event;
		callback.action.run();
	}

}