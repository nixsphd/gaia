package ie.nix.dvmc.cdc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;
import peersim.edsim.EDProtocol;

/*
 * In order to evaluate the performance of our proposed approach, we carried out
 * simulation runs using our cloud data center simulator that we have developed
 * in-house over PeerSim simulator tool. PeerSim is a peer-to-peer network
 * simulator written in Java, allowing the simulation of systems with varying
 * cardinality.
 */
public class CloudDataCenter implements Control {

	/*
	 * Hosts
	 */
	public interface HostFactory {

		boolean init();
		
		Host newHost();
	}

	public interface HostLifecycleModel extends Control {

		boolean init();
	}
	
	public interface HostPowerModel extends Control {

		boolean init();

		double powerConsumption(Host host, double utilization);
	}

	public interface HostSleepModel extends EDProtocol {
		
		public boolean isAwake(Host host);

		public void sleep(Host host);

		public void wakeUp(Host host);
		
		public Optional<Host> findSleepingHost(Host host);
	}

	/*
	 * VMs
	 */
	public interface VMFactory {

		boolean init();
		
		VM newVM();
	}

	public interface VMLifecycleModel extends Control {

		boolean init();
	}
	
	public interface VMWorkloadModel extends Control {

		boolean init();
		
		boolean isDynamic();
		
		double getWorkload(VM vm);
		
	}

	/*
	 * VM Provisioning and migration
	 */
	public interface VMProvisionModel extends Control {

		boolean init();
		
		public void provisionVM(VM vm);
	}

	public interface VMMigrationModel extends EDProtocol {

		public void migrateVM(Host fromHost, VM vm, Host toHost);
		
		public void migrateVMs(Host host, Map<VM, Host> vmMigrationMap);

		public int getNumberOfMigrations();
	}
	
	private static final Logger LOGGER = LogManager.getLogger();

	private static final String HOST_FACTORY = "host_factory";
	private static final String NUMBER_OF_HOSTS = "number_of_hosts";

	private static final String HOST_POWER = "host_power";
	
	private static final String VM_FACTORY = "vm_factory";
	private static final String NUMBER_OF_VMS = "number_of_vms";

	private static final String VM_WORKLOAD_MODEL = "vm_workload";
	
	private static final String VM_PROVISION = "vm_provision";

	private static CloudDataCenter cloudDataCenter;

	public static CloudDataCenter getCloudDataCenter() {
		return cloudDataCenter;
	}

	private HostFactory hostFactory;
	private HostLifecycleModel hostLifecycleModel;
	private HostPowerModel hostPowerModel;
	private VMFactory vmFactory;
	private VMLifecycleModel vmLifecycleModel;
	private VMProvisionModel vmProvisioner;
	private VMWorkloadModel vmWorkloadModel;
	
	private final int numberOfHosts;
	private final int numberOfVMs;	
	private final List<Host> hosts;
	private final List<VM> vms;


	public CloudDataCenter(String prefix) {
		LOGGER.info("[{}]", CommonState.getTime());

		cloudDataCenter = this;
		
		hosts = new ArrayList<Host>();
		vms = new ArrayList<VM>();

		hostFactory = (HostFactory)Configuration.getInstance(prefix + "." + HOST_FACTORY);
		numberOfHosts = Configuration.getInt(prefix + "." + NUMBER_OF_HOSTS);
		
		hostPowerModel = (HostPowerModel)Configuration.getInstance(prefix + "." + HOST_POWER);

		vmFactory = (VMFactory)Configuration.getInstance(prefix + "." + VM_FACTORY);
		numberOfVMs = Configuration.getInt(prefix + "." + NUMBER_OF_VMS);
		
		vmWorkloadModel = (VMWorkloadModel)Configuration.getInstance(prefix + "." + VM_WORKLOAD_MODEL, null);
		
		vmProvisioner = (VMProvisionModel)Configuration.getInstance(prefix + "." + VM_PROVISION);
		
		hosts.clear();
		vms.clear();
		
		LOGGER.info("[{}]~Comissioning {} hosts", CommonState.getTime(), numberOfHosts);
		for (int h = 0; h < numberOfHosts; h++) {
			comissionHost();
		}
		
		LOGGER.info("[{}]~Creating {} vms", CommonState.getTime(), numberOfVMs);
		for (int v = 0; v < numberOfVMs; v++) {
			createVM();
			
		}
		
		initVMWorkloads();
		
		provisionVMs();
		
		double dcCPUUtil = vms.stream().mapToDouble(vm -> vm.getWorkload()).sum() / 
				hosts.stream().mapToDouble(host -> host.spec.getMaxWorkload()).sum();
		double dcRAMUtil = vms.stream().mapToDouble(vm -> vm.getRAM()).sum() / 
				hosts.stream().mapToDouble(host -> host.spec.getMemory()).sum();
		long unassignedVMs = vms.stream().filter(vm -> vm.getHost() == null).count();
		long hostsAsleep = hosts.stream().filter(host -> !host.isAwake()).count();
		LOGGER.trace("[{}]~DataCentre Info: CPU Util={}, RAM Util={}, unassignedVMs={}, hostsAsleep={}", 
				CommonState.getTime(),  String.format("%.2f", dcCPUUtil),
				String.format("%.2f", dcRAMUtil), unassignedVMs, hostsAsleep);
		
	}

	public void initVMWorkloads() {
		if (vmWorkloadModel != null) {
			vmWorkloadModel.init();
		}
	}

	public void provisionVMs() {
		LOGGER.info("[{}]~Provisioning {} vms", CommonState.getTime(), numberOfVMs);
		vms.stream().filter(vm -> vm.getHost() != null).forEach(vm -> vm.getHost().removeVM(vm));
		vms.stream().forEachOrdered(vm -> provisionVM(vm));
		long numberOfHostNeeded = hosts.stream().filter(host -> host.hasVMs()).count();
		LOGGER.info("[{}]~Provisioning {} vms on {} hosts", CommonState.getTime(), numberOfVMs, numberOfHostNeeded);
		
	}

	public boolean execute() {
        LOGGER.trace("[{}]", CommonState.getTime());
		return false;
	}

	public HostFactory getHostFactory() {
		return hostFactory;
	}

	public Host comissionHost() {
		Host host = hostFactory.newHost();
		Network.add(host);
		hosts.add(host);
	    LOGGER.debug("[{}]~{}", CommonState.getTime(), host);
		return host;
	}

	public void decomissionHost(Host host) {
	    LOGGER.debug("[{}]~{}", CommonState.getTime(), host);
		Network.remove(host.getIndex());
		hosts.remove(host);
	
	}

	public List<Host> getHosts() {
		return hosts;
	}

	public HostLifecycleModel getHostLifecycleModel() {
		return hostLifecycleModel;
	}

	public void setHostLifecycleModel(HostLifecycleModel hostLifecycleModel) {
	    LOGGER.info("[{}]~{}", CommonState.getTime(), hostLifecycleModel.getClass().getSimpleName());
		this.hostLifecycleModel = hostLifecycleModel;
	}
	
	/*
	 * Host Power Model APIs
	 */
	public HostPowerModel getHostPowerModel() {
		return hostPowerModel;
	}
	
	/*
	 * VM Factory APIs
	 */
	public VMFactory getVmFactory() {
		return vmFactory;
	}

	public VM createVM() {
		VM vm = vmFactory.newVM();
		vms.add(vm);
	    LOGGER.debug("[{}]~{}", CommonState.getTime(), vm);
		return vm;
	}
	
	/*
	 * VM Provision APIs
	 */
	public void provisionVM(VM vm) {
	    LOGGER.debug("[{}]~{}", CommonState.getTime(), vm);
	    getVMProvisionModel().provisionVM(vm);
	}

	public VMProvisionModel getVMProvisionModel() {
		return vmProvisioner;
	}

	public void terminateVM(VM vm) {
	    LOGGER.debug("[{}]~{}", CommonState.getTime(), vm);
		vms.remove(vm);
		if (vm.getHost() != null) {
			vm.getHost().removeVM(vm);
		}
	}

	public int getNumberOfVMs() {
		return vms.size();
	}
	
	public List<VM> getVMs() {
		return vms;
	}

	public VMLifecycleModel getVmLifecycleModel() {
		return vmLifecycleModel;
	}

	public void setVmLifecycleModel(VMLifecycleModel vmLifecycleModel) {
	    LOGGER.trace("[{}]~{}", CommonState.getTime(), vmLifecycleModel.getClass().getSimpleName());
		this.vmLifecycleModel = vmLifecycleModel;
	}

	// TODO - Fix typo, getVMorkloadModel() to getVMWorkloadModel().
	public VMWorkloadModel getVMorkloadModel() {
		return vmWorkloadModel;
	}

	public void setVMWorkloadModel(VMWorkloadModel vmWorkloadModel) {
	    LOGGER.info("[{}]~{}", CommonState.getTime(), vmWorkloadModel.getClass().getSimpleName());
		this.vmWorkloadModel = vmWorkloadModel;
	}

}
