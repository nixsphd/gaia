package ie.nix.dvmc.cdc.model;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.HostLifecycleModel;
import ie.nix.dvmc.cdc.Host;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class SimpleHostLifecycleModel implements HostLifecycleModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
//	private static final String NUMBER_OF_HOSTS = "number_of_hosts";
	private static final String STEP = "step";
	private static final int SECONDS_PER_DAY = 60 * 60 * 24; // 86,400 secs

//	private final int numberOfHosts;
	private final double step;

	public SimpleHostLifecycleModel(String prefix) {
//		numberOfHosts = Configuration.getInt(prefix + "." + NUMBER_OF_HOSTS);
		step = Configuration.getInt(prefix + "." + STEP);
        LOGGER.debug("[{}]", CommonState.getTime());
		CloudDataCenter.getCloudDataCenter().setHostLifecycleModel(this);
	}

	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	@Override
	public boolean init() {
//        LOGGER.info("[{}]~Comissioning {} hosts", CommonState.getTime(), numberOfHosts);
//		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
//		for (int h = 0; h < numberOfHosts; h++) {
//			cdc.comissionHost();
//		}
		return false;
	}

	public boolean step() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		// First find the Host to be decomissioned
		List<Host> toFail = cdc.getHosts().stream().filter(host -> host.getFailureRatePerDay() != 0)
				.filter(host -> CommonState.r.nextDouble() <= ((step / SECONDS_PER_DAY) * host.getFailureRatePerDay()))
				.collect(Collectors.toList());
		// The terminate them, need to be done in two steps to avoid concurrent issues
		// with List.
		toFail.forEach(host -> cdc.decomissionHost(host));
		// host.setFailState(Fallible.DEAD);
		return false;
	}

}