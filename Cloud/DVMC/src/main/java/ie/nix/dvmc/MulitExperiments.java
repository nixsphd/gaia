package ie.nix.dvmc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class MulitExperiments {
	
	private static final Logger LOGGER = LogManager.getLogger();

	/*
	 * Runs the simulation by calling PeerSim with the correct properties file.
	 */
	public static void main(String[] args) {
        LOGGER.trace("[0] args={}", args.toString());
		peersim.rangesim.RangeSimulator.main(args);
        LOGGER.info("[{}] Done!", CommonState.getTime());
	}

}
