package ie.nix.dvmc.cdc.model;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter.VMFactory;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.cdc.VM.VMSpec;
import peersim.core.CommonState;

public class RandomVMFactory extends CSVFileControl implements VMFactory {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private final VMSpec[] vmSpecs;
	
	public RandomVMFactory(String prefix) {
		this(prefix, "Name", "MIPS", "RAM (GB)");
	}
	
	public RandomVMFactory(String prefix, String vmSpecNameHeader, String cpuHeader, String ramHeader) {
		super(prefix);
		
		this.vmSpecs = new VMSpec[df.getNumberOfRows()];
		for (int row = 0; row < df.getNumberOfRows(); row++) {
			//"Name", "MIPS", "RAM"
			this.vmSpecs[row] = new VMSpec(
					removeQuotes((String)df.getData(row, vmSpecNameHeader)), 
					Integer.valueOf(df.getData(row, cpuHeader).toString()),
					Double.valueOf(df.getData(row, ramHeader).toString())
				);
		}
        LOGGER.debug("[{}]", CommonState.getTime());
		
	}

	public VM newVM() {
		return new VM(vmSpecs[CommonState.r.nextInt(vmSpecs.length)]);
	}

	public Stream<VMSpec> getVMSpecs() {
		return Stream.of(vmSpecs);
	}

	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	@Override
	public boolean init() {
		LOGGER.debug("[{}]", CommonState.getTime());
		return false;
	}

	public boolean step() { return false; }
	
}