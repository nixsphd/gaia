package ie.nix.dvmc.cdc.model;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMProvisionModel;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import peersim.core.CommonState;

public class FirstFitCanFit implements VMProvisionModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public FirstFitCanFit(String prefix) {
        LOGGER.trace("[{}]", CommonState.getTime());
		
	}

	@Override
	public boolean execute() {
	    LOGGER.trace("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	@Override
	public boolean init() {
		LOGGER.trace("[{}]", CommonState.getTime());
		return false;
	}

	public boolean step() { return false; }

	@Override
	public void provisionVM(VM vm) {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<Host> hosts = cdc.getHosts();
		for (int h = 0; h < hosts.size(); h++) {
			Host host = (Host) hosts.get(h);
			if (host.canFitVM(vm)) {
				host.addVM(vm);
				LOGGER.debug("[{}]~provisioning vm {} on host {}", 
						CommonState.getTime(), vm, host);
				return;
			}
		}
		LOGGER.debug("[{}]~can't provisioning vm {}", 
				CommonState.getTime(), vm);
	
	}
	
}