package ie.nix.dvmc.ecocloud;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMProvisionModel;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.cdc.protocols.CallbackProtocol;
import ie.nix.dvmc.cdc.protocols.CallbackProtocol.Callback;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.transport.Transport;

public class EcoCloudDVMC implements CDProtocol, EDProtocol {

	public static class EcoCloudProvisionModel implements VMProvisionModel {

		public EcoCloudProvisionModel(String prefix) {
	        LOGGER.trace("[{}]", CommonState.getTime());
		}
		
		@Override
		public void provisionVM(VM vm) {
			LOGGER.trace("[{}]~vm={}", CommonState.getTime(), vm);
			Manager manager = EcoCloudDVMC.getManager();
			manager.assignVM(vm);
		}

		@Override
		public boolean init() {
			return false;
		}
		
		@Override
		public boolean execute() {
			return false;
		}
		
	}
	
	/**
	 * The type of a message. It contains a value of type double and the sender node
	 * of type {@link peersim.core.Node}.
	 */
	public static class AssignMessage {

		/** If not null, this has to be answered, otherwise this is the answer. */
		final Node sender;

		private VM vm;
		
		final Optional<Boolean> accept;

		public AssignMessage(Node sender, VM vm) {
			this(sender, vm, Optional.empty());
		}
		
		public AssignMessage(Node sender, VM vm, boolean accept) {
			this(sender, vm, Optional.of(accept));
		}
		
		public AssignMessage(Node sender, VM vm, Optional<Boolean> accept) {
			this.sender = sender;
			this.vm = vm;
			this.accept = accept;
	        LOGGER.trace("[{}]~sender={}, vm={}, accept={}", 
	        		CommonState.getTime(), sender, vm, accept);
		}

		public boolean isAssignRequest() {
			return !accept.isPresent();
		}
		
		public boolean isAssignResponse() {
			return accept.isPresent();
		}
		
		@Override
		public String toString() {
			return "AssignMessage [sender=" + sender + ", vm=" + vm + ", accept=" + accept + "]";
		}

	}
	
	public class Manager {

		protected final Host managerHost;
		protected Map<VM, List<Host>> canAssignMap;
		protected int assignDelay = 1;
		
		public Manager(Host managerHost) {
	        LOGGER.trace("[{}]", CommonState.getTime());
	        this.managerHost = managerHost;
	        this.canAssignMap = new HashMap<VM, List<Host>>();
		}

		public void assignVM(VM vm) {
			assignVM(vm, true);
		}
		
		public void assignVM(VM vm, boolean allowWakeUp) {
	        LOGGER.trace("[{}]~vm={}, allowWakeUp={}", 
	        		CommonState.getTime(), vm, allowWakeUp);
	        
	        // Create the list to store the hosts which can be assigned.
	        canAssignMap.put(vm, new ArrayList<Host>());
	        
			// Sends an invitation to all the active servers, or to a 
	        // subset of them, depending on the data center size and 
	        // architecture, to check if they are available to accept 
	        // the new VM.
	        List<Host> hosts = CloudDataCenter.getCloudDataCenter().getHosts();
	        int numberOfRequestsToSend = Math.min(hosts.size()-1, max_assign_requests);
	        int requestsSent = 0;

	        while (requestsSent < numberOfRequestsToSend) {
	        	Host host = hosts.get(CommonState.r.nextInt(hosts.size()));
	        	if (host.isAwake() && (vm.getHost() == null || (vm.getHost() != host))) {
	        		sendAssignRequest(vm, host);
	        		requestsSent++;
	        	}
			}
	        if (assignDelay == 0) {
				assignCallback(vm, allowWakeUp);
				
	        } else {
	        	CallbackProtocol.addCallback(assignDelay, new Callback(() -> {
					assignCallback(vm, allowWakeUp);
					
				}));
	        }
		}

		protected void assignCallback(VM vm, boolean allowWakeUp) {
			List<Host> canAssignHosts = canAssignMap.get(vm);
			LOGGER.debug("[{}]~vm={}, allowWakeUp={}, canAssignHosts={}", 
					CommonState.getTime(), vm, allowWakeUp, canAssignHosts);
			
			if (canAssignHosts.size() > 0) {
				Host assignToHost = canAssignHosts.get(CommonState.r.nextInt(canAssignHosts.size()));
				LOGGER.trace("[{}]~Picked random host {}", CommonState.getTime(), assignToHost);
				migrateVM(assignToHost, vm);
				
			} else if (allowWakeUp) {
				for (Host host : CloudDataCenter.getCloudDataCenter().getHosts()) {
			    	if (!host.isAwake()) {
						LOGGER.debug("[{}]~Waking up host {}", CommonState.getTime(), host);
						host.wakeUp();
			        	migrateVM(host, vm);
			        	break;
			    	}
			    }
			}
		}

		public void sendAssignRequest(VM vm, Host host) {
			Transport transport = 
					(Transport)managerHost.getProtocol(FastConfig.getTransport(protocolID));
			AssignMessage assignMessage = new AssignMessage(managerHost, vm);
			transport.send(managerHost, host, assignMessage, protocolID);
			LOGGER.trace("[{}]~Send {}", CommonState.getTime(), assignMessage);
		}

		public void handleAssignResponse(AssignMessage assignMessage) {
			VM vm = assignMessage.vm;
			Host host = (Host)assignMessage.sender;
			if (assignMessage.accept.get()) {
				
				LOGGER.trace("[{}]~Can assign vm {} to host {}", 
						CommonState.getTime(), assignMessage.vm, assignMessage.sender);
	        	List<Host> canAssignHosts = canAssignMap.get(vm);
	        	canAssignHosts.add(host);
	        	
			} else {
				
				LOGGER.trace("[{}]~Can't assign vm {} to host {}", 
						CommonState.getTime(), assignMessage.vm, assignMessage.sender);
				
			}
		}
	
		public void migrateVM(Host host, VM vm) {
			Host hostToMigrateFrom = vm.getHost();
			if (hostToMigrateFrom != null) {
				hostToMigrateFrom.migrateVM(vm, host);
				LOGGER.debug("[{}]~Migrated vm {} from {} to {}", 
						CommonState.getTime(), vm, hostToMigrateFrom, host);
				if (!hostToMigrateFrom.hasVMs()) {
					hostToMigrateFrom.sleep();
					LOGGER.trace("[{}]~Going to sleep, host={}", CommonState.getTime(), hostToMigrateFrom);
				}
			} else {
				host.addVM(vm);
				LOGGER.debug("[{}]~Assigned vm {} to {}", 
						CommonState.getTime(), vm, host);
			}
			canAssignMap.remove(vm);
			LOGGER.trace("[{}]~Removed vm {} from canAssignMap", CommonState.getTime(), vm);
		}
	}
	
	private static final Logger LOGGER = LogManager.getLogger();

	protected static final int DEFAULT_NUMBER_OF_ASSIGN_REQUESTS = 100;
	
	protected static final String ASSIGNMENT_THRESHOLD = "assignment_threshold";
	protected static final String ASSIGNMENT_SHAPE = "assignment_shape";
	protected static final String MIGRATION_HIGH_THRESHOLD = "migration_high_threshold";
	protected static final String MIGRATION_LOW_THRESHOLD = "migration_low_threshold";
	protected static final String MIGRATION_APLHA = "migration_aplha";
	protected static final String MIGRATION_BETA = "migration_beta";
	protected static final String NUMBER_OF_ASSIGN_REQUESTS = "max_assign_requests";
	
	protected static int protocolID = Configuration.lookupPid("dvmc_strategy");
	
	protected static Manager manager;
    
	public static Manager getManager() {
		if (manager == null) {
			Host managerHost = (Host)Network.get(0);
			EcoCloudDVMC managerEcoCloudDVMC = ((EcoCloudDVMC)managerHost.getProtocol(protocolID));
			manager = managerEcoCloudDVMC.new Manager(managerHost);
		}
		LOGGER.trace("[{}]~manager={}", CommonState.getTime(), manager);
		return manager;
	}

	protected static boolean bernoulliTrial(double successProbability) {
		boolean success = CommonState.r.nextDouble() <= successProbability;
		LOGGER.trace("[{}]~success={}", CommonState.getTime(), success);
		return success;
	}

	protected int callbackProtocolID;
	protected double assignmentThreshold;
	protected double shape;
	protected double migrationLowThreshold;
	protected double alpha;
	protected double migrationHighThreshold;
	protected double beta;
	protected int max_assign_requests;
	
	public EcoCloudDVMC(String prefix) {
		LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);
		this.assignmentThreshold = Configuration.getDouble(prefix + "." + ASSIGNMENT_THRESHOLD);
		this.shape = Configuration.getDouble(prefix + "." + ASSIGNMENT_SHAPE);
		this.migrationHighThreshold = Configuration.getDouble(prefix + "." + MIGRATION_HIGH_THRESHOLD);
		this.migrationLowThreshold = Configuration.getDouble(prefix + "." + MIGRATION_LOW_THRESHOLD);
		this.alpha = Configuration.getDouble(prefix + "." + MIGRATION_APLHA);
		this.beta = Configuration.getDouble(prefix + "." + MIGRATION_BETA);
		this.beta = Configuration.getDouble(prefix + "." + MIGRATION_BETA);
		this.max_assign_requests = Configuration.getInt(prefix + "." + NUMBER_OF_ASSIGN_REQUESTS, DEFAULT_NUMBER_OF_ASSIGN_REQUESTS);
	}
	
	@Override
	public EcoCloudDVMC clone() {
		EcoCloudDVMC foo = null;
		try {
			foo = (EcoCloudDVMC)super.clone();
		} catch (CloneNotSupportedException e) {}
		LOGGER.trace("[{}].clone()", CommonState.getTime());
		return foo;
	}

	@Override
	public void nextCycle(Node node, int protocolID) {
		LOGGER.trace("[{}]~node={}", CommonState.getTime(),node.getID());
		Host host = (Host)node;
		if (!host.hasVMs()) {
			LOGGER.trace("[{}]~Going to sleep, host={}", CommonState.getTime(), host);
			host.sleep();
			
		} else if (amIOverloaded(host)) {
			LOGGER.debug("[{}]~Overloaded, host={}", CommonState.getTime(), host);
			
			double successProbability = (host.getCPUUtil() > host.getRAMUtil()) ?
					getHighMigrationSuccessProbability(host.getCPUUtil()) :
					getHighMigrationSuccessProbability(host.getRAMUtil());
			LOGGER.trace("[{}]~High migration, host={},  successProbability={}", 
					CommonState.getTime(), host, successProbability);
				
			if (bernoulliTrial(successProbability)) {
				// select a vm to migrate but randomly picking a vm from the list
				// of vm now usage is higher than the difference between the overloaded
				// resource and the threshold.
				double cpuOverloading = (host.getCPUUtil() > migrationHighThreshold ? 
						host.getCPUUtil() - migrationHighThreshold: 0);
				double ramOverloading = (host.getRAMUtil() > migrationHighThreshold ? 
						host.getRAMUtil() - migrationHighThreshold: 0);
				LOGGER.trace("[{}]~cpuOverloadingy={}, ramOverloading={}", 
						CommonState.getTime(), cpuOverloading, ramOverloading);
				Optional<VM> vmToMigrate = host.vmStream().filter(vm -> 
					(vm.getWorkload() > cpuOverloading && vm.getRAM() > ramOverloading)).findAny();
				LOGGER.debug("[{}]~vmToMigrate={}", CommonState.getTime(), vmToMigrate);
				
				vmToMigrate.ifPresent(vm -> {
					LOGGER.trace("[{}]~migrate VM={}", CommonState.getTime(), vm);
					getManager().assignVM(vm);
				});
			}
		
		} else if (amIUnderloaded(host)) {
			LOGGER.debug("[{}]~Underloaded, host={}", CommonState.getTime(), host);
			double successProbability = (host.getCPUUtil() > host.getRAMUtil()) ?
					getLowMigrationSuccessProbability(host.getCPUUtil()) :
					getLowMigrationSuccessProbability(host.getRAMUtil());
			LOGGER.trace("[{}]~Low migration, host={}, successProbability={}", 
					CommonState.getTime(), host, successProbability);

			if (bernoulliTrial(successProbability)) {
				List<VM> vms = host.vmList();
				VM vm = vms.get(CommonState.r.nextInt(vms.size()));
				LOGGER.debug("[{}]~migrate VM={}", CommonState.getTime(), vm);
				getManager().assignVM(vm, false);
			}
		}
	}

	protected boolean amIOverloaded(Host host) {
		// If either resource is over utilized.
		boolean overloaded = (host.getCPUUtil() > migrationHighThreshold) ||
				(host.getRAMUtil() > migrationHighThreshold);
		LOGGER.trace("[{}]~host={}, overloaded={}", 
				CommonState.getTime(), host, overloaded);
		return overloaded;
	}

	protected boolean amIUnderloaded(Host host) {
		// Only check the most utilized resource is underutilized.
		boolean underloaded = 
				(host.isAwake()) &&
				(host.getCPUUtil() < migrationLowThreshold) &&
				(host.getRAMUtil() < migrationLowThreshold);
		LOGGER.trace("[{}]~host={}, underloaded={}", 
				CommonState.getTime(), host, underloaded);
		return underloaded;
	}

	protected double getHighMigrationSuccessProbability(double util) {
		// (1 + ((util - 1)/(1 - Thigh)))^b
		double successProbability =  Math.pow(1 + ((util - 1)/(1 - migrationHighThreshold)), beta);
		LOGGER.trace("[{}]~successProbability={}", CommonState.getTime(), successProbability);
		return successProbability;
	}
	
	protected double getLowMigrationSuccessProbability(double util) {
		// (1 - x/Tl)^a
		double successProbability = Math.pow(1 - (util/migrationLowThreshold), alpha);
		LOGGER.trace("[{}]~successProbability={}", CommonState.getTime(), successProbability);
		return successProbability;
	}

	@Override
	public void processEvent(Node node, int protocolID, Object event) {
		LOGGER.trace("[{}]~node={}, event={}", CommonState.getTime(),node.getID(), event);
		Host host = (Host)node;
		if (event instanceof AssignMessage) {
			AssignMessage assignMessage = (AssignMessage)event;
			handleAssignMessage(host, assignMessage);
		}
	}

	protected void handleAssignMessage(Host host, AssignMessage assignMessage) {
		if (assignMessage.isAssignRequest()) {
			handleAssignRequest(host, assignMessage);
			
		} else {
			handleAssignResponse(host, assignMessage);
			
		}
	}

	protected void handleAssignRequest(Host host, AssignMessage assignRequest) {
		if (amIUnderThreshold(host)) {
			AssignMessage assignResponse = 
					new AssignMessage(host, assignRequest.vm, bernoulliTrial(getAssignSuccessProbability(host)));
			sendAssignMessage(host, assignResponse);
		}
	}

	protected boolean amIUnderThreshold(Host host) {
		return host.getCPUUtil() < assignmentThreshold && host.getRAMUtil() < assignmentThreshold;
	}

	protected double getAssignSuccessProbability(Host host) {
		double successProbability = getSuccessProbability(host.getRAMUtil())*
				getSuccessProbability(host.getCPUUtil());
		LOGGER.trace("[{}]~assignSuccessProbability={}", CommonState.getTime(), successProbability);
		return successProbability;
	}

	protected double getSuccessProbability(double util) {
		double successProbability = 0;
		
		if (util < assignmentThreshold) {
			double Mp = (Math.pow(shape, shape)/Math.pow(shape+1, shape+1))
					*Math.pow(assignmentThreshold, shape+1);
			successProbability = (1/Mp)*Math.pow(util, shape)*(assignmentThreshold-util);
		}
		LOGGER.trace("[{}]~assignment Success Probability={}", CommonState.getTime(), successProbability);
		return successProbability;
	}

	protected void sendAssignMessage(Host host, AssignMessage assignMessage) {
		Transport transport = 
				(Transport)host.getProtocol(FastConfig.getTransport(protocolID));
		transport.send(host, assignMessage.sender, assignMessage, protocolID);
		LOGGER.debug("[{}]~Send {}", CommonState.getTime(), assignMessage);
	}
	
	protected void handleAssignResponse(Host host, AssignMessage assignResponse) {
		// Only the Manager should recieve a response, so delegate the handeling 
		// to the Manager inner class
		getManager().handleAssignResponse(assignResponse);
		
	}
}
