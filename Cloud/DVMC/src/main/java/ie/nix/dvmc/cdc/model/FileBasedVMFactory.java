package ie.nix.dvmc.cdc.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.cdc.VM.VMSpec;
import ie.nix.util.DataFrame;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class FileBasedVMFactory extends RandomVMFactory {
	
	private static final Logger LOGGER = LogManager.getLogger();
		
	public static final String CSV_FILE = "vms-file";

	protected Map<Integer,VM> vms;
	protected int nextVM;
	
	public FileBasedVMFactory(String prefix) {
		this(prefix, "Name", "MIPS", "RAM (GB)", "VM", "VM Spec");
	}

	public FileBasedVMFactory(String prefix, 
			String vmSpecNameHeader, String cpuHeader, String ramHeader, 
			String vmHeader, String vmSpecHeader) {
		super(prefix, vmSpecNameHeader, cpuHeader, ramHeader);
        
		String vmsCSVFile = Configuration.getString(prefix + "." + CSV_FILE, getClass().getSimpleName() + ".csv");
		LOGGER.info("[{}]~Reading from {}", CommonState.getTime(), vmsCSVFile);
		df = new DataFrame();
		df.read(vmsCSVFile);
		
		this.vms = new HashMap<Integer,VM>();
		for (int row = 0; row < df.getNumberOfRows(); row++) {
			// "VM", "VM Spec"
			String vmSpecName = removeQuotes((String)df.getData(row, vmSpecHeader));
			int index = Integer.valueOf(df.getData(row, vmHeader).toString());
	        VMSpec vmSpec = this.getVMSpecs().filter(spec -> spec.getName().equals(vmSpecName)).findFirst().get();
			VM vm = new VM(vmSpec);
			vms.put(index, vm);
	        LOGGER.trace("[{}]~Adding vm={} at index={}", CommonState.getTime(), vm, index);
		}
		nextVM = 0;
        LOGGER.debug("[{}]", CommonState.getTime());
	}
	
	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
        LOGGER.debug("[{}]", CommonState.getTime());
		return false;
	}

	public boolean step() { return false; }

	public VM newVM() {
		VM vm = vms.get(nextVM);
		nextVM++;
		return vm;
	}
	
}
