package ie.nix.dvmc.cdc.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.util.DataFrame;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;

public abstract class CSVFileControl implements Control {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static final String CSV_FILE = "csv-file";

	protected DataFrame df;

	public CSVFileControl(String prefix) {
		String csvFile = Configuration.getString(prefix + "." + CSV_FILE, getClass().getSimpleName() + ".csv");
		LOGGER.info("[{}]~Reading from {}", CommonState.getTime(), csvFile);
		df = new DataFrame();
		df.read(csvFile);
	}

	protected String removeQuotes(String data) {
		return data.replace("\"", "");
	}
	
}