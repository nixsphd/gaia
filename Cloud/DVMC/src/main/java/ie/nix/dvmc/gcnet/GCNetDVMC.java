package ie.nix.dvmc.gcnet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.cnet.ContractNETProtocol.AwardMessage;
import ie.nix.cnet.ContractNETProtocol.BidMessage;
import ie.nix.cnet.ContractNETProtocol.GossipTaskAnnounceMessage;
import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.cdc.observer.DVMCObserver;
import ie.nix.dvmc.cdc.protocols.CallbackProtocol.Callback;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;
import peersim.edsim.EDSimulator;

public class GCNetDVMC implements CDProtocol {

	public static class Observer extends DVMCObserver {

		private static final String PROTOCOL = "protocol";
		
		// Protocol that can consolidate the VM
		private int protocolID;
		
		public Observer(String prefix) {
			super(prefix);
			
			this.protocolID = Configuration.getPid(prefix+"."+PROTOCOL);
	        LOGGER.trace("[{}]~protocolID={}", CommonState.getTime(), protocolID);
			
			writeHeaders(Stream.of("time", "tenderingHosts", "biddingHosts", "beingAwardedHosts"));
		}

		public void observe(CloudDataCenter cdc) {
	        LOGGER.trace("[{}]", CommonState.getTime());

	        List<Host> hosts = cdc.getHosts();
	        boolean dynamicWorkloads = cdc.getVMorkloadModel().isDynamic();
	        
	        Set<Host> asleepHosts = getAsleepHosts(hosts);
	        Set<Host> balancedHosts = getBalancedHosts(hosts);  
	        Set<Host> overloadedHosts = getOverloadedHosts(hosts); 
	        Set<Host> violatingHosts = getViolatingHosts(hosts);
	        
	        Set<Host> tenderingHosts = getTenderingHosts(hosts);
	        Set<Host> biddingHosts = getBiddingHosts(hosts);
	        Set<Host> beingAwardedHosts = getBeingAwardedHosts(hosts);
	        
	        writeRows(Stream.of(CommonState.getTime(), 
					tenderingHosts.size(), biddingHosts.size(), beingAwardedHosts.size()));	
			
	        if (isLogTime()) {
		        LOGGER.info("[{}]~tenderingHosts={}, biddingHosts={}, beingAwardedHosts={}", 
		        		CommonState.getTime(), 
						tenderingHosts.size(), biddingHosts.size(), beingAwardedHosts.size());
	        }
	        
	        assertValid(intersection(tenderingHosts, intersection(biddingHosts, beingAwardedHosts)),
				"hosts are in multiple exclusive states");
		        
			assertValid(intersection(asleepHosts, tenderingHosts),
	        	"sleeping hosts are tendering");
			assertValid(intersection(asleepHosts, biddingHosts),
		        "sleeping hosts are bidding");
			assertValid(intersection(asleepHosts, beingAwardedHosts),
		        "sleeping hosts are being awarded");

			if (!dynamicWorkloads) {
		        assertValid(intersection(balancedHosts, tenderingHosts),
				     "balanced hosts are tendering");
	
		        assertValid(intersection(overloadedHosts, biddingHosts),
			        "overloaded hosts are bidding");
		        assertValid(intersection(overloadedHosts, beingAwardedHosts),
		        	"overloaded hosts are being awarded");
		        
		        assertValid(intersection(violatingHosts, biddingHosts),
		        	"violating hosts are bidding");
		        assertValid(intersection(violatingHosts, beingAwardedHosts),
		        	"violating hosts are being awarded");
			}

		}

		public void assertValid(Set<Host> invalidHosts, String invalidity) {
			if (invalidHosts.size() > 0) {
				LOGGER.error("[{}]~{} {} {};", 
	        		CommonState.getTime(), invalidHosts.size(),invalidity, invalidHosts);
			}
	        assert (invalidHosts.size() == 0) :
	        	invalidHosts.size() + " "+ invalidity +" " + invalidHosts;
		}

		protected Set<Host> getBeingAwardedHosts(List<Host> hosts) {
			return hosts.parallelStream().filter(host -> {
		    	GCNetDVMC gcNetDVMC = (GCNetDVMC)host.getProtocol(protocolID);
		    	return gcNetDVMC.getContractor(host).isBeingAwarded();
		    }).collect(Collectors.toSet());
		}

		protected Set<Host> getBiddingHosts(List<Host> hosts) {
			return hosts.parallelStream().filter(host -> {
		    	GCNetDVMC gcNetDVMC = (GCNetDVMC)host.getProtocol(protocolID);
		    	return gcNetDVMC.getContractor(host).isBidding();
		    }).collect(Collectors.toSet());
		}

		protected Set<Host> getTenderingHosts(List<Host> hosts) {
			return hosts.parallelStream().filter(host -> {
		    	GCNetDVMC gcNetDVMC = (GCNetDVMC)host.getProtocol(protocolID);
		    	return gcNetDVMC.getManager(host).isTendering();
		    }).collect(Collectors.toSet());
		}

	}
	
	public static class VMTenderMessage extends GossipTaskAnnounceMessage {

		private final VM vm;
		
		public VMTenderMessage(Node to, Node from, int contractID, long expirationTime, int counter, VM vm) {
			super(to, from, contractID, expirationTime, counter);
			this.vm = vm;
		}

		protected VM getVM() {
			return vm;
		}
		
		public String toString() {
			return "VMTenderMessage " 
					+ from.getIndex() + "->" + (to != null? to.getIndex() : "null") 
					+ ", contractID=" + contractID + ", expirationTime=" + expirationTime 
					+ ", counter=" + counter
					+ ", vm="+ vm;
		}
		
	}
	
	public static class VMBidMessage extends BidMessage {

		private final double powerDelta;
		
		public VMBidMessage(Node to, Node from, int contractID, double powerDelta) {
			super(to, from, contractID);
			this.powerDelta = powerDelta;
		}

		protected double getPowerDelta() {
			return powerDelta;
		}
		
	}
	
	public static class VMAwardMessage extends AwardMessage {

		private final VM vm;
		
		public VMAwardMessage(Node to, Node from, int contractID, VM vm, long expirationTime) {
			super(to, from, contractID, expirationTime);
			this.vm = vm;
		}

		protected VM getVM() {
			return vm;
		}
		
	}
	
	public static class Manager extends ie.nix.cnet.Manager {
		
		protected Queue<VM> vmsToMigrate;
		
		protected Map<VM, Host> vmMigrationMap;

		public Manager(String prefix) {
			super(prefix);
			vmsToMigrate = new LinkedList<VM>();
			vmMigrationMap = new HashMap<VM, Host>();
	        LOGGER.trace("[{}]", CommonState.getTime());
		}

		@Override
		public Manager clone() {
			Manager foo = null;
			foo = (Manager) super.clone();
			foo.vmsToMigrate = new LinkedList<VM>();
			foo.vmMigrationMap = new HashMap<VM, Host>();
	        LOGGER.trace("[{}]", CommonState.getTime());
			return foo;
		}
		
		/*
		 * Contractor
		 */
		protected final Contractor getContractor(Node node) {
			return (Contractor)node.getProtocol(contractorProtocolID);
		}
		
		public final boolean isContractorBidding(Node node) {
			return getContractor(node).isBidding();
		}
		
		public final boolean isContractorBeingAwarded(Node node) {
			return getContractor(node).isBeingAwarded();
		}
		
		
		/* 
		 * VM Mirgarion Map
		 */
		public boolean canTender(Node node) {
			boolean canTender = !isContractorBidding(node) && !isContractorBeingAwarded(node);
	        LOGGER.trace("[{}]node={}, isContractorBidding()={}, isContractorBeingAwarded={}, canTender={}", 
	        		CommonState.getTime(), node, isContractorBidding(node), isContractorBeingAwarded(node), canTender);
			return canTender;
		}
		
		public void addVMToMigrate(VM vmToMigrate) {
			this.vmsToMigrate.add(vmToMigrate);
	        LOGGER.trace("[{}], vmsToMigrate={}", CommonState.getTime(), vmToMigrate);
		}
		
		public void addVMsToMigrate(List<VM> vmsToMigrate) {
			this.vmsToMigrate.addAll(vmsToMigrate);
	        LOGGER.trace("[{}], vmsToMigrate={}", CommonState.getTime(), vmsToMigrate);
		}

		public void clearVMsToMigrate() {
	        LOGGER.trace("[{}], vmsToMigrate={}", CommonState.getTime(), vmsToMigrate); 
			this.vmsToMigrate.clear();
		}

		public Map<VM, Host> getVmMigrationMap() {
			return vmMigrationMap;
		}

		/*
		 * Tender
		 */
		@Override
		protected boolean shouldTender(Node node) {
			Host host = (Host) node;
			boolean shouldTender = 
					(vmsToMigrate.size() > 0) &&
					host.isAwake() && 
					!isContractorBidding(host);
	        LOGGER.trace("[{}] host={}, shouldTender={}, isContractorBidding={},"
	        		+ "vmsToMigrate={}", 
	        		CommonState.getTime(), node, 
	        		shouldTender, isContractorBidding(host),  vmsToMigrate.size());
			return shouldTender;
		}
		
		protected boolean isUnderloaded(Host host) {
			return host.getCPUUtil() < 0.5;
		}

		protected boolean isOverloaded(Host host) {
			return host.getCPUUtil() > 0.9;
		}
		
		@Override
		protected VMTenderMessage createTaskAnnouncementMessage(
				Node node, int protocolID, Node neighbourNode, int contractID, long expirationTime) {
			// Retrieves and removes the head of this queue,
			VM vm = vmsToMigrate.poll();
			VMTenderMessage tender = new VMTenderMessage(
					neighbourNode, node, contractID, expirationTime, getGossipCounter(), vm);
			return tender;
		}

		/*
		 * Handle Bids
		 */
		@Override
		protected Comparator<BidMessage> getBidComparator(Node node, int protocolID) {			
			Comparator<BidMessage> bidComparator = (BidMessage bid1, BidMessage bid2) -> {
				// Compares its two arguments for order. Returns a negative integer, zero, or a
				// positive integer as the first argument is less than, equal to, or greater
				// than the second.
				// powerDelta1 < powerDelta2 -> -1 (powerDelta1 - powerDelta2) -> bid1, bid2
				// powerDelta1 = powerDelta2 ->  0 (powerDelta1 - powerDelta2) -> bid1, bid2
				// powerDelta1 > powerDelta2 ->  1 (powerDelta1 - powerDelta2) -> bid2, bid1
				double powerDelta1 = (double)((VMBidMessage)bid1).powerDelta;
				double powerDelta2 = (double)((VMBidMessage)bid2).powerDelta;
				int comparePowerDelta = Double.compare(powerDelta1, powerDelta2);
		        LOGGER.trace("[{}]~contractID={}, host={}, powerDelta1={}, powerDelta2={} -> {}", 
		        		CommonState.getTime(), bid1.contractID, node, powerDelta1, powerDelta2, comparePowerDelta);
				return Double.compare(powerDelta1, powerDelta2);
			};
			return bidComparator;
		}
		
		/*
		 * Awards
		 */
		protected boolean shouldAward(Node node, int protocolID, BidMessage bestBid) {
			// Calculate power reduction if VM migrated away from this host
			Host host = (Host)node;
			VM vm = ((VMTenderMessage)getTender(bestBid.contractID)).getVM();
			double lessUtil =  host.getUtilForWorkload(vm.getWorkload()); //vm.getWorkload();
			double newPowerConsumption = host.getPowerConsumptionForUtil(host.getCPUUtil() - lessUtil);
			double powerReduction = host.getPowerConsumption() - newPowerConsumption;

	        LOGGER.trace("[{}]~contractID={}, host={}, bestBid.vm={}, "
	        		+ "origionalPower={}, lessUtil={}, newPowerConsumption={},"
	        		+ "powerReduction={}", 
	        		CommonState.getTime(), bestBid.contractID, node, vm, 
	        		host.getPowerConsumption(), lessUtil, newPowerConsumption,
	        		powerReduction);
			
			// Get the power in crease on the biffing host if the VM moves.
			VMBidMessage bestyVMBid = (VMBidMessage)bestBid;
			double powerIncrease = bestyVMBid.powerDelta;
			
			boolean shouldAward = powerReduction >= powerIncrease;
	        LOGGER.debug("[{}]~contractID={}, host={}, bestBid={}, powerReduction={}, powerIncrease={} -> {}", 
	        		CommonState.getTime(), bestBid.contractID, node, bestBid, powerReduction, powerIncrease, shouldAward);
	        return shouldAward;
		}

		@Override
		protected VMAwardMessage createAwardMessage(Node node, int protocolID, BidMessage bidMessage, long expirationTime) {
			VM vm = ((VMTenderMessage)getTender(bidMessage.contractID)).getVM();
			VMAwardMessage awardMessage = new VMAwardMessage(bidMessage.from, node, bidMessage.contractID, vm, expirationTime);	
			LOGGER.trace("[{}]~contractID={}, host={} -> {}", 
					CommonState.getTime(), awardMessage.contractID, node, awardMessage);
			return awardMessage;
		}
		
		protected void award(Node node, int protocolID, AwardMessage awardMessage) {
			VMAwardMessage vmAwardMessage = (VMAwardMessage)awardMessage;
			VM vm = vmAwardMessage.getVM();
			Host host = (Host)awardMessage.to;
			vmMigrationMap.put(vm, host);
		}

	}
	
	public static class Contractor extends ie.nix.cnet.Contractor {

		protected double overloadedThreshold;
		protected double underloadedThreshold;
		
		public Contractor(String prefix) {
			super(prefix);
			this.overloadedThreshold = Configuration.getDouble(prefix + "." + OVERLOADED_THRESHOLD,
					DEFAULT_OVERLOADED_THRESHOLD);
			this.underloadedThreshold = Configuration.getDouble(prefix + "." + UNDERLOADED_THRESHOLD,
					DEFAULT_UNDERLOADED_THRESHOLD);
	        LOGGER.trace("[{}]", CommonState.getTime());

		}
		
		@Override
		public Contractor clone() {
			Contractor foo = null;
			foo = (Contractor) super.clone();
	        LOGGER.trace("[{}]", CommonState.getTime());
			return foo;
		}
		
		/*
		 * Manager
		 */
		protected Manager getManager(Host host) {
			return (Manager)host.getProtocol(managerProtocolID);
		}

		public boolean shouldManagerTender(Host host) {
			return getManager(host).shouldTender(host);
		}
		
		public boolean isManagerTendering(Host host) {
			return getManager(host).isTendering();
		}
		
		private boolean isManagerAwarding(Host host) {
			return getManager(host).isAwarding();
		}
		
		/*
		 * Tenders
		 */
		protected Comparator<? super TaskAnnounceMessage> getTaskComparator(Node node, int protocolID) {
			return (TaskAnnounceMessage task1, TaskAnnounceMessage task2) -> { 
				Host host = (Host)node;
				// Compares its two arguments for order. Returns a negative integer, zero, or a
				// positive integer as the first argument is less than, equal to, or greater
				// than the second.
				// powerDelta1 < powerDelta2 -> -1 (powerDelta1 - powerDelta2) -> bid1, bid2
				// powerDelta1 = powerDelta2 ->  0 (powerDelta1 - powerDelta2) -> bid1, bid2
				// powerDelta1 > powerDelta2 ->  1 (powerDelta1 - powerDelta2) -> bid2, bid1
				VM vm1 = ((VMTenderMessage)task1).getVM();
				VM vm2 = ((VMTenderMessage)task2).getVM();
				double normalizedPowerDelta1 = (double)getPowerDetla(host, vm1)/vm1.getWorkload();
				double normalizedPowerDelta2 = (double)getPowerDetla(host, vm2)/vm2.getWorkload();
				int comparePowerDelta = Double.compare(normalizedPowerDelta1, normalizedPowerDelta2);
		        LOGGER.trace("[{}]~host={}, "
		        		+ "vm1={}, normalizedPowerDelta1={}, "
		        		+ "vm2={}, normalizedPowerDelta2={} -> {}", 
		        		CommonState.getTime(), node, 
		        		vm1, normalizedPowerDelta1, 
		        		vm2, normalizedPowerDelta2, comparePowerDelta);
				return Double.compare(normalizedPowerDelta1, normalizedPowerDelta2);
			};
		}

		public double getPowerDetla(Host host, VM vm) {
			return host.getPowerDeltaForWorkload(vm.getWorkload());
		}
			
		/*
		 * Bids
		 */
		@Override
		protected boolean shouldBid(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) {
			Host host = (Host)node;
			VM vm = ((VMTenderMessage)taskAnnounceMessage).getVM();
			
			boolean shouldBid = 
					host.isAwake() &&
					!isBidding() && 
					!isBeingAwarded() && 
					!isManagerTendering(host) &&
					!isManagerAwarding(host) &&
					!shouldManagerTender(host) &&
					canFit(host, vm);
	        LOGGER.trace("[{}]~contractID={}, host={}, shouldBid={}, "
	        		+ "host.isAwake={}, "
	        		+ "isBidding()={}, isBeingAwarded()={}, "
	        		+ "isManagerTendering()={}, isManagerAwarding()={}, "
	        		+ "isOverloaded={}, isUnderloaded={}"
	        		+ "host.getCPUUtil={}, host.getRAMUtil={}", 
	        		CommonState.getTime(), taskAnnounceMessage.contractID, host, shouldBid, 
	        		host.isAwake(), 
	        		isBidding(), isBeingAwarded(), 
	        		isManagerTendering(host), isManagerAwarding(host),
	        		isOverloaded(host), isUnderloaded(host),
	        		host.getCPUUtil(), host.getRAMUtil());
			return shouldBid;
			
		}

		protected boolean isUnderloaded(Host host) {
			return host.getCPUUtil() < underloadedThreshold;
		}

		protected boolean isOverloaded(Host host) {
			return host.getCPUUtil() > overloadedThreshold;
		}

		protected boolean canFit(Host host, VM vm) {
			double extraWorkload = vm.getWorkload();
			double extraUtil = host.getUtilForWorkload(extraWorkload);
			boolean canFit = 
					(host.getCPUUtil() + extraUtil) <= overloadedThreshold && 
					(host.getRAMUtil() + vm.getRAM() <= 1.0);
	        LOGGER.trace("[{}] host={}, vm={}, canFit={}, "
	        		+ "new CPUUtil={} <= overloadedUtilizationThreshold={}"
	        		+ "new RAMUtil={} <= 1.0", 
	        		CommonState.getTime(), host, vm, canFit, 
	        		(host.getCPUUtil() + extraUtil), overloadedThreshold, 
	        		(host.getRAMUtil() + vm.getRAM()));
			return canFit;
		}

		@Override
		protected BidMessage createBidMessage(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) {
			Host host = (Host)node;
			VM vm = ((VMTenderMessage)taskAnnounceMessage).getVM();
			double powerDelta = getPowerDetla(host, vm);
			VMBidMessage bidMessage = new VMBidMessage(taskAnnounceMessage.from, host, 
					taskAnnounceMessage.contractID, powerDelta);
			LOGGER.trace("[{}]~contractID={}, host={}, bidMessage={}, vm={}", 
					CommonState.getTime(), bidMessage.contractID, host, bidMessage, vm);		
			return bidMessage;
		}
		
	}
	
	private static final Logger LOGGER = LogManager.getLogger();

	protected static final String OVERLOADED_THRESHOLD = "overloaded_threshold";
	protected static final double DEFAULT_OVERLOADED_THRESHOLD = 0.9;

	protected static final String UNDERLOADED_THRESHOLD = "underloaded_threshold";
	protected static final double DEFAULT_UNDERLOADED_THRESHOLD = 0.4;

	protected static final String CALLBACK_PROTOCOL = "callback";
	protected static final String MANAGER_PROTOCOL = "manager";
	protected static final String CONTRACTOR_PROTOCOL = "contractor";

	protected double overloadedThreshold;
	protected double underloadedThreshold;
	
	protected int callbackProtocolID;
	protected int managerProtocolID;
	protected int contractorProtocolID;

	public GCNetDVMC(String prefix) {
		this.callbackProtocolID = Configuration.lookupPid(CALLBACK_PROTOCOL);
		this.managerProtocolID = Configuration.lookupPid(MANAGER_PROTOCOL);
		this.contractorProtocolID = Configuration.lookupPid(CONTRACTOR_PROTOCOL);
		this.overloadedThreshold = Configuration.getDouble(prefix + "." + OVERLOADED_THRESHOLD,
				DEFAULT_OVERLOADED_THRESHOLD);
		this.underloadedThreshold = Configuration.getDouble(prefix + "." + UNDERLOADED_THRESHOLD,
				DEFAULT_UNDERLOADED_THRESHOLD);
		LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);
	}

	@Override
	public GCNetDVMC clone() {
		GCNetDVMC foo = null;
		try {
			foo = (GCNetDVMC) super.clone();
			
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}
	
	/*
	 * Manager & Contractor
	 */
	protected final Manager getManager(Host host) {
		return (Manager)host.getProtocol(managerProtocolID);
	}
	
	protected final Contractor getContractor(Node node) {
		return (Contractor)node.getProtocol(contractorProtocolID);
	}

	@Override
	public void nextCycle(Node node, int protocolID) {
        LOGGER.trace("[{}]~node={}}", CommonState.getTime(), node);
        Host host = (Host)node;
        
		Manager manager = getManager(host);
		if (manager.canTender(host)) {
			if (amIOverloaded(host)) {
				List<VM> vmsToMigrate = selectVMsToMigrate(host);
				LOGGER.debug("[{}]~overloaded host={}, {}% > {}%, vmsToMigrate={}", 
						CommonState.getTime(), host,
						String.format("%.2f", host.getCPUUtil()*100.0), 
						overloadedThreshold *100.0, 
						vmsToMigrate);
				manager.addVMsToMigrate(vmsToMigrate);
				
				// Adding 1 to the delay as the tender might not go out till the next time step
				// and the award also the award might be a after with in this step.
				int delay = manager.getTenderTimeout()  + (manager.getStep() * 2);
				EDSimulator.add(delay, new Callback(() -> {
						Map<VM, Host> vmMigrationMap = manager.getVmMigrationMap();
						LOGGER.debug("[{}]~slept for {}, host={}, vmMigrationMap={}", 
								CommonState.getTime(), delay, node, vmMigrationMap);
						host.migrateVMs(vmMigrationMap);
						vmMigrationMap.clear();
				}), node, callbackProtocolID);
	
			} else if (amIUnderloaded(host)) {
				List<VM> vmsToMigrate = host.vmList();
				LOGGER.debug("[{}]~underloaded host={}, {}% < {}%, vmsToMigrate={}", 
						CommonState.getTime(), node, 
						String.format("%.2f", host.getCPUUtil()*100.0), 
						underloadedThreshold *100.0, 
						vmsToMigrate);
				manager.addVMsToMigrate(vmsToMigrate); 
				
				// Adding 2 to the delay as the tender might not go out till the next time step
				// and the award also the award might be a after with in this step.
				int delay = manager.getTenderTimeout() + (manager.getStep() * 2);
				EDSimulator.add(delay, new Callback(() -> {
						Map<VM, Host> vmMigrationMap = manager.getVmMigrationMap();
						LOGGER.debug("[{}]~slept for {}, host={}, vmMigrationMap={}", 
								CommonState.getTime(), delay, node, vmMigrationMap);
						host.migrateVMs(vmMigrationMap);
						vmMigrationMap.clear();
						
				}), node, callbackProtocolID);
			}
		}
	}
	
	
	/*
	 * Overloading Management
	 */
	protected boolean amIOverloaded(Host host) {
		boolean amIOverloaded = (
				host.getCPUUtil() > overloadedThreshold ||
				host.getRAMUtil() > 1.0);
		LOGGER.trace("[{}]~host={}, overloadedUtilizationThreshold={} -> {}", 
				CommonState.getTime(), host, overloadedThreshold, amIOverloaded);
		return amIOverloaded;
	}

	/*
	 * Underloading Management
	 */
	protected boolean amIUnderloaded(Host host) {
		boolean amIUnderloaded = (host.isAwake() && 
				host.getCPUUtil() < underloadedThreshold);
		LOGGER.trace("[{}]~host={}, cpuUtil()={}, getRAMUtil()={}, underloadedUtilizationThreshold={} -> {}", 
				CommonState.getTime(), host, 
				String.format("%.2f", host.getCPUUtil()), 
				underloadedThreshold, amIUnderloaded);
		return amIUnderloaded;
	}

	/*
	 * Select VMs To Migrate
	 */
	protected List<VM> selectVMsToMigrate(Host host) {
		LOGGER.trace("[{}]~host={}", CommonState.getTime(), host);
		List<VM> vmsToMigrate = new ArrayList<VM>();
		double workloadToRecover = (host.getCPUUtil() - overloadedThreshold) * host.spec.getMaxWorkload();
		double workloadRecovered = 0;
		Comparator<VM> byUtilization = (VM vm1, VM vm2) -> Double.compare(vm1.spec.getMIPs(), vm2.spec.getMIPs());
		List<VM> vmsSortedByUtilization = host.vmList();
		vmsSortedByUtilization.sort(byUtilization);
		for (VM vm : vmsSortedByUtilization) {
			if (workloadRecovered < workloadToRecover) {
				workloadRecovered += vm.getWorkload();
				vmsToMigrate.add(vm);
			} else {
				break;
			}
		}
		LOGGER.debug("[{}]~host={}, vmsToMigrate={}", CommonState.getTime(), host, vmsToMigrate);
		return vmsToMigrate;
	}
	
}