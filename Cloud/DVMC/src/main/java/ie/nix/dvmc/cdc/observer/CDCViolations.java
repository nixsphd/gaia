package ie.nix.dvmc.cdc.observer;

import java.util.List;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import peersim.core.CommonState;

public class CDCViolations extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public CDCViolations(String prefix) {
		super(prefix);
		writeHeaders(Stream.of("time", "unallocatedVMs", "inconsistantVMs"));
	}

	public void observe(CloudDataCenter cdc) {
        LOGGER.debug("[{}]", CommonState.getTime());

        List<VM> vms = cdc.getVMs();
        
        long unallocatedVMsCount = vms.parallelStream().filter(vm -> {
        	return vm.getHost() == null;
        }).count();
        
        long inconsistantVMsCount = vms.parallelStream().filter(vm -> {
        	Host host = vm.getHost();
        	return host != null && !host.hasVM(vm);
        }).count();
        
		
		writeRows(Stream.of(CommonState.getTime(), unallocatedVMsCount, inconsistantVMsCount));
		
		if (unallocatedVMsCount + inconsistantVMsCount > 0) {
			Object[] unallocatedVMs  = vms.parallelStream().filter(vm -> {
	        	return vm.getHost() == null;
	        }).toArray();
			
			Object[] inconsistantVMs  = vms.parallelStream().filter(vm -> {
	        	Host host = vm.getHost();
	        	return host != null && !host.hasVM(vm);
	        }).toArray();
			
			LOGGER.error("[{}]~unallocatedVMs={}, inconsistantVMs={}", 
	        		CommonState.getTime(), unallocatedVMsCount, inconsistantVMsCount);
			LOGGER.debug("[{}]~unallocatedVMs={}", CommonState.getTime(), unallocatedVMs);
			LOGGER.debug("[{}]~inconsistantVMs={}", CommonState.getTime(), inconsistantVMs);
			
			vms.parallelStream().filter(vm -> {
	        	return vm.getHost() == null;
	        }).toArray();
			
		} else {
			LOGGER.info("[{}]~unallocatedVMs={}, inconsistantVMs={}", 
        		CommonState.getTime(), unallocatedVMsCount, inconsistantVMsCount);
		}

	}

}
