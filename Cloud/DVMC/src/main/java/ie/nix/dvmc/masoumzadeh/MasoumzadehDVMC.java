package ie.nix.dvmc.masoumzadeh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.cdc.protocols.AverageProtocol;
import ie.nix.dvmc.cdc.protocols.CallbackProtocol.Callback;
import ie.nix.dvmc.cdc.protocols.LockProtocol;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.edsim.EDSimulator;

public class MasoumzadehDVMC implements CDProtocol, EDProtocol {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static final String OVERLOADED_UTILISATION_THRESHOLD = "overloaded_utilization_threshold";
	public static final double DEFAULT_OVERLOADED_UTILISATION_THRESHOLD = 0.9;

	public static final String CALLBACK_PROTOCOL = "callback";
	public static final String LOCK_PROTOCOL = "lock";
	public static final String AVG_CPU_UTIL_PROTOCOL = "average_cpu_util";

	protected int callbackProtocolID;
	protected int lockProtocolID;
	protected int avgCPUUtilProtocolID;
	
	protected double overloadedUtilizationThreshold;

	public MasoumzadehDVMC(String prefix) {
		this.lockProtocolID = Configuration.lookupPid(LOCK_PROTOCOL);
		this.callbackProtocolID = Configuration.lookupPid(CALLBACK_PROTOCOL);
		this.avgCPUUtilProtocolID = Configuration.lookupPid(AVG_CPU_UTIL_PROTOCOL);
		this.overloadedUtilizationThreshold = Configuration.getDouble(
				prefix + "." + OVERLOADED_UTILISATION_THRESHOLD, DEFAULT_OVERLOADED_UTILISATION_THRESHOLD);
        LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);
	}

	@Override
	public MasoumzadehDVMC clone() {
		MasoumzadehDVMC foo = null;
		try {
			foo = (MasoumzadehDVMC) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}

	@Override
	public void nextCycle(Node node, int protocolID) {
        LOGGER.trace("[{}]~node={}, protocolID={}", CommonState.getTime(), node, protocolID);
        Host host = (Host)node;
        LockProtocol lockProtocol = (LockProtocol)host.getProtocol(lockProtocolID);

		if (host.isAwake() && !host.hasVMs()) {
	        LOGGER.debug("[{}]~node={}, protocolID={}, going asleep", CommonState.getTime(), node, protocolID);
			host.sleep();
			
		} else if (host.isAwake()) {
			LOGGER.trace("[{}]~node={}, protocolID={}, awake....", CommonState.getTime(), node, protocolID);
			
			if (amIOverloaded(host)) {
				List<VM> vmsToMigrate = selectVMsToMigrate(host);
				LOGGER.debug("[{}]~node={}, protocolID={}, overloaded {}% > {}%, vmsToMigrate={}", 
						CommonState.getTime(), node, protocolID, 
						String.format("%.2f", host.getCPUUtil()*100.0), 
						overloadedUtilizationThreshold *100.0, 
						vmsToMigrate);
				
				lockProtocol.lockNeignbours(node, lockProtocolID);

				EDSimulator.add(2, new Callback(() -> {
					LOGGER.trace("[{}]~node={}, protocolID={}, slept, lockNeignbours={}", 
						CommonState.getTime(), node, protocolID, lockProtocol.getLockedNeighbours());
					Map<VM, Host> vmMigrationMap = 
							vmPlacement(vmsToMigrate, lockProtocol.getLockedNeighbours(), true);
					host.migrateVMs(vmMigrationMap);
					lockProtocol.unlockNeignbours(node, lockProtocolID);
				}), node, callbackProtocolID);

			} else if (amIUnderloaded(host)) {
				List<VM> vmsToMigrate = host.vmList();
				LOGGER.debug("[{}]~node={}, protocolID={}, underloaded {}% < {}%, vmsToMigrate={}", 
						CommonState.getTime(), node, protocolID, 
						String.format("%.2f", host.getCPUUtil()*100.0), 
						String.format("%.2f", ((AverageProtocol)host.getProtocol(avgCPUUtilProtocolID)).getAverage()*100.0), 
						vmsToMigrate);
				
				lockProtocol.lockNeignbours(node, lockProtocolID);

				EDSimulator.add(2, new Callback(() -> {
					LOGGER.trace ("[{}]~node={}, protocolID={}, slept, lockNeignbours={}", 
							CommonState.getTime(), node, protocolID, lockProtocol.getLockedNeighbours());
					Map<VM, Host> vmMigrationMap = 
							vmPlacement(vmsToMigrate, lockProtocol.getLockedNeighbours(), false);
					host.migrateVMs(vmMigrationMap);
					lockProtocol.unlockNeignbours(node, lockProtocolID);
					if (!host.hasVMs()) {
				        LOGGER.debug("[{}]~node={}, protocolID={}, going asleep", 
				        		CommonState.getTime(), node, protocolID);
						host.sleep();
					}
				}), node, callbackProtocolID);

			}
		}
		
	}

	@Override
	public void processEvent(Node node, int pid, Object event) {
		LOGGER.trace("[{}]~node={}", CommonState.getTime(),node.getID());	
	}

	/*
	 * Overloading Management
	 * 
	 * In order to determine when a host is considered as over-loaded we utilize a
	 * single threshold-based strategy. If the CPU utilization of the host node
	 * exceeds the threshold value, it is considered as overloaded.
	 */
	protected boolean amIOverloaded(Host host) {
		boolean amIOverloaded = (host.getCPUUtil() > overloadedUtilizationThreshold);
		LOGGER.trace("[{}]~host={}, cpuUtil()={}, utilizationThreshold={} -> {}", 
				CommonState.getTime(), host, 
				String.format("%.2f", host.getCPUUtil()), overloadedUtilizationThreshold, amIOverloaded);
		return amIOverloaded;
	}

	/*
	 * Underloading Management
	 *
	 * Underloading management can be done through a competitive strategy whereby a
	 * physical host node is considered underloaded if it has a CPU utilization
	 * lower than the average CPU utilization of other physical host nodes. In this
	 * situation, each physical host node needs to know the CPU utilization of other
	 * physical host nodes to make decisions.
	 *
	 */
	protected boolean amIUnderloaded(Host host) {
		AverageProtocol avgCPUUtilProtocol = (AverageProtocol)host.getProtocol(avgCPUUtilProtocolID);
		boolean amIUnderloaded = host.isAwake() && (host.getCPUUtil() < avgCPUUtilProtocol.getAverage());
		LOGGER.trace("[{}]~host={}, cpuUtil()={}, avgCPUUtilProtocol={} -> {}", 
				CommonState.getTime(), host, 
				String.format("%.2f", host.getCPUUtil()), avgCPUUtilProtocol, amIUnderloaded);
		return amIUnderloaded;

	}

	/*
	 * Select VMs To Migrate
	 * 
	 * In this situation a VM selection strategy selects one or more virtual
	 * machines to migrate VMs to, until the CPU utilization falls below the
	 * threshold value. In our work, we used a fixed threshold value and a VM
	 * selection strategy using a random policy to select virtual machines to
	 * migrate. The overloading management algorithm runs inside each physical host
	 * node locally and does not need to have information from other host nodes as
	 * an input for the decision making process.
	 * 
	 */
	protected List<VM> selectVMsToMigrate(Host host) {
		LOGGER.trace("[{}]~host={}", CommonState.getTime(), host);
		double workloadToRecover = (host.getCPUUtil() - overloadedUtilizationThreshold) * host.spec.getMaxWorkload();
		double workloadRecovered = 0;
		List<VM> vmsToMigrate = new ArrayList<VM>();
		Collections.shuffle(host.vmList());
		for (VM vm : host.vmList()) {
			if (workloadRecovered < workloadToRecover) {
				workloadRecovered += vm.getWorkload();
				vmsToMigrate.add(vm);

			} else {
				break;

			}
		}
		LOGGER.debug("[{}]~vmsToMigrate={}", CommonState.getTime(), vmsToMigrate);
		return vmsToMigrate;
	}

	/*
	 * VM Placement Algorithm
	 */
	protected Map<VM, Host> vmPlacement(List<VM> vmsToMigrate, List<Node> lockedNeighbours, boolean allowHostWakeup) {
		LOGGER.trace("[{}]~vmsToMigrate={}, lockedNeighbours={}", CommonState.getTime(), vmsToMigrate, lockedNeighbours);
		Map<VM, Host> vmMigrationMap = new HashMap<VM, Host>();

		// first off check we have some locked hosts
		if (lockedNeighbours.size() == 0) {
			LOGGER.trace("[{}]~No locked neighbours", CommonState.getTime());

		} else {

			// We need to keep track of the utilization after each VM has been migrated.
			Map<Host, Double> hostUtilizationMap = new HashMap<Host, Double>();
			for (Node node : lockedNeighbours) {
				Host host = (Host)node;
				hostUtilizationMap.put(host, host.getCPUUtil());
			}

			for (VM vm : vmsToMigrate) {
				// minPower ← MAX
				double minPower = Double.MAX_VALUE;
				// allocatedHost ← NULL
				Host allocatedHost = null;

				// for host:lockedMemberList do
				for (Node node : lockedNeighbours) {
					Host host = (Host)node;

					// if host.getState() is wakeUp then
					if (host.isAwake()) {
						double newUtilization = hostUtilizationMap.get(host)
								+ host.getUtilForWorkload(vm.getWorkload());
						// if host.utilization + vm.utilization ≤ host.threshold then
						if (newUtilization <= overloadedUtilizationThreshold) {
							// power ← estimatePower(host, vm)
							double power = host.getPowerConsumptionForUtil(newUtilization);
							// if power < minPower then
							if (power < minPower) {
								// allocatedHost ← host
								allocatedHost = host;
								// minPower ← power
								minPower = power;
							}
						}
					}
				}
				if (allocatedHost != null) {
					// migrationMap.add(vm, allocatedHost)
					vmMigrationMap.put(vm, allocatedHost);

					// Add update the utilization now that we've allocated a VM to that host.
					double initialUtilization = hostUtilizationMap.get(allocatedHost);
					hostUtilizationMap.put(allocatedHost,
							initialUtilization + allocatedHost.getUtilForWorkload(vm.getWorkload()));
				}
			}

			// Remove any VMs from the vmsToMigrate list so we don't try and allocate them
			// again!
			vmsToMigrate.removeAll(vmMigrationMap.keySet());

			// if !vmList.isEmpty() then
			if (!vmsToMigrate.isEmpty() && allowHostWakeup) {
				// for host:lockedMemberList do
				for (Node node : lockedNeighbours) {
					Host host = (Host)node;
					if (!host.isAwake()) {
						// for vm:vmList do
						for (VM vm : vmsToMigrate) {
							// if host.utilization + vm.utilization ≤ host.threshold then
							double newUtilization = host.getCPUUtil() + host.getUtilForWorkload(vm.getWorkload());
							if (newUtilization <= overloadedUtilizationThreshold) {
								vmMigrationMap.put(vm, host);

								// Need to wake up the host too...
								if (!host.isAwake()) {
									host.wakeUp();
									LOGGER.debug("[{}]~Wakeup {}", CommonState.getTime(), host);	
								}

								// Add update the utilization now that we've allocated a VM to that host.
								hostUtilizationMap.put(host,
										host.getCPUUtil() + host.getUtilForWorkload(vm.getWorkload()));
							}
						}

						// Remove any VMs from the vmsToMigrate list so we don't try and allocate them
						// again!
						vmsToMigrate.removeAll(vmMigrationMap.keySet());
					}
				}
			}
		}
		LOGGER.debug("[{}]~vmMigrationMap={}", CommonState.getTime(), vmMigrationMap);	
		return vmMigrationMap;
	}

}