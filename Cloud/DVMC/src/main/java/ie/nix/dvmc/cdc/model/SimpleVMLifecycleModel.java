package ie.nix.dvmc.cdc.model;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMLifecycleModel;
import ie.nix.dvmc.cdc.VM;
import peersim.core.CommonState;

public class SimpleVMLifecycleModel implements VMLifecycleModel {

	private static final Logger LOGGER = LogManager.getLogger();

	public SimpleVMLifecycleModel(String prefix) {
        LOGGER.debug("[{}]", CommonState.getTime());
	}
	
	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	@Override
	public boolean init() {
		return false;
	}

	public boolean step() {
        LOGGER.debug("[{}]", CommonState.getTime());
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		// First find the VM to be terminated
		List<VM> toTerminate = cdc.getVMs().stream().filter(vm -> vm.getTerminationTime() != -1)
				.filter(vm -> vm.getUptime() >= vm.getTerminationTime()).collect(Collectors.toList());
		// The terminate them, need to be done in two steps to avoid concurrent issues
		// with List.
		toTerminate.forEach(vm -> cdc.terminateVM(vm));
		return false;
	}

}