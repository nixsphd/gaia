package ie.nix.dvmc.cdc.protocols;

import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter.VMMigrationModel;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import peersim.core.CommonState;
import peersim.core.Node;

public class InstantiousVMMigration implements VMMigrationModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private int migrations;

	public InstantiousVMMigration(String prefix) {
        LOGGER.trace("[{}]", CommonState.getTime());
		migrations = 0;
	}

	@Override
	public InstantiousVMMigration clone() {
		InstantiousVMMigration foo = null;
		try {
			foo = (InstantiousVMMigration) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}

	@Override
	public void migrateVM(Host fromHost, VM vm, Host toHost) {
		fromHost.removeVM(vm);
		assert(!fromHost.hasVM(vm)) : 
			"Migration failed for vm "+vm+" from host "+fromHost+" still has the VM, toHost="+toHost;
		
		toHost.addVM(vm);
		assert(toHost.hasVM(vm)) : 
			"Migration failed for vm "+vm+" to host "+toHost+" does not have the VM, fromHost="+fromHost;
		
		migrations++;
        LOGGER.debug("[{}]~vm={} {} {}% -> {} {}%, migrations={}", CommonState.getTime(), 
        		vm, 
        		fromHost, String.format("%.2f", fromHost.getCPUUtil()*100.0),
        		toHost, String.format("%.2f", toHost.getCPUUtil()*100.0),
        		migrations);
	}

	public void migrateVMs(Host host, Map<VM, Host> vmMigrationMap) {
		for (VM vm : vmMigrationMap.keySet()) {
			Host newHost = vmMigrationMap.get(vm);
			migrateVM(host, vm, newHost);
		}
	}

	@Override
	public void processEvent(Node node, int pid, Object event) {
		// Do nothing, no real events.
	}

	@Override
	public int getNumberOfMigrations() {
		return migrations;
	}

}