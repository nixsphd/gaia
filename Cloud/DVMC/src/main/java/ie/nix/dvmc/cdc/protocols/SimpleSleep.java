package ie.nix.dvmc.cdc.protocols;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter.HostSleepModel;
import ie.nix.dvmc.cdc.Host;
import peersim.core.CommonState;
import peersim.core.Node;

public class SimpleSleep implements HostSleepModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	// Host state
	public static enum SleepState {
		AWAKE, ASLEEP
	};
	
	// The specification of the host
	private SleepState sleepState;

	public SimpleSleep(String prefix) {
		this.sleepState = SleepState.AWAKE;
        LOGGER.debug("[{}]", CommonState.getTime());
	}

	@Override
	public SimpleSleep clone() {
		SimpleSleep foo = null;
		try {
			foo = (SimpleSleep) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.debug("[{}]", CommonState.getTime());
		return foo;
	}

	/*
	 * Host Awake / Sleep protocol
	 */
	public boolean isAwake(Host host) {
		return sleepState == SleepState.AWAKE;
	}

	public void sleep(Host host) {
		setState(SleepState.ASLEEP);
        LOGGER.debug("[{}]~Host-{}", CommonState.getTime(), host.getID());

	}

	public void wakeUp(Host host) {
		setState(SleepState.AWAKE);
        LOGGER.debug("[{}]~Host-{}", CommonState.getTime(), host.getID());

	}

	protected SleepState getState() {
		return sleepState;
	}

	protected void setState(SleepState sleepState) {
		this.sleepState = sleepState;
	}

	@Override
	public void processEvent(Node node, int pid, Object event) {
		// Do nothing, no real events.
	}

	@Override
	public Optional<Host> findSleepingHost(Host host) {
		throw new UnsupportedOperationException();
	}

}