package ie.nix.dvmc.cdc.observer;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.observer.Observer;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class DVMCObserver extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static <T> Set<T> union(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new HashSet<T>(setA);
		tmp.addAll(setB);
		return tmp;
	}

	public static <T> Set<T> intersection(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new HashSet<T>();
		for (T x : setA)
			if (setB.contains(x))
				tmp.add(x);
		return tmp;
	}

	public static <T> Set<T> difference(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new HashSet<T>(setA);
		tmp.removeAll(setB);
		return tmp;
	}

	public static <T> Set<T> symDifference(Set<T> setA, Set<T> setB) {
		Set<T> tmpA;
		Set<T> tmpB;

		tmpA = union(setA, setB);
		tmpB = intersection(setA, setB);
		return difference(tmpA, tmpB);
	}
	
	public static <T> boolean isSubset(Set<T> setA, Set<T> setB) {
		return setB.containsAll(setA);
	}

	public static <T> boolean isSuperset(Set<T> setA, Set<T> setB) {
		return setA.containsAll(setB);
	}
	  
	protected static final String OVERLOADED_THRESHOLD = "overloaded_threshold";
	protected static final String UNDERLOADED_THRESHOLD = "underloaded_threshold";
	
	protected double overloadedThreshold;
	protected double underloadedThreshold;
	
	public DVMCObserver(String prefix) {
		super(prefix);
		this.overloadedThreshold = Configuration.getDouble(prefix + "." + OVERLOADED_THRESHOLD);
		this.underloadedThreshold = Configuration.getDouble(prefix + "." + UNDERLOADED_THRESHOLD);

		writeHeaders(Stream.of("time", 
				"asleepHosts", "underloadedHosts", "balancedHosts", 
				"overloadedHosts", "violatingHosts"));
	}

	public void observe(CloudDataCenter cdc) {
        LOGGER.debug("[{}]", CommonState.getTime());

        List<Host> hosts = cdc.getHosts();
        
        Set<Host> asleepHosts = getAsleepHosts(hosts);
        Set<Host> underloadedHosts = getUnderloadedHosts(hosts);
        Set<Host> balancedHosts = getBalancedHosts(hosts);  
        Set<Host> overloadedHosts = getOverloadedHosts(hosts); 
        Set<Host> violatingHosts = getViolatingHosts(hosts);
        
        writeRows(Stream.of(CommonState.getTime(), 
				asleepHosts.size(), underloadedHosts.size(), balancedHosts.size(), 
				overloadedHosts.size(), violatingHosts.size()));	
		
        LOGGER.info("[{}]~asleepHosts={}, underloadedHosts={}, balancedHosts={}, "
				+ "overloadedHosts={}, violatingHosts={}", 
        		CommonState.getTime(), 
				asleepHosts.size(), underloadedHosts.size(), balancedHosts.size(), 
				overloadedHosts.size(), violatingHosts.size());
            
        assertValid(intersection(asleepHosts, underloadedHosts),
	        	"sleeping hosts are underloaded");
        assertValid(intersection(asleepHosts, balancedHosts),
	        	"sleeping hosts are balanced");
        assertValid(intersection(underloadedHosts, balancedHosts),
	        	"underloaded hosts are balanced");
        assertValid(intersection(balancedHosts, overloadedHosts),
	        	"balanced hosts are overloaded");
        assertValid(intersection(overloadedHosts, violatingHosts),
	        	"overloaded hosts are violating");
        
        assert((asleepHosts.size() + underloadedHosts.size() + balancedHosts.size() + 
        		overloadedHosts.size() + violatingHosts.size()) == hosts.size()) :
    		"hosts are in multiple exclusive states";
 
	}

	public void assertValid(Set<Host> invalidHosts, String invalidity) {
		if (invalidHosts.size() > 0) {
			LOGGER.error("[{}]~{} {} {};", 
        		CommonState.getTime(), invalidHosts.size(),invalidity, invalidHosts);
		}
        assert (invalidHosts.size() == 0) :
        	invalidHosts.size() + " "+ invalidity +" " + invalidHosts;
	}

	protected Set<Host> getAsleepHosts(List<Host> hosts) {
		return hosts.parallelStream().filter(host -> {
        	return !host.isAwake();
        }).collect(Collectors.toSet());
	}
	
	protected Set<Host> getAwakeHosts(List<Host> hosts) {
		return hosts.parallelStream().filter(host -> {
        	return host.isAwake();
        }).collect(Collectors.toSet());
	}

	protected Set<Host> getUnderloadedHosts(List<Host> hosts) {
		return hosts.parallelStream().filter(host -> {
			double util = host.getCPUUtil();
        	return host.isAwake() && util < underloadedThreshold;
        }).collect(Collectors.toSet());
	}

	protected Set<Host> getBalancedHosts(List<Host> hosts) {
		return hosts.parallelStream().filter(host -> {
			double util = host.getCPUUtil();
        	return util >= underloadedThreshold && util <= overloadedThreshold;
        }).collect(Collectors.toSet());
	}

	protected Set<Host> getOverloadedHosts(List<Host> hosts) {
		return hosts.parallelStream().filter(host -> {
			double util = host.getCPUUtil();
        	return util > overloadedThreshold && util <= 1.0;
        }).collect(Collectors.toSet());
	}

	protected Set<Host> getViolatingHosts(List<Host> hosts) {
		return hosts.parallelStream().filter(host -> {
        	return host.getCPUUtil() > 1.0;
        }).collect(Collectors.toSet());
	}
	
	protected Set<Host> getEmptyHosts(List<Host> hosts) {
		return hosts.parallelStream().filter(host -> {
        	return !host.hasVMs();
        }).collect(Collectors.toSet());
	}

}
