package ie.nix.dvmc.cdc.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMWorkloadModel;
import ie.nix.dvmc.cdc.VM;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class SinusoidalVMWorkload implements VMWorkloadModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static final String PERIOD = "period";
	public static final String STEP = "step";
	public static final String MAX_SHIFT = "max_shift";

	public static final double DEFAULT_MAX_SHIFT = 0;
	
    protected final long step;
    protected final long period;
    protected final double maxShift;
    
	// CoAs, a non-zero center amplitude
	private Map<VM, Double> cofAs; 
	// As = amplitudes, the peak deviation of the function from zero.
	private Map<VM, Double> As; 
	// Shift is how much the sine wave is shifted along the x axis. Its a number between 0 and 2 Pie
	private Map<VM, Double> shifts; 
	
	private Map<VM, Double> workloads;
	
	public SinusoidalVMWorkload(String prefix) {
		this.step = Configuration.getLong(prefix + "." + STEP);
		this.period = Configuration.getLong(prefix + "." + PERIOD, step);
		this.maxShift = Configuration.getDouble(prefix + "." + MAX_SHIFT, DEFAULT_MAX_SHIFT);
		this.cofAs = new HashMap<VM, Double>();
		this.As = new HashMap<VM, Double>();
		this.shifts = new HashMap<VM, Double>();
		this.workloads = new HashMap<VM, Double>();
        LOGGER.info("[{}]", CommonState.getTime());
        CloudDataCenter.getCloudDataCenter().setVMWorkloadModel(this);
        init();
	}
	
	@Override
	public boolean isDynamic() {
		return true;
	}
	
	@Override
	public double getWorkload(VM vm) {
        LOGGER.trace("[{}]~{} -> {}", 
        		CommonState.getTime(), vm, workloads.get(vm));
		return workloads.get(vm);
	}
	
	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
        LOGGER.trace("[{}]", CommonState.getTime());
        
        
		CloudDataCenter.getCloudDataCenter().getVMs().stream().forEach(vm -> {
			double random1 = CommonState.r.nextDouble();
			double random2 = CommonState.r.nextDouble();
			double cofA = random1 * vm.spec.getMIPs();
			double A = Math.min(vm.spec.getMIPs() - cofA, cofA);
			double shift =  random2 * maxShift;
			
			cofAs.put(vm, cofA);
			As.put(vm, A);
			workloads.put(vm, cofA);
			shifts.put(vm, shift);
	        LOGGER.trace("[{}]~VM={}, random1={}, random2={}, cofA={}, A={}, shift={}", 
	        		CommonState.getTime(), vm,
	        		String.format("%.2f", random1), 
	        		String.format("%.2f", random2), 
	        		String.format("%.2f", cofA), 
	        		String.format("%.2f", A),
	        		String.format("%.2f", shift), 
	        		String.format("%.2f", workloads.get(vm)));
		});
		return false;
	}
		
	public boolean step() {
        LOGGER.trace("[{}]", CommonState.getTime());
        long time = CommonState.getTime();
		double angle = (2 * Math.PI * (time % period))/period; // in radians
				
        Map<VM, Double> newWorkloads = new HashMap<VM, Double>();
		CloudDataCenter.getCloudDataCenter().getVMs().stream().forEach(vm -> {
			double cofA = cofAs.get(vm);
			double A = As.get(vm);
			double shift = shifts.get(vm);
			double newWorkload = (A * Math.sin(angle + shift)) + cofA;
			newWorkloads.put(vm, newWorkload);
	        LOGGER.trace("[{}]~VM={} angle={}, cofA={}, A={}, workload={}->{}",
	        		CommonState.getTime(), vm,
	        		String.format("%.2f", angle), 
	        		String.format("%.2f", cofA), 
	        		String.format("%.2f", A),
	        		String.format("%.2f", workloads.get(vm)), 
	        		String.format("%.2f", newWorkloads.get(vm)));
		});
		
		workloads = newWorkloads;
		return false;
	}
}