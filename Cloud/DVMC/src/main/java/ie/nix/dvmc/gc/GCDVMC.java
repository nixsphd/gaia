package ie.nix.dvmc.gc;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.cdc.observer.DVMCObserver;
import ie.nix.gc.Agent;
import ie.nix.gc.TenderMessage;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;

public class GCDVMC<T,P> extends Agent<T,P> {
	
	public static class Observer extends DVMCObserver {

		private static final String PROTOCOL = "protocol";
		
		// Protocol that can consolidate the VM
		private int protocolID;
		
		public Observer(String prefix) {
			super(prefix);
			
			this.protocolID = Configuration.getPid(prefix+"."+PROTOCOL);
	        LOGGER.trace("[{}]~protocolID={}", CommonState.getTime(), protocolID);
			
			writeHeaders(Stream.of("time", "tenderingHosts", "biddingHosts", "beingAwardedHosts"));
		}

		public void observe(CloudDataCenter cdc) {
	        LOGGER.trace("[{}]", CommonState.getTime());

	        List<Host> hosts = cdc.getHosts();
	        boolean dynamicWorkloads = cdc.getVMorkloadModel().isDynamic();
	        
	        Set<Host> asleepHosts = getAsleepHosts(hosts);
	        Set<Host> balancedHosts = getBalancedHosts(hosts);  
	        Set<Host> overloadedHosts = getOverloadedHosts(hosts); 
	        Set<Host> violatingHosts = getViolatingHosts(hosts);
	        Set<Host> awakeHosts = getAwakeHosts(hosts);

	        Set<Host> emptyHosts = getEmptyHosts(hosts);
	        
	        Set<Host> tenderingHosts = getTenderingHosts(hosts);
	        Set<Host> biddingHosts = getBiddingHosts(hosts);
	        Set<Host> beingAwardedHosts = getBeingAwardedHosts(hosts);
	        
	        writeRows(Stream.of(CommonState.getTime(), 
					tenderingHosts.size(), biddingHosts.size(), beingAwardedHosts.size()));	
					
	        if (isLogTime()) {
		        LOGGER.info("[{}]~tenderingHosts={}, biddingHosts={}, beingAwardedHosts={}", 
		        		CommonState.getTime(), 
						tenderingHosts.size(), biddingHosts.size(), beingAwardedHosts.size());
	        }
	        
//	        assertValid(intersection(emptyHosts, awakeHosts), "empty hosts are awake");
	        
			assertValid(intersection(tenderingHosts, intersection(biddingHosts, beingAwardedHosts)),
				"hosts are in multiple exclusive states");
	        
			assertValid(intersection(asleepHosts, tenderingHosts),
	        	"sleeping hosts are tendering");
//			TODO
//			assertValid(intersection(asleepHosts, biddingHosts),
//		        "sleeping hosts are bidding");
//			assertValid(intersection(asleepHosts, beingAwardedHosts),
//		        "sleeping hosts are being awarded");

			if (!dynamicWorkloads) {
//		        assertValid(intersection(overloadedHosts, tenderingHosts),
//		        "overloaded hosts are tendering");
//		        assertValid(intersection(violatingHosts, tenderingHosts),
//	        	"violating hosts are tendering");
				
//		        assertValid(intersection(balancedHosts, tenderingHosts),
//			        "balanced hosts are tendering");
	
//		        assertValid(intersection(overloadedHosts, biddingHosts),
//			        "overloaded hosts are bidding");
//		        assertValid(intersection(overloadedHosts, beingAwardedHosts),
//		        	"overloaded hosts are being awarded");
//		        
//		        assertValid(intersection(violatingHosts, biddingHosts),
//		        	"violating hosts are bidding");
//		        assertValid(intersection(violatingHosts, beingAwardedHosts),
//		        	"violating hosts are being awarded");
			}

		}

		public void assertValid(Set<Host> invalidHosts, String invalidity) {
			if (invalidHosts.size() > 0) {
				LOGGER.error("[{}]~{} {} {};", 
	        		CommonState.getTime(), invalidHosts.size(),invalidity, invalidHosts);
			}
	        assert (invalidHosts.size() == 0) :
	        	invalidHosts.size() + " "+ invalidity +" " + invalidHosts;
		}

		@SuppressWarnings("rawtypes")
		protected Set<Host> getBeingAwardedHosts(List<Host> hosts) {
			return hosts.parallelStream().filter(host -> {
				GCDVMC gcDVMC = (GCDVMC)host.getProtocol(protocolID);
		    	return gcDVMC.isBeingAwarded();
		    }).collect(Collectors.toSet());
		}

		@SuppressWarnings("rawtypes")
		protected Set<Host> getBiddingHosts(List<Host> hosts) {
			return hosts.parallelStream().filter(host -> {
				GCDVMC gcDVMC = (GCDVMC)host.getProtocol(protocolID);
		    	return gcDVMC.isBidding();
		    }).collect(Collectors.toSet());
		}

		@SuppressWarnings("rawtypes")
		protected Set<Host> getTenderingHosts(List<Host> hosts) {
			return hosts.parallelStream().filter(host -> {
				GCDVMC gcDVMC = (GCDVMC)host.getProtocol(protocolID);
		    	return gcDVMC.isTendering();
		    }).collect(Collectors.toSet());
		}

	}
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	protected static final String OVERLOADED_THRESHOLD = "overloaded_threshold";
	protected static final String UNDERLOADED_THRESHOLD = "underloaded_threshold";
	
	protected static final double DEFAULT_OVERLOADED_THRESHOLD = 0.9;
	protected static final double DEFAULT_UNDERLOADED_THRESHOLD = 0.0;
	
	protected double overloadedThreshold;
	protected double underloadedThreshold;
	
	private Map<VM, Host> vmMigrationMap;
	
	public GCDVMC(String prefix) {
		super(prefix);
		this.overloadedThreshold = 
				Configuration.getDouble(prefix + "." + OVERLOADED_THRESHOLD, DEFAULT_OVERLOADED_THRESHOLD);
		this.underloadedThreshold = 
				Configuration.getDouble(prefix + "." + UNDERLOADED_THRESHOLD, DEFAULT_UNDERLOADED_THRESHOLD);
		
		this.vmMigrationMap = new HashMap<VM, Host>();
		
		LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);
	}
		
	@Override
	public GCDVMC<T,P> clone() {
		GCDVMC<T,P> foo = null;
		foo = (GCDVMC<T,P>)super.clone();
		foo.vmMigrationMap = new HashMap<VM, Host>();
		LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}

	/*
	 * Violating the SLA!
	 */
	protected boolean amIViolating(Host host) {
		boolean amIViolating = (
				host.getCPUUtil() > 1.0 || host.getRAMUtil() > 1.0);
		LOGGER.trace("[{}]~host={} -> {}", 
				CommonState.getTime(), host, amIViolating);
		return amIViolating;
	}
	
	/*
	 * Overloading Management
	 */
	protected boolean amIOverloaded(Host host) {
		boolean amIOverloaded = (
				host.getCPUUtil() > overloadedThreshold || host.getRAMUtil() > 1.0);
		LOGGER.trace("[{}]~host={}, overloadedThreshold={} -> {}", 
				CommonState.getTime(), host, overloadedThreshold, amIOverloaded);
		return amIOverloaded;
	}
	
	/*
	 * Balanced Management
	 */
	protected boolean amIBalanced(Host host) {
		boolean amIBalanced = (
				host.getCPUUtil() <= overloadedThreshold && host.getRAMUtil() <= 1.0 &&
				(host.getCPUUtil() >= underloadedThreshold || host.getRAMUtil() >= underloadedThreshold));
		LOGGER.trace("[{}]~host={}, underloadedThreshold={}, overloadedThreshold={} -> {}", 
				CommonState.getTime(), host, underloadedThreshold, overloadedThreshold, amIBalanced);
		return amIBalanced;
	}
	
	/*
	 * Underloading Management
	 */
	protected boolean amIUnderloaded(Host host) {
		boolean amIUnderloaded = (host.isAwake() && 
				(host.getCPUUtil() < underloadedThreshold && host.getRAMUtil() < underloadedThreshold));
		LOGGER.trace("[{}]~host={}, underloadedThreshold={} -> {}", 
				CommonState.getTime(), host, underloadedThreshold, amIUnderloaded);
		return amIUnderloaded;
	}
	
	/*
	 * Select VMs To Migrate
	 */
	protected List<VM> selectVMsToMigrate(Host host) {
		LOGGER.trace("[{}]~host={}", CommonState.getTime(), host);
		List<VM> vmsToMigrate = new ArrayList<VM>();
		double workloadToRecover = (host.getCPUUtil() - overloadedThreshold) * host.spec.getMaxWorkload();
		double workloadRecovered = 0;
		Comparator<VM> byUtilization = (VM vm1, VM vm2) -> Double.compare(vm1.spec.getMIPs(), vm2.spec.getMIPs());
		List<VM> vmsSortedByUtilization = host.vmList();
		vmsSortedByUtilization.sort(byUtilization);
		for (VM vm : vmsSortedByUtilization) {
			if (workloadRecovered < workloadToRecover) {
				workloadRecovered += vm.getWorkload();
				vmsToMigrate.add(vm);
			} else {
				break;
			}
		}
		LOGGER.debug("[{}]~host={}, vmsToMigrate={}", CommonState.getTime(), host, vmsToMigrate);
		return vmsToMigrate;
	}

	protected boolean canFit(Host host, VM vm) {
		double extraWorkload = vm.getWorkload();
		double extraUtil = host.getUtilForWorkload(extraWorkload);
		boolean canFit = 
				(host.getCPUUtil() + extraUtil) <= overloadedThreshold && 
				(host.getRAMUtil() + vm.getRAM() <= 1.0);
        LOGGER.trace("[{}] host={}, vm={}, canFit={}, "
        		+ "new CPUUtil={} <= overloadedThreshold={}, "
        		+ "new RAMUtil={} <= 1.0", 
        		CommonState.getTime(), host, vm, canFit, 
        		(host.getCPUUtil() + extraUtil), overloadedThreshold, 
        		(host.getRAMUtil() + vm.getRAM()));
		return canFit;
	}
	
	protected Host wakeUpAHostAndMapVMs(Host host, Stream<VM> vms) {
		Optional<Host> optionalSleepingHost = host.findSleepingHost();
		if (optionalSleepingHost.isPresent()) {
			Host sleepingHost = optionalSleepingHost.get();
			sleepingHost.wakeUp();
			LOGGER.trace("[{}]~Waking-up host={}", CommonState.getTime(), sleepingHost);
			
			vms.filter(vm -> !vmMigrationMap.keySet().contains(vm)).
				forEach(vm -> {
					mapVM(vm, sleepingHost);
				});
			
			return sleepingHost;
			
		} else {
			return null;
			
		}
	}
	
	protected void mapVM(VM vm, Host host) {
		vmMigrationMap.put(vm, host);
	}
	
	protected int numberOfMappedVMs() {
		return vmMigrationMap.size();
	}
	
	protected void clearMappedVMs() {
		vmMigrationMap.clear();
	}
	
	protected void migrateMappedVMs(Host host) {
		LOGGER.debug("[{}]~Host {} migrating vmMigrationMap={}%", 
				CommonState.getTime(), host, vmMigrationMap);
		host.migrateVMs(vmMigrationMap);
	}

	/*
	 * Agent APIs to be implemented 
	 */
	@Override
	protected boolean shouldTender(Node node) {
		Host host = (Host) node;
		boolean shouldTender = super.shouldTender(host) && host.isAwake();
	    LOGGER.trace("[{}] host={}, host.isAwake()={}, shouldTender={}",
	    		CommonState.getTime(), node, host.isAwake(), shouldTender);
		return shouldTender;
	}
	
	protected boolean shouldBid(TenderMessage<T> tenderMessage) {
		Host host = (Host)tenderMessage.to;
		
		boolean shouldBid = host.isAwake() && isIdle();
        LOGGER.trace("[{}]~contractID={}, host={}, shouldBid={}, host.isAwake={}, isIdle()={}"
        		+ "isOverloaded={}, isUnderloaded={} host.getCPUUtil={}, host.getRAMUtil={}", 
        		CommonState.getTime(), tenderMessage.contractID, host, shouldBid, host.isAwake(), 
        		isIdle(), amIOverloaded(host), amIUnderloaded(host),
        		host.getCPUUtil(), host.getRAMUtil());
		return shouldBid;
	}
		
}