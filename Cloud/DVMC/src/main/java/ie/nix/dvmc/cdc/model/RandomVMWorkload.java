package ie.nix.dvmc.cdc.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMWorkloadModel;
import ie.nix.dvmc.cdc.VM;
import peersim.core.CommonState;

public class RandomVMWorkload implements VMWorkloadModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private Map<VM, Double> workloads; 
	
	public RandomVMWorkload(String prefix) {
		workloads = new HashMap<VM, Double>();
        LOGGER.info("[{}]", CommonState.getTime());
        init();
	}
	
	@Override
	public boolean isDynamic() {
		return true;
	}
	
	@Override
	public double getWorkload(VM vm) {
        LOGGER.trace("[{}]~{} -> {}", 
        		CommonState.getTime(), vm, workloads.get(vm));
		return workloads.get(vm);
	}
	
	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
        LOGGER.trace("[{}]", CommonState.getTime());
		CloudDataCenter.getCloudDataCenter().getVMs().stream().forEach(vm -> {
			workloads.put(vm, CommonState.r.nextDouble() * vm.spec.getMIPs());
	        LOGGER.debug("[{}]~VM={} workload init->{}", CommonState.getTime(), vm, 
	        		String.format("%.2f", workloads.get(vm)));
		});
        CloudDataCenter.getCloudDataCenter().setVMWorkloadModel(this);
		return false;
	}
		
	public boolean step() {
        LOGGER.trace("[{}]", CommonState.getTime());
        Map<VM, Double> newWorkloads = new HashMap<VM, Double>();
		CloudDataCenter.getCloudDataCenter().getVMs().stream().forEach(vm -> {
			newWorkloads.put(vm, CommonState.r.nextDouble() * vm.spec.getMIPs());
	        LOGGER.debug("[{}]~VM={} workload={}->{}", CommonState.getTime(), vm, 
	        		String.format("%.2f", workloads.get(vm)), 
	        		String.format("%.2f", newWorkloads.get(vm)));
		});
		workloads = newWorkloads;
		return false;
	}
}