package ie.nix.dvmc.gc;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.gc.AwardMessage;
import ie.nix.gc.BidMessage;
import ie.nix.gc.TenderMessage;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Node;

public class GCDVMC_VMs extends GCDVMC<VM, VM> {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	protected static final String ALL_OR_NOTHING_THRESHOLD = "all_or_nothing_threshold";
	
	protected static final double DEFAULT_ALL_OR_NOTHING_THRESHOLD = 0;
	
	/* 
	 * VM Mirgarion Map
	 */
	protected int vmsMigrating;

	private double allOrNothingThreshold;
	private int tenderRetries;
	private int maxAllowedTenderRetries;
	
	public GCDVMC_VMs(String prefix) {
		super(prefix);
		this.allOrNothingThreshold = 
				Configuration.getDouble(prefix + "." + ALL_OR_NOTHING_THRESHOLD, DEFAULT_ALL_OR_NOTHING_THRESHOLD);
		this.vmsMigrating = 0;
		this.tenderRetries = 0;
		this.maxAllowedTenderRetries = 10;
		
		LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);
	}
		
	@Override
	public GCDVMC_VMs clone() {
		GCDVMC_VMs foo = null;
		foo = (GCDVMC_VMs)super.clone();
        LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}

	/*
	 * nextCycle
	 */
	@Override
	protected void preTender(Node node) {
        Host host = (Host)node;
        
    	// I'm not already trying to migrate the VMs away.
		if (amIOverloaded(host) && !isContracting()) {
			// This also catches the case for SLA Violating. 
			preOverloadedTender(host);

		} else if (amIUnderloaded(host) && !isContracting()) {
			preUnderloadedTender(host); 
			
		}
		
		// This is handy for after to know how many VM we'd planned to migrate.
		vmsMigrating = numberOfTasks();
		
	}
	
	protected void preOverloadedTender(Host host) {
		List<VM> vmsToMigrate = selectVMsToMigrate(host);
		LOGGER.debug("[{}]~overloaded host={}, {}% > {}%, vmsToMigrate={}", 
				CommonState.getTime(), host,
				String.format("%.2f", host.getCPUUtil()*100.0), 
				overloadedThreshold *100.0, 
				vmsToMigrate);
		addTasks(vmsToMigrate);
	}

	protected void preUnderloadedTender(Host host) {
		List<VM> vmsToMigrate = host.vmList();
		LOGGER.debug("[{}]~underloaded host={}, {}% < {}%, vmsToMigrate={}", 
				CommonState.getTime(), host, 
				String.format("%.2f", host.getCPUUtil()*100.0), 
				underloadedThreshold *100.0, 
				vmsToMigrate);
		addTasks(vmsToMigrate);
	}

	protected void postTender(Node node) {
        Host host = (Host)node;
		LOGGER.trace("[{}]~host={}, vmsMigrating={}, numberOfMappedVMs={}", 
			CommonState.getTime(), host, vmsMigrating, numberOfMappedVMs());

		if (hasMigratingVMs()) {
			
			double fractionMapped = fractionOfMappedVMs();
			boolean shouldRetry = false;
			
	        if (amIViolating(host)) {
	        	shouldRetry = (fractionMapped < 1.0) && wakeUpAHostAndMapVMs(host, getTasks()) != null;
				
			} else if (amIOverloaded(host)) {
				shouldRetry = (fractionMapped < 1.0);
				
			} else if (amIUnderloaded(host)) {			
				if (fractionMapped < allOrNothingThreshold) {
					// Clearing the mapped VM as not meeting the allOrNothing threshold.
					postTenderCleanUp();
					
				} else {
					shouldRetry = (fractionMapped < 1.0);
					
				}
				
			} else if (hasMigratingVMs()) {
				postTenderCleanUp();
			}
	        
			migrateMappedVMs(host);
			postTenderCleanUp();
			
			if (!host.hasVMs()) {
				LOGGER.debug("[{}]~Putting host {} to sleep, yeah!", 
						CommonState.getTime(), host);
				host.sleep();
			}
			
	        if (shouldRetry && tenderRetries < maxAllowedTenderRetries) {
				tenderRetries++;
				nextCycle(host, getProtocolID());
	        } else {
	    		tenderRetries = 0;
	        }
		}
	}

	protected void postTenderCleanUp() {
		LOGGER.debug("[{}]", CommonState.getTime());
		clearMappedVMs();
		vmsMigrating = 0;
	}

	public boolean hasMigratingVMs() {
		return vmsMigrating > 0;
	}
	
	public int numberOfMigratingVMs() {
		return vmsMigrating;
	}
	
	public double fractionOfMappedVMs() {
		return (double)numberOfMappedVMs()/numberOfMigratingVMs();
	}
	
	/******************************************************************************************
	 * 
	 * Manager APIs
	 * 
	 ******************************************************************************************/

	/*
	 * Tender Guides
	 */
	@Override
	public double getGuide(Node node, VM vm) {
		return getLinearGuide((Host)node, vm);
	}
	
	public double getLinearGuide(Host host, VM vm) {
		double cpuUtil = host.getCPUUtil();
		double guide;
		if (amIViolating(host)) {
			guide = getLinearViolatingGuide(cpuUtil);
			
		} else if (amIOverloaded(host)) {
			guide = getLinearOverloadedGuide(cpuUtil);
			
		} else if (amIUnderloaded(host)) {
			guide = getLinearUnderloadedGuide(cpuUtil);
			
		} else {
			assert(false) : "Host, "+host+", tendering even though not overloaded or underloaded";
			guide = 0d;
		}
		return guide;
	}
	
	public double getLinearViolatingGuide(double util) {
		double m = 1/(1 - overloadedThreshold);
		double b = 1- m;
		double guide = (m*util) + b;
		if (guide > 0) {
			// We add 2 as we always want an violating host to be 
			// handled before an overloaded host.
			guide += 2.0;
		}
		return guide;
	}
	
	/*
	 * When util = Th guide = 0.0, when util = 1.0, guide = 2.0;
	 */
	public double getLinearOverloadedGuide(double util) {
		double m = 1/(1 - overloadedThreshold);
		double b = 1- m;
		double guide = (m*util) + b;
		if (guide > 0) {
			// We add 1 as we always want an overloaded host to be 
			// handled before an underloaded host.
			guide += 1.0;
		}
		return guide;
	}
	
	/*
	 * When util = 0.0, guide = 1.0, 
	 */
	public double getLinearUnderloadedGuide(double util) {
		double guide = ((-1.0/underloadedThreshold)*util) + 1.0;
		return guide;
	}
	
	/*
	 * Bids
	 */
	@Override
	protected boolean shouldBid(TenderMessage<VM> tenderMessage) {
		Host host = (Host)tenderMessage.to;
		VM vm = tenderMessage.task;
		boolean canFit = canFit(host, vm);
		
		boolean shouldBid = super.shouldBid(tenderMessage) && canFit;
        LOGGER.trace("[{}]~contractID={}, host={}, canFit={}", 
        		CommonState.getTime(), tenderMessage.contractID, host, shouldBid, canFit);
		return shouldBid;
		
	}

	/*
	 * Bid Offers
	 */
	public double getOffer(TenderMessage<VM> tenderMessage) {
		return getLinearOffer(tenderMessage);
	}
	
	public double getLinearOffer(TenderMessage<VM> tenderMessage) {
		Host host = (Host)tenderMessage.to;
		VM vm = tenderMessage.task;
		double newUtil = host.getCPUUtil() + host.getUtilForWorkload(vm.getWorkload());
		double offer = (1.0/overloadedThreshold) * newUtil;
		return offer;
	}
	
	/*
	 * Awards
	 */	
	protected void award(AwardMessage<VM, VM> awardMessage) {
		VM vm = awardMessage.task;
		Host host = (Host)awardMessage.to;
		// Add an assert checking 

		if (vmsMigrating > 0) {
			mapVM(vm, host);
		} else {
			assert(false): "Sending an Award "+awardMessage+" but vmsMigrating is "+vmsMigrating;
		}
	}
	
	/* ======================================================================*/
	
	/*
	 * EcoCloud
	 */
	double shape = 3; //3;
	double beta = 1.0; //0.25;
	double alpha = 1.0; //0.25;
	
	public double getEcoCloudGuide(Node node, VM vm) {
		Host host = (Host)node;
		double cpuUtil = host.getCPUUtil();
		double guide;
		if (amIOverloaded(host)) {
			guide = getEcoCloudOverloadedGuide(cpuUtil);
			
		} else if (amIUnderloaded(host)) {
			guide = getEcoCloudUnderloadedGuide(cpuUtil);
			
		} else {
			assert(false) : "Host, "+host+", tendering even though not overloaded or underloaded";
			guide = 0d;
		}
		return guide;
	}
	
	public double getEcoCloudOverloadedGuide(double cpuUtil) {
		// (1 + ((util - 1)/(1 - Thigh)))^b
		// And added +1 because we always want an overloaded host to get precidence.
		return Math.pow(1 + ((cpuUtil - 1)/(1 - overloadedThreshold)), beta) + 1;
	}
	
	public double getEcoCloudUnderloadedGuide(double util) {
		// (1 - x/Tl)^a
		return Math.pow(1 - (util/underloadedThreshold), alpha);
	}
		
	public double getEcoCloudOffer(TenderMessage<VM> tenderMessage) {
		Host host = (Host)tenderMessage.to;
		VM vm = tenderMessage.task;
		double newUtil = host.getCPUUtil() + host.getUtilForWorkload(vm.getWorkload());
		double Mp = (Math.pow(shape, shape)/Math.pow(shape+1, shape+1)) * 
				 Math.pow(overloadedThreshold, shape+1);
		double offer = (1/Mp)*Math.pow(newUtil, shape)*(overloadedThreshold-newUtil);
		LOGGER.trace("[{}]~newUtil={}, offer={}", CommonState.getTime(), newUtil, offer);		
		return offer;
	}
	
	/*
	 * Power Delta
	 */
	public static Comparator<TenderMessage<VM>> lowestPowerDelta = 
		Comparator.comparingDouble(tender -> {
			Host host = (Host)tender.to;
			VM vm = tender.task;
			return host.getPowerDeltaForWorkload(vm.getWorkload())/vm.getWorkload();
		});	
	
 	public double getPowerDeltaOffer(TenderMessage<VM> tenderMessage) {
		Host host = (Host)tenderMessage.to;
		VM vm = tenderMessage.task;
		return host.getPowerDeltaForWorkload(vm.getWorkload());
	}
	
	protected boolean shouldAwardPowerDelta(BidMessage<VM,VM> bestBid) {
	// Calculate power reduction if VM migrated away from this host
		Host host = (Host)bestBid.to;
		VM vm = bestBid.task;
		double lessUtil =  host.getUtilForWorkload(vm.getWorkload()); 
		double newPowerConsumption = host.getPowerConsumptionForUtil(host.getCPUUtil() - lessUtil);
		double powerReduction = host.getPowerConsumption() - newPowerConsumption;

        LOGGER.trace("[{}]~contractID={}, host={}, bestBid.vm={}, "
        		+ "origionalPower={}, lessUtil={}, newPowerConsumption={},"
        		+ "powerReduction={}", 
        		CommonState.getTime(), bestBid.contractID, host, vm, 
        		host.getPowerConsumption(), lessUtil, newPowerConsumption,
        		powerReduction);
		
		// Get the power increase on the biffing host if the VM moves.
		double powerIncrease = bestBid.offer;
		
		boolean shouldAward = powerReduction >= powerIncrease;
        LOGGER.debug("[{}]~contractID={}, host={}, bestBid={}, powerReduction={}, powerIncrease={} -> {}", 
        		CommonState.getTime(), bestBid.contractID, host, bestBid, powerReduction, powerIncrease, shouldAward);
        return shouldAward;
	}
}