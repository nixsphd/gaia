package ie.nix.dvmc.gc;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.ToDoubleFunction;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import ie.nix.dvmc.gc.GCDVMC_Resources.Resources;
import ie.nix.gc.AwardMessage;
import ie.nix.gc.BidMessage;
import ie.nix.gc.Callback;
import ie.nix.gc.TenderMessage;
import peersim.core.CommonState;
import peersim.core.Node;

public class GCDVMC_Resources extends GCDVMC<Resources, List<VM>> {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class Resources {

		public static double DOESNT_FIT = -1;
		
		public static Resources getResources(List<VM> vms) {
			double cpu = vms.stream().mapToDouble(vm -> vm.getWorkload()).sum();
			double ram = vms.stream().mapToDouble(vm -> vm.getRAM()).sum();
			return new Resources(cpu, ram);
		}
		
		private final double cpu;
		private final double ram;
		
		public Resources(double cpu, double ram) {
			this.cpu = cpu;
			this.ram = ram;
		}
		
		@Override
		public String toString() {
			return "Resources [cpu=" + cpu + ", ram=" + ram + "]";
		}
		
		@Override
		public boolean equals(Object object) {
			if (object instanceof Resources) {
				Resources resources = (Resources)object;
				return resources.cpu == cpu && resources.ram == ram;
			} else {
				return false;
			}
		}

		public double getCPUResources() {
			return cpu;
		}

		public double getRAMResources() {
			return ram;
		}

		public boolean isNotNothing() {
			return cpu > 0 && ram > 0;
		}
		
		// Unit tested
		public boolean canFit(Resources resources) {
			return fit(resources) != DOESNT_FIT;
		}
		
		public boolean canFitInto(Resources resources) {
			return resources.fit(this) != DOESNT_FIT;
		}
		
		public boolean canFit(List<VM> vms) {
			return canFit(getResources(vms));
		}
		
		// Unit tested
		public double fit(Resources resources) {
			double fitCPU = resources.cpu/cpu;
			double fitRAM = resources.ram/ram;
			double fit = (fitCPU > 1 || fitRAM > 1) ? DOESNT_FIT : fitCPU * fitRAM;
			return fit;
		}
		
	}
	
	protected double yAt(double x, double xAtYis0, double xAtYis1) {
		// slope = (y1 − y2) / (x1 − x2) = (1) / (x1=xAtYis1 - x2=xAtYis0)
        double m = 1d / (xAtYis1 - xAtYis0);
        // y = m(x − x1) + y1
		double guide = (m * (x - xAtYis0));
		return guide;
	}
	
	protected static ToDoubleFunction<TenderMessage<Resources>> tenderFit = (tenderMessage -> {
		Resources availableResources = tenderMessage.task;
		Resources resources = Resources.getResources(((Host)tenderMessage.to).vmList());
		double fit = availableResources.fit(resources);
        LOGGER.trace("[{}]~availableResources={}, resources={}, fit={}", 
        		CommonState.getTime(), availableResources, resources, fit);
		return fit;
	});
	
	protected static ToDoubleFunction<TenderMessage<Resources>> tenderGuideFit = (tenderMessage -> {
		double fit = tenderFit.applyAsDouble(tenderMessage);
		double guide = tenderMessage.guide;
		double guideFit =  fit != Resources.DOESNT_FIT ? fit * guide : fit;
        LOGGER.trace("[{}]~tenderMessage={}, fit={}, guide={} -> {}", 
        		CommonState.getTime(), tenderMessage, fit, guide, guideFit);
		return guideFit;
	});
	
	protected static ToDoubleFunction<BidMessage<Resources,List<VM>>> bidFit = (bidMessage -> {
		Resources availableResources = bidMessage.task;
		Resources proposedVMsResources = Resources.getResources(bidMessage.proposal);
		double fit = availableResources.fit(proposedVMsResources);
        LOGGER.trace("[{}]~availableResources={}, proposedVMsResources={}, fit={}", 
        		CommonState.getTime(), availableResources, proposedVMsResources, fit);
		return fit;
	});
	
	protected static ToDoubleFunction<BidMessage<Resources,List<VM>>> bifOfferFit = (bidMessage -> {
		double fit = bidFit.applyAsDouble(bidMessage);
		double offer = bidMessage.offer;
		double offerFit =  fit != Resources.DOESNT_FIT ? fit * offer : fit;
        LOGGER.trace("[{}]~bidMessage={}, fit={}, offer={} -> {}", 
        		CommonState.getTime(), bidMessage, fit, offer, offerFit);
		return offerFit;
	});
	
	// Unit tested
	protected static Comparator<TenderMessage<Resources>> bestFitTenderComparator = 
		Comparator.comparingDouble(tenderFit).reversed();
	
	// Unit tested
	protected static Comparator<TenderMessage<Resources>> bestFitByGuideTenderComparator = 
		Comparator.comparingDouble(tenderGuideFit).reversed();
	
	// Unit tested
	protected static Comparator<BidMessage<Resources, List<VM>>> bestFitBidComparator = 
		Comparator.comparingDouble(bidFit).reversed();
	
	// Unit tested
	protected static Comparator<BidMessage<Resources, List<VM>>> bestFitByOfferBidComparator = 
		Comparator.comparingDouble(bifOfferFit).reversed();
	
	public GCDVMC_Resources(String prefix) {
		super(prefix);
		LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);
	}
		
	@Override
	public GCDVMC_Resources clone() {
		GCDVMC_Resources foo = null;
		foo = (GCDVMC_Resources)super.clone();
		LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}

	/*
	 * Tendering
	 */
	@Override
	protected void preTender(Node node) {
        LOGGER.trace("[{}]~node={}", CommonState.getTime(), node);
        Host host = (Host)node;
		if (isIdle()) {
			if (!host.hasVMs()) {
				host.sleep();
				LOGGER.debug("[{}]~{} going to sleep.", CommonState.getTime(), host);
				
			} else if (amIViolating(host)) {
	        	Host wokenHost = wakeUpAHostAndMapVMs(host, selectVMsToMigrate(host).stream());
	        	Optional.ofNullable(wokenHost).ifPresent(sucessfullyWokenHost -> {
	        		LOGGER.debug("[{}]~{} waking up a host {}.", 
	        				CommonState.getTime(), host, sucessfullyWokenHost);
		    		migrateMappedVMs(host);
	        	});
	    		clearMappedVMs();
			}
		}
		
	}
        
//	@Override
//	protected void postTender(Node node) {
//		LOGGER.trace("[{}]~node={}", CommonState.getTime(), node);
//		clearTasks();
//	}
	
	@Override
	protected boolean shouldTender(Node node) {
        Host host = (Host)node;
    	Resources availableResources = getAvailableResources(host);
    	
		boolean isIdle = isIdle();
		boolean amIBalanced = amIBalanced(host);
		boolean availableResourcesIsNotNothing = availableResources.isNotNothing();
		boolean tenderTrial = tenderTrial(host, availableResources);
		boolean shouldTender = isIdle && amIBalanced && 
				availableResourcesIsNotNothing && tenderTrial;
		if (shouldTender) {
        	addTask(availableResources);
		}
        LOGGER.debug("[{}]~host={} isIdle={}, amIBalanced={}, shouldTender={}, availableResources={}", 
        		CommonState.getTime(), host, isIdle, amIBalanced, shouldTender, availableResources);
		return shouldTender;
	}

	// Unit tested
	protected Resources getAvailableResources(Host host) {
		double availableCPU = 
				(overloadedThreshold - host.getCPUUtil()) * host.spec.getMaxWorkload();
		double availableRAM = 
				(1d - host.getRAMUtil()) * host.spec.getMemory();
		Resources availableResources = new Resources(availableCPU, availableRAM);
		return availableResources;
	}
	
	// Unit tested
	@Override
	public double getGuide(Node node, Resources task) {
        Host host = (Host)node;
        double highestResourceUtil = Math.max(host.getCPUUtil(), host.getRAMUtil());
        double guide = yAt(highestResourceUtil, 0, overloadedThreshold);
		return guide;
	}
	
	/*
	 * Bidding
	 */
	@Override
	protected Comparator<? super TenderMessage<Resources>> getTenderComparator() {
		return TenderMessage.highestGuide;
	}
	
	@Override
	protected boolean shouldBid(TenderMessage<Resources> tenderMessage) {
		Host host = (Host)tenderMessage.to;
		
		boolean isAwake = host.isAwake();
		boolean isIdle = isIdle();
		boolean shouldBid = isAwake && isIdle;
		
		if (shouldBid && amIBalanced(host)) {
			
			boolean canFit = ((Resources)tenderMessage.task).canFit(host.vmList());
			boolean bidTrial = bidTrial(tenderMessage);
			shouldBid &= canFit && bidTrial;
	        LOGGER.debug("[{}]~tenderMessage={}, underloaded or balanced, isAwake={}, isIdle={}, "
	        		+ "canFit={}, bidTrial={} -> {}", 
	        		CommonState.getTime(), tenderMessage, isAwake, isIdle, canFit, bidTrial, shouldBid);
	        
		} else if (shouldBid && (amIOverloaded(host) || amIViolating(host))) {
			
			boolean canFit = ((Resources)tenderMessage.task).canFit(selectVMsToMigrate(host));
			shouldBid &= canFit;
	        LOGGER.debug("[{}]~tenderMessage={}, overloaded or violating, isAwake={}, isIdle={}, "
	        		+ "canFit={} -> {}", 
	        		CommonState.getTime(), tenderMessage, isAwake, isIdle, canFit, shouldBid);
		}
		
		return shouldBid;
		
	}
	
	// Unit tested
	@Override
	public double getOffer(TenderMessage<Resources> tenderMessage) {
        Host host = (Host)tenderMessage.to;
        double mostAbundantResourceUtil = Math.max(host.getCPUUtil(), host.getRAMUtil());
        double offer = yAt(mostAbundantResourceUtil, overloadedThreshold, 0);
	    if (amIOverloaded(host)) {
	    	offer += 1.0;
		} else if (amIViolating(host)) {
	    	offer += 2.0;
		}
		return offer;
	}
	
	@Override
	public List<VM> getProposal(TenderMessage<Resources> tenderMessage) {
		Host host = (Host)tenderMessage.to;
		if (amIBalanced(host)) {
			List<VM> proposal = host.vmList();
		    LOGGER.trace("[{}]~tenderMessage={}, proposal={}", 
		    		CommonState.getTime(), tenderMessage, proposal);
			return proposal;
		} else {
			List<VM> proposal = selectVMsToMigrate(host);
		    LOGGER.trace("[{}]~tenderMessage={}, proposal={}", 
		    		CommonState.getTime(), tenderMessage, proposal);
			return proposal;
			
		}
	}
	
	/*
	 * Awarding & being Awarded
	 */	
	@Override
	protected Comparator<? super BidMessage<Resources, List<VM>>> getBidComparator() {
		return BidMessage.highestOffer;
	}
	
	@Override
	protected boolean shouldAward(BidMessage<Resources,List<VM>> bidMessage) {
		boolean canFit = ((Resources)bidMessage.task).canFit(bidMessage.proposal);
	    LOGGER.debug("[{}]~host={}, canFit={}", CommonState.getTime(), bidMessage.to, canFit);
		return canFit;
	}
	
	@Override
	protected void awarded(AwardMessage<Resources,List<VM>> awardMessage) {

		// Carry out the migration
		Host host = (Host)awardMessage.to;
		// The host to migrate to
		Host migrateTo = (Host)awardMessage.from;

		// The migrating VMs are added to the map
		awardMessage.proposal.stream().forEach(vm -> {
			assert(host.hasVM(vm)): 
				"Trying to map a VM "+vm+" but it's not currently in this host "+host;
			mapVM(vm, migrateTo);
		});
		
		migrateMappedVMs(host);
		clearMappedVMs();
		
		// After the award has expired assuming the host is idle, put it to sleep.
		long afterAwardExpirationTime = (awardMessage.expirationTime + 2) - CommonState.getTime();
		addCallback(afterAwardExpirationTime, new Callback(() -> {
			if (!host.hasVMs() && isIdle()) {
				host.sleep();
				LOGGER.debug("[{}]~{} going to sleep.", CommonState.getTime(), host);
			} 
		}), awardMessage.to);
		
		
	}
	
	/**********************************************************************************************************
	 * Weighted Guide and Offer Comparators
	 **********************************************************************************************************/
	
	private static final double guideWeight = 1.0;
	private static final double offerWeight = 1.0;
	
	protected static ToDoubleFunction<TenderMessage<Resources>> tenderWeightedGuideFit = (tenderMessage -> {
		double fit = tenderFit.applyAsDouble(tenderMessage);
		double guide = tenderMessage.guide;
		double guideFit =  fit == Resources.DOESNT_FIT ? Resources.DOESNT_FIT  : fit + (guideWeight * guide);
        LOGGER.trace("[{}]~tenderMessage={}, fit={}, guide={}, guideWeight={} -> {}", 
        		CommonState.getTime(), tenderMessage, fit, guide, guideWeight, guideFit);
		return guideFit;
	});
	
	protected static ToDoubleFunction<BidMessage<Resources,List<VM>>> bidWeightedOfferFit = (bidMessage -> {
		double fit = bidFit.applyAsDouble(bidMessage);
		double offer = bidMessage.offer;
		double offerFit =  fit == Resources.DOESNT_FIT ? Resources.DOESNT_FIT : fit + (offerWeight * offer);
        LOGGER.trace("[{}]~bidMessage={}, fit={}, offer={}, offerWeight={} -> {}", 
        		CommonState.getTime(), bidMessage, fit, offer, offerWeight, offerFit);
		return offerFit;
	});
	
	// TODO - Unit test
	protected static Comparator<TenderMessage<Resources>> bestFitPlusWeightedGuideTenderComparator = 
		Comparator.comparingDouble(tenderWeightedGuideFit).reversed();
			
	// TODO - Unit test
	protected static Comparator<BidMessage<Resources, List<VM>>> bestFitPlusWeightedOfferBidComparator = 
		Comparator.comparingDouble(bidWeightedOfferFit).reversed();
	
	/**********************************************************************************************************
	 * Tender and bid trials
	 **********************************************************************************************************/
	
	private boolean tenderTrial(Host host, Resources availableResources) {
		double guide = getGuide(host, availableResources);
		boolean tenderTrial = guide >= CommonState.r.nextDouble();
        LOGGER.trace("[{}]~host={}, availableResources={}, tenderTrial={}", 
        		CommonState.getTime(), host, availableResources, tenderTrial);
		return tenderTrial;
	}
	
	private boolean bidTrial(TenderMessage<Resources> tenderMessage) {
		double offer = getOffer(tenderMessage);
		boolean bidTrial = offer >= CommonState.r.nextDouble();
        LOGGER.trace("[{}]~host={}, tenderMessage={}, bidTrial={}", 
        		CommonState.getTime(), tenderMessage.to, tenderMessage, bidTrial);
		return bidTrial;
	}	
	
}