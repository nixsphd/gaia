package ie.nix.dvmc.cdc.observer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.util.DataFrame;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;

public abstract class Observer implements Control {
	
	private static final Logger LOGGER = LogManager.getLogger();

	public static final String LOG_DIR = "results-dir";
	public static final String LOG_FILE = "csv-file";
	public static final String STEP = "step";
	public static final String LOG_STEP = "log_step";
	
	public static final Map<String, Observer> observers = new HashMap<String, Observer>();

	protected String logFile;
	protected int observeStep;
	protected int log_step;
	protected DataFrame df;
	protected Supplier<Stream<String>> getHeaders;
	protected Supplier<Stream<String>> getRow;

	public Observer(String prefix) {
        Path logDirPath = FileSystems.getDefault().getPath(
        		Configuration.getString(prefix + "." + LOG_DIR, Configuration.getString(LOG_DIR, ".")));
        if (!Files.exists(logDirPath)) {
        	try {
				Files.createDirectories(logDirPath);
		        LOGGER.trace("[{}]~Created {}", CommonState.getTime(), logDirPath);
			} catch (IOException e) {
		        LOGGER.error("[{}]~Error creating logfile path {}", CommonState.getTime(), logDirPath, e);
			}
        }
        this.logFile = logDirPath.resolve(Configuration.getString(prefix + "." + LOG_FILE, getName() + ".csv")).toString();
        
        this.observeStep = Configuration.getInt(prefix + "." + STEP);
		this.log_step = Configuration.getInt(prefix + "." + LOG_STEP, this.observeStep);
		
        LOGGER.info("[{}]~Logging to {}", CommonState.getTime(), logFile);
        
        // Add the observers to make them accessible for testing.
        observers.put(getName(), this);
	}
	
	public final String getName() {
		return this.getClass().getSimpleName();
	}

	public void writeHeaders(Stream<String> headers) {
		try {
			df = new DataFrame(headers);
			df.setWriter(new PrintWriter(logFile));
			df.writeHeaders();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void writeRow(Stream<String> row) {
		df.addRow(row);
		df.writeLastRow();
	}
	
	public void writeRows(Stream<Object> row) {
		df.addRow(row.map(i -> i.toString()));
		df.writeLastRow();
	}
	
	@Override
	public final boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
        LOGGER.debug("[{}]", CommonState.getTime());
		observe(CloudDataCenter.getCloudDataCenter());
		return false;
	}

	public boolean step() {
        LOGGER.debug("[{}]", CommonState.getTime());
		observe(CloudDataCenter.getCloudDataCenter());
		return false;
	}
	
	public abstract void observe(CloudDataCenter cdc);

	public boolean isLogTime() {
		return CommonState.getTime() % log_step == 0;
	}
}