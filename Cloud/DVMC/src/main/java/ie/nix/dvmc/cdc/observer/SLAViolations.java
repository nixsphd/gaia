package ie.nix.dvmc.cdc.observer;

import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import peersim.core.CommonState;

public class SLAViolations extends Observer {


	private static final Logger LOGGER = LogManager.getLogger();

	protected int observations;
	
	protected long numberOfSlaViolations;
	protected DoubleSummaryStatistics availabilityStatistics;
	
	public SLAViolations(String prefix) {
		super(prefix);
		numberOfSlaViolations = 0;
		availabilityStatistics = new DoubleSummaryStatistics();
		writeHeaders(Stream.of("time", "Number of SLA X Violations", "Availability"));
	}

	public void observe(CloudDataCenter cdc) {
        LOGGER.trace("[{}]", CommonState.getTime());

        List<Host> overloadedHosts = cdc.getHosts().stream().filter(host -> host.getCPUUtil() > 1.0).
        		collect(Collectors.toList());
        
        numberOfSlaViolations += overloadedHosts.size();
        
		int numberOfHosts = cdc.getHosts().size();
		availabilityStatistics.accept((100d * (numberOfHosts - overloadedHosts.size())) / numberOfHosts);
        
		if (isLogTime()) {
			writeRows(Stream.of(CommonState.getTime(), numberOfSlaViolations*observeStep, 
					availabilityStatistics.getAverage()));
	        LOGGER.info("[{}]~numberOfSlaViolations={}, availability={}", CommonState.getTime(), 
	        		numberOfSlaViolations*observeStep, availabilityStatistics.getAverage());
	        numberOfSlaViolations = 0;
	        availabilityStatistics = new DoubleSummaryStatistics();
		}
	}
}
