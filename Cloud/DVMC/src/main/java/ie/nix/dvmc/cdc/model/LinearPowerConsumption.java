package ie.nix.dvmc.cdc.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter.HostPowerModel;
import ie.nix.dvmc.cdc.Host;
import peersim.core.CommonState;

public class LinearPowerConsumption extends CSVFileControl implements HostPowerModel {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private final Map<String, double[]> hostPowerConsumptionMap;
	private final int numberOfDataPoints;

	public LinearPowerConsumption(String prefix) {
		this(prefix, "Name");
	}
	
	public LinearPowerConsumption(String prefix, String header) {
		super(prefix);
		hostPowerConsumptionMap = new HashMap<String, double[]>();
		numberOfDataPoints = df.getNumberOfColumns() - 1;

		for (int row = 0; row < df.getNumberOfRows(); row++) {
			String hostSpecName = removeQuotes((String)df.getData(row, header)); 
			double[] hostPowerConsumption = new double[df.getNumberOfColumns()-1];
			for (int column = 1; column < df.getNumberOfColumns(); column++) {
				hostPowerConsumption[column-1] = Double.valueOf(df.getData(row, column).toString());
			}
			hostPowerConsumptionMap.put(hostSpecName, hostPowerConsumption);
	        LOGGER.debug("[{}] hostSpecName={}, hostPowerConsumption={}, numberOfDataPoints={}", 
	        		CommonState.getTime(), hostSpecName, hostPowerConsumption, numberOfDataPoints);
		}
		
	}

	public double powerConsumption(Host host, double utilization) {
		double clamperUtilization = clamp(utilization, 0.0, 1.0);
		int topBin = getTopBin(clamperUtilization);
		int bottomBin = getBottomBin(clamperUtilization);
		double topBinPower = getBinPower(host, topBin);
		double bottomBinPower = getBinPower(host, bottomBin);
		LOGGER.trace("[{}]~clamperUtilization={}, bin {}->{} to binPower {}->{}", 
        		CommonState.getTime(), clamperUtilization, bottomBin, topBin, bottomBinPower, topBinPower);
		
		double utilPerBin = 1.0/(numberOfDataPoints-1);
		double utilInBin = clamperUtilization - (utilPerBin * bottomBin);
		double powerInBin = (utilInBin / utilPerBin) * (topBinPower - bottomBinPower);
		double powerConsumption = bottomBinPower + powerInBin;
		
		LOGGER.trace("[{}]~utilPerBin={}, utilInBin={}, powerInBin={}, powerConsumption={}", 
				CommonState.getTime(), utilPerBin, utilInBin,  powerInBin, powerConsumption);
		
		return powerConsumption;
	}

	private double clamp(double value, double min, double max) {
		return Math.max(min, Math.min(value, max));
	}

	private int getTopBin(double utilization) {
		int topBin = (int)Math.ceil(utilization * (numberOfDataPoints-1));
		LOGGER.trace("[{}]~utilization={}, topBin={}", 
				CommonState.getTime(), utilization, topBin);
		return topBin;
	}
	
	private int getBottomBin(double utilization) {
		int bottomBin = (int)Math.floor(utilization * (numberOfDataPoints-1));
		LOGGER.trace("[{}]~utilization={}, bottomBin={}",
				CommonState.getTime(), utilization, bottomBin);
		return bottomBin;
	}
	
	private double getBinPower(Host host, int bin) {
		return hostPowerConsumptionMap.get(host.spec.getName())[bin];
	}

	@Override
	public boolean execute() {
	    LOGGER.trace("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
        LOGGER.trace("[{}]", CommonState.getTime());
		return false;
	}
		
	public boolean step() { return false; }

}