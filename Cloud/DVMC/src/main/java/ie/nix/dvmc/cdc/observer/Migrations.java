package ie.nix.dvmc.cdc.observer;

import java.util.List;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import peersim.core.CommonState;

public class Migrations extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public Migrations(String prefix) {
		super(prefix);
		writeHeaders(Stream.of("time", "migrations", "hosts asleep", "migration efficiency"));
	}

	public void observe(CloudDataCenter cdc) { 
        LOGGER.debug("[{}]", CommonState.getTime());
        List<Host> hosts = CloudDataCenter.getCloudDataCenter().getHosts();
        
        // We need to know which hosts are asleep and has vms migrated away. Some of the hosts might never have
        // had any vms and counting this would give a mirgation efficiency > 1.0
        long numberOfHostsAsleep = hosts.stream().filter(host -> !host.isAwake()).
        		mapToInt(host -> (host.getVmMigrationModel().getNumberOfMigrations() > 0) ? 1 : 0).sum();
        long migrations = hosts.stream().
        		mapToInt(host -> host.getVmMigrationModel().getNumberOfMigrations()).sum();
        if (migrations > 0) {
			double migrationEfficiency = ((double)numberOfHostsAsleep) / migrations;
			writeRows(Stream.of(CommonState.getTime(), migrations, numberOfHostsAsleep, migrationEfficiency));
	        LOGGER.info("[{}]~migrations={}, numberOfHostsAsleep={}, migrationEfficiency={}", 
	        		CommonState.getTime(), migrations, numberOfHostsAsleep, 
	        		String.format("%.2f", migrationEfficiency));
        } else {
			writeRows(Stream.of(CommonState.getTime(), migrations, numberOfHostsAsleep, 0));
	        LOGGER.info("[{}]~migrations={}, numberOfHostsAsleep={}, migrationEfficiency=0", 
	        		CommonState.getTime(), numberOfHostsAsleep, migrations);
        }

	}

}
