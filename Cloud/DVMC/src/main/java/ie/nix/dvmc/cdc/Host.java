package ie.nix.dvmc.cdc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter.HostSleepModel;
import ie.nix.dvmc.cdc.CloudDataCenter.VMMigrationModel;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.GeneralNode;

public class Host extends GeneralNode {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class HostSpec {

		// Name
		protected final String name;

		// Number of Cores
		protected final int cores;

		// Simulated CPU Performance Counters (GHz)
		protected final double cpu;

		// MIPS (= millions of instructions per second)
		protected final double mips;
		
		// RAM (GB)
		protected final double ram;
		
		public HostSpec(String name, int cores, double cpu) {
			this(name, cores, cpu, 0);
		}
		
		public HostSpec(String name, int cores, double cpu, double ram) {
			// double discStorage, double networkBandwidth,
			super();
			this.name = name;
			this.cores = cores;
			this.cpu = cpu;
			this.mips = cores * (cpu * 1000);
			this.ram = ram;
			
	        LOGGER.trace("[{}]", CommonState.getTime());

		}

		@Override
		public String toString() {
			return name;
		}

		public String getName() {
			return name;
		}
		/*
		 * Returns the total workload in MIPS
		 */
		public double getMaxWorkload() {
			return cores * getMaxWorkloadPerCore();
		}

		public double getMaxWorkloadPerCore() {
			return cpu * 1000;
		}

		public double getMemory() {
			return ram;
		}

		public int getCores() {
			return cores;
		}
	}
	
	private static final String VM_MIGRATION_PROTOCOL = "vm_migration";
	
	private static final String SLEEP_PROTOCOL = "host_sleep";
	
	// Use to add parameters
	public static String prefix = "host";

	// The VMs on the host
	private final ArrayList<VM> vms;

	// Models of protocol that can move VMs from host to host.
	private int vmMigrationProtocolID;
	
	// Models of protocol that can move VMs from host to host.
	private int sleepProtocolID;

	// The specification of the host
	public HostSpec spec;

	// The failure rate of the hardware which related to the age of the hardware, we
	// assume hardware is 2 years old with a failure rate of 6% per year, or 0.016
	// per day
	// https://www.statista.com/statistics/430769/annual-failure-rates-of-servers/
	// protected double failureRatePerDay = 0.016;
	protected double failureRatePerDay = 0; // Never die is 0!

	public Host(String prefix) {
		super(prefix);
		this.vms = new ArrayList<VM>();
		this.vmMigrationProtocolID = Configuration.lookupPid(VM_MIGRATION_PROTOCOL);
		this.sleepProtocolID = Configuration.lookupPid(SLEEP_PROTOCOL);
        LOGGER.trace("[{}]", CommonState.getTime());
	}

	public Host(HostSpec spec) {
		this(prefix);
		this.spec = spec;
        LOGGER.trace("[{}]~{}", CommonState.getTime(), spec);

	}

	@Override
	public Host clone() {
		Host foo = null;
		foo = (Host) super.clone();
		foo.vms.clear();
        LOGGER.trace("[{}]~{}", CommonState.getTime(), spec);
		return foo;
	}

	@Override
	public String toString() {
		return spec + "-" +  + this.getIndex()
				+ " ["
				+ "cpu=" + String.format("%.2f", getCPUUtil())  + ", "
				+ "ram=" + String.format("%.2f", getRAMUtil()) + ", " 
				+ "vms=" + getNumberOfVMs() + "]";
	}

	public double getFailureRatePerDay() {
		return failureRatePerDay;
	}

	/*
	 * VM management APIs
	 */
	public boolean hasVMs() {
		return !vms.isEmpty();
	}

	/*
	 * Returns a list of the VMs on a host.
	 */
	@SuppressWarnings("unchecked")
	public List<VM> vmList() {
		return (List<VM>) vms.clone();
	}

	public int getNumberOfVMs() {
		return vms.size();
	}

	public Stream<VM> vmStream() {
		return vms.stream();
	}

	public boolean hasVM(VM vm) {
		return vms.contains(vm);
	}

	/*
	 * We can fit the VM if we have enough CPUs and enough RAM.
	 */
	public boolean canFitVM(VM newVM) {
		double newCPUUtil = vmStream().mapToDouble(v -> v.spec.getMIPs()).sum() + newVM.spec.getMIPs();
		double newRAMUtil = vmStream().mapToDouble(v -> v.spec.getRam()).sum() + newVM.spec.getRam();
		boolean canFit = (newCPUUtil <= spec.getMaxWorkload()) && (newRAMUtil <= spec.getMemory());
        LOGGER.trace("[{}]~Host={} newVM={}, newCPUUtil={}, newRAMUtil={}, canFit={}", 
        		CommonState.getTime(), this, newVM, newCPUUtil, newRAMUtil, canFit);
		return canFit;
	}
	
	/*
	 * We can fit all the VMs if we have enough CPUs and enough RAM.
	 */
	public boolean canFitVMs(List<VM> newVMs) {
        LOGGER.trace("[{}]~Host-{} newVMs={}", CommonState.getTime(), getID(), newVMs);
		return ((vmStream().mapToDouble(v -> v.spec.getMIPs()).sum()
					+ newVMs.stream().mapToDouble(v -> v.spec.getMIPs()).sum())
					<= (spec.getMaxWorkload())) &&
			   ((vmStream().mapToDouble(v -> v.spec.getRam()).sum() 
					+ newVMs.stream().mapToDouble(v -> v.spec.getRam()).sum())
					<= (spec.getMemory()));
	}
	
	/*
	 * We can fit the VM if we have enough CPUs and enough RAM.
	 */
	public boolean canFitVMWorkload(VM newVM) {
		return canFitVMWorkload(newVM, 1.0);
	}
	
	public boolean canFitVMWorkload(VM newVM, double threshold) {
		double newCPUUtil = vmStream().mapToDouble(v -> v.getWorkload()).sum() + newVM.getWorkload();
		double newRAMUtil = vmStream().mapToDouble(v -> v.spec.getRam()).sum() + newVM.spec.getRam();
		boolean canFit = (newCPUUtil <= (threshold * spec.getMaxWorkload())) && (newRAMUtil <= spec.getMemory());
        LOGGER.trace("[{}]~Host={} newVM={}, threshold={}, newCPUUtil={}, newRAMUtil={}, canFit={}", 
        		CommonState.getTime(), this, newVM, threshold, newCPUUtil, newRAMUtil, canFit);
		return canFit;
	}
	
	/*
	 * We can fit all the VMs if we have enough CPUs and enough RAM.
	 */
	public boolean canFitVMsWorkloads(List<VM> newVMs) {
        LOGGER.trace("[{}]~Host-{} newVMs={}", CommonState.getTime(), getID(), newVMs);
		return ((vmStream().mapToDouble(v -> v.getWorkload()).sum()
					+ newVMs.stream().mapToDouble(v -> v.getWorkload()).sum())
					<= (spec.getMaxWorkload())) &&
			   ((vmStream().mapToDouble(v -> v.spec.getRam()).sum() 
					+ newVMs.stream().mapToDouble(v -> v.spec.getRam()).sum())
					<= (spec.getMemory()));
	}

	/*
	 * Add a new VM.
	 */
	public void addVM(VM vm) {
        LOGGER.trace("[{}]~Host-{} vm={}, origionalHost={}", 
        		CommonState.getTime(), this.getIndex(), vm, vm.getHost());
		assert(!hasVM(vm)) : 
			"The host"+this+" already has the vm "+vm+" so can't add it again!";
		vms.add(vm);
		vm.setHost(this);
	}

	/*
	 * Remove a VM.
	 */
	public void removeVM(VM vm) {
        LOGGER.trace("[{}]~Host={} vm={}", CommonState.getTime(), this, vm);
		assert(hasVM(vm)) : 
			"The host "+this+" does not have the vm "+vm+" so can't remove it!";
		vms.remove(vm);
		vm.setHost(null);
	}
	
	/*
	 * Host Awake / Sleep protocol
	 */
	public boolean isAwake() {
		return getSleepModel().isAwake(this);
	}

	public void sleep() {
		getSleepModel().sleep(this);
        LOGGER.trace("[{}]~Host-{}", CommonState.getTime(), getID());

	}

	public void wakeUp() {
		getSleepModel().wakeUp(this);
        LOGGER.trace("[{}]~Host-{}", CommonState.getTime(), getID());

	}

	public Optional<Host> findSleepingHost() {
		return getSleepModel().findSleepingHost(this);
	}

	public HostSleepModel getSleepModel() {
		return (HostSleepModel)getProtocol(sleepProtocolID);
	}

	/*
	 * VM Migrations APIs
	 */
	public void migrateVM(VM vm, Host host) {
		getVmMigrationModel().migrateVM(this, vm, host);
	}
	
	public void migrateVMs(Map<VM, Host> vmMigrationMap) {
		getVmMigrationModel().migrateVMs(this, vmMigrationMap);
	}

	public VMMigrationModel getVmMigrationModel() {
		return (VMMigrationModel)getProtocol(vmMigrationProtocolID);
	}

	/*
	 * CPU Utilization APIs
	 */
	public double getCPUUsed() {
		double totalVMWorkload = vms.stream().mapToDouble(vm -> vm.getWorkload()).sum();
        LOGGER.trace("[{}]~Host={} -> {}", CommonState.getTime(), 
        		getIndex(), totalVMWorkload);
		return totalVMWorkload;
	}
	
	public double getCPUUtil() {
		return getUtilForWorkload(getCPUUsed());
	}

	public double getUtilForWorkload(double workload) {
		double utilForWorkload = workload / spec.mips;
	    LOGGER.trace("[{}]~Host={} {} -> {}", CommonState.getTime(), 
	    		getIndex(), workload, utilForWorkload);
		return utilForWorkload;
	}
	
	/*
	 * RAM Utilization APIs
	 */
	public double getRAMUsed() {
		double totalVMRAM = vms.stream().mapToDouble(vm -> vm.getRAM()).sum();
        LOGGER.trace("[{}]~Host={} -> {}", 
        		CommonState.getTime(), getIndex(), totalVMRAM);
		return totalVMRAM;
	}
	
	public double getRAMUtil() {
		double totalVMRAM = vms.stream().mapToDouble(vm -> vm.getRAM()).sum();
        LOGGER.trace("[{}]~Host={} -> {}", 
        		CommonState.getTime(), getIndex(), totalVMRAM);
		return getUtilForRAM(getRAMUsed());
	}

	public double getUtilForRAM(double ram) {
		double utilForRAM = ram / spec.ram;
	    LOGGER.trace("[{}]~Host={} {} -> {}", 
	    		CommonState.getTime(), getIndex(), ram, utilForRAM);
		return utilForRAM;
	}

	/*
	 * Power Consumption
	 */
	public double getPowerConsumption() {
		double powerConsumption = 0; 
		if (isAwake()) {
			powerConsumption = getPowerConsumptionForUtil(getCPUUtil());
		}
        LOGGER.trace("[{}]~Host-{} -> {}", CommonState.getTime(), getID(), powerConsumption);
		return powerConsumption;
	}

	public double getPowerConsumptionForUtil(double utilization) {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		double powerConsumption = cdc.getHostPowerModel().powerConsumption(this, utilization);
	    LOGGER.trace("[{}]~Host-{} {} -> {}", CommonState.getTime(), getID(), utilization, powerConsumption);
		return powerConsumption;
	}

	public double getPowerConsumptionForWorkload(double workload) {
		double powerConsumption = getPowerConsumptionForUtil(getUtilForWorkload(workload));
	    LOGGER.trace("[{}]~Host-{} {} -> {}", CommonState.getTime(), getID(), workload, powerConsumption);
		return powerConsumption;
	}

	public double getPowerDeltaForWorkload(double workload) {
		double newPowerConsumption = getPowerConsumptionForUtil(getCPUUtil() + getUtilForWorkload(workload));
		return newPowerConsumption - getPowerConsumption();
	}

}
