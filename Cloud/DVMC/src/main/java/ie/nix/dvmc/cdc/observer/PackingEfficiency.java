package ie.nix.dvmc.cdc.observer;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import peersim.core.CommonState;

public class PackingEfficiency extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public PackingEfficiency(String prefix) {
		super(prefix);
		writeHeaders(Stream.of("time", "awake", "asleep", "total"));
	}

	public void observe(CloudDataCenter cdc) {
        LOGGER.debug("[{}]", CommonState.getTime());

		long awake = cdc.getHosts().stream().filter(host -> host.isAwake()).count();
		long asleep = cdc.getHosts().size() - awake;
		writeRows(Stream.of(CommonState.getTime(), awake, asleep, cdc.getHosts().size()));
        LOGGER.info("[{}]~awake={}, asleep={}", CommonState.getTime(), awake, asleep);

	}

}
