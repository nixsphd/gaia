package ie.nix.dvmc.sercon;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;

public class Sercon implements Control {

	private static final Logger LOGGER = LogManager.getLogger();

	protected static final String MAX_NUMBER_OF_MIGRATIONS = "max_number_of_migrations";
	protected static final String MIN_MIGRATION_EFFICIENCY = "min_migration_efficiency";
	protected static final String THRESHOLD = "threshold";
	
	// Used to prevent the case where we are trying to repack and 
	// already efficiently packed data centre.
	protected final int allowedNumberOfUnsucessfulMigrationAttempts = 10;
	
	// m0 - minimize number of nodes used with at most m0 migrations
	protected int maxAllowedNumberOfMigrations;
	
	// ME0 - minimize number of nodes used with migration efficiency at least ME0
	protected double minAllowedMigrationEfficiency;
	
	protected double threshold;

	protected int allowedMigrationAttempts;
	
	public Sercon(String prefix) {
		this(Configuration.getInt(prefix + "." + MAX_NUMBER_OF_MIGRATIONS, 0), 
			 Configuration.getDouble(prefix + "." + MIN_MIGRATION_EFFICIENCY, 0), 
			 Configuration.getDouble(prefix + "." + THRESHOLD));
      LOGGER.trace("[{}]", CommonState.getTime());
	}
	
	protected Sercon(int maxAllowedNumberOfMigrations, 
			double minAllowedMigrationEfficiency, 
			double threshold) {
		this.maxAllowedNumberOfMigrations = maxAllowedNumberOfMigrations;
		this.minAllowedMigrationEfficiency = minAllowedMigrationEfficiency;
		this.threshold = threshold;
		this.allowedMigrationAttempts = 3;
        LOGGER.trace("[{}]", CommonState.getTime());
	}

	@Override
	public boolean execute() {
	    LOGGER.trace("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
		LOGGER.trace("[{}]", CommonState.getTime());
		return false;
	}

	public boolean step() { 
		LOGGER.trace("[{}]", CommonState.getTime());

		hibernateIdleHosts();

		Map<VM, Host> vmMigrationMap = new HashMap<VM, Host>();
		
		serconDVMC(vmMigrationMap); 
		
		// Most importantly migrate the VMs....
		LOGGER.debug("[{}]~vmMigrationMap={}", CommonState.getTime(), vmMigrationMap);
        migrateVMs(vmMigrationMap);
        
		return false;
	}

	public void hibernateIdleHosts() {
		CloudDataCenter.getCloudDataCenter().getHosts().stream()
			.filter(host -> !host.hasVMs()).forEach(host -> host.sleep());
	}

	/*
	Sercon Algorithm Input: S, V, N, ME0
	Murtazaev A and Oh S: Sercon Algorithm Using Live Migration of VMs
	total Migrations ← 0
	unsuccessful Migration Attempts ← 0
	vm Migration Schedule ← {}
	while (get Allowed Migration Attempts (S) >= unsuccessful Migration Attempts)
		//  “minimize number of nodes used with migration efficiency at least ME0”.
		if ((this is not first iteration)
			and calculate Migration Efficiency (total Migrations, S) >= ME0) then 
			break (while loop) 
		for i←1 to |S|
			score [i] ← calculate Score(Si, V, N) 
		end for
		S ← sort Nodes by Score Decr (S, score)
		l ← get Node Index to be released (S)
		VMs To Mig ← get List of VMs from Node (S[l], V)
		vm score [] ← calculate VM Score (VMs To Mig)
		VMs To Mig ← sort VMs By Score Decr (VMs To Mig, vm score) m←0
		foreach vm ∈ VMs To Mig
			for j←1 to l–1
				success ← check If Migration Possible (vm, S[j], vm Migration Schedule) 
				if (success) then 
					create schedule to move vm to S[j];
					add this schedule to vm Migration Schedule 
					m ← m+1
					break (for loop)
			end for 
		end foreach
		if (m = | VMs To Mig |) then 
			total Migrations ← total Migrations + m
			unsuccessful Migration Attempts ← 0
		else 
			remove last added schedule from vm Migration Schedule
			unsuccessful Migration Attempts ← unsuccessful Migration Attempts + 1 
	end while
	 */
	public void serconDVMC(Map<VM, Host> vmMigrationMap) {
		LOGGER.trace("[{}]", CommonState.getTime());
		 
		int numberOfHostsReleased = 0;
		int unsuccessfulMigrationAttempts = 0;
		List<Host> hosts = CloudDataCenter.getCloudDataCenter().getHosts();
		
        while (unsuccessfulMigrationAttempts < allowedMigrationAttempts) {
	        List<Host> sortedHosts = sortHostsBySurrogateWeight(hosts, vmMigrationMap);
    		LOGGER.debug("[{}]~sortedHosts={}", CommonState.getTime(), sortedHosts);
	        
	        Optional<Host> optionalHost = 
	        		getHostToBeReleased(sortedHosts, vmMigrationMap, unsuccessfulMigrationAttempts);
	        if (!optionalHost.isPresent()) {
	    		LOGGER.debug("[{}]~Break optionalHost={} not present", CommonState.getTime(), optionalHost);
	        	break; 
	        	
	        } else if (vmMigrationMap.containsValue(optionalHost.get())) {
	    		LOGGER.debug("[{}]~Break already migrating VMs into host={}", 
	    				CommonState.getTime(), optionalHost.get());
	        	break; 
	        	
	        } else {
	        	Host host = optionalHost.get();
	        	int newTotalMigrations =  vmMigrationMap.size() + host.getNumberOfVMs();
	        	if (maxAllowedNumberOfMigrations != 0) {
        			if (newTotalMigrations > maxAllowedNumberOfMigrations) {
    	        		LOGGER.debug("[{}]~Break newTotalMigrations={} >= maxAllowedNumberOfMigrations={}", 
    	        				CommonState.getTime(), newTotalMigrations, maxAllowedNumberOfMigrations);
    	        		break;
    	        	} else {
    	        		LOGGER.trace("[{}]~Keep going... newTotalMigrations={} >= maxAllowedNumberOfMigrations={}", 
    	        				CommonState.getTime(), newTotalMigrations, maxAllowedNumberOfMigrations);
    	        	}
        		}
	        	if (minAllowedMigrationEfficiency != 0) {
	        		double newMigrationEfficiency = calculateMigrationEfficiency(
	        				numberOfHostsReleased + 1, newTotalMigrations);
		        	if (newMigrationEfficiency < minAllowedMigrationEfficiency) {
		        		LOGGER.debug("[{}]~Break newMigrationEfficiency={} >= minAllowedMigrationEfficiency={}", 
		        				CommonState.getTime(), newMigrationEfficiency, minAllowedMigrationEfficiency);
		        		break;
		        	} else {
		        		LOGGER.trace("[{}]~Keep going... newMigrationEfficiency={} >= minAllowedMigrationEfficiency={}" + 
		        				CommonState.getTime(), newMigrationEfficiency, minAllowedMigrationEfficiency);
		        	}
	        	}
	        	int l = sortedHosts.indexOf(host);
	    		LOGGER.debug("[{}]~l={}, host={}", CommonState.getTime(), l, host);
	        	
		        List<VM> sortedVMs = sortVMsBySurrogateWeightIncludingMigrations(host, vmMigrationMap);
		        // Reverse the order as we want the biggest forst, not the smallest.
		        Collections.reverse(sortedVMs);
	    		LOGGER.debug("[{}]~sortedVMs={}", CommonState.getTime(), sortedVMs);
	    		
	            for (VM vm : sortedVMs) {
	            	// for j←1 to l–1, but I reverse
	            	for (int j = sortedHosts.size()-1; j > l; j--) {
		            	Host possibleNewHost = sortedHosts.get(j);
			    		LOGGER.trace("[{}]~j={}, possibleNewHost={}", CommonState.getTime(), j, possibleNewHost);
			    		
		            	if (isMigrationPossible(vm, possibleNewHost, vmMigrationMap)) {
		            		vmMigrationMap.put(vm, possibleNewHost);
		    	    		LOGGER.debug("[{}]~Updating vmMigrationMap vm={}, newHost={}", 
		    	    				CommonState.getTime(), vm, possibleNewHost);
		            		break;
		            		
		            	} else {
		    	    		LOGGER.trace("[{}]~Can't fit vm={} into possibleNewHost={}", 
		    	    				CommonState.getTime(), vm, possibleNewHost);
		            	}
	            	}
	            }
	
	        	if (newTotalMigrations == vmMigrationMap.size()) {
	        		unsuccessfulMigrationAttempts = 0;
	        		numberOfHostsReleased++;
    	    		LOGGER.debug("[{}]~Releasing host={}, vmMigrationMap={}", 
    	    				CommonState.getTime(), host, vmMigrationMap);
    	    		
	        	} else {
	        		vmMigrationMap.entrySet().stream().filter(entry -> {
	        			return host.equals(entry.getValue());
	        		}).forEach(entrySet -> {
	        			VM vm = entrySet.getKey();
	    	    		vmMigrationMap.remove(vm);
	    	    		LOGGER.trace("[{}]~Removed vm={} from vmMigrationMap", 
	    	    				CommonState.getTime(), vm, vmMigrationMap);
	        		});
	        		unsuccessfulMigrationAttempts++;
    	    		LOGGER.debug("[{}]~Can't releasing host={}, unsuccessfulMigrationAttempts={}", 
    	    				CommonState.getTime(), host, unsuccessfulMigrationAttempts);
	        	}
	        }
        }	
	}
	
	/*
	 * The node to be released is equal to the last busy node (node that hosts at least 
	 * one VM) minus unsuccessful Migration Attempts. 
	 * 
	 */
	protected static Optional<Host> getHostToBeReleased(List<Host> sortedHosts, Map<VM, Host> vmMigrationMap, 
			int unsuccessfulMigrationAttempts) {
		Optional<Host> hostToBeReleased = 
				sortedHosts.stream().
					filter(host -> {
						LOGGER.trace("[{}]~host={} has vm={}", 
								CommonState.getTime(), host, 
								getVMsIngludingMigrations(host, vmMigrationMap).size());
						return getVMsIngludingMigrations(host, vmMigrationMap).size() != 0;
					}).
					skip(unsuccessfulMigrationAttempts).findFirst();
		LOGGER.trace("[{}]~hostToBeReleased={}, sortedHosts{}, unsuccessfulMigrationAttempts={}", 
				CommonState.getTime(), hostToBeReleased, sortedHosts, unsuccessfulMigrationAttempts);
		return hostToBeReleased;
	}

	protected static List<Host> sortHostsBySurrogateWeight(List<Host> hosts, Map<VM, Host> vmMigrationMap) {
		
		// Calculate Lambda - it doesn't car where the VMs are located, only the total 
		final double lambda = 
				hosts.stream().mapToDouble(host -> getCPUUtilIncludingMigrations(host, vmMigrationMap)).sum() / 
				hosts.stream().mapToDouble(host -> getCPUUtilIncludingMigrations(host, vmMigrationMap) + 
						getRAMUtilIncludingMigrations(host, vmMigrationMap)).sum();
		LOGGER.trace("[{}]~lambda={}", CommonState.getTime(), lambda);
		
		// Update the weights
		Map<Host, Double> hostSuggogateWeights = new HashMap<>();
		hosts.stream().forEach(host -> {
			double hostSuggogateWeight = 
					(lambda * getCPUUtilIncludingMigrations(host, vmMigrationMap)) + 
					((1 - lambda) * getRAMUtilIncludingMigrations(host, vmMigrationMap));
			hostSuggogateWeights.put(host, hostSuggogateWeight);
			LOGGER.trace("[{}]~hostSuggogateWeights host={}, hostSuggogateWeight={}", 
					CommonState.getTime(), host, hostSuggogateWeight);
		});
		
		// Sort based on the weights
    	List<Host> sortedHosts = hostSuggogateWeights.entrySet().stream().
            	sorted(Map.Entry.comparingByValue()).map(entry -> entry.getKey()).
            	collect(Collectors.toList());
		LOGGER.trace("[{}]~Sorted hostSuggogateWeights sortedHosts={}", 
				CommonState.getTime(), sortedHosts);
		
		return sortedHosts;
	}

	protected static double getCPUUtilIncludingMigrations(Host host, Map<VM, Host> vmMigrationMap) {
		List<VM> vms = getVMsIngludingMigrations(host, vmMigrationMap);
		double totalVMWorkload = vms.stream().mapToDouble(vm -> vm.getWorkload()).sum();
        LOGGER.trace("[{}]~Host={} -> {}", CommonState.getTime(), totalVMWorkload);
		return host.getUtilForWorkload(totalVMWorkload);
		
	}
	
	protected static double getRAMUtilIncludingMigrations(Host host, Map<VM, Host> vmMigrationMap) {
		List<VM> vms = getVMsIngludingMigrations(host, vmMigrationMap);
		double totalVMRAM = vms.stream().mapToDouble(vm -> vm.getRAM()).sum();
        LOGGER.trace("[{}]~Host={} -> {}", 
        		CommonState.getTime(), totalVMRAM);
		return host.getUtilForRAM(totalVMRAM);
		
	}
	
	protected static List<VM> sortVMsBySurrogateWeightIncludingMigrations(
			Host host, Map<VM, Host> vmMigrationMap) {
		List<VM> vms = getVMsIngludingMigrations(host, vmMigrationMap);
		LOGGER.trace("[{}]~vms={}", CommonState.getTime(), vms);
		
		// Calculate Lambda
		final double lambda = 
				vms.stream().mapToDouble(vm -> vm.getWorkload()).sum() /
				vms.stream().mapToDouble(vm -> vm.getWorkload() + vm.getRAM()).sum();
		LOGGER.trace("[{}]~lambda={}", CommonState.getTime(), lambda);
		
		// Update the weights
		Map<VM, Double> vmSuggogateWeights = new HashMap<>();
		vms.stream().forEach(vm -> {
			double vmSuggogateWeight = 
					(lambda * vm.getWorkload()) + ((1 - lambda) * vm.getRAM());
			vmSuggogateWeights.put(vm, vmSuggogateWeight);
			LOGGER.trace("[{}]~vmSuggogateWeights add vm={}, vmSuggogateWeight={}", 
					CommonState.getTime(), vm, vmSuggogateWeight);
		});
		
		// Sort based on the weights
		List<VM> sortedVMs = vmSuggogateWeights.entrySet().stream().
				sorted(Map.Entry.comparingByValue()).
				map(entry -> entry.getKey()).
				collect(Collectors.toList());
		LOGGER.trace("[{}]~Sorted vmSuggogateWeights sortedVMs={}", 
				CommonState.getTime(), sortedVMs);
		
		return sortedVMs;
	}

	protected static List<VM> getVMsIngludingMigrations(Host host, Map<VM, Host> vmMigrationMap) {
		// Get the original VM for the host
		List<VM> vms = host.vmStream().collect(Collectors.toList());
		
		// Filter through these and if any of the vms are in the migrationMap 
		// then they are being migrated away so remove them.
		List<VM> vmToRemove = vms.stream().filter(vm -> vmMigrationMap.containsKey(vm)).collect(Collectors.toList());
		vmToRemove.forEach(vm -> {
			LOGGER.trace("[{}]~Removed vm={} from host={}", CommonState.getTime(), vm, host);
			vms.remove(vm);
		});
		
		// Then loop through all the hosts which vms are migration to and is the our hist is in the list then 
		// add that vm.
		vmMigrationMap.entrySet().stream().filter(e -> e.getValue() == host).forEach(e -> {
			VM vm = e.getKey();
			LOGGER.trace("[{}]~Add vm={} to host={}", CommonState.getTime(), vm, host);
			vms.add(vm);
		});
		return vms;
	}

	protected static void migrateVMs(Map<VM, Host> vmMigrationMap) {
		LOGGER.trace("[{}]~vmMigrationMap={}", CommonState.getTime(), vmMigrationMap);
		vmMigrationMap.entrySet().stream().forEachOrdered(entrySet -> {
			VM vmToMigrate = entrySet.getKey();
			Host hostToMigrateFrom = vmToMigrate.getHost();
			Host hostToMigrateTo = entrySet.getValue();
			hostToMigrateFrom.migrateVM(vmToMigrate, hostToMigrateTo);
			if (!hostToMigrateFrom.hasVMs()) {
				hostToMigrateFrom.sleep();
			}
		});
		
	}

	protected static double calculateMigrationEfficiency(int numberOfHostsReleased, int numberOfMigrations) {
		double migrationEfficiency = ((1.0 * numberOfHostsReleased) / numberOfMigrations);
		LOGGER.trace("[{}]~migrationEfficiency={}", CommonState.getTime(), migrationEfficiency);
		return migrationEfficiency;
	}

	protected boolean isMigrationPossible(VM vmToMigrate, Host newHost, Map<VM, Host> vmMigrationMap) {
	    LOGGER.trace("[{}]~vmToMigrate={}, newHost={} vmMigrationMap={}", 
	    		CommonState.getTime(), vmToMigrate, newHost, vmMigrationMap);
	    // Consider ann the VMs already on the host
		List<VM> newHostVMs = newHost.vmStream().collect(Collectors.toList());
		// And the VMs already being migrated to the host
		vmMigrationMap.entrySet().stream()
			.filter(e -> e.getValue().equals(newHost))
			.map(Map.Entry::getKey).collect(Collectors.toCollection(() -> newHostVMs));
		// And the new VM in question
		newHostVMs.add(vmToMigrate);
	    LOGGER.trace("[{}]~newHostVMs={}", CommonState.getTime(), newHostVMs);
	
	    boolean canFitVM = 
	    	((newHostVMs.stream().mapToDouble(vm -> vm.getWorkload()).sum()) <= 
	    		(newHost.spec.getMaxWorkload() * threshold)) &&
			((newHostVMs.stream().mapToDouble(vm -> vm.spec.getRam()).sum()) <= 
				(newHost.spec.getMemory()));
		
		LOGGER.trace("[{}]~vmToMigrate={}, newHost={}, canFitVM={}", 
				CommonState.getTime(), vmToMigrate, newHost, canFitVM);
		return canFitVM;
	}

	
}