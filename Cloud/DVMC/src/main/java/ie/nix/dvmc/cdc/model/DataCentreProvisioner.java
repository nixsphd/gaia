package ie.nix.dvmc.cdc.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.CloudDataCenter.VMProvisionModel;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.VM;
import peersim.core.CommonState;

public class DataCentreProvisioner extends CSVFileControl implements VMProvisionModel {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	private final Map<Integer, Integer> vmProvisionMap;
	
	public DataCentreProvisioner(String prefix) {
        super(prefix);

		this.vmProvisionMap = new HashMap<Integer, Integer>(df.getNumberOfRows());
		for (int row = 0; row < df.getNumberOfRows(); row++) {
			//"Host", "VM"
			int hostNumber = Integer.valueOf(df.getData(row, "Host").toString());
			int vmNumber = Integer.valueOf(df.getData(row, "VM").toString());
			
			vmProvisionMap.put(vmNumber, hostNumber);
			
	        LOGGER.debug("[{}]~Mapping vm={} to host={}", 
	        		CommonState.getTime(), vmNumber, hostNumber);
			
		}
        LOGGER.debug("[{}]", CommonState.getTime());
		
	}

	@Override
	public boolean execute() {
	    LOGGER.trace("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	@Override
	public boolean init() {
		LOGGER.trace("[{}]", CommonState.getTime());
		return false;
	}

	public boolean step() { return false; }

	@Override
	public void provisionVM(VM vm) {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();

		int vmNumber = cdc.getVMs().indexOf(vm);
		int hostNumber = vmProvisionMap.get(vmNumber);
		
		Host host = cdc.getHosts().get(hostNumber);
		host.addVM(vm);
        LOGGER.info("[{}]~Added vm={} to host={}", 
        		CommonState.getTime(), vm, host);
	
	}
	
}