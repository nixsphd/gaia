package ie.nix.dvmc.cdc.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.Host.HostSpec;
import ie.nix.util.DataFrame;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class FileBasedHostFactory extends RandomHostFactory {
	
	private static final Logger LOGGER = LogManager.getLogger();
		
	public static final String CSV_FILE = "hosts-file";

	protected Map<Integer,Host> hosts;
	protected int nextHost;
	
	public FileBasedHostFactory(String prefix) {
		this(prefix, "Name", "Number of Cores", "CPU (GHz)", "RAM (GB)", "Host", "Host Spec");
	}
	
	public FileBasedHostFactory(String prefix, 
			String hostSpecNameHeader, String numberOfCoresHeader, String cpuHeader,String ramHeader,
			String hostHeader, String hostSpecHeader) {
		
		super(prefix, hostSpecNameHeader, numberOfCoresHeader, cpuHeader, ramHeader);
        
		String hostsCSVFile = Configuration.getString(prefix + "." + CSV_FILE, getClass().getSimpleName() + ".csv");
		LOGGER.info("[{}]~Reading from {}", CommonState.getTime(), hostsCSVFile);
		df = new DataFrame();
		df.read(hostsCSVFile);
		
		this.hosts = new HashMap<Integer, Host>();
		for (int row = 0; row < df.getNumberOfRows(); row++) {			
			// "Host", "Host Spec"
			String hostSpecName = removeQuotes((String)df.getData(row, hostSpecHeader));
			int index = Integer.valueOf(df.getData(row, hostHeader).toString());
	        HostSpec hostSpec = this.getHostSpecs().filter(
	        		spec -> spec.getName().equals(hostSpecName)).findFirst().get();
			Host host = new Host(hostSpec);
			hosts.put(index, host);
	        LOGGER.trace("[{}]~Adding host={} at index={}", 
	        		CommonState.getTime(), host, index);
		}
		nextHost = 0;
        LOGGER.debug("[{}]", CommonState.getTime());
	}
	
	@Override
	public boolean execute() {
	    LOGGER.debug("[{}]", CommonState.getTime());
		if (CommonState.getTime() == 0) {
			return init();
		} else {
			return step();
		}
	}

	public boolean init() {
        LOGGER.debug("[{}]", CommonState.getTime());
		return false;
	}

	public boolean step() { return false; }

	public Host newHost() {
		Host host = hosts.get(nextHost);
		nextHost++;
		return host;
	}
	
}
