library(ggplot2)
library(reshape2)

source(file.path("Util.R"))
args = commandArgs(trailingOnly=TRUE)
exp = args[1]
# exp = "exp1"
results_dir=".."
csv_file = "GC.csv"

#  "time","tenderingHosts","biddingHosts","beingAwardedHosts"
column_names = c("Time","Tendering","Bidding","Being Awarded",
                 "Model", "Seed", "Time (mins)", "Time (hrs)")

hostActivity <- c("Tendering","Bidding","Being Awarded")

dataFiles <- listCSVFiles(); 
# dataFiles
data <- readDataFiles(dataFiles)
colnames(data) <- column_names
# data
dataAvgs <- getDataAvgs(data)
# dataAvgs
dataSDs <- getDataSDs(data)
# dataSDs

meltedAvgs <- getMeltedSubset(dataAvgs)
names(meltedAvgs)[names(meltedAvgs) == 'variable'] <- 'GC State'
meltedSDs <- getMeltedSubset(dataSDs)
names(meltedAvgs)[names(meltedAvgs) == 'variable'] <- 'GC State'

qplot(data=meltedAvgs[meltedAvgs$value > 0,], main = "GC",
        x=`Time (hrs)`, xlab = "Time (hrs)", 
        xlim = c(0, max(meltedAvgs$`Time (hrs)`)),
        y=value, ylab = "Number of Host per GC State",
        color=`GC State`,
        facets = `GC State`~Model,
        geom=c("point"), size=I(0.1)) +
    theme(legend.position="none")

plotFileName <- file.path(results_dir, exp, "GC")
# plotFileName
savePlot(plotFileName)

