library(ggplot2)
library(reshape2)

source(file.path("Util.R"))
args = commandArgs(trailingOnly=TRUE)
exp = args[1]
# exp = "exp1"
results_dir=".."
csv_file = "DVMC.csv"

#  "time","asleepHosts","underloadedHosts","balancedHosts","overloadedHosts",
#  "violatingHosts","tenderingHosts","biddingHosts","beingAwardedHosts"
column_names = c("Time","Asleep","Underloaded","Balanced",
                 "Overloaded","Violating", "Model", "Seed",
                 "Time (mins)", "Time (hrs)")

hostStates <- c("Asleep","Underloaded","Balanced", 
                "Overloaded","Violating")

dataFiles <- listCSVFiles(); 
# dataFiles
data <- readDataFiles(dataFiles)
colnames(data) <- column_names
# data
dataAvgs <- getDataAvgs(data)
# dataAvgs
dataSDs <- getDataSDs(data)
# dataSDs

meltedAvgs <- getMeltedSubset(dataAvgs)
names(meltedAvgs)[names(meltedAvgs) == 'variable'] <- 'Host State'
meltedSDs <- getMeltedSubset(dataSDs)
names(meltedSDs)[names(meltedSDs) == 'variable'] <- 'Host State'

qplot(data=meltedAvgs, main = exp,
      x=`Time (hrs)`, xlab = "Time (hrs)",
      y=value, ylab = "Number of Host per Host State",
      color=`Host State`,
      facets = Model~.,
      geom=c("line", "point")) +
    theme(legend.position="top")

plotFileName <- file.path(results_dir, exp, "DVMC")
# plotFileName
savePlot(plotFileName)

