library(ggplot2)
library(reshape2)

source(file.path("Util.R"))
args = commandArgs(trailingOnly=TRUE)
exp = args[1]
# exp = "exp3"
results_dir=".."
csv_file = "CDCViolations.csv"
#  "time","unallocatedVMs","inconsistantVMs"
column_names = c("Time","UnallocatedVMs", "InconsistantVMs", "Model", "Seed",
                 "Time (mins)", "Time (hrs)")

dataFiles <- listCSVFiles(); 
# dataFiles
data <- readDataFiles(dataFiles)
colnames(data) <- column_names
# data
dataAvgs <- getDataAvgs(data)
# dataAvgs
dataSDs <- getDataSDs(data)
# dataSDs

meltedSubset <- getMeltedSubset(data=dataAvgs)
qplot(data=meltedSubset, main = exp,
    x=`Time (hrs)`, xlab = "Time (hrs)",
    y=value, ylab = "CDC Violations",
    color=variable,
    facets = variable ~ Model,
    geom=c("line")) +
    theme(legend.position="none")

plotFileName <- file.path(results_dir, exp, "CDCViolations")
# plotFileName
savePlot(plotFileName)

