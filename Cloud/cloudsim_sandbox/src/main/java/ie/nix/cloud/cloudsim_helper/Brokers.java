package ie.nix.cloud.cloudsim_helper;

import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.VMParams;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlTransient;


@XmlRootElement(name="Brokers")
public class Brokers {
	
	@XmlRootElement(name="Broker")
	public static class BrokerParams {

		@XmlRootElement(name="VM")
		public static class VMParams {
			
			@XmlRootElement(name="CloudletScheduler")
			public static class CloudletSchedulerParams {

				@XmlAttribute
				protected String type;
				
				public String getType() {
					return type;
				}
				
				@Override
				public String toString() {
					return "CloudletScheduler [type=" + type + "]";
				}
		
			}
			
			protected double creationTime = 0;
			
			protected double upTime;

			protected int brokerId;
			
			protected int count = 1;  // Number of VMs to create with consecutive id starting from id.
		
			protected int id = NEXT_VM_ID++;
		
			protected int mips = 250;
		
			protected long size = 10000; // image size (MB)
		
			protected int ram = 512; // vm memory (MB)
		
			protected long bw = 1000;
		
			protected int pesNumber = 1; // number of cpus
		
			protected String vmm = "Xen"; // VMM name
			
			protected CloudletSchedulerParams cloudletSchedulerParams;

			@XmlAttribute
			public double getCreationTime() {
				return creationTime;
			}

			public void setCreationTime(double nextCreationTime) {
				this.creationTime = nextCreationTime;
			}

			@XmlAttribute
			public double getUpTime() {
				return upTime;
			}

			public void setUpTime(double upTime) {
				this.upTime = upTime;
				
			}

			@XmlTransient
			public int getBrokerId() {
				return brokerId;
			}

			public void setBrokerId(int brokerId) {
				this.brokerId = brokerId;
			}

			@XmlAttribute
			public int getCount() {
				return count;
			}

			public void setCount(int count) {
				this.count = count;
				NEXT_VM_ID += count - 1; // Minus 1 for the increment when the VMParams are created.
			}

			@XmlAttribute(required = false)
			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			@XmlAttribute
			public int getMips() {
				return mips;
			}

			public void setMips(int mips) {
				this.mips = mips;
			}

			@XmlAttribute
			public long getSize() {
				return size;
			}

			public void setSize(long size) {
				this.size = size;
			}

			@XmlAttribute
			public int getRam() {
				return ram;
			}

			public void setRam(int ram) {
				this.ram = ram;
			}

			@XmlAttribute
			public long getBw() {
				return bw;
			}

			public void setBw(long bw) {
				this.bw = bw;
			}

			@XmlAttribute
			public int getPesNumber() {
				return pesNumber;
			}

			public void setPesNumber(int pesNumber) {
				this.pesNumber = pesNumber;
			}

			public void setVmm(String vmm) {
				this.vmm = vmm;
			}
			
			@XmlAttribute
			public String getVmm() {
				return vmm;
			}

			public void setCloudletSchedulerParams(
					CloudletSchedulerParams cloudletSchedulerParams) {
				this.cloudletSchedulerParams = cloudletSchedulerParams;
			}

			@XmlElements({
			    @XmlElement(name = "CloudletScheduler", type = CloudletSchedulerParams.class)
			})
			public CloudletSchedulerParams getCloudletSchedulerParams() {
				return cloudletSchedulerParams;
			}

			@Override
			public String toString() {
				return "VMParams [count=" + count + ", id=" + id
						+ ", mips=" + mips + ", size=" + size + ", ram=" + ram
						+ ", bw=" + bw + ", pesNumber=" + pesNumber + ", vmm="
						+ vmm + "]";
			}
			
		}

		@XmlRootElement(name="Cloudlet")
		public static class CloudletParams {
			
			@XmlRootElement(name="UtilizationModel")
			public static class UtilizationModelParams {
		
				@XmlAttribute
				protected String type;
				
				public String getType() {
					return type;
				}
				
				@Override
				public String toString() {
					return "UtilizationModel [type=" + type + "]";
				}
		
			}
			
			protected double creationTime = 0;
			
			protected int brokerId;

			protected int id = NEXT_CLOUDLET_ID++;  // Number of Cloudlets to create with consecutive id starting from id.

			protected int count = 1;

			protected long length = 400000;  
		
			protected int pesNumber = 1;
		
			protected long fileSize = 300;
		
			protected long outputSize = 300;

			protected UtilizationModelParams cpuUtilizationModelParams;

		    protected UtilizationModelParams ramUtilizationModelParams;
			
		    protected UtilizationModelParams bwUtilizationModelParams;

			@XmlAttribute
			public double getCreationTime() {
				return creationTime;
			}

			public void setCreationTime(double nextCreationTime) {
				this.creationTime = nextCreationTime;
			}

			@XmlTransient
			public int getBrokerId() {
				return brokerId;
			}

			public void setBrokerId(int brokerId) {
				this.brokerId = brokerId;
			}

			@XmlAttribute(required = false)
			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			@XmlAttribute
			public long getLength() {
				return length;
			}

			public void setLength(long length) {
				this.length = length;
			}

			@XmlAttribute
			public int getPesNumber() {
				return pesNumber;
			}

			public void setPesNumber(int pesNumber) {
				this.pesNumber = pesNumber;
			}

			@XmlAttribute
			public long getFileSize() {
				return fileSize;
			}

			public void setFileSize(long fileSize) {
				this.fileSize = fileSize;
			}

			@XmlAttribute
			public long getOutputSize() {
				return outputSize;
			}
		
			public void setOutputSize(long outputSize) {
				this.outputSize = outputSize;
			}

			@XmlAttribute
			public int getCount() {
				return count;
			}

			public void setCount(int count) {
				this.count = count;
				NEXT_CLOUDLET_ID += count - 1; // Minus 1 for the increment when the CloudletParams are created.
			}
			
			@XmlElements({
		        @XmlElement(name = "CPUUtilizationModel", type = UtilizationModelParams.class)
		    })
			public UtilizationModelParams getCpuUtilizationModelParams() {
				return cpuUtilizationModelParams;
			}
			public void setCpuUtilizationModelParams(
					UtilizationModelParams cpuUtilizationModelParams) {
				this.cpuUtilizationModelParams = cpuUtilizationModelParams;
			}

			@XmlElements({
		        @XmlElement(name = "RAMUtilizationModel", type = UtilizationModelParams.class)
		    })
			public UtilizationModelParams getRamUtilizationModelParams() {
				return ramUtilizationModelParams;
			}

			public void setRamUtilizationModelParams(
					UtilizationModelParams ramUtilizationModelParams) {
				this.ramUtilizationModelParams = ramUtilizationModelParams;
			}

			@XmlElements({
		        @XmlElement(name = "BWUtilizationModel", type = UtilizationModelParams.class)
		    })
			public UtilizationModelParams getBwUtilizationModelParams() {
				return bwUtilizationModelParams;
			}

			public void setBwUtilizationModelParams(
					UtilizationModelParams bwUtilizationModelParams) {
				this.bwUtilizationModelParams = bwUtilizationModelParams;
			}

			@Override
			public String toString() {
				return "CloudletParams [count=" + count+ 
						", id=" + id +
						", brokerId=" + brokerId+
						", length=" + length + 
						", pesNumber="+ pesNumber + 
						", fileSize=" + fileSize + 
						", outputSize="+ outputSize + 
						", cpuUtilizationModelParams="+ cpuUtilizationModelParams + 
						", ramUtilizationModelParams="+ ramUtilizationModelParams + 
						", bwUtilizationModelParams="+ bwUtilizationModelParams + "]";
			}
			
		}

		public static class BindParams {
			
			protected int count = 1;  // Number of VMs and Cloudlets to bind.
			
			protected double creationTime = 0;
			
			protected int vmId;
		
			protected int cloudletId;

			@XmlAttribute
			public int getCount() {
				return count;
			}

			public void setCount(int count) {
				this.count = count;
			}

			@XmlAttribute
			public double getCreationTime() {
				return creationTime;
			}

			public void setCreationTime(double nextCreationTime) {
				this.creationTime = nextCreationTime;
			}

			@XmlAttribute(name = "VM")
			public int getVmId() {
				return vmId;
			}

			public void setVmId(int vmId) {
				this.vmId = vmId;
			}

			@XmlAttribute(name = "Cloudlet")
			public int getCloudletId() {
				return cloudletId;
			}

			public void setCloudletId(int cloudletId) {
				this.cloudletId = cloudletId;
			}

			@Override
			public String toString() {
				return "BindParams [vmId=" + vmId + ", cloudletId="+ cloudletId + "]";
			}
			
		}

		protected String type;

		protected long creationTime = 0;

		protected int count = 1;

		protected String name = "Broker";

		protected List<VMParams> vMParams;

		protected List<CloudletParams> cloudletParams;
		
		protected List<BindParams> bindParams;

		public BrokerParams() {
			vMParams = new ArrayList<VMParams>();
			cloudletParams = new ArrayList<CloudletParams>();
			bindParams = new ArrayList<BindParams>();
		}

		@XmlAttribute
		public String getType() {
			return type;
		}
		
		public void setType(String type) {
			this.type = type;
			
		}

		@XmlAttribute
		public long getCreationTime() {
			return creationTime;
		}

		public void setCreationTime(long creationTime) {
			this.creationTime = creationTime;
		}

		@XmlAttribute
		public int getCount() {
			return count;
		}

		public void setCount(int count) {
			this.count = count;
		}

		@XmlAttribute
		public String getName() {
			return name;
		}		
		
		public void setName(String name) {
			this.name = name;
		}

		@XmlElements({
		    @XmlElement(name = "VM", type = VMParams.class)
		})
		public List<VMParams> getvMParams() {
			return vMParams;
		}

		public void setvMParams(List<VMParams> vMParams) {
			this.vMParams = vMParams;
		}
		
		@XmlElements({
		    @XmlElement(name = "Cloudlet", type = CloudletParams.class)
		})
		public List<CloudletParams> getCloudletParams() {
			return cloudletParams;
		}
		
		public void setCloudletParams(List<CloudletParams> cloudletParams) {
			this.cloudletParams = cloudletParams;
		}

		@XmlElements({
		    @XmlElement(name = "Bind", type = BindParams.class)
		})
		public List<BindParams> getBindParams() {
			return bindParams;
		}
		
		public void setBindParams(List<BindParams> bindParams) {
			this.bindParams = bindParams;
		}

		@Override
		public String toString() {
			return "BrokerParams [name=" + name + ", \n"
					+ "VMParams="+ vMParams + ", \n"
					+ "CloudletParams="+ cloudletParams + ", \n"
					+ "BindParams="+ bindParams + "]";
		}

	}

	public static int NEXT_VM_ID = 500; 
	public static int NEXT_CLOUDLET_ID = 1000; 
	
	protected List<BrokerParams> brokerParams;

	public Brokers() {
		super();
		brokerParams = new ArrayList<BrokerParams>();
	}

	@XmlElements({
	    @XmlElement(name = "Broker", type = BrokerParams.class)
	})
	public List<BrokerParams> getBrokerParams() {
		return brokerParams;
	}

	public void setBrokerParams(List<BrokerParams> brokerParams) {
		this.brokerParams = brokerParams;
	}


	@Override
	public String toString() {
		return "Brokers ["
				+ "BrokerParams="+ brokerParams + "]";
	}
	
}
