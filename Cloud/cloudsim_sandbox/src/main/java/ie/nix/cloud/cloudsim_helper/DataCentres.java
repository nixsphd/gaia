package ie.nix.cloud.cloudsim_helper;

import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="DataCentres")
public class DataCentres {

	@XmlRootElement(name="DataCentre")
	public static class DataCentreParams {
		
		@XmlRootElement(name="Host")
		public static class HostParams {
			
			@XmlRootElement(name="VMScheduler")
			public static class VMSchedulerParams {

				@XmlAttribute
				protected String type;
				
				public String getType() {
					return type;
				}
				
				@Override
				public String toString() {
					return "VMSchedulerParams [type=" + type + "]";
				}
		
			}
			
			@XmlRootElement(name="RamProvisioner")
			public static class RamProvisionerParams {

				@XmlAttribute
				protected String type;
				
				public String getType() {
					return type;
				}
				
				@Override
				public String toString() {
					return "RamProvisioner [type=" + type + "]";
				}
		
			}
			
			@XmlRootElement(name="BwProvisioner")
			public static class BwProvisionerParams {

				@XmlAttribute
				protected String type;
				
				public String getType() {
					return type;
				}
				
				@Override
				public String toString() {
					return "BwProvisioner [type=" + type + "]";
				}
		
			}
			
			@XmlRootElement(name="PE")
			public static class PEParams {
				
				@XmlRootElement(name="PeProvisioner")
				public static class PeProvisionerParams {

					@XmlAttribute
					protected String type;
					
					public String getType() {
						return type;
					}
					
					@Override
					public String toString() {
						return "PeProvisioner [type=" + type + "]";
					}
			
				}

				@XmlAttribute
				protected String type;
				
				@XmlAttribute
				protected int count = 1;

				@XmlAttribute
				protected int id;
				
				@XmlAttribute
				protected double mips;
				
			    @XmlElements({
			        @XmlElement(name = "PeProvisioner", type = PeProvisionerParams.class)
			    })
			    protected PeProvisionerParams peProvisioner;
			    
			    public String getType() {
					return type;
				}
				
				public int getCount() {
					return count;
				}
				
				public int getId() {
					return id;
				}
			
				public double getMips() {
					return mips;
				}
			
				public PeProvisionerParams getPeProvisionerParams() {
					return peProvisioner;
				}

				@Override
				public String toString() {
					return "PEParams [count=" + count + 
							", id=" + id + 
							", mips=" + mips + 
							", peProvisioner" + peProvisioner + "]";
				}
			
			}

			protected String type;

			protected int count = 1;

			protected int id = NEXT_HOST_ID++;
			
			protected int mips = 1000; // million instructions per second
		
			protected int ram = 2048; // host memory (MB)
		
			protected long storage = 1000000; // host storage ~ 10 GB
		
			protected int bw = 10000;
			
		    protected RamProvisionerParams ramProvisioner;

		    protected BwProvisionerParams bwProvisioner;
			
		    protected List<PEParams> peParams;
		    
		    protected VMSchedulerParams vmSchedulerParams;

			@XmlAttribute
			public String getType() {
				return type;
			}

			public void setType(String type) {
				this.type = type;
			}

			@XmlAttribute
			public int getId() {
				return id;
			}

			public void setId(int id) {
				this.id = id;
			}

			@XmlAttribute
			public int getCount() {
				return count;
			}

			public void setCount(int count) {
				this.count = count;
				NEXT_HOST_ID += count - 1; // Minus 1 for the increment when the Host are created.
			}

			@XmlAttribute
			public int getMips() {
				return mips;
			}

			public void setMips(int mips) {
				this.mips = mips;
			}

			@XmlAttribute
			public int getRam() {
				return ram;
			}

			public void setRam(int ram) {
				this.ram = ram;
			}

			@XmlAttribute
			public long getStorage() {
				return storage;
			}

			public void setStorage(long storage) {
				this.storage = storage;
			}

			@XmlAttribute
			public int getBw() {
				return bw;
			}

			public void setBw(int bw) {
				this.bw = bw;
			}

			@XmlElements({
		        @XmlElement(name = "RamProvisioner", type = RamProvisionerParams.class)
		    })
			public RamProvisionerParams getRamProvisioner() {
				return ramProvisioner;
			}
			
		    public void setRamProvisioner(RamProvisionerParams ramProvisioner) {
				this.ramProvisioner = ramProvisioner;
			}

			@XmlElements({
		        @XmlElement(name = "BwProvisioner", type = BwProvisionerParams.class)
		    })
			public BwProvisionerParams getBwProvisioner() {
				return bwProvisioner;
			}
		    
		    public void setBwProvisioner(BwProvisionerParams bwProvisioner) {
				this.bwProvisioner = bwProvisioner;
			}

			@XmlElements({
		        @XmlElement(name = "PE", type = PEParams.class)
		    })
			public List<PEParams> getPEParams() {
				return peParams;
			}
		    
		    public void setPEParams(List<PEParams> peParams) {
				this.peParams = peParams;
			}

			@XmlElements({
		        @XmlElement(name = "VMScheduler", type = VMSchedulerParams.class)
		    })
			public VMSchedulerParams getVMSchedulerParams() {
				return vmSchedulerParams;
			}

			public void setVMSchedulerParams(VMSchedulerParams vmSchedulerParams) {
				this.vmSchedulerParams = vmSchedulerParams;
			}

			@Override
			public String toString() {
				return "HostParams [id=" + id + 
						", count="+count+
						", mips=" + mips+ 
						", ram=" + ram + ", storage=" + storage + ", bw="+ bw + 
						", ramProvisioner="+ ramProvisioner +
						", bwProvisioner="+ bwProvisioner +
						", peParams="+ peParams +
						", vmSchedulerParams="+ vmSchedulerParams + "]";
			}
		}

		@XmlRootElement(name="VmAllocationPolicy")
		public static class VmAllocationPolicyParams {

			@XmlAttribute
			protected String type;
			
			public String getType() {
				return type;
			}
			
			@Override
			public String toString() {
				return "VmAllocationPolicy [type=" + type + "]";
			}
	
		}

		@XmlAttribute
		protected String type;

		@XmlAttribute
		public int count;

		@XmlAttribute
		protected String name; // The name of the Data Centre

		@XmlAttribute
		protected String arch = "x86"; // system architecture

		@XmlAttribute
		protected String os = "Linux"; // operating system

		@XmlAttribute
		protected String vmm = "Xen";// Virtual Machine Manager

		@XmlAttribute
		protected double time_zone = 10.0; // time zone this resource located

		@XmlAttribute
		protected double cost = 3.0; // the cost of using processing in this resource

		@XmlAttribute
		protected double costPerMem = 0.05; // the cost of using memory in this resource

		@XmlAttribute
		protected double costPerStorage = 0.001; // the cost of using storage in this resource

		@XmlAttribute
		protected double costPerBw = 0.0; // the cost of using bw in this resource
		
	    @XmlElements({
	        @XmlElement(name = "Host", type = HostParams.class)
	    })
	    protected List<HostParams> hostParams;

	    @XmlElements({
	        @XmlElement(name = "VmAllocationPolicy", type = VmAllocationPolicyParams.class)
	    })
	    protected VmAllocationPolicyParams vmAllocationPolicyParams;

		@XmlAttribute
		protected double schedulingInterval = 0.0;
		
		public int getCount() {
			return count;
		}
		
		public String getName() {
			return name;
		}

		public String getArch() {
			return arch;
		}

		public String getOs() {
			return os;
		}

		public String getVmm() {
			return vmm;
		}

		public double getTime_zone() {
			return time_zone;
		}

		public double getCost() {
			return cost;
		}

		public double getCostPerMem() {
			return costPerMem;
		}

		public double getCostPerStorage() {
			return costPerStorage;
		}

		public double getCostPerBw() {
			return costPerBw;
		}

		public List<HostParams> getHostParams() {
			return hostParams;
		}

		public VmAllocationPolicyParams getVmAllocationPolicyParams() {
			return vmAllocationPolicyParams;
		}

		public double getSchedulingInterval() {
			return schedulingInterval;
		}

		@Override
		public String toString() {
			return "DataCentreParams [name=" + name + ", arch=" + arch
					+ ", os=" + os + ", vmm=" + vmm + ", time_zone="
					+ time_zone + ", cost=" + cost + ", costPerMem="
					+ costPerMem + ", costPerStorage=" + costPerStorage
					+ ", costPerBw=" + costPerBw 
					+ ", hostParams="+ hostParams 
					+ ", vmAllocationPolicyParams="+ vmAllocationPolicyParams  
					+ ", schedulingInterval="+ schedulingInterval + "]";
		}
		
		
	}

	public static int NEXT_HOST_ID = 100; 
	
	@XmlElements({
	    @XmlElement(name = "DataCentre", type = DataCentreParams.class)
	})
	protected List<DataCentreParams> dataCentreParams;

	public List<DataCentreParams> getDataCentreParams() {
		return dataCentreParams;
	}

	@Override
	public String toString() {
		return "CloudSimParams ["
				+ "dataCentreParams="+ dataCentreParams + "]";
	}
	
}
