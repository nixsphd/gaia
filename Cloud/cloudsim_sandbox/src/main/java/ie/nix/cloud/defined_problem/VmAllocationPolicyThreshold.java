package ie.nix.cloud.defined_problem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.core.CloudSim;

public class VmAllocationPolicyThreshold extends VmAllocationPolicySimple {

	/** The used pes. */
	private Map<String, Integer> usedRAM;

	/** The free pes. */
	private List<Integer> freeRAM;
	
	public VmAllocationPolicyThreshold(List<? extends Host> list) {
		super(list);
		
		setFreeRAM(new ArrayList<Integer>());
		for (Host host : getHostList()) {
			getFreeRAM().add(host.getRam());

		}
		setUsedRAM(new HashMap<String, Integer>());
	}

	protected Map<String, Integer> getUsedRAM() {
		return usedRAM;
	}

	protected void setUsedRAM(Map<String, Integer> usedRAM) {
		this.usedRAM = usedRAM;
	}

	protected List<Integer> getFreeRAM() {
		return freeRAM;
	}

	protected void setFreeRAM(List<Integer> freeRAM) {
		this.freeRAM = freeRAM;
	}

	/**
	 * Allocates a host for a given VM.
	 * 
	 * @param vm VM specification
	 * @return $true if the host could be allocated; $false otherwise
	 * @pre $none
	 * @post $none
	 */
	@Override
	public boolean allocateHostForVm(Vm vm) {
		int requiredPes = vm.getNumberOfPes();
		// we want the host with least available RAM that satisfies the need
		int requiredRAM = vm.getRam();

		Log.printLine("NDB: "+CloudSim.clock()+": VmAllocationPolicyThreshold: requiredRAM="+requiredRAM
				+ ", requiredPes="+requiredPes);
		
		boolean result = false;
		int tries = 0;
		List<Integer> freePesTmp = new ArrayList<Integer>();
		for (Integer freePes : getFreePes()) {
			freePesTmp.add(freePes);
		}
		List<Integer> freeRAMTmp = new ArrayList<Integer>();
		for (Integer freeRAM : getFreeRAM()) {
			freeRAMTmp.add(freeRAM);
		}

		if (!getVmTable().containsKey(vm.getUid())) { // if this vm was not created
			do {// we still trying until we find a host or until we try all of them
				
				int bestRAM = Integer.MAX_VALUE;
				int idx = -1;
				
				for (int i = 0; i < freeRAMTmp.size(); i++) {
					Log.printLine("NDB: "+CloudSim.clock()+": VmAllocationPolicyThreshold: host "+getHostList().get(i).getId()+
							", RAM="+freeRAMTmp.get(i)+ ", PEs="+getFreePes().get(i));
					
					if ( (freeRAMTmp.get(i) > requiredRAM) && 
						 (freeRAMTmp.get(i) < bestRAM)) {//  &&
						 //(getFreePes().get(i) >= requiredPes) ) {
						bestRAM = freeRAMTmp.get(i);
						idx = i;
						Log.printLine("NDB: "+CloudSim.clock()+": VmAllocationPolicyThreshold: favoring host "+getHostList().get(i).getId()+
								" with bestRAM="+bestRAM+" and PEs="+freeRAMTmp.get(i));
					}
				}
				if (idx == -1) {
					// We can't find a suitable host :(
					break;
				}
				Host host = getHostList().get(idx);
				Log.printLine(CloudSim.clock()+": VmAllocationPolicyThreshold: bestRAM="+bestRAM+" on host "+host.getId());
				
				result = host.vmCreate(vm);

				if (result) { // if vm were successfully created in the host
					getVmTable().put(vm.getUid(), host);
					
					getUsedPes().put(vm.getUid(), requiredPes);
					getFreePes().set(idx, getFreePes().get(idx) - requiredPes);

					getUsedRAM().put(vm.getUid(), requiredRAM);
					getFreeRAM().set(idx, getFreeRAM().get(idx) - requiredRAM);
					result = true;
					break;
				} else {
					freeRAMTmp.set(idx, Integer.MIN_VALUE);
				}
				tries++;
			} while (!result && tries < getFreeRAM().size());

		}

		if (!result) {
			System.err.println("NDB: "+CloudSim.clock()+": VmAllocationPolicyThreshold: Failed to allocate VM #"+vm.getId()+
					" requiredRAM="+requiredRAM+", requiredPes="+requiredPes);
		}
		return result;
	}
	
	/**
	 * Releases the host used by a VM.
	 * 
	 * @param vm the vm
	 * @pre $none
	 * @post none
	 */
	@Override
	public void deallocateHostForVm(Vm vm) {
		Host host = getVmTable().remove(vm.getUid());
		int idx = getHostList().indexOf(host);
		int pes = getUsedPes().remove(vm.getUid());
		int ram = getUsedRAM().remove(vm.getUid());
		if (host != null) {
			host.vmDestroy(vm);
			getFreePes().set(idx, getFreePes().get(idx) + pes);
			getFreeRAM().set(idx, getFreeRAM().get(idx) + ram);
		}
	}

}
