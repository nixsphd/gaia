package ie.nix.cloud.defined_problem;

import ie.nix.cloud.cloudsim_helper.CloudSimHelper;
import ie.nix.cloud.cloudsim_helper.CloudSimHelper.Logger;


/**
 * Hello world!
 *
 */
public class DefinedProblem {
	
	public static void runExperiment(String dataCenterFilename, String brokersFilename, 
			String logFileName) {
		try {
    		CloudSimHelper definedProblem = new CloudSimHelper(dataCenterFilename, brokersFilename);
//
//			@SuppressWarnings("unused")
//			Logger logger = definedProblem.new Logger();
    		definedProblem.logger.setOutputFileName(logFileName);
    		definedProblem.logger.setLogInterval(10);
			
    		definedProblem.runSimulation();
    		definedProblem.exportCloudlets("DefinedProblemCloudlets.csv");
		
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
    public static void main( String[] args ) {

    	runExperiment(
    			"src/main/java/ie/nix/cloud/defined_problem/DataCentres.xml",
    			"src/main/java/ie/nix/cloud/defined_problem/Brokers.xml");
		
		System.out.println("Done.");
        
    }

}

			
