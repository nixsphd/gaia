package ie.nix.cloud.defined_problem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Log;

import ie.nix.cloud.cloudsim_helper.Brokers;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.BindParams;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.CloudletParams;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.VMParams;
import ie.nix.util.Randomness;
import ie.nix.util.XMLParser;

public class RandomExperimentGenerator {

//	<VM 
//		mips = "1000"
//		size = "10000"
//		ram = "512"
//		bw = "1000"
//		pesNumber = "1"
//		vmm = "Xen">
//		<CloudletScheduler type = "CloudletSchedulerSpaceShared"/>
//	</VM>
	
	// counts
	static int[] counts = {
		1,		// One
		2, 
		3, 
		4, 
		5, 
		8, 
		7, 
		8,
	};
	
	// vm memory (MB)
	static int[] ramSizes = {
//		  16,	//	 16 MB
//		  32,	//	 32 MB
//		  64,	//	 64 MB
//		 128,	//	128 MB
//		 512,	//	512 MB
//		1024,	//	  1 GB
		2048,	//	  2 GB	
		4096,	//	  4 GB	(Average)
		8192,	//	  8 GB	
	   16384,	//	 16 GB	
	};
	
	// pesNumber
	static int[] pesNumbers = {
		   1,	//	  1 cores
		   2,	//	  2 cores
		   4,	//	  4 cores
		   8,	//	  8 cores
		  16,	//	 16 cores
	};

	protected static int numberOfExperiments = 2;
	protected static int numberOfVMs = 50;
	protected static double nextCreationTime = 0; 
	protected static double averageCreationTimeDaley = 500;
	protected static double averageVMUptime = 1500;
	
	public static int nextRandomIndex(int[] array) {
		return Randomness.nextInt(array.length);
	}
	
	public static VMParams generateRandomVM() {
			VMParams vm = new VMParams();
			vm.setCreationTime(nextCreationTime);
			double uptime = averageVMUptime + (averageVMUptime * Randomness.nextGaussian(5));
			while (uptime <= averageVMUptime/2) {
				uptime = averageVMUptime + (averageVMUptime * Randomness.nextGaussian(5));
			}
//			System.err.println("NDB uptime="+uptime);
			vm.setUpTime(uptime);
			vm.setCount(counts[nextRandomIndex(counts)]);
			vm.setRam(ramSizes[nextRandomIndex(ramSizes)]);
			// vm.setPesNumber(pesNumbers[Randomness.nextInt(pesNumbers.length)]);
			return vm;
	}
	
	public static CloudletParams generateRandomCloutlet() {
		CloudletParams cloutlet = new CloudletParams();
		cloutlet.setCreationTime(nextCreationTime);
		return cloutlet;
	}

	public static BindParams generateBindParams(VMParams vm,
			CloudletParams cloudlet) {
		BindParams bind = new BindParams();
		bind.setCreationTime(nextCreationTime); // Can only bind after the Cloudlet and Vm are created in the broker.
		bind.setVmId(vm.getId());
		bind.setCloudletId(cloudlet.getId());
		return bind;
	}

	public static void main(String[] args) {
		try {
			for (int e = 0; e < numberOfExperiments; e++) {
				
				String brokersFilename = "src/main/java/ie/nix/cloud/defined_problem/REGBrokers"+e+".xml";
				String logFileFilename = "DefinedProblemLog"+e+".csv";
				OutputStream os = new FileOutputStream(new File(brokersFilename));
				
				Randomness.setSeed(0);
				
				Brokers brokers = new Brokers();
				
				BrokerParams broker = new BrokerParams();
				broker.setType("ie.nix.cloud.defined_problem.Broker");
				brokers.getBrokerParams().add(broker);
				
				for (int vms = 0; vms < numberOfVMs; vms++) {
					
					VMParams vm = generateRandomVM();
					broker.getvMParams().add(vm);
					
					// One cloudlet per VM
					CloudletParams cloudlet = generateRandomCloutlet();
					cloudlet.setCount(vm.getCount());
					broker.getCloudletParams().add(cloudlet);
					
					// And bind them together
					BindParams bind = generateBindParams(vm, cloudlet);
					bind.setCount(vm.getCount());
					broker.getBindParams().add(bind);
					
					// Move the creationTime along.
					nextCreationTime += (averageCreationTimeDaley + 
							Math.rint(Randomness.nextGaussian(averageCreationTimeDaley/2)));
				}
				
				XMLParser.writeXMLIntoStream(os, Brokers.class, brokers);
				
		    	DefinedProblem.runExperiment("src/main/java/ie/nix/cloud/defined_problem/DataCentres.xml", 
		    			brokersFilename, logFileFilename);
			}
	    	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
//	public static int getRandomNormal(int number, int expected) {
//		double random = Randomness.nextGaussian();
//		return -1;
//	}
	
}
