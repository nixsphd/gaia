package ie.nix.cloud.cloudsim_helper;

import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.BindParams;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.CloudletParams;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.CloudletParams.UtilizationModelParams;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.VMParams;
import ie.nix.cloud.cloudsim_helper.Brokers.BrokerParams.VMParams.CloudletSchedulerParams;
import ie.nix.cloud.cloudsim_helper.DataCentres.DataCentreParams;
import ie.nix.cloud.cloudsim_helper.DataCentres.DataCentreParams.HostParams;
import ie.nix.cloud.cloudsim_helper.DataCentres.DataCentreParams.HostParams.BwProvisionerParams;
import ie.nix.cloud.cloudsim_helper.DataCentres.DataCentreParams.HostParams.PEParams;
import ie.nix.cloud.cloudsim_helper.DataCentres.DataCentreParams.HostParams.PEParams.PeProvisionerParams;
import ie.nix.cloud.cloudsim_helper.DataCentres.DataCentreParams.HostParams.RamProvisionerParams;
import ie.nix.cloud.cloudsim_helper.DataCentres.DataCentreParams.HostParams.VMSchedulerParams;
import ie.nix.cloud.cloudsim_helper.DataCentres.DataCentreParams.VmAllocationPolicyParams;
import ie.nix.util.DataFrame;
import ie.nix.util.XMLParser;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletScheduler;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.UtilizationModelNull;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.VmScheduler;
import org.cloudbus.cloudsim.VmSchedulerSpaceShared;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.core.SimEntity;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.provisioners.BwProvisioner;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisioner;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisioner;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

/**
 * Hello world!
 *
 */
public class CloudSimHelper {
	
	public class Logger extends SimEntity {

		private static final int LOG = 0;
		
		private double logInterval = 100;

		private DataFrame df;
		private String outputFileName;
		
		public Logger() {
			super("Logger");
			initDataFrame();
		}

		public double getLogInterval() {
			return logInterval;
		}

		public void setLogInterval(double logInterval) {
			this.logInterval = logInterval;
		}

		@Override
		public void processEvent(SimEvent ev) {
			switch (ev.getTag()) {
			case LOG:
				log();
				CloudSim.resumeSimulation();
				break;

			default:
				Log.printLine(getName() + ": unknown event type");
				break;
			}
		}

		@Override
		public void startEntity() {
			Log.printLine(super.getName()+" is starting...");
			schedule(getId(), getLogInterval(), LOG);
		}

		@Override
		public void shutdownEntity() {
			Log.printLine(super.getName()+" is shutting down...");
			df.write(outputFileName);
		}
		
		public String getOutputFileName() {
			return outputFileName;
		}

		public void setOutputFileName(String outputFileName) {
			this.outputFileName = outputFileName;
		}

		private void initDataFrame() {
			df = new DataFrame();
			df.addColumns(new String[]{"Clock","DataCentre ID", "Host ID", 
					"RAM", "Available RAM", "User RAM"});//,
//					"VM", "Allocated RAM"});
		}

		private void log() {
			double clock = CloudSim.clock();
//			Log.printLine(clock+": "+getName()+": Logging LogInterval="+getLogInterval()+", state="+getState());
			for (Datacenter dc : dataCentreList) {
				for (Host host : dc.getHostList()) {
					RamProvisioner rp = host.getRamProvisioner();
					df.addRow(clock, dc.getId(), 
							host.getId(),
							rp.getRam(), rp.getAvailableRam(), rp.getUsedRam());
//					if (host.getVmList().isEmpty()) {
//						df.addRow(clock, dc.getId(), 
//								host.getId(),
//								rp.getRam(), rp.getAvailableRam(), rp.getUsedRam(),
//								0, 0);
//					} else {
//						for (Vm vm : host.getVmList()) {
//							df.addRow(clock, dc.getId(), 
//									host.getId(), 
//									rp.getRam(), rp.getAvailableRam(), rp.getUsedRam());//,
////									vm.getId(), vm.getCurrentAllocatedRam());
//						}
//					}
				}
			}
			if (brokersAlive()) {
				schedule(getId(), getLogInterval(), LOG);
			}
		}
		
		private boolean brokersAlive() {
			boolean brokersAlive = false;
			for (DatacenterBroker b :  brokersList) {
				if ((b.getVmList().size() == 0) ||
					((b.getVmList().size() > 0) &&
					 (b.getCloudletReceivedList().size() != b.getCloudletSubmittedList().size()))) {
					brokersAlive = true;
					break;
					
				}
			}
			return brokersAlive;
		}

	}
	
	public class DelatedCreator extends SimEntity {

		private static final int SCHEDULE_CREATE = 0;

		private List<Object> delayedObjects = new ArrayList<Object>();
		
		public DelatedCreator() {
			super("DelatedCreator");
		}

		@Override
		public void processEvent(SimEvent ev) {
			switch (ev.getTag()) {
				case SCHEDULE_CREATE:
					Log.printLine(CloudSim.clock()+": "+getName() + ": SCHEDULE_CREATE "+ev.getData());
					Object o = ev.getData();
					if (o instanceof VMParams) {
						VMParams vMParams = (VMParams)o;
						List<Vm> vms = new ArrayList<Vm>();
			        	for (int vmCount = 0; vmCount < vMParams.count; vmCount++) {
			        		Vm vm = createVM(vmCount, vMParams);
			        		vms.add(vm);
			        		if (vMParams.getUpTime() != 0) {
			        			schedule(dataCentreList.get(0).getId(), 
			        					(vMParams.getCreationTime()+vMParams.getUpTime()),
			        					CloudSimTags.VM_DESTROY, vm);
			        		}
			        	}
		        		// TODO - Fix this so it can handle more than one broker.
			        	brokersList.get(0).submitVmList(vms);
			        	
			        	// When we create a new VM we have to trigger the Broker to process with this event.
						schedule(brokersList.get(0).getId(), 0, CloudSimTags.RESOURCE_CHARACTERISTICS_REQUEST);
			        	
					} else if (o instanceof CloudletParams) {
						CloudletParams cloudletParams = (CloudletParams)o;
						List<Cloudlet> cloudlets = new ArrayList<Cloudlet>();
			        	for (int cloudletCount = 0; cloudletCount < cloudletParams.count; cloudletCount++) {
			        		cloudlets.add(createCloudlet(cloudletCount, cloudletParams));
			        	}
		        		// TODO - Fix this so it can handle more than one broker.
			        	brokersList.get(0).submitCloudletList(cloudlets);
			        	// Apparently I need to send this...
			        	// http://www.researchgate.net/post/How_to_add_new_cloudlets_and_or_new_VMs_in_between_running_simulation_in_CloudSim
			        	// TODO - Support finding the right Datacentre
			        	sendNow(dataCentreList.get(0).getId(), CloudSimTags.VM_DATACENTER_EVENT); 

					} else if (o instanceof BindParams) {
						BindParams bindParams = (BindParams)o;
			        	for (int bindCount = 0; bindCount < bindParams.getCount(); bindCount++) {
			        		brokersList.get(0).bindCloudletToVm(bindParams.getCloudletId()+bindCount, bindParams.getVmId()+bindCount);
			        	}
						
					} else {
						Log.printLine(super.getName()+" unknown object type "+o.getClass());
					}
					
					CloudSim.resumeSimulation();
					break;
	
				default:
					Log.printLine(getName() + ": unknown event type");
					break;
			}
		}

		@Override
		public void startEntity() {
			Log.printLine(super.getName()+" is starting...");
			for (Object o : delayedObjects) {
				// TODO - Pull the time from the Params objects.
				if (o instanceof VMParams) {
					VMParams vMParams = (VMParams)o;
					creator.schedule(creator.getId(), vMParams.getCreationTime(), DelatedCreator.SCHEDULE_CREATE, vMParams);
				} else if (o instanceof CloudletParams) {
					CloudletParams cloudletParams = (CloudletParams)o;
					creator.schedule(creator.getId(), cloudletParams.getCreationTime(), DelatedCreator.SCHEDULE_CREATE, cloudletParams);
				} else if (o instanceof BindParams) {
					BindParams bindParams = (BindParams)o;
					creator.schedule(creator.getId(), bindParams.getCreationTime(), DelatedCreator.SCHEDULE_CREATE, bindParams);
				} else {
					Log.printLine(super.getName()+" unknown object type "+o.getClass());
					
				}
			}
		}

		@Override
		public void shutdownEntity() {
			Log.printLine(super.getName()+" is shutting down...");
		}
		
	};
	
	public Logger logger;
	public DelatedCreator creator;
	
	// Holds all the parameters for the simulation
	private DataCentres datacentres;
	private Brokers brokers;
	
	private List<Datacenter> dataCentreList;
	private List<DatacenterBroker> brokersList;
	
	public CloudSimHelper(String dataCentresFileName, String brokersFileName) throws InterruptedException {
		
    	datacentres = (DataCentres)XMLParser.readXMLFileInto(dataCentresFileName, DataCentres.class);
    	brokers = (Brokers)XMLParser.readXMLFileInto(brokersFileName, Brokers.class);

    	// Initialise the lists
    	dataCentreList = new ArrayList<Datacenter>();
    	brokersList = new ArrayList<DatacenterBroker>();
    	
		// Init CloudSim
        initCloudSim(brokers.brokerParams.size());
        // Init logger
		logger = this.new Logger();
		// Init DelayedCreator
        creator = this.new DelatedCreator();

        for (DataCentreParams dataCentreParams : datacentres.getDataCentreParams()) {
	    	for (int count = 0; count < dataCentreParams.count; count++) {
	    		dataCentreList.add(createDatacenter(count, dataCentreParams));
	    	}
        }       
        
		// Init Brokers
		for (final BrokerParams brokerParams : brokers.getBrokerParams()) {
			for (int count = 0; count < brokerParams.count; count++) {
				brokersList.add(createBroker(count, brokerParams));
			}
		}
	}
//				if (brokerParams.creationTime == 0.0) {
//			} else {
//				
//				// A thread that will create a new broker at 200 clock time
//				Runnable monitor = new Runnable() {
//					public void run() {
//						CloudSim.pauseSimulation(brokerParams.creationTime);
//						while (true) {
//							if (CloudSim.isPaused()) {
//								break;
//							}
//							try {
//								Thread.sleep(100);
//							} catch (InterruptedException e) {
//								e.printStackTrace();
//							}
//						}
//
//						Log.printLine("\n\n\n" + CloudSim.clock() + ": The simulation is paused for 5 sec \n\n");
//
//						try {
//							Thread.sleep(5000);
//						} catch (InterruptedException e) {
//							e.printStackTrace();
//						}
//
//						for (int count = 0; count < brokerParams.count; count++) {
//							brokersList.add(createBroker(count, brokerParams));
//						}
//
//						CloudSim.resumeSimulation();
//					}
//				};
//
//				new Thread(monitor).start();
//				Thread.sleep(1000);
//
//			}
	
	public List<Datacenter> getDatacCenterList() {
		return dataCentreList;
	}

	public List<DatacenterBroker> getBrokersList() {
		return brokersList;
	}

	public void runSimulation() throws InterruptedException {
		// Start the simulation
		CloudSim.startSimulation();
		
	}
	
	public void exportCloudlets(String outputFileName) {	
		DataFrame df = new DataFrame();
		df.addColumns(new String[]{
				"DataCentre","VM","Cloudlet","Status",
				"Start Time","End Time","Time"});
		
		// Print the results
		for (DatacenterBroker broker : brokersList) {
			for (Cloudlet cloudlet : broker.getCloudletReceivedList()) {
				df.addRow(cloudlet.getResourceId(), cloudlet.getVmId(), cloudlet.getCloudletId(), cloudlet.getCloudletStatusString(),
						cloudlet.getExecStartTime(), cloudlet.getFinishTime(), cloudlet.getActualCPUTime());
			}
		
		}
		
		df.write(outputFileName);
		
	}

	protected void initCloudSim(int numUser) {
		Calendar calendar = Calendar.getInstance();
		boolean trace_flag = false; // mean trace events, this is for trace printing. 
	
		CloudSim.init(numUser, calendar, trace_flag);
		
	}

	protected Datacenter createDatacenter(int count, DataCentreParams dataCentreParams) {
		
		List<Host> hostList = new ArrayList<Host>();
		if (dataCentreParams.getHostParams() != null) {
			for (HostParams hostParams : dataCentreParams.getHostParams()) {
		    	for (int hostCount = 0; hostCount < hostParams.count; hostCount++) {
					Host newHost = createHost(hostCount, hostParams);
					hostList.add(newHost);
		    	}
	        }
		}
		
		LinkedList<Storage> storageList = new LinkedList<Storage>();

		DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
				dataCentreParams.arch, 
				dataCentreParams.os, 
				dataCentreParams.vmm, 
				hostList, 
				dataCentreParams.time_zone, 
				dataCentreParams.cost, 
				dataCentreParams.costPerMem,
				dataCentreParams.costPerStorage, 
				dataCentreParams.costPerBw);
		
		String name = dataCentreParams.name;
		if (dataCentreParams.count != 1) {
			name += count;
		}
		
		Datacenter datacenter = null;
		try {
			if (dataCentreParams.type == null) {
				datacenter = new Datacenter(
						name, 
						characteristics, 
						createVmAllocationPolicy(dataCentreParams.getVmAllocationPolicyParams(), hostList),
						storageList, 
						dataCentreParams.schedulingInterval);
			} else {
				datacenter = (Datacenter)Class.forName(dataCentreParams.type).getDeclaredConstructor(
							String.class, DatacenterCharacteristics.class, 
							VmAllocationPolicy.class, List.class, double.class).newInstance(
						name, 
						characteristics, 
						createVmAllocationPolicy(dataCentreParams.getVmAllocationPolicyParams(), hostList),
						storageList, 
						dataCentreParams.schedulingInterval);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return datacenter;
	}

	protected Host createHost(int count, HostParams hostParams) {
		List<Pe> peList = new ArrayList<Pe>();

		for (PEParams peParams : hostParams.getPEParams()) {
	    	for (int peCount = 0; peCount < peParams.count; peCount++) {
				peList.add(createPE(peCount, peParams));
	    	}
		}

		try {
			if (hostParams.type == null) {
				return new Host(
						hostParams.id+count,
						createRamProvisioner(hostParams.getRamProvisioner(), hostParams.ram),
						createBwProvisioner(hostParams.getBwProvisioner(), hostParams.bw),
						hostParams.storage,
						peList,
						createVMScheduler(hostParams.getVMSchedulerParams(), peList));
			} else {
					return (Host)Class.forName(hostParams.type).getDeclaredConstructor(
								int.class, RamProvisioner.class, BwProvisioner.class, 
								long.class, List.class, VmScheduler.class).newInstance(
							hostParams.id+count,
							createRamProvisioner(hostParams.getRamProvisioner(), hostParams.ram),
							createBwProvisioner(hostParams.getBwProvisioner(), hostParams.bw),
							hostParams.storage,
							peList,
							createVMScheduler(hostParams.getVMSchedulerParams(), peList));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
	}
	
	protected RamProvisioner createRamProvisioner(RamProvisionerParams ramProvisionerParams, int ram) {
		if (ramProvisionerParams != null) {
			if ("RamProvisionerSimple".equals(ramProvisionerParams.getType())) {
				return new RamProvisionerSimple(ram);
				
			} else {
				System.err.println("CloudSimHelper.createRamProvisioner() does not recognise the RamProvisioner type "
						+ramProvisionerParams.getType());
				return null;
			}
		} else {
			return new RamProvisionerSimple(ram);
			
		}
		
	}
	
	protected BwProvisioner createBwProvisioner(BwProvisionerParams bwProvisionerParams, int bw) {
		if (bwProvisionerParams != null) {
			if ("BwProvisionerSimple".equals(bwProvisionerParams.getType())) {
				return new BwProvisionerSimple(bw);
				
			} else {
				System.err.println("CloudSimHelper.createBwProvisioner() does not recognise the RamProvisioner type "
						+bwProvisionerParams.getType());
				return null;
			}
		} else {
			return new BwProvisionerSimple(bw);
			
		}
		
	}
	
	protected Pe createPE(int peCount, PEParams pEParams) {
		return new Pe(pEParams.id+peCount, createPeProvisionerParams(pEParams.getPeProvisionerParams(), pEParams.mips));
	}

	protected PeProvisioner createPeProvisionerParams(PeProvisionerParams peProvisionerParams, double mips) {
		if ("PeProvisionerSimple".equals(peProvisionerParams.getType())) {
			return new PeProvisionerSimple(mips);
			
		} else {
			System.err.println("CloudSimHelper.createPeProvisionerParams() does not recognise the PeProvisioner type "
					+peProvisionerParams.getType());
			return null;
		}
		
	}
	
	protected VmScheduler createVMScheduler(VMSchedulerParams vmSchedulerParams, List<Pe> peList) {
		VmScheduler vmScheduler = null;
		try {
			if (vmSchedulerParams == null || vmSchedulerParams.type == null) {
				vmScheduler = new VmSchedulerTimeShared(peList);
			} else {
				vmScheduler =  (VmScheduler)Class.forName(vmSchedulerParams.type).
						getDeclaredConstructor(List.class).newInstance(peList);
			}
		} catch (Exception e) {
			e.printStackTrace();
			
		} 
		return vmScheduler;
		
	}

	protected VmAllocationPolicy createVmAllocationPolicy(VmAllocationPolicyParams vmAllocationPolicyParams, List<Host> hostList) {
		VmAllocationPolicy vmAllocationPolicy = null;
		try {
			if (vmAllocationPolicyParams.type == null) {
				vmAllocationPolicy = new VmAllocationPolicySimple(hostList);
			} else {
				vmAllocationPolicy = (VmAllocationPolicy)Class.forName(vmAllocationPolicyParams.type)
						.getDeclaredConstructor(List.class).newInstance(hostList);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vmAllocationPolicy;
	}
	
	protected DatacenterBroker createBroker(int count, BrokerParams brokerParams) {

		List<Vm> vmlist = new ArrayList<Vm>();
		List<Cloudlet> cloudletList = new ArrayList<Cloudlet>();
		
		DatacenterBroker broker = null;
		String name = brokerParams.getName();
		if (brokerParams.count != 1) {
			name += count;
		}
		
		try {
			if (brokerParams.type == null) {
				broker = new DatacenterBroker(name);
			} else {
				broker =  (DatacenterBroker)Class.forName(brokerParams.type).
						getDeclaredConstructor(String.class).newInstance(name);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		Log.printLine(CloudSim.clock()+": CloudSimHelper: Created broker.");
		
		// Init the VMs
        for (VMParams vMParams : brokerParams.getvMParams()) {
			vMParams.setBrokerId(broker.getId());
    		if (vMParams.getCreationTime() == 0) {
	        	for (int vmCount = 0; vmCount < vMParams.count; vmCount++) {
	        		vmlist.add(createVM(vmCount, vMParams));
	        	} 
        	} else {
    			creator.delayedObjects.add(vMParams);
    		}
        }
		broker.submitVmList(vmlist);
		Log.printLine(CloudSim.clock()+": CloudSimHelper: Added VMs.");
		
		// Init the cloudlets
        for (CloudletParams cloudletParams : brokerParams.getCloudletParams()) {
			cloudletParams.setBrokerId(broker.getId());
    		if (cloudletParams.getCreationTime() == 0) {
    			for (int cloudletCount = 0; cloudletCount < cloudletParams.count; cloudletCount++) {
        			cloudletList.add(createCloudlet(cloudletCount, cloudletParams));
        		}
    		} else {
        		creator.delayedObjects.add(cloudletParams);
        	}
        }
		broker.submitCloudletList(cloudletList);
		Log.printLine(CloudSim.clock()+": CloudSimHelper: Added Cloudlets.");
		
		// Bind the cloudlets to VMs if needed.
		if (brokerParams.getBindParams() != null) {
	        for (BindParams bindParams : brokerParams.getBindParams()) {
        		if (bindParams.getCreationTime() == 0) {
        			for (int bindCount = 0; bindCount < bindParams.count; bindCount++) {
	    	    		broker.bindCloudletToVm(bindParams.getCloudletId()+bindCount, bindParams.getVmId()+bindCount);
	        		}
        		} else {
        			creator.delayedObjects.add(bindParams);
	        	}
	        }
		}
		Log.printLine(CloudSim.clock()+": CloudSimHelper: Added Bindings.");
        
		return broker;
	}
	
	protected Vm createVM(int count, VMParams vMParams) {
		// create VM
		Vm vm = new Vm(vMParams.id + count, 
				vMParams.brokerId,
				vMParams.mips, 
				vMParams.pesNumber, 
				vMParams.ram, 
				vMParams.bw, 
				vMParams.size, 
				vMParams.vmm, 
				new CloudletSchedulerTimeShared()); // How are the Cloudlets scheduled.
		return vm;
	}

	protected Cloudlet createCloudlet(int count, CloudletParams cloudletParams) {
		Cloudlet cloudlet = new Cloudlet(
				cloudletParams.id+count, 
				cloudletParams.length, 
				cloudletParams.pesNumber, 
				cloudletParams.fileSize, 
				cloudletParams.outputSize, 
				createUtilizationModel(cloudletParams.cpuUtilizationModelParams), // CPU full utilization
				createUtilizationModel(cloudletParams.ramUtilizationModelParams), // RAM full utilization
				createUtilizationModel(cloudletParams.bwUtilizationModelParams)); // BW full utilization
		cloudlet.setUserId(cloudletParams.brokerId);
		return cloudlet;
	}

	protected UtilizationModel createUtilizationModel(UtilizationModelParams utilizationModelParams) {
		if (utilizationModelParams!= null) {
			if ("UtilizationModelFull".equals(utilizationModelParams.getType())) {
				return new UtilizationModelFull();
				
			} else if ("UtilizationModelNull".equals(utilizationModelParams.getType())) {
				return new UtilizationModelNull();
				
			} else {
				System.err.println("CloudSimHelper.createUtilizationModel() does not recognise the UtilizationModel type "
						+utilizationModelParams.getType());
				return null;
			}
		} else {
			return new UtilizationModelFull();
		}
	}
	
	protected CloudletScheduler createCloudletScheduler(CloudletSchedulerParams cloudletSchedulerParams) {
		if ("CloudletSchedulerTimeShared".equals(cloudletSchedulerParams.getType())) {
			return new CloudletSchedulerTimeShared();
			
		} else if ("CloudletSchedulerTimeShared".equals(cloudletSchedulerParams.getType())) {
			return new CloudletSchedulerTimeShared();
			
		} else {
			System.err.println("CloudSimHelper.createCloudletScheduler() does not recognise the CloudletScheduler type "
					+cloudletSchedulerParams.getType());
			return null;
		}
	}
	
	public static void main(String[] args) throws InterruptedException {

//    	CloudSimHelper helper = new CloudSimHelper("src/test/java/ie/nix/cloud/cloudsim_sandbox/CloudSimHelperTest.xml");
//    	helper.runSimulation();
//		System.out.println(helper.getCloudSimParams().toString());
    	
//    	CloudSimHelper helper1 = new CloudSimHelper(
//    			"src/main/java/ie/nix/cloud/cloudsim_sandbox/Example1DataCentres.xml",
//    			"src/main/java/ie/nix/cloud/cloudsim_sandbox/Example1Brokers.xml");
//    	helper1.runSimulation();
		//System.out.println(helper1.toString());
//		
//    	CloudSimHelper helper2 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example2.xml");
//    	helper2.runSimulation();
//		System.out.println(helper2.getCloudSimParams().toString());
//		
//    	CloudSimHelper helper3 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example3.xml");
//    	helper3.runSimulation();
//		System.out.println(helper3.getCloudSimParams().toString());
//		
//    	CloudSimHelper helper4 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example4.xml");
//    	helper4.runSimulation();
//		System.out.println(helper4.getCloudSimParams().toString());
//		
//    	CloudSimHelper helper5 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example5.xml");
//    	helper5.runSimulation();
//		System.out.println(helper5.getCloudSimParams().toString());
//		
//    	CloudSimHelper helper6 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example6.xml");
//    	helper6.runSimulation();
//		System.out.println(helper6.getCloudSimParams().toString());
		
//    	CloudSimHelper helper7 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example7.xml");
//    	helper7.runSimulation();
//    	helper7.printCloudletList();
//    	helper7.exportCloudlets("Cloudlets.csv");
//    	
//		System.out.println("Done.");
        
    }

}

			
