package ie.nix.cloud.defined_problem;

import java.util.List;

import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicy;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.core.SimEvent;

public class DataCentre extends Datacenter {

	public DataCentre(String name, DatacenterCharacteristics characteristics, 
			VmAllocationPolicy vmAllocationPolicy, List<Storage> storageList,
			double schedulingInterval) throws Exception {
		super(name, characteristics, vmAllocationPolicy, storageList, schedulingInterval);

		Log.printLine("NDB::DataCentre()");
	}
	

	/**
	 * Process the event for an User/Broker who wants to destroy a VM previously created in this
	 * PowerDatacenter. This PowerDatacenter may send, upon request, the status back to the
	 * User/Broker.
	 * 
	 * @param ev a Sim_event object
	 * @param ack the ack
	 * @pre ev != null
	 * @post $none
	 */
	protected void processVmDestroy(SimEvent ev, boolean ack) {
		Vm vm = (Vm) ev.getData();

		if (getVmList().contains(vm)) {
			super.processVmDestroy(ev, ack);
			
		} else {
			if (ack) {
				int[] data = new int[3];
				data[0] = getId();
				data[1] = vm.getId();
				data[2] = CloudSimTags.FALSE;

				sendNow(vm.getUserId(), CloudSimTags.VM_DESTROY_ACK, data);
			}
		}
		
	}
	
}
