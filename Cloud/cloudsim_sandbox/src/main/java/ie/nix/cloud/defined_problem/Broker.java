package ie.nix.cloud.defined_problem;

import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;

public class Broker extends DatacenterBroker {

	public Broker(String name) throws Exception {
		super(name);
		Log.printLine("NDB::Broker()");
	}
	
}
