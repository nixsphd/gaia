package ie.nix.cloud.cloudsim_sandbox;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;

import java.util.List;

//import junit.framework.TestCase;
import static org.junit.Assert.*;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;
import org.junit.Test;

import ie.nix.cloud.cloudsim_helper.CloudSimHelper;

import org.apache.commons.lang3.StringUtils;

public class CloudSimHelperTest {
	
	@Test
    public void testExample1() throws InterruptedException, IOException {
		String dir = "src/test/java/ie/nix/cloud/cloudsim_sandbox/";
		String actOutputFileName = "Example1.act";
		
		// Set up the logger
		OutputStream logFileOutputStream = 
				new FileOutputStream(dir+actOutputFileName, false);
		Log.setOutput(logFileOutputStream);
		
		// Set up the helper
    	CloudSimHelper helper = new CloudSimHelper(
    			dir+"Example1DataCentres.xml", 
    			dir+"Example1Brokers.xml");
    	
    	// Run the simulation
    	helper.runSimulation();
    	
    	// Print the results
    	printCloudletList(helper.getBrokersList());
    	
		// Check the output
        String expected = new String(Files.readAllBytes(Paths.get(dir,"Example1.exp")));
        String actual = new String(Files.readAllBytes(Paths.get(dir,actOutputFileName)));
        assertEquals("The outpur differs\n"+StringUtils.difference(expected,actual), 
        		expected, actual);
        
    }
	
	public void printCloudletList(List<DatacenterBroker>brokersList) {
		String indent = "    ";
		Log.printLine();
		Log.printLine("========== OUTPUT ==========");
		Log.printLine("Cloudlet ID" + indent + "STATUS" + indent
				+ "Data center ID" + indent + "VM ID" + indent + "Time" + indent
				+ "Start Time" + indent + "Finish Time");
	
		DecimalFormat dft = new DecimalFormat("###.##");
		
		for (DatacenterBroker broker : brokersList) {
			for (Cloudlet cloudlet : broker.getCloudletReceivedList()) {
	
				Log.print(indent + cloudlet.getCloudletId() + indent + indent);
	
				if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS) {
					Log.print("SUCCESS");
	
					Log.printLine(indent + indent + cloudlet.getResourceId()
							+ indent + indent + indent + cloudlet.getVmId()
							+ indent + indent
							+ dft.format(cloudlet.getActualCPUTime()) + indent
							+ indent + dft.format(cloudlet.getExecStartTime())
							+ indent + indent
							+ dft.format(cloudlet.getFinishTime()));
				}
			}
		}
	}
}

//CloudSimHelper helper = new CloudSimHelper("src/test/java/ie/nix/cloud/cloudsim_sandbox/CloudSimHelperTest.xml");
//helper.runSimulation();
//System.out.println(helper.getCloudSimParams().toString());
//
//CloudSimHelper helper1 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example1.xml");
//helper1.runSimulation();
//System.out.println(helper1.getCloudSimParams().toString());
//
//CloudSimHelper helper2 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example2.xml");
//helper2.runSimulation();
//System.out.println(helper2.getCloudSimParams().toString());
//
//CloudSimHelper helper3 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example3.xml");
//helper3.runSimulation();
//System.out.println(helper3.getCloudSimParams().toString());
//
//CloudSimHelper helper4 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example4.xml");
//helper4.runSimulation();
//System.out.println(helper4.getCloudSimParams().toString());
//
//CloudSimHelper helper5 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example5.xml");
//helper5.runSimulation();
//System.out.println(helper5.getCloudSimParams().toString());
//
//CloudSimHelper helper6 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example6.xml");
//helper6.runSimulation();
//System.out.println(helper6.getCloudSimParams().toString());

//CloudSimHelper helper7 = new CloudSimHelper("src/main/java/ie/nix/cloud/cloudsim_sandbox/Example7.xml");
//helper7.runSimulation();
//helper7.printCloudletList();
//helper7.exportCloudlets("Cloudlets.csv");
//
//System.out.println("Done.");
