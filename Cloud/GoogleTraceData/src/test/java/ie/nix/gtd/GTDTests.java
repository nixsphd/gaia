package ie.nix.gtd;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.dvmc.cdc.CloudDataCenter;
import ie.nix.dvmc.cdc.Host;
import ie.nix.dvmc.cdc.Host.HostSpec;
import ie.nix.dvmc.cdc.VM;

public class GTDTests {

	private static final Logger LOGGER = LogManager.getLogger();
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/resources/exp.properties"});
		
	}

	/*
	 * Machines
	 */
	@Test
	@DisplayName("Number of Hosts Test")
	public void numberOfHostsTest() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<Host> hosts = cdc.getHosts();

		int exectedlNumberOfHosts = hosts.size();
		int actualNumberOfHosts = 100;
		
		LOGGER.debug("~exectedlNumberOfHosts={}", exectedlNumberOfHosts);
		LOGGER.debug("~actualNumberOfHosts={}", actualNumberOfHosts);
		
		Assertions.assertEquals(exectedlNumberOfHosts, actualNumberOfHosts, 
				"Expected "+exectedlNumberOfHosts+" hosts, but got "+actualNumberOfHosts+" hosts");
	}
	
	@Test
	@DisplayName("Small Host Add Small VM Test")
	public void smallHostAddSmallVMTTest() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<Host> hosts = cdc.getHosts();
		List<VM> vms = cdc.getVMs();
		
		// "Name","Number of Cores","CPUs","Memory"
		//	0,     1,                0.25,  0.2498
		Host smallHost = hosts.get(0);
		removeAllVMs(smallHost);
		
		// "Name","MIPS","memory request"
		//  0,     0,     0
		VM smallVM = vms.get(0);
		
		boolean expectedToFit = true;
		hostAddVMTest(smallHost, smallVM, expectedToFit);
	}
	
	@Test
	@DisplayName("Small Host Add Big VM Test")
	public void smallHostAddBigVMTTest() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<Host> hosts = cdc.getHosts();
		List<VM> vms = cdc.getVMs();

		// "Name","Number of Cores","CPUs","Memory"
		//	0,     1,                0.25,  0.2498
		Host smallHost = hosts.get(0);
		removeAllVMs(smallHost);
		
		// "Name","MIPS","memory request"
		// 1403,   500,   0.1909
		VM bigVM = vms.get(1);
		
		boolean expectedToFit = false;
		hostAddVMTest(smallHost, bigVM, expectedToFit);
	}

	private void hostAddVMTest(Host host, VM vm, boolean expectedToFit) {
		boolean actualToFit = host.canFitVM(vm);

		LOGGER.debug("~host={}, vm={}", host, vm);
		LOGGER.debug("~expectedToFit={}", expectedToFit);
		LOGGER.debug("~actualToFit={}", actualToFit);
		
		Assertions.assertEquals(expectedToFit, actualToFit, 
				"Expected "+expectedToFit+" to fit vm="+vm+" in host="
						+host+", but got "+actualToFit+" hosts");
	}
	
	/*
	 * Tasks
	 */
	@Test
	@DisplayName("Power Consumption Empty Test")
	public void powerConsumptionEmptyTest() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<Host> hosts = cdc.getHosts();
		
		Host host = hosts.get(0);
		removeAllVMs(host);
		
		double exectedPower = 0;
		powerConsumptiomTest(host, exectedPower);
		
	}
	
	@Test
	@DisplayName("Power Consumption Tiny VM Test")
	public void powerConsumptionTinyTest() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<Host> hosts = cdc.getHosts();
		List<VM> vms = cdc.getVMs();
		
		Host host = hosts.get(0);
		HostSpec hostSpec = host.spec;
				
		removeAllVMs(host);
		host.addVM(vms.get(0));
		
	
		double exectedPower = hostSpec.getCores() * 0.7 * 135/4;
		powerConsumptiomTest(host, exectedPower);
	}
	
	@Test
	@DisplayName("Power Consumption Huge VM Test")
	public void powerConsumptionHugeTest() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<Host> hosts = cdc.getHosts();
		List<VM> vms = cdc.getVMs();
		
		Host host = hosts.get(1);
		HostSpec hostSpec = host.spec;
				
		removeAllVMs(host);
		host.addVM(vms.get(1));
		host.addVM(vms.get(2));
		LOGGER.info("~vm1={}", vms.get(1));
		LOGGER.info("~vm2={}", vms.get(2));
	
		double exectedPower = hostSpec.getCores() * 135/4;
		powerConsumptiomTest(host, exectedPower);
		
	}

	private void powerConsumptiomTest(Host host, double exectedPower) {
		double actualPower = host.getPowerConsumption();

		LOGGER.info("~host={}", host);
		LOGGER.info("~exectedPower={}", exectedPower);
		LOGGER.info("~actualPower={}", actualPower);
		
		Assertions.assertEquals(exectedPower, actualPower, 0.001, 
				"Expected "+exectedPower+" power, but got "+actualPower+" power");
	}
	
	private void removeAllVMs(Host host) {
		// Get rid of any existing VMs
		if (host.hasVMs()) {
			host.vmList().stream().forEach(vm -> {
				host.removeVM(vm);
			});
		}
	}

	/*
	 * Tasks
	 */
	@Test
	@DisplayName("Number of VMs Test")
	public void numberOfVMsTest() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<VM> vms = cdc.getVMs();

		int exectedlNumberOfVMs = 250;
		int actualNumberOfVMs = vms.size(); 
		
		LOGGER.debug("~exectedlNumberOfVMs={}", exectedlNumberOfVMs);
		LOGGER.debug("~actualNumberOfVMs={}", actualNumberOfVMs);
		
		Assertions.assertEquals(exectedlNumberOfVMs, actualNumberOfVMs, 
				"Expected "+exectedlNumberOfVMs+" vms, but got "+actualNumberOfVMs+" vms");
	}
	
	/*
	 * Task Usage
	 */
	@Test
	@DisplayName("Task Usage Test")
	public void taskUsageTest() {
		CloudDataCenter cdc = CloudDataCenter.getCloudDataCenter();
		List<VM> vms = cdc.getVMs();

		double exectedTaskUsage = 22;
		double actualTaskUsage = vms.get(3).getWorkload();
		
		LOGGER.info("~exectedTaskUsage={}", exectedTaskUsage);
		LOGGER.info("~actualTaskUsage={}", actualTaskUsage);
		
		Assertions.assertEquals(exectedTaskUsage, actualTaskUsage, 
				"Expected "+exectedTaskUsage+" task usage, but got "+actualTaskUsage+" task usage");
		
		exectedTaskUsage = 35;
		actualTaskUsage = vms.get(199).getWorkload();
		
		LOGGER.debug("~exectedTaskUsage={}", exectedTaskUsage);
		LOGGER.debug("~actualTaskUsage={}", actualTaskUsage);
		
		Assertions.assertEquals(exectedTaskUsage, actualTaskUsage, 
				"Expected "+exectedTaskUsage+" task usage, but got "+actualTaskUsage+" task usage");
	}
}
