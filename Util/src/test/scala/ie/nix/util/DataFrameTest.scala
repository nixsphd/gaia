package ie.nix.util

import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class DataFrameTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  override def beforeEach(): Unit = Random.setSeed(0)

  private def dataframe =
    new DataFrame(
      Vector(
        Vector("Header1", "Header2", "Header3"),
        Vector("row1-1", "row1-2", "row1-3"),
        Vector("row2-1", "row2-2", "row2-3"),
        Vector("row3-1", "row3-2", "row3-3"),
      ))

  behavior of "DataFrameTest"

  it should "get" in {
    val df11 = dataframe.get(1, 1)
    assert(df11 === "row1-2")
  }

  it should "headers" in {
    val headers = dataframe.headers
    assert(headers === Vector("Header1", "Header2", "Header3"))
  }

  it should "getRow" in {
    val df1 = dataframe.getRow(1)
    assert(df1 === Vector("row1-1", "row1-2", "row1-3"))
  }

  it should "getColumn" in {
    val df1 = dataframe.getColumn(1)
    assert(df1 === Vector("row1-2", "row2-2", "row3-2"))
  }

  it should "getColumn with header name" in {
    val df1 = dataframe.getColumn("Header2")
    assert(df1 === Vector("row1-2", "row2-2", "row3-2"))
  }

  it should "addRow" in {
    val dfWithRowAdded = dataframe.addRow(Vector("row4-1", "row4-2", "row4-3"))
    assert(dfWithRowAdded.numberOfRows === 5)

  }

  it should "filter" in {
    logger info s"~dataframe=$dataframe"
    val filteredDF = dataframe.filter("Header2", "row2-2")
    logger info s"~filteredDF=$filteredDF"

    assert(filteredDF.get(1, 1) === "row2-2")
  }

  it should "subset" in {
    logger info s"~dataframe=$dataframe"
    val subsetedDataFrame = dataframe.subset(Vector("Header1", "Header3"))
    logger info s"~subsetedDataFrame=$subsetedDataFrame"

    assert(subsetedDataFrame.getRow(0) === Vector("Header1", "Header3"))
  }

  it should "subsetColumns" in {
    logger info s"~dataframe=$dataframe"
    val subsetedDataFrame = dataframe.subsetColumns(Vector(0, 2))
    logger info s"~subsetedDataFrame=$subsetedDataFrame"

    assert(subsetedDataFrame.getRow(0) === Vector("Header1", "Header3"))
  }

}
