package ie.nix.ecj

import ie.nix.ecj.PeerSimProblem.bestFitness
import ie.nix.ecj.PeerSimProblemSystemTest.{automata, automataWithDerivedValue, resetSecurityManager}
import ie.nix.ecj.gp.Variable
import ie.nix.peersim.Utils.{nodesAs, now, numberOfNodes}
import ie.nix.peersim.automata.{Automata, AutomataProtocol, AutomataWithDerivedValue, DerivedValueProtocol}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import java.security.Permission
import java.util.DoubleSummaryStatistics

@RunWith(classOf[JUnitRunner])
class PeerSimProblemSystemTest extends AnyFlatSpec with BeforeAndAfterAll with Logging {

  behavior of "PeerSimProblem"

  it should "check Automata" ignore {
    val testProperties =
      Array[String]("-file", "src/test/resources/ie/nix/ecj/TestPeerSimProblem.properties")

    peersim.Simulator.main(testProperties)

    val derivedIsMax = automataWithDerivedValue.count(automaton => automaton.getDerivedValue == maxValue)
    logger info s"derivedIsMax=$derivedIsMax, automata=${automata.mkString(", ")}"
    assert(numberOfNodes === derivedIsMax)

  }

  def maxValue: Double = {
    val stats = new DoubleSummaryStatistics
    automata.foreach(automaton => stats.accept(automaton.getValue.toDouble))
    stats.getMax
  }

  it should "run PeerSimProblem" in {
    resetSecurityManager()
    val properties = Array[String](
      "-file",
      "src/test/resources/ie/nix/ecj/TestPeerSimProblem.params",
      "-p",
      "seed.0=0",
//      "-p",
//      "silent=false",
//      "-p",
//      "gp.problem.logging=console",
    )
    assertThrows[SecurityException] {
      ec.Evolve.main(properties)
    }
    val theBestFitness = bestFitness
    logger info s"bestFitness=${theBestFitness}"

    assert(theBestFitness === 1d)

  }

}

object PeerSimProblemSystemTest extends Logging {

  def automata: Vector[Automata] = nodesAs[Automata](Automata.toAutomata)
  def automataWithDerivedValue: Vector[AutomataWithDerivedValue] =
    nodesAs[AutomataWithDerivedValue](ie.nix.peersim.automata.DerivedValue.toAutomataWithDerivedValue)

  def resetSecurityManager(): Unit =
    System.setSecurityManager(new SecurityManager() {
      override def checkPermission(perm: Permission): Unit = {}
      override def checkExit(status: Int): Unit = throw new SecurityException
    })

}

class EvolvedAutomataProtocol(prefix: String)
    extends AutomataProtocol(prefix: String)
    with DerivedValueProtocol
    with Logging {

  import DerivedValueProtocol.Message

  override protected def getMessageValue(automata: AutomataWithDerivedValue): Int = {
    Value.value = automata.getValue.toDouble
    DerivedValue.value = automata.getDerivedValue.toDouble
    PeerSimProblem.gpProblem.evaluateTreeForDouble(0).toInt
  }

  override protected def getNewDerivedValue(automata: AutomataWithDerivedValue, message: Message): Int = {
    Value.value = automata.getValue.toDouble
    DerivedValue.value = automata.getDerivedValue.toDouble
    MessageValue.value = message.value.toDouble
    logger debug s"[$now] $automata received message $message"
    val newDerivedValue = PeerSimProblem.gpProblem.evaluateTreeForDouble(1)
    logger debug s"[$now] newDerivedValue=$newDerivedValue"
    newDerivedValue.toInt
  }

}

object Value { var value: Double = 0d }
case class Value() extends Variable("Value", (_: Array[TypedData], result: TypedData) => result.set(Value.value)) {}

object DerivedValue { var value: Double = 0d }
class DerivedValue()
    extends Variable("DerivedValue", (_: Array[TypedData], result: TypedData) => result.set(DerivedValue.value)) {}

object MessageValue { var value: Double = 0d }
class MessageValue()
    extends Variable("MessageValue", (_: Array[TypedData], result: TypedData) => result.set(MessageValue.value)) {}

class MaxProblem() extends PeerSimProblem with Logging {

  override protected def evaluateSimulation: Double = {
    val evaluation = numberOfNodes - derivedIsMaxCount
    logger debug s"numberOfNodes=$numberOfNodes, derivedIsMaxCount=$derivedIsMaxCount, evaluation=$evaluation"
    evaluation.toDouble
  }

  private def automata: Vector[Automata] = nodesAs[Automata]

  def derivedIsMaxCount: Int = {
    val stats = new DoubleSummaryStatistics
    automata.foreach(automaton => stats.accept(automaton.getValue.toDouble))
    automataWithDerivedValue.count(automaton => automaton.getDerivedValue == stats.getMax)
  }

}
