package ie.nix.ecj

import ec.gp.koza.KozaFitness
import ec.{EvolutionState, Individual}
import ie.nix.ecj.PeerSimProblemSystemTest.resetSecurityManager
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.sys.process.{Process, ProcessBuilder}

@RunWith(classOf[JUnitRunner])
class MasterSlaveSystemTest extends AnyFlatSpec with BeforeAndAfterAll with Logging {

  private val TEST_EGC_AGENT_PARAMS: String =
    "src/test/resources/ie/nix/ecj/TestPeerSimProblem.params"

  behavior of "MasterSlave"

  it should "Master and Slave" in {
    resetSecurityManager()
    testProblem(masterProperties, slaveProperties)
  }

  def testProblem(masterProperties: Array[String], slaveProperties: Array[String]): Unit = {

    val slave1 = slaveProcess(slaveProperties)
    val slave1Process = slave1.run()
    val slave2 = slaveProcess(slaveProperties)
    val slave2Process = slave2.run()
    val slave3 = slaveProcess(slaveProperties)
    val slave3Process = slave3.run()
    val slave4 = slaveProcess(slaveProperties)
    val slave4Process = slave4.run()

    assertThrows[SecurityException] {
      ec.Evolve.main(masterProperties)
    }

    val slave1Exit = slave1Process.exitValue()
    val slave2Exit = slave2Process.exitValue()
    val slave3Exit = slave3Process.exitValue()
    val slave4Exit = slave4Process.exitValue()
    logger info s"slave1Exit=$slave1Exit, $slave2Exit, $slave3Exit, $slave4Exit completed."
    val bestFitness = MasterProblem.bestFitness
    logger info s"bestFitness=$bestFitness"

    assert(bestFitness === 1d)
  }

  def slaveProcess(properties: Seq[String]): ProcessBuilder =
    Process(s"$java -cp $classpath ec.eval.Slave ${properties.mkString(" ")}")
  def separator: String = System.getProperty("file.separator")
  def classpath: String =
    "/Users/nicolamcdonnell/Desktop/Gaia/Util/lib/*:" +
      "/Users/nicolamcdonnell/Desktop/Gaia/Util/build/classes/main:" +
      "/Users/nicolamcdonnell/Desktop/Gaia/Util/build/resources/main:" +
      "/Users/nicolamcdonnell/Desktop/Gaia/Util/build/classes/test:" +
      "/Users/nicolamcdonnell/Desktop/Gaia/Util/build/resources/test"
  def java: String = System.getProperty("java.home") + separator + "bin" + separator + "java"

  def masterProperties: Array[String] =
    Array[String]() :+
      "-file" :+ TEST_EGC_AGENT_PARAMS :+
      "-p" :+ "seed.0=1" :+
      "-p" :+ "generations=10" :+
      "-p" :+ "eval.masterproblem=ie.nix.ecj.MasterProblem" :+
      "-p" :+ "eval.master.host=127.0.0.1" :+
      "-p" :+ "eval.master.port=12358" :+
      "-p" :+ "eval.masterproblem.job-size=20" :+
      "-p" :+ "eval.masterproblem.max-jobs-per-slave=4"
//      "-p" :+ "silent=false" :+
//      "-p" :+ "gp.problem.logging=console"

  def population = 1000
  def slaveProperties: Array[String] =
    masterProperties :+
      "-p" :+ "eval.slave.silent=true"

}

class MasterProblem extends ec.eval.MasterProblem with Logging {

  override def evaluate(state: EvolutionState, individual: Individual, threadnum: Int, log: Int): Unit = {
    super.evaluate(state, individual, threadnum, log)
    MasterProblem.evolutionState(state)
  }

}

object MasterProblem extends Logging {

  private var _evolutionState: EvolutionState = _

  def evolutionState: EvolutionState = _evolutionState
  def evolutionState(evolutionState: EvolutionState): Unit =
    _evolutionState = evolutionState

  def bestFitness: Double = {
    val fitnesses = for {
      subPop <- evolutionState.population.subpops.asScala
      individual <- subPop.individuals.asScala
    } yield individual.fitness
    val kozaFitness = fitnesses.collect { case kozaFitness: KozaFitness => kozaFitness }
    logger trace s"kozaFitness=${kozaFitness.map(_.adjustedFitness()).mkString(",")}"
    kozaFitness.map(_.adjustedFitness()).max
  }
}
