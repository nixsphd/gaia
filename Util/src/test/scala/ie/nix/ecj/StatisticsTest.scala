package ie.nix.ecj

import ec.EvolutionState
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import java.io._

@RunWith(classOf[JUnitRunner])
class StatisticsTest extends AnyFlatSpec with BeforeAndAfterAll with Logging {

//  override def beforeAll(): Unit = resetSecurityManager()

  behavior of "TestProblem"

  it should "read and write fitness" in {
    val state = new EvolutionState
    val kozaFitnessWithStats = new KozaFitnessWithStats
    val kozaFitnessWithStats2 = new KozaFitnessWithStats
    kozaFitnessWithStats.setStandardizedFitness(state, 2)
    logger info s"Before kozaFitnessWithStats=${kozaFitnessWithStats.fitness()}"
    logger info s"Before kozaFitnessWithStats2=${kozaFitnessWithStats2.fitness()}"
    writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2)
    logger info s"After kozaFitnessWithStats=${kozaFitnessWithStats.fitness()}"
    logger info s"After kozaFitnessWithStats2=${kozaFitnessWithStats2.fitness()}"
    Assertions.assertEquals(
      kozaFitnessWithStats.fitness,
      kozaFitnessWithStats2.fitness,
      "The fitnesses are different sizes kozaFitnessWithStats=" + kozaFitnessWithStats.fitness + " and kozaFitnessWithStats2=" + kozaFitnessWithStats.fitness
    )
  }

  it should "read and write stat count" in {
    val state = new EvolutionState
    val kozaFitnessWithStats = new KozaFitnessWithStats
    val kozaFitnessWithStats2 = new KozaFitnessWithStats
    kozaFitnessWithStats.recordStat("statName", 1)
    writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2)
    logger info s"After kozaFitnessWithStats=${kozaFitnessWithStats.numberOfStats}"
    logger info s"After kozaFitnessWithStats2=${kozaFitnessWithStats2.numberOfStats}"
    Assertions.assertEquals(
      kozaFitnessWithStats.numberOfStats,
      kozaFitnessWithStats2.numberOfStats,
      s"The stats are different sizes kozaFitnessWithStats=" +
        s"${kozaFitnessWithStats.numberOfStats} and kozaFitnessWithStats2=" +
        s"${kozaFitnessWithStats.numberOfStats}"
    )
  }

  it should "read and write a stat" in {
    val state = new EvolutionState
    val kozaFitnessWithStats = new KozaFitnessWithStats
    val kozaFitnessWithStats2 = new KozaFitnessWithStats
    kozaFitnessWithStats.recordStat("statName", 1)
    writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2)
    logger info s"After kozaFitnessWithStats=${kozaFitnessWithStats.numberOfStats}"
    logger info s"After kozaFitnessWithStats2=${kozaFitnessWithStats2.numberOfStats}"
    checkStat(kozaFitnessWithStats, kozaFitnessWithStats2, "statName")
  }

  it should "read and write many stats" in {
    val state = new EvolutionState
    val kozaFitnessWithStats = new KozaFitnessWithStats
    val kozaFitnessWithStats2 = new KozaFitnessWithStats
    kozaFitnessWithStats.recordStat("statName", 1)
    kozaFitnessWithStats.recordStat("statName", 5)
    kozaFitnessWithStats.recordStat("statName", 10)
    kozaFitnessWithStats.recordStat("statName2", -1)
    kozaFitnessWithStats.recordStat("statName2", 50)
    kozaFitnessWithStats.recordStat("statName2", 100)

    writeReadFitness(state, kozaFitnessWithStats, kozaFitnessWithStats2)
    logger info s"After kozaFitnessWithStats=$kozaFitnessWithStats"
    logger info s"After kozaFitnessWithStats2=$kozaFitnessWithStats2"
    checkStat(kozaFitnessWithStats, kozaFitnessWithStats2, "statName")
    checkStat(kozaFitnessWithStats, kozaFitnessWithStats2, "statName2")

  }

  def writeReadFitness(state: EvolutionState,
                       kozaFitnessWithStats: KozaFitnessWithStats,
                       kozaFitnessWithStats2: KozaFitnessWithStats): Unit = {
    val outputStream = new ByteArrayOutputStream
    val dataOutput = new DataOutputStream(outputStream)
    try {
      kozaFitnessWithStats.writeFitness(state, dataOutput)
      dataOutput.asInstanceOf[OutputStream].flush()
      val inputStream = new ByteArrayInputStream(outputStream.asInstanceOf[ByteArrayOutputStream].toByteArray)
      val dataInput = new DataInputStream(inputStream)
      kozaFitnessWithStats2.readFitness(state, dataInput)
    } catch {
      case e: IOException =>
        e.printStackTrace()
    }
  }

  def checkStat(kozaFitnessWithStats: KozaFitnessWithStats,
                kozaFitnessWithStats2: KozaFitnessWithStats,
                statName: String): Unit =
    Assertions.assertEquals(kozaFitnessWithStats.getStat(statName), kozaFitnessWithStats2.getStat(statName))

//  def testFunctionSetDouble(): Unit = {
//    val properties = Array[String](
//      "-file",
//      "src/test/resources/ie/nix/ecj/gp/TestGP.params",
//      "-p",
//      "gp.tc.0.returns=double",
//      "-p",
//      "eval.problem=ie.nix.ecj.gp.DoubleTestProblem",
//      "-p",
//      "seed.0=1"
//    )
//    Assertions.assertThrows(classOf[RuntimeException], () => ec.Evolve.main(properties))
//
//    logger info s"bestAdjustedFitness=${bestAdjustedFitness}"
//    Assertions.assertEquals(1d, bestAdjustedFitness, "Did not get a good enough fitness " + bestAdjustedFitness + ".")
//  }
//
//  def resetSecurityManager(): Unit =
//    System.setSecurityManager(new SecurityManager() {
//      override def checkPermission(perm: Permission): Unit = {}
//      override def checkExit(status: Int): Unit = throw new SecurityException
//    })
}
