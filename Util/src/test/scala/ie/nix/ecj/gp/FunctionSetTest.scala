package ie.nix.ecj.gp

import ec.gp.koza.KozaFitness
import ec.gp.{GPIndividual, GPProblem}
import ec.simple.SimpleProblemForm
import ec.{EvolutionState, Individual}
import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.TestProblem.{
  bestAdjustedFitness,
  expectedBooleanResultSupplier,
  expectedDoubleResultSupplier,
  numberOfRuns
}
import ie.nix.ecj.gp.Variable.{A, B, C, X, Y, Z}
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import java.security.Permission
import java.util.function.Supplier

@RunWith(classOf[JUnitRunner])
class FunctionSetTest extends AnyFlatSpec with BeforeAndAfterAll with Logging {

  override def beforeAll(): Unit = resetSecurityManager()

  behavior of "TestProblem"

  it should "find constant, true" in {
    expectedBooleanResultSupplier = () => true
    testFunctionSetBoolean()
  }

  it should "find constant, 12" in {
    expectedDoubleResultSupplier = () => 12d
    testFunctionSetDouble()
  }

  it should "find constant, Pi" in {
    expectedDoubleResultSupplier = () => Math.PI
    testFunctionSetDouble()
  }

  it should "find variable, X" in {
    expectedDoubleResultSupplier = () => X.value
    testFunctionSetDouble()
  }

  it should "find expression, PI * X" in {
    expectedDoubleResultSupplier = () => Math.PI * X.value
    testFunctionSetDouble()
  }

  it should "find expression, 6 * X" in {
    expectedDoubleResultSupplier = () => 6d * X.value
    testFunctionSetDouble()
  }

  it should "find expression, (X * Z) + Y" in {
    expectedDoubleResultSupplier = () => (X.value * Z.value) + Y.value
    testFunctionSetDouble()
  }

  it should "find expression, if (x > y) x else y" ignore {
    expectedDoubleResultSupplier = () => if (X.value > Z.value) X.value else Y.value
    testFunctionSetDouble()
  }

  it should "find expression, x < y" in {
    expectedBooleanResultSupplier = () => Y.value < X.value
    testFunctionSetBoolean()
  }

  it should "find expression, x || y" in {
    expectedBooleanResultSupplier = () => (A.value || B.value)
    testFunctionSetBoolean()
  }

  it should "find expression, A || B && C" in {
    expectedBooleanResultSupplier = () => A.value || B.value && C.value
    testFunctionSetBoolean()
  }

  it should "find expression, (x < y) || (y < z)" in {
    expectedBooleanResultSupplier = () => X.value < Y.value || Y.value < Z.value
    testFunctionSetBoolean()
  }

  def testFunctionSetDouble(): Unit = {
    val properties = Array[String](
      "-file",
      "src/test/resources/ie/nix/ecj/gp/TestGP.params",
      "-p",
      "gp.tc.0.returns=double",
      "-p",
      "eval.problem=ie.nix.ecj.gp.DoubleTestProblem",
      "-p",
      "seed.0=1"
    )
    Assertions.assertThrows(classOf[RuntimeException], () => ec.Evolve.main(properties))

    logger info s"bestAdjustedFitness=${bestAdjustedFitness}"
    Assertions.assertEquals(1d, bestAdjustedFitness, "Did not get a good enough fitness " + bestAdjustedFitness + ".")
  }

  def testFunctionSetBoolean(): Unit = {
    val properties = Array[String](
      "-file",
      "src/test/resources/ie/nix/ecj/gp/TestGP.params",
      "-p",
      "gp.tc.0.returns=boolean",
      "-p",
      "eval.problem=ie.nix.ecj.gp.BooleanTestProblem",
      "-p",
      "seed.0=1",
    )
    Assertions.assertThrows(classOf[RuntimeException], () => ec.Evolve.main(properties))
    logger info s"bestAdjustedFitness=${bestAdjustedFitness}"

    Assertions.assertEquals(1d, bestAdjustedFitness, "Did not get a good enough fitness " + bestAdjustedFitness + ".")
  }

  def resetSecurityManager(): Unit =
    System.setSecurityManager(new SecurityManager() {
      override def checkPermission(perm: Permission): Unit = {}
      override def checkExit(status: Int): Unit = throw new SecurityException
    })

}

abstract class TestProblem extends GPProblem with SimpleProblemForm with Logging {

  override def evaluate(state: EvolutionState, individual: Individual, subpopulation: Int, threadnum: Int): Unit = {
    val gpIndividual: GPIndividual = as[GPIndividual](individual)
    val actualResult: TypedData = as[TypedData](input)
    if (!(individual.evaluated)) {
      evaluateGPIndividual(state, threadnum, gpIndividual, actualResult)
    }
  }

  def evaluateGPIndividual(state: EvolutionState,
                           threadnum: Int,
                           gpIndividual: GPIndividual,
                           actualResult: TypedData): Unit = {
    var sum: Double = 0
    var hits = 0
    for (_ <- 0 until numberOfRuns) {
      val evaluation = evaluateIndividualRun(state, threadnum, gpIndividual, actualResult)
      sum += evaluation
      if (evaluation == 0) hits += 1
    }
    logger trace s"gpIndividual=$gpIndividual, sum=$sum"
    // the fitness better be KozaFitness!
    val kozaFitness: KozaFitness = as[KozaFitness](gpIndividual.fitness)
    kozaFitness.setStandardizedFitness(state, sum)
    kozaFitness.hits = hits
    gpIndividual.evaluated = true
    if (bestAdjustedFitness < kozaFitness.adjustedFitness) {
      bestAdjustedFitness = kozaFitness.adjustedFitness
      logger info s"bestAdjustedFitness=$bestAdjustedFitness"
    }
  }

  def evaluateIndividualRun(state: EvolutionState,
                            threadnum: Int,
                            gpIndividual: GPIndividual,
                            actualResult: TypedData): Double

  def initVariables(state: EvolutionState, threadnum: Int): Unit = {
    Variable.A.value = state.random(threadnum).nextBoolean
    Variable.B.value = state.random(threadnum).nextBoolean
    Variable.C.value = state.random(threadnum).nextBoolean
    Variable.X.value = state.random(threadnum).nextDouble * state.random(threadnum).nextInt(5)
    Variable.Y.value = state.random(threadnum).nextDouble * state.random(threadnum).nextInt(5)
    Variable.Z.value = state.random(threadnum).nextDouble * Math.PI
  }

}

object TestProblem {
  val numberOfRuns: Int = 100
  var expectedBooleanResultSupplier: Supplier[Boolean] = () => false
  var expectedDoubleResultSupplier: Supplier[Double] = () => 0d;
  var bestAdjustedFitness: Double = 0
}

class BooleanTestProblem extends TestProblem with Logging {

  override def evaluateIndividualRun(state: EvolutionState,
                                     threadnum: Int,
                                     gpIndividual: GPIndividual,
                                     actualResult: TypedData): Double = {
    initVariables(state, threadnum)
    gpIndividual.trees(0).child.eval(state, threadnum, actualResult, stack, gpIndividual, this)
    val expectedResult: Boolean = expectedBooleanResultSupplier.get
    logger trace s"expectedResult=$expectedResult, actualResult=${actualResult.getBoolean}"
    if (actualResult.getBoolean == expectedResult) 0d else 1d
  }

}

class DoubleTestProblem extends TestProblem with Logging {

  override def evaluateIndividualRun(state: EvolutionState,
                                     threadnum: Int,
                                     gpIndividual: GPIndividual,
                                     actualResult: TypedData): Double = {
    initVariables(state, threadnum)
    gpIndividual.trees(0).child.eval(state, threadnum, actualResult, stack, gpIndividual, this)
    val expectedResult: Double = expectedDoubleResultSupplier.get
    logger trace s"expectedResult=$expectedResult, actualResult=${actualResult.getBoolean}"
    Math.abs(expectedResult - actualResult.getDouble)
  }
}
