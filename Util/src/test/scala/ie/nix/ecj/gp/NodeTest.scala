package ie.nix.ecj.gp

import ec.EvolutionState
import ec.gp.GPNode
import ec.util.MersenneTwisterFast
import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Constant.{DoubleConstant, False, One, True, Zero}
import ie.nix.ecj.gp.ERC.ERC1
import ie.nix.ecj.gp.Variable.{A, B, X, Y}
import org.apache.logging.log4j.scala.Logging
import org.junit.jupiter.api.Assertions
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NodeTest extends AnyFlatSpec with Logging {

  behavior of "Constant"

  it should "True return true" in testNode(new True(), true)
  it should "False return false" in testNode(new False(), false)
  it should "Zero return 0" in testNode(new Zero(), 0d)
  it should "One return 1" in testNode(new One(), 1d)

  behavior of "Variable"

  it should "A return true" in {
    val nodeA = new Variable.A
    A.value = true
    testNode(nodeA, true)
  }
  it should "B return false" in {
    val nodeB = new Variable.B
    B.value = false
    testNode(nodeB, false)
  }
  it should "X return 0" in {
    val nodeX = new Variable.X
    X.value = 0d
    testNode(nodeX, 0d)
  }
  it should "Y return 1" in {
    val nodeX = new Variable.Y
    Y.value = 1d
    testNode(nodeX, 1d)
  }

  behavior of "Arithmetic Operators"

  it should "add" in {
    val node = new ArithmeticOperator.Add
    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(2d)
    val expectedResult = 3d
    testNode(node, expectedResult)
  }
  it should "subtract" in {
    val node = new ArithmeticOperator.Sub
    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(2d)
    val expectedResult = -1d
    testNode(node, expectedResult)
  }
  it should "multiply" in {
    val node = new ArithmeticOperator.Mul
    node.children = Array[GPNode]() :+
      new DoubleConstant(10d) :+
      new DoubleConstant(2d)
    val expectedResult = 20d
    testNode(node, expectedResult)
  }
  it should "divide" in {
    val node = new ArithmeticOperator.Div
    node.children = Array[GPNode]() :+
      new DoubleConstant(10d) :+
      new DoubleConstant(2d)
    val expectedResult = 5d
    testNode(node, expectedResult)
  }
  it should "divide by zero" in {
    val node = new ArithmeticOperator.Div
    node.children = Array[GPNode]() :+
      new DoubleConstant(10d) :+
      new DoubleConstant(0d)
    val expectedResult = 0d
    testNode(node, expectedResult)
  }

  behavior of "Conditional Operators"

  it should "if double true" in {
    val node = new ConditionalOperator.IfDouble
    node.children = Array[GPNode]() :+
      new True :+
      new DoubleConstant(1d) :+
      new DoubleConstant(2d)
    val expectedResult = 1d
    testNode(node, expectedResult)
  }
  it should "if double false" in {
    val node = new ConditionalOperator.IfDouble
    node.children = Array[GPNode]() :+
      new False :+
      new DoubleConstant(1d) :+
      new DoubleConstant(2d)
    val expectedResult = 2d
    testNode(node, expectedResult)
  }
  it should "if boolean true" in {
    val node = new ConditionalOperator.IfBoolean
    node.children = Array[GPNode]() :+
      new True :+
      new True :+
      new False
    val expectedResult = true
    testNode(node, expectedResult)
  }
  it should "if boolean false" in {
    val node = new ConditionalOperator.IfBoolean
    node.children = Array[GPNode]() :+
      new False :+
      new True :+
      new False
    val expectedResult = false
    testNode(node, expectedResult)
  }

  behavior of "Logical Operators"

  it should "not" in {
    val node = new LogicalOperator.Not
    node.children = Array[GPNode]() :+
      new True
    val expectedResult = false
    testNode(node, expectedResult)
  }
  it should "and" in {
    val node = new LogicalOperator.And
    node.children = Array[GPNode]() :+
      new True :+
      new True
    var expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new True :+
      new False
    expectedResult = false
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new False :+
      new True
    expectedResult = false

    node.children = Array[GPNode]() :+
      new False :+
      new False
    expectedResult = false
    testNode(node, expectedResult)

  }
  it should "or" in {
    val node = new LogicalOperator.Or
    node.children = Array[GPNode]() :+
      new True :+
      new True
    var expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new True :+
      new False
    expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new False :+
      new True
    expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new False :+
      new False
    expectedResult = false
    testNode(node, expectedResult)
  }

  behavior of "Relational Operators"

  it should "equals" in {
    val node = new RelationalOperator.Equals
    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(1d)
    var expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(2d)
    expectedResult = false
    testNode(node, expectedResult)
  }
  it should "not equals" in {
    val node = new RelationalOperator.NotEquals
    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(1d)
    var expectedResult = false
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(2d)
    expectedResult = true
    testNode(node, expectedResult)
  }
  it should "less than" in {
    val node = new RelationalOperator.LessThan
    node.children = Array[GPNode]() :+
      new DoubleConstant(0d) :+
      new DoubleConstant(1d)
    var expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(1d)
    expectedResult = false
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(2d) :+
      new DoubleConstant(1d)
    expectedResult = false
    testNode(node, expectedResult)

  }
  it should "less than or equals" in {
    val node = new RelationalOperator.LessThanOrEquals
    node.children = Array[GPNode]() :+
      new DoubleConstant(0d) :+
      new DoubleConstant(1d)
    var expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(1d)
    expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(2d) :+
      new DoubleConstant(1d)
    expectedResult = false
    testNode(node, expectedResult)
  }
  it should "greater than" in {
    val node = new RelationalOperator.GreaterThan
    node.children = Array[GPNode]() :+
      new DoubleConstant(0d) :+
      new DoubleConstant(1d)
    var expectedResult = false
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(1d)
    expectedResult = false
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(2d) :+
      new DoubleConstant(1d)
    expectedResult = true
    testNode(node, expectedResult)
  }
  it should "greater than or equals" in {
    val node = new RelationalOperator.GreaterThanOrEquals
    node.children = Array[GPNode]() :+
      new DoubleConstant(0d) :+
      new DoubleConstant(1d)
    var expectedResult = false
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(1d)
    expectedResult = true
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(2d) :+
      new DoubleConstant(1d)
    expectedResult = true
    testNode(node, expectedResult)
  }

  behavior of "Arithmetic Functions"

  it should "abs" in {
    val node = new ArithmeticFunctions.Abs
    node.children = Array[GPNode]() :+
      new DoubleConstant(-1d)
    var expectedResult = 1d
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(1d)
    expectedResult = 1d
    testNode(node, expectedResult)
  }
  it should "min" in {
    val node = new ArithmeticFunctions.Min
    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(2d)
    var expectedResult = 1d
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(2d) :+
      new DoubleConstant(1d)
    expectedResult = 1d
    testNode(node, expectedResult)
  }
  it should "max" in {
    val node = new ArithmeticFunctions.Max
    node.children = Array[GPNode]() :+
      new DoubleConstant(1d) :+
      new DoubleConstant(2d)
    var expectedResult = 2d
    testNode(node, expectedResult)

    node.children = Array[GPNode]() :+
      new DoubleConstant(2d) :+
      new DoubleConstant(1d)
    expectedResult = 2d
    testNode(node, expectedResult)
  }

  behavior of "ERC"

  it should "erc" in {
    val state = getEvolutionState
    val nodeA = new ERC1
    nodeA.resetNode(state, 0)
    val expectedResult = nodeA.value
    testNode(nodeA, expectedResult)
  }

  behavior of "Random"

  it should "random" in {
    val state = getEvolutionState
    state.random(0).setSeed(0)
    val result = new TypedData
    val nodeA = new Random
    nodeA.eval(state, 0, result, null, null, null)
    val expectedResult = result.getDouble

    state.random(0).setSeed(0)
    testNode(nodeA, state, expectedResult)
  }

  def testNode(node: GPNode, expectedResult: Boolean): Unit = {
    val actualResult = TypedData()
    node.eval(null, 0, actualResult, null, null, null)
    logger info s"expectedResult=$expectedResult"
    logger info s"actualResult=${actualResult.getBoolean}"
    Assertions.assertEquals(expectedResult,
                            actualResult.getBoolean,
                            "Expected " + expectedResult + ", but  got" + actualResult.getBoolean + ".")
  }

  def testNode(node: GPNode, expectedResult: Double): Unit = {
    val actualResult = TypedData()
    node.eval(null, 0, actualResult, null, null, null)
    logger info s"expectedResult=$expectedResult"
    logger info s"actualResult=${actualResult.getDouble}"
    Assertions.assertEquals(expectedResult,
                            actualResult.getDouble,
                            "Expected " + expectedResult + ", but  got" + actualResult.getDouble + ".")
  }

  def testNode(node: GPNode, state: EvolutionState, expectedResult: Double): Unit = { // (EvolutionState state, int thread, GPData input, ADFStack stack,
    //		GPIndividual individual, Problem problem)
    val actualResult = new TypedData
    node.eval(state, 0, actualResult, null, null, null)
    logger info s"expectedResult=$expectedResult"
    logger info s"actualResult=$actualResult"
    Assertions.assertEquals(expectedResult,
                            actualResult.getDouble,
                            "Expected " + expectedResult + ", but  got" + actualResult.getDouble + ".")
  }
  def getEvolutionState: EvolutionState = {
    val state = new EvolutionState
    state.random = new Array[MersenneTwisterFast](1)
    state.random(0) = new MersenneTwisterFast
    state
  }
}
