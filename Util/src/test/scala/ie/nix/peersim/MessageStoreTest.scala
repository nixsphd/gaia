package ie.nix.peersim

import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.Utils.{isConfigLoaded, nodeAs}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class MessageStoreTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  trait HasNode {
    def node: Node
  }

  override protected def beforeAll(): Unit = initPeerSim()
  override protected def beforeEach(): Unit = {
    Random.setSeed(0)
    messageStore.clearMessages()
  }

  val messageStore: MessageStore = new MessageStore() with HasNode {
    override def node: Node = testNode0
  }

  behavior of "MessageStoreTest"

  it should "messages" in {
    val aMessageA = messageA
    val anotherMessageA = messageA
    val aMessageB = messageB
    messageStore.addMessage(aMessageA)
    messageStore.addMessage(anotherMessageA)
    messageStore.addMessage(aMessageB)
    val theMessages = messageStore.messages[MessageA]
    logger info s"theMessages=$theMessages"

    assert(theMessages.contains(aMessageA))
    assert(theMessages.contains(anotherMessageA))
    assert(!theMessages.contains(aMessageB))
  }

  it should "allMessages" in {
    val aMessageA = messageA
    val anExpiredMessageA = expiredMessageA
    val aMessageB = messageB
    messageStore.addMessage(aMessageA)
    messageStore.addMessage(anExpiredMessageA)
    messageStore.addMessage(aMessageB)
    val theMessages = messageStore.allMessages
    logger info s"theMessages=$theMessages"

    assert(theMessages.contains(aMessageA))
    assert(theMessages.contains(anExpiredMessageA))
    assert(theMessages.contains(aMessageB))
  }

  it should "has" in {
    assert(!messageStore.hasMessage[MessageA])
    assert(!messageStore.hasMessage[MessageB])
  }

  it should "add" in {
    messageStore.addMessage(messageA)
    messageStore.addMessage(messageB)
    assert(messageStore.hasMessage[MessageA])
    assert(messageStore.hasMessage[MessageB])
  }

  it should "remove" in {
    val aMessageA = messageA
    messageStore.addMessage(aMessageA)
    messageStore.addMessage(messageB)
    messageStore.removeMessage(aMessageA)
    assert(!messageStore.hasMessage[MessageA])
    assert(messageStore.hasMessage[MessageB])
  }

  it should "clearMessages" in {
    messageStore.addMessage(messageA)
    messageStore.addMessage(messageB)
    messageStore.clearMessages()
    assert(!messageStore.hasMessages)
  }

  it should "hasLive" in {
    messageStore.addMessage(expiredMessageA)
    messageStore.addMessage(messageB)
    assert(!messageStore.hasLiveMessage[MessageA])
    assert(messageStore.hasLiveMessage[MessageB])
  }

  it should "liveMessages" in {
    val aMessageA = messageA
    val anExpiredMessageA = expiredMessageA
    val aMessageB = messageB
    messageStore.addMessage(aMessageA)
    messageStore.addMessage(anExpiredMessageA)
    messageStore.addMessage(aMessageB)
    val theMessages = messageStore.liveMessages
    logger debug s"theMessages=$theMessages"

    assert(theMessages.contains(aMessageA))
    assert(!theMessages.contains(anExpiredMessageA))
    assert(theMessages.contains(aMessageB))
  }

  it should "liveMessages MessageA" in {
    val aMessageA = messageA
    val anExpiredMessageA = expiredMessageA
    val aMessageB = messageB
    messageStore.addMessage(aMessageA)
    messageStore.addMessage(anExpiredMessageA)
    messageStore.addMessage(aMessageB)
    val theMessages = messageStore.liveMessages[MessageA]
    logger debug s"theMessages=$theMessages"

    assert(theMessages.contains(aMessageA))
    assert(!theMessages.contains(anExpiredMessageA))
    assert(!theMessages.contains(aMessageB))
  }

  it should "clearExpiredMessages" in {}

  def messageA = new MessageA(testNode0, testNode1, 100L)
  def messageB = new MessageB(testNode0, testNode1, 100L)
  def expiredMessageA = new MessageA(testNode0, testNode1, 0L)
  def expiredMessageB = new MessageB(testNode0, testNode1, 0L)

  def testNode0: TestNode = nodeAs[TestNode](index = 0)
  def testNode1: TestNode = nodeAs[TestNode](index = 1)

  class MessageA(from: Node, to: Node, val expiresAt: Long) extends Message(from, to) with Expirations
  class MessageB(from: Node, to: Node, val expiresAt: Long) extends Message(from, to) with Expirations

  def initPeerSim(): Unit =
    if (!isConfigLoaded)
      peersim.Simulator.main(
        Array[String](
          "src/test/resources/ie/nix/peersim/Test.properties",
        ))

}
