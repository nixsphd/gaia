package ie.nix.peersim

import ie.nix.peersim.Gossiping.Gossips
import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging

class TestGossipingProtocol(prefix: String) extends TestProtocol(prefix) with Gossiping with Logging {

  override def nextCycle(node: Node): Unit = TestGossipingProtocol.nextCycleBehavior.apply(this, node)

  override def processEvent(node: Node, message: Message): Unit = {
    logger info s"[$now] $this"
    super.processEvent(node, message)
    gossipMessage(message)
  }
}

object TestGossipingProtocol extends Logging {

  var nextCycleBehavior: (Protocol, Node) => Unit =
    (protocol, node) => logger info s"[$now] protocol=$protocol, node=$node"

  class GossipMessage(from: Node, to: Node, val gossipCounter: Int) extends Message(from, to) with Gossips {
    override def copy(to: Node, gossipCounter: Int): Message with Gossips =
      new GossipMessage(from, to, gossipCounter)
  }

}
