package ie.nix.peersim

import ie.nix.peersim.EventHandler.Message
import peersim.core.CommonState

class TestProtocol(override val prefix: String) extends Protocol(prefix) {

  logger trace s"[${CommonState.getTime}] prefix=$prefix"

  var messagesReceived = Vector[Message]()

  override def toString = if (Option(node).isDefined) { s"TestProtocol-${this.getIndex}" } else { "TestProtocol-?" }

  def nextCycle(node: Node): Unit = {
    logger trace s"[${CommonState.getTime}] $this"
  }

  def processEvent(node: Node, message: Message): Unit = {
    messagesReceived = messagesReceived :+ message
    logger info s"[${CommonState.getTime}] Received ${messagesReceived.length} messages"
  }

}

object TestProtocol {

  class TestMessage() {
    override def toString: String = "TestMessage"
  }

}
