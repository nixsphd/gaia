package ie.nix.peersim

import ie.nix.peersim.TestGossipingProtocol.nextCycleBehavior
import ie.nix.peersim.Utils.{protocolAs, protocolsAs, isConfigLoaded, nodeAs}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.cdsim.CDSimulator
import peersim.edsim.EDSimulator

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class GossipingTest extends AnyFlatSpec with BeforeAndAfterEach with BeforeAndAfterAll with Logging {

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "Gossiping"

  it should "sendMessage node=0 gossipCounter=0" in {
    nextCycleBehavior = node0Gossip0
    runPeerSim()
    assert(messagesReceived == 1)
  }

  it should "sendMessage node=0 gossipCounter=1" in {
    nextCycleBehavior = node0Gossip1
    runPeerSim()
    assert(messagesReceived == 3)
  }

  val node0Gossip = (gossipCounter: Int, protocol: Protocol, node: Node) =>
    if (node.getIndex == 0) {
      val aGossipMessage = new TestGossipingProtocol.GossipMessage(node, testNode1, gossipCounter)
      protocol.sendMessage(aGossipMessage)
      logger info s"$node send $aGossipMessage"
  }
  val node0Gossip0 = node0Gossip(0, _: Protocol, _: Node)
  val node0Gossip1 = node0Gossip(1, _: Protocol, _: Node)

  def testNode0: TestNode = nodeAs[TestNode](index = 0)
  def testNode1: TestNode = nodeAs[TestNode](index = 1)

  def testProtocolID: Int = peersim.config.Configuration.lookupPid("test")
  def testProtocol0: TestProtocol = protocolAs[TestProtocol](testNode0, testProtocolID)

  def gossipMessage(gossipCounter: Int) = new TestGossipingProtocol.GossipMessage(testNode0, testNode1, gossipCounter)

  def messagesReceived: Int = protocolsAs[TestProtocol](testProtocolID).map(_.messagesReceived.size).sum

  private def runPeerSim(): Unit =
    if (!isConfigLoaded)
      peersim.Simulator.main(
        Array[String]("-file",
                      "src/test/resources/ie/nix/peersim/Test.properties",
                      "protocol.test=ie.nix.peersim.TestGossipingProtocol"))
    else if (CDSimulator.isConfigurationCycleDriven)
      CDSimulator.nextExperiment()
    else if (EDSimulator.isConfigurationEventDriven)
      EDSimulator.nextExperiment()
}
