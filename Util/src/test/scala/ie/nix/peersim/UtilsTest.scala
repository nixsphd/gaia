package ie.nix.peersim

import ie.nix.peersim.Utils._
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.core.CommonState

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class UtilsTest extends AnyFlatSpec with BeforeAndAfterEach with BeforeAndAfterAll with Logging {

  import ie.nix.peersim.TestNode.toTestNode

  override def beforeAll(): Unit = initPeerSim

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "PeerSim Utils"

  it should "numberOfNodes" in {
    val numberOfTestNodes = numberOfNodes
    assert(numberOfTestNodes === numberOfNodesFromConfig)
  }

  it should "node" in {
    setNode3()
    val testNode = node
    assert(testNode.getIndex === 3)
  }

  it should "nodeAs" in {
    setNode3()
    val testNode = nodeAs[TestNode]
    assert(testNode.toString === "TestNode-3")
  }

  it should "nodes" in {
    val testNodes = nodes
    assert(testNodes.length === numberOfNodesFromConfig)
  }

  it should "nodesAs" in {
    val testNodes = nodesAs[TestNode]
    assert(testNodes.toString === "Vector(TestNode-0, TestNode-1, TestNode-2, TestNode-3, TestNode-4)")
  }

  it should "getProtocol" in {
    setNode3()
    val testProtocol = protocol(node, testProtocolID)
    assert(testProtocol.toString === "TestProtocol-3")
  }

  it should "getProtocolAs" in {
    val testProtocol = protocolAs[TestProtocol](node, testProtocolID)
    assert(testProtocol.getClass.toString === "class ie.nix.peersim.TestProtocol")
  }

  it should "getProtocols" in {
    val testProtocols = protocols(testProtocolID)
    assert(
      testProtocols.toString === "Vector(TestProtocol-0, TestProtocol-1, " +
        "TestProtocol-2, TestProtocol-3, TestProtocol-4)")
  }

  it should "getProtocolsAs" in {
    val testProtocols = protocols(testProtocolID)
    assert(testProtocols.count(_.isInstanceOf[TestProtocol]) === numberOfNodesFromConfig)
  }

  def setNode3(): Unit = CommonState.setNode(testNode3)

  def testNode0: TestNode = nodeAs[TestNode](index = 0)
  def testNode1: TestNode = nodeAs[TestNode](index = 1)
  def testNode3: TestNode = nodeAs[TestNode](index = 3)

  def initPeerSim(): Unit =
    peersim.Simulator.main(Array[String]("-file", "src/test/resources/ie/nix/peersim/Test.properties"))

  def numberOfNodesFromConfig = peersim.config.Configuration.getInt("network.size")

  def testProtocolID = 1
}
