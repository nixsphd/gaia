package ie.nix.util

object Utils {

  def as[T](maybeT: AnyRef): T = maybeT.asInstanceOf[T]

}
