package ie.nix.util

import org.apache.logging.log4j.scala.Logging

import scala.io.Source

trait CSVFileReader {
  def readData(): DataFrame

}

object CSVFileReader {

  val RESOURCES_DIR = "src/main/resources"

  def apply(dirName: String, fileName: String): CSVFileReader =
    new CSVFileReaderImpl(dirName, fileName)

  def apply(fileName: String): CSVFileReader =
    new CSVFileReaderImpl(".", fileName)

  def dataSetToFileName(dataSetName: String): String =
    s"$RESOURCES_DIR/$dataSetName.csv"

}

@SuppressWarnings(Array("org.wartremover.warts.Null"))
class CSVFileReaderImpl(val dirName: String, val fileName: String) extends CSVFileReader with Logging {

  logger.debug(s"~dirName=$dirName, fileName=$fileName")
  private val source = Source.fromFile(s"$dirName/$fileName")

  def readData(): DataFrame = {
    val lines = source.getLines()

    val headersString = lines.next()
    val headers = headersString.split(",").map(_.trim).toVector
    logger.trace(s"~headers=$headers")

    val dataFrame = DataFrame(headers)

    lines.foldLeft(dataFrame)((dataFrame, row) => {
      val rowValues = row.split(",").map(_.trim).toVector
      logger.trace(s"~rowValues=${rowValues.mkString(" ")}")
      dataFrame.addRow(rowValues)
    })
  }

}
