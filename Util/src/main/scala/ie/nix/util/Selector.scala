package ie.nix.util

import org.apache.logging.log4j.scala.Logging

import scala.annotation.tailrec
import scala.util.Random

trait Selector extends Logging {

  final def selectItem[I](itemProbabilities: Seq[(I, Double)]): I = {
    val sumOfProbabilities = itemProbabilities.map(_._2).sum
    selectItem[I](itemProbabilities, (sumOfProbabilities * Random.nextDouble()))
  }

  @tailrec
  final def selectItem[I](itemProbabilities: Seq[(I, Double)],
                          selectProbability: Double,
                          accumulativeProbability: Double = 0d): I =
    itemProbabilities match {
      case (item, _) +: Nil                                                                      => item
      case (item, probability) +: _ if accumulativeProbability + probability > selectProbability => item
      case (_, probability) +: tail =>
        selectItem(tail, selectProbability, accumulativeProbability + probability)
    }

}
