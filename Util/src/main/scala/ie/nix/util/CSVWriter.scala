package ie.nix.util

import org.apache.logging.log4j.scala.Logging

import java.io.IOException
import java.nio.file.{FileSystems, Files, Path, StandardOpenOption}
import java.util.StringJoiner
import scala.util.{Failure, Success, Try}

trait CSVFileWriter {

  def writeHeaders(headersString: String): Unit

  def writeHeaders(headersList: List[Any]): Unit

  def writeHeaders(headers: Any*): Unit

  def writeRow(rowString: String): Unit

  def writeRow(rowItemsList: List[Any]): Unit

  def writeRow(rowItems: Any*): Unit

  def close(): Unit

}

object CSVFileWriter {

  def apply(logDirName: String = ".",
            logFileName: String,
            doublePrecision: Int = 4,
            append: Boolean = false): CSVFileWriter =
    new CSVFileWriterImpl(logDirName, logFileName, doublePrecision, append)

}

@SuppressWarnings(Array("org.wartremover.warts.Null"))
class CSVFileWriterImpl(val logDirName: String, val logFileName: String, val doublePrecision: Int, val append: Boolean)
    extends CSVFileWriter
    with Logging {

  private val logDir: Path = FileSystems.getDefault.getPath(logDirName)
  ensureDirExists(logDir)

  private val logFile: Path = logDir.resolve(logFileName)

  private val writer = append match {
    case false => Files.newBufferedWriter(logFile, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)
    case true  => Files.newBufferedWriter(logFile, StandardOpenOption.CREATE, StandardOpenOption.APPEND)
  }

  override def close(): Unit = {
    logger.debug(s"Closing ${writer.hashCode()}")
    writer.flush()
    writer.close()
  }

  def writeHeaders(headers: Any*): Unit = {
    writeHeaders(headers.toList)
  }

  def writeHeaders(headersList: List[Any]): Unit = {
    writeHeaders(generateCSVString(headersList))
  }

  def writeHeaders(headersString: String): Unit = {
    try {
      logger.debug(s"Writing headers ~$headersString")
      writer.write(s"$headersString\n")
      writer.flush()
    } catch {
      case exception: IOException =>
        logger.error(s"~Error writing headers to logfile $logFile", exception)
    }
  }

  def writeRow(rowItems: Any*): Unit = {
    writeRow(rowItems.toList)
  }

  def writeRow(rowItemsList: List[Any]): Unit = {
    val rowString = generateCSVString(rowItemsList)
    writeRow(rowString)
  }

  def writeRow(rowString: String): Unit = {
    try {
      logger.debug(s"Writing row ~$rowString")
      writer.write(s"$rowString\n")
      writer.flush()
    } catch {
      case exception: IOException =>
        logger.error(s"~Error writing headers to logfile $logFile", exception)
    }
  }

  protected def getName: String = this.getClass.getSimpleName

  protected def ensureDirExists(logDir: Path): Unit = {
    if (!Files.exists(logDir)) {
      Try[Path] {
        logger.trace(s"~Creating $logDir")
        Files.createDirectories(logDir)
      } match {
        case Success(_) => ()
        case Failure(exception) =>
          logger.error(s"~Error creating logDir $logDir", exception)
      }
    }
  }

  def generateCSVString(row: List[Any]): String = {

    val rowString = new StringJoiner(",", "", "")
    row.foreach((any: Any) =>
      any match {
        case double: Double => rowString.add(formatDouble(double))
        case _              => rowString.add(any.toString)
    })
    rowString.toString
  }

  def formatDouble(value: Double): String = s"%.${doublePrecision}f".format(value)

}
