package ie.nix.peersim

import ie.nix.peersim.Utils.{nodesAs, now}
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

import scala.language.implicitConversions
import scala.sys.exit

abstract class NodeAdapter[N](prefix: String) extends Node(prefix) with Logging {

  private var maybeNode: Option[N] = None

  override def toString: String =
    if (maybeNode.isDefined)
      s"NodeAdapter[node=${maybeNode.get}]"
    else
      s"NodeAdapter[node=$maybeNode]"

  def node(node: N): Unit = maybeNode = Some(node)

  def node: N = maybeNode match {
    case Some(node) => node
    case _ =>
      logger error s"Fatal Error: Node is being accesses before it's iniatilised, $maybeNode"
      exit
  }

}

abstract class NodeAdapterInit[N](prefix: String) extends NodeInit(prefix) {

  logger info s"prefix=$prefix"

  override def init: Boolean = {
    nodesAs[NodeAdapter[N]].foreach(
      (nodeAdapter) =>
        if (nodeAdapter.isUp && nodeAdapter.node == null)
          initNode(nodeAdapter))
    logger debug s"[$now]"
    super.init
  }

  def initNode(nodeAdapter: NodeAdapter[N]): Unit

}

object NodeAdapter {

  implicit def toNodeAdaptor[N](node: Node): NodeAdapter[N] = as[NodeAdapter[N]](node)

  protected def nodeAdapters[N]: Vector[NodeAdapter[N]] = nodesAs[NodeAdapter[N]]

}
