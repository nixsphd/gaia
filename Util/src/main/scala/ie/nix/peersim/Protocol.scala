package ie.nix.peersim

import org.apache.logging.log4j.scala.Logging
import peersim.cdsim.CDProtocol

abstract class Protocol(override val prefix: String) extends EventHandler(prefix) with CDProtocol with Logging {

  override def nextCycle(peersimNode: peersim.core.Node, protocolID: Int): Unit =
    peersimNode match {
      case node: Node => nextCycle(node)
      case _          => // Do Nothing
    }

  def nextCycle(node: Node): Unit

}
