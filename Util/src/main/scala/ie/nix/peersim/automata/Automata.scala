package ie.nix.peersim.automata

import ie.nix.peersim.Utils.nodesAs
import ie.nix.peersim.automata.Automata.maxValue
import ie.nix.peersim.automata.DerivedValue.DEFAULT_DERIVED_VALUE
import ie.nix.peersim.automata.MeanValue.DEFAULT_MEAN_VALUE
import ie.nix.peersim.{Node, NodeInit}
import ie.nix.util.Utils.as
import peersim.config.Configuration
import peersim.core.CommonState

import scala.util.Random

class Automata(prefix: String) extends Node(prefix) {

  private val maxValue: Int = Automata.maxValue
  private var value: Int = Automata.defaultValue

  override def clone: Node = new Automata(prefix)
  override def toString: String = s"Automata-$getIndex{$value}"

  def setValue(value: Int): Unit = this.value = value
  def getValue: Int = value
  def getMaxValue: Int = maxValue

  def incrementValue(): Unit = setValue(clamp(getValue + 1))
  def decrementValue(): Unit = setValue(clamp(getValue - 1))
  private def clamp(x: Int): Int = 0 max x min maxValue

}

object Automata {
  implicit val toAutomata = (node: Node) => as[Automata](node)

  val DEFAULT_DEFAULT_VALUE: Int = 0
  val DEFAULT_VALUE: String = "default_value"
  val DEFAULT_MAX_VALUE: Int = 1000
  val MAX_VALUE: String = "max_value"

  def maxValue: Int = Configuration.getInt(s"network.node.$MAX_VALUE", DEFAULT_MAX_VALUE)
  def defaultValue: Int = Configuration.getInt(s"network.node.$DEFAULT_VALUE", DEFAULT_DEFAULT_VALUE)

}

class RandomNodeInit(prefix: String) extends NodeInit(prefix) {
  override def initNode(node: Node): Unit = node match {
    case automata: Automata => automata.setValue(CommonState.r.nextInt(automata.getMaxValue))
    case notAnAutomata      => logger error s"Expected Automata, but got ${notAnAutomata.getClass.getSimpleName}"
  }
}

trait DerivedValue {
  automata: Automata =>

  protected var derivedValue: Int = DEFAULT_DERIVED_VALUE

  def getDerivedValue: Int = derivedValue
  def setDerivedValue(derivedValue: Int): Unit = this.derivedValue = derivedValue

}

object DerivedValue {
  implicit val toAutomataWithDerivedValue = (node: Node) => as[AutomataWithDerivedValue](node)

  val DEFAULT_DERIVED_VALUE: Int = 0
  val DERIVED_VALUE: String = "derived_value"

  def derivedValue(prefix: String): Int = Configuration.getInt(prefix + "." + DERIVED_VALUE, DEFAULT_DERIVED_VALUE)

}

case class AutomataWithDerivedValue(override val prefix: String) extends Automata(prefix) with DerivedValue {

  override def clone: Node = AutomataWithDerivedValue(prefix)

  override def toString: String = s"AutomataWithDerivedValue-$getIndex{${getValue}, $derivedValue}"
}

trait MeanValue {
  automata: Automata =>

  protected var meanValue: Int = DEFAULT_MEAN_VALUE

  override def toString: String = s"AutomataWithMeanValue-$getIndex{${automata.getValue}, $meanValue}"

  def getMeanValue: Int = meanValue
  def setMeanValue(meanValue: Int): Unit = this.meanValue = meanValue

}

object MeanValue {
  implicit val toAutomataWithMeanValue = (node: Node) => as[AutomataWithMeanValue](node)

  val DEFAULT_MEAN_VALUE: Int = maxValue / 2
  val MEAN_VALUE: String = "mean_value"

  def meanValue: Int = Configuration.getInt(s"network.node.$MEAN_VALUE", DEFAULT_MEAN_VALUE)

}

case class AutomataWithMeanValue(override val prefix: String) extends Automata(prefix) with MeanValue {
  override def clone: Node = AutomataWithMeanValue(prefix)
}

class ResetNodesWithMeanValue(prefix: String) extends NodeInit(prefix) {

  val maxValue: Int = Automata.maxValue
  val meanValue: Int = MeanValue.meanValue

  override def init(): Boolean = {
    super.init()
    resetNodes()
  }
  override def step(): Boolean = resetNodes()

  protected def resetNodes(): Boolean = {
    super.init()
    nodesAs[AutomataWithMeanValue].foreach(automaton => {
      automaton.setMeanValue(meanValue)
      automaton.setValue(meanValue)
    })
    shuffle(Random.shuffle(nodesAs[Automata].toList))
    false
  }

  protected def shuffle(automata: List[Automata]): Unit = automata match {
    case Nil                                    => // Do nothing
    case _ :: Nil                               => // Do nothing
    case anAutomaton :: anotherAutomaton :: Nil => shuffle(anAutomaton, anotherAutomaton)
    case anAutomaton :: anotherAutomaton :: tail => {
      shuffle(anAutomaton, anotherAutomaton)
      shuffle(tail)
    }
  }

  protected def shuffle(anAutomaton: Automata, anotherAutomaton: Automata): Unit = {
    val maxNumberToShuffle = Math.min(anAutomaton.getValue, maxValue - anotherAutomaton.getValue)
    if (maxNumberToShuffle > 0) {
      val numberToShuffle = Random.nextInt(maxNumberToShuffle) + 1
      anAutomaton.setValue(anAutomaton.getValue - numberToShuffle)
      anotherAutomaton.setValue(anotherAutomaton.getValue + numberToShuffle)

    }
  }
}
