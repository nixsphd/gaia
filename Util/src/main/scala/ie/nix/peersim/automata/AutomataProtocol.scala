package ie.nix.peersim.automata

import ie.nix.peersim.Utils.now
import ie.nix.peersim.{EventHandler, Node, Protocol}
import org.apache.logging.log4j.scala.Logging
import peersim.config.FastConfig
import peersim.core.CommonState
import peersim.transport.Transport

abstract class AutomataProtocol(prefix: String) extends Protocol(prefix) with Logging {

  def automata: Automata = Automata.toAutomata(node)
  def leftAutomata: Automata = neighbour(0)
  def rightAutomata: Automata = neighbour(1)
  def index: Int = automata.getIndex

  override def nextCycle(node: Node): Unit = node match {
    case automata: Automata => nextCycle(automata: Automata)
    case _                  =>
  }

  override def processEvent(node: Node, protocolMessage: EventHandler.Message): Unit =
    (node, protocolMessage) match {
      case (automata: Automata, message: AutomataProtocol.Message) => processEvent(automata, message)
      case _ =>
        logger error s"Expected Automata and Message, but got $node and $protocolMessage"
    }

  protected def sendMessages(message: EventHandler.Message, toAutomata: Automata): Unit = {
    val fromAutomata = CommonState.getNode.asInstanceOf[Automata]
    val protocolID = CommonState.getPid
    val transport = fromAutomata.getProtocol(FastConfig.getTransport(protocolID)).asInstanceOf[Transport]
    transport.send(fromAutomata, toAutomata, message, protocolID)
    logger debug s"[$now]~Sending $message"
  }

  def getAutomataValue: Int = automata.getValue
  def setAutomataValue(value: Int): Unit = automata.setValue(value)
  def incrementAutomataValue(): Unit = automata.incrementValue()
  def decrementAutomataValue(): Unit = automata.decrementValue()

  def nextCycle(automata: Automata): Unit
  def processEvent(automata: Automata, protocolMessage: AutomataProtocol.Message): Unit

}

object AutomataProtocol {
  class Message(override val from: Automata, override val to: Automata) extends EventHandler.Message(from, to)
}

trait DerivedValueProtocol {
  automataProtocol: AutomataProtocol =>

  import DerivedValueProtocol.Message

  def automataWithDerivedValue: AutomataWithDerivedValue = DerivedValue.toAutomataWithDerivedValue(node)

  override def nextCycle(node: Automata): Unit = node match {
    case automata: AutomataWithDerivedValue => nextCycle(automata: AutomataWithDerivedValue)
    case _                                  =>
  }

  def nextCycle(automata: AutomataWithDerivedValue): Unit = {
    val messageValue = getMessageValue(automata);
    val toAutomata = rightAutomata
    val message = Message(automata, toAutomata, messageValue)
    sendMessages(message, toAutomata)
    logger debug s"[$now] Sent $message to $toAutomata"
  }

  protected def getMessageValue(automata: AutomataWithDerivedValue): Int = {
    if (automata.getValue > automata.getDerivedValue)
      automata.getValue
    else
      automata.getDerivedValue
  }

  override def processEvent(automata: Automata, protocolMessage: AutomataProtocol.Message): Unit =
    (node, protocolMessage) match {
      case (automata: AutomataWithDerivedValue, message: Message) => processEvent(automata, message)
      case _ =>
        logger error s"Expected AutomataWithDerivedValue and DerivedValueProtocol.Message" +
          s", but got $node and $protocolMessage"
    }

  def processEvent(automata: AutomataWithDerivedValue, message: Message): Unit = {
    val newDerivedValue = getNewDerivedValue(automata, message);
    automata.setDerivedValue(newDerivedValue)
  }

  protected def getNewDerivedValue(automata: AutomataWithDerivedValue, message: Message): Int = {
    val newDerivedValue = message.value
    logger debug s"[$now] automata=$automata $message newDerivedValue=$newDerivedValue"
    newDerivedValue
  }

}

object DerivedValueProtocol {

  case class Message(override val from: Automata, override val to: Automata, value: Int)
      extends AutomataProtocol.Message(from, to)

}

class AutomataProtocolWithDerivedValue(prefix: String) extends AutomataProtocol(prefix) with DerivedValueProtocol
