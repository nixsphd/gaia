package ie.nix.peersim

import ie.nix.peersim.Scheduler.Callback
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState
import peersim.edsim.{EDProtocol, EDSimulator}

class Scheduler(val prefix: String) extends EDProtocol with Logging {

  val protocolName: String = prefix.replaceAll("protocol\\.", "")
  val protocolID: Int = Configuration.lookupPid(protocolName)
  logger trace s"[${CommonState.getTime}] prefix=$prefix, protocolID=$protocolID"

  private var _node: Node = _

  override def clone: AnyRef = super.clone

  override def toString: String = if (Option(node).isDefined) { s"$protocolName-$getIndex" } else { s"$protocolName-?" }

  override def processEvent(peerSimNode: peersim.core.Node, protocolID: Int, event: Any): Unit =
    event match {
      case callback: Callback => callback.callback.apply()
      case _                  => // Do Nothing
    }

  def node: Node = _node
  def node(node: Node): Unit = _node = node

  def getIndex: Int = _node.getIndex

  def addCallback(delay: Int, callback: Callback): Unit = {
    logger trace s"[${CommonState.getTime}]~delay=$delay, node=$node"
    require(delay > 0)
    require(node != null)
    EDSimulator.add(delay.toLong, callback, node, protocolID)
  }

}

object Scheduler {
  case class Callback(val callback: () => Unit)
}
