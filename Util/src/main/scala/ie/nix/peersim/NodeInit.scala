package ie.nix.peersim

import ie.nix.peersim.Node.defaultNetworkSize
import ie.nix.peersim.Utils.{nodes, numberOfNodes}
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.core.Network

class NodeInit(val prefix: String) extends Control with Logging {

  override def init(): Boolean = {
    resizeNetwork()
    for { node <- nodes } {
      initNode(node)
      initNodesProtocols(node)
    }
    false
  }

  def initNodesProtocols(node: Node): Unit =
    for {
      protocolId <- 0 until node.protocolSize()
      protocol = node.getProtocol(protocolId)
    } protocol match {
      case protocol: Protocol => {
        protocol.node(node)
        initProtocol(node, protocol)
      }
      case scheduler: Scheduler => scheduler.node(node)
      case _                    => // Do nothings
    }

  protected def resizeNetwork(): Unit = {
    val networkSize = Network.size
    if (numberOfNodes > 0 && networkSize != defaultNetworkSize) {
      if (networkSize > defaultNetworkSize)
        shrinkNetwork(networkSize, defaultNetworkSize)
      else
        growNetwork(networkSize, defaultNetworkSize)
    }
    logger debug s"Resizing network from $networkSize to $defaultNetworkSize"
  }

  protected def shrinkNetwork(networkSize: Int, newNetworkSize: Int): Unit =
    for (_ <- newNetworkSize until networkSize) Network.remove

  protected def growNetwork(networkSize: Int, newNetworkSize: Int): Unit = {
    for (_ <- networkSize until newNetworkSize) {
      val newNode = as[Node](Network.prototype).clone()
      Network.add(newNode)
      initNode(newNode)
      initNodesProtocols(newNode)
      logger debug s"Resizing network from $networkSize to $newNetworkSize, created $newNode"
    }
  }

  def initNode(node: Node): Unit = { logger trace s"node=$node" }

  def initProtocol(node: Node, protocol: Protocol): Unit = { logger trace s"node=$node, protocol=$protocol" }

}
