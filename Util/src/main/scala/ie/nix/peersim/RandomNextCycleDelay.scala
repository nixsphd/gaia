package ie.nix.peersim

import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState
import peersim.edsim.NextCycleEvent

class RandomNextCycleDelay(val prefix: String) extends NextCycleEvent(prefix) with Logging {

  override protected def nextDelay(step: Long): Long = {
    val nextGaussian = CommonState.r.nextDouble()
    val currentDelay = now % step
    val nextDelay = ((step * nextGaussian) + step - currentDelay).toLong
    logger trace s"[$now] currentDelay=$currentDelay, step=$step, nextGaussian=$nextGaussian, nextDelay=$nextDelay"
    super.nextDelay(nextDelay)
  }

}
