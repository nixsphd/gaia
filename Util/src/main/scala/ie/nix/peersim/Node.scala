package ie.nix.peersim

import ie.nix.peersim.Scheduler.Callback
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.{CommonState, GeneralNode}

class Node(val prefix: String) extends GeneralNode(prefix) with Logging {

  logger trace s"[${CommonState.getTime}] prefix=$prefix => $this"

  override def clone: Node = as[Node](super.clone)

  override def toString = s"Node-$getID"

  def addCallback(delay: Int, callback: Callback): Unit = {
    logger trace s"[${CommonState.getTime}]~delay=$delay, node=$this"
    val schedulerProtocolID = Configuration.lookupPid("scheduler")
    this.getProtocol(schedulerProtocolID) match {
      case scheduler: Scheduler => scheduler.addCallback(delay, callback)
      case _                    => logger error s"No Scheduler configured."
    }
  }

}

object Node {

  private val NETWORK_SIZE = "network.size"
  private val NOT_SET = -1

  private var _defaultNetworkSize = NOT_SET

  def defaultNetworkSize: Int =
    if (_defaultNetworkSize == NOT_SET)
      Configuration.getInt(NETWORK_SIZE)
    else
      _defaultNetworkSize

  def defaultNetworkSize(defaultNetworkSize: Int): Unit =
    _defaultNetworkSize = defaultNetworkSize

}
