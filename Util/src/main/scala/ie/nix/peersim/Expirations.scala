package ie.nix.peersim

import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging

trait Expirations extends Logging {
  message: Message =>

  val expiresAt: Long

  override def toString: String = s"${super.toString}, expiresAt=$expiresAt"

  def isExpired: Boolean = now > expiresAt

}
