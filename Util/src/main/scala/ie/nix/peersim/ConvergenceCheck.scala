package ie.nix.peersim

import org.apache.logging.log4j.scala.Logging

abstract class ConvergenceCheck(val prefix: String) extends Control with Logging {

  override def step(): Boolean = isConverged

  def isConverged: Boolean

}
