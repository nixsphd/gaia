package ie.nix.peersim

import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

class Observer(prefix: String) extends Control with Logging {

  logger trace s"Observer($prefix), finalObservation=${CommonState.getEndTime}"

  override protected def step(): Boolean = {
    if (!isFinal) {
      if (!isLastStep) {
        observe()
      }
    } else {
      finalObservation()
    }
    false
  }

  protected def observe(): Unit = {}

  protected def finalObservation(): Unit = observe()

}
