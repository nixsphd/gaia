package ie.nix.peersim

import ie.nix.peersim.EventHandler.Message
import org.apache.logging.log4j.scala.Logging

import scala.language.reflectiveCalls
import scala.reflect.ClassTag

trait MessageStore extends Logging {
  sender: { def node: Node } =>

  private var messages = Vector[Message with Expirations]()

  def clearMessages(): Unit = messages = Vector[Message with Expirations]()

  def clearExpiredMessages(): Unit = messages = messages.filterNot(_.isExpired)
  def addMessage(message: Message with Expirations): Unit = messages = messages :+ message
  def removeMessage(message: Expirations): Unit = messages = messages.filterNot(_ == message)
  def allMessages: Vector[Message with Expirations] = messages
  def hasMessages: Boolean = messages.nonEmpty
  def hasMessages(message: Message with Expirations): Boolean = messages.contains(message)

  def sentMessages: Vector[Message with Expirations] = messages.filter(_.from == sender.node)
  def receivedMessages: Vector[Message with Expirations] = messages.filter(_.to == sender.node)

  def hasSentMessages: Boolean = sentMessages.isEmpty
  def hasReceivedMessages: Boolean = receivedMessages.isEmpty

  def messages[M <: Expirations: ClassTag]: Vector[M] = messages.collect { case message: M         => message }
  def sentMessages[M <: Expirations: ClassTag]: Vector[M] = sentMessages.collect { case message: M => message }
  def receivedMessages[M <: Expirations: ClassTag]: Vector[M] = receivedMessages.collect {
    case message: M => message
  }

  def hasMessage[M <: Expirations: ClassTag]: Boolean = messages[M].nonEmpty
  def hasSentMessage[M <: Expirations: ClassTag]: Boolean = sentMessages[M].nonEmpty
  def hasReceivedMessage[M <: Expirations: ClassTag]: Boolean = receivedMessages[M].nonEmpty

  def liveMessages: Vector[Message with Expirations] = messages.filterNot(_.isExpired)

  def liveMessages[M <: Expirations: ClassTag]: Vector[M] = messages[M].filterNot(_.isExpired)
  def liveSentMessages[M <: Expirations: ClassTag]: Vector[M] = sentMessages[M].filterNot(_.isExpired)
  def liveReceivedMessages[M <: Expirations: ClassTag]: Vector[M] = receivedMessages[M].filterNot(_.isExpired)

  def hasLiveMessage[M <: Expirations: ClassTag]: Boolean = liveMessages[M].nonEmpty
  def hasLiveSentMessage[M <: Expirations: ClassTag]: Boolean = liveSentMessages[M].nonEmpty
  def hasLiveReceivedMessage[M <: Expirations: ClassTag]: Boolean = liveReceivedMessages[M].nonEmpty

}
