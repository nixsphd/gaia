package ie.nix.peersim

import ie.nix.peersim.Logger.{getCSVFileWriter, getLogDirName, getLogFileName, getObserveStep, getShouldAppend}
import ie.nix.peersim.Utils.now
import ie.nix.util.CSVFileWriter
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import peersim.core.CommonState

import java.nio.file.{Files, Paths}

class Logger(prefix: String, observeStep: Long, val csvFileWriter: CSVFileWriter)
    extends Observer(prefix)
    with Logging {

  val logFile = s"${getLogDirName(prefix)}/${getLogFileName(prefix)}"

  logger info s"Logging to $logFile, append=$shouldAppend"

  def this(prefix: String) =
    this(prefix, getObserveStep(prefix), getCSVFileWriter(prefix))

  override protected def init(): Boolean = {
    initLog()
    false
  }

  override protected def step(): Boolean = {
    if (!isFinal) {
      if (!isLastStep) {
        log()
        if ((now % observeStep) == 0)
          observe()
      }
    } else {
      finalLog()
      finalObservation()
      csvFileWriter.close()
    }
    false
  }

  protected def shouldAppend: Boolean = getShouldAppend(prefix)
  protected def initLog(): Unit = {}
  protected def log(): Unit = {}
  protected def finalLog(): Unit = log()

}

object Logger extends Logging {
  val LOG_DIR = "log_dir"
  val LOG_FILE = "log_file"
  val STEP = "step"
  val OBSERVE_STEP = "observe_step"
  val DEFAULT_OBSERVE_STEP: Long = CommonState.getEndTime - 1
  val APPEND: String = "append"

  def getObserveStep(prefix: String): Long =
    Configuration.getLong(s"$prefix.$OBSERVE_STEP", DEFAULT_OBSERVE_STEP)

  def getLogDirName(prefix: String): String =
    Configuration.getString(s"$prefix.$LOG_DIR", Configuration.getString(LOG_DIR, "."))

  def getLogFileName(prefix: String): String =
    Configuration.getString(s"$prefix.$LOG_FILE")

  def getCSVFileWriter(prefix: String): CSVFileWriter =
    CSVFileWriter(getLogDirName(prefix), getLogFileName(prefix), append = getShouldAppend(prefix))

  def getStep(prefix: String): Int =
    Configuration.getInt(s"$prefix.$STEP")

  def getShouldAppend(prefix: String): Boolean =
    Configuration.getBoolean(s"$prefix.$APPEND", false)

}

trait Times extends Logging {
  this: Logger =>

  val startFrom = lastTime

  override def initLog(): Unit =
    if (!shouldAppend || !logFileHasData)
      csvFileWriter.writeHeaders(s"Time,$headers")

  def logTime: Long = startFrom + now

  def logFileHasData: Boolean = Files.size(Paths.get(logFile)) > 0
  def lastTime: Long = {
    val lines = Files.readAllLines(Paths.get(logFile))
    if (lines.isEmpty) 0 else lines.get(lines.size() - 1).split(",")(0).toLong
  }

  def writeRow(row: String): Unit = csvFileWriter.writeRow(s"$logTime, $row")

  def headers: String

}
