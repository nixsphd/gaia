package ie.nix.peersim

import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.cdsim.CDSimulator
import peersim.config.{Configuration, ParsedProperties}
import peersim.core.CommonState
import peersim.edsim.EDSimulator

import java.io.{File, FileOutputStream, PrintStream}

object Utils extends Logging {

  def now: Long = CommonState.getTime

  def numberOfNodes: Int = peersim.core.Network.size
  def node: Node = as[Node](CommonState.getNode)
  def nodeAs[T](implicit toT: Node => T): T = toT(node)
  def node(index: Int): Node = as[Node](peersim.core.Network.get(index))
  def nodeAs[T](index: Int): T = as[T](peersim.core.Network.get(index))
  def nodes: Vector[Node] =
    (0 until numberOfNodes).toVector
      .map(id => as[Node](peersim.core.Network.get(id)))
  def nodesAs[T](implicit toT: Node => T): Vector[T] = nodes.map(toT)

  def protocol(node: Node, protocolID: Int): Protocol =
    as[Protocol](node.getProtocol(protocolID))
  def protocols(protocolID: Int): Vector[Protocol] =
    nodes.map(protocol(_, protocolID))
  def protocolAs[T](node: Node, protocolID: Int): T =
    as[T](node.getProtocol(protocolID))
  def protocolsAs[T](protocolID: Int): Vector[T] =
    nodes.map(protocolAs[T](_, protocolID))

  def isConfigLoaded: Boolean =
    try {
      Configuration.contains("")
      true
    } catch {
      case _: RuntimeException => false
    }

  def initConfiguration(properties: Array[String]): Unit =
    if (!isConfigLoaded)
      peersim.config.Configuration.setConfig(new ParsedProperties(properties))

  def runSimulator() =
    if (CDSimulator.isConfigurationCycleDriven)
      CDSimulator.nextExperiment()
    else if (EDSimulator.isConfigurationEventDriven)
      EDSimulator.nextExperiment()

  def silent(): Unit =
    redirectSystemOutErr("/dev/null", "/dev/null")

  protected def redirectSystemOutErr(outFilename: String, errFilename: String): Unit = {
    logger trace s"redirectSystemOutErr outFilename=$outFilename, errFilename=$errFilename"
    System.setOut(new PrintStream(new FileOutputStream(new File(outFilename))))
    System.setErr(new PrintStream(new FileOutputStream(new File(errFilename))))
  }

}
