package ie.nix.peersim

import ie.nix.peersim.EventHandler.Message
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.config.FastConfig
import peersim.core.{CommonState, Linkable}
import peersim.transport.Transport

import scala.reflect.ClassTag
import scala.util.Random

abstract class EventHandler(override val prefix: String) extends Scheduler(prefix) with Logging {

  override def processEvent(peersimNode: peersim.core.Node, protocolID: Int, event: Any): Unit = {
    super.processEvent(peersimNode, protocolID, event)
    (peersimNode, event) match {
      case (_: Node, runnable: Runnable)  => runnable.run()
      case (node: Node, message: Message) => processEvent(node, message)
      case _                              => // Do Nothing
    }
  }

  def processEvent(node: Node, message: Message): Unit

  def transport: Transport = as[Transport](node.getProtocol(FastConfig.getTransport(protocolID)))

  def neighbourhood: Linkable = as[Linkable](node.getProtocol(FastConfig.getLinkable(protocolID)))

  def neighbour(index: Int): Node = as[Node](neighbourhood.getNeighbor(index))

  def numberOfNeighbours: Int = neighbourhood.degree()

  def neighbourAs[T](index: Int): T = as[T](neighbour(index))

  def neighbours: Vector[Node] = neighboursAs[Node]

  def neighboursAs[T]: Vector[T] =
    (0 until neighbourhood.degree())
      .map(neighbourIndex => as[T](neighbourhood.getNeighbor(neighbourIndex)))
      .toVector

  def randomNeighbour: Node = neighbours(Random.nextInt(numberOfNeighbours))

  def randomNeighbourAs[T]: T = as[T](neighbours(Random.nextInt(numberOfNeighbours)))

  def sendMessage(message: Message): Unit = {
    transport.send(node, message.to, message, protocolID)
    logger trace s"[${CommonState.getTime}]~$node sent $message to ${message.to} protocolID=$protocolID"
  }
}

object EventHandler {

  class Message(val from: Node, val to: Node) {

    override def toString: String = s"${getClass.getSimpleName}[$from->$to]"

    def fromAs[N <: Node: ClassTag]: N = from match {
      case n: N => n
    }

    def toAs[N <: Node: ClassTag]: N = to match {
      case n: N => n
    }

  }

}
