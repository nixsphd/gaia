package ie.nix.peersim

import ie.nix.peersim.Gossiping.Gossips
import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging

trait Gossiping extends Logging {
  protocol: Protocol =>

  def gossipMessage(message: Message): Unit = {
    logger trace s"[$now] message=$message"
    message match {
      case gossips: Gossips if gossips.gossipCounter > 0 =>
        neighbours.foreach(neighbour => {
          val message = gossips.copy(to = neighbour, gossips.gossipCounter - 1)
          logger trace s"[$now]~Gossiping $message"
          sendMessage(message)
        })
      case _ => // Do nothing, subclasses will handle this
    }
  }

}

object Gossiping extends Logging {

  trait Gossips {
    message: Message =>

    val gossipCounter: Int

    override def toString: String = s"${super.toString}, gossipCounter=$gossipCounter"

    def copy(to: Node = this.to, gossipCounter: Int = this.gossipCounter): Message with Gossips

  }

}
