package ie.nix.peersim

import ie.nix.peersim.Control.{name, register}
import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

trait Control extends peersim.core.Control with Logging {

  override final def execute: Boolean = {
    if (CommonState.getTime == 0) {
      register(name(this), this)
      init()
    } else {
      logger trace s"[$now] isFinal=$isFinal, isLastStep=$isLastStep"
      if (!isFinal) {
        if (!isLastStep) {
          step()
        }
      } else {
        finalStep()
      }
    }
    false
  }

  protected def isFinal: Boolean = CommonState.getPhase == CommonState.POST_SIMULATION

  protected def isLastStep: Boolean = now == (CommonState.getEndTime - 1)

  protected def init(): Boolean = false
  protected def step(): Boolean = false
  protected def finalStep(): Boolean = step()

}

object Control extends Logging {

  var controls: Map[String, Control] = Map[String, Control]()

  def name(control: Control): String = control.getClass.getSimpleName

  def register(name: String, control: Control): Unit = {
    logger debug s"registering $name -> $control"
    controls += (name -> control)
  }

  def controlFor(name: String): Option[Control] = {
    val control = controls.get(name)
    logger debug s"controlFor $name -> $control  ${Control.controls.mkString}"
    control
  }

}
