package ie.nix.ecj

import ec.EvolutionState
import ec.gp.koza.KozaFitness
import ec.util.Parameter
import ie.nix.ecj.Statistics.P_FILE
import org.apache.logging.log4j.scala.Logging

import java.io._
import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.collection.SortedMap

class Statistics extends ec.Statistics with Logging {

  protected var fileLog: Int = _
  protected var wroteHeaders = false

  override def setup(state: EvolutionState, base: Parameter): Unit = {
    super.setup(state, base)
    val file: File = state.parameters.getFile(base.push(P_FILE), null)
    if (file != null) {
      try fileLog = state.output.addLog(file, true)
      catch {
        case ioException: IOException =>
          state.output.fatal(s"An IOException occurred while trying to create the log $file:\n$ioException")
      }
    }
  }

  override def postEvaluationStatistics(state: EvolutionState): Unit = {
    super.postEvaluationStatistics(state)
    writeStatistics(state)
  }

  def writeStatistics(state: EvolutionState): Unit =
    for {
      subPop <- state.population.subpops.asScala
      individualIndex <- 0 until subPop.individuals.size()
      individual = subPop.individuals.get(individualIndex)
      fitness = individual.fitness
      generation = state.generation
    } {
      fitness match {
        case kozaFitnessWithStats: KozaFitnessWithStats => {
          val stats = kozaFitnessWithStats.getStats()
          writeHeaders(state, Iterable[String]("generation", "individual", "fitness") ++ stats.map(toStatName))
          writeRow(state,
                   Iterable[String](generation.toString,
                                    individualIndex.toString,
                                    kozaFitnessWithStats.adjustedFitness().toString) ++ stats.map(toStatAverage))
        }
        case _ => {
          writeHeaders(state, Iterable[String]("generation", "individual", "fitness"))
          writeRow(state, Iterable[String](generation.toString, individualIndex.toString, fitness.fitness().toString))
        }
      }
    }

  protected def toStatName(stat: Stat): String = stat.name
  protected def toStatAverage(stat: Stat): String = stat.average.toString

  protected def writeHeaders(state: EvolutionState, headerNames: Iterable[String]): Unit =
    if (!wroteHeaders) {
      val headers = headerNames.mkString(",")
      state.output.println(headers, fileLog)
      wroteHeaders = true
      logger trace s"headers=$headers"
    }

  def writeRow(state: EvolutionState, rowValues: Iterable[String]): Unit = {
    val row = rowValues.mkString(",")
    state.output.println(row, fileLog)
    logger trace s"row=$row"
  }

}

object Statistics {
  protected val P_FILE = "file"
}

class KozaFitnessWithStats extends KozaFitness with Logging {

  protected var statMap: SortedMap[String, Stat] = SortedMap[String, Stat]()

  def recordStat(statName: String, statValue: Double): Unit = {
    val stat: Stat = getStat(statName)
    statMap = statMap + (statName -> stat.accept(statValue))
    logger trace s"$statName=$stat"
  }

  def numberOfStats: Int = statMap.size
  def getStats(): Iterable[Stat] = statMap.values
  def getStat(statName: String): Stat = statMap.getOrElse(statName, new Stat(statName))
  def clearStats(): Unit = statMap = SortedMap[String, Stat]()

  @throws[IOException]
  override def writeFitness(state: EvolutionState, dataOutput: DataOutput): Unit = {
    super.writeFitness(state, dataOutput)
    writeStats(dataOutput)
  }

  @throws[IOException]
  protected def writeStats(dataOutput: DataOutput): Unit = {
    dataOutput.writeInt(statMap.size)
    statMap.values.foreach(_.write(dataOutput))
  }

  @throws[IOException]
  override def readFitness(state: EvolutionState, dataInput: DataInput): Unit = {
    super.readFitness(state, dataInput)
    readStats(dataInput)
  }

  @throws[IOException]
  protected def readStats(dataInput: DataInput): Unit = {
    val numberOfStats: Int = dataInput.readInt
    for (_ <- 0 until numberOfStats) {
      val stat = Stat.read(dataInput)
      statMap = statMap + (stat.name -> stat)
    }
  }

}

@SerialVersionUID(1)
class Stat(val name: String,
           val count: Long = 0l,
           val sum: Double = 0d,
           val min: Double = Double.PositiveInfinity,
           val max: Double = Double.NegativeInfinity)
    extends Serializable
    with Logging {

  def accept(value: Double): Stat =
    new Stat(name, count + 1, sum + value, Math.min(min, value), Math.max(max, value))

  def average: Double = sum / count

  @throws[IOException]
  def write(dataOutput: DataOutput): Unit = {
    dataOutput.writeUTF(name)
    dataOutput.writeLong(count)
    dataOutput.writeDouble(sum)
    dataOutput.writeDouble(min)
    dataOutput.writeDouble(max)
  }

  override def equals(otherObject: Any): Boolean = {
    otherObject match {
      case otherStat: Stat =>
        (name == otherStat.name) &&
          (count == otherStat.count) &&
          (sum == otherStat.sum) &&
          (min == otherStat.min) &&
          (max == otherStat.max)
      case _ => false
    }
  }

  override def toString: String =
    s"Stat[$name, count=$count, sum=$sum, min=$min, max=$max"
}

object Stat {

  @throws[IOException]
  def read(dataInput: DataInput): Stat = {
    val statName: String = dataInput.readUTF
    val count: Long = dataInput.readLong
    val sum: Double = dataInput.readDouble
    val min: Double = dataInput.readDouble
    val max: Double = dataInput.readDouble
    new Stat(statName, count, sum, min, max)
  }

}
