package ie.nix.ecj.gp

import ec.gp.{ADFStack, GPData, GPIndividual, GPNode}
import ec.{EvolutionState, Problem}
import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Node.{Eval, NullNode, notANodeError}
import org.apache.logging.log4j.scala.Logging

import java.util.function.BiConsumer
import java.io.Serializable

class Node protected (val symbol: String, val numberOfChildren: Int, val eval: Eval) extends GPNode with Serializable {
//    with OccamStatistics.OccamStatisticsNode {

  // This is just for unit testing
  children = new Array[GPNode](numberOfChildren)

//  logger trace s"symbol=$symbol, numberOfChildren=$numberOfChildren, eval=$eval"

//  def this(symbol: String, numberOfChildren: Int, eval: Eval) =
//    this(symbol, symbol, numberOfChildren, eval)

  override def toString: String = symbol

  override def toStringForHumans: String = symbol

  override def expectedChildren: Int = numberOfChildren

  override def eval(state: EvolutionState,
                    thread: Int,
                    input: GPData,
                    stack: ADFStack,
                    individual: GPIndividual,
                    problem: Problem): Unit = {
    val childrensResults = Array.fill[TypedData](numberOfChildren)(new TypedData())
    for (child <- 0 until numberOfChildren) {
      children(child).eval(state, thread, childrensResults(child), stack, individual, problem)
    }
    if (eval != null) {
      val result = input.asInstanceOf[TypedData]
      eval.accept(childrensResults, result)
    }
  }

  def childNodes: IndexedSeq[Node] =
    (0 to expectedChildren).map(childAt)

  def childAt(at: Int): Node = {
    gpChildAt(at) match {
      case node: Node => node
      case notANode   => notANodeError(notANode)
    }
  }

  def gpChildAt(at: Int): GPNode = if (at < children.length) children(at) else NullNode

  /*
   * Occam Statistics APIs
   */
//  override def evalConstant: TypedData = {
//    val result = new TypedData
//    val childrensResults = new Array[TypedData](expectedChildren)
//    for (c <- 0 until expectedChildren) {
//      childrensResults(c) = childAt(c).asInstanceOf[OccamStatistics.OccamStatisticsNode].evalConstant
//    }
//    eval.accept(childrensResults, result)
//    Node.LOGGER.trace("result={}", result)
//    result
//  }
//
//  def equals(`object`: Node): Boolean = {
//    val equals = false
//    Node.LOGGER.trace("equals={}", equals)
//    equals
//  }
}

object Node extends Logging {
  type Eval = BiConsumer[Array[TypedData], TypedData]
  object NullNode extends Node("NullNode", 0, (_, _) => ())

  def notANodeError(notANode: Any): Node = {
    logger error s"The node id the wrong type, expected Noxe, got ${notANode.getClass.getSimpleName}"
    NullNode
  }

}
