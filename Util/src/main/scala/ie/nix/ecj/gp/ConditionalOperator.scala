package ie.nix.ecj.gp

import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Node.Eval

class ConditionalOperator protected (symbol: String, eval: Eval) extends Node(symbol, 3, eval) {

//    override def isRedundant: Boolean = children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isConstant
//
//    override def getNextRelevantChild: OccamStatistics.OccamStatisticsNode = {
//        val result = children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].evalConstant
//        if (result.getBoolean) children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        else children(2).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//    }
//
//    override def toOccamStringForHumansFunction: String = {
//        val occamString = "(" + children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].toOccamStringForHumans + " ? " + children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].toOccamStringForHumans + " : " + children(2).asInstanceOf[OccamStatistics.OccamStatisticsNode].toOccamStringForHumans + ")"
//        occamString
//    }
}

object ConditionalOperator {

  class IfDouble()
      extends ConditionalOperator("ifd",
                                  (results: Array[TypedData], result: TypedData) =>
                                    result.set(
                                      if (results(0).getBoolean)
                                        results(1).getDouble
                                      else
                                        results(2).getDouble))

  class IfBoolean()
      extends ConditionalOperator("ifb",
                                  (results: Array[TypedData], result: TypedData) =>
                                    result.set(
                                      if (results(0).getBoolean)
                                        results(1).getBoolean
                                      else
                                        results(2).getBoolean)) {}
}
