package ie.nix.ecj.gp

import ie.nix.ecj.gp.Node.Eval
import org.apache.logging.log4j.scala.Logging

class Constant(symbol: String, eval: Eval) extends Node(symbol, 0, eval)
//{
//    override def isConstant: Boolean = {
//        val isConstant = true
//        LOGGER.trace("node={}, isConstant={}", this, isConstant)
//        isConstant
//    }

//    override def equals(`object`: Any): Boolean = {
//        val equals = `object`.isInstanceOf[OccamStatistics.OccamStatisticsNode] && `object`.asInstanceOf[OccamStatistics.OccamStatisticsNode].isConstant && evalConstant == `object`.asInstanceOf[OccamStatistics.OccamStatisticsNode].evalConstant
//        LOGGER.trace("equals={}", equals)
//        equals
//    }
//}

object Constant extends Logging {

//    def apply(symbol: String, stringForHumans: String, doubeValue: Double): Constant =
//        this(symbol, stringForHumans, 0, (_: Vector[TypedData], result: TypedData) => {
//            result.set(doubeValue)
//            logger trace s"result.doubeValue=${result.getDouble}"
//        })
//
//    def apply(doubeValue: Double) =
//        this(String.format("%.1f", doubeValue), 0, (_: Vector[TypedData], result: TypedData) => {
//            result.set(doubeValue)
//            logger trace s"result.doubeValue=${result.getDouble}"
//        })
//
//
//
//    def apply(booleanValue: Boolean) {
//        this()
//        super (String.valueOf(booleanValue), 0, (results: Vector[TypedData], result: TypedData) => {
//            def foo(results: Array[TypedData], result: TypedData) = {
//                result.asInstanceOf[TypedData].set(booleanValue)
//                LOGGER.trace("result.booleanValue={}", result.asInstanceOf[TypedData].getBoolean)
//            }
//
//            foo(results, result)
//        })
//    }
//
//    def apply(symbol: String, booleanValue: Boolean) {
//        this()
//        super (symbol, 0, (results: Array[TypedData], result: TypedData) => {
//            def foo(results: Array[TypedData], result: TypedData) = {
//                result.asInstanceOf[TypedData].set(booleanValue)
//                LOGGER.trace("result.booleanValue={}", result.asInstanceOf[TypedData].getBoolean)
//            }
//
//            foo(results, result)
//        })
//    }

  class BooleanConstant(symbol: String, value: Boolean) extends Constant(symbol, (_, result) => result.set(value)) {
    def this(value: Boolean) = this(value.toString, value)
  }

  class True() extends BooleanConstant("true", true) {}

  class False() extends BooleanConstant("false", false) {}

  class DoubleConstant(symbol: String, value: Double) extends Constant(symbol, (_, result) => result.set(value)) {
    def this(value: Double) = this(value.toString, value)
  }

  class Zero() extends DoubleConstant("0.0", 0d) {
//    override def isZero = true
  }

  class One() extends DoubleConstant("1.0", 1d) {
//    override def isOne = true
  }

  class Five() extends DoubleConstant("5.0", 5d) {}

  class Ten() extends DoubleConstant("10.0", 10d) {}

  class Hundred() extends DoubleConstant("100.0", 100d) {}

  class Thousand() extends DoubleConstant("1000.0", 1000d) {}

  class TenThousand() extends DoubleConstant("10000.0", 1000d) {}

  class PositiveInfinity() extends DoubleConstant("positiveInfinity", Double.PositiveInfinity) {}

  class NegativeInfinity() extends DoubleConstant("negativeInfinity", Double.NegativeInfinity) {}

  class PI() extends DoubleConstant("PI", Math.PI)

  class E() extends DoubleConstant("E", Math.E)

  class Phi() extends DoubleConstant("PHI", 1 + Math.sqrt(5) / 2) {}
}
