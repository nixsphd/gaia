package ie.nix.ecj.gp

import ie.nix.ecj.TypedData

class ArithmeticOperator protected (symbol: String, numberOfChildren: Int, eval: Node.Eval)
    extends Node(symbol, numberOfChildren, eval) {}

object ArithmeticOperator {

  class Add()
      extends ArithmeticOperator(
        "+",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble + results(1).getDouble))
//    {
//        override def isRedundant: Boolean = {
//            val isRedundant = children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero || children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero
//            LOGGER.trace("isRedundant={}", isRedundant)
//            isRedundant
//        }
//
//        override def getNextRelevantChild: OccamStatistics.OccamStatisticsNode = if (children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero) children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        else children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//    }

  class Minus()
      extends ArithmeticOperator("-",
                                 1,
                                 (results: Array[TypedData], result: TypedData) => result.set(-results(0).getDouble)) {}

  class Sub()
      extends ArithmeticOperator(
        "-",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble - results(1).getDouble))
//  {
//    override def isConstant: Boolean = {
//      val isConstant = super.isConstant || children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode] == children(1)
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
//
//    override def evalConstant: TypedData = {
//      var result = new TypedData
//      if (super.isConstant) result = super.evalConstant
//      else result.set(0d)
//      LOGGER.trace("result={}", result)
//      result
//    }
//
//    override def isRedundant: Boolean = children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero
//
//    override def getNextRelevantChild: OccamStatistics.OccamStatisticsNode =
//      if (children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero)
//        children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else null
//  }

  @SerialVersionUID(1)
  class Mul()
      extends ArithmeticOperator(
        "*",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble * results(1).getDouble))
//  {
//    override def isConstant: Boolean = {
//      val isConstant = super.isConstant || children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .isZero || children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
//
//    override def evalConstant: TypedData = {
//      var result = new TypedData
//      if (super.isConstant) result = super.evalConstant
//      else result.set(0d)
//      LOGGER.trace("result={}", result)
//      result
//    }
//
//    override def isRedundant: Boolean =
//      children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isOne || children(1)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .isOne
//
//    override def getNextRelevantChild: OccamStatistics.OccamStatisticsNode =
//      if (children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isOne)
//        children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else if (children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isOne)
//        children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else null
//  }

  @SerialVersionUID(1)
  class Div()
      extends ArithmeticOperator(
        "div",
        2,
        (results: Array[TypedData], result: TypedData) => {
          if (results(1).getDouble == 0) result.set(0)
          else result.set(results(0).getDouble / results(1).getDouble)
        }
      )
//  {
//    override def isConstant: Boolean = {
//      val isConstant = super.isConstant || children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode] == children(1) || (children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .isZero || children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero)
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
//
//    override def evalConstant: TypedData = {
//      var result = new TypedData
//      if (children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode] == children(1)) result.set(1d)
//      else if (children(0)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isZero || children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero) result.set(0d)
//      else result = super.evalConstant
//      LOGGER.trace("result={}", result)
//      result
//    }
//
//    override def toOccamStringForHumansBinaryOperator: String =
//      toStringForHumans + "(" + childAt(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .toOccamStringForHumans + ", " + childAt(1)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .toOccamStringForHumans + ")"
//  }
}
