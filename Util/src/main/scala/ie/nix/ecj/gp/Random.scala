package ie.nix.ecj.gp

import ec.gp.{ADFStack, GPData, GPIndividual}
import ec.{EvolutionState, Problem}
import ie.nix.ecj.TypedData
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

class Random() extends Node("rand", 0, (_, _) => {}) with Logging {

  override def eval(state: EvolutionState,
                    thread: Int,
                    input: GPData,
                    stack: ADFStack,
                    individual: GPIndividual,
                    problem: Problem): Unit = {
    val result = as[TypedData](input)
    result.set(state.random(thread).nextDouble)
    logger trace s"result=$result"
  }

//  override def isConstant = false

//  override def evalConstant: TypedData = {
//    Random.LOGGER.error("ERROR this should never be called on class {}", getClass.getName)
//    throw new RuntimeException("evalConstant should never be called on Random")
//  }

  override def toStringForHumans: String = {
    val occamString = symbol + "()"
    occamString
  }
}
