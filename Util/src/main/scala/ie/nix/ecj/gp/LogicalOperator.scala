package ie.nix.ecj.gp

import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Node.Eval

abstract class LogicalOperator protected (symbol: String, numberOfChildren: Int, eval: Eval)
    extends Node(symbol, numberOfChildren, eval) {}

object LogicalOperator {

  class Not()
      extends LogicalOperator(
        "not",
        1,
        (childrensResults: Array[TypedData], result: TypedData) => result.set(!childrensResults(0).getBoolean)) {

    override def toStringForHumans: String = "!"

//    override def isConstant: Boolean = {
//      val isConstant = children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isConstant
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
//
//    override def isRedundant: Boolean =
//      children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isInstanceOf[LogicalOperator.Not]
//
//    override def getNextRelevantChild: OccamStatistics.OccamStatisticsNode =
//      (children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode])
//        .childAt(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
  }

  class And()
      extends LogicalOperator("and", 2, (childrensResults: Array[TypedData], result: TypedData) => {
        result.set(childrensResults(0).getBoolean && childrensResults(1).getBoolean)
      }) {

    override def toStringForHumans: String = "&&"

//      override def isConstant: Boolean = {
//      val isConstant = Stream
//        .of(children)
//        .allMatch((child: GPNode) => child.asInstanceOf[OccamStatistics.OccamStatisticsNode].isConstant)
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
  }

  class Or()
      extends LogicalOperator("or", 2, (childrensResults: Array[TypedData], result: TypedData) => {
        result.set(childrensResults(0).getBoolean || childrensResults(1).getBoolean)
      }) {

    override def toStringForHumans: String = "||"
//    override def isConstant: Boolean = {
//      val isConstant = Stream
//        .of(children)
//        .anyMatch(
//          (child: GPNode) =>
//            child
//              .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//              .isConstant && child.asInstanceOf[OccamStatistics.OccamStatisticsNode].evalConstant.getBoolean == true)
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
//
//    override def evalConstant: TypedData = {
//      val result = new TypedData
//      result.set(true)
//      LOGGER.trace("result={}", result)
//      result
//    }
  }

}
