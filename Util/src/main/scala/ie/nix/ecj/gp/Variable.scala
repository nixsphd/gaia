package ie.nix.ecj.gp

import ie.nix.ecj.gp.Node.Eval

abstract class Variable(symbol: String, eval: Eval) extends Node(symbol, 0, eval) {

//  override def equals(`object`: Any): Boolean = {
//    val equals = this.getClass == `object`.getClass
//    logger trace s"equals=$equals"
//    equals
//  }
//
//  override def isConstant: Boolean = {
//    val isConstant = false
//    logger trace s"node=$this, isConstant=$isConstant"
//    isConstant
//  }

}

object Variable {

  object A { var value = false }
  class A() extends Variable("a", (_, result) => result.set(A.value))

  object B { var value = false }
  class B() extends Variable("b", (_, result) => result.set(B.value))

  object C { var value = false }
  class C() extends Variable("c", (_, result) => result.set(C.value))

  object X { var value = 0d }
  class X() extends Variable("x", (_, result) => result.set(X.value))

  object Y { var value = 0d }
  class Y() extends Variable("y", (_, result) => result.set(Y.value))

  object Z { var value = 0d }
  class Z() extends Variable("z", (_, result) => result.set(Z.value))

}
