package ie.nix.ecj.gp

import ec.{EvolutionState, Problem}
import ec.gp.{ADFStack, GPData, GPIndividual, GPNode}
import ec.util.Code
import ie.nix.ecj.TypedData

import java.io.{DataInput, DataOutput, IOException}

class ERC protected (var max: Int) extends ec.gp.ERC { //} with { //OccamStatistics.OccamStatisticsNode {

  var value = 0d

  override def toString(): String = s"$name[$value]"

  override def name: String = super.name + max

  override def encode: String = Code.encode(value)

  override def eval(state: EvolutionState,
                    thread: Int,
                    input: GPData,
                    stack: ADFStack,
                    individual: GPIndividual,
                    problem: Problem): Unit = {
    val data = input.asInstanceOf[TypedData]
    data.set(value)
  }

  override def nodeEquals(node: GPNode): Boolean =
    node.isInstanceOf[ERC] && (node.asInstanceOf[ERC].value == value)

  override def resetNode(state: EvolutionState, threadnum: Int): Unit =
    value = state.random(threadnum).nextInt(max).doubleValue()

  override def mutateERC(state: EvolutionState, threadnum: Int): Unit =
    value += state.random(threadnum).nextGaussian

  override def toStringForHumans: String = this.value.toString

  /*
   * Occam Statistics
   */
//
//    override def isConstant = true
//
//    override def evalConstant: TypedData = {
//        val result = new TypedData
//        result.set(value)
//        result
//    }
//
//    override def childAt(at: Int): OccamStatistics.OccamStatisticsNode = children(at).asInstanceOf[OccamStatistics.OccamStatisticsNode]

  @throws[IOException]
  override def writeNode(state: EvolutionState, dataOutput: DataOutput): Unit = {
    dataOutput.writeDouble(value)
    dataOutput.writeInt(max)
  }

  @throws[IOException]
  override def readNode(state: EvolutionState, dataInput: DataInput): Unit = {
    value = dataInput.readDouble
    max = dataInput.readInt
  }

}

object ERC {

  class ERC1() extends ERC(1) {
    override def resetNode(state: EvolutionState, threadnum: Int): Unit = {
      value = state.random(threadnum).nextDouble
    }
  }

  class ERC10() extends ERC(10)

  class ERC100() extends ERC(100)

  class ERC1000() extends ERC(1000)

}
