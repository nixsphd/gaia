package ie.nix.ecj.gp

import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Node.Eval

class RelationalOperator protected (symbol: String, numberOfChildren: Int, eval: Eval)
    extends Node(symbol, numberOfChildren, eval) {

//  override def isConstant: Boolean = {
//    val isConstant = Stream
//      .of(children)
//      .allMatch((child: GPNode) => child.asInstanceOf[OccamStatistics.OccamStatisticsNode].isConstant) || children(0) == children(
//      1)
//    RelationalOperator.LOGGER.trace("isConstant={}", isConstant)
//    isConstant
//  }
//
//  override def evalConstant: TypedData = {
//    var result = new TypedData
//    if (children(0) == children(1)) result.set(true)
//    else result = super.evalConstant
//    RelationalOperator.LOGGER.trace("result={}", result)
//    result
//  }

}

object RelationalOperator {

  class Equals()
      extends RelationalOperator(
        "==",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble == results(1).getDouble)) {}

  class NotEquals()
      extends RelationalOperator(
        "!=",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble != results(1).getDouble)) {

//    override def evalConstant: TypedData = {
//      var result = new TypedData
//      if (children(0) == children(1)) result.set(false)
//      else result = super.evalConstant
//      LOGGER.trace("result={}", result)
//      result
//    }

  }

  class GreaterThan()
      extends RelationalOperator(
        "gt",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble > results(1).getDouble)) {

    override def toStringForHumans: String = ">"

//    override def isConstant: Boolean = {
//      val isConstant = super.isConstant || (children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .isZero && children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive) || (children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .isPositive && children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero)
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
//
//    override def evalConstant: TypedData = {
//      var result = new TypedData
//      if (children(0) == children(1)) result.set(false)
//      else if (children(0)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isZero && children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive) result.set(false)
//      else if (children(0)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isPositive && children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero) result.set(true)
//      else result = super.evalConstant
//      LOGGER.trace("result={}", result)
//      result
//    }

  }

  class GreaterThanOrEquals()
      extends RelationalOperator(
        "ge",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble >= results(1).getDouble)) {

    override def toStringForHumans: String = ">="

//      override def isConstant: Boolean = {
//      val isConstant = super.isConstant || (children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .isPositive && children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero)
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
//
//    override def evalConstant: TypedData = {
//      var result = new TypedData
//      if (children(0) == children(1)) result.set(false)
//      else if (children(0)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isPositive && children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero) result.set(true)
//      else result = super.evalConstant
//      LOGGER.trace("result={}", result)
//      result
//    }

  }

  class LessThan()
      extends RelationalOperator(
        "lt",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble < results(1).getDouble)) {

    override def toStringForHumans: String = "<"

//      override def isConstant: Boolean = {
//      val isConstant = super.isConstant || (children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .isPositive && children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero)
//      LOGGER.trace("isConstant={}", isConstant)
//      isConstant
//    }
//
//    override def evalConstant: TypedData = {
//      var result = new TypedData
//      if (children(0) == children(1)) result.set(false)
//      else if (children(0)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isPositive && children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero) result.set(false)
//      else result = super.evalConstant
//      LOGGER.trace("result={}", result)
//      result
//    }

  }

  class LessThanOrEquals()
      extends RelationalOperator(
        "le",
        2,
        (results: Array[TypedData], result: TypedData) => result.set(results(0).getDouble <= results(1).getDouble)) {

    override def toStringForHumans: String = "<="

  }
}
