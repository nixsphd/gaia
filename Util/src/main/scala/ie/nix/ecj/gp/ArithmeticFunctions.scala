package ie.nix.ecj.gp

import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Node.Eval

class ArithmeticFunctions protected (symbol: String, numberOfChildren: Int, eval: Eval)
    extends Node(symbol, numberOfChildren, eval) {

//  override def toStringForHumansBinaryOperator: String = {
//    val occamString = toStringForHumans + "(" + children(0)
//      .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      .toOccamStringForHumans + ", " + children(1)
//      .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      .toOccamStringForHumans + ")"
//    occamString
//  }

}

object ArithmeticFunctions {

  class Abs()
      extends ArithmeticFunctions(
        "abs",
        1,
        (results: Array[TypedData], result: TypedData) => result.set(Math.abs(results(0).getDouble)))
//  {
//      override def isRedundant: Boolean = {
//      val isRedundant = children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive
//      LOGGER.trace("isRedundant={}", isRedundant)
//      isRedundant
//    }
//
//    override def getNextRelevantChild: OccamStatistics.OccamStatisticsNode =
//      children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//
//    override def toOccamStringForHumansUnaryOperator: String = {
//      val occamString = toStringForHumans + "(" + children(0)
//        .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//        .toOccamStringForHumans + ")"
//      occamString
//    }
//  }

  class Min()
      extends ArithmeticFunctions(
        "min",
        2,
        (results: Array[TypedData], result: TypedData) =>
          result.set(
            if (results(0).getDouble < results(1).getDouble)
              results(0).getDouble
            else
              results(1).getDouble)
      )
//  {
//      override def isRedundant: Boolean = {
//      var isRedundant = children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode] == children(1)
//      isRedundant = isRedundant || children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive && children(
//        1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero
//      isRedundant = isRedundant || children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive && children(
//        0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero
//      LOGGER.trace("isRedundant={}", isRedundant)
//      isRedundant
//    }
//
//    override def getNextRelevantChild: OccamStatistics.OccamStatisticsNode =
//      if (children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode] == children(1))
//        children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else if (children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive && children(1)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isZero) children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else if (children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive && children(0)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isZero) children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else super.getNextRelevantChild
//  }

  class Max()
      extends ArithmeticFunctions(
        "max",
        2,
        (results: Array[TypedData], result: TypedData) =>
          result.set(
            if (results(0).getDouble > results(1).getDouble)
              results(0).getDouble
            else
              results(1).getDouble)
      )
//  {
//      override def isRedundant: Boolean = {
//      var isRedundant = children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode] == children(1)
//      isRedundant = isRedundant || children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive && children(
//        1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero
//      isRedundant = isRedundant || children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive && children(
//        0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isZero
//      LOGGER.trace("isRedundant={}", isRedundant)
//      isRedundant
//    }
//
//    override def getNextRelevantChild: OccamStatistics.OccamStatisticsNode =
//      if (children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode] == children(1))
//        children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else if (children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive && children(1)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isZero) children(0).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else if (children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode].isPositive && children(0)
//                 .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//                 .isZero) children(1).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      else super.getNextRelevantChild
//  }

}
