package ie.nix.ecj

import ec.gp.ge.{GEIndividual, GESpecies}
import ec.gp.{GPIndividual, GPNode}
import ec.simple.SimpleStatistics
import ec.util.Parameter
import ec.{EvolutionState, Individual}
import org.apache.logging.log4j.scala.Logging

class OccamStatistics extends SimpleStatistics with Logging {

  private var headers: IndexedSeq[String] = _
  private var footers: IndexedSeq[String] = _

  override def setup(state: EvolutionState, base: Parameter): Unit = {
    super.setup(state, base)

    val theNumberOfTrees = numberOfTrees(state, base)
    logger trace s"theNumberOfTrees=$theNumberOfTrees"

    headers = for (tree <- 0 until theNumberOfTrees) yield treeHeaderParam(state, base, tree)
    logger trace s"headers=$headers"

    footers = for (tree <- 0 until theNumberOfTrees) yield treeFooterParam(state, base, tree)
    logger trace s"footers=$footers"

  }

  protected def numberOfTrees(state: EvolutionState, base: Parameter): Int =
    state.parameters.getIntWithDefault(base.push(OccamStatistics.P_NUMBER_OF_TREES), null, 0)

  protected def treeHeaderParam(state: EvolutionState, base: Parameter, tree: Int): String = {
    val treeHeader = "tree." + tree + "." + OccamStatistics.P_HEADER
    state.parameters.getStringWithDefault(base.push(treeHeader), null, "")
  }

  protected def treeFooterParam(state: EvolutionState, base: Parameter, tree: Int): String = {
    val treeFooter = "tree." + tree + "." + OccamStatistics.P_FOOTER
    state.parameters.getStringWithDefault(base.push(treeFooter), null, "")
  }

  override def postEvaluationStatistics(state: EvolutionState): Unit = {
    super.postEvaluationStatistics(state)
    state.output.println("*** Occam Trees ***", statisticslog)
    for (x <- 0 until state.population.subpops.size) {
      state.output.println("Subpopulation " + x + ":", statisticslog)
      writeOccamStatistics(state, best_of_run(x))
    }
  }

  /** Logs the best individual of the run. */
  override def finalStatistics(state: EvolutionState, result: Int): Unit = {
    super.finalStatistics(state, result)
    state.output.println("*** Occam Trees ***", statisticslog)
    for (x <- 0 until state.population.subpops.size) {
      state.output.println("Subpopulation " + x + ":", statisticslog)
      writeOccamStatistics(state, best_of_run(x))
    }
  }

  protected def writeOccamStatistics(state: EvolutionState, bestIndividualOfRun: Individual): Unit = {
    bestIndividualOfRun match {
      case pIndividual: GPIndividual => printTrees(state, statisticslog, pIndividual)
      case individual: GEIndividual =>
        val bestGPIndividualOfRun =
          (bestIndividualOfRun.species.asInstanceOf[GESpecies]).map(state, individual, 0, null)
        if (bestGPIndividualOfRun != null) printTrees(state, statisticslog, bestGPIndividualOfRun)
        else
          state.output.println(
            "Error printing tree, individual is of type " + "GEIndividual but the corresponding GPIndividual could " + "not be retrived.",
            statisticslog)
      case _ =>
        state.output.println(
          "Error printing tree, individual is of type " + bestIndividualOfRun.getClass + " but should be of " + classOf[
            GPIndividual],
          statisticslog)
    }
  }

  protected def printTrees(state: EvolutionState, statisticslog: Int, individual: GPIndividual): Unit =
    for (tree <- individual.trees.indices) {
      printHeader(state, statisticslog, tree)
      printTree(state, statisticslog, individual, tree)
      printFooter(state, statisticslog, tree)
    }

  protected def printHeader(state: EvolutionState, statisticslog: Int, tree: Int): Unit =
    if (haveHeaderForTree(tree))
      state.output.print(s"${headers(tree)} ", statisticslog)
    else
      state.output.println("Occam Tree " + tree + ":", statisticslog)

  protected def haveHeaderForTree(tree: Int) = tree < headers.size

  protected def printTree(state: EvolutionState, statisticslog: Int, individual: GPIndividual, tree: Int): Unit = {
    val theRootNode = rootNode(individual, tree)
    val treeAsStringForHumans = getNodesAsStringForHumans(theRootNode)
    state.output.print(treeAsStringForHumans, statisticslog)
  }

  protected def rootNode(individual: GPIndividual, tree: Int): GPNode =
    individual.trees(tree).child

  protected def getNodesAsStringForHumans(node: GPNode): String =
    expectedChildren(node) match {
      case 0 => node.toStringForHumans
      case 1 => unaryOperatorToForHumans(node)
      case 2 => binaryOperatorToOccamStringForHumans(node)
      case _ => functionToOccamStringForHumans(node)
    }

  protected def expectedChildren(node: GPNode): Int = node.children.size

  protected def unaryOperatorToForHumans(node: GPNode): String =
    node.toStringForHumans + "(" + getNodesAsStringForHumans(node.children(0)) + ")"

  protected def binaryOperatorToOccamStringForHumans(node: GPNode): String =
    "(" + getNodesAsStringForHumans(node.children(0)) + " " +
      node.toStringForHumans + " " +
      getNodesAsStringForHumans(node.children(1)) + ")"

  protected def functionToOccamStringForHumans(node: GPNode): String =
    node.children.map(getNodesAsStringForHumans(_)).mkString(s"${node.toStringForHumans} (", ", ", ")")

  protected def printFooter(state: EvolutionState, statisticslog: Int, tree: Int): Unit =
    if (haveFooterForTree(tree))
      state.output.println(footers(tree), statisticslog)
    else
      state.output.println("", statisticslog)

  protected def haveFooterForTree(tree: Int) = tree < footers.size

}

object OccamStatistics {
  private val P_NUMBER_OF_TREES = "num-trees"
  private val P_HEADER = "header"
  private val P_FOOTER = "footer"
}

//private def printTree(state: EvolutionState, statisticslog: Int, individual: GPIndividual, tree: Int) = {
//  individual.trees(tree).child match {
//  case node: Node =>
//  val occam = node.toStringForHumans
//  state.output.print(occam, statisticslog)
//
//  case _ =>
//  state.output.println(s"Error printing tree, root node is of type ${individual.trees(tree).child.getClass}" +
//  s" but should be of Node",
//  statisticslog)
//  }
//  }
//
//trait OccamStatisticsNode extends Logging {
//
//  def expectedChildren: Int
//  def evalConstant: TypedData
//  def toStringForHumans: String
//  def childAt(at: Int): OccamStatistics.OccamStatisticsNode
//  def isRedundant = false
//  def getNextRelevantChild: OccamStatistics.OccamStatisticsNode = this
//
//  def isConstant: Boolean = {
//    var isConstant = true
//    for (c <- 0 until expectedChildren) {
//      val child = childAt(c).asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      isConstant = isConstant && child.isConstant
//    }
//    logger trace ("node={}, isConstant={}", this, isConstant)
//    isConstant
//  }
//
//  def isZero: Boolean = isConstant && evalConstant.getDouble == 0.0
//
//  def isOne: Boolean = isConstant && evalConstant.getDouble == 1.0
//
//  def isPositive: Boolean = isConstant && evalConstant.getDouble >= 0
//
//  def toOccamStringForHumans: String =
//    if (isRedundant) getNextRelevantChild.toOccamStringForHumans
//    else if (isConstant) {
//      val result = evalConstant.toStringForHumans
//      result
//    } else {
//      var occamStringForHumans = new String
//      if (expectedChildren == 0) occamStringForHumans += toStringForHumans
//      else if (expectedChildren == 1) occamStringForHumans += toOccamStringForHumansUnaryOperator
//      else if (expectedChildren == 2) occamStringForHumans += toOccamStringForHumansBinaryOperator
//      else occamStringForHumans += toOccamStringForHumansFunction
//      logger trace("occamStringForHumans={}", occamStringForHumans)
//      occamStringForHumans
//    }
//
//  def toOccamStringForHumansUnaryOperator: String =
//    toStringForHumans + "(" + childAt(0).asInstanceOf[OccamStatistics.OccamStatisticsNode].toOccamStringForHumans + ")"
//
//  def toOccamStringForHumansBinaryOperator: String =
//    "(" + childAt(0)
//      .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      .toOccamStringForHumans + " " + toStringForHumans + " " + childAt(1)
//      .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      .toOccamStringForHumans + ")"
//
//  def toOccamStringForHumansFunction: String = {
//    var occamString = toStringForHumans + "(" + childAt(0)
//      .asInstanceOf[OccamStatistics.OccamStatisticsNode]
//      .toOccamStringForHumans
//    for (x <- 1 until expectedChildren) {
//      occamString += ", " + childAt(x).asInstanceOf[OccamStatistics.OccamStatisticsNode].toOccamStringForHumans
//    }
//    occamString += ")"
//  }
//}
