package ie.nix.ecj

import ec.gp.GPData
import ie.nix.ecj.TypedData.error
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

class TypedData extends GPData {

  protected var data: Any = _

  override def toString: String = s"Data[${data.getClass.getTypeName}]"

  def getDouble: Double = data match {
    case data: Double => data
    case _            => error(data); exit()
  }
  def set(data: Double): Unit = this.data = data

  def getBoolean: Boolean = data match {
    case data: Boolean => data
    case _             => error(data); exit()
  }
  def set(data: Boolean): Unit = this.data = data
}

object TypedData extends Logging {
  def apply() = new TypedData()

  def error(data: Any): Unit =
    logger error s"$this trying to get a Double from a ${data.getClass.getTypeName} type."
}
