package ie.nix.ecj
import ec.gp.GPIndividual
import ec.gp.koza.KozaFitness
import ec.util.Parameter
import ec.{EvolutionState, Individual}
import ie.nix.ecj.PeerSimProblem.{P_NUMBER_OF_RUNS, P_PROPERTIES, P_PROTOCOL, P_PROTOCOL_NAME, configurationInitialised}
import ie.nix.ecj.RedirectedLogging.{CONSOLE, FILE, P_LOGGING, SILENT}
import ie.nix.ecj.Timers.{DEFAULT_MAX_EVALUATION_TIME, P_MAX_EVALUATION_TIME}
import ie.nix.util.Utils.as
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.logging.log4j.scala.Logging
import peersim.cdsim.CDSimulator
import peersim.config.ParsedProperties
import peersim.edsim.EDSimulator

import java.io.{File, FileOutputStream, PrintStream}
import scala.collection.JavaConverters.collectionAsScalaIterableConverter

abstract class PeerSimProblem
    extends ec.gp.GPProblem
    with RedirectedLogging
    with StatRecording
    with Timers
    with Logging {

  protected var propertiesFile: String = null
  protected var protocol: String = null
  protected var protocolName: String = null
  protected var numberOfRuns = 0

  var state: EvolutionState = null
  protected var threadnum = 0
  protected var gpIndividual: GPIndividual = null
  protected var generation: Int = 0
  protected var run = 0

  override def setup(state: EvolutionState, base: Parameter): Unit = {
    super.setup(state, base) // very important, remember this
    redirectedLoggingSetup(state, base)
    setupTimers(state, base)

    this.state = state

    propertiesFile =
      state.parameters.getStringWithDefault(base.push(P_PROPERTIES), defaultBase.push(P_PROPERTIES), null)
    logger debug s"propertiesFile=$propertiesFile"
    assert(propertiesFile != null, "Specify the properties file in the params file")

    protocol = state.parameters.getStringWithDefault(base.push(P_PROTOCOL), defaultBase.push(P_PROTOCOL), null)
    logger debug s"protocol=$protocol"
    assert(protocol != null, "Specify the protocol file in the params file")

    protocolName =
      state.parameters.getStringWithDefault(base.push(P_PROTOCOL_NAME), defaultBase.push(P_PROTOCOL_NAME), null)
    logger debug s"protocolName=$protocolName"
    assert(protocolName != null, s"Specify the $P_PROTOCOL_NAME file in the params file")

    numberOfRuns =
      state.parameters.getIntWithDefault(base.push(P_NUMBER_OF_RUNS), defaultBase.push(P_NUMBER_OF_RUNS), 0)
    logger debug s"numberOfRuns=$numberOfRuns"
    assert(numberOfRuns >= 1, s"numberOfRuns is $numberOfRuns, it needs to be >= 1.")

  }

  override def evaluate(state: EvolutionState, individual: Individual, threadnum: Int, log: Int): Unit = {

    if (!individual.evaluated) {

      this.threadnum = threadnum
      this.state = state
      this.generation = state.generation
      this.gpIndividual = as[GPIndividual](individual)
      PeerSimProblem.gpProblem(this)

      setupLogging()
      clearStats(individual)
      startEvaluationTimer()

      var totalEvaluation = 0d
      var hits = 0
      for { run <- 0 until numberOfRuns } {
        this.run = run
        if (shouldContinueEvaluating()) {
          startRunTimer()
          initSimulation()
          runSimulation()
          val evaluation = evaluateSimulation
          if (evaluation == 0) hits += 1
          totalEvaluation += evaluation
          endRunTimer()
        } else {
          totalEvaluation = Double.MaxValue
          logger trace s"generation=${state.generation}, gpIndividual=${gpIndividual.hashCode}, " +
            s"run=$run, Cancelling evaluation at $evaluationTime"
        }
        logger trace s"generation=${state.generation}, gpIndividual=${gpIndividual.hashCode}, run=$run, " +
          s"totalEvaluation=$totalEvaluation"
      }

      gpIndividual.fitness match {
        case kozaFitness: KozaFitness =>
          kozaFitness.setStandardizedFitness(state, totalEvaluation / numberOfRuns)
          kozaFitness.hits = hits
          gpIndividual.evaluated = true
          logger trace
            s"generation=${state.generation}, gpIndividual=${gpIndividual.hashCode}, " +
              s"hits=$hits, totalEvaluation=$totalEvaluation, adjustedFitness=${kozaFitness.adjustedFitness}"
      }
      endEvaluationTimer()
      resetLogging()
    }
  }

  protected def initSimulation(): Unit =
    if (!configurationInitialised) {
      peersim.config.Configuration.setConfig(new ParsedProperties(properties))
      configurationInitialised = true
    }

  protected def runSimulation(): Unit =
    if (CDSimulator.isConfigurationCycleDriven)
      CDSimulator.nextExperiment()
    else if (EDSimulator.isConfigurationEventDriven)
      EDSimulator.nextExperiment()

  protected def evaluateSimulation: Double

  def evaluateTreeForDouble(treeNumber: Int): Double = {
    gpIndividual.trees(treeNumber).child.eval(state, threadnum, input, stack, gpIndividual, this)
    val result = input.asInstanceOf[TypedData].getDouble
    result
  }

  def evaluateTreeForBoolean(treeNumber: Int): Boolean = {
    gpIndividual.trees(treeNumber).child.eval(state, threadnum, input, stack, gpIndividual, this)
    val result = input.asInstanceOf[TypedData].getBoolean
    result
  }

  protected def properties: Array[String] =
    Array[String](propertiesFile, s"random.seed=$run", s"protocol.$protocolName=$protocol")

}
object PeerSimProblem extends Logging {

  val P_NUMBER_OF_RUNS = "number_of_runs"

  val P_PROPERTIES = "properties"
  val P_PROTOCOL = "protocol"
  val P_PROTOCOL_NAME = "protocol_name"

  protected var configurationInitialised = false

  private var _gpProblem: PeerSimProblem = _

  def gpProblem: PeerSimProblem = _gpProblem
  def gpProblem(gpProblem: PeerSimProblem): Unit =
    PeerSimProblem._gpProblem = gpProblem

  def bestFitness: Double = {
    val state = PeerSimProblem.gpProblem.state
    val fitnesses = for {
      subPop <- state.population.subpops.asScala
      individual <- subPop.individuals.asScala
    } yield individual.fitness
    val kozaFitness = fitnesses.collect { case kozaFitness: KozaFitness => kozaFitness }
    logger trace s"kozaFitness=${kozaFitness.map(_.adjustedFitness()).mkString(",")}"
    kozaFitness.map(_.adjustedFitness()).max
  }

}

trait RedirectedLogging extends Logging {
  this: PeerSimProblem =>

  var logging: String = CONSOLE
  val out: PrintStream = System.out
  val err: PrintStream = System.err

  def redirectedLoggingSetup(state: EvolutionState, base: Parameter): Unit = {
    logging = state.parameters.getStringWithDefault(base.push(P_LOGGING), this.defaultBase.push(P_LOGGING), CONSOLE)
    logger trace s"logging=$logging"
    assert(logging == SILENT || logging == FILE || logging == CONSOLE,
           s"logging is $logging, it needs to be $SILENT or $FILE or $CONSOLE.")
  }

  def setupLogging(): Unit =
    if (logging == FILE)
      redirectSystemOutErr("logs/out.txt", "logs/err.txt")
    else if (logging == SILENT)
      redirectSystemOutErr("/dev/null", "/dev/null")

  def resetLogging(): Unit = {
    System.setOut(out)
    System.setErr(err)
    logger trace s"resetLogging out=$out, err=$err"
  }

  private def redirectSystemOutErr(outFilename: String, errFilename: String): Unit = {
    logger trace s"redirectSystemOutErr outFilename=$outFilename, errFilename=$errFilename"
    System.setOut(new PrintStream(new FileOutputStream(new File(outFilename))))
    System.setErr(new PrintStream(new FileOutputStream(new File(errFilename))))
  }

}
object RedirectedLogging {
  val P_LOGGING = "logging"
  val SILENT = "silent"
  val FILE = "file"
  val CONSOLE = "console"
}

trait StatRecording {
  this: PeerSimProblem =>

  def clearStats(individual: Individual): Unit = individual.fitness match {
    case kozaFitnessWithStats: KozaFitnessWithStats => kozaFitnessWithStats.clearStats()
    case notKozaFitnessWithStats =>
      logger error s"Individual's fitness was not a KozaFitnessWithStats, but a " +
        s"${notKozaFitnessWithStats.getClass.getSimpleName}" // Do nothing
  }

  def recordStat(individual: Individual, statName: String, stat: Double): Unit = individual.fitness match {
    case kozaFitnessWithStats: KozaFitnessWithStats => kozaFitnessWithStats.recordStat(statName, stat)
    case notKozaFitnessWithStats =>
      logger error s"Individual's fitness was not a KozaFitnessWithStats, but a " +
        s"${notKozaFitnessWithStats.getClass.getSimpleName}" // Do nothing
  }
}

trait Timers {
  this: PeerSimProblem with StatRecording =>

  var maxEvaluationTime: Long = DEFAULT_MAX_EVALUATION_TIME

  private var evaluationTimerStartTime: Long = _
  private var runTimerStartTime: Long = _
  private var runTimerStatistics: DescriptiveStatistics = _

  def setupTimers(state: EvolutionState, base: Parameter): Unit = {
    maxEvaluationTime = state.parameters.getLongWithDefault(base.push(P_MAX_EVALUATION_TIME),
                                                            this.defaultBase.push(P_MAX_EVALUATION_TIME),
                                                            DEFAULT_MAX_EVALUATION_TIME)
    logger debug s"maxEvaluationTime=$maxEvaluationTime"
    assert(maxEvaluationTime > 0L, s"maxEvaluationTime is $maxEvaluationTime, it needs to be greater then zero.")
  }

  def startEvaluationTimer(): Unit = {
    evaluationTimerStartTime = System.currentTimeMillis
    runTimerStatistics = new DescriptiveStatistics()
  }

  def startRunTimer(): Unit = {
    runTimerStartTime = System.currentTimeMillis
  }

  def evaluationTime: Double = runTimerStatistics.getSum

  def shouldContinueEvaluating(): Boolean =
    evaluationTime < maxEvaluationTime

  def endRunTimer(): Unit = {
    val runTimerElapsedTimeSecs = (System.currentTimeMillis - runTimerStartTime) / 1000d
    runTimerStatistics.addValue(runTimerElapsedTimeSecs)
    logger trace s"Run ${runTimerStatistics.getN} was ${runTimerElapsedTimeSecs}s, " +
      s"mean ${"%.3f".format(meanRunTime)} +/-(${"%.2f".format(sdRunTime)})s"
  }

  def endEvaluationTimer(): Unit = {
    val evaluationTimerElapsedTimeSecs = (System.currentTimeMillis - evaluationTimerStartTime) / 1000d

    recordStat(gpIndividual, "Evaluation Time", evaluationTimerElapsedTimeSecs)
    recordStat(gpIndividual, "Run Count", runCount)
    recordStat(gpIndividual, "Mean Run Time", meanRunTime)
    recordStat(gpIndividual, "SD Run Time", sdRunTime)

    logger trace s"Generation=$generation Individual=${gpIndividual.hashCode()} Fitness=${gpIndividual.fitness.fitness()}, " +
      s"ran $runCount evaluations in ${evaluationTimerElapsedTimeSecs}s, " +
      s"mean ${"%.3f".format(meanRunTime)} +/-(${"%.2f".format(sdRunTime)})s"
  }

  def runCount: Double = runTimerStatistics.getN.toDouble
  def meanRunTime: Double = runTimerStatistics.getMean
  def sdRunTime: Double = runTimerStatistics.getStandardDeviation

}
object Timers {
  val P_MAX_EVALUATION_TIME = "max_evaluation_time"
  val DEFAULT_MAX_EVALUATION_TIME = 60L
}
