/*****************************************************************
 SAJaS - Simple API for JADE-based Simulations is a framework to 
 facilitate running multi-agent simulations using the JADE framework.
 Copyright (C) 2015 Henrique Lopes Cardoso
 Universidade do Porto

 GNU Lesser General Public License

 This file is part of SAJaS.

 SAJaS is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 SAJaS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with SAJaS.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************/

package sajas.core;

/**
 * Note: this class has been re-implemented due to non-public method declarations that need to be accessed from TimerDispatcher.
 * 
 * @see jade.core.Timer
 * @author hlc
 *
 */
public class Timer {

	private long expireTimeMillis;
	private boolean fired;
	private TimerListener owner;

	public Timer(long when, TimerListener tl) {
		expireTimeMillis = when;
		owner = tl;
		fired = false;
	}

	public boolean equals(Object o) {
		Timer t = (Timer)o;
		return (expireTimeMillis == t.expireTimeMillis);
	}


	// Called by the TimerDispatcher

	boolean isExpired() {
		return expireTimeMillis < System.currentTimeMillis();
	}

	void fire() {
		if (!fired) {
			fired = true;
			owner.doTimeOut(this);
		}
	}

	final long expirationTime() {
		return expireTimeMillis;
	}

	void setExpirationTime(long t) {
		expireTimeMillis = t;
	}

}
