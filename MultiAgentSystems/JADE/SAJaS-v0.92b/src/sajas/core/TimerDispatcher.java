/*****************************************************************
 SAJaS - Simple API for JADE-based Simulations is a framework to 
 facilitate running multi-agent simulations using the JADE framework.
 Copyright (C) 2015 Henrique Lopes Cardoso
 Universidade do Porto

 GNU Lesser General Public License

 This file is part of SAJaS.

 SAJaS is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 SAJaS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with SAJaS.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************/

package sajas.core;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * This class handles timer dispatching, replacing JADE's version which is based on a separate thread.
 * It should not be used by application developers.
 * 
 * @see jade.core.TimerDispatcher
 * @author hlc
 *
 */
public class TimerDispatcher {
	// The singleton TimerDispatcher
	private static TimerDispatcher theDispatcher;

	private TimersList timers = new TimersList();
	
	public synchronized Timer add(Timer t) {
		while (!addTimer(t)) {
			t.setExpirationTime(t.expirationTime()+1);
		}
		return t;
	}
	
	public synchronized void remove(Timer t) {
		timers.removeElement(t);
	}

	/**
	 * @see jade.core.TimerDispatcher#run()
	 */
	public void step() {
		Timer t = null;
		if (!timers.isEmpty()) {
			t = (Timer) timers.firstElement();
			if (t.isExpired()) {
				timers.removeElement(t);
				t.fire();
			}
		}
	}

	public static TimerDispatcher getTimerDispatcher() {
		if (theDispatcher == null) {
			theDispatcher = new TimerDispatcher();
		}
		return theDispatcher;
	}

	private boolean addTimer(Timer t) {
		return timers.add(t);
	}

	
	private class TimersList {
		private TreeSet set = new TreeSet(new TimerComparator());

		private final Object firstElement() {
			return set.first();
		}
		
		private final void removeElement(Object obj) {
			set.remove(obj);
		}
		
		private final void removeAllElements() {
			set.clear();
		}
		
		private final boolean isEmpty() {
			return set.isEmpty();
		}
		
		private boolean add(Object obj) {
			return set.add(obj);
		}
	}
	
	private class TimerComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			Timer t1 = (Timer) o1;
			Timer t2 = (Timer) o2;
			if (t1.expirationTime() < t2.expirationTime()) {
				return -1;
			}
			else if (t1.expirationTime() == t2.expirationTime()) {
				return 0;
			}
			else {
				return 1;
			}
		}
	}

}
