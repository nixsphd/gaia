/*****************************************************************
 SAJaS - Simple API for JADE-based Simulations is a framework to 
 facilitate running multi-agent simulations using the JADE framework.
 Copyright (C) 2015 Henrique Lopes Cardoso
 Universidade do Porto

 GNU Lesser General Public License

 This file is part of SAJaS.

 SAJaS is free software: you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 SAJaS is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public License
 along with SAJaS.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************/

package sajas.core;

import jade.content.ContentManager;
import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import sajas.core.behaviours.Behaviour;
import sajas.domain.AMSService;
import sajas.domain.DFService;
import sajas.wrapper.ContainerController;

/**
 * This is the base class for implementing agents.
 * 
 * Note: this class does not extend <code>jade.core.Agent</code> due to non-public method declarations that needed to be re-implemented.
 * 
 * @see jade.core.Agent
 * @author hlc
 *
 */
public class Agent 
implements TimerListener {
	
	private String myName = null;  
	private AID myAID = null;
	private String myHap = null;

	private Scheduler myScheduler = new Scheduler();
	
	private transient AssociationTB pendingTimers;
	private transient TimerDispatcher theDispatcher;

	private MessageQueue mailBox = new MessageQueue();

	private transient Object[] arguments = null;

	public Agent() {
		pendingTimers = new AssociationTB();
		theDispatcher = TimerDispatcher.getTimerDispatcher();
	}
	
	/**
	 * @see jade.core.Agent#setArguments(Object[])
	 */
	public final void setArguments(Object args[]) {
		arguments = args;
	}

	/**
	 * @see jade.core.Agent#getArguments()
	 */
	public Object[] getArguments() {
		return arguments;
	}
	
	/**
	 * @see jade.core.Agent#addBehaviour(jade.core.behaviours.Behaviour)
	 */
	public void addBehaviour(Behaviour b) {
		b.setAgent(this);
		myScheduler.add(b);
	}

	/**
	 * @see jade.core.Agent#removeBehaviour(jade.core.behaviours.Behaviour)
	 * @see jade.core.Scheduler#remove(jade.core.behaviours.Behaviour)
	 */
	public void removeBehaviour(Behaviour b) {
		myScheduler.remove(b);
	}
	
	/**
	 * @see jade.core.Agent#setup()
	 */
	protected void setup() {
	}

	/**
	 * @see jade.core.Agent#takeDown()
	 */
	protected void takeDown() {
	}

	/**
	 * @see jade.core.Agent#doDelete()
	 */
	public void doDelete() {
		alive = false;
	}

	/**
	 * @see jade.core.Agent#setAID(jade.core.AID)
	 */
	public void setAID(AID aid) {
		myName = aid.getLocalName();
		myHap = aid.getHap();
		this.myAID = aid;
	}
	
	/**
	 * @see jade.core.Agent#getAID()
	 */
	public final AID getAID() {
		return myAID;
	}
	
	/**
	 * @see jade.core.Agent#getLocalName()
	 */
	public final String getLocalName() {
		return myName;
	}
	
	/**
	 * @see jade.core.Agent#getName()
	 */
	public final String getName() {
		if (myHap != null) {
			return myName + '@' + myHap;
		}
		else {
			return myName;
		}
	}
	
	/**
	 * @see jade.core.Agent#getAMS()
	 */
	public final AID getAMS() {
		return AMSService.amsAID;  
	}

	/**
	 * @see jade.core.Agent#getDefaultDF()
	 */
	public AID getDefaultDF() {
		return DFService.getDFAID();
	}

	/**
	 * @see jade.core.Agent#powerUp(jade.core.AID, Thread)
	 * @exclude
	 */
	public void powerUp() {
	}


	private boolean firstStep = true;
	private boolean alive = true;
	
	/**
	 * Note that this implementation does not take into account agent life-cycle management.
	 * @see jade.core.Agent#run()
	 * @see jade.core.Agent$ActiveLifeCycle#init()
	 * @see jade.core.Agent$ActiveLifeCycle#execute()
	 * @see jade.core.Agent$DeletedLifeCycle#end()
	 * @exclude
	 */
	public final void step() {
		if(firstStep) {
			setup();
			firstStep = false;
			return;
		}
		
		if(alive) {
			Behaviour currentBehaviour;
			try {
				currentBehaviour = myScheduler.schedule();
				// might get a null behaviour if no behaviour is ready
				if(currentBehaviour != null) {
					long oldRestartCounter = currentBehaviour.getRestartCounter();

					currentBehaviour.actionWrapper();
					if(currentBehaviour.done()) {
						currentBehaviour.onEnd();   // ignoring return value
						myScheduler.remove(currentBehaviour);
					} else {
						if(oldRestartCounter != currentBehaviour.getRestartCounter()) {
							currentBehaviour.handleRestartEvent();
						}

						if(!currentBehaviour.isRunnable()) {
							myScheduler.block(currentBehaviour);
						}
					}
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		} else {
			takeDown();
			myContainer.removeLocalAgent(this);
		}
	}
	
	/**
	 * @see jade.core.Agent#send(jade.lang.acl.ACLMessage)
	 */
	public final void send(ACLMessage msg) {
		if (msg != null) {
			msg.setSender(getAID());
			AMSService.send(msg, true, false);
		}
	}
	
	/**
	 * @see jade.core.Agent#receive()
	 */
	public final ACLMessage receive() {
		return receive(null);
	}

	/**
	 * @see jade.core.Agent#receive(jade.lang.acl.MessageTemplate)
	 */
	public final ACLMessage receive(MessageTemplate template) {
		return mailBox.receive(template);
	}

	/**
	 * @see jade.core.Agent#blockingReceive()
	 */
	public final ACLMessage blockingReceive() {
		return blockingReceive(null, 0);
	}

	/**
	 * @see jade.core.Agent#blockingReceive(long)
	 */
	public final ACLMessage blockingReceive(long millis) {
		return blockingReceive(null, millis);
	}

	/**
	 * @see jade.core.Agent#blockingReceive(jade.lang.acl.MessageTemplate)
	 */
	public final ACLMessage blockingReceive(MessageTemplate pattern) {
		return blockingReceive(pattern, 0);
	}

	/**
	 * @see jade.core.Agent#blockingReceive(jade.lang.acl.MessageTemplate, long)
	 */
	public final ACLMessage blockingReceive(MessageTemplate pattern, long millis) {
		System.err.println("WARNING: SAJaS does not currently support blocking approaches -- returning null");
		return null;
	}

	/**
	 * @see jade.core.Agent#postMessage(jade.lang.acl.ACLMessage)
	 */
	public final void postMessage(final ACLMessage msg) {
		mailBox.add(msg);
		myScheduler.restartAll();
	}


	// the container controller
	private /*transient*/ ContainerController myContainer = null;
	
	/**
	 * Used by sajas.wrapper.ContainetController to appropriately set the ContainerController of this agent.
	 * Programmers should have no need to use this method.
	 * @param containerController
	 * @exclude
	 */
	public void setContainerController(ContainerController containerController) {
		this.myContainer = containerController;
	}

	/**
	 * @see jade.core.Agent#getContainerController()
	 */
	public ContainerController getContainerController() {
		return myContainer;
	}
	
	
	// the content manager
	private ContentManager theContentManager = null;

	/**
	 * @see jade.core.Agent#getContentManager()
	 */
	public ContentManager getContentManager() {
		if (theContentManager == null) {
			theContentManager = new ContentManager();
		}
		return theContentManager;
	}

	/**
	 * @see jade.core.Agent#restartLater(jade.core.behaviours.Behaviour, long)
	 */
	public void restartLater(Behaviour b, long millis) {
		if (millis <= 0) 
			return;
		Timer t = new Timer(System.currentTimeMillis() + millis, this);
		pendingTimers.addPair(b, t);
	}
	
	/**
	 * @see jade.core.Agent#notifyRestarted(jade.core.behaviours.Behaviour)
	 */
	public void notifyRestarted(Behaviour b) {
		Behaviour root = b.root();
		if(root.isRunnable()) {
			myScheduler.restart(root);
		}
	}
	
	/**
	 * @see jade.core.Agent#notifyChangeBehaviourState(jade.core.behaviours.Behaviour, String, String)
	 */
	public void notifyChangeBehaviourState(Behaviour b, String from, String to) {
		b.setExecutionState(to);
	}

	/**
	 * @see jade.core.Agent#removeTimer(jade.core.behaviours.Behaviour)
	 */
	public void removeTimer(Behaviour b) {
		Timer t = pendingTimers.getPeer(b);
		if(t != null) {
			pendingTimers.removeMapping(b);
		}
	}
	
	
	/**
	 * This class stores all mail addressed to this agent.
	 * @author joaolopes, hlc
	 *
	 */
	private class MessageQueue {
		
		private List<ACLMessage> messages = new ArrayList<ACLMessage>();
		
		public MessageQueue() {
		}

		/**
		 * Adds a new message to the mail box. 
		 * @param message
		 */
		public void add(ACLMessage message) {
			messages.add(message);
		}

		/**
		 * Uses a template to retrieve the first matching message from the queue.
		 * This message will be then removed from the box.
		 * @param template
		 * @see jade.core.InternalMessageQueue#receive(jade.lang.acl.MessageTemplate)
		 * @return
		 */
		public ACLMessage receive(MessageTemplate pattern) {
			ACLMessage msg;
			for(Iterator<ACLMessage> it = messages.iterator(); it.hasNext();) {
				msg = (ACLMessage) it.next();
				if(pattern == null || pattern.match(msg)) {
					it.remove();
					return msg;
				}
			}
			
			return null;
		}
		
	}

	@Override
	public void doTimeOut(Timer t) {
		Behaviour b = null;
		synchronized (theDispatcher) {
			b = pendingTimers.getPeer(t);
			if(b != null) {
				pendingTimers.removeMapping(b);
			}
		}
		if(b != null) {
			b.restart();
		} else {
			System.out.println("Warning: No mapping found for expired timer "+t.expirationTime());
		}
	}

	
	/**
	 Inner class AssociationTB.
	 This class manages bidirectional associations between Timer and
	 Behaviour objects, using hash tables. This class is 
	 synchronized with the operations
	 carried out by the TimerDispatcher. It allows also to avoid a deadlock when:
	 1) A behaviour blocks for a very short time --> A Timer is added
	 to the TimerDispatcher
	 2) The Timer immediately expires and the TimerDispatcher try to 
	 restart the behaviour before the pair (b, t) is added to the 
	 pendingTimers of this agent.
	 */
	private class AssociationTB {
		private Hashtable BtoT = new Hashtable();
		private Hashtable TtoB = new Hashtable();

		public void clear() {
			synchronized (theDispatcher) {
				Enumeration e = timers();
				while (e.hasMoreElements()) {
					Timer t = (Timer) e.nextElement();
					theDispatcher.remove(t);
				}

				BtoT.clear();
				TtoB.clear();

			} //end synch
		}

		public void addPair(Behaviour b, Timer t) {
			TBPair pair = new TBPair(Agent.this, t, b);
			addPair(pair);
		}

		public void addPair(TBPair pair) {
			synchronized (theDispatcher) {
				if(pair.getOwner() == null) {
					pair.setOwner(Agent.this);
				}

				pair.setTimer(theDispatcher.add(pair.getTimer()));
				TBPair old = (TBPair)BtoT.put(pair.getBehaviour(), pair);
				if(old != null) {
					theDispatcher.remove(old.getTimer());
					TtoB.remove(old.getTimer());
				}
				// Note that timers added to the TimerDispatcher are unique --> there
				// can't be an old value to handle
				TtoB.put(pair.getTimer(), pair);

			} //end synch
		}

		public void removeMapping(Behaviour b) {
			synchronized (theDispatcher) {
				TBPair pair = (TBPair)BtoT.remove(b);
				if(pair != null) {
					TtoB.remove(pair.getTimer());

					theDispatcher.remove(pair.getTimer());
				}
			} //end synch
		}


		public Timer getPeer(Behaviour b) {
			// this is not synchronized because BtoT is an Hashtable (that is already synch!)
			TBPair pair = (TBPair)BtoT.get(b);
			if(pair != null) {
				return pair.getTimer();
			}
			else {
				return null;
			}
		}

		public Behaviour getPeer(Timer t) {
			// this is not synchronized because BtoT is an Hashtable (that is already synch!)
			TBPair pair = (TBPair)TtoB.get(t);
			if(pair != null) {
				return pair.getBehaviour();
			}
			else {
				return null;
			}
		}

		private Enumeration timers() {
			return TtoB.keys();
		}


	} // End of inner class AssociationTB 

	/** Inner class TBPair
	 *
	 */
	private static class TBPair {

		public TBPair() {
			expirationTime = -1;
		}

		public TBPair(Agent a, Timer t, Behaviour b) {
			owner = a;
			myTimer = t;
			expirationTime = t.expirationTime();
			myBehaviour = b;
		}

		public void setTimer(Timer t) {
			myTimer = t;
		}

		public Timer getTimer() {
			return myTimer;
		}

		public Behaviour getBehaviour() {
			return myBehaviour;
		}

		public void setBehaviour(Behaviour b) {
			myBehaviour = b;
		}


		public Agent getOwner() {
			return owner;
		}

		public void setOwner(Agent o) {
			owner = o;
			createTimerIfNeeded();
		}

		public long getExpirationTime() {
			return expirationTime;
		}

		public void setExpirationTime(long when) {
			expirationTime = when;
			createTimerIfNeeded();
		}

		// If both the owner and the expiration time have been set,
		// but the Timer object is still null, create one
		private void createTimerIfNeeded() {
			if(myTimer == null) {
				if((owner != null) && (expirationTime > 0)) {
					myTimer = new Timer(expirationTime, owner);
				}
			}
		}  

		private Timer myTimer;
		private long expirationTime;
		private Behaviour myBehaviour;
		private Agent owner;

	} // End of inner class TBPair 
	
}
