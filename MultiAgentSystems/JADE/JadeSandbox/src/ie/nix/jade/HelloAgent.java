package ie.nix.jade;

import ie.nix.util.Randomness;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.core.behaviours.WakerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class HelloAgent extends Agent {

	private static final String HI_BACK = "Hi back";

	private static final String HI = "Hi";

	// The list of known seller agents
	private AID[] otherAgents;
	
	private String name;

	protected void setup() {
		
		System.out.println("NDB::" + getAID().getLocalName() + " is ready.");
		Randomness.setSeed(17);
		
		handleArgs();
		
		registerWithDF("hello-agent");
				
		storeAgentsNames(getAgentsfromDF("hello-agent"));
		
		listen();
		
		randomlySayHi(5000);
		
		planToDie(10000);

	}

	protected void handleArgs() {
			// Get the title of the book to buy as a start-up argument
			Object[] args = getArguments();
			if (args != null && args.length > 0) {
				name = (String) args[0];
				System.out.println("NDB::" + getAID().getLocalName() + " called " + name);
				
			} else {
				// Make the agent terminate immediately
				System.out.println("NDB::" + getAID().getLocalName() + " no name for me :(");
				doDelete();
			}
		}

	protected void registerWithDF(String withType) {
		// Register as a hello-agent service in the yellow pages
		DFAgentDescription  myDirectoryFacilitator = new DFAgentDescription();
		myDirectoryFacilitator.setName(getAID());
		ServiceDescription myServiceDescription = new ServiceDescription();
		myServiceDescription.setType(withType);
		myServiceDescription.setName("SmallTalker");
		myDirectoryFacilitator.addServices(myServiceDescription);
		try {
			DFService.register(this, myDirectoryFacilitator);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
	}

	protected DFAgentDescription[] getAgentsfromDF(String withType) {
		try {
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription otherServiceDescription = new ServiceDescription();
			otherServiceDescription.setType(withType);
			template.addServices(otherServiceDescription);
			
			DFAgentDescription[] result = DFService.search(this, template);
	
			System.out.println("NDB::" + getAID().getLocalName() + " found "+result.length+" HelloAgent :D");
			return result;
		} catch (FIPAException fe) {
			fe.printStackTrace();
			return new DFAgentDescription[0];
		}
	}

	protected void storeAgentsNames(DFAgentDescription[] result) {
		otherAgents = new AID[result.length];
		for (int i = 0; i < result.length; ++i) {
			if (!result[i].getName().equals(getName())) {
				 otherAgents[i] = result[i].getName();
				 System.out.println("HelloAgent " + getAID().getLocalName() + " aware of HelloAgent " + otherAgents[i].getLocalName());
			} else {
				 System.out.println("HelloAgent " + getAID().getLocalName() + " ignoring of HelloAgent " + result[i].getName());
					
			}
		}
	}

	@SuppressWarnings("serial")
	protected void randomlySayHi(int maxWait) {
		addBehaviour(new TickerBehaviour(this, Randomness.nextInt(maxWait)) {
			protected void onTick() {
				// Message carrying a request for offer
				ACLMessage sayHiMessage = new ACLMessage(ACLMessage.INFORM);
				sayHiMessage.addReceiver(otherAgents[Randomness.nextInt(otherAgents.length-1)]);
				sayHiMessage.setContent(HI);
				myAgent.send(sayHiMessage);
				System.out.println("NDB::" + getAID().getLocalName() + " said Hi");
	
			}
		});
	}

	protected void listen() {
		addBehaviour(new CyclicBehaviour() {
			public void action() {
				MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
				ACLMessage msg = myAgent.receive(mt);
				if (msg != null) {
					System.out.println("NDB::" + getAID().getLocalName() + " got a msg :D");
					String message = msg.getContent();
					if (message.equals(HI)) {
						System.out.println("NDB::" + getAID().getLocalName() + " said Hi");
						ACLMessage reply = msg.createReply();
						reply.setContent(HI_BACK);
						System.out.println("NDB::" + getAID().getLocalName() + " saying Hi back");
						myAgent.send(reply);
						
					} else if (message.equals(HI_BACK)) {
						System.out.println("NDB::" + getAID().getLocalName() + " said "+HI_BACK);
						
					} else {
						System.out.println("NDB::" + getAID().getLocalName() + " don't know about this one: "+message);
						
					}
					
				} else {
					block();
				}
			}
		});
	}

	protected void planToDie(int when) {
		addBehaviour(new WakerBehaviour(this, when) {
			protected void handleElapsedTimeout() {
				System.out.println("NDB::" + getAID().getLocalName() + " time to die :D");
				doDelete();
			}
		});
	}

	protected void takeDown() {
		// Deregister from the yellow pages
		try {
			DFService.deregister(this);
		} catch (FIPAException fe) {
			fe.printStackTrace();
		}
		// Printout a dismissal message
		System.out.println("NDB::"+getAID().getLocalName()+" terminating.");
	}
}
