package ie.nix.jade;

public class Main {

	public static void main(String[] args) {
//		The following command line launches a Main Container activating the JADE management 
//		GUI (-gui) option. <classpath> must include all jade classes plus all required 
//		application-specific classes.
//		java -cp <classpath> jade.Boot -gui
		
//		Besides the ability of accepting registrations from other containers, a main 
//		container differs from normal containers as it holds two special agents 
//		(automatically started when the main container is launched).
		
//		The AMS (Agent Management System) that provides the naming service (i.e. ensures 
//		that each agent in the platform has a unique name) and represents the authority 
//		in the platform (for instance it is possible to create/kill agents on remote 
//		containers by requesting that to the AMS). This tutorial does not illustrate how 
//		to interact with the AMS as this is part of the advanced JADE programming.
		
//		The DF (Directory Facilitator) that provides a Yellow Pages service by means of 
//		which an agent can find other agents providing the services he requires in order 
//		to achieve his goals. Using the Yellow Pages service provided by the DF agent 
//		is illustrated in chapter 6.
		
//		The following command line launches a peripheral container (-container option) that 
//		registers to a main container running on host avalon.tilab.com (-host option) and 
//		activates an agent called john of class myPackage.MyClass (-agents) option
//		java -cp <classpath> jade.Boot -container -host avalon.tilab.com -agents
//		john:myPackage.myClass
		
		args = new String[] {
				"-agents",
				"hello1:ie.nix.jade.HelloAgent(Nix)"+";"+
				"hello2:ie.nix.jade.HelloAgent(Wayne)"+";"+
				"hello3:ie.nix.jade.HelloAgent(Lia)"+";"+
				"hello4:ie.nix.jade.HelloAgent(Jen)"+";"+
				"hello5:ie.nix.jade.HelloAgent(Hugh)"+";"+
				"hello6:ie.nix.jade.HelloAgent(Gav)"+";"+
				"hello7:ie.nix.jade.HelloAgent(Mark)"
		};		
		jade.Boot.main(args);
	}

}
