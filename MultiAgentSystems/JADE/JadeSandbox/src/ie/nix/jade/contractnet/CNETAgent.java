package ie.nix.jade.contractnet;

import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import ie.nix.util.Randomness;
import jade.core.AID;
import jade.core.Agent;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.FailureException;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.proto.ContractNetInitiator;
import jade.proto.ContractNetResponder;

@SuppressWarnings("serial")
public class CNETAgent extends Agent {
	
	public class Manager extends ContractNetInitiator {

		int numberOfConttractors;
		
		public Manager(Agent a, ACLMessage cfp, int numberOfConttractors) {
			super(a, cfp);
			this.numberOfConttractors = numberOfConttractors;
			
		}

		protected void handlePropose(ACLMessage propose, Vector v) {
			System.out.println("NDB::" + propose.getSender().getLocalName() + " proposed " + propose.getContent());
		}

		protected void handleRefuse(ACLMessage refuse) {
			System.out.println("NDB::" + refuse.getSender().getLocalName() + " refused");
		}

		protected void handleFailure(ACLMessage failure) {
			if (failure.getSender().equals(myAgent.getAMS())) {
				// FAILURE notification from the JADE runtime: the receiver
				// does not exist
				System.out.println("Responder does not exist");
			} else {
				System.out.println("Agent " + failure.getSender().getName() + " failed");
			}
			// Immediate failure --> we will not receive a response from this agent
			numberOfConttractors--;
		}

		protected void handleAllResponses(Vector responses, Vector acceptances) {
			if (responses.size() < numberOfConttractors) {
				// Some responder didn't reply within the specified timeout
				System.out.println("Timeout expired: missing " + (numberOfConttractors - responses.size()) + " responses");
			}
			
			// Evaluate proposals.
			int bestProposal = -1;
			AID bestProposer = null;
			ACLMessage accept = null;
			Enumeration e = responses.elements();
			while (e.hasMoreElements()) {
				ACLMessage msg = (ACLMessage) e.nextElement();
				if (msg.getPerformative() == ACLMessage.PROPOSE) {
					ACLMessage reply = msg.createReply();
					reply.setPerformative(ACLMessage.REJECT_PROPOSAL);
					acceptances.addElement(reply);
					int proposal = Integer.parseInt(msg.getContent());
					if (proposal > bestProposal) {
						bestProposal = proposal;
						bestProposer = msg.getSender();
						accept = reply;
					}
				}
			}
			
			// Accept the proposal of the best proposer
			if (accept != null) {
				System.out.println(
						"Accepting proposal " + bestProposal + " from responder " + bestProposer.getName());
				accept.setPerformative(ACLMessage.ACCEPT_PROPOSAL);
			}
		}

		protected void handleInform(ACLMessage inform) {
//			System.out.println(
//					"Agent " + inform.getSender().getName() + " successfully performed the requested action");
		}
	}

	public class Contractor extends ContractNetResponder {
		
		public Contractor(Agent a, MessageTemplate mt) {
			super(a, mt);
		}

		@Override
		protected ACLMessage handleCfp(ACLMessage cfp) throws NotUnderstoodException, RefuseException {
			System.out.println("NDB::" + getLocalName() + ": CFP received from " + cfp.getSender().getLocalName()+
					". Action is " + cfp.getContent());
			int proposal = evaluateAction();
			if (proposal > 30) {
				// We provide a proposal
				System.out.println("NDB::" + getLocalName() + ": Proposing " + proposal);
				ACLMessage propose = cfp.createReply();
				propose.setPerformative(ACLMessage.PROPOSE);
				propose.setContent(String.valueOf(proposal));
				return propose;
			} else {
				// We refuse to provide a proposal
				System.out.println("NDB::" + getLocalName() + ": Refuse");
				throw new RefuseException("evaluation-failed");
			}
		}

		@Override
		protected ACLMessage handleAcceptProposal(ACLMessage cfp, ACLMessage propose, ACLMessage accept) throws FailureException {
			System.out.println("NDB::" + getLocalName() + ": Proposal accepted");
			if (performAction()) {
				System.out.println("NDB::" + getLocalName() + ": Action successfully performed");
				ACLMessage inform = accept.createReply();
				inform.setPerformative(ACLMessage.INFORM);
				return inform;
			} else {
				System.out.println("NDB::" + getLocalName() + ": Action execution failed");
				throw new FailureException("unexpected-error");
			}
		}

		protected void handleRejectProposal(ACLMessage cfp, ACLMessage propose, ACLMessage reject) {
//			System.out.println("Agent " + getLocalName() + ": Proposal rejected");
		}
		
		private int evaluateAction() {
			// Simulate an evaluation by generating a random number
			return (int) (Randomness.nextInt(100));
		}

		private boolean performAction() {
			// Simulate action execution by generating a random number
			return (Math.random() > 0.2);
		}
		
	};
	
	protected void setup() {
		
		Randomness.setSeed(17);

		AID[] contractors = parseArgs(getArguments());
		if (contractors != null) {
			
			// the task requested is a dummy
			String task = "dummy-task";
			
			// We want to receive a reply in 10 secs
			int waitTime = 10000;
			
			initManager(contractors, task, waitTime);
		}

		initContractor();
		
	}

	public AID[] parseArgs(Object[] args) {
		System.out.println("NDB::"+getAID().getLocalName() + " is ready.");
		AID[] contractors = null;
		// Read names of responders as arguments
		if (args != null && args.length > 0) {
			contractors = new AID[args.length];
			System.out.println("NDB::"+getAID().getLocalName() + " has " + contractors.length + " contractors.");
			
			for (int i = 0; i < contractors.length; ++i) {
				contractors[i] = new AID((String) args[i], AID.ISLOCALNAME);
			}
			
		} else {
//			System.out.println("NDB::"+getAID().getLocalName() + " no contractors specified.");
		}
		
		return contractors;
	}

	public void initManager(AID[] contractors, String task, int wait) {
		// Fill the CFP message
		ACLMessage msg = new ACLMessage(ACLMessage.CFP);
		for (int c = 0; c < contractors.length; ++c) {
			msg.addReceiver(contractors[c]);
		}
		msg.setProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET);
		msg.setReplyByDate(new Date(System.currentTimeMillis() + wait));
		msg.setContent(task);
		
		addBehaviour(new Manager(this, msg, contractors.length));
	}
	
	public void initContractor() {
		System.out.println("NDB::" + getLocalName() + " waiting for CFP...");
		MessageTemplate template = MessageTemplate.and(
				MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_CONTRACT_NET),
				MessageTemplate.MatchPerformative(ACLMessage.CFP));
		
		addBehaviour(new Contractor(this, template));
		
	}

	public static void main(String[] args) {		
		args = new String[] {
				"-agents",
				"cnet1:ie.nix.jade.contractnet.CNETAgent(cnet2,cnet3,cnet4,cnet5,cnet6,cnet7)"+";"+
				"cnet2:ie.nix.jade.contractnet.CNETAgent"+";"+
				"cnet3:ie.nix.jade.contractnet.CNETAgent"+";"+
				"cnet4:ie.nix.jade.contractnet.CNETAgent"+";"+
				"cnet5:ie.nix.jade.contractnet.CNETAgent"+";"+
				"cnet6:ie.nix.jade.contractnet.CNETAgent"+";"+
				"cnet7:ie.nix.jade.contractnet.CNETAgent"
		};		
		jade.Boot.main(args);
	}
}
