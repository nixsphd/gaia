package schellingLSPM;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;

abstract public class Shape {
	
	protected ShapeContextBuilder contextBuilder;
	protected Context<Shape> context;
	protected Grid<Shape> grid;
	protected int id;
	protected double desiredNeighbourRatio;
	protected int neighbourhoodSize;
	protected int location;
	protected boolean isHappy;
	
	@SuppressWarnings("unchecked")
	protected Shape(ShapeContextBuilder contextBuilder, int location, 
			int neighbourhoodSize,  double desiredNeighbourRatio) {
		this.contextBuilder = contextBuilder;
		this.context = contextBuilder.getContext();
		this.grid = (Grid<Shape>)context.getProjection("Grid");
		this.id = location;
		this.location = location;
		this.neighbourhoodSize = neighbourhoodSize;
		this.desiredNeighbourRatio = desiredNeighbourRatio;
	}
	
	@ScheduledMethod(start=1, interval=1, priority=ScheduleParameters.FIRST_PRIORITY)
	public boolean isHappy() {
//		System.out.print(" "+id);
//		System.out.println("NDB::"+id+".isHappy()");
		isHappy = (getLikeNeighbourRatio() >= desiredNeighbourRatio);
//		System.out.println("NDB::"+id+".isHappy()->"+(boolean)(neighbourRatio > desiredNeighbourRatio));
		return isHappy;
		
	}
	
	@ScheduledMethod(start=1, interval=1, pick=1)
	public void happyEnding() {
		System.out.println("NDB::"+id+".happyEnding()");
		if (contextBuilder.allHappy()) {
			if (RunEnvironment.getInstance() != null) {
				RunEnvironment.getInstance().endRun();
			}
		}
	}

	
	@ScheduledMethod(start=1, interval=1, priority=ScheduleParameters.LAST_PRIORITY-1)
	public void beHappy() {
//		System.out.println("NDB::"+id+".beHappy()");
		if (!isHappy) {
			int happyPlace = findHappyPlace();
			if (happyPlace != -1) {
				moveTo(happyPlace);
			} else {
				if (RunEnvironment.getInstance() != null) {
					RunEnvironment.getInstance().endRun();
				} else {
					throw new RuntimeException("Happiness will always alude this grid setup :(");
				}
			}
		}
//		printGrid();
		
	}
	
	@ScheduledMethod(start=1, interval=1, pick=1, priority=ScheduleParameters.LAST_PRIORITY)
	public void foreverUnhappy() {
//		System.out.println("NDB::"+id+".foreverUnhappy()");
		if (contextBuilder.foreverUnhappy()) {
//			System.out.println("NDB::"+id+".foreverUnhappy()->Off the cliff so...");
			if (RunEnvironment.getInstance() != null) {
				RunEnvironment.getInstance().endRun();
			} else {
				throw new RuntimeException("Happiness will always alude this grid setup :(");
			}
		}
	}
	
	public double getLikeNeighbourRatio() {
		return getNeighboursRatio(location-neighbourhoodSize, location+neighbourhoodSize);
	}
	
	protected double getNeighboursRatio(int start, int end) {
		start = clampStart(start);
		end = clampEnd(end);
//		System.out.println("NDB::"+id+".getNeighboursRatio("+start+","+end+")");
		double same = 0;
		for (int i = start; i <= end; i++) {
			Shape shape = grid.getObjectAt(i,0);
			if (shape != null && getClass().equals(shape.getClass()) ) {
				same++;
			} 
		}
//		System.out.println("NDB::"+id+".getNeighboursRatio("+start+","+end+")->"+same/(end-start+1));
		return same/(end-start+1);
		
	}

	protected int getLocation() {
		return location;
	}
	
	protected void setLocation(int location) {
//		System.out.println("NDB::"+id+".setLocation("+location+")");
		this.location = location;
		grid.moveTo(this, location, 0);
	}
	
	protected int findHappyPlace() {
//		System.out.println("NDB::"+id+".findHappyPlace()");
		// find nearest happy place
		for (int displacement = 1; displacement < grid.getDimensions().getWidth()-1; displacement++) {
//			System.out.println("NDB::"+id+".findHappyPlace()~displacement="+displacement);
			if (location+displacement < grid.getDimensions().getWidth()) {
				if (displacement <= neighbourhoodSize) {
					double neighbourRatio = getNeighboursRatio(
							location+displacement-neighbourhoodSize, 
							location+displacement+neighbourhoodSize);
					if (neighbourRatio >= desiredNeighbourRatio) {
//						System.out.println("NDB::"+id+".findHappyPlace()->"+(location+displacement));
						return (location+displacement);
					}
				} else {
					double neighbourRatio = getNeighboursRatio(
							location+displacement-neighbourhoodSize+1, 
							location+displacement+neighbourhoodSize);
					if (neighbourRatio >= desiredNeighbourRatio) {
//						System.out.println("NDB::"+id+".findHappyPlace()->"+(location+displacement));
						return (location+displacement);
					}
				}
			}

			if (location-displacement >= 0) {
				if (displacement <= neighbourhoodSize) {
					double neighbourRatio = getNeighboursRatio(
							location-displacement-neighbourhoodSize, 
							location-displacement+neighbourhoodSize);
					if (neighbourRatio >= desiredNeighbourRatio) {
//						System.out.println("NDB::"+id+".findHappyPlace()->"+(location-displacement));
						return (location-displacement);
					}
				} else {
					double neighbourRatio = getNeighboursRatio(
							location-displacement-neighbourhoodSize, 
							location-displacement+neighbourhoodSize-1);
					if (neighbourRatio >= desiredNeighbourRatio) {
//						System.out.println("NDB::"+id+".findHappyPlace()->"+(location-displacement));
						return (location-displacement);
					}
				}
			}
		}
		// No happy place
//		System.out.println("NDB::"+id+".findHappyPlace()->None ;(");
		return -1;
	}

	private void moveTo(int newLocation) {
//		System.out.println("NDB::"+id+".moveTo("+newLocation+")");
		context.remove(this);
		if (location < newLocation) {
//			System.out.println("NDB::"+id+".moveTo("+newLocation+")~Shift up");
			// Shift all jumped down 1
			for (int i = location + 1; i <= newLocation; i++) {
//				System.out.println("NDB::"+id+".moveTo("+newLocation+")~Shift "+i+" to "+(i-1));
				grid.getObjectAt(i,0).setLocation(i-1);
			}
		} else {
//			System.out.println("NDB::"+id+".moveTo("+newLocation+")~Shift down");
			// Shift all jumped up 1
			for (int i = location - 1; i >= newLocation; i--) {
//				System.out.println("NDB::"+id+".moveTo("+newLocation+")~Shift "+i+" to "+(i+1));
				grid.getObjectAt(i,0).setLocation(i+1);
			}
		}
//		System.out.println("NDB::"+id+".moveTo("+newLocation+")~Shift "+id+" to "+newLocation);
		context.add(this);
		setLocation(newLocation);
	}
	
	private int clampStart(int start) {
		return Math.max(0, start);
	}
	
	private int clampEnd(int end) {
		return Math.min(grid.getDimensions().getWidth()-1, end);
	}
	
}