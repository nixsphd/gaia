package schellingLSPM;

public class Circle extends Shape {
	
	public Circle(ShapeContextBuilder contextBuilder, int id, 
			int neighbourhoodSize, double desiredNeighbourRatio) {
		super(contextBuilder, id, neighbourhoodSize, desiredNeighbourRatio);
	}
	
	public String toString() {
		return "Circle["+id+"]";
	}
}
