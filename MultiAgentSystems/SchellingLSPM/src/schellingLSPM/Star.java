package schellingLSPM;

public class Star extends Shape {
	
	public Star(ShapeContextBuilder contextBuilder, int id, 
			int neighbourhoodSize, double desiredNeighbourRatio) {
		super(contextBuilder, id, neighbourhoodSize, desiredNeighbourRatio);
	}
	
	public String toString() {
		return "Star["+id+"]";
	}
}
