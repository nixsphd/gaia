package schellingLSPM;

import java.util.ArrayList;
import java.util.Iterator;

import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.space.grid.StrictBorders;

public class ShapeContextBuilder implements ContextBuilder<Shape> {
			
	private Context<Shape> context;
	private Grid<Shape> grid;
	
	private int gridSize;
	private int neighbourhoodSize;
	private double desiredNeighbourRatio;
	
	private char[] state;
	
	public Context<Shape> build(Context<Shape> context) {
		RunEnvironment runEnvironment = RunEnvironment.getInstance();
		Parameters parms = runEnvironment.getParameters();
		// This is in case the program gets stuck in an infinite loop which can happen with big neighbourhoods. 
		runEnvironment.endAt(500);
		return build(context, (Integer)parms.getValue("gridSize"), (Integer)parms.getValue("neighbourhoodSize"), (Double)parms.getValue("desiredNeighbourRatio"));
	}
	
	public Context<Shape> build(Context<Shape> context, int gridSize, int neighbourhoodSize, double desiredNeighbourRatio) {
		this.context = context;
		this.gridSize = gridSize;
		this.neighbourhoodSize = neighbourhoodSize;
		this.desiredNeighbourRatio = desiredNeighbourRatio;
		// Create the grid for the Shapes
		this.grid = GridFactoryFinder.createGridFactory(null).createGrid("Grid", context, 
				new GridBuilderParameters<Shape>(new StrictBorders(), 
						new RandomGridAdder<Shape>(), false, gridSize, 1));

		initShapes();
		return context;
	}

	public void initShapes() {
		// Todo: Add in initial shape ratio.
		// Create and place a new Shape in each grid location.
		for (int i = 0; i < gridSize; i++){
			if (RandomHelper.nextIntFromTo(0, 1) == 0) {
				addStar(i);
			} else {
				addCircle(i);
			}
		}
	}
	
	public void clearShapes() {
		context.clear();
	}
	
	private void addStar(int location) {		
		Star star = new Star(this,location, neighbourhoodSize, desiredNeighbourRatio);
		if (!context.add(star)) {
			throw new RuntimeException("Probelm adding "+star+"");
		}
		grid.moveTo(star, location, 0);
	}
	
	private void addCircle(int location) {
		Circle circle = new Circle(this,location, neighbourhoodSize, desiredNeighbourRatio);
		if (!context.add(circle)) {
			throw new RuntimeException("Probelm adding "+circle+"");
		}
		
		grid.moveTo(circle, location, 0);
	}
	
	public boolean allHappy() {
//		System.out.println("NDB::ShapeContextBuilder.happyOut()");
		for (int i=0; i < grid.getDimensions().getWidth(); i++) {
			if (!grid.getObjectAt(i,0).isHappy) {
//				System.out.println("NDB::ShapeContextBuilder.happyOut()->No, I'm not says "+i);
//				System.out.println("NDB::ShapeContextBuilder.happyOut()->No, grid="+new String(getGridState()));
				return false;
			}
		}
//		System.out.println("NDB::ShapeContextBuilder.happyOut()->Yes");
		return true;
	}
	
	public boolean foreverUnhappy() {
		if (state == null) {
			state = getGridState();
		} else {
			char[] newState = getGridState();
			if (new String(state).equals(new String(newState))) {
				return true;
			} else {
				state = newState;
			}
		}
		return false;
	}
	
	public char[] getGridState() {
		char[] gridState = new char[gridSize];
		for (int i = 0; i < grid.getDimensions().getWidth(); i++) {
			for (Iterator<Shape> shapes = grid.getObjectsAt(i,0).iterator(); shapes.hasNext(); ) {
				Shape next = shapes.next();
				if (next instanceof Circle) {
					gridState[i] = 'c';
				} else if (next instanceof Star) {
					gridState[i] = 's';
				} else {
					gridState[i] = ' ';
				}
					
			}
		}
		return gridState;
	}
	
	public Iterable<Shape> getShapes() {
		ArrayList<Shape> shapes = new ArrayList<Shape>(); 
		for (int i = 0; i < grid.getDimensions().getWidth(); i++) {
			Iterator<Shape> shapesAtLocation = grid.getObjectsAt(i,0).iterator();
			if (shapesAtLocation.hasNext()) {
				shapes.add(shapesAtLocation.next());
			} else {
				throw new RuntimeException("Empty grid cell at "+i);
			}
			// TODO - Should I check if the cell has more than one shape?
		}
		return shapes;
	}

	public Context<Shape> getContext() {
		return context;
	}


	
}