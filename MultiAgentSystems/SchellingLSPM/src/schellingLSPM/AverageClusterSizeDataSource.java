/**
 * 
 */
package schellingLSPM;

import java.util.Iterator;

import repast.simphony.data2.AggregateDataSource;

/**
 * @author nick
 *
 */
public class AverageClusterSizeDataSource implements AggregateDataSource {

	@Override
	public String getId() {
		return "Average Cluster Size";
	}

	@Override
	public Class<?> getDataType() {
		return double.class;
	}

	@Override
	public Class<?> getSourceType() {
		return Shape.class;
	}

	@Override
	public Object get(Iterable<?> objs, int size) {
		
		int numberOfClusters = 1;
		@SuppressWarnings("unchecked")
		Iterator<Shape> shapes = (Iterator<Shape>)objs.iterator();
		
		Shape shape = shapes.next();
		while (shapes.hasNext()) {
			Shape neighbourShape = shapes.next();
			if (shape.getClass() != neighbourShape.getClass()) {
				numberOfClusters++;
			}
			shape = neighbourShape;
		}
		return size/(double)(numberOfClusters);
	}

	@Override
	public void reset() {}
	
}
