/**
 * 
 */
package schellingLSPM;

import java.util.Iterator;

import repast.simphony.data2.AggregateDataSource;

public class NumberOfClustersDataSource implements AggregateDataSource {

	@Override
	public String getId() {
		return "Number of Cluster";
	}

	@Override
	public Class<?> getDataType() {
		return int.class;
	}

	@Override
	public Class<?> getSourceType() {
		return Shape.class;
	}

	@Override
	public Object get(Iterable<?> objs, int size) {
		int numberOfClusters = 1;
		@SuppressWarnings("unchecked")
		Iterator<Shape> shapes = (Iterator<Shape>)objs.iterator();
		
		Shape shape = shapes.next();
		while (shapes.hasNext()) {
			Shape neighbourShape = shapes.next();
			if (shape.getClass() != neighbourShape.getClass()) {
				numberOfClusters++;
			}
			shape = neighbourShape;
		}
		return numberOfClusters;
	}

	@Override
	public void reset() {
	}

}
