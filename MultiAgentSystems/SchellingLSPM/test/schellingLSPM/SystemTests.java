package schellingLSPM;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.engine.schedule.Schedule;
import repast.simphony.random.RandomHelper;

public class SystemTests {

	private Context<Shape> context;
	private ShapeContextBuilder shapeContectBuilder;

	@Before
	public void setUp() throws Exception {
		context = new DefaultContext<Shape>();	
		shapeContectBuilder = new ShapeContextBuilder();
	
	}

	
	@Test
	public void testBasic() {
		int randomSeed = 11;
		int gridSize = 70;
		int neighbourhoodSize = 4; 
		double desiredNeighbourRatio = 0.5; 
		String expectedFinalState = "ccccsssssssssscccccccccccccccssssssscccccccssssssscccccccssssssssscccc";
		testState(randomSeed, gridSize, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	@Test
	public void testZeroGrid() {
		int randomSeed = 11;
		int gridSize = 0;
		int neighbourhoodSize = 4; 
		double desiredNeighbourRatio = 0.5; 
		String expectedFinalState = "";
		testState(randomSeed, gridSize, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	@Test
	public void testTinyGrid() {
		int randomSeed = 11;
		int gridSize = 1;
		int neighbourhoodSize = 4; 
		double desiredNeighbourRatio = 0.5; 
		String expectedFinalState = "c";
		testState(randomSeed, gridSize, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	@Test (expected=RuntimeException.class)
	public void testNeighbourhoodBiggerThanGrid() {
		int randomSeed = 11;
		int gridSize = 10;
		int neighbourhoodSize = 11; 
		double desiredNeighbourRatio = 0.5; 
		String expectedFinalState = "";
		testState(randomSeed, gridSize, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	@Test
	public void testBigGrid() {
		int randomSeed = 11;
		int gridSize = 700;
		int neighbourhoodSize = 4; 
		double desiredNeighbourRatio = 0.5; 
		String expectedFinalState = "ccccsssssssssscccccccssssssssssssssscccccsssssccccccccccccccccccssssssssccccccccssssssccccccsssssssssccccccccssssssssssccccccccccccccssssssssssssssssssssssssscccccccccccssssssssssscccccccccccccccccccccccssssssssssssssssccccccccccccccsssssssssccccccccsssssssssccccccccsssssssssssssssccccccccccssssssscccccccccsssssssscccccccccccssssssssssssssssccccccccssssssscccccccssssssscccccccsssssssscccccccccccccccsssssssssssssssssssscccccccsssssssscccccccccccccccccccssssssssssscccccccccccccssssssssssscccccccccsssssscccccccccccccccsssssssssssscccccccccccccccccccccccccccccsssssssssssscccccsssssccccccccccsssssssssssssccccccsssssccccccccssssssssscccccsssssssscccccccssssssssssssssssccccccccccccccccccsssssssssss";
		testState(randomSeed, gridSize, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	@Test
	public void testBigNeighbourhood() {
		int randomSeed = 11;
		int gridSize = 70;
		int neighbourhoodSize = 30; 
		double desiredNeighbourRatio = 0.5; 
		String expectedFinalState = "cccccccccccccccccccccccccccccccccccccsssssssssssssssssssssssssssssssss";
		testState(randomSeed, gridSize, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	@Test (expected=RuntimeException.class)
	public void testUnhappyForever() {
		int randomSeed = 11;
		int gridSize = 70;
		int neighbourhoodSize = 20; 
		double desiredNeighbourRatio = 0.5; 
		String expectedFinalState = "cccccccccccccccccccccccccccccccccccccsssssssssssssssssssssssssssssssss";
		testState(randomSeed, gridSize, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	public void testState(int randomSeed, int gridSize, int neighbourhoodSize, double desiredNeighbourRatio, String expectedFinalState) {
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, gridSize, neighbourhoodSize, desiredNeighbourRatio);
		
		ISchedule schedule = new Schedule();
		for (Shape shape : shapeContectBuilder.getShapes()) {
			schedule.schedule(shape);
		}

		while (!shapeContectBuilder.allHappy()) {
			schedule.execute();
		}

		String actualFinalState = new String(shapeContectBuilder.getGridState());
//		System.out.println("NDB::testOne()~actualFinalState="+actualFinalState);
		Assert.assertEquals(expectedFinalState, actualFinalState);
			
	}
	
	@Test
	public void testDataSources() {
		int randomSeed = 11;
		int gridSize = 70;
		int neighbourhoodSize = 4; 
		double desiredNeighbourRatio = 0.5; 
		double expectedAverageClusterSize = 7.777777777777778;
		int expectedNumberOfClusters = 9;
		double expectedPercentageOfLikeNeighbours= 73.60090702947846;
		testDataSources(randomSeed, gridSize, neighbourhoodSize, desiredNeighbourRatio, 
				expectedAverageClusterSize, expectedNumberOfClusters, expectedPercentageOfLikeNeighbours);
		
	}

	public void testDataSources(int randomSeed, int gridSize, int neighbourhoodSize, double desiredNeighbourRatio, 
			double expectedAverageClusterSize, int expectedNumberOfClusters, double expectedPercentageOfLikeNeighbours) {
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, gridSize, neighbourhoodSize, desiredNeighbourRatio);

		Iterable<Shape> shapes = shapeContectBuilder.getShapes();
		
		ISchedule schedule = new Schedule();
		for (Shape shape : shapes) {
			schedule.schedule(shape);
		}

		while (!shapeContectBuilder.allHappy()) {
			schedule.execute();
		}
		
		// The order will have changes so I need to get them again.
		shapes = shapeContectBuilder.getShapes();
		
		AverageClusterSizeDataSource averageClusterSizeDataSource = new AverageClusterSizeDataSource();
		Double actualAverageClusterSize = (Double)averageClusterSizeDataSource.get(shapes, gridSize);
//		System.out.println("NDB::testOne()~actualAverageClusterSize="+actualAverageClusterSize);
		Assert.assertEquals(expectedAverageClusterSize, actualAverageClusterSize.doubleValue(),0.0000001);
		
		NumberOfClustersDataSource numberOfClustersDataSource = new NumberOfClustersDataSource();
		Integer actualNumberOfClusters = (Integer)numberOfClustersDataSource.get(shapes, gridSize);
//		System.out.println("NDB::testOne()~actualNumberOfClusters="+actualNumberOfClusters);
		Assert.assertEquals(expectedNumberOfClusters, actualNumberOfClusters.intValue());
		
		PercentageOfLikeNeighboursDataSource percentageOfLikeNeighboursDataSource = new PercentageOfLikeNeighboursDataSource();
		Double actualPercentageOfLikeNeighbours = (Double)percentageOfLikeNeighboursDataSource.get(shapes, gridSize);
//		System.out.println("NDB::testOne()~actualPercentageOfLikeNeighbours="+actualPercentageOfLikeNeighbours);
		Assert.assertEquals(expectedPercentageOfLikeNeighbours, actualPercentageOfLikeNeighbours.doubleValue(), 0.0000001);
	}
}
