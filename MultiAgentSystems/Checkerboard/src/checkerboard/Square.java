package checkerboard;

public class Square extends Piece {

	public Square(Checkerboard checkerboard) {
		super(checkerboard);
	}

	@Override
	public String toString() {
		return "Square " + location;
	}

}
