package checkerboard;

import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.space.grid.StrictBorders;

public class Checkerboard implements ContextBuilder<Piece> {

	protected Context<Piece> context;
	protected Grid<Piece> grid;
	// TODO support a rectangular board.
	protected Integer gridSize;
	// TODO support different number of pieces per type
	protected int numberOfPieces;
	protected Model model;

	@Override
	public Context<Piece> build(Context<Piece> context) {
		RunEnvironment runEnvironment = RunEnvironment.getInstance();
		Parameters parms = runEnvironment.getParameters();
		return build(context,
				((Integer)parms.getValue("GridSize")).intValue(),
				((Integer)parms.getValue("NumberOfPieces")).intValue(),
				Model.getModel((String)parms.getValue("Model")));
	}

	protected Context<Piece> build(Context<Piece> context, int gridSize, int numberOfPieces, Model model) {
		this.context = context;
		this.gridSize = gridSize;
		this.model =  model;
		
		// Create the board
		this.grid = GridFactoryFinder.createGridFactory(null).createGrid("CheckerboardGrid", context, 
				new GridBuilderParameters<Piece>(new StrictBorders(), 
						new RandomGridAdder<Piece>(), false, gridSize, gridSize));
	
		initPieces(numberOfPieces);
		return context;
	}

	protected Context<Piece> build(Context<Piece> context, char[][] pieces, Model model) {
		this.context = context;
		this.gridSize = pieces.length;
		this.model =  model;
		
		// Create the board
		this.grid = GridFactoryFinder.createGridFactory(null).createGrid("CheckerboardGrid", context, 
				new GridBuilderParameters<Piece>(new StrictBorders(), 
						new RandomGridAdder<Piece>(), true, gridSize, gridSize));
		
		initPieces(pieces);
		return context;
	}

	protected void initPieces(int numberOfPieces) {
		for (int p = 0; p < numberOfPieces; p++) {
			// Create and place a new Piece in each grid location.
			addPiece(new Cross(this));
			addPiece(new Square(this));
		}
		
	}

	protected void initPieces(char[][] pieces) {
		for (int x = 0; x < pieces.length; x++) {
			for (int y = 0; y < pieces[x].length; y++){
				if (pieces[x][y] == 's') {
					Square square = new Square(this);
					addPiece(square,new GridPoint(x,y));
//					System.out.println("NDB::initPieces()~"+pieces[x][y]+"["+x+","+y+"]->"+square);
				} else if (pieces[x][y] == 'c') {
					Cross cross = new Cross(this);
					addPiece(cross,new GridPoint(x,y));
//					System.out.println("NDB::initPieces()~"+pieces[x][y]+"["+x+","+y+"]->"+cross);
				}
			}
		}
		
	}

	protected void addPiece(Piece newPiece) {
		if (!context.add(newPiece)) {
			throw new RuntimeException("Probelm adding "+newPiece+"");
		}
		newPiece.moveTo(grid.getLocation(newPiece));
	}
	
	protected void addPiece(Piece newPiece, GridPoint location) {
		if (!context.add(newPiece)) {
			throw new RuntimeException("Probelm adding "+newPiece+"");
		}
		newPiece.moveTo(location);
	}
	
	protected Piece[][] getPieces() {
		Piece[][] pieces = new Piece[grid.getDimensions().getWidth()][grid.getDimensions().getHeight()];
		for (int x = 0; x < pieces.length; x++) {
			for (int y = 0; y < pieces[x].length; y++) {
				Piece piece = getPiece(x, y);
				pieces[x][y] = piece;
//				System.out.println("NDB::getPieces()~"+pieces[x][y]+"["+x+","+y+"]->"+piece);
			}
		}
		return pieces;		
	}
	protected char[][] getState() {
		char[][] state = new char[grid.getDimensions().getWidth()][grid.getDimensions().getHeight()];
		for (int x = 0; x < state.length; x++) {
			for (int y = 0; y < state[x].length; y++) {
				Piece piece = getPiece(x, y);
				if (piece == null) {
					state[x][y] = ' ';					
				} else if (piece instanceof Cross) {
					state[x][y]  = 'c';
				} else if (piece instanceof Square) {
					state[x][y]  = 's';
				} else {
					state[x][y]  = '?';
				}
			}
		}
		return state;		
	}
	
	public Piece getPiece(GridPoint location) {
		return grid.getObjectAt(location.getX(), location.getY());
	}
	
	public Piece getPiece(int x, int y) {
		return grid.getObjectAt(x, y);
	}

	public Iterable<Piece> getListOfPieces() {
		return grid.getObjects();
	}
	
}
