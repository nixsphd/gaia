package checkerboard;

public class Cross extends Piece {

	public Cross(Checkerboard checkerboard) {
		super(checkerboard);
	}

	@Override
	public String toString() {
		return "Cross " + location;
	}

}
