package checkerboard;

public class Model {
	
	public static Model getModel(String modelName) {
		if (modelName.equals("Crossroads")) {
			return Crossroads;
		} else if (modelName.equals("Mutual Suspicion")) {
			return MutualSuspicion;
		} else if (modelName.equals("Segregation")) {
			return Segregation;
		} else if (modelName.equals("Social Climber")) {
			return SocialClimber;
		} else if (modelName.equals("Social Worker")) {
			return SocialWorker;
		} else if (modelName.equals("Boy-Girl")) {
			return BoyGirl;
		} else if (modelName.equals("Couples")) {
			return Couples;
		} else if (modelName.equals("Husbands-Wives")) {
			return HusbandsWives;
		} 
		// TODO - Do something smarter here like return an Exception.
		return null;
	}

	protected static Model Crossroads = new Model(1, 1, 0, 0);
	protected static Model MutualSuspicion = new Model(0, 0, -1, -1);
	protected static Model Segregation = new Model(1, 1, -1, -1);
	protected static Model SocialClimber = new Model(-1, 1, 1, -1);
	protected static Model SocialWorker = new Model(1,- 1, 1, -1);
	protected static Model BoyGirl = new Model(-1, -1, 1, 1);
	protected static Model Couples = new Model(-4, -4, 1, 1);
	protected static Model HusbandsWives = new Model(-4, 1, 2, 2);
	
	int squares2Squares;	
	int squares2Crosses;
	int crosses2Squares;
	int crosses2Crosses;
	// TODO - Make this a param, choices are 2 or 4
	double distanceWeight = 2;
	
	protected Model(int squares2Squares, int crosses2Crosses, 
			int squares2Crosses, int crosses2Squares) {
		this.squares2Squares = squares2Squares;
		this.squares2Crosses = squares2Crosses;
		this.crosses2Squares = crosses2Squares;
		this.crosses2Crosses = crosses2Crosses;
	}
	
	public int getSquares2Squares() {
		return squares2Squares;
	}

	public int getSquares2Crosses() {
		return squares2Crosses;
	}

	public int getCrosses2Squares() {
		return crosses2Squares;
	}

	public int getCrosses2Crosses() {
		return crosses2Crosses;
	}

	public double getValence(Piece pieceA, Piece pieceB) {
		if (pieceA instanceof Square && pieceB instanceof Square) {
			return getSquares2Squares();
		} else if (pieceA instanceof Square && pieceB instanceof Cross) {
			return getSquares2Crosses();
		} else if (pieceA instanceof Cross && pieceB instanceof Square) {
			return getCrosses2Squares();
		} else if (pieceA instanceof Cross && pieceB instanceof Cross) {
			return getCrosses2Crosses();
		} else {
			// TODO - Should probably do something better here like throw an exception.
			return 0;
		}
	}

	public double getDistanceWeight() {
		return distanceWeight;
	}

}