package checkerboard;

import java.util.ArrayList;
import java.util.List;

import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.GridPoint;

public abstract class Piece {
	
	Checkerboard checkerboard;
	GridPoint location;

	public Piece(Checkerboard checkerboard) {
		this.checkerboard = checkerboard;
	}

	public GridPoint getLocation() {
		return location;
	}

	private void setLocation(GridPoint location) {
		this.location = location;
	}

	@ScheduledMethod(start=1, interval=1)
	public void move() {
		// Look first in the immediate neighbourhood, 8 adjoining cells. 
		GridPoint mostAdvantageousMove = getMostAdvantageousMove(1);		
		if (mostAdvantageousMove.getX() == location.getX() && 
			mostAdvantageousMove.getY() == location.getY()) {
			// If there no better advantage ther then look first in the 
			// next closest neighbourhood, 16 cells. 
			mostAdvantageousMove = getMostAdvantageousMove(2);
			
		}
		// If there is a better space then move, otherwise stay put.
		if (!(mostAdvantageousMove.getX() == location.getX() && 
			  mostAdvantageousMove.getY() == location.getY())) {
			moveTo(mostAdvantageousMove);
//			System.out.println("NDB::"+this+".move()~mostAdvantageousMove="+mostAdvantageousMove);
		}
		
	}

	protected GridPoint getMostAdvantageousMove(int searchDistance) {
		char[][] checkMask = checkerboard.getState();
		
		// Get a list of locations to check
		List<GridPoint> locationsToCheck = new ArrayList<GridPoint>();		
		for (int x = location.getX()-searchDistance; x <= location.getX()+searchDistance; x++) {
			for (int y = location.getY()-searchDistance; y <= location.getY()+searchDistance; y++) {
				if ((x >= 0) && (x < checkerboard.gridSize) && 
					(y >= 0) && (y < checkerboard.gridSize) &&
					(!((x >= location.getX()-(searchDistance-1)) && (x <= location.getX()+(searchDistance-1)) &&
					   (y >= location.getY()-(searchDistance-1)) && (y <= location.getY()+(searchDistance-1)))) &&
					(checkerboard.getPiece(x,y) == null))
				{
					locationsToCheck.add(new GridPoint(x,y));
					checkMask[x][y] = 'X';
				} 
			}	
		}
//		System.out.println("NDB::"+this+".getMostAdvantageousMove()~checkMask="+PieceUnitTests.toString(checkMask));	

		// Get values for the current location.
		GridPoint mostAdvantageousLocation = this.location;
		double bestFitness = Double.NEGATIVE_INFINITY;
		
		// Find the best location form the list.
		for (GridPoint locationToCheck : locationsToCheck) {
			double fitnessAtLocation = getFitnessAt(locationToCheck);
			if (bestFitness < fitnessAtLocation) {
				bestFitness = fitnessAtLocation;
				mostAdvantageousLocation = locationToCheck;
			}
		}
//		System.out.println("NDB::"+this+".getMostAdvantageousMove()~mostAdvantageousLocation="+mostAdvantageousLocation);	
		return mostAdvantageousLocation;
	}

	protected double getFitnessAt(GridPoint location) {
		// Get distance weight
		double distanceWeight = checkerboard.model.getDistanceWeight();
		
		double totalFitness = 0;
		for (Piece piece : checkerboard.getListOfPieces()) {
			if (!piece.getLocation().equals(location)) {
				// Get valence
				double valence = checkerboard.model.getValence(this, piece);
				// Get distance
				if (valence != 0.0) {
					double distance = Math.pow(piece.getLocation().getX() - location.getX(),2) + 
									  Math.pow(piece.getLocation().getY() - location.getY(),2);						
					// The fitness is the sum of the valance divided by the weighted distance
					totalFitness += valence/Math.pow(distance,1.0/distanceWeight);
	//				System.out.println("NDB::"+this+".getFitnessAt()~totalFitness="+totalFitness);	
				} 
			}
		}
		return totalFitness;
	}

	protected void moveTo(GridPoint newLocation) {
		checkerboard.grid.moveTo(this, newLocation.getX(), newLocation.getY());
		setLocation(newLocation);
		
	}
}
