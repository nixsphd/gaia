package checkerboard;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.GridPoint;

public class UnitTests {
	
	private Context<Piece> context;
	private Checkerboard checkerboard;

	@Before
	public void setUp() throws Exception {
		RandomHelper.setSeed(1);
		context = new DefaultContext<Piece>();	
		checkerboard = new Checkerboard();
	
	}
	
	@Test
	public void testMoveTo() {		
		int gridSize = 8;
		int numberOfPieces = 6;
		Model model = Model.Crossroads;
		GridPoint moveTo = new GridPoint(3, 3);
		
		checkerboard.build(context, gridSize, numberOfPieces, model);
		Piece piece = context.getRandomObject();
		
		piece.moveTo(moveTo);

//		System.out.println("NDB::testBuild()~actualPieces="+toString(checkerboard.getPieces()));
		Assert.assertEquals(moveTo, checkerboard.grid.getLocation(piece));
		Assert.assertEquals(moveTo, piece.getLocation());
		
	}
	
	@Test
	public void testBuild() {		
		char[][] pieces = {
				{'c','s'},
				{'c',' '}
				};
		Model model = Model.Crossroads;
		
		checkerboard.build(context, pieces, model);
		
		Piece[][] actualPieces = checkerboard.getPieces();
//		System.out.println("NDB::testBuild()~pieces="+toString(pieces));
//		System.out.println("NDB::testBuild()~actualPieces="+toString(actualPieces));		
		Assert.assertEquals(toString(pieces),toString(actualPieces));
		
	}
	
	@Test
	public void testGetMostAdvantageousMoveSearchDist1() {		
		char[][] initialPieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.Crossroads;
		GridPoint locationToTest = new GridPoint(3,3);
		int searchDistance = 1;
		GridPoint expectedNewLocation = new GridPoint(3,2);
		
		testGetMostAdvantageousMove(initialPieces, model, locationToTest, searchDistance, expectedNewLocation);
		
	}
	
	@Test
	public void testGetMostAdvantageousMoveNot() {		
		char[][] initialPieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s','s',' ',' ',' '},
				{' ',' ','c','c','s',' ',' ',' '},
				{' ',' ','c','c','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.Crossroads;
		GridPoint locationToTest = new GridPoint(3,3);
		int searchDistance = 1;
		GridPoint expectedNewLocation = new GridPoint(3,3);
		
		testGetMostAdvantageousMove(initialPieces, model, locationToTest, searchDistance, expectedNewLocation);
		
	}
	
	@Test
	public void testGetMostAdvantageousMoveSearchDist2() {		
		char[][] initialPieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.Crossroads;
		GridPoint locationToTest = new GridPoint(3,3);
		int searchDistance = 2;
		GridPoint expectedNewLocation = new GridPoint(1,3);
		
		testGetMostAdvantageousMove(initialPieces, model, locationToTest, searchDistance, expectedNewLocation);
		
	}

	public void testGetMostAdvantageousMove(char[][] initialPieces, Model model, GridPoint locationToTest, int searchDistance, GridPoint expectedNewLocation) {	 
		checkerboard.build(context, initialPieces, model);
		
		Piece piece = checkerboard.getPiece(locationToTest);
		GridPoint actualNewLocation = piece.getMostAdvantageousMove(searchDistance);
		
//		System.out.println("NDB::testGetMostAdvantageousMove()~actualNewLocation="+actualNewLocation);
		Assert.assertEquals(expectedNewLocation, actualNewLocation);
		
	}
	
	@Test
	public void testGetFitnessAt() {		
		char[][] pieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.Crossroads;
		GridPoint locationToTest = new GridPoint(3,3);
		double expectedFitness = 2.207873767279779;
		
		checkerboard.build(context, pieces, model);
		
		Piece piece = checkerboard.getPiece(locationToTest);
		double actualFitness = piece.getFitnessAt(locationToTest);
		
//		System.out.println("NDB::testGetFitnessAt()~actualFitness="+actualFitness);
		Assert.assertEquals(expectedFitness, actualFitness,0.0000001);
		
	}
	
	@Test
	public void testMove() {		
		char[][] pieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.Crossroads;
		GridPoint locationToTest = new GridPoint(3,3);
		char[][] expectedPieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ','c',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		
		checkerboard.build(context, pieces, model);
		
		Piece piece = checkerboard.getPiece(locationToTest);
		piece.move();
		
		Piece[][] actualPieces = checkerboard.getPieces();
//		System.out.println("NDB::testBuild()~expectedPieces="+toString(expectedPieces));
//		System.out.println("NDB::testBuild()~actualPieces="+toString(actualPieces));		
		Assert.assertEquals(toString(expectedPieces),toString(actualPieces));
		
	}
	
	static String toString(char[][] state) {
		StringBuilder string = new StringBuilder("{\n");
		for (int x = 0; x < state.length; x++) {
			string.append("{");
			for (int y = 0; y < state[x].length; y++) {
				string.append("'"+state[x][y]+"',");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.lastIndexOf(","), string.length(),"\n}");
		return string.toString();
	}	
	
	static String toString(Piece[][] state) {
		StringBuilder string = new StringBuilder("{\n");
		for (int x = 0; x < state.length; x++) {
			string.append("{");
			for (int y = 0; y < state[x].length; y++) {
				Piece piece = state[x][y];
				if (piece == null) {
					string.append("' ',");
				} else if (piece instanceof Square) {
					string.append("'s',");
				} else if (piece instanceof Cross) {
					string.append("'c',");
				}
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.lastIndexOf(","), string.length(),"\n}");
		return string.toString();
	}
}
