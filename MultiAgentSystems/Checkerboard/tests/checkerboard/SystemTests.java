package checkerboard;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.engine.schedule.Schedule;
import repast.simphony.random.RandomHelper;

public class SystemTests {
	
	private Context<Piece> context;
	private Schedule schedule;
	private Checkerboard checkerboard;

	@Before
	public void setUp() throws Exception {	
		RandomHelper.setSeed(1);
		context = new DefaultContext<Piece>();	
		schedule = new Schedule(); 
		checkerboard = new Checkerboard();	
	
	}
	
	@Test
	public void testCrossroads() {	
		char[][] initialPieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.Crossroads;
		int numberOfSteps = 50;
		char[][] expectedPieces = {
				{' ',' ',' ',' ',' ','s','s',' '},
				{' ',' ','c',' ',' ',' ','s','s'},
				{' ','c',' ','c',' ','s','s',' '},
				{' ','c','c',' ',' ',' ',' ',' '},
				{' ',' ','c',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
		};
		
		testSteps(initialPieces, model, numberOfSteps, expectedPieces);
	}
		
	@Test
	public void testMutualSuspicion() {	
		char[][] initialPieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.MutualSuspicion;
		int numberOfSteps = 50;
		char[][] expectedPieces = {
				{' ','s','s',' ',' ',' ','c',' '},
				{'s','s',' ',' ',' ',' ',' ','c'},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c','c',' ',' ',' ',' ',' ','s'},
				{' ','c','c',' ',' ',' ','s',' '}
				};
		
		testSteps(initialPieces, model, numberOfSteps, expectedPieces);
	}
	
	@Test
	public void testSegregation() {	
		char[][] initialPieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.Segregation;
		int numberOfSteps = 50;
		char[][] expectedPieces = {
				{' ','s','s',' ',' ',' ',' ',' '},
				{'s','s','s',' ',' ',' ',' ',' '},
				{' ',' ','s',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ','c',' '},
				{' ',' ',' ',' ',' ',' ','c','c'},
				{' ',' ',' ',' ','c','c','c',' '}
				};
		
		testSteps(initialPieces, model, numberOfSteps, expectedPieces);
	}

	@Test
	public void testSocialClimber() {	
		char[][] initialPieces = {
				{'s',' ',' ','s',' ','s',' ','c'},
				{' ','s','c',' ',' ','c',' ',' '},
				{' ',' ','c','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '}
				};
		Model model = Model.SocialClimber;
		int numberOfSteps = 50;
		char[][] expectedPieces = {
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ','s','c','c'},
				{' ','s','s',' ',' ',' ',' ','s'},
				{'c','c',' ',' ',' ',' ',' ',' '},
				{'c',' ','c','s',' ',' ',' ',' '},
				{' ','s',' ',' ',' ',' ',' ',' '}
				};
		
		testSteps(initialPieces, model, numberOfSteps, expectedPieces);
	}
	
	@Test
	public void testSocialWorker() {	
		char[][] initialPieces = {
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c',' ',' ','c',' ',' ',' ','c'},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ','s',' '},
				{' ',' ',' ',' ','s','s','s','c'},
				{' ','c',' ',' ',' ',' ','s',' '}
				};
		Model model = Model.SocialWorker;
		int numberOfSteps = 50;
		char[][] expectedPieces = {
				{' ',' ',' ',' ',' ',' ',' ','c'},
				{'c',' ','c',' ',' ',' ',' ','c'},
				{' ',' ',' ',' ',' ',' ','s',' '},
				{' ',' ',' ',' ',' ','s','s',' '},
				{' ',' ',' ',' ','s','s',' ',' '},
				{' ',' ',' ',' ',' ','s',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ','c',' ',' ',' ',' ',' ','c'}
				};
		
		testSteps(initialPieces, model, numberOfSteps, expectedPieces);
	}

	@Test
	public void testBoyGirl() {	
		char[][] initialPieces = {
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c',' ',' ','c',' ',' ',' ','c'},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ','s',' '},
				{' ',' ',' ',' ','s','s','s','c'},
				{' ','c',' ',' ',' ',' ','s',' '}
				};
		Model model = Model.BoyGirl;
		int numberOfSteps = 70;
		char[][] expectedPieces = {
				{' ',' ',' ','s',' ',' ',' ',' '},
				{' ',' ',' ','c',' ',' ',' ',' '},
				{' ',' ',' ','s',' ',' ',' ',' '},
				{'s','c',' ','c',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ','s',' '},
				{' ',' ',' ',' ',' ',' ','c',' '},
				{'c','s',' ',' ',' ','s',' ',' '},
				{' ',' ',' ',' ',' ','c',' ',' '}
				};
		
		testSteps(initialPieces, model, numberOfSteps, expectedPieces);
	}
	
	@Test
	public void testCouples() {	
		char[][] initialPieces = {
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c',' ',' ','c',' ',' ',' ','c'},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ','s',' '},
				{' ',' ',' ',' ','s','s','s','c'},
				{' ','c',' ',' ',' ',' ','s',' '}
				};
		Model model = Model.Couples;
		int numberOfSteps = 50;
		char[][] expectedPieces = {
				{' ','s',' ',' ',' ','s','c',' '},
				{' ','c',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ','s',' '},
				{' ','s','c',' ',' ',' ','c',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ','c',' ',' ',' ',' ',' ',' '},
				{' ','s',' ',' ',' ',' ','c','s'}
				};
		
		testSteps(initialPieces, model, numberOfSteps, expectedPieces);
	}
	
	@Test
	public void testHusbandsWives() {	
		char[][] initialPieces = {
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c',' ',' ','c',' ',' ',' ','c'},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'c',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ','s',' ','s',' '},
				{' ',' ',' ',' ','s','s','s','c'},
				{' ','c',' ',' ',' ',' ','s',' '}
				};
		Model model = Model.HusbandsWives;
		int numberOfSteps = 50;
		char[][] expectedPieces = {
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' ','s',' '},
				{' ',' ',' ',' ',' ',' ',' ',' '},
				{'s',' ',' ',' ',' ',' ',' ',' '},
				{' ',' ',' ','s','c','s',' ',' '},
				{' ',' ',' ',' ','c','c',' ',' '},
				{' ',' ',' ','s','c','c','s',' '},
				{' ',' ',' ',' ',' ','c',' ',' '}
				};
		
		testSteps(initialPieces, model, numberOfSteps, expectedPieces);
	}
	
	public void testSteps(char[][] initialPieces, Model model, int numberOfSteps, char[][] expectedPieces) {
		
//		System.out.println("NDB::testBuild()~initialPieces="+PieceUnitTests.toString(initialPieces));
//		System.out.println("NDB::testBuild()~expectedPieces="+PieceUnitTests.toString(expectedPieces));
	
		checkerboard.build(context, initialPieces, model);

		Iterable<Piece> pieces = checkerboard.getListOfPieces();
		for (Piece piece : pieces) {
			schedule.schedule(piece);
		}

		while (numberOfSteps >= 0) {
			schedule.execute();
			numberOfSteps--;
		}
		
		Piece[][] actualPieces = checkerboard.getPieces();
//		System.out.println("NDB::testBuild()~actualPieces="+PieceUnitTests.toString(actualPieces));			
		Assert.assertEquals(UnitTests.toString(expectedPieces),UnitTests.toString(actualPieces));
		
	}
}
