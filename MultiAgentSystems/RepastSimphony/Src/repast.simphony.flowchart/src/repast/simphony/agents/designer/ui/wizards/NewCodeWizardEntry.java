package repast.simphony.agents.designer.ui.wizards;

public class NewCodeWizardEntry {

	public String title = "";
	public String description = "";

	public NewCodeWizardEntry(String title, String description) {
		this.title = title;
		this.description = description;
	}
	
}
