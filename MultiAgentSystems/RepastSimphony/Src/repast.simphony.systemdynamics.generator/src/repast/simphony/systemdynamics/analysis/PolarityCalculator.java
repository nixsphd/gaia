package repast.simphony.systemdynamics.analysis;

public interface PolarityCalculator {
	
	public double compute(double[] args);

}
