/**
 * 
 */
package repast.simphony.systemdynamics.support;

/**
 * @author bragen
 *
 */
public interface Message {

    public void println(String s);
}
