/**
 * 
 */
package repast.simphony.systemdynamics.support;

/**
 * 
 * Use this class to perform any requried initialization prior to model execution
 * 
 * @author bragen
 *
 */
public class Initializer {

    public static void initialize() {
	
    }
    
}
