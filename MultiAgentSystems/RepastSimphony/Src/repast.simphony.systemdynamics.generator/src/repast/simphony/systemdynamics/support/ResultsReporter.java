/**
 * 
 */
package repast.simphony.systemdynamics.support;

/**
 * @author bragen
 *
 */
public interface ResultsReporter {

    public void writeReport(String file, Data data);
    
}
