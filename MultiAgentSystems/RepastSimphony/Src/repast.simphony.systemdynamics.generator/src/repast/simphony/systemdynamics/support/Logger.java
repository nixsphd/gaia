package repast.simphony.systemdynamics.support;

public interface Logger {
    public void log(String s);
}
