/**
 * 
 */
package repast.simphony.systemdynamics.translator;

/**
 * @author bragen
 *
 */
public class ReaderConstants {
    
    public static final String C = "C";
    public static final String CPP = "CPP";
    public static final String JAVA = "Java";
    public static final String JAVASCRIPT = "Javascript";
    public static final String ARRAYS = "Arrays";
    public static final String HASHMAPS = "HashMaps";

    public static String OUTPUT_DIRECTORY;
    public static String GENERATED_CODE_DIRECTORY;
    public static String PACKAGE;
    public static final String SUPPORT = "repast.simphony.systemdynamics.support";

}
