package repast.simphony.systemdynamics.translator;

public interface TranslatorConstants {
  
  String FINAL_TIME = "FINAL TIME";
  String INITIAL_TIME = "INITIAL TIME";
  String SAVEPER = "SAVEPER";
  String TIME_STEP = "TIME STEP";
 
}
