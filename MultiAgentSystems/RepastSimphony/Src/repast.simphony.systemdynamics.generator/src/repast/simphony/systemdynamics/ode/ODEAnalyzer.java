package repast.simphony.systemdynamics.ode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import repast.simphony.systemdynamics.sdmodel.VariableType;
import repast.simphony.systemdynamics.support.MutableInteger;
import repast.simphony.systemdynamics.translator.CodeGenerator;
import repast.simphony.systemdynamics.translator.Equation;
import repast.simphony.systemdynamics.translator.InformationManagers;
import repast.simphony.systemdynamics.translator.NativeDataTypeManager;
import repast.simphony.systemdynamics.translator.Node;

/**
 * This class analyzes the set of equations to collect the appropriate
 * information to generate an Apache Commons ODE solver class.
 * 
 * @author bragen
 * 
 */

public class ODEAnalyzer {

	private Map<String, String> stockToIndex;
	private Map<String, String> indexToStock;
	private List<Equation> auxiliaries;

	private Map<String, Equation> equations;
	List<String> orderedEquations;

	public ODEAnalyzer(Map<String, Equation> equations, List<String> orderedEquations) {
		this.equations = equations;
		stockToIndex = new HashMap<String, String>();
		indexToStock = new HashMap<String, String>();
		auxiliaries = new ArrayList<Equation>();
		this.orderedEquations = orderedEquations;
	}

	public void analyze() {

		analyzeStocks();
		analyzeAuxiliaries();

//		print();

		// experimentWithStocks();
	}

	public List<Equation> getEquationIterator() {
		List<Equation> eqns = new ArrayList<Equation>();
		for (String lhs : orderedEquations) {
			eqns.add(equations.get(lhs));
		}
		return eqns;
	}

	private void print() {
		for (String s : stockToIndex.keySet()) {
			System.out.println("Stock: " + s);
		}
		for (Equation eqn : auxiliaries) {
			System.out.println("aux: " + eqn.getLhs());
		}
	}

	private void analyzeStocks() {
		MutableInteger index = new MutableInteger(0);
		for (Equation eqn : equations.values()) {
			if (!eqn.isStock())
				continue;
			String var = eqn.getLhs();
			if (!stockToIndex.containsKey(var)) {
				stockToIndex.put(var, index.toString());
				indexToStock.put(index.toString(), var);
				index.add(1);
			}
		}
	}

	public int getNumberODE() {
		return stockToIndex.size();
	}

	public String getIndexFor(String stockVar) {
		NativeDataTypeManager ndtm = InformationManagers.getInstance().getNativeDataTypeManager();
		String original = ndtm.getOriginalName(stockVar);
		return stockToIndex.get(original);
	}

	public String getStockFor(String index) {
		return indexToStock.get(index);
	}

	private void analyzeAuxiliaries() {
		for (Equation eqn : equations.values()) {

//			System.out.println("EQN: " + eqn.getLhs() + " type: "
//					+ eqn.getVariableType().toString());
			if (!eqn.getVariableType().equals(VariableType.AUXILIARY)
					&& !eqn.getVariableType().equals(VariableType.CONSTANT)
					&& !eqn.getVariableType().equals(VariableType.RATE))
				continue;

			if (eqn.isAutoGenerated())
				continue;

			auxiliaries.add(eqn);
		}

	}

	public boolean isStock(String var) {
		NativeDataTypeManager ndtm = InformationManagers.getInstance().getNativeDataTypeManager();
		String original = ndtm.getOriginalName(var);
		return stockToIndex.containsKey(original);
	}

	public boolean isAuxiliary(String var) {
		return auxiliaries.contains(var);
	}

	public List<Equation> getAuxiliariesForConstructor() {
		List<Equation> al = new ArrayList<Equation>();
		for (Equation eqn : auxiliaries) {
			if (eqn.isOneTime())
				al.add(eqn);

		}

		return al;
	}

	public List<Equation> getAuxiliariesForMethodBody() {
		List<Equation> al = new ArrayList<Equation>();
		for (Equation eqn : auxiliaries) {
			if (!eqn.isOneTime())
				al.add(eqn);

		}

		return al;
	}

	public List<Equation> getAuxiliaries() {
		return auxiliaries;
	}

	public Map<String, Equation> getEquations() {
		return equations;
	}
	
	public Equation getEquationForLHS(String s) {
		return equations.get(s);
	}
}
