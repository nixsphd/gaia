/**
 * 
 */
package repast.simphony.systemdynamics.support;

/**
 * Use this class to perform task that need to take place at the beginning of each timestep.
 * 
 * @author bragen
 *
 */
public class Synchronizer {

    public static void synchronize(double time, double timeStep) {
	
    }
}
