/**
 */
package repast.simphony.systemdynamics.sdmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cloud</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see repast.simphony.systemdynamics.sdmodel.SDModelPackage#getCloud()
 * @model
 * @generated
 */
public interface Cloud extends Stock {
} // Cloud
