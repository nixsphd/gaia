/**
 */
package repast.simphony.systemdynamics.sdmodel.provider;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.edit.provider.ChangeNotifier;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.IChangeNotifier;
import org.eclipse.emf.edit.provider.IDisposable;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.INotifyChangedListener;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import repast.simphony.systemdynamics.sdmodel.util.SDModelAdapterFactory;

/**
 * This is the factory that is used to provide the interfaces needed to support Viewers.
 * The adapters generated by this factory convert EMF adapter notifications into calls to {@link #fireNotifyChanged fireNotifyChanged}.
 * The adapters also support Eclipse property sheets.
 * Note that most of the adapters are shared among multiple instances.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SDModelItemProviderAdapterFactory extends SDModelAdapterFactory implements ComposeableAdapterFactory, IChangeNotifier, IDisposable {
  /**
   * This keeps track of the root adapter factory that delegates to this adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ComposedAdapterFactory parentAdapterFactory;

  /**
   * This is used to implement {@link org.eclipse.emf.edit.provider.IChangeNotifier}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected IChangeNotifier changeNotifier = new ChangeNotifier();

  /**
   * This keeps track of all the supported types checked by {@link #isFactoryForType isFactoryForType}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Collection<Object> supportedTypes = new ArrayList<Object>();

  /**
   * This constructs an instance.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SDModelItemProviderAdapterFactory() {
    supportedTypes.add(IEditingDomainItemProvider.class);
    supportedTypes.add(IStructuredItemContentProvider.class);
    supportedTypes.add(ITreeItemContentProvider.class);
    supportedTypes.add(IItemLabelProvider.class);
    supportedTypes.add(IItemPropertySource.class);
  }

  /**
   * This keeps track of the one adapter used for all {@link repast.simphony.systemdynamics.sdmodel.SystemModel} instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SystemModelItemProvider systemModelItemProvider;

  /**
   * This creates an adapter for a {@link repast.simphony.systemdynamics.sdmodel.SystemModel}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Adapter createSystemModelAdapter() {
    if (systemModelItemProvider == null) {
      systemModelItemProvider = new SystemModelItemProvider(this);
    }

    return systemModelItemProvider;
  }

  /**
   * This keeps track of the one adapter used for all {@link repast.simphony.systemdynamics.sdmodel.InfluenceLink} instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InfluenceLinkItemProvider influenceLinkItemProvider;

  /**
   * This creates an adapter for a {@link repast.simphony.systemdynamics.sdmodel.InfluenceLink}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Adapter createInfluenceLinkAdapter() {
    if (influenceLinkItemProvider == null) {
      influenceLinkItemProvider = new InfluenceLinkItemProvider(this);
    }

    return influenceLinkItemProvider;
  }

  /**
   * This keeps track of the one adapter used for all {@link repast.simphony.systemdynamics.sdmodel.Cloud} instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CloudItemProvider cloudItemProvider;

  /**
   * This creates an adapter for a {@link repast.simphony.systemdynamics.sdmodel.Cloud}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Adapter createCloudAdapter() {
    if (cloudItemProvider == null) {
      cloudItemProvider = new CloudItemProvider(this);
    }

    return cloudItemProvider;
  }

  /**
   * This keeps track of the one adapter used for all {@link repast.simphony.systemdynamics.sdmodel.Stock} instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected StockItemProvider stockItemProvider;

  /**
   * This creates an adapter for a {@link repast.simphony.systemdynamics.sdmodel.Stock}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Adapter createStockAdapter() {
    if (stockItemProvider == null) {
      stockItemProvider = new StockItemProvider(this);
    }

    return stockItemProvider;
  }

  /**
   * This keeps track of the one adapter used for all {@link repast.simphony.systemdynamics.sdmodel.Rate} instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RateItemProvider rateItemProvider;

  /**
   * This creates an adapter for a {@link repast.simphony.systemdynamics.sdmodel.Rate}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Adapter createRateAdapter() {
    if (rateItemProvider == null) {
      rateItemProvider = new RateItemProvider(this);
    }

    return rateItemProvider;
  }

  /**
   * This keeps track of the one adapter used for all {@link repast.simphony.systemdynamics.sdmodel.Variable} instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VariableItemProvider variableItemProvider;

  /**
   * This creates an adapter for a {@link repast.simphony.systemdynamics.sdmodel.Variable}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Adapter createVariableAdapter() {
    if (variableItemProvider == null) {
      variableItemProvider = new VariableItemProvider(this);
    }

    return variableItemProvider;
  }

  /**
   * This keeps track of the one adapter used for all {@link repast.simphony.systemdynamics.sdmodel.Subscript} instances.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SubscriptItemProvider subscriptItemProvider;

  /**
   * This creates an adapter for a {@link repast.simphony.systemdynamics.sdmodel.Subscript}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Adapter createSubscriptAdapter() {
    if (subscriptItemProvider == null) {
      subscriptItemProvider = new SubscriptItemProvider(this);
    }

    return subscriptItemProvider;
  }

  /**
   * This returns the root adapter factory that contains this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComposeableAdapterFactory getRootAdapterFactory() {
    return parentAdapterFactory == null ? this : parentAdapterFactory.getRootAdapterFactory();
  }

  /**
   * This sets the composed adapter factory that contains this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setParentAdapterFactory(ComposedAdapterFactory parentAdapterFactory) {
    this.parentAdapterFactory = parentAdapterFactory;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object type) {
    return supportedTypes.contains(type) || super.isFactoryForType(type);
  }

  /**
   * This implementation substitutes the factory itself as the key for the adapter.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Adapter adapt(Notifier notifier, Object type) {
    return super.adapt(notifier, this);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object adapt(Object object, Object type) {
    if (isFactoryForType(type)) {
      Object adapter = super.adapt(object, type);
      if (!(type instanceof Class<?>) || (((Class<?>)type).isInstance(adapter))) {
        return adapter;
      }
    }

    return null;
  }

  /**
   * This adds a listener.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void addListener(INotifyChangedListener notifyChangedListener) {
    changeNotifier.addListener(notifyChangedListener);
  }

  /**
   * This removes a listener.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void removeListener(INotifyChangedListener notifyChangedListener) {
    changeNotifier.removeListener(notifyChangedListener);
  }

  /**
   * This delegates to {@link #changeNotifier} and to {@link #parentAdapterFactory}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void fireNotifyChanged(Notification notification) {
    changeNotifier.fireNotifyChanged(notification);

    if (parentAdapterFactory != null) {
      parentAdapterFactory.fireNotifyChanged(notification);
    }
  }

  /**
   * This disposes all of the item providers created by this factory. 
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void dispose() {
    if (systemModelItemProvider != null) systemModelItemProvider.dispose();
    if (influenceLinkItemProvider != null) influenceLinkItemProvider.dispose();
    if (cloudItemProvider != null) cloudItemProvider.dispose();
    if (stockItemProvider != null) stockItemProvider.dispose();
    if (rateItemProvider != null) rateItemProvider.dispose();
    if (variableItemProvider != null) variableItemProvider.dispose();
    if (subscriptItemProvider != null) subscriptItemProvider.dispose();
  }

}
