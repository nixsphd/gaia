package repast.simphony.systemdynamics.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.DiagramsPreferencePage;

import repast.simphony.systemdynamics.diagram.part.SystemdynamicsDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramGeneralPreferencePage extends DiagramsPreferencePage {

  /**
   * @generated
   */
  public DiagramGeneralPreferencePage() {
    setPreferenceStore(SystemdynamicsDiagramEditorPlugin.getInstance().getPreferenceStore());
  }
}
