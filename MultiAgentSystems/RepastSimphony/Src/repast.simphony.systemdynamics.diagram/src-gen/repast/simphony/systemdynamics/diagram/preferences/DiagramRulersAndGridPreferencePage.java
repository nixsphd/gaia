package repast.simphony.systemdynamics.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

import repast.simphony.systemdynamics.diagram.part.SystemdynamicsDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

  /**
   * @generated
   */
  public DiagramRulersAndGridPreferencePage() {
    setPreferenceStore(SystemdynamicsDiagramEditorPlugin.getInstance().getPreferenceStore());
  }
}
