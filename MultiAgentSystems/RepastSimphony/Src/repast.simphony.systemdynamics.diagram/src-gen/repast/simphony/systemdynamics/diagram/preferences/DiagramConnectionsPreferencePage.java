package repast.simphony.systemdynamics.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.ConnectionsPreferencePage;

import repast.simphony.systemdynamics.diagram.part.SystemdynamicsDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramConnectionsPreferencePage extends ConnectionsPreferencePage {

  /**
   * @generated
   */
  public DiagramConnectionsPreferencePage() {
    setPreferenceStore(SystemdynamicsDiagramEditorPlugin.getInstance().getPreferenceStore());
  }
}
