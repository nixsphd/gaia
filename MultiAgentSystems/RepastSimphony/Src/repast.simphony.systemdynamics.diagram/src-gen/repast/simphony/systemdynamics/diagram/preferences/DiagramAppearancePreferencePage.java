package repast.simphony.systemdynamics.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.AppearancePreferencePage;

import repast.simphony.systemdynamics.diagram.part.SystemdynamicsDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramAppearancePreferencePage extends AppearancePreferencePage {

  /**
   * @generated
   */
  public DiagramAppearancePreferencePage() {
    setPreferenceStore(SystemdynamicsDiagramEditorPlugin.getInstance().getPreferenceStore());
  }
}
