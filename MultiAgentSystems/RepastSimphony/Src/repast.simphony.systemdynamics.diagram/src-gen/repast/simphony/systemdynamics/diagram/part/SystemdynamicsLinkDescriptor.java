package repast.simphony.systemdynamics.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;

/**
 * @generated
 */
public class SystemdynamicsLinkDescriptor extends UpdaterLinkDescriptor {
  /**
   * @generated
   */
  public SystemdynamicsLinkDescriptor(EObject source, EObject destination,
      IElementType elementType, int linkVID) {
    super(source, destination, elementType, linkVID);
  }

  /**
   * @generated
   */
  public SystemdynamicsLinkDescriptor(EObject source, EObject destination, EObject linkElement,
      IElementType elementType, int linkVID) {
    super(source, destination, linkElement, elementType, linkVID);
  }

}
