package repast.simphony.systemdynamics.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

  /**
   * @generated
   */
  static {
    NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
  }

  /**
   * @generated
   */
  private Messages() {
  }

  /**
   * @generated
   */
  public static String SystemdynamicsCreationWizardTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsCreationWizard_DiagramModelFilePageTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsCreationWizard_DiagramModelFilePageDescription;

  /**
   * @generated
   */
  public static String SystemdynamicsCreationWizardOpenEditorError;

  /**
   * @generated
   */
  public static String SystemdynamicsCreationWizardCreationError;

  /**
   * @generated
   */
  public static String SystemdynamicsCreationWizardPageExtensionError;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditorUtil_CreateDiagramProgressTask;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditorUtil_CreateDiagramCommandLabel;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_isModifiable;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_handleElementContentChanged;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_IncorrectInputError;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_NoDiagramInResourceError;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_DiagramLoadingError;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_UnsynchronizedFileSaveError;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_SaveDiagramTask;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_SaveNextResourceTask;

  /**
   * @generated
   */
  public static String SystemdynamicsDocumentProvider_SaveAsOperation;

  /**
   * @generated
   */
  public static String InitDiagramFile_ResourceErrorDialogTitle;

  /**
   * @generated
   */
  public static String InitDiagramFile_ResourceErrorDialogMessage;

  /**
   * @generated
   */
  public static String InitDiagramFile_WizardTitle;

  /**
   * @generated
   */
  public static String InitDiagramFile_OpenModelFileDialogTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_CreationPageName;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_CreationPageTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_CreationPageDescription;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_RootSelectionPageName;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_RootSelectionPageTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_RootSelectionPageDescription;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_RootSelectionPageSelectionTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_InitDiagramCommand;

  /**
   * @generated
   */
  public static String SystemdynamicsNewDiagramFileWizard_IncorrectRootError;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditor_SavingDeletedFile;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditor_SaveAsErrorTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditor_SaveAsErrorMessage;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditor_SaveErrorTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsDiagramEditor_SaveErrorMessage;

  /**
   * @generated
   */
  public static String SystemdynamicsElementChooserDialog_SelectModelElementTitle;

  /**
   * @generated
   */
  public static String ModelElementSelectionPageMessage;

  /**
   * @generated
   */
  public static String ValidateActionMessage;

  /**
   * @generated
   */
  public static String Sdmodel1Group_title;

  /**
   * @generated
   */
  public static String InfluenceArrow1CreationTool_title;

  /**
   * @generated
   */
  public static String InfluenceArrow1CreationTool_desc;

  /**
   * @generated
   */
  public static String Rate2CreationTool_title;

  /**
   * @generated
   */
  public static String Rate2CreationTool_desc;

  /**
   * @generated
   */
  public static String Stock3CreationTool_title;

  /**
   * @generated
   */
  public static String Stock3CreationTool_desc;

  /**
   * @generated
   */
  public static String Constant4CreationTool_title;

  /**
   * @generated
   */
  public static String Constant4CreationTool_desc;

  /**
   * @generated
   */
  public static String Variable5CreationTool_title;

  /**
   * @generated
   */
  public static String Variable5CreationTool_desc;

  /**
   * @generated
   */
  public static String Cloud6CreationTool_title;

  /**
   * @generated
   */
  public static String Cloud6CreationTool_desc;

  /**
   * @generated
   */
  public static String Lookup7CreationTool_title;

  /**
   * @generated
   */
  public static String Lookup7CreationTool_desc;

  /**
   * @generated
   */
  public static String CommandName_OpenDiagram;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Variable_2001_incominglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Variable_2001_outgoinglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Variable_2005_incominglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Variable_2005_outgoinglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_SystemModel_1000_links;

  /**
   * @generated
   */
  public static String NavigatorGroupName_InfluenceLink_4004_target;

  /**
   * @generated
   */
  public static String NavigatorGroupName_InfluenceLink_4004_source;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Rate_4003_target;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Rate_4003_source;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Rate_4003_incominglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Rate_4003_outgoinglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Cloud_2002_incominglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Cloud_2002_outgoinglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Stock_2003_incominglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Stock_2003_outgoinglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Variable_2004_incominglinks;

  /**
   * @generated
   */
  public static String NavigatorGroupName_Variable_2004_outgoinglinks;

  /**
   * @generated
   */
  public static String NavigatorActionProvider_OpenDiagramActionName;

  /**
   * @generated
   */
  public static String AbstractParser_UnexpectedValueType;

  /**
   * @generated
   */
  public static String AbstractParser_WrongStringConversion;

  /**
   * @generated
   */
  public static String AbstractParser_UnknownLiteral;

  /**
   * @generated
   */
  public static String MessageFormatParser_InvalidInputError;

  /**
   * @generated
   */
  public static String SystemdynamicsModelingAssistantProviderTitle;

  /**
   * @generated
   */
  public static String SystemdynamicsModelingAssistantProviderMessage;

  //TODO: put accessor fields manually	
}
