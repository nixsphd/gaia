package repast.simphony.systemdynamics.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
 * @generated
 */
public class SystemdynamicsNodeDescriptor extends UpdaterNodeDescriptor {
  /**
   * @generated
   */
  public SystemdynamicsNodeDescriptor(EObject modelElement, int visualID) {
    super(modelElement, visualID);
  }

}
