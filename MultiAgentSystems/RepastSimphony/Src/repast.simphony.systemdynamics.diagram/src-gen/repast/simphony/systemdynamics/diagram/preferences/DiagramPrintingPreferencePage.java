package repast.simphony.systemdynamics.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;

import repast.simphony.systemdynamics.diagram.part.SystemdynamicsDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

  /**
   * @generated
   */
  public DiagramPrintingPreferencePage() {
    setPreferenceStore(SystemdynamicsDiagramEditorPlugin.getInstance().getPreferenceStore());
  }
}
