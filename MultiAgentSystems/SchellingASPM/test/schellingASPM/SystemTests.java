package schellingASPM;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.engine.schedule.Schedule;
import repast.simphony.random.RandomHelper;

public class SystemTests {

	private Schedule schedule;
	private Context<Object> context;
	private ShapeContextBuilder shapeContectBuilder;

	@Before
	public void setUp() throws Exception {
		schedule = new Schedule(); 
//		RunEnvironment.init(schedule, null, null, true); 
		
		context = new DefaultContext<Object>();	
//		RunState.init().setMasterContext(context);
		
		shapeContectBuilder = new ShapeContextBuilder();
		
	}

	
	@Test
	public void testBasic() {

		int randomSeed = 1;
		int gridWidth = 16; 
		int gridHeight = 13;
		double freeSpaceRatio = 0.25;
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.5; 
		char[][] expectedState = {
				{'c','c','c',' ',' ',' ',' ','s','c','c','c','c',' ','s',' ','s'},
				{' ','c','c','c','c','c','s','s',' ','c','c','c','c','s','s',' '},
				{'s','s',' ','c','c','c','s','s','s','s',' ','c',' ',' ',' ',' '},
				{'s','s','s','s',' ',' ','s',' ',' ','s','s','s',' ','s','s',' '},
				{' ',' ','s','s','s','s','s','c','c','c','s','s',' ','s','s',' '},
				{'s','s','s','s',' ','s','c','c','c','c',' ','s',' ',' ','s','s'},
				{'s',' ',' ','c','c','c',' ',' ',' ','c',' ','c','c','c','s','s'},
				{'s',' ','c','c','c','c',' ','c',' ','c','c','c','c','c','c','s'},
				{'s','c','c','c','c','s','s',' ','c',' ','c',' ','s',' ','c',' '},
				{'s',' ','c',' ','s','s','s','s','c','c',' ','s','s','s','c','c'},
				{'s',' ','s',' ','c','s','s','s',' ','c','c','s','s','s',' ','c'},
				{'s','s','s','c','c','c',' ','s','s',' ','c',' ','c',' ','c',' '},
				{'s',' ',' ','c',' ','c','c',' ','s','s','c','c','c','c','c','c'}
			};
		testState(randomSeed, gridWidth, gridHeight, freeSpaceRatio, neighbourhoodSize, desiredNeighbourRatio, expectedState);
		
		
	}

	public void testState(int randomSeed, int gridWidth, int gridHeight, double freeSpaceRatio, int neighbourhoodSize, 
			double desiredNeighbourRatio, char[][] expectedState) {
			
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, gridWidth, gridHeight, freeSpaceRatio, neighbourhoodSize, desiredNeighbourRatio);
		
//		System.out.println("NDB::testState()~initialState="+UnitTests.toString(shapeContectBuilder.getGridState()));
//		System.out.println("NDB::testState()~expectedState="+UnitTests.toString(expectedState));

		Iterable<Shape> shapes = shapeContectBuilder.getShapes();

		for (Shape shape : shapes) {
			schedule.schedule(shape);
		}

		while (!shapeContectBuilder.ending()) {
			schedule.execute();
		}

		char[][] actualState = shapeContectBuilder.getGridState();
//		System.out.println("NDB::testState()~actualState="+UnitTests.toString(actualState));
		equals(expectedState, actualState);	
			
	}
	
	static void equals(char[][] a, char[][] a2) {		
		for (int h = 0; h < a.length; h++) {			
			Assert.assertTrue(Arrays.equals(a[h], a2[h]));
		}
	}
}
