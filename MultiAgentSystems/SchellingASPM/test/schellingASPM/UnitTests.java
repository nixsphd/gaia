package schellingASPM;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.random.RandomHelper;

public class UnitTests {

	private Context<Object> context;
	private ShapeContextBuilder shapeContectBuilder;

	@Before
	public void setUp() throws Exception {
		context = new DefaultContext<Object>();	
		shapeContectBuilder = new ShapeContextBuilder();
	
	}

	@Test
	public void testBuildGrid() {
		int randomSeed = 1;
		int gridWidth = 3; 
		int gridHeight = 6;
		double freeSpaceRatio = 0;
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.5; 
		char[][] expectedState = {
				{'c','c','s'},
				{'s','s','s'},
				{'s','s','c'},
				{'c','c','s'},
				{'c','s','c'},
				{'c','c','c'}
			};
		testBuildGrid(randomSeed, gridWidth, gridHeight, freeSpaceRatio, neighbourhoodSize, desiredNeighbourRatio, expectedState);
		
	}
	
	public void testBuildGrid(int randomSeed, int gridWidth, int gridHeight, double freeSpaceRatio, int neighbourhoodSize, double desiredNeighbourRatio, char[][] expectedState) {
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, gridWidth, gridHeight, freeSpaceRatio, neighbourhoodSize, desiredNeighbourRatio);

		char[][] actualState = shapeContectBuilder.getGridState();
//		System.out.println("NDB::testBuildGrid()~actualState="+toString(actualState));		
		Assert.assertEquals(toString(expectedState), toString(actualState));
	}
	
	@Test
	public void testBasicLikeNeighbourRatio() {
		int randomSeed = 1;
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.5; 
		char[][] initialState={
				{'c','c','c'},
				{'s','c','s'},
				{'s','c','s'},
				{'s','c','s'}
				};		
		double[][] expectedFinalState = {
				{0.666,0.6,0.666},
				{0.2,0.5,0.2},
				{0.4,0.25,0.4},
				{0.333,0.2,0.333}
				};
		testLikeNeighbourRatio(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	public void testLikeNeighbourRatio(int randomSeed, char[][] initialState, int neighbourhoodSize, double desiredNeighbourRatio, double[][] expectedFinalState) {
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, initialState, neighbourhoodSize, desiredNeighbourRatio);
		
//		System.out.println("NDB::testLikeNeighbourRatio()~deesiredInitialState="+toString(initialState));	
//		System.out.println("NDB::testLikeNeighbourRatio()~actualInitialState="+toString(shapeContectBuilder.getGridState()));		
//		System.out.println("NDB::testLikeNeighbourRatio()~expectedFinalState="+toString(expectedFinalState));
		
		double[][] actualFinalState = new double[initialState.length][initialState[0].length];
		for (int h = 0; h < actualFinalState.length; h++) {
			for (int w = 0; w < actualFinalState[h].length; w++) {
				actualFinalState[h][w] = shapeContectBuilder.getShapeAt(w, h).getLikeNeighbourRatio();
			}	
		}
		
//		System.out.println("NDB::testLikeNeighbourRatio()~actualFinalState="+toString(actualFinalState));
		equals(expectedFinalState, actualFinalState);
	}
	
	@Test
	public void testIsHappy() {
		int randomSeed = 1;
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.5; 
		char[][] initialState={
				{'c','c','c'},
				{'s','c','s'},
				{'s','c','s'},
				{'s','c','s'}
				};
		boolean[][] expectedFinalState = {
				{true,true,true},
				{false,true,false},
				{false,false,false},
				{false,false,false}
				};
		testIsHappy(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, expectedFinalState);
		
	}
	
	public void testIsHappy(int randomSeed, char[][] initialState, int neighbourhoodSize, double desiredNeighbourRatio, boolean[][] expectedFinalState) {
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, initialState, neighbourhoodSize, desiredNeighbourRatio);

//		System.out.println("NDB::testIsHappy()~deesiredInitialState="+toString(initialState));	
//		System.out.println("NDB::testIsHappy()~actualInitialState="+toString(shapeContectBuilder.getGridState()));		
//		System.out.println("NDB::testIsHappy()~expectedFinalState="+toString(expectedFinalState));
			
		boolean[][] actualFinalState = new boolean[initialState.length][initialState[0].length];
		for (int h = 0; h < actualFinalState.length; h++) {
			for (int w = 0; w < actualFinalState[h].length; w++) {
				actualFinalState[h][w] = shapeContectBuilder.getShapeAt(w, h).isHappy();
			}	
		}
		
//		System.out.println("NDB::testIsHappy()~actualFinalState="+toString(actualFinalState));
		equals(expectedFinalState, actualFinalState);
	}

	@Test
	public void testfindHappyPlaceTopRight() {
		int randomSeed = 1;
		char[][] initialState={
				{'c',' ','s','s','s','s'},
				{'s','s','c','c','c','s'},
				{'s','s',' ','c',' ','s'},
				{'c','s',' ',' ',' ','s'},
				{' ','s','s',' ',' ',' '},
				{'s','c','s','c',' ','c'},
				{'s',' ','c','s',' ','c'},
				{'c',' ','c','s',' ','c'}
				};
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.5; 
		int[] unhappyShape = {0,0};
		int[] expectedHappyPlace = {2,2};
		testfindHappyPlace(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, unhappyShape, expectedHappyPlace);
		
	}
	
	@Test
	public void testfindHappyPlaceBottomLeft() {
		int randomSeed = 1;
		char[][] initialState={
				{'c',' ','s','s','s','s'},
				{'s','s','c','c','c','s'},
				{'s','s',' ','c',' ','s'},
				{'c','s',' ',' ',' ','s'},
				{' ','s','s',' ',' ',' '},
				{'s','c','s','c',' ','c'},
				{'s',' ','c','s',' ','c'},
				{'c',' ','c','s',' ','c'}
				};
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.5; 
		int[] unhappyShape = {5,7};
		int[] expectedHappyPlace = {4,6};
		testfindHappyPlace(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, unhappyShape, expectedHappyPlace);
		
	}
	
	@Test
	public void testfindHappyPlaceNone() {
		int randomSeed = 1;
		char[][] initialState={
				{'c','s','c','s','c','c'},
				{'s','c','c','s','s','c'},
				{'s','s','s','s','c','c'},
				{'c','c','s','c','c','c'},
				{'c','s','c','s','c','c'},
				{'c','s','s','s','s','c'},
				{'c','c','c','s','c','c'},
				{'s','s','c','s','s','s'}
				};
		int neighbourhoodSize = 1; 
		int[] unhappyShape = {2,4};
		double desiredNeighbourRatio = 0.5; 
		int[] expectedHappyPlace = null;
		testfindHappyPlace(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, unhappyShape, expectedHappyPlace);
		
	}
	
	public void testfindHappyPlace(int randomSeed, char[][] initialState, int neighbourhoodSize, double desiredNeighbourRatio, int[] shape, int[] expectedHappyPlace) {
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, initialState, neighbourhoodSize, desiredNeighbourRatio);
				
//		System.out.println("NDB::testfindHappyPlace()~deesiredInitialState="+toString(initialState));	
//		System.out.println("NDB::testfindHappyPlace()~actualInitialState="+toString(shapeContectBuilder.getGridState()));	
//		if (expectedHappyPlace != null) {	
//			System.out.println("NDB::testfindHappyPlace()~expectedHappyPlace=["+expectedHappyPlace[0]+","+expectedHappyPlace[1]+"]");
//		} else {
//			System.out.println("NDB::testfindHappyPlace()~expectedHappyPlace=None");
//		}
		
		Shape unhappyShape = shapeContectBuilder.getShapeAt(shape[0],shape[1]);
		int[] actualHappyPlace = unhappyShape.findHappyPlace();

//		if (actualHappyPlace != null) {	
//			System.out.println("NDB::testfindHappyPlace()~actualHappyPlace=["+actualHappyPlace[0]+","+actualHappyPlace[1]+"]");
//		} else {
//			System.out.println("NDB::testfindHappyPlace()~actualHappyPlace=None");
//		}
		Assert.assertArrayEquals(expectedHappyPlace, actualHappyPlace);	
	}
	
	@Test
	public void testBeHappy() {
		int randomSeed = 1;
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.5; 
		char[][] initialState = {
				{'c',' ','s','s','s','s'},
				{'s','s','c','c','c','s'},
				{'s','s',' ','c',' ','s'},
				{'c','s',' ',' ',' ','s'},
				{' ','s','s',' ',' ',' '},
				{'s','c','s','c',' ','c'},
				{'s',' ','c','s',' ','c'},
				{'c',' ','c','s',' ','c'}
				};
		int[] unhappyShape = {0,0};
		char[][] expectedState = {
				{' ',' ','s','s','s','s'},
				{'s','s','c','c','c','s'},
				{'s','s','c','c',' ','s'},
				{'c','s',' ',' ',' ','s'},
				{' ','s','s',' ',' ',' '},
				{'s','c','s','c',' ','c'},
				{'s',' ','c','s',' ','c'},
				{'c',' ','c','s',' ','c'}
				};
		testBeHappy(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, unhappyShape, expectedState);
		
	}
	
	@Test (expected=RuntimeException.class)
	public void testBeHappyNone() {
		int randomSeed = 1;
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.5; 
		char[][] initialState = {
				{'c','s','s','s','c','c'},
				{'s','c','s','c','s','s'},
				{'s','s','s','s','s','c'},
				{'s','c','s','s','c','s'},
				{'s','s','c','s','c','c'},
				{'s','c','c','s','s','s'},
				{'s','s','c','s','s','s'},
				{'s','s','c','c','c','s'}
				};
		char[][] expectedState = null;
		testBeHappy(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, new int[]{2,4}, expectedState);
		
	}
	
	public void testBeHappy(int randomSeed, char[][] initialState, int neighbourhoodSize, double desiredNeighbourRatio, int[] shape, char[][] expectedState) {
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, initialState, neighbourhoodSize, desiredNeighbourRatio);
		
//		System.out.println("NDB::testBeHappy()~deesiredInitialState="+toString(initialState));	
//		System.out.println("NDB::testBeHappy()~actualInitialState="+toString(shapeContectBuilder.getGridState()));	
//		System.out.println("NDB::testBeHappy()~expectedState="+toString(expectedState));
		
		Shape unhappyShape = shapeContectBuilder.getShapeAt(shape[0],shape[1]);
		unhappyShape.beHappy();
		char[][] actualState = shapeContectBuilder.getGridState();

//		System.out.println("NDB::testBeHappy()~actualState="+toString(actualState));
		Assert.assertArrayEquals(expectedState, actualState);
	
		
	}
	
	@Test
	public void testEndingAllHappy() {
		int randomSeed = 1;
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.1; 
		char[][] initialState={
				{'c','c','c'},
				{'s','c','s'},
				{'s','c','s'}
				};
		boolean ending = true;
		testEnding(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, ending);
		
	}

	@Test
	public void testEndingNeverHappy() {
		int randomSeed = 1;
		int neighbourhoodSize = 1; 
		double desiredNeighbourRatio = 0.9; 
		char[][] initialState={
				{'c','c','c'},
				{'s','c','s'},
				{'s','c','s'}
				};
		boolean expectedEnding = false;
		testEnding(randomSeed, initialState, neighbourhoodSize, desiredNeighbourRatio, expectedEnding);
		
	}

	public void testEnding(int randomSeed, char[][] initialState, int neighbourhoodSize, double desiredNeighbourRatio, boolean expectedEnding) {
		RandomHelper.setSeed(randomSeed);
		shapeContectBuilder.build(context, initialState, neighbourhoodSize, desiredNeighbourRatio);
		
//		System.out.println("NDB::testEnding()~deesiredInitialState="+toString(initialState));	
//		System.out.println("NDB::testEnding()~actualInitialState="+toString(shapeContectBuilder.getGridState()));	
//		System.out.println("NDB::testEnding()~expectedEnding="+expectedEnding);
		
		for (Shape shape : shapeContectBuilder.getShapes()) {
			shape.isHappy();
		}
		
		boolean actualEnding = shapeContectBuilder.ending();		
//		System.out.println("NDB::testEnding()~actualEnding="+actualEnding);
		Assert.assertEquals(expectedEnding, actualEnding);
		
	}
	
	static String toString(boolean[][] state) {
		StringBuilder string = new StringBuilder("{\n");
		for (int h = 0; h < state.length; h++) {
			string.append("{");
			for (int w = 0; w < state[h].length; w++) {
				string.append(Boolean.toString(state[h][w])+",");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.lastIndexOf(","), string.length(),"\n}");
		return string.toString();
	}

	static String toString(double[][] state) {
		StringBuilder string = new StringBuilder("{\n");
		for (int h = 0; h < state.length; h++) {
			string.append("{");
			for (int w = 0; w < state[h].length; w++) {
				string.append(Double.toString(state[h][w])+",");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.lastIndexOf(","), string.length(),"\n}");
		return string.toString();
	}	
	
	static String toString(char[][] state) {
		StringBuilder string = new StringBuilder("{\n");
		for (int h = 0; h < state.length; h++) {
			string.append("{");
			for (int w = 0; w < state[h].length; w++) {
				string.append("'"+state[h][w]+"',");
			}
			string.replace(string.length()-1, string.length(),"},\n");
			
		}
		string.replace(string.lastIndexOf(","), string.length(),"\n}");
		return string.toString();
	}	
	
	static String toString(Shape[][] state) {
		StringBuilder string = new StringBuilder("{");
		for (int h = 0; h < state.length; h++) {
			for (int w = 0; w < state[h].length; w++) {
				string.append(state[h][w]+",");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.length()-1, string.length(),"}");
		return string.toString();
	}
	
	static void equals(boolean[][] a, boolean[][] a2) {		
		for (int h = 0; h < a.length; h++) {			
			Assert.assertTrue(Arrays.equals(a[h], a2[h]));
		}
	}
	
	static void equals(double[][] a, double[][] a2) {		
		for (int h = 0; h < a.length; h++) {			
			Assert.assertArrayEquals(a[h], a2[h], 0.1);
		}
	}
}
