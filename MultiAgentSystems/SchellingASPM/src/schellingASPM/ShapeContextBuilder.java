package schellingASPM;

import java.util.ArrayList;
import java.util.Iterator;

import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.space.grid.StrictBorders;

public class ShapeContextBuilder implements ContextBuilder<Object> {
			
	private Context<Object> context;
	private Grid<Object> grid;
	
	private int gridWidth;
	private int gridHeight;
	private int neighbourhoodSize;
	private double desiredNeighbourRatio;
	
	ArrayList<Shape> shapes;
	
	public String toString() {
		return "ShapeContextBuilder";
	}

	public Context<Object> build(Context<Object> context) {
		RunEnvironment runEnvironment = RunEnvironment.getInstance();
		Parameters parms = runEnvironment.getParameters();
		// This is in case the program gets stuck in an infinite loop which can happen with big neighbourhoods. 
		runEnvironment.endAt(500);
		return build(context, 
				(Integer)parms.getValue("gridWidth"), 
				(Integer)parms.getValue("gridHeight"), 
				(Double)parms.getValue("freeSpaceRatio"), 
				(Integer)parms.getValue("neighbourhoodSize"), 
				(Double)parms.getValue("desiredNeighbourRatio"));
	}
	
	public Context<Object> build(Context<Object> context, int gridWidth, int gridHeight, double freeSpaceRatio, int neighbourhoodSize, double desiredNeighbourRatio) {
		this.context = context;
		this.gridWidth = gridWidth;
		this.gridHeight = gridHeight;
		this.neighbourhoodSize = neighbourhoodSize;
		this.desiredNeighbourRatio = desiredNeighbourRatio;
		// Create the grid for the Shapes
		this.grid = GridFactoryFinder.createGridFactory(null).createGrid("Grid", context, 
				new GridBuilderParameters<Object>(new StrictBorders(), 
						new RandomGridAdder<Object>(), true, gridWidth, gridHeight));

		initShapes(freeSpaceRatio);
		// Add the builder to the context so the termination methods are scheduled.
		if (!context.add(this)) {
			throw new RuntimeException("Probelm adding "+this+"");
		} else {
			grid.moveTo(this, 0, 0);
		}
		return context;
	}
	
	public Context<Object> build(Context<Object> context, char[][] shapes, int neighbourhoodSize, double desiredNeighbourRatio) {
		this.context = context;
		this.gridWidth = shapes[0].length;
		this.gridHeight = shapes.length;
		this.neighbourhoodSize = neighbourhoodSize;
		this.desiredNeighbourRatio = desiredNeighbourRatio;
		// Create the grid for the Shapes
		this.grid = GridFactoryFinder.createGridFactory(null).createGrid("Grid", context, 
				new GridBuilderParameters<Object>(new StrictBorders(), 
						new RandomGridAdder<Object>(), true, gridWidth, gridHeight));

		initShapes(shapes);
		// Add the builder to the context so the termination methods are scheduled.
		if (!context.add(this)) {
			throw new RuntimeException("Probelm adding "+this+"");
		} else {
			grid.moveTo(this, 0, 0);
		}
		return context;
	}

	public Context<Object> getContext() {
		return context;
	}

	public void initShapes(double freeSpaceRatio) {
		shapes = new ArrayList<Shape>(); 
		// Todo: Add in initial shape ratio.
		// Create and place a new Shape in each grid location.
		for (int w = 0; w < gridWidth; w++){
			for (int h = 0; h < gridHeight; h++){
				// Assign the free space
				if (RandomHelper.nextDoubleFromTo(0, 1) >= freeSpaceRatio) {
					if (RandomHelper.nextIntFromTo(0, 1) == 0) {
						addStar(w,h);
					} else {
						addCircle(w,h);
					}
				}
			}
		}
	}
	
	public void initShapes(char[][] shapes) {
		this.shapes = new ArrayList<Shape>(); 
		// Todo: Add in initial shape ratio.
		// Create and place a new Shape in each grid location.
		for (int w = 0; w < gridWidth; w++){
			for (int h = 0; h < gridHeight; h++) {
				if (shapes[h][w] == 's') {
					addStar(w,h);
				} else if (shapes[h][w] == 'c') {
					addCircle(w,h);
				}
			}
		}
	}

	public Shape[][] getGrid() {
		Shape[][] gridOfShapes = new Shape[gridHeight][gridWidth];
		for (int h = 0; h < grid.getDimensions().getHeight(); h++) {
			for (int w = 0; w < grid.getDimensions().getWidth(); w++) {
				gridOfShapes[h][w] = getShapeAt(w, h);
			}
		}
		return gridOfShapes;
	}

	public int getGridWidth() {
		return gridWidth;
	}

	public int getGridHeight() {
		return gridHeight;
	}
	
	public Iterable<Shape> getShapes() {
		return shapes;
	}
	
	public int getNumberOfShapes() {
		return shapes.size();
	}

	public Shape getShapeAt(int w, int h) {
		Iterator<Object> shapes = grid.getObjectsAt(w, h).iterator();
		if (shapes.hasNext()) {
			Object shape = shapes.next();
			if (shape instanceof Shape) {
				return (Shape)shape;
			}
		}
		return null;
		
	}

	public void clearShapes() {
		context.clear();
		shapes = new ArrayList<Shape>(); 
//		state = null;
	}
	
	public char[][] getGridState() {
		char[][] gridState = new char[gridHeight][gridWidth];
		for (int h = 0; h < grid.getDimensions().getHeight(); h++) {
			for (int w = 0; w < grid.getDimensions().getWidth(); w++) {
				gridState[h][w] = ' ';
				Shape shape = getShapeAt(w, h);
				if (shape == null) {
					gridState[h][w] = ' ';					
				} else if (shape instanceof Circle) {
					gridState[h][w] = 'c';
				} else if (shape instanceof Star) {
					gridState[h][w] = 's';
				} else {
					gridState[h][w] = '?';
				}
					
			}
		}
		return gridState;
	}

	@ScheduledMethod(start=1, interval=1, priority=ScheduleParameters.LAST_PRIORITY)
	public boolean ending() {
//		System.out.println("NDB::ShapeContextBuilder.happyEnding()");
		boolean allHappy = true;
		Iterator<Shape> shapeIterator = shapes.iterator();
		while (shapeIterator.hasNext() && allHappy) {
			if (!shapeIterator.next().isHappy) {
//				System.out.println("NDB::ShapeContextBuilder.happyOut()->No, I'm not says "+shape);
				allHappy = false;
				
			}
		}
		if (allHappy) {
			if (RunEnvironment.getInstance() != null) {
				System.out.println("Happiness is everywhere :)");
				RunEnvironment.getInstance().endRun();
			}
			return true;
//		This festure is diaabled as when I went looking for a test case for it
//		I couldn't find on so it not wothj the computational effort to check for it.	
//		} else {	
//			if (state == null) {
//				state = getGridState();
//			} else {
//				boolean noChanges = true;
//				char[][] newState = getGridState();
//				int h = 0; 
//				while (noChanges && h < grid.getDimensions().getHeight()) {
//					if (!Arrays.equals(state[h], newState[h])) {
//						noChanges = false;						
//					} 
//					h++;
//				}
//				if (noChanges) {
//					if (RunEnvironment.getInstance() != null) {
//						RunEnvironment.getInstance().endRun();
//					} else {
//						throw new RuntimeException("Happiness will always alude this grid setup :(");
//					}
//					return true;
//				} else {
//					state = newState;
//				}
//			}
		}
		return false;
	}

	private void addStar(int width, int height) {		
		Star star = new Star(this, width, height, neighbourhoodSize, desiredNeighbourRatio);
		if (!context.add(star)) {
			throw new RuntimeException("Probelm adding "+star+"");
		}
		grid.moveTo(star, width, height);
		shapes.add(star);
//		System.out.println("NDB::addStar(w="+width+", h="+height+")~"+star);
	}
	
	private void addCircle(int width, int height) {
		Circle circle = new Circle(this, width, height, neighbourhoodSize, desiredNeighbourRatio);
		if (!context.add(circle)) {
			throw new RuntimeException("Probelm adding "+circle+"");
		}
		
		grid.moveTo(circle, width, height);
		shapes.add(circle);
//		System.out.println("NDB::addCircle(w="+width+", h="+height+")~"+circle);
	}
	
}