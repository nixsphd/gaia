package schellingASPM;

public class Star extends Shape {
	
	public Star(ShapeContextBuilder contextBuilder, int width, int height,
			int neighbourhoodSize, double desiredNeighbourRatio) {
		super(contextBuilder, width, height, neighbourhoodSize, desiredNeighbourRatio);
	}
	
	public String toString() {
		return "Star("+width+","+height+")";
	}
}
