package schellingASPM;

public class Circle extends Shape {
	
	public Circle(ShapeContextBuilder contextBuilder, int width, int height, 
			int neighbourhoodSize, double desiredNeighbourRatio) {
		super(contextBuilder, width, height, neighbourhoodSize, desiredNeighbourRatio);
	}
	
	public String toString() {
		return "Circle("+width+","+height+")";
	}
}
