package schellingASPM;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.grid.Grid;

abstract public class Shape {
	
	protected ShapeContextBuilder contextBuilder;
	protected Context<Object> context;
	protected Grid<Object> grid;
	protected String id;
	protected double desiredNeighbourRatio;
	protected int neighbourhoodSize;
	protected int width;
	protected int height;
	protected boolean isHappy;
	
	@SuppressWarnings("unchecked")
	protected Shape(ShapeContextBuilder contextBuilder, int width, int height,
			int neighbourhoodSize,  double desiredNeighbourRatio) {
		this.contextBuilder = contextBuilder;
		this.context = contextBuilder.getContext();
		this.grid = (Grid<Object>)context.getProjection("Grid");
		this.width = width;
		this.height = height;
		this.id = this.toString();
		this.neighbourhoodSize = neighbourhoodSize;
		this.desiredNeighbourRatio = desiredNeighbourRatio;
	}
	
	@ScheduledMethod(start=1, interval=1, priority=ScheduleParameters.FIRST_PRIORITY)
	public boolean isHappy() {
//		System.out.println("NDB::"+this+".isHappy()");
		isHappy = (getLikeNeighbourRatio() >= desiredNeighbourRatio);
//		System.out.println("NDB::"+this+".isHappy()->"+isHappy);
		return isHappy;
		
	}

	@ScheduledMethod(start=1, interval=1)
//	@ScheduledMethod(start=1, interval=1, priority=ScheduleParameters.LAST_PRIORITY+1)
	public void beHappy() {
//		System.out.println("NDB::"+this+".beHappy()");
		if (!isHappy){ //isHappy()) {
//			System.out.println("NDB::"+this+".beHappy() unhappy one...");
			int[] happyPlace = findHappyPlace();
			if (happyPlace != null) {
				moveTo(happyPlace);
			} else {
				if (RunEnvironment.getInstance() != null) {
					System.out.println("Happiness will always alude this shape "+this+" :(");
					RunEnvironment.getInstance().endRun();
				} else {
					throw new RuntimeException("Happiness will always alude this shape "+this+" :(");
				}
			}
		}
	}
	
	protected double getLikeNeighbourRatio() {
		return getAbsoluteNeighboursRatio(width, height);
	}
	
	protected double getAbsoluteNeighboursRatio(int width, int height) {
//		System.out.println("NDB::"+this+".getNeighboursRatio("+width+","+height+")");
		double sameCount = 0;
		double numberOfNeighbours = 0;
		for (int w = width-neighbourhoodSize; w <= width+neighbourhoodSize; w++) {
			for (int h = height-neighbourhoodSize; h <= height+neighbourhoodSize; h++) {
				//if (!(w == width && h == height) 
				if (!(w == this.width && h == this.height)
					&& (w >= 0)
					&& (w < contextBuilder.getGridWidth()) 
					&&  (h >= 0) 
					&& (h < contextBuilder.getGridHeight())) {
					Shape shape = contextBuilder.getShapeAt(w,h);
					if (shape != null) {
						if (getClass().equals(shape.getClass())) {
							sameCount++;
						}
						numberOfNeighbours++;
					}
				}
			}
		}
		double neighboursRatio = sameCount/numberOfNeighbours;
//		System.out.println("NDB::"+this+".getNeighboursRatio("+width+","+height+")->"+neighboursRatio);
		return neighboursRatio;
	}
	
//	protected double getRelativeNeighboursRatio(int width, int height) {
////		System.out.println("NDB::"+this+".getNeighboursRatio("+width+","+height+")");
//		double differentCount = 0;
//		double sameCount = 0;
//		for (int w = width-neighbourhoodSize; w <= width+neighbourhoodSize; w++) {
//			for (int h = height-neighbourhoodSize; h <= height+neighbourhoodSize; h++) {
//				if (!(w == width && h == height)
//					&& (w >= 0)
//					&& (w < contextBuilder.getGridWidth()) 
//					&&  (h >= 0) 
//					&& (h < contextBuilder.getGridHeight())) {
//					Shape shape = grid.getObjectAt(w,h);
//					if (shape != null) {
//						if (getClass().equals(shape.getClass())) {
//							sameCount++;
//						} else {
//							differentCount++;
//						}
//					}
//				}
//			}
//		}
//		double neighboursRatio;
//		if (differentCount == 0) {
//			neighboursRatio = Double.MAX_VALUE;
//		} else {
//			neighboursRatio = sameCount/differentCount;
//		}
////		System.out.println("NDB::"+this+".getNeighboursRatio("+width+","+height+")->"+neighboursRatio);
//		return neighboursRatio;
//	}
	protected int[] findHappyPlace() {
//		System.out.println("NDB::"+this+".findHappyPlace()");
		// find nearest happy place
		int maxDisplacement = Math.max(contextBuilder.getGridHeight(), contextBuilder.getGridWidth());
//		System.out.println("NDB::"+this+".findHappyPlace()~maxDisplacement="+maxDisplacement);
		for (int displacement = 1; displacement < maxDisplacement; displacement++) {
//			System.out.println("NDB::"+this+".findHappyPlace()~displacement="+displacement);
//			char[][] checkMask = new char[contextBuilder.getGridHeight()][contextBuilder.getGridWidth()];
			for (int w = width-displacement; w <= width+displacement; w++) {
				for (int h = height-displacement; h <= height+displacement; h++) {
					if ((w >= 0) && (w < contextBuilder.getGridWidth()) && 
						(h >= 0) && (h < contextBuilder.getGridHeight()) &&
						(!((w >= width-(displacement-1)) && (w <= width+(displacement-1)) &&
						   (h >= height-(displacement-1)) && (h <= height+(displacement-1)))) &&
						(grid.getObjectAt(w,h) == null))
					{
						// possible place
//						System.out.println("NDB::"+this+".findHappyPlace()~Evaluating "+w+", "+h+", Neighbours Ratio is "+getNeighboursRatio(w,h));
						if (getAbsoluteNeighboursRatio(w,h) >= desiredNeighbourRatio) {
//							System.out.println("NDB::"+this+".findHappyPlace()~Found one at "+w+", "+h);
							return new int[]{w,h};
						}
//						checkMask[h][w] = 'X';
					} 
				}
//				System.out.println("NDB::"+this+".findHappyPlace()~checkMask="+UnitTests.toString(checkMask));	
		
			}
		}
		// No happy place
//		System.out.println("NDB::"+this+".findHappyPlace()->None ;(");
		return null;
	}

	protected void moveTo(int[] newLocation) {
//		System.out.println("NDB::"+this+".moveTo("+newLocation[0]+","+newLocation[1]+")");
		setLocation(newLocation);
	}
	protected int[] getLocation() {
		return new int[]{width,height};
	}
	
	protected void setLocation(int[] location) {
//		System.out.println("NDB::"+this+".setLocation(w="+location[0]+", h="+location[1]+")");
		width = location[0];
		height = location[1];
		grid.moveTo(this, width, height);
	}
	
}