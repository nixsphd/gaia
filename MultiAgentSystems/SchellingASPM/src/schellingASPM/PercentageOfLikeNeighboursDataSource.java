/**
 * 
 */
package schellingASPM;

import repast.simphony.data2.AggregateDataSource;

/**
 * @author nick
 *
 */
public class PercentageOfLikeNeighboursDataSource implements AggregateDataSource {

	@Override
	public String getId() {
		return "Percentage Of Like Neighbours";
	}

	@Override
	public Class<?> getDataType() {
		return double.class;
	}

	@Override
	public Class<?> getSourceType() {
		return Shape.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object get(Iterable<?> objs, int size) {
		
		double percentageOfLikeNeighboursDataSource = 0;
		for (Shape shape: (Iterable<Shape>)objs) {
			percentageOfLikeNeighboursDataSource += shape.getLikeNeighbourRatio();
			
		}
		return (percentageOfLikeNeighboursDataSource/size)*100;
	}

	@Override
	public void reset() {}
	
}
