package ie.nix.gc;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.core.Node;

public class TenderMessage<T> implements Cloneable {

	private static final Logger LOGGER = LogManager.getLogger();
		
	@SuppressWarnings("rawtypes")
	public static Comparator<TenderMessage> expiringLast = 
			Comparator.comparingDouble(tender -> -tender.expirationTime);	

	@SuppressWarnings("rawtypes")
	public static Comparator<TenderMessage> expiringFirst = 
			Comparator.comparingDouble(tender -> tender.expirationTime);
	
	@SuppressWarnings("rawtypes")
	public static Comparator<TenderMessage> highestGuide = 
			Comparator.comparingDouble(tender -> -tender.guide);

	@SuppressWarnings("rawtypes")
	public static Comparator<TenderMessage> lowestGuide = 
			Comparator.comparingDouble(tender -> tender.guide);
	
	private static int nextContractID = 1000;

	protected static int getNextContractID() {
		int contractID = nextContractID++;
		return contractID;
	}

	public Node to;
	public final Node from;
	public final int contractID;
	public final long expirationTime;
	public final T task;
	public final double guide;
	public long gossipCounter;

	public TenderMessage(Node from, Node to, long expirationTime, T task) {
		this(from, to, getNextContractID(), expirationTime, task, 0, 0);
	}
	
	public TenderMessage(Node from, Node to, long expirationTime, T task, double guide) {
		this(from, to, getNextContractID(), expirationTime, task, guide, 0);
	}
	
	public TenderMessage(Node from, Node to, long expirationTime, T task, double guide, int gossipCounter) {
		this(from, to, getNextContractID(), expirationTime, task, guide, gossipCounter);
	}
	
	private TenderMessage(Node from, Node to, int contractID, long expirationTime, T task, double guide, long gossipCounter) {
		this.from = from;
		this.to = to;
		this.contractID = contractID;
		this.expirationTime = expirationTime;
		this.task = task;
		this.guide = guide;
		this.gossipCounter = gossipCounter;
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public TenderMessage<T> clone() {
		 TenderMessage<T> foo = null;
		try {
			foo = (TenderMessage<T>)super.clone();
			
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}
	
	public String toString() {
		return "TenderMessage " 
				+ "contractID=" + contractID + ", "
				+ from + " -> " + to + ", "
				+ "expirationTime=" + expirationTime +", "
				+ "task=" + task  +", "
				+ "guide=" + guide  +", "
				+ "gossipCounter=" + gossipCounter;
	}

	public boolean hasExpired() {
		return CommonState.getTime() >= expirationTime;
	}

}
