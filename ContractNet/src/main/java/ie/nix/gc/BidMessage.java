package ie.nix.gc;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.core.Node;

public class BidMessage<T, P> {

	private static final Logger LOGGER = LogManager.getLogger();

	@SuppressWarnings("rawtypes")
	public static Comparator<BidMessage> expiringLast = 
			Comparator.comparingDouble(bid -> -bid.expirationTime);	

	@SuppressWarnings("rawtypes")
	public static Comparator<BidMessage> expiringFirst = 
			Comparator.comparingDouble(bid -> bid.expirationTime);	

	@SuppressWarnings("rawtypes")
	public static Comparator<BidMessage> highestOffer = 
			Comparator.comparingDouble(bid -> -bid.offer);	

	@SuppressWarnings("rawtypes")
	public static Comparator<BidMessage> lowestOffer = 
			Comparator.comparingDouble(bid -> bid.offer);	
		
	public final int contractID;
	public final Node to;
	public final Node from;
	public final T task;
	public final P proposal;
	public final long expirationTime;
	public final double offer;

	public BidMessage(TenderMessage<T> tenderMessage, double offer) {
		this(tenderMessage, tenderMessage.expirationTime, null, offer);
	}
	
	public BidMessage(TenderMessage<T> tenderMessage, P proposal, double offer) {
		this(tenderMessage, tenderMessage.expirationTime, proposal, offer);
	}
	
	public BidMessage(TenderMessage<T> tenderMessage, long expirationTime, P proposal, double offer) {
		this.contractID = tenderMessage.contractID;
		this.to = tenderMessage.from;
		this.from = tenderMessage.to;
		this.task = tenderMessage.task;
		this.expirationTime = expirationTime;
		this.proposal = proposal;
		this.offer = offer;
		LOGGER.trace("[{}]~{}", CommonState.getTime(), this);
	}
	
	@Override
	public String toString() {
		return "BidMessage " 
				+ "contractID=" + contractID + ", "
				+ from + " -> " + to + ", "
				+ "expirationTime=" + expirationTime +", "
				+ "task=" + task +", "
				+ "proposal=" + proposal +", "
				+ "offer=" + offer;
	}
	
}