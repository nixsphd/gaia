package ie.nix.gc;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.core.Node;

public class StrategyAgent<T,P> extends Agent<T,P> {

	private static final Logger LOGGER = LogManager.getLogger();

	protected interface PreTender<T> {
		void preTender(Node node);
	}
	
	protected interface PostTender<T> {
		void postTender(Node node);
	}
	
	public interface ShouldTender<T> {
		boolean shouldTender(Node node);
	}
	
	public interface ShouldBid<T> {
		boolean shouldBid(TenderMessage<T> tenderMessage);
	}

	public interface ShouldAward<T,P> {
		boolean shouldAward(BidMessage<T,P> bidMessage);
	}
	
	public interface GetProposal<T,P> {
		P getProposal(TenderMessage<T> tenderMessage);
	}
	
	public interface GetGuide<T> {
		double getGuide(Node node, T task);
	}
	
	public interface GetOffer<T> {
		double getOffer(TenderMessage<T> tenderMessage);
	}
	
	public interface GetTenderComparator<T> {
		Comparator<? super TenderMessage<T>> getTenderComparator();
	}
	
	public interface GetBidComparator<T,P> {
		Comparator<? super BidMessage<T,P>> getBidComparator();
	}

	public interface Award<T,P> {
		void award(AwardMessage<T,P> awardMessage);
	}

	public interface Awarded<T,P> {
		void awarded(AwardMessage<T,P> awardMessage);
	}

//	public interface CreateTenderMessage<T> {
//		TenderMessage<T> createTenderMessage(Node node, Node neighbourNode);
//	}
//
//	public interface CreateBidMessage<T,P> {
//		BidMessage<T,P> createBidMessage(TenderMessage<T> tenderMessage);
//	}
//
//	public interface CreateAwardMessage<T,P> {
//		AwardMessage<T,P> createAwardMessage(BidMessage<T,P> bidMessage);
//	}

	@Override
	protected void preTender(Node node) {
		if (preTender != null) {
			preTender.preTender(node);
		} else {
			super.preTender(node);
		}
		LOGGER.trace("[{}]~node={} -> {}", CommonState.getTime(), node);
	}
	
	@Override
	protected void postTender(Node node) {
		if (postTender != null) {
			postTender.postTender(node);
		} else {
			super.postTender(node);
		}
		LOGGER.trace("[{}]~node={} -> {}", CommonState.getTime(), node);
	}
	
	@Override
	protected boolean shouldTender(Node node) {
		boolean should;
		if (shouldTender != null) {
			should = shouldTender.shouldTender(node);
		} else {
			should = super.shouldTender(node);
		}
		LOGGER.trace("[{}]~node={} -> {}", CommonState.getTime(), node, should);
		return should;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Comparator<? super BidMessage<T,P>> getBidComparator() {
		LOGGER.trace("[{}]", CommonState.getTime());
		if (getBidComparator != null) {
			return getBidComparator.getBidComparator();
		} else {
			return super.getBidComparator();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected boolean shouldAward(BidMessage<T,P> bidMessage) {
		boolean should;
		if (shouldAward != null) {
			should = shouldAward.shouldAward(bidMessage);
		} else {
			should = super.shouldAward(bidMessage);
		}
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), should);
		return should;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void award(AwardMessage<T,P> awardMessage) {
		LOGGER.trace("[{}]~awardMessage={}", CommonState.getTime(), awardMessage);
		if (award != null) {
			award.award(awardMessage);
		} else {
			super.award(awardMessage);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected boolean shouldBid(TenderMessage<T> tenderMessage) {
		boolean should;
		if (shouldBid != null) {
			should = shouldBid.shouldBid(tenderMessage);
		} else {
			should = super.shouldBid(tenderMessage);
		}
		LOGGER.debug("[{}]~tenderMessage={} -> {}", 
				CommonState.getTime(), tenderMessage, should);
		return should;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected P getProposal(TenderMessage<T> tenderMessage) {
		P proposal;
		if (getProposal != null) {
			proposal = (P)getProposal.getProposal(tenderMessage);
		} else {
			proposal = super.getProposal(tenderMessage);
		}
		LOGGER.debug("[{}]~tenderMessage={}, proposal={} -> {}", 
				CommonState.getTime(), tenderMessage, proposal);
		return proposal;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected double getGuide(Node node, T task) {
		double guide;
		if (getGuide != null) {
			guide = getGuide.getGuide(node, task);
		} else {
			guide = super.getGuide(node, task);
		}
		LOGGER.debug("[{}]~node={}, task={} -> {}", 
				CommonState.getTime(), node, task, guide);
		return guide;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected double getOffer(TenderMessage<T> tenderMessage) {
		double offer;
		if (getOffer != null) {
			offer = getOffer.getOffer(tenderMessage);
		} else {
			offer = super.getOffer(tenderMessage);
		}
		LOGGER.debug("[{}]~tenderMessage={} -> {}", 
				CommonState.getTime(), tenderMessage, offer);
		return offer;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected Comparator<? super TenderMessage<T>> getTenderComparator() {
		LOGGER.trace("[{}]", CommonState.getTime());
		if (getTenderComparator != null) {
			return getTenderComparator.getTenderComparator();
		} else {
			return super.getTenderComparator();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void awarded(AwardMessage<T,P> awardMessage) {
		LOGGER.trace("[{}]~awardMessage={}", CommonState.getTime(), awardMessage);
		if (awarded != null) {
			awarded.awarded(awardMessage);
		} else {
			super.awarded(awardMessage);
		}
	}

//	@SuppressWarnings("unchecked")
//	@Override
//	protected TenderMessage<T> createTenderMessage(Node node, Node neighbourNode) {
//		TenderMessage<T> tenderMessage;
//		if (createTenderMessage != null) {
//			tenderMessage = createTenderMessage.createTenderMessage(node, neighbourNode);
//		} else {
//			tenderMessage = super.createTenderMessage(node, neighbourNode);
//		}
//		LOGGER.trace("[{}]~tenderMessage={}}", CommonState.getTime(), tenderMessage);
//		return tenderMessage;
//	}	

//	@SuppressWarnings("unchecked")
//	@Override
//	protected AwardMessage<T,P> createAwardMessage(BidMessage<T,P> bidMessage) {
//		LOGGER.trace("[{}]~contractID={}", CommonState.getTime(), bidMessage.contractID);
//		if (createAwardMessage != null) {
//			return createAwardMessage.createAwardMessage(bidMessage);
//			
//		} else {
//			return super.createAwardMessage(bidMessage);
//			
//		}
//	}

//	@SuppressWarnings("unchecked")
//	@Override
//	protected BidMessage<T,P> createBidMessage(TenderMessage<T> tenderMessage) {
//		LOGGER.trace("[{}]~tenderMessage={}", CommonState.getTime(), tenderMessage);
//		if (createBidMessage != null) {
//			return createBidMessage.createBidMessage(tenderMessage);
//		} else {
//			return super.createBidMessage(tenderMessage);
//		}
//	}
	
	@SuppressWarnings("rawtypes")
	public static PreTender preTender;
	@SuppressWarnings("rawtypes")
	public static PostTender postTender;
	
	@SuppressWarnings("rawtypes")
	public static ShouldTender shouldTender;
	@SuppressWarnings("rawtypes")
	public static ShouldBid shouldBid;
	@SuppressWarnings("rawtypes")
	public static ShouldAward shouldAward;

	@SuppressWarnings("rawtypes")
	public static GetProposal getProposal;
	@SuppressWarnings("rawtypes")
	public static GetGuide getGuide;
	@SuppressWarnings("rawtypes")
	public static GetOffer getOffer;
		
	@SuppressWarnings("rawtypes")
	public static GetTenderComparator getTenderComparator;
	@SuppressWarnings("rawtypes")
	public static GetBidComparator getBidComparator;
	
	@SuppressWarnings("rawtypes")
	public static Award award;
	@SuppressWarnings("rawtypes")
	public static Awarded awarded;

//	@SuppressWarnings("rawtypes") 
//	public static CreateTenderMessage createTenderMessage;
//	@SuppressWarnings("rawtypes") 
//	public static CreateBidMessage createBidMessage;
//	@SuppressWarnings("rawtypes") 
//	public static CreateAwardMessage createAwardMessage;

	public static void clearAll() {
		
		StrategyAgent.preTender = null;
		StrategyAgent.postTender = null;
		
		StrategyAgent.shouldTender = null;
		StrategyAgent.getBidComparator = null;
		StrategyAgent.shouldAward = null;
		StrategyAgent.award = null;

		StrategyAgent.shouldBid = null;
		StrategyAgent.getTenderComparator = null;
		StrategyAgent.awarded = null;
		
//		StrategyAgent.createTenderMessage = null;
//		StrategyAgent.createAwardMessage = null;
//		StrategyAgent.createBidMessage = null;
	}

	public StrategyAgent(String prefix) {
		super(prefix);
	}

	@Override
	public String toString() {
		return "StrategyAgent";
	}

}
