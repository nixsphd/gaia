package ie.nix.gc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.edsim.EDSimulator;
import peersim.transport.Transport;

public class Agent<T,P> implements CDProtocol, EDProtocol {
	
//	class SimpleAgent<T> extends Agent<T,T> {
//		
//	}
	
	private static final Logger LOGGER = LogManager.getLogger();
	
//	@SuppressWarnings("rawtypes")
//	public static Comparator<TenderMessage> highestOffer = 
//			Comparator.comparingDouble(tenderMessage -> {
//				double offer = tenderMessage.to.getOffer(tenderMessage);
//		LOGGER.trace("[{}]~tenderMessage={}, offer={}", CommonState.getTime(), tenderMessage, offer);	
//		// The '-' just make it highest offer is best as the it normally lowest first.
//		return -offer; //*tenderMessage.guide;
//		});
////	return bestOffer;

	private static final String STEP = "step";
	private static final String TENDER_TIMEOUT = "tender-timeout";
	private static final String TENDER_GOSSIP_NEIGHBOURS = "tender-gossip-neighbours";
	private static final String TENDER_GOSSIP_COUNTER = "tender-gossip-counter";
    private static final String BID_BUFFER = "bid-buffer";
	private static final String AWARD_TIMEOUT = "award-timeout";
	
	private static final int DEFAULT_TENDER_TIMEOUT = 3;
	private static final int DEFAULT_TENDER_GOSSIP_NEIGHBOURS = 3;
	private static final int DEFAULT_TENDER_GOSSIP_COUNT = 3;
	private static final int DEFAULT_BID_BUFFER = 1;
	private static final int DEFAULT_AWARD_TIMEOUT = 1;
	private final int protocolID;
	
	private final int step;	
    private final int tenderTimeout;
	private final int tenderGossipNeighbours;
    private final int tenderGossipCount;
	private final long bidBuffer;
    private final int awardTimeout;
    

    private Queue<T> tasks;		
    private Map<Integer, TenderMessage<T>> tendersSent;		
    private Map<Integer, TenderMessage<T>> tendersRecieved;
	private BidMessage<T,P> bidSent;
    private Map<Integer, List<BidMessage<T,P>>> bidsRecieved;	
    private Map<Integer, AwardMessage<T,P>> awardsSent;
	private AwardMessage<T,P> awardRecieved;
	
	public Agent(String prefix) {
		LOGGER.trace("[{}]~prefix={}", CommonState.getTime(), prefix);

		this.protocolID = Configuration.lookupPid(prefix.replaceFirst("^protocol.", ""));

		this.step = Configuration.getInt(prefix+"."+STEP);
		this.tenderTimeout = Configuration.getInt(prefix+"."+TENDER_TIMEOUT, DEFAULT_TENDER_TIMEOUT);
		this.tenderGossipCount = Configuration.getInt(prefix+"."+TENDER_GOSSIP_COUNTER, DEFAULT_TENDER_GOSSIP_COUNT);
		this.tenderGossipNeighbours = Configuration.getInt(prefix+"."+TENDER_GOSSIP_NEIGHBOURS, DEFAULT_TENDER_GOSSIP_NEIGHBOURS);
		this.bidBuffer = Configuration.getInt(prefix+"."+BID_BUFFER, DEFAULT_BID_BUFFER);
		this.awardTimeout = Configuration.getInt(prefix+"."+AWARD_TIMEOUT, DEFAULT_AWARD_TIMEOUT);
		
		this.tasks = new LinkedList<T>();
		this.tendersSent = new HashMap<Integer, TenderMessage<T>>();
		this.tendersRecieved = new HashMap<Integer,TenderMessage<T>>();
		this.bidSent = null;
		this.bidsRecieved = new HashMap<Integer, List<BidMessage<T, P>>>();
		this.awardsSent = new HashMap<Integer, AwardMessage<T, P>>();
		this.awardRecieved = null;
			
		LOGGER.trace("[{}]~Agent={}", CommonState.getTime(), this);

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Agent<T,P> clone() {
		Agent<T,P> foo = null;
		try {
			foo = (Agent<T,P>)super.clone();
			
			foo.tasks = new LinkedList<T>();
			foo.tendersSent = new HashMap<Integer, TenderMessage<T>>();
			foo.bidsRecieved = new HashMap<Integer, List<BidMessage<T,P>>>();
			foo.awardsSent = new HashMap<Integer, AwardMessage<T,P>>();
			foo.tendersRecieved = new HashMap<Integer,TenderMessage<T>>();
			
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.trace("[{}]", CommonState.getTime());
		return foo;
	}

	public void shutdown() {
		this.tasks.clear();
		this.tendersSent.clear();
		this.tendersRecieved.clear();
		this.bidSent = null;
		this.bidsRecieved.clear();
		this.awardsSent.clear();
		this.awardRecieved = null;
        LOGGER.debug("[{}] {}", CommonState.getTime(), this);
	}

	/******************************************************************************************
	 * 
	 * 	Customisable
	 * 
	 ******************************************************************************************/
	
	protected void preTender(Node node) {}
	
	protected void postTender(Node node) {}
	
	protected boolean shouldTender(Node node) { 
		boolean shouldTender = !isContracting() && hasTasks();
	    LOGGER.trace("[{}] node={}, isContracting={}, hasTasks={}, shouldTender={}",
	    		CommonState.getTime(), node, isContracting(), hasTasks(), shouldTender);
		return shouldTender;
	}
	
	protected boolean shouldBid(TenderMessage<T> tenderMessage) {
		boolean shouldBid = isIdle();
        LOGGER.trace("[{}]~contractID={}, node={}, isIdle()={}, shouldBid={}", 
        		CommonState.getTime(), tenderMessage.contractID, tenderMessage.to, isIdle(), shouldBid);
		return shouldBid;
	}
	
	protected boolean shouldAward(BidMessage<T,P> bestBid) {
		return true;
	}

	protected double getGuide(Node node, T task) {
		return 0;
	}

	protected P getProposal(TenderMessage<T> tenderMessage) {
		return null;
	}
	
	protected double getOffer(TenderMessage<T> tenderMessage) {
		return 0;
	}

	protected Comparator<? super TenderMessage<T>> getTenderComparator() {
		return TenderMessage.highestGuide;
	}
	
	protected Comparator<? super BidMessage<T,P>> getBidComparator() {
		return BidMessage.highestOffer;
	}
	
	protected void award(AwardMessage<T,P> awardMessage) {
	    LOGGER.debug("[{}]~awardMessage={}", CommonState.getTime(), awardMessage);
	}

	protected void awarded(AwardMessage<T,P> awardMessage) {
		LOGGER.debug("[{}]~awarded={}", CommonState.getTime(), awardMessage);
		
	}
	
	/******************************************************************************************
	 * 
	 * 	PeerSim Next Cycle
	 * 
	 ******************************************************************************************/
	
	@Override
	public final void nextCycle(Node node, int protocolID) {
		LOGGER.trace("[{}] host={}", CommonState.getTime(), node);
	
		preTender(node);
		
		while (shouldTender(node)) {
			LOGGER.debug("[{}]~node={}, shouldTender", CommonState.getTime(), node);
			
			// Get the task
			T task = removeTask();
			
			// Create the tender message
			TenderMessage<T> tenderMessage = createTenderMessage(node, task);
			
			// Prepare for the bids.
			addSentTender(tenderMessage);
			
			// Send to all the neighbors
			sendTenderMessages(tenderMessage);
	
			// After a delay, review the bids
			addCallback(getTenderTimeout(), new Callback(() -> {
				evaluateBidsForTender(tenderMessage);
			}), node);
		}
	
		// After a delay, review the bids
		addCallback(getTenderTimeout() + 1, new Callback(() -> {
			postTender(node);
		}), node);
		
	}
	
	/*
	 * Bids Sent
	 */
	private final void bidForTender(TenderMessage<T> tenderMessage) {
		// Send a bid ...
		BidMessage<T,P> bid = createBidMessage(tenderMessage);
		setBidSent(bid);
		
		sendBidMessage(bid);
	
		// Expire the bid after (hence the +1) the Tender is expired.
		long bidExpirationDelay = (bid.expirationTime + 1 - CommonState.getTime());
		addCallback(bidExpirationDelay, new Callback(() -> {
			LOGGER.trace("[{}]~bidSent={}, after delay={}", CommonState.getTime(), bidSent, bidExpirationDelay);
			if (bidSent != null) {
				clearBidSent();
			}
		}), bid.from);
	}
	
	private final void evaluateBidsForTender(TenderMessage<T> tender) {	
			
		BidMessage<T,P> bestBid = getATopRecievedBid(tender);
		
		// Should I award?
		if (bestBid != null && shouldAward(bestBid)) {
			LOGGER.debug("[{}]~Awarding contractID={} to {}", 
					CommonState.getTime(), tender.contractID, bestBid.from);
			awardBid(bestBid);
			
		} else {
			LOGGER.debug("[{}]~No bids for tender={}", CommonState.getTime(), tender);
			
		}
	
		// Clean-up
		removeSentTender(tender);
	}
	
	private final BidMessage<T,P> getATopRecievedBid(TenderMessage<T> tender) {

		List<BidMessage<T,P>> tendersBidsRecieved = getBidsRecieved(tender.contractID);
		if (tendersBidsRecieved != null && tendersBidsRecieved.size() > 0) {
			
			Comparator<? super BidMessage<T,P>> comparator = getBidComparator();
			
			// Get top bid
			Collections.sort(tendersBidsRecieved, comparator);
			final BidMessage<T,P> topBid = tendersBidsRecieved.get(0);
			
			// Get all the other equally good
			List<BidMessage<T,P>> topBidsList = tendersBidsRecieved.stream().
					filter(bid -> comparator.compare(bid, topBid) == 0).
					collect(Collectors.toList());
			
			// If there's several tops bids so randomly select one.
			if (topBidsList.size() > 1) {
				BidMessage<T,P> newTopBid = topBidsList.get(CommonState.r.nextInt(topBidsList.size()));
				LOGGER.trace("[{}]~Choice of {} top bids, selecting {} from {}", 
						CommonState.getTime(), topBidsList.size(), newTopBid, topBidsList);
				return newTopBid;
				
			} else {
				// Otherwise just take the one already selected, it's the outright best.
				return topBid;
				
			}
			
		} else {
			LOGGER.trace("[{}]~No bids recieved :(", CommonState.getTime());
			return null;
			
		}
		
	}

	/*
	 * Awards Sent
	 */
	private final void awardBid(BidMessage<T,P> bidMessage) {
		LOGGER.debug("[{}]~bidMessage={}", CommonState.getTime(), bidMessage);
		
		AwardMessage<T,P> awardMessage = createAwardMessage(bidMessage);
		
		addAwardSent(awardMessage);
		
		award(awardMessage);
		
		sendAwardMessage(getTransport(bidMessage.from), awardMessage);
		
		// Expire the award at the timeout, give 1 tick extra to make sure it's expired.
		long awardExpirationTime = (awardMessage.expirationTime + 1) - CommonState.getTime();
		addCallback(awardExpirationTime, new Callback(() -> {
			removeAwardSent(awardMessage);
		}), awardMessage.from);
		
	}

	
	/******************************************************************************************
	 * 
	 * 	PeerSim Process Events
	 * 
	 ******************************************************************************************/

	@SuppressWarnings("unchecked")
	@Override
	public void processEvent(Node node, int protocolID, Object event) {
		LOGGER.trace("[{}]~node={}, event={}", CommonState.getTime(), node, event);
		if (event instanceof Callback) {
			handleCallback(event);
			
		} else if (event instanceof TenderMessage) {
			handleTenderMessage((TenderMessage<T>)event);
		
		} else if (event instanceof BidMessage) {
			handleBidMessage((BidMessage<T,P>) event);
					
		} else if (event instanceof AwardMessage) {
			handleAwardMessage((AwardMessage<T,P>)event);
		
		}
	}

	private final void handleCallback(Object event) {
		((Callback)event).action.run();
	}

	/*
	 * Tender Received
	 */
	private final void handleTenderMessage(TenderMessage<T> tenderMessage) {
		// If it hasn't expired and it isn't already processed, as in we have it.
		if (!tenderMessage.hasExpired() && !haveRecievedTender(tenderMessage)) {
			LOGGER.trace("[{}]]~tenderMessage={}", CommonState.getTime(), tenderMessage);
			
			// Add the tender
			addRecievedTender(tenderMessage);

			// Add a reevaluation just before this tender expires.
			evaluateRecievedTenders(tenderMessage);
			
			// Expire the tender after (hence the +1) the Tender is expired.
			long tenderExpirationDelay = (tenderMessage.expirationTime + 1 - CommonState.getTime());
			addCallback(tenderExpirationDelay, new Callback(() -> {
				LOGGER.trace("[{}]~tenderRecievedt={}, after delay={}", 
						CommonState.getTime(), tenderMessage, tenderExpirationDelay);
				removeRecievedTender(tenderMessage);
			}), tenderMessage.to);
		
			if (shouldGossip() && tenderMessage.gossipCounter > 0) {
				resendTenderMessage(tenderMessage);
			}
		}
	}

	private final void evaluateRecievedTenders(TenderMessage<T> tenderMessage) {
		long bidEvaluationDelay = 
				(tenderMessage.expirationTime - getBidBuffer()) - CommonState.getTime();
	
		LOGGER.trace("[{}] delay={}", CommonState.getTime(), bidEvaluationDelay);	
		addCallback(bidEvaluationDelay, new Callback(() -> {
			LOGGER.trace("[{}] slept for {}", CommonState.getTime(), bidEvaluationDelay);		
			
			// remove expired tenders....
			removeExpierdRecievedTenders();
			
			// If there's any left, then we get the top one.
			if (hasRecievedTenders()) {
				TenderMessage<T> topTender = getATopRecievedTender();
				int topTenderTimeout = 
						(int)(topTender.expirationTime - getBidBuffer() - CommonState.getTime());
				LOGGER.trace("[{}]~topTender={}, topTenderTimeout={}", 
						CommonState.getTime(), topTender, topTenderTimeout);	
				if ((topTenderTimeout <= 0) && shouldBid(topTender)) {
					bidForTender(topTender);
				}
			}
			
		 }), tenderMessage.to);
	}

	private final void removeExpierdRecievedTenders() {
		LOGGER.trace("[{}] node={}", CommonState.getTime());
		// We can remove the expired tenders as we go cause that we be modifing the list as we traverse it.
		List<TenderMessage<T>> tendersToRemove = new ArrayList<TenderMessage<T>>();
		for (TenderMessage<T> tender : getRecievedTenders()) {
			if (tender.hasExpired()) {
				tendersToRemove.add(tender);
			}
		}
		if (tendersToRemove.size() > 0) {
			LOGGER.trace("[{}]~tendersToRemove={}", 
					CommonState.getTime(), tendersToRemove);
			tendersToRemove.forEach(tender -> {
				removeRecievedTender(tender);
			});
		}
	}
	
	private final TenderMessage<T> getATopRecievedTender() {
		Comparator<? super TenderMessage<T>> comparator = getTenderComparator();
		// Sort all the tenders again.	
		List<TenderMessage<T>> tenderList = getRecievedTenders();
		Collections.sort(tenderList, comparator);
		TenderMessage<T> topTender = tenderList.get(0);
		
		List<TenderMessage<T>> topTenderList = tenderList.stream().
				filter(tender -> comparator.compare(tender, topTender) == 0).
				collect(Collectors.toList());
		
		if (topTenderList.size() > 1) {
			TenderMessage<T> randomTopTender = topTenderList.get(CommonState.r.nextInt(topTenderList.size()));
			LOGGER.trace("[{}]~Choice of {} top tenders, selecting {} from {}", 
					CommonState.getTime(), topTenderList.size(), randomTopTender, topTenderList);
			return randomTopTender;
		} else {
			LOGGER.trace("[{}]~No choice selecting {}", 
					CommonState.getTime(), topTender);
			return topTender;
		}
	}

	/*
	 * Bids Received
	 */
	private final void handleBidMessage(BidMessage<T,P> bidMessage) {
		LOGGER.trace("[{}]~contractID={}, bidMessage={}", CommonState.getTime(), bidMessage.contractID, bidMessage);
		List<BidMessage<T,P>> contractBids = getBidsRecieved(bidMessage.contractID);
		if (contractBids != null) {
			// Add the new bid
			contractBids.add(bidMessage);
		} else {
			LOGGER.debug("[{}]~Ignoring {}, not acception bids for contractID={}", 
					CommonState.getTime(), bidMessage, bidMessage.contractID);
		}
	}

	/*
	 * Awards Received
	 */	
	private final void handleAwardMessage(AwardMessage<T,P> awardMessage) {
		LOGGER.debug("[{}]~awardMessage={}", CommonState.getTime(), awardMessage);
		
		assert (bidSent != null) : 
			this + " was not bidding but just won an award for " + awardMessage;
		assert (bidSent.contractID == awardMessage.contractID) : 
			this + " was bidding for " + bidSent + " but just won an award for " + awardMessage;
		
		setRecievedAward(awardMessage);
				
		// Expire the award at the timeout, need the +1 to make sure the award is expired. 
		long awardExpirationTime = (awardMessage.expirationTime + 1) - CommonState.getTime();
		LOGGER.debug("[{}]~cleanup in awardExpirationTime={}, awardRecieved={}, awardMessage={}", 
				CommonState.getTime(), awardExpirationTime, awardRecieved, awardMessage);
		addCallback(awardExpirationTime, new Callback(() -> {
			LOGGER.debug("[{}]~Calling clearRecievedAward() awardRecieved={}, awardMessage={}", 
					CommonState.getTime(), awardRecieved, awardMessage);
			clearRecievedAward();
		}), awardMessage.to);
		
		awarded(awardMessage);
	
	}
	
	/******************************************************************************************
	 * 
	 * 	PeerSim Utilities
	 * 
	 ******************************************************************************************/
	
	protected int getProtocolID() {
		return protocolID;
	}
	
	protected void addCallback(long delay, Callback callback, Node node) {
		LOGGER.trace("[{}]~delay={}, node={}", CommonState.getTime(), delay, node);
		EDSimulator.add(delay, callback, node, protocolID);
	}
	/*
	protected final Stream<Node> getNeighbourNodes(Node node) {
		Linkable neighbourhood = (Linkable) node.getProtocol(FastConfig.getLinkable(protocolID));
		Builder<Node> builder = Stream.builder();
		for (int n = 0; n < neighbourhood.degree(); n++) {
			Node neighbourNode = (Node) neighbourhood.getNeighbor(n);
			// Failure handling
			if (neighbourNode.isUp()) {
				builder.add(neighbourNode);
			}
		}
		Stream<Node> neighbourNodes = builder.build();
		LOGGER.trace("[{}] node={} -> {}", CommonState.getTime(), node,neighbourNodes);
		return neighbourNodes;
		
	}
	*/
	protected final Stream<Node> getNeighbourNodes(Node node) {
		Stream<Node> neighbourNodes = getNeighbors(node, tenderGossipNeighbours);
		LOGGER.trace("[{}] node={} -> {}", CommonState.getTime(), node,neighbourNodes);
		return neighbourNodes;

	}
	
	private Stream<Node> getNeighbors(Node node, int nodesToFind) {
		Linkable neighbourhood = (Linkable)node.getProtocol(FastConfig.getLinkable(protocolID));
		ArrayList<Node> nodes = new ArrayList<Node>(nodesToFind);
		int neighbourhoodSize = neighbourhood.degree();
		int allowedMisses = neighbourhoodSize*2;
		while (allowedMisses != 0 && nodes.size() < nodesToFind) {
			Node neigbhour = neighbourhood.getNeighbor(CommonState.r.nextInt(neighbourhoodSize));
			if (!nodes.contains(neigbhour)) {
				nodes.add(neigbhour);
			} else {
				allowedMisses--;
			}
		}
		return nodes.stream();
	}

	protected final Transport getTransport(Node node) {
		return (Transport) node.getProtocol(FastConfig.getTransport(protocolID));
	}
	
	/******************************************************************************************
	 * 
	 * 	Config
	 * 
	 ******************************************************************************************/
	
	protected int getStep() {
		return step;
	}
	
	protected int getTenderTimeout() {
		return tenderTimeout;
	}

	protected int getAwardTimeout() {
		return awardTimeout;
	}
	
	protected boolean shouldGossip() {
		return tenderGossipCount > 0;
	}
	
	protected int getTenderGossipCount() {
		return tenderGossipCount;
	}
	
	protected long getBidBuffer() {
		return bidBuffer;
	}

	/******************************************************************************************
	 * 
	 * 	Task APIs
	 * 
	 ******************************************************************************************/
	
	protected final boolean hasTasks() {
		return !tasks.isEmpty();
	}
	
	protected final int numberOfTasks() {
		return tasks.size();
	}
	
	protected final void addTask(T task) {
		tasks.add(task);
        LOGGER.trace("[{}], task={}, tasks={}", CommonState.getTime(), task, tasks);
	}
	
	protected final T nextTask() {
		return tasks.peek();
	}
	
	protected final T removeTask() {
		T task = tasks.poll();
        LOGGER.trace("[{}], task={}, tasks={}", CommonState.getTime(), task, tasks);
		return task;
	}

	protected void addTasks(List<T> tasksToAdd) {
		tasks.addAll(tasksToAdd);
        LOGGER.trace("[{}], tasksToAdd={}, tasks={}", CommonState.getTime(), tasksToAdd, tasks);
	}
	
	protected Stream<T> getTasks() {
		return tasks.stream();
	}

	protected  void clearTasks() {
		tasks.clear();
        LOGGER.trace("[{}], tasks={}", CommonState.getTime(), tasks);
	}
	
	/******************************************************************************************
	 * 
	 * 	State APIs
	 * 
	 ******************************************************************************************/
	
	protected final boolean isTendering() {
		boolean isTendering = (tendersSent.size() > 0);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(),isTendering);
		return isTendering;
	}

	protected final boolean isBidding() {
		boolean isBidding = (bidSent != null);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), isBidding);
		return isBidding;
	}

	protected final boolean isAwarding() {
		boolean isAwarding = (awardsSent.size() > 0);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), isAwarding);
		return isAwarding;
	}

	protected final boolean isBeingAwarded() {
		boolean isBeingAwarded = (awardRecieved != null);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), isBeingAwarded);
		return isBeingAwarded;
	}

	protected boolean isIdle() {
		boolean isIdle = !isTendering() && !isBidding() && !isAwarding() && !isBeingAwarded();
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), isIdle);
		return isIdle;
	}

	protected boolean isManaging() {
		boolean canTender = isTendering() || isAwarding();
	    LOGGER.trace("[{}]~isTendering()={}, isAwarding={}", 
	    		CommonState.getTime(), isTendering(), isAwarding());
		return canTender;
	}

	protected boolean isContracting() {
		boolean canTender = isBidding() || isBeingAwarded();
	    LOGGER.trace("[{}]~isBidding()={}, isBeingAwarded={}", 
	    		CommonState.getTime(), isBidding(), isBeingAwarded());
		return canTender;
	}

	/******************************************************************************************
	 * 
	 * 	Setters & Getters APIs
	 * 
	 ******************************************************************************************/
	
	/*
	 * Tenders Sent
	 */
	@SuppressWarnings("unused")
	private final TenderMessage<T> getSentTender(int contractID) {
		TenderMessage<T> sentTender = tendersSent.get(contractID);
		LOGGER.trace("[{}] contractID={} -> {}", CommonState.getTime(), contractID, sentTender);
		return sentTender;
	}

	private final void addSentTender(TenderMessage<T> tenderMessage) {
		LOGGER.trace("[{}]~contractID={}", CommonState.getTime(), tenderMessage.contractID);
		
		// Add to archive of the Tender messages
		tendersSent.putIfAbsent(tenderMessage.contractID, tenderMessage);
		// Create an empty list to store the bids for the tender.
		bidsRecieved.putIfAbsent(tenderMessage.contractID, new ArrayList<BidMessage<T,P>>());
	}
	
	private final void removeSentTender(TenderMessage<T> tenderMessage) {
		LOGGER.debug("[{}]~tenderMessage={}", CommonState.getTime(), tenderMessage);
		tendersSent.remove(tenderMessage.contractID);
		bidsRecieved.remove(tenderMessage.contractID);
	}
	
	/*
	 * Tenders Recieved
	 */
	private final boolean hasRecievedTenders() {
		boolean hasRecievedTenders = tendersRecieved.size() > 0;
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), hasRecievedTenders);
		return hasRecievedTenders;
	}
	
	@SuppressWarnings("unused")
	private final int numberOfRecievedTenders() {
		int numberOfRecievedTenders = tendersRecieved.size();
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), numberOfRecievedTenders);
		return numberOfRecievedTenders;
	}

	private final boolean haveRecievedTender(TenderMessage<T> tenderMessage) {
		boolean haveRecievedTender = tendersRecieved.containsKey(tenderMessage.contractID);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), haveRecievedTender);
		return haveRecievedTender;
	}

	private final List<TenderMessage<T>> getRecievedTenders() {
		return new ArrayList<TenderMessage<T>>(tendersRecieved.values());
	}

	private final void addRecievedTender(TenderMessage<T> tenderMessage) {
		LOGGER.trace("[{}]~tenderMessage={}", CommonState.getTime(), tenderMessage);
		tendersRecieved.put(tenderMessage.contractID, tenderMessage);
	}
	
	private final void removeRecievedTender(TenderMessage<T> tenderMessage) {
		LOGGER.trace("[{}]~tenderMessage={}", CommonState.getTime(), tenderMessage);
		tendersRecieved.remove(tenderMessage.contractID);
	}
	
	/*
	 * Bids Sent
	 */
	@SuppressWarnings("unused")
	private final BidMessage<T,P> getBidSent() {
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), bidSent);
		return bidSent;
	}
	
	private final void setBidSent(BidMessage<T,P> bidMessage) {
		LOGGER.trace("[{}]~currentBid={}", CommonState.getTime(), bidSent);
		assert(bidSent == null): "The bidSent is not null, "+bidSent;
		bidSent = bidMessage;
	}
	
	private final void clearBidSent() {
		LOGGER.trace("[{}]~bidSent={}", CommonState.getTime(), bidSent);
		assert(bidSent != null): "The bidSent is null!";
		bidSent = null;
	}

	/*
	 * Bids Received
	 */
	private final List<BidMessage<T,P>> getBidsRecieved(int contractID) {
		return bidsRecieved.get(contractID);
	}
	
	/*
	 * Awards Sent
	 */
	private final void addAwardSent(AwardMessage<T,P> awardMessage) {
		LOGGER.trace("[{}]~{}", CommonState.getTime(), awardMessage);
		// Add to archive of the Tender messages awarded
		awardsSent.putIfAbsent(awardMessage.contractID, awardMessage);
	}
	
	private final void removeAwardSent(AwardMessage<T,P> awardMessage) {
		LOGGER.trace("[{}]~{}", CommonState.getTime(), awardMessage);
		awardsSent.remove(awardMessage.contractID);
	}
	
	@SuppressWarnings("unused")
	private final AwardMessage<T,P> getSentAward(int contractID) {
		AwardMessage<T,P> sentAward = awardsSent.get(contractID);
		LOGGER.trace("[{}] contractID={} -> {}", CommonState.getTime(), contractID, sentAward);
		return sentAward;
	}
	
	/*
	 * Awards Received
	 */
	private final void setRecievedAward(AwardMessage<T,P> awardMessage) {
		LOGGER.trace("[{}]~{}", CommonState.getTime(), awardRecieved);
		assert(awardRecieved == null): "The awardRecieved is not null, "+awardRecieved;
		awardRecieved = awardMessage;
	}
	
	private final void clearRecievedAward() {
		assert(awardRecieved != null): "The awardRecieved is null for "+this;
		LOGGER.trace("[{}]~{} contractID={}", 
				CommonState.getTime(), this, awardRecieved.contractID);
		awardRecieved = null;
	}

	/******************************************************************************************
	 * 
	 * 	Message APIs
	 * 
	 ******************************************************************************************/
	
	protected TenderMessage<T> createTenderMessage(Node node, T task) {
		return createTenderMessage(node, null, task);
	}
	
	protected TenderMessage<T> createTenderMessage(Node node, Node neighbourNode, T task) {
		long expirationTime = CommonState.getTime() + getTenderTimeout();
		int tenderGossipCount = getTenderGossipCount();
		double guide = getGuide(node, task);
		TenderMessage<T> tenderMessage = 
				new TenderMessage<T>(node, neighbourNode, expirationTime, task, guide, tenderGossipCount);
		LOGGER.trace("[{}]~tenderMessage={}", CommonState.getTime(), tenderMessage);		
		return tenderMessage;
	}

	private final void sendTenderMessages(TenderMessage<T> tenderMessage) {
		Stream<Node> neighbourNodes = getNeighbourNodes(tenderMessage.from);
		Transport transport = getTransport(tenderMessage.from);
		neighbourNodes.forEach(neighbourNode -> {
			TenderMessage<T> tenderMessageClone = tenderMessage.clone();
			tenderMessageClone.to = neighbourNode;
			transport.send(tenderMessageClone.from, tenderMessageClone.to, tenderMessageClone, getProtocolID());
			LOGGER.debug("[{}]~Sending {}", CommonState.getTime(), tenderMessageClone);
		});
	}
	
	private final void resendTenderMessage(TenderMessage<T> tenderMessage) {
		Transport transport = getTransport(tenderMessage.from);
		Stream<Node> neighbourNodes = getNeighbourNodes(tenderMessage.to);
		neighbourNodes.forEach(neighbourNode -> {
			if (neighbourNode.getID() != tenderMessage.from.getID()) {
				TenderMessage<T> tenderMessageClone = tenderMessage.clone();
				tenderMessageClone.gossipCounter--;
				tenderMessageClone.to = neighbourNode;
				transport.send(tenderMessageClone.from, tenderMessageClone.to, tenderMessageClone, getProtocolID());
				LOGGER.trace("[{}]~Resending {}", CommonState.getTime(), tenderMessageClone);
			} 
		});
	}
	
	protected BidMessage<T,P> createBidMessage(TenderMessage<T> tenderMessage) {
		BidMessage<T,P> bidMessage = new BidMessage<T,P>(tenderMessage, 
				getProposal(tenderMessage), getOffer(tenderMessage));
		LOGGER.trace("[{}]~bidMessage={}", CommonState.getTime(), bidMessage);		
		return bidMessage;
	}

	private final void sendBidMessage(BidMessage<T,P> bidMessage) {
		Transport transport = getTransport(bidMessage.from);
		transport.send(bidMessage.from, bidMessage.to, bidMessage, getProtocolID());
		LOGGER.debug("[{}]~Sending {}", CommonState.getTime(), bidMessage);
	}

	protected AwardMessage<T,P> createAwardMessage(BidMessage<T,P> bidMessage) {
		// Calculate the expiration time.
		long expirationTime = CommonState.getTime() + getAwardTimeout();
		return createAwardMessage(bidMessage, expirationTime);
	}
	
	protected final AwardMessage<T,P> createAwardMessage(BidMessage<T,P> bidMessage, long expirationTime) {
		AwardMessage<T,P> awardMessage = new AwardMessage<T,P>(bidMessage, expirationTime);
		LOGGER.trace("[{}]~awardMessage={}", CommonState.getTime(), awardMessage);
		return awardMessage;
	}
	
	private final void sendAwardMessage(Transport transport, AwardMessage<T,P> awardMessage) {
		transport.send(awardMessage.from, awardMessage.to, awardMessage, getProtocolID());
		LOGGER.debug("[{}]~Sending {}", CommonState.getTime(), awardMessage);
	}
	
}