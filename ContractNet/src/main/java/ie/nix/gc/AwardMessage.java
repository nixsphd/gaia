package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.core.Node;

public class AwardMessage<T, P> implements Cloneable {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	public final int contractID;
	public final Node to;
	public final Node from;
	public final long expirationTime;
	public final T task;
	public final P proposal;
	
	public AwardMessage(BidMessage<T, P> bidMessage) {
		this(bidMessage, bidMessage.expirationTime);
	}
	
	public AwardMessage(BidMessage<T, P> bidMessage, long expirationTime) {
		this.contractID = bidMessage.contractID;
		this.to = bidMessage.from;
		this.from = bidMessage.to;
		this.expirationTime = expirationTime;
		this.task = bidMessage.task;
		this.proposal = bidMessage.proposal;
		LOGGER.trace("[{}]~{}", CommonState.getTime(), this);
	}
	
	@Override
	public String toString() {
		return "AwardMessage " 
				+ "contractID=" + contractID + ", "
				+ from + " -> " + to + ", "
				+ "expirationTime=" + expirationTime +", "
				+ "task=" + task +", "
				+ "proposal=" + proposal;
	}
	
}