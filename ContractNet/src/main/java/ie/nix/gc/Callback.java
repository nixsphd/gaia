package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class Callback {
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	final Runnable action;

	public Callback(Runnable action) {
		this.action = action;
		LOGGER.trace("[{}]", CommonState.getTime());
	}

	@Override
	public String toString() {
		return "Callback";
	}

}