package ie.nix.cnet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.cnet.ContractNETProtocol.AwardMessage;
import ie.nix.cnet.ContractNETProtocol.BidMessage;
import ie.nix.cnet.ContractNETProtocol.Callback;
import ie.nix.cnet.ContractNETProtocol.GossipTaskAnnounceMessage;
import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.edsim.EDSimulator;
import peersim.transport.Transport;

public class Contractor implements EDProtocol {

	private static final Logger LOGGER = LogManager.getLogger();
	
	// TODO Consider tasks are typed.
	// TODO Support multiple bids at once, so currentBid becomes a List.

    public static final String MANAGER_PROTOCOL = "manager";
	
    private static final String BID_BUFFER = "bid-buffer";

	private static final int DEFAULT_BID_BUFFER = 1;

    protected final int managerProtocolID;
	
    private Map<Integer, TaskAnnounceMessage> tasks;
	
	private BidMessage currentBid;
	private AwardMessage currentAward;

	private long bidBuffer;

	public Contractor(String prefix) {
		this.managerProtocolID = Configuration.getPid(prefix + "." + MANAGER_PROTOCOL);
		this.bidBuffer = Configuration.getInt(prefix+"."+BID_BUFFER, DEFAULT_BID_BUFFER);
		this.tasks = new HashMap<Integer,TaskAnnounceMessage>();
		this.currentBid = null;
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), this);
	}

	@Override
	public Contractor clone() {
		Contractor foo = null;
		try {
			foo = (Contractor) super.clone();
			foo.tasks = new HashMap<Integer,TaskAnnounceMessage>();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), foo);
		return foo;
	}

	@Override
	public String toString() {
		return "Contractor [currentBid=" + currentBid+ "]";
	}

	private final Stream<Node> getNeighbourNodes(Node node, int protocolID) {
		Linkable neighbourhood = (Linkable) node.getProtocol(FastConfig.getLinkable(protocolID));
		Builder<Node> builder = Stream.builder();
		for (int n = 0; n < neighbourhood.degree(); n++) {
			Node neighbourNode = (Node) neighbourhood.getNeighbor(n);
			// Failure handling
			if (neighbourNode.isUp()) {
				builder.add(neighbourNode);
			}
		}
		Stream<Node> neighbourNodes = builder.build();
		LOGGER.trace("[{}] node={} -> {}", CommonState.getTime(), node,neighbourNodes);
		return neighbourNodes;
		
	}

	@Override
	public void processEvent(Node node, int protocolID, Object event) {
		LOGGER.trace("[{}]~node={}, event={}", CommonState.getTime(), node, event);
		if (event instanceof Callback) {
			((Callback)event).action.run();
			
		} else if (event instanceof TaskAnnounceMessage) {
			handleTaskAnnouncementMessage(node, protocolID, (TaskAnnounceMessage)event);
		
		} else if (event instanceof AwardMessage) {
			handleAwardMessage(node, protocolID, (AwardMessage)event);
		
		}
	}

	private final void handleTaskAnnouncementMessage(Node node, int protocolID, 
			TaskAnnounceMessage taskAnnounceMessage) {
		LOGGER.trace("[{}]~contractID={}, node={}, taskAnnounceMessage={}", 
				CommonState.getTime(),  taskAnnounceMessage.contractID, node, taskAnnounceMessage);
		
		// If it hasn't expired and it isn't already processed, as in we have it.
		if (!taskHasExpired(taskAnnounceMessage) &&  !haveTask(taskAnnounceMessage)) {
				
			LOGGER.trace("[{}]]~contractID={}, node={}, taskAnnounceMessage={} is valid", 
					CommonState.getTime(), taskAnnounceMessage.contractID, node, taskAnnounceMessage);
			
			// Add the task
			addTask(taskAnnounceMessage);
			
			// Add a reevaluation just before this task expires.
			long bidDelay = getTenderTimeout(taskAnnounceMessage);
			evaluateTasks(bidDelay, node, protocolID);
			LOGGER.trace("[{}]~contractID={}, node={}, bidDelay={}", 
					CommonState.getTime(), taskAnnounceMessage.contractID, node, bidDelay);
		
			if (taskAnnounceMessage instanceof GossipTaskAnnounceMessage) {
				GossipTaskAnnounceMessage gossipTaskAnnounceMessage = (GossipTaskAnnounceMessage)taskAnnounceMessage;
				if (gossipTaskAnnounceMessage.getCounter() > 0) {
					Transport transport = (Transport) node.getProtocol(FastConfig.getTransport(protocolID));
					Stream<Node> neighbourNodes = getNeighbourNodes(node, protocolID);
					neighbourNodes.forEach(neighbourNode -> {
						GossipTaskAnnounceMessage clonedTaskAnnounceMessage = 
								(GossipTaskAnnounceMessage)taskAnnounceMessage.clone();
						clonedTaskAnnounceMessage.to = neighbourNode;
						clonedTaskAnnounceMessage.decrementCounter();
						// Don't resend to the originator.
						if (clonedTaskAnnounceMessage.to.getID() != clonedTaskAnnounceMessage.from.getID()) {
							transport.send(node, clonedTaskAnnounceMessage.to, clonedTaskAnnounceMessage, protocolID);
							LOGGER.trace("[{}]~contractID={}, Re-sending from {} to {}, clonedTaskAnnounceMessage={}", 
									CommonState.getTime(), gossipTaskAnnounceMessage.contractID, node, neighbourNode,
									clonedTaskAnnounceMessage);
						} else {
							LOGGER.trace("[{}]~contractID={}, Not re-sending from {} to {}, clonedTaskAnnounceMessage={}", 
									CommonState.getTime(), gossipTaskAnnounceMessage.contractID, node, neighbourNode,
									clonedTaskAnnounceMessage);
						}
					});
				}
			}
		}
	}

	/*
	 * Tenders
	 */
	private final boolean haveTasks() {
		return tasks.size() > 0;
	}

	private final boolean haveTask(TaskAnnounceMessage taskAnnounceMessage) {
		return tasks.containsKey(taskAnnounceMessage.contractID);
	}

	private final List<TaskAnnounceMessage> getTasks() {
		return new ArrayList<TaskAnnounceMessage>(tasks.values());
	}

	private final void addTask(TaskAnnounceMessage taskAnnounceMessage) {
		tasks.put(taskAnnounceMessage.contractID, taskAnnounceMessage);
	}

	private final long getTenderTimeout(TaskAnnounceMessage taskAnnounceMessage) {
		return (taskAnnounceMessage.expirationTime - bidBuffer) - CommonState.getTime();
	}
	
	private final boolean taskHasExpired(TaskAnnounceMessage taskAnnounceMessage) {
		return CommonState.getTime() > taskAnnounceMessage.expirationTime;
	}

	private final void removeExpierdTasks(Node node, int protocolID) {
		LOGGER.trace("[{}] node={}", CommonState.getTime(), node);
		// We can remove the expired tasks as we go cause that we be modifing the list as we traverse it.
		List<TaskAnnounceMessage> tasksToRemove = new ArrayList<TaskAnnounceMessage>();
		for (TaskAnnounceMessage task : getTasks()) {
			if (taskHasExpired(task)) {
				tasksToRemove.add(task);
			}
		}
		if (tasksToRemove.size() > 0) {
			LOGGER.trace("[{}]~node={}, tasksToRemove={}", 
					CommonState.getTime(), node, tasksToRemove);
			tasksToRemove.forEach(task -> {
				tasks.remove(task.contractID);
			});
		}
	}
	
	private final void evaluateTasks(long bidEvaluationDelay, Node node, int protocolID) {
		LOGGER.trace("[{}] node={} delay={}", CommonState.getTime(), node, bidEvaluationDelay);	
		EDSimulator.add(bidEvaluationDelay, new Callback(() -> {
			LOGGER.trace("[{}] node={} slept for {}", CommonState.getTime(), node, bidEvaluationDelay);		
			
			// remove expired tasks....
			removeExpierdTasks(node, protocolID);
			if (haveTasks()) {
				TaskAnnounceMessage topTask = getTopTask(node, protocolID);
				int topTaskTimeout = (int)getTenderTimeout(topTask);
				LOGGER.trace("[{}]~node={}, topTask={}, topTaskTimeout={}", 
						CommonState.getTime(), node, topTask, topTaskTimeout);	
				if ((topTaskTimeout <= 0) && shouldBid(node, protocolID, topTask)) {
					bidForTask(node, protocolID, topTask);
				}
			}
			
		 }), node, protocolID);
	}

	private final TaskAnnounceMessage getTopTask(Node node, int protocolID) {
		// Sort all the tasks again.
		List<TaskAnnounceMessage> taskList = getTasks();
		Collections.sort(taskList, getTaskComparator(node, protocolID));
		LOGGER.trace("[{}]~After sort taskList={}", CommonState.getTime(), taskList);	
		return taskList.get(0);
	}

	protected Comparator<? super TaskAnnounceMessage> getTaskComparator(Node node, int protocolID) {
		return (TaskAnnounceMessage task1, TaskAnnounceMessage task2) -> { 
			return (int)(task2.expirationTime - task1.expirationTime); 
		};
	}

	/*
	 * Bids
	 */
	public final boolean isBidding() {
		boolean isBidding = (currentBid != null);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), isBidding);
		return isBidding;
	}
		
	protected boolean shouldBid(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) {
		return false;
	}

	protected final void bidForTask(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) {
		// Send a bid ...
		BidMessage bid = createBidMessage(node, protocolID, taskAnnounceMessage);
		sendBidMessage(node, protocolID, bid);
	
		// Expire the bid after (hence the +1) the TaskAnnounce is expired.
		long bidExpirationDelay = (taskAnnounceMessage.expirationTime + 1 - CommonState.getTime());
		expireCurrentBid(bidExpirationDelay, node, protocolID);
	}

	protected BidMessage createBidMessage(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) {
		BidMessage bidMessage = new BidMessage(taskAnnounceMessage.from, node, taskAnnounceMessage.contractID);
		LOGGER.trace("[{}]~={} node={} bidMessage={}", 
				CommonState.getTime(), taskAnnounceMessage.contractID, node, protocolID, bidMessage);		
		return bidMessage;
	}

	private final void sendBidMessage(Node node, int protocolID, BidMessage bidMessage) {
		currentBid = bidMessage;
		Transport transport = (Transport) node.getProtocol(FastConfig.getTransport(protocolID));
		transport.send(node, bidMessage.to, bidMessage, managerProtocolID);
		LOGGER.debug("[{}]~bidMessage={}", CommonState.getTime(), bidMessage);
	}

	private final void expireCurrentBid(long delay, Node node, int protocolID) {
		LOGGER.trace("[{}] node={}", CommonState.getTime(), node);
		EDSimulator.add(delay, new Callback(() -> {
			expireCurrentBid(node, protocolID);
		}), node, protocolID);
	}

	private final void expireCurrentBid(Node node, int protocolID) {
		if (currentBid != null) {
			LOGGER.debug("[{}]~contractID={}, node={}", 
					CommonState.getTime(), currentBid.contractID, node);
			currentBid = null;
		}
	}

	/*
	 * Awards
	 */
	public final boolean isBeingAwarded() {
		boolean isBeingAwarded = (currentAward != null);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), isBeingAwarded);
		return isBeingAwarded;
	}
	
	private final void handleAwardMessage(Node node, int protocolID, AwardMessage awardMessage) {
		LOGGER.debug("[{}]~contractID={}, node={} awardMessage={}", 
				CommonState.getTime(), awardMessage.contractID, node, awardMessage);
		
		assert (currentBid != null) : 
			this + " was not bidding but just won an award for " + awardMessage;
		assert (currentBid.contractID == awardMessage.contractID) : 
			this + " was bidding for " + currentBid + " but just won an award for " + awardMessage;
		
		currentAward = awardMessage;
		expireCurrentBid(node, protocolID);
		
		// Expire the bid when the TaskAnnounce is expired.
		long awardExpirationTime = (awardMessage.expirationTime) - CommonState.getTime();
		expireCurrentAward(awardExpirationTime, node, protocolID);

	}
	
	protected final void awarded(Node node, int protocolID, AwardMessage awardMessage) {
		assert (currentAward != null) : 
			this + " was not being awarded but just got awarded " + awardMessage;
		assert (currentAward.contractID == awardMessage.contractID) : 
			this + " was being awarded for " + currentAward + "but just got awarded " + awardMessage;

		expireCurrentAward(node, protocolID);
		
	}
	
	private final void expireCurrentAward(long delay, Node node, int protocolID) {
		LOGGER.trace("[{}] node={}", CommonState.getTime(), node);
		EDSimulator.add(delay, new Callback(() -> {
			expireCurrentAward(node, protocolID);
		}), node, protocolID);
	}
	
	private final void expireCurrentAward(Node node, int protocolID) {
		if (currentAward != null) {
			LOGGER.debug("[{}]~contractID={}, node={}", 
					CommonState.getTime(), currentAward.contractID, node);
			currentAward = null;
		}
	}
}
