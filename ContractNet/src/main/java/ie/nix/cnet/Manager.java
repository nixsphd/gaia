package ie.nix.cnet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.cnet.ContractNETProtocol.AwardMessage;
import ie.nix.cnet.ContractNETProtocol.BidMessage;
import ie.nix.cnet.ContractNETProtocol.Callback;
import ie.nix.cnet.ContractNETProtocol.GossipTaskAnnounceMessage;
import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Node;
import peersim.edsim.EDProtocol;
import peersim.edsim.EDSimulator;
import peersim.transport.Transport;

public class Manager implements Cloneable, CDProtocol, EDProtocol {

	private static final Logger LOGGER = LogManager.getLogger();

	private static final String STEP = "step";
	private static final String CONTRACTOR_PROTOCOL = "contractor";
	private static final String TENDER_TIMEOUT = "tender-timeout";
	private static final String GOSSIP_COUNTER = "gossip-counter";
	private static final String AWARD_TIMEOUT = "award-timeout";
	
	private static final int DEFAULT_TENDER_TIMEOUT = 10;
	private static final int DEFAULT_GOSSIP_COUNTER = 0;
	private static final int DEFAULT_AWARD_TIMEOUT = 0;

    protected final int contractorProtocolID;

	private final int step;	
    private final int tenderTimeout;
    private final int gossipCounter;
    private final int awardTimeout;
    
    private Map<Integer, TaskAnnounceMessage> taskAnnounces;		
    private Map<Integer, List<BidMessage>> bids;	
    private Map<Integer, AwardMessage> awards;
		
    public Manager(String prefix) {
		contractorProtocolID = Configuration.getPid(prefix + "." + CONTRACTOR_PROTOCOL);
		tenderTimeout = Configuration.getInt(prefix+"."+TENDER_TIMEOUT, DEFAULT_TENDER_TIMEOUT);
		awardTimeout = Configuration.getInt(prefix+"."+AWARD_TIMEOUT, DEFAULT_AWARD_TIMEOUT);
		step = Configuration.getInt(prefix+"."+STEP);
		gossipCounter = Configuration.getInt(prefix+"."+GOSSIP_COUNTER, DEFAULT_GOSSIP_COUNTER);
		taskAnnounces = new HashMap<Integer, TaskAnnounceMessage>();
		bids = new HashMap<Integer, List<BidMessage>>();
		awards = new HashMap<Integer, AwardMessage>();
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), this);
	}
	
	@Override
	public Manager clone() {
		Manager foo = null;
	    try {
			foo = (Manager)super.clone();
			foo.taskAnnounces = new HashMap<Integer, TaskAnnounceMessage>();
			foo.bids = new HashMap<Integer, List<BidMessage>>();
			foo.awards = new HashMap<Integer, AwardMessage>();
			LOGGER.trace("[{}]", CommonState.getTime());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	    return foo;
	}

	@Override
	public String toString() {
		return "Manager [taskAnnounces=" + taskAnnounces + "]";
	}

	protected final Stream<Node> getNeighbourNodes(Node node, int protocolID) {
		Linkable neighbourhood = (Linkable) node.getProtocol(FastConfig.getLinkable(protocolID));
		Builder<Node> builder = Stream.builder();
		for (int n = 0; n < neighbourhood.degree(); n++) {
			Node neighbourNode = (Node) neighbourhood.getNeighbor(n);
			// Failure handling
			if (neighbourNode.isUp()) {
				builder.add(neighbourNode);
			}
		}
		Stream<Node> neighbourNodes = builder.build();
		LOGGER.trace("[{}] host={} -> {}", CommonState.getTime(), node,neighbourNodes);
		return neighbourNodes;
		
	}

	public final int getStep() {
		return step;
	}

	@Override
	public final void nextCycle(Node node, int protocolID) {
		LOGGER.trace("[{}] host={}", CommonState.getTime(), node);
	
		while (shouldTender(node)) {
			// Build the Task Announcement
			int contractID = ContractNETProtocol.getNextContractID();
			
			LOGGER.trace("[{}]~contractID={}, host={}, shouldTender", 
					CommonState.getTime(), contractID, node);
	
			// Create an empty list to store the bids for the tender.
			bids.putIfAbsent(contractID, new ArrayList<BidMessage>());
			
			// Calculate the expiration time.
			long expirationTime = CommonState.getTime() + getTenderTimeout();
			TaskAnnounceMessage taskAnnounceMessage = 
					createTaskAnnouncementMessage(node, protocolID, null, contractID, expirationTime);
	
			// Archive the TaskAnnouncement message
			taskAnnounces.put(contractID, taskAnnounceMessage);
			
			// Send to all the neighbours
			sendTaskAnnouncementMessages(node, protocolID, taskAnnounceMessage);
	
			// After a delay, review the bids
			evaluateBids(getTenderTimeout(), node, protocolID, contractID);
		}
	
	}

	@Override
	public final void processEvent(Node node, int protocolID, Object event) {
		LOGGER.trace("[{}] host={}", CommonState.getTime(), node);
		if (event instanceof Callback) {
			((Callback) event).action.run();
	
		} else if (event instanceof BidMessage) {
			handleBidMessage(node, protocolID, (BidMessage) event);
		}
	
	}

	/*
	 * TaskAnnouncements
	 */
	public final boolean isTendering() {
		boolean isTendering = (taskAnnounces.size() > 0);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(),isTendering);
		return isTendering;
	}
	
	protected boolean shouldTender(Node node) {
		return false;
	}

	protected TaskAnnounceMessage createTaskAnnouncementMessage(Node node, int protocolID, Node neighbourNode,
			int contractID, long expirationTime) {
		TaskAnnounceMessage taskAnnounceMessage;
		if (gossipCounter == -1) {
			taskAnnounceMessage = new TaskAnnounceMessage(neighbourNode, node, contractID, expirationTime);
		} else {
			taskAnnounceMessage = new GossipTaskAnnounceMessage(neighbourNode, node, contractID, expirationTime,
					gossipCounter);
		}
		LOGGER.trace("[{}] host={} -> {}", CommonState.getTime(), node, taskAnnounceMessage);
		return taskAnnounceMessage;
	}
	
	public final int getTenderTimeout() {
		return tenderTimeout;
	}
	
	protected final int getGossipCounter() {
		return gossipCounter;
	}

	private final void sendTaskAnnouncementMessages(Node node, int protocolID,
			TaskAnnounceMessage taskAnnounceMessage) {
		Transport transport = (Transport) node.getProtocol(FastConfig.getTransport(protocolID));
		Stream<Node> neighbourNodes = getNeighbourNodes(node, protocolID);
		neighbourNodes.forEach(neighbourNode -> {
			TaskAnnounceMessage clonedTaskAnnounceMessage = taskAnnounceMessage.clone();
			clonedTaskAnnounceMessage.to = neighbourNode;
			transport.send(node, clonedTaskAnnounceMessage.to, clonedTaskAnnounceMessage, contractorProtocolID);
			LOGGER.debug("[{}]~host={}, sending {}", CommonState.getTime(), node,
					clonedTaskAnnounceMessage);
		});
	}

	protected final TaskAnnounceMessage getTender(int contractID) {
		LOGGER.trace("[{}]~contractID={}", CommonState.getTime(), contractID);
		return taskAnnounces.get(contractID);
	}
	
	private final void removeTender(int contractID) {
		LOGGER.trace("[{}]~contractID={}", CommonState.getTime(), contractID);
		bids.remove(contractID);
		taskAnnounces.remove(contractID);
	}

	/*
	 * Bids
	 */
	private final void handleBidMessage(Node node, int protocolID, BidMessage bidMessage) {
		LOGGER.trace("[{}]~contractID={}, host={}, bidMessage={}", 
				CommonState.getTime(), bidMessage.contractID, node, bidMessage);
		List<BidMessage> contractBids = bids.get(bidMessage.contractID);
		if (contractBids != null) {
			// Add the new bid
			contractBids.add(bidMessage);
		} else {
			LOGGER.debug("[{}]~Ignoring {}, not acception bids for contractID={}", 
					CommonState.getTime(), bidMessage, bidMessage.contractID);
		}
	}
	
	private final void awardBid(Node node, int protocolID, BidMessage bidMessage) {
		LOGGER.trace("[{}]~contractID={}, host={}, bidMessage={}", 
				CommonState.getTime(), bidMessage.contractID, node, bidMessage);
		
		// Calculate the expiration time.
		long expirationTime = CommonState.getTime() + getAwardTimeout();
		AwardMessage awardMessage = createAwardMessage(node, protocolID, bidMessage, expirationTime);
		
		awards.put(awardMessage.contractID, awardMessage);
		EDSimulator.add(getAwardTimeout(), new Callback(() -> {
			awards.remove(awardMessage.contractID);
		}), node, protocolID);
		
		sendAwardMessage(node, protocolID, awardMessage);
		award(node, protocolID, awardMessage);
		
	}

	private final void evaluateBids(long delay, Node node, int protocolID, int contractID) {
		EDSimulator.add(delay, new Callback(() -> {
			
			List<BidMessage> contractBids = bids.get(contractID);
			if (contractBids != null && contractBids.size() > 0) {
				
				// Sort all the bids again.
				Collections.sort(contractBids, getBidComparator(node, protocolID));
				BidMessage bestBid = contractBids.get(0);
				
				if (shouldAward(node, protocolID, bestBid)) {
					LOGGER.debug("[{}]~contractID={}, node={} shouldAward to {}", 
							CommonState.getTime(), contractID, node, contractBids.get(0).from.getID());
					awardBid(node, protocolID, bestBid);
				}
			} else {
				LOGGER.debug("[{}]~No bids for contractID={}", CommonState.getTime(), contractID);
			}
			
			removeTender(contractID);
	
		}), node, protocolID);
	}

	protected Comparator<BidMessage> getBidComparator(Node node, int protocolID) {
		LOGGER.trace("[{}] host={}", CommonState.getTime(), node);
		Comparator<BidMessage> bidComparator = (BidMessage bid1, BidMessage bid2) -> {
			return bid2.contractID - bid1.contractID;
		};
		return bidComparator;
	}

	/*
	 * Awards
	 */
	public boolean isAwarding() {
		boolean isAwarding = (awards.size() > 0);
		LOGGER.trace("[{}] -> {}", CommonState.getTime(), isAwarding);
		return isAwarding;
	}
	
	public final int getAwardTimeout() {
		return awardTimeout;
	}
	
	protected boolean shouldAward(Node node, int protocolID, BidMessage bestBid) {
		return false;
	}
	
	protected AwardMessage createAwardMessage(Node node, int protocolID, BidMessage bidMessage, long expirationTime) {
		AwardMessage awardMessage = new AwardMessage(bidMessage.from, node, bidMessage.contractID, expirationTime);
		LOGGER.trace("[{}]~contractID={}, host={} -> {}", 
				CommonState.getTime(), bidMessage.contractID, node, awardMessage);
		return awardMessage;
	}
	
	private final void sendAwardMessage(Node node, int protocolID, AwardMessage awardMessage) {
		Transport transport = (Transport) node.getProtocol(FastConfig.getTransport(protocolID));
		transport.send(node, awardMessage.to, awardMessage, contractorProtocolID);
		LOGGER.debug("[{}]~contractID={}, host={} sending {}", 
				CommonState.getTime(), awardMessage.contractID, node, awardMessage);
	}

	protected void award(Node node, int protocolID, AwardMessage awardMessage) {
        LOGGER.debug("[{}]~contractID={}, host={}, awardMessage={}", 
        		CommonState.getTime(), awardMessage.contractID, node, awardMessage);
	}

}
