package ie.nix.cnet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.GeneralNode;
import peersim.core.Node;

public class ContractNETProtocol { 

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class Callback {
		final Runnable action;

		public Callback(Runnable action) {
			this.action = action;
			LOGGER.trace("[{}]", CommonState.getTime());
		}

		@Override
		public String toString() {
			return "Callback";
		}

	}

	public static class TaskAnnounceMessage implements Cloneable {

		public Node to;
		public final Node from;
		public final int contractID;
		public final long expirationTime;

		public TaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime) {
			this.to = to;
			this.from = from;
			this.contractID = contractID;
			this.expirationTime = expirationTime;

			LOGGER.trace("[{}]", CommonState.getTime());
		}
		
		@Override
		public TaskAnnounceMessage clone() {
		    try {
		    	final TaskAnnounceMessage foo = (TaskAnnounceMessage)super.clone();
				LOGGER.trace("[{}]~clone={}", 
						CommonState.getTime(), foo);
				return foo;
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		    return null;
		}
		
		public String toString() {
			return "TaskAnnounceMessage " 
					+ from.getIndex() + "->" + (to != null? to.getIndex() : "null") 
					+ ", contractID=" + contractID 
					+ ", expirationTime=" + expirationTime;
		}
		
	}
	
	public static class GossipTaskAnnounceMessage extends TaskAnnounceMessage {

		protected int counter = 0;
		
		public GossipTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime, int counter) {
			super(to, from, contractID, expirationTime);
			this.counter = counter;
			LOGGER.trace("[{}] -> {}", 
					CommonState.getTime(), this);
		}

		@Override
		public GossipTaskAnnounceMessage clone() {
	    	final GossipTaskAnnounceMessage foo = (GossipTaskAnnounceMessage)super.clone();
			LOGGER.trace("[{}]~clone={}", 
					CommonState.getTime(), foo);
			return foo;
		}
		
		public String toString() {
			return "GossipTaskAnnounceMessage " 
					+ from.getIndex() + "->" + (to != null? to.getIndex() : "null") 
					+ ", contractID=" + contractID + ", expirationTime=" + expirationTime 
					+ ", counter=" + counter+ ")";
		}
		
		public int getCounter() {
			return counter;
		}
		
		public int decrementCounter() {
			return counter -= 1;
		}
	
	}

	public static class BidMessage implements Cloneable {

		public final Node to;
		public final Node from;
		public final int contractID;
		// TODO - Add an bid expiration time too, which the GE might take great advantage of.
		
		public BidMessage(Node to, Node from, int contractID) {
			this.to = to;
			this.from = from;
			this.contractID = contractID;
			LOGGER.trace("[{}]~{}", 
					CommonState.getTime(), this);
		}
		
		@Override
		public BidMessage clone() {
		    try {
		    	final BidMessage foo = (BidMessage)super.clone();
				LOGGER.trace("[{}]~clone={}", 
						CommonState.getTime(), foo);
				return foo;
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		    return null;
		}
		
		public String toString() {
			return "BidMessage " + from.getIndex() + "->" + to.getIndex() + ", contractID=" + contractID + ")";
		}
	}

	public static class AwardMessage implements Cloneable {
		
		public final Node to;
		public final Node from;
		public final int contractID;
		public final long expirationTime;

		public AwardMessage(Node to, Node from, int contractID, long expirationTime) {
			this.to = to;
			this.from = from;
			this.contractID = contractID;
			this.expirationTime = expirationTime;
			LOGGER.trace("[{}]~{}", CommonState.getTime(), this);
		}
		
		@Override
		public AwardMessage clone() {
		    try {
	    		final AwardMessage foo = (AwardMessage)super.clone();
				LOGGER.trace("[{}]~clone={}", 
						CommonState.getTime(), foo);
				return foo;
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		    return null;
		}
		
		public String toString() {
			return "AwardMessage " + from.getIndex() + "->" + to.getIndex() + ", contractID=" + contractID+")";
		}
	}
	
	public static class DualRoleNode extends GeneralNode {
		
		private static final String MANAGER_PROTOCOL = "manager";
		private static final String CONTRACTOR_PROTOCOL = "contractor";
		
		private final int managerProtocolID;
		private final int contractorProtocolID;	

		public DualRoleNode(String prefix) {
			super(prefix);
			managerProtocolID = Configuration.getPid(prefix + "." + MANAGER_PROTOCOL);
			contractorProtocolID = Configuration.getPid(prefix + "." + CONTRACTOR_PROTOCOL);
			LOGGER.trace("[{}]~{}", CommonState.getTime(), this);

		}

		protected int getManagerProtocolID() {
			return managerProtocolID;
		}

		protected Manager getManager() {
			return (Manager)getProtocol(managerProtocolID);
		}

		protected int getContractorProtocolID() {
			return contractorProtocolID;
		}

		protected Contractor getContractor() {
			return (Contractor)getProtocol(contractorProtocolID);
		}
		
		public boolean isContractorBidding() {
			return getContractor().isBidding();
		}
		
		public boolean isManagerTendering() {
			return getManager().isTendering();
		}

	}

    private static int nextContractID = 1000;
    
	protected static int getNextContractID() {
		int contractID = nextContractID++;
		return contractID;
	}

}