package ie.nix.gc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDSimulator;

public class AgentTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	int random_seed = 1;
	int neighbourhoodSize = 2;
	int agentProtocol = 0;

	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] { 
				"src/test/java/ie/nix/gc/AgentTest.properties" });
		
	}

	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to " + random_seed);
	}

	@AfterEach
	void afterEach() {
		TestAgent.clearAll();
		System.out.println();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}

	/*
	 * Helper methods
	 */
	protected TestNode getNode(int n) {
		TestNode testNode = (TestNode)Network.get(n);
		LOGGER.trace("[{}]~testNode={}", CommonState.getTime(), testNode);
		return testNode;
	}
	
	protected TestAgent getAgent(Node node) {
		TestAgent testAgent = ((TestAgent)node.getProtocol(agentProtocol));
		LOGGER.trace("[{}]~testAgent={}", CommonState.getTime(), testAgent);
		return testAgent;
	}
	
	protected TestAgent getAgent(int n) {
		TestAgent testAgent = ((TestAgent)getNode(n).getProtocol(agentProtocol));
		LOGGER.trace("[{}]~testAgent={}", CommonState.getTime(), testAgent);
		return testAgent;
	}
	
	protected Stream<TestAgent> getAgents() {
        Stream.Builder<TestAgent> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(getAgent(n));;
		}
		return streamBuilder.build();
	}
	
	/*
	 * Send Tenders
	 */

	@Test
	@DisplayName("One Should Tender")
	public void oneShouldTenderTest() {
		shouldTenderTest(1);

	}

	@Test
	@DisplayName("Two Should Tender")
    public void twoShouldTenderTest() {
		shouldTenderTest(2);
	
	}

	protected void shouldTenderTest(int numberToTender) {
		shouldTenderOnce(numberToTender);
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfTenderersSent() == numberToTender,
				"Tenderers should be " + numberToTender + ", but is " + getNumberOfTenderersSent() + ".");
	}

	protected void shouldTenderOnce(int numberToTender) {
		
		
		StrategyAgent.shouldTender = (Node node) -> {
			boolean should = 
					!getAgent(node).isTendering() && (node.getIndex() % (Network.size() / numberToTender) == 0);
			LOGGER.trace("StrategyAgent.shouldTender[{}]~node={}, should={}", 
					CommonState.getTime(), node, should);
			return should;
		};
	}

	protected int getNumberOfTenderersSent() {
		int tenderSent = 0;
		for (int n = 0; n < Network.size(); n++) {
			tenderSent += getAgent(n).tendersSent;
		}
		LOGGER.debug("[{}]~tenderSent={}", CommonState.getTime(), tenderSent);
		return tenderSent;
	}

	/*
	 * Tenders Received
	 */
	@Test
	@DisplayName("One Tenders Recieved")
	public void oneTendersRecievedTest() {
		int numberToTender = 1;
		tendersRecievedTest(numberToTender, numberToTender * neighbourhoodSize);
	
	}

	@Test
	@DisplayName("Two Tenders Recieved")
	public void twoTendersRecievedTest() {
		int numberToTender = 2;
		tendersRecievedTest(numberToTender, numberToTender * neighbourhoodSize);	
	}

	protected void tendersRecievedTest(int numberToTender, int numberOfTendersToBeRecieved) {
		shouldTenderOnce(numberToTender);
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfTendersRecieved() == numberOfTendersToBeRecieved,
				"Tenders recieved id should be " + numberOfTendersToBeRecieved + ", but is "
						+ getNumberOfTendersRecieved() + ".");
	}
	
	protected int getNumberOfTendersRecieved() {
		int tendersRecieved = 0;
		for (int n = 0; n < Network.size(); n++) {
			tendersRecieved += getAgent(n).tendersRecieved;
		}
		return tendersRecieved;
	}
	
	/*
	 * Should Bid
	 */
	@Test
	@DisplayName("One Should Bid")
	public void oneShouldBidTest() {
		int numberToTender = Network.size();
		int numberToBid = 1;
		oneShouldBidTest(numberToTender, numberToBid);
		
	}

	protected void oneShouldBidTest(int numberToTender, int numberToBid) {
		shouldTenderOnce(numberToTender);
		
		oneShouldBidOnce();
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfBidsSent() == numberToBid,
				"Bid sent should be " + numberToBid + ", but is " + getNumberOfBidsSent() + ".");
		
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void oneShouldBidOnce() {
	
		StrategyAgent.preTender = (Node node) -> {
			ArrayList<Integer> toBidders = new ArrayList<Integer>();
			toBidders.add(0);
			getAgents().forEach(agent -> {
				agent.addTask(toBidders);
			});
		};
		
		StrategyAgent.shouldBid = (TenderMessage tenderMessage) -> {
			ArrayList<Integer> bidders = (ArrayList<Integer>)tenderMessage.task;
			boolean should = bidders.contains(tenderMessage.to.getIndex());
			LOGGER.debug("StrategyAgent.shouldBid[{}]~tenderMessage={}, should={}", 
					CommonState.getTime(), tenderMessage, should);
			return should;
		};
	
	}
	
	protected int getNumberOfBidsSent() {
		int bidsSent = 0;
		for (int n = 0; n < Network.size(); n++) {
			bidsSent += getAgent(n).bidsSent;
		}
		return bidsSent;
		
	}
	

	@Test
	@DisplayName("Two Should Bid")
	public void twoShouldBidTest() {
		int numberToTender = 1;
		shouldBidTest(numberToTender, numberToTender * neighbourhoodSize);
		
	}

	@Test
	@DisplayName("Four Should Bid")
	public void fourShouldBidTest() {
		
		CommonState.initializeRandom(16);
		
		int numberToTender = 2;
		shouldBidTest(numberToTender, numberToTender * neighbourhoodSize);
		
	}

	protected void shouldBidTest(int numberToTender, int numberToBid) {
		shouldTenderOnce(numberToTender);
		
		allShouldBidOnce();
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfBidders() == numberToBid,
				"Bidders should be " + numberToBid + ", but is " + getNumberOfBidders() + ".");
	}

	@SuppressWarnings("rawtypes")
	protected void allShouldBidOnce() {
		StrategyAgent.shouldBid = (TenderMessage tenderMessage) -> {
			TestNode testNode = (TestNode)tenderMessage.to;
			TestAgent testAgent = (TestAgent)testNode.getProtocol(agentProtocol);
			boolean should = !testAgent.isBidding() && !testAgent.isTendering();
			LOGGER.debug("StrategyAgent.shouldBid[{}]!should={}", 
					CommonState.getTime(), should);
			return should;
		};
	}
	
	protected int getNumberOfBidders() {
		int bidders = 0;
		for (int n = 0; n < Network.size(); n++) {
			if (getAgent(n).bidsSent > 0) {
				bidders++;
			}
		}
		return bidders;
	}

	/*
	 * Award Sent
	 */
	@Test
	@DisplayName("One Awarded")
	public void oneAwarded() {
		int numberToTender = 1;
		int numberToAward = 1;
		awardTest(numberToTender, numberToAward);
		
	}

	@Test
	@DisplayName("Two Awarded")
	public void twoAwarded() {
		int numberToTender = 2;
		awardTest(numberToTender, numberToTender);
		
	}

	protected void awardTest(int numberToTender, int numberToAward) {
		shouldTenderOnce(numberToTender);
		allShouldBidOnce();
		awardOne();
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfAwardsSent() == numberToAward,
				"Awards should be " + numberToAward + ", but is " + getNumberOfAwardsSent() + ".");
	}

	@SuppressWarnings("rawtypes")
	protected void awardOne() {
		StrategyAgent.getBidComparator = () ->{
			Comparator<BidMessage> bidComparator = (BidMessage bid1, BidMessage bid2) -> {
				return bid2.from.getIndex() - bid1.from.getIndex();
			};
			return bidComparator;
		};
		StrategyAgent.shouldAward = (BidMessage bidMessage) -> {
			return true;
		};
	}
	
	protected int getNumberOfAwardsSent() {
		int awards = 0;
		for (int n = 0; n < Network.size(); n++) {
			if (getAgent(n).awardsSent > 0) {
				awards++;
			}
		}
		return awards;
	}
	
	/*
	 * Award Recieved
	 */
	@Test
	@DisplayName("One Award Recieved")
	public void oneAwardRecievedTest() {
		int numberToTender = 1;
		int numberOfAwardsToBeRecieved = 1;
		awardRecievedTest(numberToTender, numberOfAwardsToBeRecieved);	
	}

	@Test
	@DisplayName("Two Award Recieved")
	public void twoAwardRecievedTest() {
		int numberToTender = 2;
		awardRecievedTest(numberToTender, numberToTender);	
	}

	protected void awardRecievedTest(int numberToTender, int numberOfAwardsToBeRecieved) {
		shouldTenderOnce(numberToTender);
		allShouldBidOnce();
		awardOne();
		
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfAwardsRecieved() == numberOfAwardsToBeRecieved,
				"Awards recieved id should be " + numberOfAwardsToBeRecieved + ", but is "
						+ getNumberOfAwardsRecieved() + ".");
	}
	
	protected int getNumberOfAwardsRecieved() {
		int awardsRecieved = 0;
		for (int n = 0; n < Network.size(); n++) {
			awardsRecieved += getAgent(n).awardsRecieved;
		}
		return awardsRecieved;
	}
	
	/*
	 * Tender Comparator
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@DisplayName("Tender Message Expiring Last")
	public void tenderMessageExpiringLastTest() {
		
		TenderMessage tm1 = new TenderMessage(null, null, 50, null, 0d, 0);
		TenderMessage tm2 = new TenderMessage(null, null, 1, null, 0d, 0);
		TenderMessage tm3 = new TenderMessage(null, null, 10, null, 0d, 0);
		
		List<TenderMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(tm1);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm2);

		Comparator<TenderMessage> comparator = TenderMessage.expiringLast;
		
		tenderMessageComparatorTest(actualTenderMessages, expectedTenderMessages, comparator);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@DisplayName("Tender Message Expiring First")
	public void tenderMessageExpiringFirstTest() {
		
		TenderMessage tm1 = new TenderMessage(null, null, 50, null, 0d, 0);
		TenderMessage tm2 = new TenderMessage(null, null, 1, null, 0d, 0);
		TenderMessage tm3 = new TenderMessage(null, null, 10, null, 0d, 0);
		
		List<TenderMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(tm2);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm1);
		
		Comparator<TenderMessage> comparator = TenderMessage.expiringFirst;
		
		tenderMessageComparatorTest(actualTenderMessages, expectedTenderMessages, comparator);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@DisplayName("Tender Message Highest Guide")
	public void tenderMessageHighestGuideTest() {
		
		TenderMessage tm1 = new TenderMessage(null, null, 50, null, 1.0, 0);
		TenderMessage tm2 = new TenderMessage(null, null, 1, null, 5.0, 0);
		TenderMessage tm3 = new TenderMessage(null, null, 10, null, 2.0, 0);
		
		List<TenderMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(tm2);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm1);
		
		Comparator<TenderMessage> comparator = TenderMessage.highestGuide;
		
		tenderMessageComparatorTest(actualTenderMessages, expectedTenderMessages, comparator);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@DisplayName("Tender Message Lowest Guide")
	public void tenderMessageLowestGuideTest() {
		
		TenderMessage tm1 = new TenderMessage(null, null, 50, null, 1.0, 0);
		TenderMessage tm2 = new TenderMessage(null, null, 1, null, 5.0, 0);
		TenderMessage tm3 = new TenderMessage(null, null, 10, null, 2.0, 0);
		
		List<TenderMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(tm1);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm2);
		
		Comparator<TenderMessage> comparator = TenderMessage.lowestGuide;
		
		tenderMessageComparatorTest(actualTenderMessages, expectedTenderMessages, comparator);
	}

	@SuppressWarnings("rawtypes")
	public void tenderMessageComparatorTest(List<TenderMessage> actualTenderMessages, List<TenderMessage> expectedTenderMessages,
			Comparator<TenderMessage> comparator) {
		Collections.sort(actualTenderMessages, comparator);
		
		LOGGER.debug("[{}]~expectedTenderMessages={}", CommonState.getTime(), expectedTenderMessages);
		LOGGER.debug("[{}]~actualTenderMessages={}", CommonState.getTime(), actualTenderMessages);

		Assertions.assertEquals(expectedTenderMessages, actualTenderMessages);
	}
	
	/*
	 * Bid Comparator
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@DisplayName("Bid Message Expiring Last")
	public void bidMessageExpiringLastTest() {

		TenderMessage tm = new TenderMessage(null, null, 20, null, 0d, 0);
		BidMessage bm1 = new BidMessage(tm, 50, null, 0d);
		BidMessage bm2 = new BidMessage(tm, 1, null, 0d);
		BidMessage bm3 = new BidMessage(tm, 10, null, 0d);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm1);
		expectedTenderMessages.add(bm3);
		expectedTenderMessages.add(bm2);

		Comparator<BidMessage> comparator = BidMessage.expiringLast;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, comparator);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@DisplayName("Bid Message Expiring First")
	public void bidMessageExpiringFirstTest() {
		
		TenderMessage tm = new TenderMessage(null, null, 20, null, 0d, 0);
		BidMessage bm1 = new BidMessage(tm, 50, null, 0d);
		BidMessage bm2 = new BidMessage(tm, 1, null, 0d);
		BidMessage bm3 = new BidMessage(tm, 10, null, 0d);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm2);
		expectedTenderMessages.add(bm3);
		expectedTenderMessages.add(bm1);

		Comparator<BidMessage> comparator = BidMessage.expiringFirst;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, comparator);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@DisplayName("Bid Message Highest Offer")
	public void bidMessageHighestOfferTest() {

		TenderMessage tm = new TenderMessage(null, null, 20, null, 0d, 0);
		BidMessage bm1 = new BidMessage(tm, 50, 2.0);
		BidMessage bm2 = new BidMessage(tm, 1, 4.0);
		BidMessage bm3 = new BidMessage(tm, 10, 1.0);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm2);
		expectedTenderMessages.add(bm1);
		expectedTenderMessages.add(bm3);

		Comparator<BidMessage> comparator = BidMessage.highestOffer;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, comparator);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@DisplayName("Bid Message Lowest Offer")
	public void bidMessageLowestOfferTest() {

		TenderMessage tm = new TenderMessage(null, null, 20, null, 0d, 0);
		BidMessage bm1 = new BidMessage(tm, 50, 2.0);
		BidMessage bm2 = new BidMessage(tm, 1, 4.0);
		BidMessage bm3 = new BidMessage(tm, 10, 1.0);
		
		List<BidMessage> actualTenderMessages = new ArrayList();
		actualTenderMessages.add(bm1);
		actualTenderMessages.add(bm2);
		actualTenderMessages.add(bm3);
		
		List<BidMessage> expectedTenderMessages = new ArrayList();
		expectedTenderMessages.add(bm3);
		expectedTenderMessages.add(bm1);
		expectedTenderMessages.add(bm2);

		Comparator<BidMessage> comparator = BidMessage.lowestOffer;
		
		bidMessageComparatorTest(actualTenderMessages, expectedTenderMessages, comparator);
	}

	@SuppressWarnings("rawtypes")
	public void bidMessageComparatorTest(List<BidMessage> actualBidMessages, List<BidMessage> expectedBidMessages,
			Comparator<BidMessage> comparator) {
		
		Collections.sort(actualBidMessages, comparator);
		
		LOGGER.debug("[{}]~expectedBidMessages={}", CommonState.getTime(), expectedBidMessages);
		LOGGER.debug("[{}]~actualBidMessages={}", CommonState.getTime(), actualBidMessages);

		Assertions.assertEquals(expectedBidMessages, actualBidMessages);
	}
}