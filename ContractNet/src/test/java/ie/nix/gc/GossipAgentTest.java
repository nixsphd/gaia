package ie.nix.gc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import peersim.core.CommonState;

public class GossipAgentTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private AgentTest helper;
	static int N = 10000;
	static int K = 5;
	static int tenderGossipCount = 1;
	static int tenderGossipNeighbours = 2;
	
	@BeforeAll
	static void beforeAll() {
		// Redirect std err to files.
//		try {
//			File errFile = new File("err.txt");
//			FileOutputStream errFOS = new FileOutputStream(errFile);
//			PrintStream errPS = new PrintStream(errFOS);
//			System.setErr(errPS);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/java/ie/nix/gc/AgentTest.properties", 
				"network.size="+N,
				"protocol.agent.tender-gossip-neighbours="+tenderGossipNeighbours,
				"protocol.agent.tender-gossip-counter="+tenderGossipCount,
				"init.rnd.k="+K,
				});
		
	}

	@BeforeEach
	void beforeEach() {
		helper = new AgentTest();
		helper.beforeEach();
	}

	@AfterEach
	void afterEach() {
		helper.afterEach();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	/*
	 * Tenders Sent
	 */
	@Test
    @DisplayName("One Should Tender")
    public void oneShouldTenderTest() {
		helper.shouldTenderTest(1);
    
    }
	
	@Test
	@DisplayName("Two Should Tender")
    public void twoShouldTenderTest() {
		helper.shouldTenderTest(2);
	
	}
	
	/*
	 * Tenders Recieved
	 */
	@Test
	@DisplayName("One Tenderer Tenders Recieved")
	public void oneTendersRecievedTest() {
		int numberToTender = 1;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.tendersRecievedTest(numberToTender, expectedTenders);
	
	}

	@Test
	@DisplayName("Two Tenderers Tenders Recieved")
	public void twoTendersRecievedTest() {
		int numberToTender = 2;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.tendersRecievedTest(numberToTender, expectedTenders);	
	}

	protected int calculateExpectedTenders(int numberToTender) {
		int tendersPerTestNode = 0;
		for (int l = tenderGossipCount+1; l > 0; l--) {
			tendersPerTestNode += Math.pow(tenderGossipNeighbours, l);
		}
		LOGGER.info("[{}]~networkSize={}, tenderGossipCount={}, tendersPerTestNode={}", 
				CommonState.getTime(), tenderGossipNeighbours, tenderGossipCount, tendersPerTestNode);
		return numberToTender * tendersPerTestNode;
	}
	
	@Test
	@DisplayName("Two Should Bid")
	public void twoShouldBidTest() {
		int numberToTender = 1;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.shouldBidTest(numberToTender, expectedTenders);
		
	}
	
	@Test
	@DisplayName("Four Should Bid")
	public void fourShouldBidTest() {
		int numberToTender = 2;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.shouldBidTest(numberToTender, expectedTenders);
		
	}

}