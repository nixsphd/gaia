package ie.nix.gc;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import peersim.core.CommonState;
import peersim.edsim.EDSimulator;

public class GossipSnapTest {
	
	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/java/ie/nix/gc/SnapTest.properties",
				"protocol.agent.tender-timeout="+tenderTimeout,
				"protocol.agent.tender-gossip-neighbours="+gossipNeighbours,
				"network.size="+N, 
				"init.rnd.k="+K,
				"protocol.agent.tender-gossip-counter="+gossipCounter});
		
	}

	// Number of Nodes
	private static int N = 200;
	// Size of Neignbourhood
	private static int K = 80;
	private static int gossipNeighbours = 8;
	private static int tenderTimeout = 5;
	private static int gossipCounter = 5;
	
	SnapTest helper;
	
	@BeforeEach
	void beforeEach() {
		helper = new SnapTest();
		helper.beforeEach();
	}

	@AfterEach
	void afterEach() {
		helper.afterEach();
	}
	
	@Test
    @DisplayName("GC Gossip Snap Test")
    public void gossipSnapTest() {

		CommonState.initializeRandom(0);
		
		helper.initCards();
		helper.initObservers();
		
		helper.initPreTender();
		
		helper.initShouldTender();
		
		helper.initTenderComparator();
		helper.initShouldBid();
		helper.initGetProposal();
		
		helper.initGetBidComparator();
		helper.initShouldAward();
		helper.initAward();
		helper.initAwarded();
		
		EDSimulator.nextExperiment();
		
		int numberOfSnaps = 200;
		Assertions.assertTrue(helper.getNumberOfSnaps() == numberOfSnaps,
				"Snaps should be " + numberOfSnaps + ", but is "+ helper.getNumberOfSnaps() + ".");
		
	}
}
