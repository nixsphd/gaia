package ie.nix.gc;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;
import peersim.core.Node;

public class TestAgent extends StrategyAgent<ArrayList<Integer>, Object> {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public int tendersSent = 0;		
	public int tendersRecieved = 0;
	public int bidsSent = 0;
	public int bidsRecieved = 0;
	public int awardsRecieved = 0;
	public int awardsSent = 0;
	
	public TestAgent(String prefix) {
		super(prefix);
		LOGGER.trace("[{}]~new TestAgent", CommonState.getTime()); 
	}

	@Override
	public String toString() {
		return "TestAgent["
			+ "tendersSent="+tendersSent+", "
			+ "tendersRecieved="+tendersRecieved+", "
			+ "bidsSent="+bidsSent+", "
			+ "bidsRecieved="+bidsRecieved+", "
			+ "awardsSent="+awardsSent+", "
			+ "awardsRecieved="+awardsRecieved+"]";
	}
	
	@Override
	public void processEvent(Node node, int protocolID, Object event) {
		if (event instanceof TenderMessage) {
			tendersRecieved++;
			LOGGER.info("[{}]~agent={}, event={}", 
					CommonState.getTime(), this, event);
		
		} else if (event instanceof BidMessage) {
			bidsRecieved++;
			LOGGER.info("[{}]~agent={}, event={}", 
					CommonState.getTime(), this, event);
		
		} else if (event instanceof AwardMessage) {
			awardsRecieved++;
			LOGGER.info("[{}]~agent={}, event={}", 
					CommonState.getTime(), this, event);
		
		}
		super.processEvent(node, protocolID, event);
	}
	
	@Override
	protected TenderMessage<ArrayList<Integer>> createTenderMessage(Node node, ArrayList<Integer> task) {
		TenderMessage<ArrayList<Integer>> tenderMessage = super.createTenderMessage(node, task);
		tendersSent++;
		LOGGER.trace("[{}]~tenderMessage={}, tendersSent={}", 
				CommonState.getTime(), tenderMessage, tendersSent);
		return tenderMessage;
	}

	@Override
	protected BidMessage<ArrayList<Integer>, Object> createBidMessage(TenderMessage<ArrayList<Integer>> tenderMessage) {
		BidMessage<ArrayList<Integer>,Object> bidMessage = super.createBidMessage(tenderMessage);
		bidsSent++;
		LOGGER.trace("[{}]~tenderMessage={}, bidMessage={},  bidsSent={}", 
				CommonState.getTime(), tenderMessage, bidMessage, bidsSent);
		return bidMessage;
	}

	@Override
	protected AwardMessage<ArrayList<Integer>,Object> createAwardMessage(BidMessage<ArrayList<Integer>,Object> bidMessage) {
		AwardMessage<ArrayList<Integer>,Object> awardMessage = super.createAwardMessage(bidMessage);
		awardsSent++;
		LOGGER.info("[{}]~bidMessage={}, awardMessage={}, tendersSent={}", 
				CommonState.getTime(), bidMessage, awardMessage, awardsSent);
		return awardMessage;
	}

}
