package ie.nix.gc;

import peersim.core.Control;
import peersim.core.GeneralNode;

public class TestNode extends GeneralNode {

	public static class Init implements Control {

		public static Control control;

		public Init(String prefix) {}

		public boolean execute() {
			if (control != null) {
				return control.execute();
			} else {
				return false;
			}
		}
	}

	public static class Observer implements Control {

		public static Control control;

		public Observer(String prefix) {}

		public boolean execute() {
			if (control != null) {
				return control.execute();
			} else {
				return false;
			}
		}

	}
	
	public TestNode(String prefix) {
		super(prefix);
	}

	@Override
	public String toString() {
		return "TestNode["+getIndex()+"]";
	}
	
}