package ie.nix.gc;

import java.util.Comparator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDSimulator;

public class SnapTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class Snap extends TestNode {

		private int myCard;
		private int swapableCard;
		
		public Snap(String prefix) {
			super(prefix);
		}
		
		public boolean snapped() {
			return (myCard == swapableCard);
		}
		
		public void swapCard(int swapableCard) {
			this.swapableCard = swapableCard;
			if (this.myCard == this.swapableCard) {
				setFailState(DOWN);
			}
		}

		public int getMyCard() {
			return myCard;
		}

		public int getSwapableCard() {
			return swapableCard;
		}
		
		public String toString() {
			return "Snap["+myCard+", "+swapableCard+"]";
		}
		
	}
	
	public static class SnapAgent extends StrategyAgent<Integer, Integer> {

		public SnapAgent(String prefix) {
			super(prefix);
		}

	}

	int random_seed = 4;

	int agentProtocol = 0;
	
	@BeforeAll
	static void beforeAll() {
		// Redirect std err to files.
//		try {
//			File errFile = new File("err.txt");
//			FileOutputStream errFOS = new FileOutputStream(errFile);
//			PrintStream errPS = new PrintStream(errFOS);
//			System.setErr(errPS);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {"src/test/java/ie/nix/gc/SnapTest.properties"});
		
	}

	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to "+random_seed);
	}

	@AfterEach
	void afterEach() {
		StrategyAgent.clearAll();
		System.out.println();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	@Test
    @DisplayName("Snap Test")
    public void snapTest() {
		
		CommonState.initializeRandom(14);
		
		initCards();
		initObservers();
		
		initPreTender();
		
		initShouldTender();
		
		initTenderComparator();
		initShouldBid();
		initGetProposal();
		
		initGetBidComparator();
		initShouldAward();
		initAward();
		initAwarded();
		
		EDSimulator.nextExperiment();
		
		int numberOfSnaps = 20;
		Assertions.assertTrue(getNumberOfSnaps() == numberOfSnaps,
				"Snaps should be " + numberOfSnaps + ", but is "+ getNumberOfSnaps() + ".");
		
    }

	protected void initCards() {
		Snap.Init.control = () -> {
			System.out.println("Init["+CommonState.getTime()+"]~Network.size()="+Network.size());
			for (int n = 0; n < Network.size(); n++) {
				Snap snap = (Snap)Network.get(n);
				snap.myCard = n;
				snap.swapableCard = (Network.size() -1) - n;
			}
			return false;
	    };
	}
	
	protected void initObservers() {
		Snap.Observer.control = () -> {
			System.out.println("Observer[" + CommonState.getTime() + "]");
			int snaps = 0;
			int distance = 0;
			StringBuffer idsString =          new StringBuffer("ids         =[");
			StringBuffer myCardString =       new StringBuffer("myCards     =[");
			StringBuffer swapableCardString = new StringBuffer("swapableCard=[");
			for (int n = 0; n < Network.size(); n++) {
				Snap snap = (Snap) Network.get(n);
				if (snap.snapped()) {
					snaps++;
				}
				distance +=  Math.abs(snap.myCard - snap.swapableCard);
				idsString.append(String.format("%2d", snap.getIndex())+",");
				myCardString.append(String.format("%2d", snap.myCard)+",");
				swapableCardString.append(String.format("%2d", snap.swapableCard)+",");
			}
			idsString.append("]");
			myCardString.append("]");
			swapableCardString.append("]");
			System.out.println("Observer[" + CommonState.getTime() + "]Snap.SnapObserver.execute()~snaps=" + snaps
					+ " out of " + Network.size()+ ", distance="+distance);
			System.out.println(idsString);
			System.out.println(myCardString);
			System.out.println(swapableCardString);
			return false;
		};
	}

	protected void initPreTender() {
		StrategyAgent.preTender = (Node node) -> {
			Snap snap = (Snap)node;
			SnapAgent snapAgent = (SnapAgent)snap.getProtocol(agentProtocol);
			snapAgent.clearTasks();
			snapAgent.addTask(snap.swapableCard);
		};
	}
	
	protected void initShouldTender() {
		StrategyAgent.shouldTender = (Node node) -> {
			Snap snap = (Snap) node;
			SnapAgent snapAgent = (SnapAgent)node.getProtocol(agentProtocol);
			boolean shouldTender = !snap.snapped() && snapAgent.isIdle();
			LOGGER.debug("StrategyAgent.shouldBid[{}]~node={}, shouldTender={}, snap.snapped()={}, "
					+ "snapAgent.isIdle()={}", 
					CommonState.getTime(), snap, shouldTender, snap.snapped(), snapAgent.isIdle());
			return shouldTender;
		};
	}
	
	protected void initTenderComparator() {
		StrategyAgent.getTenderComparator = () -> {
			Comparator<TenderMessage<Integer>> taskComparator = 
					(TenderMessage<Integer> tender1, TenderMessage<Integer> tender2) -> { 
				Snap mySnapNode = (Snap)tender1.to;
				int card1 = tender1.task;
				int card2 = tender2.task;
				int diff1 = Math.abs(card1 - mySnapNode.myCard);
				int diff2 = Math.abs(card2 - mySnapNode.myCard);
				return diff1 - diff2; 
			};
			return taskComparator;
		};
	}
	
	protected void initShouldBid() {
		StrategyAgent.shouldBid =  (tenderMessage) -> {
			Snap snap = (Snap)tenderMessage.to;
			SnapAgent snapAgent = (SnapAgent)snap.getProtocol(agentProtocol);
			boolean shouldBid = !snap.snapped() && snapAgent.isIdle();
			LOGGER.debug("StrategyAgent.shouldBid[{}]~node={}, should={}, snap.snapped()={}, "
					+ "snapAgent.isIdle()={}", 
					CommonState.getTime(), snap, shouldBid, snap.snapped(), snapAgent.isIdle());
			return shouldBid;
		};
	}

	protected void initGetProposal() {
		StrategyAgent.getProposal = (tenderMessage) -> {
			Snap snap = (Snap)tenderMessage.to;
			Integer proposal = snap.swapableCard;
			LOGGER.debug("StrategyAgent.initGetOffer[{}]~node={}, proposal={}", 
					CommonState.getTime(), snap, proposal);
			return proposal;	
		};
	}
	
	protected void initGetBidComparator() {
		StrategyAgent.getBidComparator = () -> {
			Comparator<BidMessage<Integer,Integer>> bidComparator = 
					(BidMessage<Integer,Integer> bid1, BidMessage<Integer,Integer> bid2) -> { 
				Snap snapNode = (Snap)bid1.to;
				int card1 = (int)bid1.proposal;
				int card2 = (int)bid2.proposal;
				int diff1 = Math.abs(card1 - snapNode.myCard);
				int diff2 = Math.abs(card2 - snapNode.myCard);
				return diff1 - diff2; 
			};
			return bidComparator;
		};
	}
	
	protected void initShouldAward() {
		StrategyAgent.shouldAward = (bidMessage) -> {
			boolean shouldAward = true;
			return shouldAward;
		};
		
	}
		
	protected void initAward() {
		StrategyAgent.award = (awardMessage) -> {
			Snap snap = (Snap)awardMessage.from;
			
			// Part 1 of the swap
			snap.swapCard((int)awardMessage.proposal);
			LOGGER.debug("StrategyAgent.awarded[{}]~snap={}", CommonState.getTime(), snap);
		};
	}
	
	protected void initAwarded() {
		StrategyAgent.awarded = (awardMessage) -> {
			Snap snap = (Snap)awardMessage.to;

			// Part 2 of the swap
			snap.swapCard((int)awardMessage.task);
			LOGGER.debug("StrategyAgent.awarded[{}]~snap={}", CommonState.getTime(), snap);
		};
	}
	
	protected int getNumberOfSnaps() {
		int numberOfSnaps = 0;
		for (int n = 0; n < Network.size(); n++) {
			Snap snapNode = (Snap) Network.get(n);
			if (snapNode.snapped()) {
				numberOfSnaps++;
			}
		}
		return numberOfSnaps;
	}
	
}
