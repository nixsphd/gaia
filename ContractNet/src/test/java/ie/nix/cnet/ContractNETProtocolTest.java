package ie.nix.cnet;

import java.util.Comparator;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.cnet.ContractNETProtocol.BidMessage;
import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import ie.nix.cnet.TestNode.Contractor;
import ie.nix.cnet.TestNode.Manager;
import ie.nix.cnet.TestNode.TestTaskAnnounceMessage;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDSimulator;

public class ContractNETProtocolTest {

	int random_seed = 1;
	private int neighbourhoodSize = 2;

	@BeforeAll
	static void beforeAll() {
		// Redirect std err to files.
//		try {
//			File errFile = new File("err.txt");
//			FileOutputStream errFOS = new FileOutputStream(errFile);
//			PrintStream errPS = new PrintStream(errFOS);
//			System.setErr(errPS);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] { "src/test/java/ie/nix/cnet/ContractNETProtocolTest.properties" });

	}

	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to " + random_seed);
	}

	@AfterEach
	void afterEach() {
		TestNode.clearAll();
		System.out.println();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}

	/*
	 * Sedn Tenders
	 */

	@Test
	@DisplayName("One Should Tender")
	public void oneShouldTenderTest() {
		shouldTenderTest(1);

	}

	@Test
	@DisplayName("Two Should Tender")
    public void twoShouldTenderTest() {
		shouldTenderTest(2);
	
	}

	protected void shouldTenderTest(int numberToTender) {
		shouldTenderOnce(numberToTender);
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfTenderersSent() == numberToTender,
				"Tenderers should be " + numberToTender + ", but is " + getNumberOfTenderersSent() + ".");
	}

	protected void shouldTenderOnce(int numberToTender) {
		Manager.shouldTender = (Node node) -> {
			TestNode testNode = (TestNode)node;
			Manager manager = (Manager)testNode.getManager();
			return !manager.isTendering() && (node.getID() % (Network.size() / numberToTender) == 0);
		};
	}

	protected int getNumberOfTenderersSent() {
		int tenderSent = 0;
		for (int n = 0; n < Network.size(); n++) {
			Manager manager = getManager(n);
			if (manager.taskAnnouncementSent > 0) {
				tenderSent += manager.taskAnnouncementSent;
			}
		}
		return tenderSent;
	}

	protected Manager getManager(int n) {
		return (TestNode.Manager) ((TestNode) Network.get(n)).getManager();
	}

	/*
	 * Tenders Received
	 */
	@Test
	@DisplayName("Two Tenders Recieved")
	public void oneTendersRecievedTest() {
		int numberToTender = 1;
		tendersRecievedTest(numberToTender, numberToTender * neighbourhoodSize);
	
	}

	@Test
	@DisplayName("Four Tenders Recieved")
	public void twoTendersRecievedTest() {
		int numberToTender = 2;
		tendersRecievedTest(numberToTender, numberToTender * neighbourhoodSize);	
	}

	protected void tendersRecievedTest(int numberToTender, int numberOfTendersToBeRecieved) {
		shouldTenderOnce(numberToTender);
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfTendersRecieved() == numberOfTendersToBeRecieved,
				"Tenders recieved id should be " + numberOfTendersToBeRecieved + ", but is "
						+ getNumberOfTendersRecieved() + ".");
	}
	
	protected int getNumberOfTendersRecieved() {
		int tendersRecieved = 0;
		for (int n = 0; n < Network.size(); n++) {
			Contractor contractor = getContractor(n);
			tendersRecieved += contractor.tendersRecieved;
		}
		return tendersRecieved;
	}

	protected Contractor getContractor(int n) {
		return (TestNode.Contractor)((TestNode)Network.get(n)).getContractor();
	}

	/*
	 * Should Bid
	 */
	@Test
	@DisplayName("One Should Bid")
	public void oneShouldBidTest() {
		int numberToTender = Network.size();
		int numberToBid = 1;
		oneShouldBidTest(numberToTender, numberToBid);
		
	}

	protected void oneShouldBidTest(int numberToTender, int numberToBid) {
		shouldTenderOnce(numberToTender);
		
		oneShouldBidOnce();
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfBidsSent() == numberToBid,
				"Bid sent should be " + numberToBid + ", but is " + getNumberOfBidsSent() + ".");
		
	}

	protected void oneShouldBidOnce() {
		Manager.createTaskAnnouncementMessage = (Node node, int protocolID, Node neighbourNode,
				int contractID, long expirationTime) -> {
			TestTaskAnnounceMessage testTaskAnnounceMessage = 
					new TestTaskAnnounceMessage(neighbourNode, node, contractID, expirationTime, 0);
			return testTaskAnnounceMessage;
		};
	
		Contractor.shouldBid = (Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) -> {
			TestTaskAnnounceMessage testTaskAnnounceMessage = (TestTaskAnnounceMessage)taskAnnounceMessage;
			return testTaskAnnounceMessage.shouldBid(node.getIndex());
		};
	
	}
	
	protected int getNumberOfBidsSent() {
		int bidsSent = 0;
		for (int n = 0; n < Network.size(); n++) {
			Contractor contractor = getContractor(n);
			if (contractor.bidMessages > 0) {
				bidsSent += contractor.bidMessages;
			}
		}
		return bidsSent;
		
	}
	

	@Test
	@DisplayName("Two Should Bid")
	public void twoShouldBidTest() {
		int numberToTender = 1;
		shouldBidTest(numberToTender, numberToTender * neighbourhoodSize);
		
	}

	@Test
	@DisplayName("Four Should Bid")
	public void fourShouldBidTest() {
		int numberToTender = 2;
		shouldBidTest(numberToTender, numberToTender * neighbourhoodSize);
		
	}

	protected void shouldBidTest(int numberToTender, int numberToBid) {
		shouldTenderOnce(numberToTender);
		
		allShouldBidOnce();
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfBidders() == numberToBid,
				"Bidders should be " + numberToBid + ", but is " + getNumberOfBidders() + ".");
	}

	protected void allShouldBidOnce() {
		Contractor.shouldBid = (Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) -> {
			TestNode testNode = (TestNode)node;
			return !testNode.isContractorBidding() && !testNode.isManagerTendering();
		};
	}
	
	protected int getNumberOfBidders() {
		int bidders = 0;
		for (int n = 0; n < Network.size(); n++) {
			if (getContractor(n).bidMessages > 0) {
				bidders++;
			}
		}
		return bidders;
	}

	/*
	 * Award Sent
	 */
	@Test
	@DisplayName("One Awarded")
	public void oneAwarded() {
		int numberToTender = 1;
		int numberToAward = 1;
		awardTest(numberToTender, numberToAward);
		
	}

	@Test
	@DisplayName("Two Awarded")
	public void twoAwarded() {
		int numberToTender = 2;
		awardTest(numberToTender, numberToTender);
		
	}

	protected void awardTest(int numberToTender, int numberToAward) {
		shouldTenderOnce(numberToTender);
		allShouldBidOnce();
		awardOne();
	
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfAwardsSent() == numberToAward,
				"Awards should be " + numberToAward + ", but is " + getNumberOfAwardsSent() + ".");
	}

	protected void awardOne() {
		Manager.getBidComparator = (Node node, int protocolID) -> {
			Comparator<BidMessage> bidComparator = (BidMessage bid1, BidMessage bid2) -> {
				return bid2.from.getIndex() - bid1.from.getIndex();
			};
			return bidComparator;
		};
		Manager.shouldAward = (Node node, int protocolID, BidMessage bidMessage) -> {
			Linkable neighbourhood = (Linkable) node.getProtocol(FastConfig.getLinkable(protocolID));
			Node neighbourNode0 = (Node) neighbourhood.getNeighbor(0);
			Node neighbourNode1 = (Node) neighbourhood.getNeighbor(1);
			Node higentsIndexNode = (neighbourNode0.getIndex() > neighbourNode1.getIndex()? neighbourNode0 : neighbourNode1);
	    	return bidMessage.from == higentsIndexNode;
	    };
	}
	
	protected int getNumberOfAwardsSent() {
		int awards = 0;
		for (int n = 0; n < Network.size(); n++) {
			Manager manager = getManager(n);
			if (manager.awardsSent > 0) {
				awards++;
			}
		}
		return awards;
	}
	
	/*
	 * Award Recieved
	 */
	@Test
	@DisplayName("One Award Recieved")
	public void oneAwardRecievedTest() {
		int numberToTender = 1;
		int numberOfAwardsToBeRecieved = 1;
		awardRecievedTest(numberToTender, numberOfAwardsToBeRecieved);	
	}

	@Test
	@DisplayName("Two Award Recieved")
	public void twoAwardRecievedTest() {
		int numberToTender = 2;
		awardRecievedTest(numberToTender, numberToTender);	
	}

	protected void awardRecievedTest(int numberToTender, int numberOfAwardsToBeRecieved) {
		shouldTenderOnce(numberToTender);
		allShouldBidOnce();
		awardOne();
		
		EDSimulator.nextExperiment();
		Assertions.assertTrue(getNumberOfAwardsRecieved() == numberOfAwardsToBeRecieved,
				"Awards recieved id should be " + numberOfAwardsToBeRecieved + ", but is "
						+ getNumberOfAwardsRecieved() + ".");
	}
	
	protected int getNumberOfAwardsRecieved() {
		int awardsRecieved = 0;
		for (int n = 0; n < Network.size(); n++) {
			Contractor contractor = getContractor(n);
			awardsRecieved += contractor.awardsRecieved;
		}
		return awardsRecieved;
	}

}