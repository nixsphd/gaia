package ie.nix.cnet;

import java.util.Comparator;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.cnet.ContractNETProtocol.GossipTaskAnnounceMessage;
import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import ie.nix.cnet.TestNode.Contractor;
import ie.nix.cnet.TestNode.Manager;
import peersim.core.CommonState;
import peersim.core.Node;
import peersim.edsim.EDSimulator;

public class GossipSnapTest extends SnapTest {
	
	public static class GossipSnapTaskAnnounceMessage extends GossipTaskAnnounceMessage {

		public int card;
		
		public GossipSnapTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime, int card, int counter) {
			super(to, from, contractID, expirationTime, counter);
			this.card = card;
		}
		
	}
	
	@BeforeAll
	static void beforeAll() {
		// Redirect std err to files.
//		try {
//			File errFile = new File("err.txt");
//			FileOutputStream errFOS = new FileOutputStream(errFile);
//			PrintStream errPS = new PrintStream(errFOS);
//			System.setErr(errPS);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {"src/test/java/ie/nix/cnet/GossipSnapTest.properties"});
		
	}

	private int gossipCounter = 1;
	
	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to "+random_seed);
	}

	@AfterEach
	void afterEach() {
		TestNode.clearAll();
		System.out.println();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	@Test
    @DisplayName("GCNet Gossip Snap Test")
    public void gossipSnapTest() {
		initCards();
		initObservers();
		
		initShouldTender();
		initTaskAnnounce();
		
		initGetTaskComparator();
		initShouldBid();
		initBid();
		
		initGetBidComparator();
		initShouldAward();
		initAward();
		
		EDSimulator.nextExperiment();
		
		int numberOfSnaps = 200;
		Assertions.assertTrue(getNumberOfSnaps() == numberOfSnaps,
				"Snaps should be " + numberOfSnaps + ", but is "+ getNumberOfSnaps() + ".");
		
	}
	
	protected void initTaskAnnounce() {
		Manager.createTaskAnnouncementMessage = 
				(Node node, int protocolID, Node neighbourNode, int contractID, long expirationTime) -> {
			Snap snap = (Snap)node;
			GossipSnapTaskAnnounceMessage gossipSnapTaskAnnounceMessage = new GossipSnapTaskAnnounceMessage(
					neighbourNode, node, contractID, expirationTime, gossipCounter, snap.getSwapableCard());
			return gossipSnapTaskAnnounceMessage;	
		};
	}

	protected void initGetTaskComparator() {
		Contractor.getTaskComparator = (Node node, int protocolID) -> {
			Snap snapNode = (Snap)node;
			Comparator<TaskAnnounceMessage> taskComparator = (TaskAnnounceMessage task1, TaskAnnounceMessage task2) -> { 
				int card1 = (int)((GossipSnapTaskAnnounceMessage)task1).card;
				int card2 = (int)((GossipSnapTaskAnnounceMessage)task2).card;
				int diff1 = Math.abs(card1 - snapNode.getMyCard());
				int diff2 = Math.abs(card2 - snapNode.getMyCard());
				return diff1 - diff2; 
			};
			return taskComparator;
		};
	}
}
