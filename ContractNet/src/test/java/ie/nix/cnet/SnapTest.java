package ie.nix.cnet;

import java.util.Comparator;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.cnet.ContractNETProtocol.AwardMessage;
import ie.nix.cnet.ContractNETProtocol.BidMessage;
import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import ie.nix.cnet.TestNode.Contractor;
import ie.nix.cnet.TestNode.Manager;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.edsim.EDSimulator;

public class SnapTest {
	
	public static class SnapTaskAnnounceMessage extends TaskAnnounceMessage {

		public int card;
		
		public SnapTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime, int card) {
			super(to, from, contractID, expirationTime);
			this.card = card;
		}
		
	}
	
	public static class SnapBidMessage extends BidMessage {

		public int card;
		
		public SnapBidMessage(Node to, Node from, int contractID, int card) {
			super(to, from, contractID);
			this.card = card;
		}
		
	}
	
	public static class SnapAwardMessage extends AwardMessage {

		public int card;
		
		public SnapAwardMessage(Node to, Node from, int contractID, int card, long expirationTime) {
			super(to, from, contractID, expirationTime);
			this.card = card;
		}
		
	}
	
	public static class Snap extends TestNode {

		private int myCard;
		private int swapableCard;
		
		public Snap(String prefix) {
			super(prefix);
		}
		
		public boolean snapped() {
			return (myCard == swapableCard);
		}
		
		public void swapCard(int swapableCard) {
			this.swapableCard = swapableCard;
		}

		public int getMyCard() {
			return myCard;
		}

		public int getSwapableCard() {
			return swapableCard;
		}
		
	}

	int random_seed = 1;
	
	@BeforeAll
	static void beforeAll() {
		// Redirect std err to files.
//		try {
//			File errFile = new File("err.txt");
//			FileOutputStream errFOS = new FileOutputStream(errFile);
//			PrintStream errPS = new PrintStream(errFOS);
//			System.setErr(errPS);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {"src/test/java/ie/nix/cnet/SnapTest.properties"});
		
	}

	@BeforeEach
	void beforeEach() {
		CommonState.initializeRandom(random_seed);
		System.out.println("Reset seed to "+random_seed);
	}

	@AfterEach
	void afterEach() {
		TestNode.clearAll();
		System.out.println();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	@Test
    @DisplayName("GCNet Snap Test")
    public void gossipSnapTest() {
		
		CommonState.initializeRandom(14);
		
		initCards();
		initObservers();
		
		initShouldTender();
		initTaskAnnounce();
		
		initGetTaskComparator();
		initShouldBid();
		initBid();
		
		initGetBidComparator();
		initShouldAward();
		initAward();
		
		EDSimulator.nextExperiment();
		
		int numberOfSnaps = 20;
		Assertions.assertTrue(getNumberOfSnaps() == numberOfSnaps,
				"Snaps should be " + numberOfSnaps + ", but is "+ getNumberOfSnaps() + ".");
		
    }

	protected void initCards() {
		Snap.Init.control = () -> {
			System.out.println("Init["+CommonState.getTime()+"]~Network.size()="+Network.size());
			for (int n = 0; n < Network.size(); n++) {
				Snap snap = (Snap)Network.get(n);
				snap.myCard = n;
				snap.swapableCard = (Network.size() -1) - n;
//				System.out.println("Init["+CommonState.getTime()+"]~["+snap.getID()+"]myCard="+snap.myCard);
//				System.out.println("Init["+CommonState.getTime()+"]~["+snap.getID()+"]swapableCard="+snap.swapableCard);
				
			}
			return false;
	    };
	}
	
	protected void initObservers() {
		Snap.Observer.control = () -> {
			System.out.println("Observer[" + CommonState.getTime() + "]");
			int snaps = 0;
			int distance = 0;
			StringBuffer idsString =          new StringBuffer("ids         =[");
			StringBuffer myCardString =       new StringBuffer("myCards     =[");
			StringBuffer swapableCardString = new StringBuffer("swapableCard=[");
			for (int n = 0; n < Network.size(); n++) {
				Snap snap = (Snap) Network.get(n);
				if (snap.snapped()) {
					snaps++;
				}
				distance +=  Math.abs(snap.myCard - snap.swapableCard);
				idsString.append(String.format("%2d", snap.getID())+",");
				myCardString.append(String.format("%2d", snap.myCard)+",");
				swapableCardString.append(String.format("%2d", snap.swapableCard)+",");
			}
			myCardString.append("]");
			swapableCardString.append("]");
			System.out.println("Observer[" + CommonState.getTime() + "]Snap.SnapObserver.execute()~snaps=" + snaps
					+ " out of " + Network.size()+ ", distance="+distance);
			System.out.println(idsString);
			System.out.println(myCardString);
			System.out.println(swapableCardString);
			return false;
		};
	}
	
	protected void initShouldTender() {
		Manager.shouldTender = (Node node) -> {
			Snap snapNode = (Snap) node;
//			System.out.println("NDB::[" + CommonState.getTime() + "]Snap.Manager[" + node.getID()
//					+ "].shouldTender()~snapNode.snapped()=" + snapNode.snapped());
//			System.out.println("NDB::[" + CommonState.getTime() + "]Snap.Manager[" + node.getID()
//					+ "].shouldTender()~snapNode.isManagerTendering()=" + snapNode.isManagerTendering(node));
//			System.out.println("NDB::[" + CommonState.getTime() + "]Snap.Manager[" + node.getID()
//					+ "].shouldTender()~snapNode.isContractorBidding()=" + snapNode.isContractorBidding(node));

			boolean shouldTender = !snapNode.snapped() 
					&& !snapNode.isManagerTendering()
					&& !snapNode.isContractorBidding();
//			System.out.println("NDB::[" + CommonState.getTime() + "]Snap.Manager[" + node.getID()
//					+ "].shouldTender()~snapNode.shouldTender=" + shouldTender);

			return shouldTender;
		};
	}
	
	protected void initTaskAnnounce() {
		Manager.createTaskAnnouncementMessage = 
				(Node node, int protocolID, Node neighbourNode, int contractID, long expirationTime) -> {
			Snap snap = (Snap)node;
			SnapTaskAnnounceMessage SnapTaskAnnounceMessage = 
					new SnapTaskAnnounceMessage(neighbourNode, node, contractID, expirationTime, snap.swapableCard);
			return SnapTaskAnnounceMessage;	
		};
	}

//	protected void initShouldBidImmediatly() {
//		Contractor.shouldBidImmediatly = (Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) -> {
//			Snap snap = (Snap) node;
//			boolean shouldBidImmediatly = !snap.snapped() 
//					&& !snap.isManagerTendering()
//					&& !snap.isContractorBidding()
//					&& (snap.myCard == (int)taskAnnounceMessage.bidSpecification.get("CARD"));
////			System.out.println("NDB::[" + CommonState.getTime() + "]Snap.Contractor[" + node.getID()
////					+ "].initShouldBidImmediatly()~shouldBidImmediatly=" + shouldBidImmediatly);
//
//			return shouldBidImmediatly;
//		};
//	}
	
	protected void initGetTaskComparator() {
		Contractor.getTaskComparator = (Node node, int protocolID) -> {
			Snap snapNode = (Snap)node;
			Comparator<TaskAnnounceMessage> taskComparator = (TaskAnnounceMessage task1, TaskAnnounceMessage task2) -> { 
				int card1 = (int)((SnapTaskAnnounceMessage)task1).card;
				int card2 = (int)((SnapTaskAnnounceMessage)task2).card;
				int diff1 = Math.abs(card1 - snapNode.myCard);
				int diff2 = Math.abs(card2 - snapNode.myCard);
				return diff1 - diff2; 
			};
			return taskComparator;
		};
	}
	
	protected void initShouldBid() {
		Contractor.shouldBid = (Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) -> {
			Snap snap = (Snap) node;
			boolean shouldBid = !snap.snapped() 
					&& !snap.isManagerTendering()
					&& !snap.isContractorBidding();
			return shouldBid;
		};
	}
		
//	protected void initShouldAwardImmediatly() {
//		Manager.shouldAwardImmediatly = (Node node, int protocolID, BidMessage bidMessage) -> {
//			Snap snap = (Snap)node;
//			return (snap.myCard == (int)bidMessage.nodeAbstraction.get("CARD"));
//		};
//	}
	
	protected void initGetBidComparator() {
		Manager.getBidComparator = (Node node, int protocolID) -> {
			Snap snapNode = (Snap)node;
			Comparator<BidMessage> bidComparator = (BidMessage bid1, BidMessage bid2) -> { 
				int card1 = (int)((SnapBidMessage)bid1).card;
				int card2 = (int)((SnapBidMessage)bid2).card;
				int diff1 = Math.abs(card1 - snapNode.myCard);
				int diff2 = Math.abs(card2 - snapNode.myCard);
				return diff1 - diff2; 
			};
			return bidComparator;
		};
	}

	protected void initBid() {
		Contractor.createBidMessage = (Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) -> {
			Snap snap = (Snap)node;
			SnapBidMessage snapBidMessage = 
					new SnapBidMessage(taskAnnounceMessage.from, node, taskAnnounceMessage.contractID, snap.swapableCard);
			return snapBidMessage;	
		};
	}
	
	protected void initShouldAward() {
		Manager.shouldAward = (Node node, int protocolID, BidMessage bidMessage) -> {
			boolean shouldAward = true;
			return shouldAward;
		};
		
	}
		
	protected void initAward() {
		Manager.createAwardMessage = (Node node, int protocolID, BidMessage bidMessage, long expirationTime) -> {
			Snap snap = (Snap)node;
			SnapBidMessage snapBidMessage = (SnapBidMessage)bidMessage;
			SnapAwardMessage snapAwardMessage = 
					new SnapAwardMessage(snapBidMessage.from, node, snapBidMessage.contractID, snap.swapableCard, expirationTime);
			// Part 1 of the swap
			snap.swapCard(snapBidMessage.card);
			return snapAwardMessage;	
		};

		Manager.award = (Node node, int protocolID, AwardMessage awardMessage) -> {
			SnapAwardMessage snapAwardMessage = (SnapAwardMessage)awardMessage;
			Snap snap = (Snap)awardMessage.to;
			// Part 2 of the swap
			snap.swapCard((int)snapAwardMessage.card);
		};
	}
	
	protected int getNumberOfSnaps() {
		int numberOfSnaps = 0;
		for (int n = 0; n < Network.size(); n++) {
			Snap snapNode = (Snap) Network.get(n);
			if (snapNode.snapped()) {
				numberOfSnaps++;
			}
		}
		return numberOfSnaps;
	}
	
}
