package ie.nix.cnet;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.cnet.ContractNETProtocol.AwardMessage;
import ie.nix.cnet.ContractNETProtocol.BidMessage;
import ie.nix.cnet.ContractNETProtocol.DualRoleNode;
import ie.nix.cnet.ContractNETProtocol.GossipTaskAnnounceMessage;
import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Node;

public class TestNode extends DualRoleNode {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class TestTaskAnnounceMessage extends TaskAnnounceMessage {

		public ArrayList<Integer> bidderIDs;
		
		public TestTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime) {
			this(to, from, contractID, expirationTime, new ArrayList<Integer>());
		}
		
		public TestTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime, int bidderID) {
			this(to, from, contractID, expirationTime);
			this.bidderIDs.add(bidderID);
		}
		
		public TestTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime, ArrayList<Integer> bidderIDs) {
			super(to, from, contractID, expirationTime);
			this.bidderIDs = bidderIDs;
		}

		public boolean shouldBid(int index) {
			boolean shouldBid = bidderIDs.contains(index);
			LOGGER.trace("[{}]~bidderIDs={}, index={} -> {}",
					CommonState.getTime(), bidderIDs, index, shouldBid);
			return shouldBid;
		}
		
	}
	
	public static class TestGossipTaskAnnounceMessage extends GossipTaskAnnounceMessage {

		public ArrayList<Integer> bidderIDs;
		
		public TestGossipTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime, int counter) {
			this(to, from, contractID, expirationTime, counter, new ArrayList<Integer>());
		}
		
		public TestGossipTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime, int counter, int bidderID) {
			this(to, from, contractID, expirationTime, counter);
			this.bidderIDs.add(bidderID);
		}
		
		public TestGossipTaskAnnounceMessage(Node to, Node from, int contractID, long expirationTime, int counter, ArrayList<Integer> bidderIDs) {
			super(to, from, contractID, expirationTime, counter);
			this.bidderIDs = bidderIDs;
		}

		public boolean shouldBid(int index) {
			boolean shouldBid = bidderIDs.contains(index);
			LOGGER.trace("[{}]~bidderIDs={}, index={} -> {}",
					CommonState.getTime(), bidderIDs, index, shouldBid);
			return shouldBid;
		}
		
	}
	
	public static class Manager extends ie.nix.cnet.Manager {

		public interface ShouldTender {
			boolean shouldTender(Node node);
		}
		
		public interface CreateTaskAnnouncementMessage {
			TaskAnnounceMessage createTaskAnnouncementMessage(Node node, int protocolID, Node neighbourNode,
				int contractID, long expirationTime);
		}


		public interface GetBidComparator {
			Comparator<BidMessage> getBidComparator(Node node, int protocolID);
		}

		public interface ShouldAward {
			boolean shouldAward(Node node, int protocolID, BidMessage bidMessage);
		}

		public interface CreateAwardMessage {
			AwardMessage createAwardMessage(Node node, int protocolID, BidMessage bidMessage, long expirationTime);
		}

		public interface Award {
			void award(Node node, int protocolID, AwardMessage awardMessage);
		}
		
		public static ShouldTender shouldTender;
		public static CreateTaskAnnouncementMessage createTaskAnnouncementMessage;
		public static GetBidComparator getBidComparator;
		public static ShouldAward shouldAward;
		public static CreateAwardMessage createAwardMessage;
		public static Award award;

		public int taskAnnouncementSent = 0;
		public int bidsRecieved = 0;
		public int awardsSent = 0;

		public Manager(String prefix) {
			super(prefix);
		}

		@Override
		protected boolean shouldTender(Node node) {
			boolean should;
			if (shouldTender != null) {
				should = shouldTender.shouldTender(node);
			} else {
				should = super.shouldTender(node);
			}
			LOGGER.trace("[{}]~node={} -> {}", CommonState.getTime(), node, should);
			return should;
		}

		@Override
		protected TaskAnnounceMessage createTaskAnnouncementMessage(Node node, int protocolID, Node neighbourNode,
				int contractID, long expirationTime) {
			LOGGER.trace("[{}]~contractID={}, node={}, neighbourNode={}", 
					CommonState.getTime(), contractID, node, neighbourNode);
			taskAnnouncementSent++;
			if (createTaskAnnouncementMessage != null) {
				return createTaskAnnouncementMessage.createTaskAnnouncementMessage(
						node, protocolID, neighbourNode, contractID, expirationTime);
				
			} else {
				return super.createTaskAnnouncementMessage(
						node, protocolID, neighbourNode, contractID, expirationTime);
				
			}
		}

		@Override
		protected Comparator<BidMessage> getBidComparator(Node node, int protocolID) {
			LOGGER.trace("[{}]~node={}", CommonState.getTime(), node);
			if (getBidComparator != null) {
				return getBidComparator.getBidComparator(node, protocolID);
			} else {
				return super.getBidComparator(node, protocolID);
			}
		}

		@Override
		protected boolean shouldAward(Node node, int protocolID, BidMessage bidMessage) {
			boolean should;
			if (shouldAward != null) {
				should = shouldAward.shouldAward(node, protocolID, bidMessage);
			} else {
				should = super.shouldAward(node, protocolID, bidMessage);
			}
			LOGGER.trace("[{}]~node={} -> {}", CommonState.getTime(), node, should);
			return should;
		}

		@Override
		protected AwardMessage createAwardMessage(Node node, int protocolID, BidMessage bidMessage, long expirationTime) {
			LOGGER.trace("[{}]~contractID={}, node={}", 
					CommonState.getTime(), bidMessage.contractID, node);
			awardsSent++;
			if (createAwardMessage != null) {
				return createAwardMessage.createAwardMessage(node, protocolID, bidMessage, expirationTime);
				
			} else {
				return super.createAwardMessage(node, protocolID, bidMessage, expirationTime);
				
			}
		}
		
		@Override
		protected void award(Node node, int protocolID, AwardMessage awardMessage) {
			LOGGER.trace("[{}]~contractID={}, node={}, awardMessage={}", 
					CommonState.getTime(), awardMessage.contractID, node, awardMessage);
			if (award != null) {
				award.award(node, protocolID, awardMessage);
			} else {
				super.award(node, protocolID, awardMessage);
			}
		}

	}

	public static class Contractor extends ie.nix.cnet.Contractor {
		
		public interface ShouldBid {
			boolean shouldBid(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage);
		}
		
		public interface CreateBidMessage {
			BidMessage createBidMessage(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage);
		}
		
		public interface GetTaskComparator {
			Comparator<? super TaskAnnounceMessage> getTaskComparator(Node node, int protocolID);
		}
		
//		public interface Awarded {
//			void awarded(Node node, int protocolID, AwardMessage awardMessage);
//		}
		
		public static ShouldBid shouldBid;
		public static CreateBidMessage createBidMessage;
		public static GetTaskComparator getTaskComparator;
//		public static Awarded awarded;
		
		public int tendersRecieved;
		public int bidMessages;
		public int awardsRecieved;
		
		public Contractor(String prefix) {
			super(prefix);
		}
		
		@Override
		public void processEvent(Node node, int protocolID, Object event) {
			if (event instanceof TaskAnnounceMessage) {
				tendersRecieved++;
			
			} else if (event instanceof AwardMessage) {
				TestNode testNode = (TestNode)node;
				AwardMessage awardMessage = (AwardMessage)event;
				
				awardsRecieved++;

				testNode.awards.putIfAbsent(awardMessage.contractID, awardMessage);
				testNode.awardTimes.putIfAbsent(awardMessage.contractID, CommonState.getTime());
			
			}
			super.processEvent(node, protocolID, event);
		}

		@Override
		protected boolean shouldBid(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) {
			boolean should;
			if (shouldBid != null) {
				should = shouldBid.shouldBid(node, protocolID, taskAnnounceMessage);
			} else {
				should = super.shouldBid(node, protocolID, taskAnnounceMessage);
			}
			LOGGER.trace("[{}]~contractID={}, node={}, taskAnnounceMessage={} -> {}", 
					CommonState.getTime(), taskAnnounceMessage.contractID, node, taskAnnounceMessage, should);
			return should;
		}

		@Override
		protected BidMessage createBidMessage(Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) {
			LOGGER.trace("[{}]~contractID={}, node={}, taskAnnounceMessage={}", 
					CommonState.getTime(), taskAnnounceMessage.contractID, node, taskAnnounceMessage);
			bidMessages++;
			if (createBidMessage != null) {
				return createBidMessage.createBidMessage(node, protocolID, taskAnnounceMessage);
			} else {
				return super.createBidMessage(node, protocolID, taskAnnounceMessage);
			}
		}

		@Override
		protected Comparator<? super TaskAnnounceMessage> getTaskComparator(Node node, int protocolID) {
			LOGGER.trace("[{}]~node={}", CommonState.getTime(), node);
			if (getTaskComparator != null) {
				return getTaskComparator.getTaskComparator(node, protocolID);
			} else {
				return super.getTaskComparator(node, protocolID);
			}
		}

//		@Override
//		protected void awarded(Node node, int protocolID, AwardMessage awardMessage) {
//			LOGGER.trace("[{}]~contractID={}, node={}, awardMessage={}", 
//					CommonState.getTime(), awardMessage.contractID, node, awardMessage);
//			if (awarded != null) {
//				awarded.awarded(node, protocolID, awardMessage);
//			} else {
//				super.awarded(node, protocolID, awardMessage);
//			}
//		}
	}

	public static class Init implements Control {

		public static Control control;

		public Init(String prefix) {}

		public boolean execute() {
			if (control != null) {
				return control.execute();
			} else {
				return false;
			}
		}
	}

	public static class Observer implements Control {

		public static Control control;

		public Observer(String prefix) {}

		public boolean execute() {
			if (control != null) {
				return control.execute();
			} else {
				return false;
			}
		}

	}

	public static void clearAll() {
		Manager.shouldTender = null;
		Manager.createTaskAnnouncementMessage = null;
		Manager.getBidComparator = null;
		Manager.shouldAward = null;
		Manager.createAwardMessage = null;
		Manager.award = null;
		
		Contractor.shouldBid = null;
		Contractor.getTaskComparator = null;
		Contractor.createBidMessage = null;
//		Contractor.awarded = null;
	}
	
	public Map<Integer, TaskAnnounceMessage> taskAnnounces;		
	public Map<Integer, List<BidMessage>> bids;	
	public Map<Integer, AwardMessage> awards;	
	public Map<Integer, Long> awardTimes;	

	public TestNode(String prefix) {
		super(prefix);
		taskAnnounces = new HashMap<Integer, TaskAnnounceMessage>();
		bids = new HashMap<Integer, List<BidMessage>>();
		awards = new HashMap<Integer, AwardMessage>();
		awardTimes = new HashMap<Integer, Long>();

	}

	@Override
	public String toString() {
		return "TestNode [id="+getIndex()+"]";
	}

}
