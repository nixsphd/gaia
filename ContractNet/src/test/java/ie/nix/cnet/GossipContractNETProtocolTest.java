package ie.nix.cnet;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.cnet.ContractNETProtocol.TaskAnnounceMessage;
import ie.nix.cnet.TestNode.Contractor;
import ie.nix.cnet.TestNode.Manager;
import ie.nix.cnet.TestNode.TestGossipTaskAnnounceMessage;
import peersim.core.CommonState;
import peersim.core.Node;

public class GossipContractNETProtocolTest {

	private ContractNETProtocolTest helper;
	int random_seed= 1;
	private int neighbourhoodSize = 2;
	private int gossipCounter = 1;
	
	@BeforeAll
	static void beforeAll() {
		// Redirect std err to files.
//		try {
//			File errFile = new File("err.txt");
//			FileOutputStream errFOS = new FileOutputStream(errFile);
//			PrintStream errPS = new PrintStream(errFOS);
//			System.setErr(errPS);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				"src/test/java/ie/nix/cnet/GossipContractNETProtocolTest.properties"});
		
	}

	@BeforeEach
	void beforeEach() {
		helper = new ContractNETProtocolTest();
		helper.beforeEach();
	}

	@AfterEach
	void afterEach() {
		helper.afterEach();
	}

	@AfterAll
	static void afterAll() {
		System.out.println("Done.");
	}
	
	/*
	 * Tenders Sent
	 */
	@Test
    @DisplayName("One Should Tender")
    public void oneShouldTenderTest() {
		helper.shouldTenderTest(1);
    
    }
	
	protected void oneShouldBidOnce() {
		Manager.createTaskAnnouncementMessage = 
				(Node node, int protocolID, Node neighbourNode, int contractID, long expirationTime) -> {
			TestGossipTaskAnnounceMessage testGossipTaskAnnounceMessage = 
					new TestGossipTaskAnnounceMessage(neighbourNode, node, 
							contractID, expirationTime, gossipCounter, 0);
			return testGossipTaskAnnounceMessage;
		};

		Contractor.shouldBid = (Node node, int protocolID, TaskAnnounceMessage taskAnnounceMessage) -> {
			TestGossipTaskAnnounceMessage testTaskAnnounceMessage = 
					(TestGossipTaskAnnounceMessage) taskAnnounceMessage;
			return testTaskAnnounceMessage.shouldBid(node.getIndex());
		};

	}
	
	@Test
	@DisplayName("Two Should Tender")
    public void twoShouldTenderTest() {
		helper.shouldTenderTest(2);
	
	}
	
	/*
	 * Tenders Recieved
	 */
	@Test
	@DisplayName("One Tenderer Tenders Recieved")
	public void oneTendersRecievedTest() {
		int numberToTender = 1;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.tendersRecievedTest(numberToTender, expectedTenders);
	
	}

	@Test
	@DisplayName("Two Tenderers Tenders Recieved")
	public void twoTendersRecievedTest() {
		CommonState.initializeRandom(12);
		int numberToTender = 2;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.tendersRecievedTest(numberToTender, expectedTenders);	
	}

	protected int calculateExpectedTenders(int numberToTender) {
		int tendersPerTestNode = 0;
		for (int l = gossipCounter + 1; l > 0; l--) {
			tendersPerTestNode += Math.pow(neighbourhoodSize, l);
		}
		return numberToTender * tendersPerTestNode;
	}
	
	@Test
	@DisplayName("Two Should Bid")
	public void twoShouldBidTest() {
		// Regular random seed will cause test to fail is one node is in both networks of tenderers.
		CommonState.initializeRandom(12);
		int numberToTender = 1;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.shouldBidTest(numberToTender, expectedTenders);
		
	}
	
	@Test
	@DisplayName("Four Should Bid")
	public void fourShouldBidTest() {
		// Regular random seed will cause test to fail is one node is in both networks of tenderers.
		CommonState.initializeRandom(11);
		int numberToTender = 2;
		int expectedTenders = calculateExpectedTenders(numberToTender);
		helper.shouldBidTest(numberToTender, expectedTenders);
		
	}

}