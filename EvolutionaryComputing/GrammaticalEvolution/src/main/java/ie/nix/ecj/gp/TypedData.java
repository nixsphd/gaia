package ie.nix.ecj.gp;

import ec.gp.GPData;

public class TypedData extends Data {
	
	private static final long serialVersionUID = 1047156104416222251L;

	protected enum Type {
		Double,
		Boolean
	}
	
	protected Type type;
	
	private boolean booleanData;
	
	@Override
    public void copyTo(final GPData gpd) { 
		super.copyTo(gpd);
    	((TypedData)gpd).booleanData = booleanData; 
    	((TypedData)gpd).type = type;
    }

	@Override
	public String toString() {
		return "Data["+getDouble() +", "+booleanData+"]";
	}
	
	public double getDouble() {
		assert(type == Type.Double) : "trying to get a double from a "+type+" type.";
		return super.getDouble();
	}
	
	public boolean getBoolean() {
		assert(type == Type.Boolean) : "trying to get a boolean from a "+type+" type.";
		return booleanData;
	}
	
	public void set(double doubleData) {
		super.set(doubleData);
		type = Type.Double;
	}
	
	public void set(boolean booleanData) {
		this.booleanData = booleanData;
		type = Type.Boolean;
	}
	    
}
