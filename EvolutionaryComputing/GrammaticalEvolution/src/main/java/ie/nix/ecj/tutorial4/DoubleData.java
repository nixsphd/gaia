package ie.nix.ecj.tutorial4;

import ec.gp.GPData;

public class DoubleData extends GPData {

	private static final long serialVersionUID = -8432292374478369186L;
	
	 // return value
	public double x;   

	// copy my stuff to another DoubleData
    public void copyTo(final GPData gpd) { 
    	((DoubleData)gpd).x = x; 
    }

}