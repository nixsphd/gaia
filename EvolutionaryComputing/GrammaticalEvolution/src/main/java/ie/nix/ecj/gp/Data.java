package ie.nix.ecj.gp;

import ec.gp.GPData;

public class Data extends GPData {

	private static final long serialVersionUID = 1047156104416222251L;
	
	private double doubleData;

	@Override
    public void copyTo(final GPData gpd) { 
    		((Data)gpd).doubleData = doubleData; 
    }

	@Override
	public String toString() {
		return "Data[" + doubleData + "]";
	}

	public double getDouble() {
		return doubleData;
	}
	
	public void set(double doubleData) {
		this.doubleData = doubleData;
	}
	    
}
