package ie.nix.ecj.gp;

import java.util.function.BiConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class LogicalOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();

	public static class Not extends LogicalOperator {
		public Not() {
			super("not", "!", 1,  (childrensResults, result) -> 
			((TypedData)result).set(!
					((TypedData)childrensResults[0]).getBoolean()));
		}	
	}
	
	public static class And extends LogicalOperator {
		public And() {
			super("and", "&&", 2,  (childrensResults, result) -> {
				((TypedData)result).set(
						((TypedData)childrensResults[0]).getBoolean() && 
						((TypedData)childrensResults[1]).getBoolean());
                LOGGER.trace("{} && {} -> {}", 
                		((TypedData)childrensResults[0]).getBoolean(),
                		((TypedData)childrensResults[1]).getBoolean(),
                		((TypedData)result).getBoolean());
			});
		}
	}
	
	public static class Or extends LogicalOperator {
		public Or() {
			super("or", "||", 2,  (childrensResults, result) -> {
				((TypedData)result).set(
						((TypedData)childrensResults[0]).getBoolean() || 
						((TypedData)childrensResults[1]).getBoolean());
                LOGGER.trace("{} || {} -> {}", 
                		((TypedData)childrensResults[0]).getBoolean(),
                		((TypedData)childrensResults[1]).getBoolean(),
                		((TypedData)result).getBoolean());
			});
		}
	}
	
	protected String stringForHumans;
	
	protected LogicalOperator(String symbol, int numberOfChildren, BiConsumer<Data[], Data> eval) {
		this(symbol, symbol, numberOfChildren, eval);
	}
	
	protected LogicalOperator(String symbol, String stringForHumans, int numberOfChildren, BiConsumer<Data[], Data> eval) {
		super(symbol, numberOfChildren, eval);
		this.stringForHumans = stringForHumans;
	}

	public String toStringForHumans() { 
		return stringForHumans; 
	}
}
