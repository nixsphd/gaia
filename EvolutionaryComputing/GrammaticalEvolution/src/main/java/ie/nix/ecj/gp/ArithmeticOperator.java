package ie.nix.ecj.gp;

import java.util.function.BiConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class ArithmeticOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class Add extends ArithmeticOperator {
		
		public Add() {
			super("+", 2, (results, result) -> 
				result.set(results[0].getDouble() + results[1].getDouble()));
		}	
	}
	
	public static class Sub extends ArithmeticOperator {
		public Sub() {
			super("-", 2, (results, result) -> 
				result.set(results[0].getDouble() - results[1].getDouble()));
		}	
	}
	
	public static class Mul extends ArithmeticOperator {
		public Mul() {
			super("*", 2, (results, result) -> 
				result.set(results[0].getDouble() * results[1].getDouble()));
		}	
	}
	
	public static class Div extends ArithmeticOperator {
		public Div() {
			super("/", 2, (results, result) -> {
				if (results[1].getDouble() == 0) {
					result.set(0);
				} else {
					result.set(results[0].getDouble() / results[1].getDouble());
				}
			});
		}	
	}
	
	protected ArithmeticOperator(String symbol, int numberOfChildren, BiConsumer<Data[], Data> eval) {
		super(symbol, numberOfChildren, eval);
	}
}
