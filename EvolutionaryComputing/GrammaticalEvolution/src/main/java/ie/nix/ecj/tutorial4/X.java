package ie.nix.ecj.tutorial4;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;

public class X extends GPNode {

	private static final long serialVersionUID = 9003509246893593053L;

	public String toString() { 
		return "x"; 
	}

    public int expectedChildren() { 
    	return 0; 
    	
    }
    
    public void eval(final EvolutionState state,
            final int thread,
            final GPData input,
            final ADFStack stack,
            final GPIndividual individual,
            final Problem problem) {
		
		DoubleData rd = ((DoubleData)(input));

        rd.x = ((MultiValuedRegression)problem).currentX;
	}
    
}
