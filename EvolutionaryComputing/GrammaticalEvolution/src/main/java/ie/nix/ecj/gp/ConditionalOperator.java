package ie.nix.ecj.gp;

import java.util.function.BiConsumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("serial")
public class ConditionalOperator extends Node {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public static class IfDouble extends ConditionalOperator {
		public IfDouble() {
			super("ifd", (results, result) -> 
				result.set(
					((TypedData)results[0]).getBoolean() ? 
					results[1].getDouble() : 
					results[2].getDouble()));
		}	
	}
	
	public static class IfBoolean extends ConditionalOperator {
		public IfBoolean() {
			super("ifb", (results, result) -> 
				((TypedData)result).set(
						((TypedData)results[0]).getBoolean() ? 
						((TypedData)results[1]).getBoolean() : 
						((TypedData)results[2]).getBoolean()));
		}	
	}
	
	protected ConditionalOperator(String symbol, BiConsumer<Data[], Data> eval) {
		super(symbol, 3, eval);
	}
}
