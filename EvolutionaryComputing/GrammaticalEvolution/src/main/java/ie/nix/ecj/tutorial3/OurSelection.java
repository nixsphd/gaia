package ie.nix.ecj.tutorial3;

import java.util.ArrayList;

import ec.EvolutionState;
import ec.Individual;
import ec.SelectionMethod;
import ec.util.Parameter;

public class OurSelection extends SelectionMethod {

	private static final long serialVersionUID = 8952426758138743591L;

	// We have to specify a default base
	public static final String P_OURSELECTION = "our-selection";

	public Parameter defaultBase() {
		return new Parameter(P_OURSELECTION);
	}

	// We'll explain the ec.util.Parameter class later. Next we must load the P
	// probability parameter from the parameter database. To do this, we need to
	// define two things. First, we must define the default base (already done: it's
	// our-selection). Next, we need to define the parameter name. How about:
	// middle-probability. I'd use probability, except that a superclass already
	// uses that. Thus to set this parameter value, the user would define the
	// parameter pop.subpop.0.species.pipe.source.1.probability = ... , and barring
	// that, the system would look for the parameter our-selection.probability = ...
	// We define and load the parameter in the setup() method:

	public static final String P_MIDDLEPROBABILITY = "middle-probability"; // our parameter name

	public double middleProbability;

	public void setup(final EvolutionState state, final Parameter base) {

		// always call super.setup(...) first if it exists!
		super.setup(state, base);

		Parameter def = defaultBase();

		// gets a double between min (0.0) and max (1.0), from the parameter
		// database, returning a value of min-1 (-1.0) if the parameter doesn't exist or
		// was
		// outside this range.
		middleProbability = state.parameters.getDoubleWithMax(base.push(P_MIDDLEPROBABILITY),
				def.push(P_MIDDLEPROBABILITY), 0.0, 1.0);
		if (middleProbability < 0.0) {
			state.output.fatal("Middle-Probability must be between 0.0 and 1.0", base.push(P_MIDDLEPROBABILITY),
					def.push(P_MIDDLEPROBABILITY));
		}
	}

	@Override
	public int produce(final int subpopulation, final EvolutionState state, final int thread) {
		
		// toss a coin
		if (state.random[thread].nextBoolean(middleProbability)) {
			
			// pick three individuals, return the middle one
			ArrayList<Individual> inds = state.population.subpops.get(subpopulation).individuals;
			int one = state.random[thread].nextInt(inds.size());
			int two = state.random[thread].nextInt(inds.size());
			int three = state.random[thread].nextInt(inds.size());
			
			// generally the betterThan(...) method imposes an ordering,
			// so you shouldn't see any cycles here except in very unusual domains...
			if (inds.get(two).fitness.betterThan(inds.get(one).fitness)) {
				if (inds.get(three).fitness.betterThan(inds.get(two).fitness)) {
					// 1 < 2 < 3
					return two;
				} else if (inds.get(three).fitness.betterThan(inds.get(one).fitness)) {
					// 1 < 3 < 2
					return three;
				} else {
					// 3 < 1 < 2
					return one;
				}
			} else if (inds.get(three).fitness.betterThan(inds.get(one).fitness)) {
				// 2 < 1 < 3
				return one;
			} else if (inds.get(three).fitness.betterThan(inds.get(two).fitness)) {
				// 2 < 3 < 1
				return three;
			} else {
				// 3 < 2 < 1
				return two;
			}
		} else {
			// select a random individual's index
			return state.random[thread].nextInt(state.population.subpops.get(subpopulation).individuals.size());
		}
	}
}
