package ie.nix.ecj.tutorial1;

import ec.EvolutionState;
import ec.Individual;
import ec.Problem;
import ec.simple.SimpleFitness;
import ec.simple.SimpleProblemForm;
import ec.vector.BitVectorIndividual;

public class MaxOnes extends Problem implements SimpleProblemForm {

	private static final long serialVersionUID = 4932494889672240730L;

	@Override
   	public void evaluate(final EvolutionState state, final Individual ind, final int subpopulation, final int threadnum) {
		
		if (ind.evaluated) return;   //don't evaluate the individual if it's already evaluated

		// Individuals contain two main pieces of data: evaluated, which indicates that they've been evaluated already,
		// and fitness, which stores their fitness object. Continuing:

		if (!(ind instanceof BitVectorIndividual)) {
			state.output.fatal("Whoa!  It's not a BitVectorIndividual!!!",null);
		}
		
		BitVectorIndividual ind2 = (BitVectorIndividual)ind;

		// First we check to see if ind is a BitVectorIndividual -- otherwise something 
		// has gone terribly wrong. If something's wrong, we issue a fatal error through 
		// the state's Output facility. Messages (like fatal) all have one or two additional 
		// arguments where you can specify a Parameter that caused the fatal error, because 
		// it's very common to issue a fatal error on loading something from the 
		// ParameterDatabase and discovering it's incorrectly specified. Since this fatal error 
		// doesn't have anything to do with any specific parameter we know about, we pass in null. 
		// Continuing:
		int sum = 0;        
		for(int x = 0; x < ind2.genome.length; x++) {
			sum += (ind2.genome[x] ? 1 : 0);	
		}
		
		// VectorIndividuals have all have an array called genome. The type of this array 
		// (int, boolean, etc.) varies depending on the subclass. For BitVectorIndividual, 
		// genome is a boolean array. We're simply counting the number of trues in it. 
		//Continuing:
		if (!(ind2.fitness instanceof SimpleFitness)) {
			state.output.fatal("Whoa!  It's not a SimpleFitness!!!",null);
		}
		
		((SimpleFitness)ind2.fitness).setFitness(state, // the fitness
				((double)sum)/ind2.genome.length,       // is the individual ideal?  Indicate here...
				sum == ind2.genome.length);

		ind2.evaluated = true;
	}
}