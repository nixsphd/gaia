package ie.nix.ecj.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.gp.koza.KozaFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;

public class GPDemo extends GPProblem implements SimpleProblemForm {

	private static final long serialVersionUID = -7609354049349870294L;
	
	private static final Logger LOGGER = LogManager.getLogger();
	
	// GPProblem defines an instance variable called input, which contains a GPData instance loaded from our 
	// parameters. Here, we get a chance to verify that this instance is of a class we can use. In our case,
	// we want it to be a DoubleData class or some subclass of that.
    public void setup(final EvolutionState state, final Parameter base) {
        // very important, remember this
        super.setup(state, base);

        // verify our input is the right class (or subclasses from it)
        if (!(input instanceof Data)) {
            state.output.fatal("GPData class must subclass from " + Data.class, base.push(P_DATA), null);
        }
	}

	// Last we need to define the evaluate method, which actually evaluates the individual and sets its 
	// fitness. We will do so by testing the individual against ten data points, then setting its fitness to 
	// the sum of how close it got in each case. Thus 0 is the ideal fitness -- in GP, 0 is the ideal and 
	// infinity is worse than the worst possible fitness. The hit measure (an auxillary measure) is simply 
	// how often the system got "reasonably close".
    public void evaluate(final EvolutionState state, 
                         final Individual individual, 
                         final int subpopulation,
                         final int threadnum) {
    	
    	GPIndividual gpIndividual = (GPIndividual)individual;
    	Data actualResult = (Data)(this.input);
    	
    	// don't bother reevaluating
        if (!individual.evaluated)  {

            double sum = 0.0;
            
            for (int run = 0; run < 10; run++) {

                double x = state.random[threadnum].nextDouble() * state.random[threadnum].nextInt(3);
                double y = state.random[threadnum].nextDouble() * state.random[threadnum].nextInt(5);
                double z = state.random[threadnum].nextDouble() * state.random[threadnum].nextInt(10);

                Variable.X.value = x;
                Variable.Y.value = y;
                Variable.Z.value = z;
                		
                gpIndividual.trees[0].child.eval(state, threadnum, actualResult, stack, gpIndividual, this);

//                double expectedResult = x*x*y + x*y + z;
//                double expectedResult = Math.PI;
//                double expectedResult = x * y;
//                double expectedResult = y + Math.PI;
                double expectedResult = 13 * y;
                
                sum += Math.abs(expectedResult - actualResult.getDouble());
                LOGGER.trace("actualResult={}", actualResult.getDouble());
                
            }

            // the fitness better be KozaFitness!
            KozaFitness kozaFitness = ((KozaFitness)individual.fitness);
            kozaFitness.setStandardizedFitness(state, sum);
            individual.evaluated = true;
        }
    }
}
