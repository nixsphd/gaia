package ie.nix.ecj.gp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.gp.koza.KozaFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;

public class TwoTreesDemo extends GPDemo {

	private static final long serialVersionUID = -7609354049349870294L;
	
	private static final Logger LOGGER = LogManager.getLogger();

	// Last we need to define the evaluate method, which actually evaluates the individual and sets its 
	// fitness. We will do so by testing the individual against ten data points, then setting its fitness to 
	// the sum of how close it got in each case. Thus 0 is the ideal fitness -- in GP, 0 is the ideal and 
	// infinity is worse than the worst possible fitness. The hit measure (an auxillary measure) is simply 
	// how often the system got "reasonably close".
    public void evaluate(final EvolutionState state, 
                         final Individual individual, 
                         final int subpopulation,
                         final int threadnum) {
    	
    	GPIndividual gpIndividual = (GPIndividual)individual;
    	Nil actualResult = (Nil)(this.input);
    	
    	// don't bother reevaluating
        if (!individual.evaluated)  {

            double sum = 0.0;
            
            for (int run = 0; run < 10; run++) {

                double w = state.random[threadnum].nextDouble();
                double x = state.random[threadnum].nextDouble();
                double y = state.random[threadnum].nextDouble();
                double z = state.random[threadnum].nextDouble();

                Variable.W.value = w;
                Variable.X.value = x;
                Variable.Y.value = y;
                Variable.Z.value = z;
                		
                gpIndividual.trees[0].child.eval(state, threadnum, actualResult, stack, gpIndividual, this);             
                double actualResult0 = actualResult.data;
                		
                gpIndividual.trees[1].child.eval(state, threadnum, actualResult, stack, gpIndividual, this);
                double actualResult1 = actualResult.data;
                
                double expectedResult0 = x*x*w + x*w + w;
                double expectedResult1 = Math.PI;
                
                sum += Math.abs(expectedResult0 - actualResult0);  
                sum += Math.abs(expectedResult1 - actualResult1);  
                
                LOGGER.info("expectedResult0={}, actualResult0={}", expectedResult0, actualResult0);
                LOGGER.info("expectedResult1={}, actualResult1={}", expectedResult0, actualResult0);
                
            }

            // the fitness better be KozaFitness!
            KozaFitness kozaFitness = ((KozaFitness)individual.fitness);
            kozaFitness.setStandardizedFitness(state, sum);
            individual.evaluated = true;
        }
    }
}
