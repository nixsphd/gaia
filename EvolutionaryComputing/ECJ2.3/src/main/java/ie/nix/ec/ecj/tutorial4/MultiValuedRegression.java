package ie.nix.ec.ecj.tutorial4;

import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.gp.koza.KozaFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;

public class MultiValuedRegression extends GPProblem implements SimpleProblemForm {

	private static final long serialVersionUID = -7609354049349870294L;

	public static final String P_DATA = "data";

    public double currentX;
	public double currentY;

	// GPProblem defines an instance variable called input, which contains a GPData instance loaded from our 
	// parameters. Here, we get a chance to verify that this instance is of a class we can use. In our case,
	// we want it to be a DoubleData class or some subclass of that.
    public void setup(final EvolutionState state, final Parameter base) {
        // very important, remember this
        super.setup(state, base);

        // verify our input is the right class (or subclasses from it)
        if (!(input instanceof DoubleData)) {
            state.output.fatal("GPData class must subclass from " + DoubleData.class, base.push(P_DATA), null);
        }
	}

	// Last we need to define the evaluate method, which actually evaluates the individual and sets its 
	// fitness. We will do so by testing the individual against ten data points, then setting its fitness to 
	// the sum of how close it got in each case. Thus 0 is the ideal fitness -- in GP, 0 is the ideal and 
	// infinity is worse than the worst possible fitness. The hit measure (an auxillary measure) is simply 
	// how often the system got "reasonably close".
    public void evaluate(final EvolutionState state, 
                         final Individual ind, 
                         final int subpopulation,
                         final int threadnum) {
    	// don't bother reevaluating
        if (!ind.evaluated)  {
            DoubleData input = (DoubleData)(this.input);

            int hits = 0;
            double sum = 0.0;
            double expectedResult;
            double result;
            for (int y = 0; y < 10; y++) {
                currentX = state.random[threadnum].nextDouble();
                currentY = state.random[threadnum].nextDouble();
                expectedResult = currentX*currentX*currentY + currentX*currentY + currentY;
                ((GPIndividual)ind).trees[0].child.eval(
                    state,threadnum,input,stack,((GPIndividual)ind),this);

                result = Math.abs(expectedResult - input.x);
                if (result <= 0.01) {
                	hits++;
                }
                sum += result;                  
            }

            // the fitness better be KozaFitness!
            KozaFitness f = ((KozaFitness)ind.fitness);
            f.setStandardizedFitness(state, sum);
            f.hits = hits;
            ind.evaluated = true;
        }
    }
}
