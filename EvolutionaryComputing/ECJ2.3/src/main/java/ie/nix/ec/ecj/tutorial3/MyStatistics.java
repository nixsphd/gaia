package ie.nix.ec.ecj.tutorial3;

import ec.*;
import ec.util.*;
import java.io.*;
import ec.vector.*;

/*
	Immediately before and immediately after initialization
    (The Big Loop)
        Immediately before and immediately after evaluation
        Immediately before and immediately after pre-breeding exchange
        Immediately before and immediately after breeding
        Immediately before and immediately after post-breeding exchange
        Immediately before and immediately after checkpointing, if checkpointing is done 
    Immediately before finishing 
 */
public class MyStatistics extends Statistics {

	private static final long serialVersionUID = -6372720694155867181L;
	
	// The parameter string and log number of the file for our readable population
    public static final String P_POPFILE = "pop-file";
    public int popLog;

    // The parameter string and log number of the file for our best-genome-#3 individual
    public static final String P_INFOFILE = "info-file";
    public int infoLog;

    public void setup(final EvolutionState state, final Parameter base) {
        
    	// DO NOT FORGET to call super.setup(...) !!
        super.setup(state,base);
        
        // We will set up a log called "pop-file". This is done through our Output object. 
        // Of course we could just write to the file stream ourselves; but it is 
        // convenient to use the Output logger because it recovers from checkpoints 
        // and it's threadsafe.
        // set up popFile
        File popFile = state.parameters.getFile(base.push(P_POPFILE),null);

        // We tell Output to add a log file writing to "pop-file". We will NOT post 
        // announcements to the log (no fatal, error, warning messages etc.). The log 
        // WILL open via appending (not rewriting) upon a restart after a checkpoint failure. 
        if (popFile != null) {
        	try {
	            popLog = state.output.addLog(popFile,true);
        	} catch (IOException i) {
        		state.output.fatal("An IOException occurred while trying to create the log " + 
        				popFile + ":\n" + i);
            }
        }
        
        // similarly we set up infoFile
        File infoFile = state.parameters.getFile(base.push(P_INFOFILE),null);
        if (infoFile!=null) {
        	try {
        		infoLog = state.output.addLog(infoFile,true);
            } catch (IOException i) {
            	state.output.fatal("An IOException occurred while trying to create the log " + 
            			infoFile + ":\n" + i);
            }

        }
        
    }
    public void postEvaluationStatistics(final EvolutionState state) {
    	// Statistics objects have children. Hook methods are called in depth-first
    	// post-order. When a hook method is called in the a statistics object, the 
    	// first thing that must be done is a call to super.hook(...).
    	// be certain to call the hook on super!
    	super.postEvaluationStatistics(state);
    	
    	// write out a warning that the next generation is coming 
        state.output.println(
        		"-----------------------\n"
        		+ "GENERATION " + state.generation + "\n"
        		+ "-----------------------", popLog);
        
		//	Now we print out all the individuals. There are various ways to do this:
		//	    In a computer-readable format (writeIndividual) written to DataOutput
		//	    In a computer- and human-readable format (printIndividual) written to a log or to a Writer
		//	    In a human-readable format (printIndividualForHumans) written to a log or to a Writer 
		//	Likwise, Subpopulations have writeSubpopulation, printSubpopulation,, and 
		//		printSubpopulationForHumans. And Population has writePopulation, printPopulation,
		//	and printPopulationForHumans.
		//	Our choice will be to do Population.printPopulation.
        // print out the population 
        state.population.printPopulation(state,popLog);
        
        // Now let's print out the individual whose genome #3 is highest to infoLog. We'll print 
        // that one out in a human-readable fashion, so we use printIndividualForHumans. 
        // print out best genome #3 individual in subpop 0
        int best = 0;
        double best_val = ((DoubleVectorIndividual)state.population.subpops[0].individuals[0]).genome[3];
        for(int y = 1; y < state.population.subpops[0].individuals.length; y++) {
            // We'll be unsafe and assume the individual is a DoubleVectorIndividual
            double val = ((DoubleVectorIndividual)state.population.subpops[0].individuals[y]).genome[3];
            if (val > best_val) {
                best = y;
                best_val = val;
            }
        }
        state.population.subpops[0].individuals[best].printIndividualForHumans(state,infoLog);
        
    }
    
}
