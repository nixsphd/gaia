package ie.nix.ec.ecj.tutorial4;

import ec.gp.*;

public class DoubleData extends GPData {

	private static final long serialVersionUID = 4750045721579481844L;
	
	public double x;    // return value

	// copy my stuff to another DoubleData
    public void copyTo(final GPData gpd) { 
    	((DoubleData)gpd).x = x; 
    }
    
}