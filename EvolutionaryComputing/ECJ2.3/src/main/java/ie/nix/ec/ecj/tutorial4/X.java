package ie.nix.ec.ecj.tutorial4;

import ec.EvolutionState;
import ec.Problem;
import ie.nix.ec.ecj.tutorial4.MultiValuedRegression;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import ec.util.Parameter;

public class X extends GPNode {

	private static final long serialVersionUID = 4587138094783751590L;

	public String toString() { 
    	return "x"; 
    }

	// Note that there are no children.
    public int expectedChildren() { 
    	return 0; 
    }
	
    // Now what we'll do is define in our Problem two public doubles called currentX and currentY. 
    // These are the values that these nodes return when evaluated. 
	@Override
	public void eval(
			EvolutionState state, 
			int thread, 
			GPData input,
			ADFStack stack, 
			GPIndividual individual, 
			Problem problem) {
	
        DoubleData rd = ((DoubleData)(input));
        rd.x = ((MultiValuedRegression)problem).currentX;
		
	}
    
}
