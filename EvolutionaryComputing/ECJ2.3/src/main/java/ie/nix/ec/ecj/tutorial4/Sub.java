package ie.nix.ec.ecj.tutorial4;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import ec.util.Parameter;

public class Sub extends GPNode {
	
	private static final long serialVersionUID = 869052005025432198L;

	public String toString() { 
    	return "-"; 
    }

    public int expectedChildren() { 
    	return 2; 
    }
 
	public void checkConstraints(
			final EvolutionState state,
			final int tree,
			final GPIndividual typicalIndividual,
			final Parameter individualBase) {
		super.checkConstraints(state,tree,typicalIndividual,individualBase);
		if (children.length!=2) {
			state.output.error("Incorrect number of children for node " + toStringForError() + " at " + individualBase);
			
		}
	}
	
	@Override
	public void eval(
			EvolutionState state, 
			int thread, 
			GPData input,
			ADFStack stack, 
			GPIndividual individual, 
			Problem problem) {
	
		double result;
		DoubleData rd = ((DoubleData)(input));
		
		children[0].eval(state,thread,input,stack,individual,problem);
		result = rd.x;
		
		children[1].eval(state,thread,input,stack,individual,problem);
		rd.x = result - rd.x;
		
	}
    
}
