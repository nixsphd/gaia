package ie.nix.ec.ecj.tutorial2;

import ec.vector.*;
import ec.*;
import ec.util.*;

public class OurMutatorPipeline extends BreedingPipeline {

	private static final long serialVersionUID = 8422934159706046723L;
	
	//used only for our default base
    public static final String P_OURMUTATION = "our-mutation";

    public static final int NUM_SOURCES = 1;
    
    // Specifies the default parameter dabase for OurMutatorPipeline
    // We have to specify a default base, even though we never use it
    public Parameter defaultBase() { 
    	// Since we have no parameters that we're loading (we're 
    	// stealing the mutation-probability parameter from elsewhere), 
    	// there's no need for a base, but we define a simple one anyway
    	return VectorDefaults.base().push(P_OURMUTATION); 
    }

    // how many sources our breeding pipeline must have (it can be 
    // variable or a fixed number). In our case, we have just a 
    // single breeding source. 
    // Return 1 -- we only use one source
    public int numSources() { 
    	return NUM_SOURCES; 
    }
    

    // We're supposed to create a most _max_ and at least _min_ individuals,
    // drawn from our source and mutated, and stick them into slots in inds[]
    // starting with the slot inds[start].  Let's do this by telling our 
    // source to stick those individuals into inds[] and then mutating them
    // right there.  produce(...) returns the number of individuals actually put into inds[]
    public int produce(final int min, 
		       final int max, 
		       final int start,
		       final int subpopulation,
		       final Individual[] inds,
		       final EvolutionState state,
		       final int thread) {
    	
    	// The easy way to mutate is to just ask our source to stick those individuals 
    	// into inds[], and then mutate them. We also have to make sure that they're 
    	// they're copies -- we don't want to stick original individuals from the 
    	// previous population into the next one. This is a guarantee that all 
    	// BreedingPipelines must make. 
    	// grab individuals from our source and stick 'em right into inds.
        // we'll modify them from there
        int numberCreated = sources[0].produce(min,max,start,subpopulation,inds,state,thread);
        
        // should we bother?
    	if (!state.random[thread].nextBoolean(likelihood)) {
    		// DON'T produce children from source -- we already did
    		return reproduce(numberCreated, start, subpopulation, inds, state, thread, false);
    	}
    	
    	// it's time to actually mutate the individuals we got from our source. We'll
    	// first copy them if the source isn't a BreedingPipeline. Copying is done 
    	// through the clone() method. This is a method in Individual which is 
    	// guaranteed to creat e a deep clone of the individual, including its Fitness. 
    	// In some situations (such as crossing over GPIndividuals, w here cloning the 
    	// individual is very expensive) you may wish to light clone the Individual, 
    	// then clone parts of it as necessary. In that case you can judiciously use 
    	// lightClone() instead. But that's not very common.

        // clone the individuals if necessary -- if our source is a BreedingPipeline
        // they've already been cloned, but if the source is a SelectionMethod, the
        // individuals are actual individuals from the previous population
        if (!(sources[0] instanceof BreedingPipeline)) {
            for(int q=start; q < numberCreated+start; q++) {
                inds[q] = (Individual)(inds[q].clone());
            }
        }
        
        // Next we'll test to make certain that the individuals are in fact 
        // IntegerVectorIndividuals. It's a quick hack of a test, not a complete 
        // one, but usually the complete one is unnecessary. We'll also extract 
        // the IntegerVectorSpecies object the individuals are sharing, so we can 
        // get the per-gene mutationProbability value out of it.

        // Check to make sure that the individuals are IntegerVectorIndividuals and
        // grab their species.  For efficiency's sake, we assume that all the 
        // individuals in inds[] are the same type of individual and that they all
        // share the same common species -- this is a safe assumption because they're 
        // all breeding from the same subpopulation.
        if (!(inds[start] instanceof IntegerVectorIndividual)) {
            // uh oh, wrong kind of individual
            state.output.fatal("OurMutatorPipeline didn't get an " +
            "IntegerVectorIndividual.  The offending individual is " +
            "in subpopulation " + subpopulation + " and it's:" + inds[start]);
        }
        
        IntegerVectorSpecies species = (IntegerVectorSpecies)(inds[start].species);
 
        // Now we're ready to go through the individuals and mutate them. For each
        // gene in each individual, we'll flip a coin of per-gene mutationProbability, 
        // and change the sign of the gene if it comes up heads. We set the flag in 
        // the individual indicating that its evaluation is invalid and needs to be 
        // reevaluated. Finally, we'll return the number of individuals we created 
        // (n), and the class is done:

        // mutate 'em!
        for(int q=start; q < numberCreated+start; q++) {
            IntegerVectorIndividual i = (IntegerVectorIndividual)inds[q];
            for(int x=0; x<i.genome.length; x++) {
                if (state.random[thread].nextBoolean(species.mutationProbability(x))) {
                	i.genome[x] = -i.genome[x];
                }
	            // it's a "new" individual, so it's no longer been evaluated
	            i.evaluated=false;
            }
        }
        return numberCreated;
	}

	
}