package ie.nix.ec.ecj.tutorial4;

import ec.EvolutionState;
import ec.Problem;
import ie.nix.ec.ecj.tutorial4.MultiValuedRegression;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import ec.util.Parameter;

public class Y extends GPNode {

	private static final long serialVersionUID = 7317099394218212534L;

	public String toString() { 
    	return "y"; 
    }

	// Note that there are no children.
    public int expectedChildren() { 
    	return 0; 
    }
	
    // Now what we'll do is define in our Problem two public doubles called currentX and currentY. 
    // These are the values that these nodes return when evaluated. 
	@Override
	public void eval(
			EvolutionState state, 
			int thread, 
			GPData input,
			ADFStack stack, 
			GPIndividual individual, 
			Problem problem) {
	
        DoubleData rd = ((DoubleData)(input));
        rd.x = ((MultiValuedRegression)problem).currentY;
		
	}
    
}
