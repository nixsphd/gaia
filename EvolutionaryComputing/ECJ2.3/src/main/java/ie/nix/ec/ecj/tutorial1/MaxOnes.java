package ie.nix.ec.ecj.tutorial1;

import ec.*;
import ec.simple.*;
import ec.vector.*;

public class MaxOnes extends Problem implements SimpleProblemForm {

	private static final long serialVersionUID = -6029801962214240309L;

	public void evaluate(final EvolutionState state, 
						 final Individual ind, 
						 final int subpopulation, 
						 final int threadnum) {
		// don't evaluate the individual if it's already evaluated
		// Individuals contain two main pieces of data: evaluated, which indicates that they've
		// been evaluated already, and fitness, which stores their fitness object. Continuing:
		if (ind.evaluated) {
			return;   
		}

        // First we check to see if ind is a BitVectorIndividual -- otherwise something has 
        // gone terribly wrong. If something's wrong, we issue a fatal error through the 
        // state's Output facility. Messages (like fatal) all have one or two additional 
        // arguments where you can specify a Parameter that caused the fatal error, because 
        // it's very common to issue a fatal error on loading something from the ParameterDatabase 
        // and discovering it's incorrectly specified. Since this fatal error doesn't have anything 
        // to do with any specific parameter we know about, we pass in null. Continuing:
        if (!(ind instanceof BitVectorIndividual)) {
            state.output.fatal("Whoa!  It's not a BitVectorIndividual!!!",null);
        }

        // VectorIndividuals have all have an array called genome. The type of this array 
        // (int, boolean, etc.) varies depending on the subclass. For BitVectorIndividual, 
        // genome is a boolean array. We're simply counting the number of trues in it. Continuing:
        BitVectorIndividual bitVectorInd = (BitVectorIndividual)ind;
        int sum=0;        
        for(int x=0; x<bitVectorInd.genome.length; x++) {
        	sum += (bitVectorInd.genome[x] ? 1 : 0);
        }
        
        // Make sure we have a SimpleFitness...
        if (!(bitVectorInd.fitness instanceof SimpleFitness)) {
            state.output.fatal("Whoa!  It's not a SimpleFitness!!!",null);
        }
        
        // Note that Fitness itself doesn't actually contain any methods for setting the fitness, 
        // only for getting the fitness. This is because different Fitness subtypes operate differently. 
        // In order to set a fitness, we must assume that it's some particular Fitness, in this case,
        // SimpleFitness. Just in case, we double-check first. [If you're just hacking something up
        // fast and you know that you're using a given kind of Individual and a given kind of Fitness, 
        // the double-checking is probably unnecessary, but if you change your Individual or Fitness 
        // in your parameters, your code may break in an icky way of course].
        ((SimpleFitness)bitVectorInd.fitness).setFitness(state,
                // ...the fitness...
                ((double)sum)/bitVectorInd.genome.length,
                ///... is the individual ideal?  Indicate here...
                sum == bitVectorInd.genome.length);
        bitVectorInd.evaluated = true;
	}

}
