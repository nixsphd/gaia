package ie.nix.evolutionary_algorithms.gp;

import java.util.Arrays;

import ie.nix.util.Randomness;
import ie.nix.evolutionary_algorithms.gp.GeneticProgramming;
import ie.nix.evolutionary_algorithms.gp.GeneticProgramming.KozaTableau;
import ie.nix.evolutionary_algorithms.gp.Primitives.Function;
import ie.nix.evolutionary_algorithms.gp.Primitives.Primitive;
import ie.nix.evolutionary_algorithms.gp.Programs.Program;
import ie.nix.evolutionary_algorithms.gp.functions.Add;
import ie.nix.evolutionary_algorithms.gp.functions.Divide;
import ie.nix.evolutionary_algorithms.gp.functions.Multiply;
import ie.nix.evolutionary_algorithms.gp.functions.Subtract;
import ie.nix.evolutionary_algorithms.gp.genetic_operators.BinaryTournamentSelection;
import ie.nix.evolutionary_algorithms.gp.genetic_operators.Crossover;
import ie.nix.evolutionary_algorithms.gp.genetic_operators.FunctionMutation;
import ie.nix.evolutionary_algorithms.gp.terminals.Constant;
import ie.nix.evolutionary_algorithms.gp.terminals.Variable;
import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Unit test for simple App.
 */
public class GeneticProgramingTest extends TestCase
{

	private boolean overwriteExpectedFiles = false;
	protected String expectedOutputDirName = "src/test/ie/nix/evolutionary_algorithms/gp/";
	
    @Override
	protected void setUp() throws Exception {
    	Randomness.setSeed(1);
    	Primitives.nextID = 1;
	}
    
	public void testFunctionAdd() {
		Function function = new Add(new Constant(3.0),new Constant(-2.0));
		double expectedEvaluation = 1;
		
		testFunction(function, expectedEvaluation);
		
	}
	
	public void testFunctionSubtract() {
		Function function = new Subtract(new Constant(3.0),new Constant(-2.0));
		double expectedEvaluation = 5;
		
		testFunction(function, expectedEvaluation);
		
	}
	
	public void testFunctionMultiply() {
		Function function = new Multiply(new Constant(3.0),new Constant(-2.0));
		double expectedEvaluation = -6;
		
		testFunction(function, expectedEvaluation);
		
	}
	
	public void testFunctionDivide() {
		Function function = new Divide(new Constant(3.0),new Constant(-2.0));
		double expectedEvaluation = 3.0/-2.0;
		
		testFunction(function, expectedEvaluation);
		
	}
	
	public void testFunctionDivideByZero() {
		Function function = new Divide(new Constant(3.0),new Constant(0.0));
		double expectedEvaluation = Double.POSITIVE_INFINITY;
		
		testFunction(function, expectedEvaluation);
		
		function = new Divide(new Constant(3.0),new Constant(-0.0));
		expectedEvaluation = Double.NEGATIVE_INFINITY;
		
		testFunction(function, expectedEvaluation);
		
	}
	
	public void testFunction(Function function, double expectedEvaluation) {
		
    	double actualEvaluation = function.evaluate();
    	
//    	System.out.println("NDB::testFunction()~actualEvaluation="+actualEvaluation);  	
    	Assert.assertEquals(expectedEvaluation, actualEvaluation);
    	
	}

	public void testGetParentRoot() {
		Function root = new Multiply(
    			new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));

		Function expectedParent = null;
		Primitive primitive = root;

		testGetParent(root, primitive, expectedParent);
	}
	
	public void testGetParentMiddle() {
		Function root = new Multiply(
    			new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));

		Function expectedParent = root;
		Primitive primitive = expectedParent.getInput(0);;

		testGetParent(root, primitive, expectedParent);
	}
		
	public void testGetParentLeaf() {
		Function root = new Multiply(
				new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));
	
		Function expectedParent = ((Function)root.getInput(0));
		Primitive primitive = expectedParent.getInput(0);
	
		testGetParent(root, primitive, expectedParent);
	}

	public void testGetParent(Function root, Primitive primitive, Function expectedParent) {
		
    	Primitive actualParent = primitive.getParent();
//    	System.out.println("NDB::testGetParent()~actualParent="+toString((Function)actualParent));  	    	
//    	System.out.println("NDB::testGetParent()~expectedParent="+toString(expectedParent));  	
    	
    	Assert.assertTrue(expectedParent == actualParent);
    	
	}
	
	public void testGetDepthRoot() {
		Function root = new Multiply(
				new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));
	
		int expectedDepth = 1;
		Primitive primitive = root;
	
		testGetDepth(root, primitive, expectedDepth);
	}
	
	public void testGetDepthMiddle() {
		Function root = new Multiply(
				new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));
	
		int expectedDepth = 2;
		Primitive primitive = root.getInput(0);
	
		testGetDepth(root, primitive, expectedDepth);
	}
	
	public void testGetDepthLeaf() {
		Function root = new Multiply(
				new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));
	
		int expectedDepth = 3;
		Primitive primitive = ((Function)root.getInput(0)).getInput(0);
	
		testGetDepth(root, primitive, expectedDepth);
	}

	public void testGetDepthNone() {
		Function function = new Multiply();
	
		int expectedDepth = 1;
		Primitive primitive = function;
	
		testGetDepth(function, primitive, expectedDepth);
	}
	
	public void testGetDepth(Function root, Primitive primitive, int expectedDepth) {
		
    	int actualDepth = primitive.getDepth();
//    	System.out.println("NDB::testGetDepth()~actualParent="+actualDepth);  	    	
//    	System.out.println("NDB::testGetDepth()~expectedParent="+expectedDepth);  	
    	
    	Assert.assertTrue(expectedDepth == actualDepth);
    	
	}
	
	public void testGetPrimitivesAtDepth1() {
		Function root = new Multiply(
				new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));
	
		int depth = 1;
		Primitive[] expectedPrimitives = {root};
	
		testGetPrimitivesAtDepth(root, depth, expectedPrimitives);
	}
	
	public void testGetPrimitivesAtDepth2() {
		Function root = new Multiply(
				new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));
	
		int depth = 2;
		Primitive[] expectedPrimitives = {root.getInput(0), root.getInput(1)};
	
		testGetPrimitivesAtDepth(root, depth, expectedPrimitives);
	}
	
	public void testGetPrimitivesAtDepth3() {
		Function root = new Multiply(
				new Add(
					new Constant(1.0),
					new Constant(2.0)),
				new Subtract(
					new Constant(3.0),
					new Constant(4.0)));
	
		int depth = 3;
		Primitive[] expectedPrimitives = {
				((Function)root.getInput(0)).getInput(0), 
				((Function)root.getInput(0)).getInput(1), 
				((Function)root.getInput(1)).getInput(0), 
				((Function)root.getInput(1)).getInput(1)};
	
		testGetPrimitivesAtDepth(root, depth, expectedPrimitives);
	}
	
	public void testGetPrimitivesAtDepth(Function root, int depth, Primitive[] expectedPrimitives) {
		
    	Primitive[] actualePrimitives = root.getPrimitivesAtDepth(depth).toArray(new Primitive[0]);
//    	System.out.println("NDB::testGetPrimitivesAtDepth()~actualePrimitives="+toString(actualePrimitives));  	    	
//    	System.out.println("NDB::testGetPrimitivesAtDepth()~expectedPrimitives="+toString(expectedPrimitives));  	
    	
    	Assert.assertTrue(TestHelper.equals(expectedPrimitives, actualePrimitives));
    	
	}
	
	
	
	public void testBuildFullRandomTreeDepth1() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.maxProgramDepth = 1;
		kozaTableau.numberOfPrograms = 0;
		
    	Primitive expectedFunction = new Constant(3.0);

		testBuildFullRandomTree(kozaTableau, expectedFunction);
	}

	public void testBuildFullRandomTreeDepth2() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.maxProgramDepth = 2;
		kozaTableau.numberOfPrograms = 0;
		
    	Function expectedFunction = new Multiply(
    			new Constant(-2.0),
    			new Constant(1.0));

    	
		testBuildFullRandomTree(kozaTableau, expectedFunction);
	}
	
	public void testBuildFullRandomTreeDepth3() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.maxProgramDepth = 3;
		kozaTableau.numberOfPrograms = 0;
		
    	Function expectedFunction = new Multiply(
    			new Add(
    					new Constant(1.0),
    					new Constant(3.0)),
    				new Add(
    					new Constant(-2.0),
    					new Constant(-4.0)));

		testBuildFullRandomTree(kozaTableau, expectedFunction);
	}
	
	public void testBuildFullRandomTreeDepthVariables() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 10;
    	kozaTableau.constants = new double[]{};
		kozaTableau.maxProgramDepth = 3;
		kozaTableau.numberOfPrograms = 0;
		
    	Function expectedFunction = new Multiply(
    			new Add(
    					new Variable(8),
    					new Variable(4)),
    				new Add(
    					new Variable(5),
    					new Variable(5)));

		testBuildFullRandomTree(kozaTableau, expectedFunction);
	}
	
	public void testBuildFullRandomTreeDepthMonster() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 100;
		kozaTableau.maxProgramDepth = 5;
		kozaTableau.numberOfPrograms = 0;
		
    	Function expectedFunction = new Multiply(
    			new Add(
    					new Subtract(
    						new Subtract(
    							new Variable(99),
    							new Variable(20)),
    						new Subtract(
    							new Variable(41),
    							new Variable(77))),
    					new Multiply(
    						new Add(
    							new Variable(23),
    							new Variable(41)),
    						new Add(
    							new Variable(40),
    							new Variable(65)))),
    				new Divide(
    					new Divide(
    						new Divide(
    							new Variable(27),
    							new Variable(45)),
    						new Divide(
    							new Variable(14),
    							new Variable(66))),
    					new Subtract(
    						new Subtract(
    							new Variable(53),
    							new Variable(99)),
    						new Add(
    							new Variable(69),
    							new Variable(20)))));

		testBuildFullRandomTree(kozaTableau, expectedFunction);
	}
	
	public void testBuildFullRandomTree(KozaTableau kozaTableau, 
			Primitive expectedFunction) {
    	GeneticProgramming gp = new GeneticProgramming(kozaTableau);
		
    	Program program = new Program(kozaTableau.numberOfVariables);
    	program.buildFullRandomTree(gp.primitives, kozaTableau.maxProgramDepth);
    	Primitive actulFunction = program.root;

//    	System.out.println("NDB::testBuildFullRandomTree()~expectedFunction=\n"+TestHelper.toString(expectedFunction)+";");
//    	System.out.println("NDB::testBuildFullRandomTree()~actulFunction=\n"+TestHelper.toString(actulFunction)+";");
    	
    	Assert.assertTrue(TestHelper.equals(expectedFunction, actulFunction));
    	
    	if (program.getRoot() instanceof Function) {
        	int actualDepth = ((Function)program.getRoot()).getMaxDepth();
//        	System.out.println("NDB::testBuildFullRandomTree()~maxProgramDepth="+kozaTableau.maxProgramDepth);
//        	System.out.println("NDB::testBuildFullRandomTree()~actualDepth="+actualDepth);
        	Assert.assertEquals(kozaTableau.maxProgramDepth, actualDepth);
        	
    	}
    	
    }
	
	public void testCloneProgramDepth1() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.numberOfPrograms = 0;
		
		Primitive tree = new Constant(3.0);

		testCloneProgram(kozaTableau, tree);
	}
	
	public void testCloneProgramDepth2() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.numberOfPrograms = 0;
		
		Primitive tree = new Multiply(
    			new Constant(-2.0),
    			new Constant(1.0));

		testCloneProgram(kozaTableau, tree);
	}
	
	public void testCloneProgramDepth3() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 10;
		kozaTableau.numberOfPrograms = 0;
		
		Primitive tree = new Multiply(
    			new Add(
    					new Variable(8),
    					new Variable(4)),
    				new Add(
    					new Variable(5),
    					new Variable(5)));

		testCloneProgram(kozaTableau, tree);
	}
	
	public void testCloneProgramDepthMonster() {
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.numberOfVariables = 100;
		kozaTableau.numberOfPrograms = 0;
		
		Primitive tree = new Multiply(
    			new Add(
    					new Subtract(
    						new Subtract(
    							new Variable(99),
    							new Variable(20)),
    						new Subtract(
    							new Variable(41),
    							new Variable(77))),
    					new Multiply(
    						new Add(
    							new Variable(23),
    							new Variable(41)),
    						new Add(
    							new Variable(40),
    							new Variable(65)))),
    				new Divide(
    					new Divide(
    						new Divide(
    							new Variable(27),
    							new Variable(45)),
    						new Divide(
    							new Variable(14),
    							new Variable(66))),
    					new Subtract(
    						new Subtract(
    							new Variable(53),
    							new Variable(99)),
    						new Add(
    							new Variable(69),
    							new Variable(20)))));

		testCloneProgram(kozaTableau, tree);
	}
	
	public void testCloneProgram(KozaTableau kozaTableau, Primitive tree) {
    	GeneticProgramming gp = new GeneticProgramming(kozaTableau);
    	
    	Program expectedProgram = new Program(kozaTableau.numberOfVariables); 
    	expectedProgram.root = tree; 
    	
    	Program actualProgram = new Program(kozaTableau.numberOfVariables);
    	actualProgram.cloneTree(gp.primitives, expectedProgram);
//    	System.out.println("NDB::testCloneTree()~actualProgram=\n"+toString(actualProgram.root)+";");
//    	System.out.println("NDB::testCloneTree()~expectedProgram=\n"+toString(expectedProgram.root)+";");
    	
    	Assert.assertTrue(TestHelper.equals(expectedProgram.root, actualProgram.root));
    	
    }

	public void testPrintProgram() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.numberOfPrograms = 1;
		String expectedOutputFileName = expectedOutputDirName+"testPrintProgram.dot";
		
		testPrintProgram(kozaTableau, expectedOutputFileName);
	}
	
	public void testPrintProgram(KozaTableau kozaTableau, String expectedOutputFileName) {
		String expectedProgramAsAString = TestHelper.readFile(expectedOutputFileName);
		
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);

		Program program = gp.getPrograms().getProgram(0);
		
		String actualProgramAsAString = TestHelper.toDOTString(program);
//    	System.out.println("NDB::testPrintProgram()~actualProgramAsAString=\n"+actualProgramAsAString);
//    	System.out.println("NDB::testPrintProgram()~expectedProgramAsAString=\n"+expectedProgramAsAString);
		TestHelper.writeFile(expectedOutputFileName+".act", actualProgramAsAString);
		
		if (overwriteExpectedFiles) {
			TestHelper.writeFile(expectedOutputFileName,TestHelper.toDOTString(program));			
		}
    	Assert.assertEquals(expectedProgramAsAString, actualProgramAsAString);
		
	}
	
	public void testEvaluateProgram() {
		double[] variableValues = {1.0,2.0,3.0};
		
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = variableValues.length;
		kozaTableau.numberOfPrograms = 0;
		double expectedEvaluation = -1.0;
		
		testEvaluateProgram(kozaTableau, variableValues, expectedEvaluation);
	}
	
	public void testEvaluateProgram(KozaTableau kozaTableau, double[] variableValues, double expectedEvaluation) {
		
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);

		Program program = gp.getPrograms().initRandomProgram();
		
		double actualEvaluation = program.evaluate(variableValues);
//    	System.out.println("NDB::testEvaluateProgram()~actualEvaluation=\n"+actualEvaluation);
//    	System.out.println("NDB::testEvaluateProgram()~expectedEvaluation=\n"+expectedEvaluation);
		
//		String actualProgramAsAString = TestHelper.toString(program);
//    	System.out.println("NDB::testEvaluateProgram()~actualProgramAsAString=\n"+actualProgramAsAString);
//    	writeFile("src/test/ie/nix/evolutionary_algorithms/gp/"+"testEvaluateProgram.dot", actualProgramAsAString);
    	
    	Assert.assertEquals(expectedEvaluation, actualEvaluation);
		
	}
	
	public void testMultiEvaluateProgram() {
		double[][] variableValues = {{1.0},{2.0},{3.0}};
		
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = variableValues[0].length;
    	
		double[] expectedEvaluation = {-10.0,-5.0,0.0};
		
		testMultiEvaluateProgram(kozaTableau, variableValues, expectedEvaluation);
	}
	
	public void testMultiEvaluateProgram(KozaTableau kozaTableau, double[][] variableValues, double[] expectedEvaluation) {
		
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
		
		Program program = gp.getPrograms().initRandomProgram();
		
		double[] actualEvaluation = program.evaluate(variableValues);
//    	System.out.println("NDB::testMultiEvaluateProgram()~actualEvaluation=\n"+TestHelper.toString(actualEvaluation));
//    	System.out.println("NDB::testMultiEvaluateProgram()~expectedEvaluation=\n"+TestHelper.toString(expectedEvaluation));
//		String actualProgramAsAString = TestHelper.toString(program);
//    	System.out.println("NDB::testMultiEvaluateProgram()~actualProgramAsAString=\n"+actualProgramAsAString);
//    	TestHelper.writeFile("src/test/ie/nix/evolutionary_algorithms/gp/"+"testMultiEvaluateProgram.dot.act", actualProgramAsAString);
    	
    	Assert.assertTrue(Arrays.equals(expectedEvaluation, actualEvaluation));
		
	}
	
	public void testEvaluatePrograms() {
		double[][] variableValues = { 
				{1.0},
				{2.0},
				{3.0}};
		
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 1;
		kozaTableau.maxProgramDepth = 3;

    	kozaTableau.numberOfVariables = variableValues[0].length;
		double[] desiredResults = {
				2.0,
				4.0,
				6.0};
		double[] expectedFitnesses = {44.0};
		
		testEvaluatePrograms(kozaTableau, variableValues, desiredResults, expectedFitnesses);
	}
	
	public void testEvaluateTwoPrograms() {
		double[][] variableValues = { 
				{1.0},
				{2.0},
				{3.0}};
		
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = variableValues[0].length;
		double[] desiredResults = {
				2.0,
				4.0,
				6.0};
		double[] expectedFitnesses = {44.0, 0.0};
		
		testEvaluatePrograms(kozaTableau, variableValues, desiredResults, expectedFitnesses);
	}
	
	public void testEvaluateThreePrograms() {
		double[][] variableValues = { 
				{1.0},
				{2.0},
				{3.0}};
		
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 3;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = variableValues[0].length;
		double[] desiredResults = {
				2.0,
				4.0,
				6.0};
		double[] expectedFitnesses = {44.0, 0.0, 4.0};
		
		testEvaluatePrograms(kozaTableau, variableValues, desiredResults, expectedFitnesses);
	}
	
	public void testEvaluatePrograms(KozaTableau kozaTableau, double[][] inputs, double[] desiredResults, double[] expectedFitnesses) {
		
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
		
		gp.evaluate(inputs, desiredResults);
		
		double[] actualFitnesses = new double[kozaTableau.numberOfPrograms];
		for (int p = 0; p < kozaTableau.numberOfPrograms; p++) {
			Program program = gp.getPrograms().getProgram(p);
			actualFitnesses[p] = program.fitness;

//			String actualProgramAsAString = TestHelper.toString(program);
//	    	System.out.println("NDB::testMultiEvaluateProgram()~actualProgramAsAString=\n"+actualProgramAsAString);
		}
		
//    	System.out.println("NDB::testEvaluatePrograms()~actualFitnesses=\n"+TestHelper.toString(actualFitnesses));
//    	System.out.println("NDB::testEvaluatePrograms()~expectedFitnesses=\n"+TestHelper.toString(expectedFitnesses));
    	
    	Assert.assertTrue(Arrays.equals(expectedFitnesses, actualFitnesses));
		
	}
	
	public void testBestProgram() {
		double[][] variableValues = { 
				{1.0},
				{2.0},
				{3.0}};
		
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 3;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = variableValues[0].length;
		double[] desiredResults = {
				2.0,
				4.0,
				6.0};
		double expectedBestProgramFitness = 0.0;
		
		testBestProgram(kozaTableau, variableValues, desiredResults, expectedBestProgramFitness);
	}
	
	public void testBestProgram(KozaTableau kozaTableau, double[][] inputs, double[] desiredResults, double expectedBestProgramFitness) {
		
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
		
		gp.evaluate(inputs, desiredResults);
		
		double actualBestProgramFitness = gp.getPrograms().getBestProgram().fitness;
//		System.out.println("NDB::testBestProgram()~programs=\n"+TestHelper.toString(gp.getPrograms().programs));
//    	
//    	System.out.println("NDB::testBestProgram()~actualBestProgramFitness=\n"+actualBestProgramFitness);
//    	System.out.println("NDB::testBestProgram()~expectedBestProgramFitness=\n"+expectedBestProgramFitness);
    	
    	Assert.assertEquals(expectedBestProgramFitness, actualBestProgramFitness);
		
	}
	
	// TODO - This test doesn't work cause it passes even if autostopis off. I need to fix this.
	//        Probably should add a generationsNumber variable to GP which I can check.
	public void testAutoStop() {
		double[][] variableValues = { 
				{1.0},
				{2.0},
				{3.0}};
		
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.autostop = true;
		kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 5;
		kozaTableau.numberOfPrograms = 3;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = variableValues[0].length;
		double[] desiredResults = {
				2.0,
				4.0,
				6.0};
		int expectedGeneration = 1;
		
		testAutoStop(kozaTableau, variableValues, desiredResults, expectedGeneration);
	}
	
	public void testAutoStop(KozaTableau kozaTableau, double[][] inputs, double[] desiredResults, int expectedGeneration) {
		
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
		int actualGeneration = gp.evolve(inputs, desiredResults);
		
//		System.out.println("NDB::testBestProgram()~actualGeneration=\n"+actualGeneration);
//    	System.out.println("NDB::testBestProgram()~expectedGeneration=\n"+expectedGeneration);
    	
    	Assert.assertEquals(expectedGeneration, actualGeneration);
		
	}
	
	public void testCalculateErrorFitnessPerfect() {
		KozaTableau kozaTableau = new KozaTableau();
		double[] actualResults = {1.0, 2.0, 3.0}; 
		double[] desiredResults = {1.0, 2.0, 3.0};
		double expectedFitness = 0;
	
		testCalculateErrorFitness(kozaTableau, actualResults, desiredResults, expectedFitness);
	}
	
	public void testCalculateErrorFitnessOffBy10() {
		KozaTableau kozaTableau = new KozaTableau();
		double[] actualResults = {1.0, 2.0, 3.0}; 
		double[] desiredResults = {11.0, 12.0, 13.0};
		double expectedFitness = 30;
	
		testCalculateErrorFitness(kozaTableau, actualResults, desiredResults, expectedFitness);
	}
	
	public void testCalculateErrorFitnessOffOnlyInfinity() {
		KozaTableau kozaTableau = new KozaTableau();
		double[] actualResults = {Double.NEGATIVE_INFINITY}; 
		double[] desiredResults = {1.0};
		double expectedFitness = Double.POSITIVE_INFINITY;
	
		testCalculateErrorFitness(kozaTableau, actualResults, desiredResults, expectedFitness);
	}
	
	public void testCalculateErrorFitnessOffOneInfinity() {
		KozaTableau kozaTableau = new KozaTableau();
		double[] actualResults = {Double.NEGATIVE_INFINITY, 2.0, 3.0}; 
		double[] desiredResults = {1.0, 12.0, 13.0};
		double expectedFitness = Double.POSITIVE_INFINITY;
	
		testCalculateErrorFitness(kozaTableau, actualResults, desiredResults, expectedFitness);
	}
	public void testCalculateErrorFitnessOffOneNaN() {
		KozaTableau kozaTableau = new KozaTableau();
		double[] actualResults = {Double.NaN, 2.0, 3.0}; 
		double[] desiredResults = {1.0, 12.0, 13.0};
		double expectedFitness = Double.NaN;
	
		testCalculateErrorFitness(kozaTableau, actualResults, desiredResults, expectedFitness);
	}
	
	public void testCalculateErrorFitness(KozaTableau kozaTableau, double[] actualResults, double[] desiredResults, 
			double expectedFitness) {
		
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
	
		double actualFitness = gp.calculateErrorFitness(actualResults, desiredResults);		
//    	System.out.println("NDB::testCalculateErrorFitness()~actualFitness="+actualFitness);
//    	System.out.println("NDB::testCalculateErrorFitness()~expectedFitness="+expectedFitness);
    	
    	Assert.assertEquals(expectedFitness, actualFitness);
	}
	
	public void testBestFitnessInfinity() {
		KozaTableau kozaTableau = new KozaTableau();
		Program[] programs = {
				new Program(1),
				new Program(1),
				new Program(1),
		};
		programs[0].setFitness(Double.POSITIVE_INFINITY);
		programs[1].setFitness(100.0);
		programs[2].setFitness(0.0);
	
		testBestFitness(kozaTableau, programs, programs[2]);
	}
	
	public void testBestFitnessNaN() {
		KozaTableau kozaTableau = new KozaTableau();
		Program[] programs = {
				new Program(1),
				new Program(1),
				new Program(1),
		};
		programs[0].setFitness(Double.NaN);
		programs[1].setFitness(100.0);
		programs[2].setFitness(0.0);
	
		testBestFitness(kozaTableau, programs, programs[2]);
	}
	
	public void testBestFitness(KozaTableau kozaTableau, Program[] programs, Program expectedBestProgram) {
		
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
		gp.programs.programs = Arrays.asList(programs);
	
		Program actualBestProgram = gp.getPrograms().getBestProgram();	
		
//    	System.out.println("NDB::testBestFitness()~actualBestProgram="+actualBestProgram.hashCode());
//    	System.out.println("NDB::testBestFitness()~expectedBestProgram="+expectedBestProgram.hashCode());
    	
    	Assert.assertEquals(expectedBestProgram.hashCode(), actualBestProgram.hashCode());
	}
	
	public void testFunctionMutationNoMutation() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 1;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
		double functionMutationProbability = 0.0;
		String expectedOutputFileName = expectedOutputDirName+"testFunctionMutationNoMutation.dot";
		
		testFunctionMutation(kozaTableau, functionMutationProbability, expectedOutputFileName);
	}
	
	public void testFunctionMutationFullMutationDepth1() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 1;
		kozaTableau.maxProgramDepth = 1;
    	kozaTableau.numberOfVariables = 1;
		double functionMutationProbability = 1.0;
		String expectedOutputFileName = expectedOutputDirName+"testFunctionMutationFullMutationDepth1.dot";
		
		testFunctionMutation(kozaTableau, functionMutationProbability, expectedOutputFileName);
	}
	
	public void testFunctionMutationFullMutationDepth2() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 1;
		kozaTableau.maxProgramDepth = 2;
    	kozaTableau.numberOfVariables = 1;
		double functionMutationProbability = 1.0;
		String expectedOutputFileName = "src/test/ie/nix/evolutionary_algorithms/gp/testFunctionMutationFullMutationDepth2.dot";
		
		testFunctionMutation(kozaTableau, functionMutationProbability, expectedOutputFileName);
	}
	
	public void testFunctionMutationFullMutationDepth3() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.constants = new double[]{};
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 1;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
		double functionMutationProbability = 1.0;
		String expectedOutputFileName = expectedOutputDirName+"testFunctionMutationFullMutationDepth3.dot";
		
		testFunctionMutation(kozaTableau, functionMutationProbability, expectedOutputFileName);
	}

	public void testFunctionMutation(KozaTableau kozaTableau, double functionMutationProbability, String expectedOutputFileName) {
		
		String expectedProgramsAsAString = TestHelper.readFile(expectedOutputFileName);
    	
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
		FunctionMutation functionMutation = new FunctionMutation(functionMutationProbability);		
//    	writeFile(expectedOutputFileName,toDOTString(gp.programs[0]));
    	
		// Apply the operator
		functionMutation.operate(gp.programs);
		String actualProgramsAsAString = TestHelper.toDOTString(gp.programs.programs);
		
//    	System.out.println("NDB::testPrintProgram()~actualProgramsAsAString=\n"+actualProgramsAsAString);
//    	System.out.println("NDB::testPrintProgram()~expectedProgramsAsAString=\n"+expectedProgramsAsAString);
//    	TestHelper.writeFile(expectedOutputFileName+".act",actualProgramsAsAString);
		
		if (overwriteExpectedFiles) {
			TestHelper.writeFile(expectedOutputFileName,TestHelper.toDOTString(gp.programs.programs));
			
		}
    	Assert.assertEquals(expectedProgramsAsAString, actualProgramsAsAString);
		
	}
	
	public void testBinaryTournamentSelectionAlwaysSelectFittest2() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.probabilotyOfSelectFittest = 1.0;
		double[] programFitnesses = {100.0, 0.0};
		double[] expectedSelectedFinesses = {0.0,0.0};
		
		testBinaryTournamentSelection(kozaTableau, programFitnesses, expectedSelectedFinesses);
	}
	
	public void testBinaryTournamentSelectionAlwaysSelectUnfittest2() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.probabilotyOfSelectFittest = 0.0;
		double[] programFitnesses = {100.0, 0.0};
		double[] expectedSelectedFinesses = {0.0,100.0}; // First Tournament is P1 against itself
		
		testBinaryTournamentSelection(kozaTableau, programFitnesses, expectedSelectedFinesses);
	}
	
	public void testBinaryTournamentSelection5050() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.probabilotyOfSelectFittest =  0.5;
		double[] programFitnesses = {100.0, 0.0};
		double[] expectedSelectedFinesses = {0.0,0.0}; // First Tournament is P1 against itself, second selects fittest
		
		testBinaryTournamentSelection(kozaTableau, programFitnesses, expectedSelectedFinesses);
	}
	
	public void testBinaryTournamentSelectionAlwaysSelectFittest10() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 10;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
		kozaTableau.probabilotyOfSelectFittest = 1.0;
		double[] programFitnesses = {900.0, 800.0, 700.0, 600.0, 500.0, 400.0, 300.0, 200.0, 100.0, 0.0};
		double[] expectedSelectedFinesses = {0.0,200.0,200.0,100.0,0.0,200.0,400.0,300.0,400.0,100.0};
		
		testBinaryTournamentSelection(kozaTableau, programFitnesses, expectedSelectedFinesses);
	}
	
	public void testBinaryTournamentSelection(KozaTableau kozaTableau, double[] programFitnesses, double[] expectedSelectedFinesses) {

		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
		BinaryTournamentSelection binaryTournamentSelection = new BinaryTournamentSelection(kozaTableau.numberOfPrograms, kozaTableau.probabilotyOfSelectFittest);
    	
		// Assign the fitnesses
		for (int p = 0; p < kozaTableau.numberOfPrograms; p++) {
			gp.getPrograms().getProgram(p).fitness = programFitnesses[p];
		}
		
		// Apply the operator
		binaryTournamentSelection.operate(gp.programs);
		Program[] actualSelectedPrograms = gp.getPrograms().getPrograms().toArray(new Program[kozaTableau.numberOfPrograms]);
		double[] actualSelectedFinesses = new double[actualSelectedPrograms.length];
		for (int p = 0; p < expectedSelectedFinesses.length; p++) {
			actualSelectedFinesses[p] = actualSelectedPrograms[p].getFitness();
		}

//		System.out.println("NDB::testBinaryTournamentSelection()~actualSelectedFinesses="+toString(actualSelectedFinesses));
//		System.out.println("NDB::testBinaryTournamentSelection()~expectedSelectedFinesses="+toString(expectedSelectedFinesses));
		
		Assert.assertTrue(Arrays.equals(expectedSelectedFinesses, actualSelectedFinesses));
		
	}
	
	public void testCrossoverNoProbabibility() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{1};
		kozaTableau.probabilotyOfCrossover = 0.0;
		String expectedOutputFileName = expectedOutputDirName+"testCrossoverNoProbabibility.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossover2PD1() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 1;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{1};
		kozaTableau.probabilotyOfCrossover = 1;
		String expectedOutputFileName = expectedOutputDirName+"testCrossover2PD1.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossover2PD2() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 2;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{1};
		kozaTableau.probabilotyOfCrossover = 1;
		String expectedOutputFileName = expectedOutputDirName+"testCrossover2PD2.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossover2PD3() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{1};
		kozaTableau.probabilotyOfCrossover = 1;
		String expectedOutputFileName = expectedOutputDirName+"testCrossover2PD3.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossover2PD4() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 4;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{1};
		kozaTableau.probabilotyOfCrossover = 1;
		String expectedOutputFileName = expectedOutputDirName+"testCrossover2PD4.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossover2PD4Temp() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 4;
    	kozaTableau.numberOfVariables = 0;
    	kozaTableau.constants = new double[]{1,2,3,4,5};
		kozaTableau.probabilotyOfCrossover = 1;
		String expectedOutputFileName = expectedOutputDirName+"testCrossover2PD4.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossover2PD32G() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 2;
		kozaTableau.numberOfPrograms = 2;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{1};
		kozaTableau.probabilotyOfCrossover = 1;
		String expectedOutputFileName = expectedOutputDirName+"testCrossover2PD32G.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossover10PD3() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 10;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{1};
		kozaTableau.probabilotyOfCrossover = 1;
		String expectedOutputFileName = expectedOutputDirName+"testCrossover10PD3.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossover11PD3() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 1;
		kozaTableau.numberOfPrograms = 11;
		kozaTableau.maxProgramDepth = 3;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{1};
		kozaTableau.probabilotyOfCrossover = 1;
		String expectedOutputFileName = expectedOutputDirName+"testCrossover11PD3.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}
	
	public void testCrossoverMonster() {
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.maxNumberOfGenerations = 20;
		kozaTableau.numberOfPrograms = 8;
		kozaTableau.maxProgramDepth = 5;
    	kozaTableau.numberOfVariables = 5;
    	kozaTableau.constants = new double[]{-2,-1,0,1,2};
		kozaTableau.probabilotyOfCrossover = 1.0;
		String expectedOutputFileName = expectedOutputDirName+"testCrossoverMonster.dot";
		
		testCrossover(kozaTableau, expectedOutputFileName);
	}

	public void testCrossover(KozaTableau kozaTableau, String expectedOutputFileName) {
		
		String expectedProgramsAsAString = TestHelper.readFile(expectedOutputFileName);
    	
		GeneticProgramming gp = new GeneticProgramming(kozaTableau);
//    	writeFile(expectedOutputFileName+".init",toDOTString(gp.programs));
//    	System.out.println("NDB::testCrossover()~initialPrograms=\n"+toDOTString(gp.programs));
    	
		Crossover crossover = new Crossover(kozaTableau.probabilotyOfCrossover);
    	
		// Apply the operator
    	while (kozaTableau.maxNumberOfGenerations > 0) {
    		crossover.operate(gp.programs);
    		kozaTableau.maxNumberOfGenerations--;
    	}
		String actualProgramsAsAString = TestHelper.toDOTString(gp.programs.programs.toArray(new Program[kozaTableau.numberOfPrograms]));
		
//    	System.out.println("NDB::testCrossover()~actualProgramsAsAString=\n"+actualProgramsAsAString);
    	System.out.println("NDB::testCrossover()~expectedProgramsAsAString=\n"+expectedProgramsAsAString);
    	TestHelper.writeFile(expectedOutputFileName+".act",actualProgramsAsAString);
    	
		if (overwriteExpectedFiles) {
			TestHelper.writeFile(expectedOutputFileName,TestHelper.toDOTString(gp.programs.programs));			
		}
    	Assert.assertEquals(expectedProgramsAsAString, actualProgramsAsAString);
		
	}

}
