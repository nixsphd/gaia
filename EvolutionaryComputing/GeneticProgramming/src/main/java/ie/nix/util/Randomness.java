package ie.nix.util;

import java.util.Random;

public class Randomness {
	
	private static Random rng;
	
	public static void setSeed(int seed) {
		rng = new Random(seed);
	}

	public static int nextBinary() {
		int nextRandomGene = rng.nextInt(2);
//		System.out.println("NDB::nextRandomGene() ~Generated="+nextRandomGene);
		return nextRandomGene;		
	}	
	
	public static double nextDouble() {
		double nextRandomDouble = rng.nextDouble();
		return nextRandomDouble;
	}

	public static double nextDouble(double start, double end) {
		double nextRandomDouble = (rng.nextDouble() * (end - start)) + start;
		return nextRandomDouble;
	}

	public static int nextInt(int start, int end) {
	    int nextInt = rng.nextInt(end-start) + start;
		return nextInt;
	}
	
	public static int nextInt(int positiveBound) {
	    int nextInt = rng.nextInt(positiveBound);
		return nextInt;
	}
	
	public static double nextGaussian() {
		double nextGaussian = rng.nextGaussian();
		return nextGaussian;
	}
	
	public static double nextGaussian(double standardDeviation) {
		double nextGaussian = rng.nextGaussian() * standardDeviation;
		return nextGaussian;
	}
}