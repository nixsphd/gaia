package ie.nix.evolutionary_algorithms.gp.terminals;

import ie.nix.evolutionary_algorithms.gp.Primitives.Primitive;
import ie.nix.evolutionary_algorithms.gp.Primitives.Terminal;

public class Variable extends Terminal {
	
	protected int variableNumber;
	
	public Variable() {
		super("");
	}
	
	public Variable(int variableNumber) {
		super("v"+variableNumber);
		this.variableNumber = variableNumber;
	}
	
	public int getNumber() {
		return variableNumber;
	}

	public void setValue(double value) {
		evaluation = value;
	}
	
	public void copy(Primitive primitive) {
		super.copy(primitive);
		this.variableNumber = ((Variable)primitive).variableNumber;
	}
}
