package ie.nix.evolutionary_algorithms.gp;

import ie.nix.evolutionary_algorithms.gp.GeneticProgramming.KozaTableau;
import ie.nix.evolutionary_algorithms.gp.Programs.Program;
import ie.nix.util.Randomness;

/**
 * Hello world!
 *
 */
public class GeneticProgrammingDemo {
	
    public static void main(String[] args) {
    	
    	Randomness.setSeed(1);
    	
    	KozaTableau kozaTableau = new KozaTableau() ;
    	kozaTableau.maxNumberOfGenerations = 50;
    	kozaTableau.maxProgramDepth = 5;
    	kozaTableau.numberOfPrograms = 50;
    	kozaTableau.numberOfVariables = 1;
    	kozaTableau.constants = new double[]{-2, -1, 0, 1, 2};
		kozaTableau.probabilityOfFunctionMutation = 0.1;
		kozaTableau.probabilotyOfSelectFittest = 0.9;
		kozaTableau.probabilotyOfCrossover = 0.5;
    	
    	GeneticProgramming gp = new GeneticProgramming(kozaTableau);
    	
    	// F(x) = x + 4
    	double[][] trainingInputs =  {{1},{2},{3},{5},{8},{13},{25},{38}};
    	double[] trainingResults = {
    			Math.pow(trainingInputs[0][0], 2),
    			Math.pow(trainingInputs[1][0], 2),
    			Math.pow(trainingInputs[2][0], 2),
    			Math.pow(trainingInputs[3][0], 2),
    			Math.pow(trainingInputs[4][0], 2),
    			Math.pow(trainingInputs[5][0], 2),
    			Math.pow(trainingInputs[6][0], 2),
    			Math.pow(trainingInputs[7][0], 2),
    	};

        System.out.println("average="+gp.calculateErrorFitness(new double[trainingResults.length], trainingResults));
        
        int numberOfGenerations = 1;
        for (int g = 0; g < kozaTableau.maxNumberOfGenerations; g += numberOfGenerations) {
        	gp.evolve(numberOfGenerations, trainingInputs, trainingResults);
        	Program bestProgram = gp.getPrograms().getBestProgram();
            System.out.println("Generation="+g+", bestFitess="+bestProgram.getFitness());
//            System.out.println("Generation="+g+"bestProgram=\n"+TestHelper.toDOTString(bestProgram));
        }
    	  	
        TestHelper.toDOTString(gp.getPrograms().getBestProgram());
    }
    
}
