package ie.nix.evolutionary_algorithms.gp.functions;

import ie.nix.evolutionary_algorithms.gp.Primitives.Function;
import ie.nix.evolutionary_algorithms.gp.Primitives.Primitive;

public class Add extends Function {
	
	public Add() {
		super("+", 2);
	}

	public Add(Primitive primativeA, Primitive primativeB) {
		this();
		setInput(0, primativeA);
		setInput(1, primativeB);
		
	}

	@Override
	public double evaluate() {
		evaluateInputs();
		evaluation = inputEvaluations[0] + inputEvaluations[1];
		return evaluation;
		
	}
	
}