package ie.nix.evolutionary_algorithms.gp;

import ie.nix.evolutionary_algorithms.gp.Primitives.Function;
import ie.nix.evolutionary_algorithms.gp.Primitives.Primitive;
import ie.nix.evolutionary_algorithms.gp.Primitives.Terminal;
import ie.nix.evolutionary_algorithms.gp.terminals.Variable;
import ie.nix.evolutionary_algorithms.gp.Programs.Program;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

public class TestHelper {

	public static boolean equals(Program[] expectedSelectedPrograms, Program[] actualSelectedPrograms) {
		boolean equals = expectedSelectedPrograms.length == actualSelectedPrograms.length;
		int program = expectedSelectedPrograms.length-1;
		while (equals && program >= 0) {
			equals = equals(expectedSelectedPrograms[program], actualSelectedPrograms[program]);
			program--;
		}
		return equals;
	}
	
	public static boolean equals(Program programA, Program programB) {
		return equals(programA.getRoot(), programB.getRoot());
	}

	public static boolean equals(Primitive[] expectedPrimitives, Primitive[] actualePrimitives) {
		boolean equals = expectedPrimitives.length == actualePrimitives.length;
		int program = expectedPrimitives.length-1;
		while (equals && program >= 0) {
			equals = equals(expectedPrimitives[program], actualePrimitives[program]);
			program--;
		}
		return equals;
	}

	public static boolean equals(Primitive expectedPrimitive, Primitive actualPrimitive) {
		boolean equals = true;
		if (expectedPrimitive.getClass() == actualPrimitive.getClass()) {
			if (expectedPrimitive  instanceof Function) {
				equals = equals((Function)expectedPrimitive, (Function)actualPrimitive);
				
			} else if (expectedPrimitive  instanceof Terminal) {
				equals = equals((Terminal)expectedPrimitive, (Terminal)actualPrimitive);
				
			} else {
				// Not sure what class it is so I'll return a panicky false.
				equals = false;
			}
		} else {
			equals = false;
			
		}
		return equals;
	}
	
	public static boolean equals(Function expectedFunction, Function actualFunction) {
		boolean equals = true;
		if (expectedFunction.getClass() == actualFunction.getClass()) {
			int a = 0;
			while (equals == true && a < expectedFunction.getArity()) {
				Primitive expectedPrimitive = expectedFunction.getInput(a);
				Primitive actualPrimitive = actualFunction.getInput(a);
				equals = equals(expectedPrimitive, actualPrimitive);
				a++;
			}
		} else {
			equals = false;
			
		}
		return equals;
	}

	public static boolean equals(Terminal expectedTerminal, Terminal actualTerminal) {
		boolean equals = true;
		if (expectedTerminal.getClass() == actualTerminal.getClass()) {
			if (expectedTerminal.evaluation != actualTerminal.evaluation) {
				if (!(Double.isNaN(expectedTerminal.evaluation) && Double.isNaN(actualTerminal.evaluation))) {
					equals = false;
				}
			}
		} else {
			equals = false;
			
		}
		return equals;
	}

	public static String toString(Primitive[] primitives) {		
		StringBuilder string = new StringBuilder("{ \n");
		for (int s = 0; s < primitives.length; s++) {
			string.append(""+toString(primitives[s],"\t")+",\n");
		}
		string.replace(string.length()-2, string.length(),"};");
		return string.toString();
	}
	
	public static String toString(Primitive primitive) {
		return toString(primitive, "");
	}

	public static String toString(Primitive primitive, String tabs) {
		if (primitive  instanceof Function) {
			return toString((Function)primitive, tabs);
			
		} else if (primitive  instanceof Terminal) {
			return toString((Terminal)primitive);
			
		} 
		return null;
	}

	public static String toString(Function function) {
		if (function != null) {
			return toString(function, "\t");
		} else {
			return "null";
		}
	}
	
	public static String toString(Function function, String tabs) {
		StringBuilder string = new StringBuilder("new "+function.getClass().getSimpleName()+"(\n");
		for (int i = 0; i < function.getArity(); i++) {
			if (function.getInput(i) instanceof Function) {
				Function subFunction = ((Function)function.getInput(i));
				string.append(tabs+toString(subFunction, tabs+"\t")+",\n");
				 
			} else {
				Terminal terminal = (Terminal)function.getInput(i);
				string.append(tabs+toString(terminal)+",\n");
				
			}
		}
		string.replace(string.length()-2, string.length(), ")");
		return string.toString();
	}

	public static String toString(Terminal terminal) {
		if (terminal instanceof Variable) {
			Variable variable = (Variable)terminal;
			return "new "+terminal.getClass().getSimpleName()+"("+variable.getNumber()+")";
			
		} else {
			return "new "+terminal.getClass().getSimpleName()+"("+terminal.evaluation+")";
		}
	}
	
	public static String toString(List<Program> programs) {
		return toString(programs.toArray(new Program[programs.size()]));
		
	}
	public static String toString(Program[] programs) {
		StringBuilder string = new StringBuilder("");
		for (int p = 0; p < programs.length; p++) {
			string.append(toString(programs[p])+"\n");
		}
		return string.toString();
	}

	public static String toString(Program program) {
		StringBuilder string = new StringBuilder("new Program("+program.getNumberOfVariables()+", \n");
		string.append("\t"+toString(program.getRoot(), "\t\t")+");");
		return string.toString();
	}

	public static String toDOTString(List<Program> programs) {
		return toDOTString(programs.toArray(new Program[programs.size()]));
		
	}
	
	public static String toDOTString(Program[] programs) {

		StringBuilder string = new StringBuilder("graph {\n");
		for (int p = 0; p < programs.length; p++) {
			Primitive root = programs[p].getRoot();
			string.append(root.id+" [label=\""+root.name+"\"];\n");
			if (root  instanceof Function) {
				string.append(toDOTString((Function)root));
			} 
			string.append("\"0\" [label=\"\", shape=point]; "+ "\"0\" -- \"" + root.id + "\" [label=\""+root.evaluate() +"\"]\n\n");
				
		}
		string.append("}\n");
		return string.toString();
	}
	
	public static String toDOTString(Program program) {
		Primitive root = program.getRoot();
		StringBuilder string = new StringBuilder("graph {\n"+root.id+" [label=\""+root.name+"\"];\n");
		if (root  instanceof Function) {
			string.append(toDOTString((Function)root));
		} 
		string.append("\"0\" [label=\"\", shape=point]; "+
				"\"0\" -- \"" + root.id + "\" [label=\""+root.evaluate() +"\"]\n"+
				"}\n");
		return string.toString();
	}

	public static String toDOTString(Function function) {
		StringBuilder string = new StringBuilder();
		for (int f = 0; f < function.getArity(); f++) {
			Primitive primitive = function.getInput(f);
			string.append(primitive.id+ " [label=\""+primitive.name+"\"];\n");
			if (primitive instanceof Function) {
				string.append(toDOTString((Function)primitive));
			} 
			string.append("\""+function.id + "\" -- \"" + primitive.id + "\" [label=\""+primitive.evaluate() +"\"];\n");
		
		}
		return string.toString();
	}
	
	public static String readFile(String fileName) {
		String output = "";
		try {
			output = new Scanner(new File(fileName)).useDelimiter("\\Z").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	    return output;
	}

	public static void writeFile(String fileName, String contents) {
		try {
			PrintWriter out = new PrintWriter(fileName);
			out.println(contents);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
