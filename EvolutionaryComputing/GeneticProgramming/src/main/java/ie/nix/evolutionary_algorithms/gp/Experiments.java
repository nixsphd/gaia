
package ie.nix.evolutionary_algorithms.gp;

import ie.nix.evolutionary_algorithms.gp.GeneticProgramming.KozaTableau;
import ie.nix.evolutionary_algorithms.gp.Programs.Program;
import ie.nix.util.Randomness;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Experiments {
	
	public static class TrainingSet {
		String name;
		double[][] trainingInputs;
    	double[] trainingResults;
    	
		public TrainingSet(String name, double[][] trainingInputs, double[] trainingResults) {
			this.name = name;
			this.trainingInputs = trainingInputs;
			this.trainingResults = trainingResults;
			
		}
		
		public TrainingSet(String name, double[][] trainingInputs, Equation equation) {
			this(name, trainingInputs, equation.generateTrainingResults(trainingInputs));
			
		}

	}
	
	public abstract static class Equation {
		
		public abstract double evaluate(double[] inputs);

		public double[] generateTrainingResults(double[][] trainingInputs) {
			double[] trainingResults = new double[trainingInputs.length]; 
			for (int ti = 0; ti < trainingInputs.length; ti++) {
				trainingResults[ti] = evaluate(trainingInputs[ti]);
			};
			return trainingResults;
			
		}
	}
	
	public static class XSquared extends Equation {
		@Override
		public double evaluate(double[] inputs) {
			return Math.pow(inputs[0], 2);
		}
		
	}
	
	public static class XPlus6 extends Equation {
		@Override
		public double evaluate(double[] inputs) {
			return inputs[0] + 6;
		}
		
	}
	
	public static class XPlusY extends Equation {
		@Override
		public double evaluate(double[] inputs) {
			return inputs[0] + inputs[1];
		}
		
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		
		// TODO - Test varying initial program depth
		// TODO - Test varying the number of programs
		// TODO - Test varying the Crossover rate
		// TODO - Test varying the Function Mutation rate
		
		// TODO - Add an Occam's Razor Filter
		// TODO - Test a Group Selection operator
		// TODO - Test a Proportional Fitness Selection operator
		// TODO - Test varying number of constants
		// TODO - Test a Variable Mutation operator
		// TODO - Test a Constant Mutation operator.
		// TODO - Test having random Gaussian constants. 
		// TODO - Try generating variable length random programs.
		
		// TODO - Add noise to the trainingset.
		
		regressionRun();
//		regressionBatch();
		System.out.println("Done.");
	}
	
	public static void regressionRun() {
		
		KozaTableau kozaTableau = new KozaTableau();
    	kozaTableau.maxProgramDepth = 5;
    	kozaTableau.numberOfPrograms = 50;
    	kozaTableau.maxNumberOfGenerations = 500;
    	kozaTableau.autostop = true;
		kozaTableau.probabilityOfFunctionMutation = 0.1;
		kozaTableau.probabilotyOfSelectFittest = 0.9;
		kozaTableau.probabilotyOfCrossover = 0.5;
		
		double[][] trainingInputs =  {{1},{2},{3},{5},{8},{13},{25},{38}};
		double[][] trainingInputs2 =  {{1,1},{2,8},{3,5},{5,-4},{8,-10},{13,0.8},{25,5.2},{2.3,38}};
		TrainingSet[] trainingSets = {
				new TrainingSet("XSquared", trainingInputs, new XSquared()), 
				new TrainingSet("XPlus6", trainingInputs, new XPlus6()),
				new TrainingSet("XPlusY", trainingInputs2, new XPlusY())
		};

        int numberOfGenerationsToBeRun = 10;
        
		PrintWriter writer = initwriter("RegressionRun.csv");
        
		run(kozaTableau, trainingSets, numberOfGenerationsToBeRun, writer);		

	}

	protected static void run(KozaTableau kozaTableau, TrainingSet[] trainingSets, 
			int numberOfGenerationsToBeRun, PrintWriter writer) {

		logRunHeaders(writer);
		
		Randomness.setSeed(1);

		for (int trainingSet = 0; trainingSet < trainingSets.length; trainingSet++ ) {

			Randomness.setSeed(1);
	    	kozaTableau.numberOfVariables = trainingSets[trainingSet].trainingInputs[0].length;
	    	
	    	GeneticProgramming gp = new GeneticProgramming(kozaTableau);
	    	
			// for each generation to be run
			int g = 0;
			double fitness = -1;
			while(fitness != 0.0 && g <= kozaTableau.maxNumberOfGenerations) {
	        	gp.evolve(numberOfGenerationsToBeRun, trainingSets[trainingSet].trainingInputs, trainingSets[trainingSet].trainingResults);
	        	Program bestProgram = gp.getPrograms().getBestProgram();
	        	fitness = bestProgram.getFitness();
	        	double averageFitness = gp.getPrograms().getAverageFitness();
	            System.out.format("TraingSet=%s, Generation=%d, bestFitess=%.1f, averageFitness=%.1f %n", 
	            		trainingSets[trainingSet].name, g, bestProgram.getFitness(), averageFitness);
	            
	            logRun(kozaTableau, trainingSets[trainingSet].name, g, averageFitness, bestProgram.getFitness(), writer);

				g += numberOfGenerationsToBeRun;
				
			}
		}
	}
	
	protected static void logRunHeaders(PrintWriter writer) {
		writer.println(
				"\"NumberOfGenerations\", \"NumberOfPrograms\", \"MaxProgramDepth\", "+ 
				"\"ProbabilityOfSelectingFittest\", \"ProbabilotyOfCrossover\", \"ProbabilityOfFunctionMutation\", "+ 
				"\"TrainingSet\", \"Generation\", "+
				"\"AverageFitness\", \"BestFitness\"");
	}
	
	protected static void logRun(KozaTableau kozaTableau, String traininSetName,int generation, double averageFitness, double bestFitness, PrintWriter writer) {
		writer.println(kozaTableau.maxNumberOfGenerations+", "+kozaTableau.numberOfPrograms+", "+kozaTableau.maxProgramDepth+", "+
				kozaTableau.probabilotyOfSelectFittest+", "+kozaTableau.probabilotyOfCrossover+", "+kozaTableau.probabilotyOfCrossover+", "+
				traininSetName+", "+generation+", "+
				averageFitness+", "+bestFitness);
		// Need to keep flushing the print stream otherwise the output is truncated :(
		writer.flush();
		
	}
	
	public static void regressionBatch() {
		
		KozaTableau kozaTableau = new KozaTableau();
		kozaTableau.constants = new double[]{-2, -1, 0, 1, 2};
		kozaTableau.maxProgramDepth = 5;
		kozaTableau.numberOfPrograms = 100;
		kozaTableau.maxNumberOfGenerations = 1000;
		kozaTableau.autostop = true;
		kozaTableau.probabilityOfFunctionMutation = 0.1;
		kozaTableau.probabilotyOfSelectFittest = 0.9;
		kozaTableau.probabilotyOfCrossover = 0.5;
	
		int runs = 20;
		
		double[][] trainingInputs =  {{1},{2},{3},{5},{8},{13},{25},{38}};
		double[][] trainingInputs2 =  {{1,1},{2,8},{3,5},{5,-4},{8,-10},{13,0.8},{25,5.2},{2.3,38}};
		TrainingSet[] trainingSets = {
				new TrainingSet("XSquared", trainingInputs, new XSquared()), 
				new TrainingSet("XPlus6", trainingInputs, new XPlus6()),
				new TrainingSet("XPlusY", trainingInputs2, new XPlusY())
		};
	
		PrintWriter writer = initwriter("RegressionBatch.csv");
		
		batch(runs, kozaTableau, trainingSets, writer);	
	
	}

	protected static void batch(int numberOfRuns, KozaTableau kozaTableau, TrainingSet[] trainingSets, PrintWriter writer) {
	
		logBatchHeaders(writer);
		
		Randomness.setSeed(1);
	
		for (int trainingSet = 0; trainingSet < trainingSets.length; trainingSet++ ) {
	
	    	kozaTableau.numberOfVariables = trainingSets[trainingSet].trainingInputs[0].length;
	    	
			GeneticProgramming gp = new GeneticProgramming(kozaTableau);
			
			for (int runNumber = 1; runNumber <= numberOfRuns; runNumber++ ) {
	
				int numberOfGenerations = gp.evolve(trainingSets[trainingSet].trainingInputs, trainingSets[trainingSet].trainingResults);
				Program bestProgram = gp.getPrograms().getBestProgram();
	            System.out.format("Run=%d, TraingSet=%s, Generation=%d, bestFitess=%.1f, averageFitness=%.1f %n", 
	            		runNumber, trainingSets[trainingSet].name, numberOfGenerations, bestProgram.getFitness(), gp.getPrograms().getAverageFitness());
					
				logBatch(runNumber, kozaTableau, trainingSets[trainingSet].name, numberOfGenerations, 
						gp.getPrograms().getAverageFitness(), bestProgram.getFitness(), writer);
			}
		}
		
	}

	protected static void logBatchHeaders(PrintWriter writer) {
		writer.println("\"Run\", "+
				"\"NumberOfGenerations\", \"NumberOfPrograms\", \"MaxProgramDepth\", "+ 
				"\"ProbabilityOfSelectingFittest\", \"ProbabilotyOfCrossover\", \"ProbabilityOfFunctionMutation\", "+ 
				"\"TrainingSet\", \"Generation\", "+
				"\"AverageFitness\", \"BestFitness\"");
	}

	protected static void logBatch(int run, KozaTableau kozaTableau, String traininSetName, int numberOfGenerations, double averageFitness, double bestFitness, PrintWriter writer) {
		
		writer.println(run+", "+kozaTableau.maxNumberOfGenerations+", "+kozaTableau.numberOfPrograms+", "+kozaTableau.maxProgramDepth+", "+
				kozaTableau.probabilotyOfSelectFittest+", "+kozaTableau.probabilotyOfCrossover+", "+kozaTableau.probabilotyOfCrossover+", "+
				traininSetName+", "+numberOfGenerations+", "+
				averageFitness+", "+bestFitness);
		
		// Need to keep flushing the print stream otherwise the output is truncated :(
		writer.flush();
		
	}

	protected static PrintWriter initwriter(String outputFileName) {
			String outputDirectoryName = "R";
			
			File outputDirectory = new File(outputDirectoryName);
			if (!outputDirectory.exists()) {
				outputDirectory.mkdirs();
			}
	
			PrintWriter writer;
			try {
				writer = new PrintWriter(new String(outputDirectoryName+"/"+outputFileName));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				writer = new PrintWriter(System.out);
			}
			return writer;
		}

}
