package ie.nix.evolutionary_algorithms.gp.genetic_operators;

import ie.nix.util.Randomness;
import ie.nix.evolutionary_algorithms.gp.GeneticProgramming.GeneticOperator;
import ie.nix.evolutionary_algorithms.gp.Primitives.Function;
import ie.nix.evolutionary_algorithms.gp.Primitives;
import ie.nix.evolutionary_algorithms.gp.Programs;
import ie.nix.evolutionary_algorithms.gp.Programs.Program;

public class FunctionMutation implements GeneticOperator {

	private double probabillityOfMutation;
	
	public FunctionMutation(double probabillityOfMutation) {
		this.probabillityOfMutation = probabillityOfMutation;
	}

	@Override
	public void operate(Programs programs) {
		int sizeOfPopulation = programs.getNumberOfPrograms();
		for (int p = 0; p < sizeOfPopulation; p++) {	
			Program program = programs.getProgram(p);
			if (program.getRoot() instanceof Function) {
				Function rootFunction = (Function)program.getRoot();
				if (probabillityOfMutation > Randomness.nextDouble()) {
					Function newFunction = mutateFuntion(rootFunction, programs.getPrimitives());
					program.setRoot(newFunction);
				}
				// This might have changes
				mutateChildern((Function)program.getRoot(), programs.getPrimitives());
			}
			// No functions at all so nothing to mutate.
		}
	}
	
	protected Function mutateFuntion(Function function, Primitives primitives) {
		Function newFunction = primitives.getRandomFunction();
		newFunction.copyInputs(function);
		return newFunction;
	}

	protected void mutateChildern(Function function, Primitives primitives) {
		for (int a = 0; a < function.getArity(); a++) {
			if (function.getInput(a) instanceof Function) {
				Function childFunction = (Function)function.getInput(a);
				if (probabillityOfMutation > Randomness.nextDouble()) {
					Function newFunction = mutateFuntion(childFunction, primitives);
					function.replace(childFunction, newFunction);
				}
				// This might have changes
				mutateChildern((Function)function.getInput(a), primitives);
			}
		}
	}
	
}