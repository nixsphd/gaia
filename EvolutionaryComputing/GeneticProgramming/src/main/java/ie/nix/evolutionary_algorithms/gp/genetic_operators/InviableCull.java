package ie.nix.evolutionary_algorithms.gp.genetic_operators;

import java.util.ArrayList;
import java.util.List;

import ie.nix.evolutionary_algorithms.gp.GeneticProgramming.GeneticOperator;
import ie.nix.evolutionary_algorithms.gp.Programs.Program;
import ie.nix.evolutionary_algorithms.gp.Programs;

public class InviableCull implements GeneticOperator {


	@Override
	public void operate(Programs programs) {
		int numberOfPrograms = programs.getNumberOfPrograms();
		List<Program> viablePrograms = new ArrayList<Program>();
		for (int p = 0; p < numberOfPrograms; p++) { 
			double fitness = programs.getProgram(p).getFitness();
			// Only add is it a valid value
			if ((!Double.isNaN(fitness)) && (!Double.isInfinite(fitness))) {
				viablePrograms.add(programs.getProgram(p));
			}
			
		}
		programs.setPrograms(viablePrograms);
		
	}
	
}