package ie.nix.evolutionary_algorithms.gp.genetic_operators;

import java.util.ArrayList;

import ie.nix.util.Randomness;
import ie.nix.evolutionary_algorithms.gp.GeneticProgramming.GeneticOperator;
import ie.nix.evolutionary_algorithms.gp.Primitives.Function;
import ie.nix.evolutionary_algorithms.gp.Programs;
import ie.nix.evolutionary_algorithms.gp.Primitives.Primitive;

public class Crossover implements GeneticOperator {

	protected double probabilotyOfCrossover;
	
	public Crossover( double probabilotyOfCrossover) {
		this.probabilotyOfCrossover = probabilotyOfCrossover;
	}

	@Override
	public void operate(Programs programs) {
		int sizeOfPopulation = programs.getNumberOfPrograms();

		for (int p = 1; p < sizeOfPopulation; p = p + 2) {
			if (probabilotyOfCrossover > Randomness.nextDouble()) {		
				Primitive treeA = programs.getProgram(p - 1).getRoot();	
				Primitive treeB = programs.getProgram(p).getRoot();
				if (treeA instanceof Function && treeB instanceof Function) {
					// Not worth considering crossover unless both trees are functions.
					crossover((Function)treeA, (Function)treeB);
				}
			}
		}
	}

	protected void crossover(Function treeA, Function treeB) {
		// The respective depths to cross at. Can't cross at 0 depth since it doesn't exist and can't 
		// cross at 1 since that would leave on whole tree empty, so we start at 2.
		int maxDepthA = treeA.getMaxDepth();
		int maxDepthB = treeB.getMaxDepth();
		// W can only cross over if there is at least 3 nodes in both trees.
		if (maxDepthA > 2 && maxDepthB > 2) {
			int crossoverDepthA = Randomness.nextInt(2, maxDepthA);
			int crossoverDepthB = Randomness.nextInt(2, maxDepthB);
			// The primitives at this depth.
			ArrayList<Primitive> primitivesA = treeA.getPrimitivesAtDepth(crossoverDepthA);
			ArrayList<Primitive> primitivesB = treeB.getPrimitivesAtDepth(crossoverDepthB);
			// The actual primitives to cross.
			int primitiveIndexA = Randomness.nextInt(primitivesA.size());
			int primitiveIndexB = Randomness.nextInt(primitivesB.size());
			Primitive toCrossA = primitivesA.get(primitiveIndexA);
			Primitive toCrossB = primitivesB.get(primitiveIndexB);
			// Get the two parents
			Function parentA = ((Function)toCrossA.getParent());
			Function parentB = ((Function)toCrossB.getParent());
			// Replace the trees in the two parents
			parentA.replace(toCrossA, toCrossB);
			parentB.replace(toCrossB, toCrossA);
			
//			System.out.println("NDB::crossover()~maxDepthA="+maxDepthA);
//			System.out.println("NDB::crossover()~maxDepthB="+maxDepthB);
//			System.out.println("NDB::crossover()~crossoverDepthA="+crossoverDepthA);
//			System.out.println("NDB::crossover()~crossoverDepthB="+crossoverDepthB);
//			System.out.println("NDB::crossover()~# 0f primitivesA="+primitivesA.size());
//			System.out.println("NDB::crossover()~# 0f primitivesB="+primitivesB.size());
//			System.out.println("NDB::crossover()~primitiveIndexA="+primitiveIndexA);
//			System.out.println("NDB::crossover()~primitiveIndexB="+primitiveIndexB);
//			System.out.println("NDB::crossover()~toCrossA="+toCrossA);
//			System.out.println("NDB::crossover()~toCrossB="+toCrossB);
//			System.out.println("NDB::crossover()~parentA="+parentA);
//			System.out.println("NDB::crossover()~parentB="+parentB);
		}
	}
}
