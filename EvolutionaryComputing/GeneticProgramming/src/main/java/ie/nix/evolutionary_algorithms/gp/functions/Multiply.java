package ie.nix.evolutionary_algorithms.gp.functions;

import ie.nix.evolutionary_algorithms.gp.Primitives.Function;
import ie.nix.evolutionary_algorithms.gp.Primitives.Primitive;

public class Multiply extends Function {
	
	public Multiply() {
		super("x", 2);
	}

	public Multiply(Primitive primativeA, Primitive primativeB) {
		this();
		setInput(0, primativeA);
		setInput(1, primativeB);
		
	}

	@Override
	public double evaluate() {
		evaluateInputs();
		evaluation = inputEvaluations[0] * inputEvaluations[1];
		return evaluation;
		
	}
	
}