package ie.nix.evolutionary_algorithms.gp;

import ie.nix.evolutionary_algorithms.gp.Primitives.Function;
import ie.nix.evolutionary_algorithms.gp.Primitives.Primitive;
import ie.nix.evolutionary_algorithms.gp.Primitives.Terminal;
import ie.nix.evolutionary_algorithms.gp.terminals.Variable;

import java.util.ArrayList;
import java.util.List;

public class Programs {
	
	public static class Program {

		protected Primitive root;
		protected double fitness;
		protected ArrayList<Variable>[] vis; // variable instances (vis)
		
		@SuppressWarnings("unchecked")
		public Program(int numberOfVariables) {
			vis = new ArrayList[numberOfVariables];
			for (int v = 0; v < numberOfVariables; v++) {
				vis[v] = new ArrayList<Variable>();
			}
		}
		
		// This is really more for testing , so I may remove?
		protected Program(int numberOfVariables, Primitive root) {
			this(numberOfVariables);
			this.root = root;
			if (root instanceof Function) {
				addVariableInstances((Function)root);
			} else if (root instanceof Variable) {
				addVariableInstance((Variable)root);
			}
		}

		// TODO - This is 'full', add support for 'grow' and 'half and half'.
		public void buildFullRandomTree(Primitives primatives, int maxProgramDepth) {
			if (maxProgramDepth > 1) {
				setRoot(primatives.getRandomFunction());
				buildFullRandomTree(primatives, (Function)getRoot(), maxProgramDepth-1);
			} else {
				setRoot(primatives.getRandomTerminal());
			}
			
		}

		protected void buildFullRandomTree(Primitives primatives, Function function, int depth) {
			int arity = function.getArity();
			for (int a = 0; a < arity; a++) {
				if (depth > 1) {
					function.setInput(a, primatives.getRandomFunction());
					Function functionInstance = (Function)function.getInput(a);
					buildFullRandomTree(primatives, functionInstance, depth-1);
				} else {
					function.setInput(a, primatives.getRandomTerminal());
					if (function.getInput(a) instanceof Variable) {
						Variable variableInstance = (Variable)function.getInput(a);
						addVariableInstance(variableInstance);
					}
				}
			}
		}
		
		public void cloneProgram(Primitives primatives, Program program) {
			this.fitness = program.fitness;
			cloneTree(primatives, program);
		}

		public void cloneTree(Primitives primatives, Program program) {
			if (program.getRoot() instanceof Function) {
				setRoot(primatives.cloneFunction((Function)program.getRoot()));
				cloneFunction(primatives, (Function)getRoot(), (Function)program.getRoot());
			} else {
				setRoot(primatives.cloneTerminal((Terminal)program.getRoot()));
				
			}
			
		}

		protected void cloneFunction(Primitives primatives, Function function, Function functionToClone) {
			int arity = functionToClone.getArity();
			for (int a = 0; a < arity; a++) {
				if (functionToClone.getInput(a) instanceof Function) {
					function.setInput(a, primatives.cloneFunction((Function)functionToClone.getInput(a)));
					cloneFunction(primatives, (Function)function.getInput(a), (Function)functionToClone.getInput(a));
				} else {
					function.setInput(a, primatives.cloneTerminal((Terminal)functionToClone.getInput(a)));
					if (function.getInput(a)  instanceof Variable) {
						addVariableInstance((Variable)function.getInput(a));
					}
				}
			}
		}
		
		protected void addVariableInstances(Function function) {
			int arity = function.getArity();
			for (int a = 0; a < arity; a++) {
				if (function.getInput(a) instanceof Variable) {
					addVariableInstance((Variable)function.getInput(a));
				}
			}
		}

		public double[] evaluate(double[][] variableValues) {
			double[] evaluations = new double[variableValues.length];
			for (int e = 0; e < variableValues.length; e++) {
				evaluations[e] = evaluate(variableValues[e]);
				
			}
			return evaluations;
		}

		public double evaluate(double[] variableValue) {
			assignVariables(variableValue);
			return root.evaluate();
		}

		public int getNumberOfVariables() {
			return vis.length;
		}

		public void assignVariables(double[] variableValue) {
			// For each variable (v)
			for (int v = 0; v < vis.length; v++) {
				// For each variable instance (vi)  
				for (int vi = 0; vi < vis[v].size(); vi++) {
					vis[v].get(vi).setValue(variableValue[v]);
				}
			}
			
		}

		protected void addVariableInstance(Variable variableInstance) {
			vis[variableInstance.getNumber()-1].add(variableInstance);
		}
		
		public double getFitness() {
			return fitness;
		}

		public void setFitness(double fitness) {
//			System.out.println("NDB::"+this.hashCode()+".setFitness("+fitness+")");
			this.fitness = fitness;
			
		}

		public Primitive getRoot() {
			return root;
		}

		public void setRoot(Primitive primitive) {
			this.root = primitive;
			// Set the parent to null;
			primitive.parent = null;
		}

		public ArrayList<Variable> getVariableInstancesFor(Variable variable) {
			return vis[variable.getNumber()-1];
		}

	}

	protected List<Program> programs;
	protected int numberOfPrograms;
	protected int maxDepth;
	protected Primitives primitives;

	public Programs(int numberOfPrograms, int maxDepth, Primitives primitives) {
		this.numberOfPrograms = numberOfPrograms;
		programs = new ArrayList<Program>(numberOfPrograms);
		this.maxDepth = maxDepth;
		this.primitives = primitives;

	}

	protected void initRandomPrograms() {
		for (int p = 0; p < numberOfPrograms; p++) {
			programs.add(p,initRandomProgram());
		}

	}

	protected Program initRandomProgram() {
		Program program = new Program(primitives.numberOfVariables);
		program.buildFullRandomTree(primitives, maxDepth);
		return program;

	}

	public Program getProgram(int index) {
		return programs.get(index);
		
	}

	public Program getBestProgram() {
		Program bestProgram = null;
		double bestFitness = Double.POSITIVE_INFINITY;
		for (int p = 0; p < getNumberOfPrograms(); p++) {
			double challengeFitness = getProgram(p).getFitness();
			if (challengeFitness != Double.NaN
					&& challengeFitness < bestFitness) {
				bestProgram = getProgram(p);
				bestFitness = challengeFitness;
			}
		}
		if (bestFitness != Double.POSITIVE_INFINITY) {
			return bestProgram;
		} else {
			return null;
		}
		
	}

	public double getAverageFitness() {
		int numberOfPrograms = getNumberOfPrograms();
		double averageFitness = 0;
		for (int p = 0; p < numberOfPrograms; p++) { 
			double fitness = getProgram(p).getFitness();
			// Only add is it a valid value
			if (!Double.isNaN(fitness) && !Double.isInfinite(fitness)) {
				averageFitness += getProgram(p).getFitness();
			}
			
		}
		averageFitness /= numberOfPrograms;
		return averageFitness;
	}

	public List<Program> getPrograms() {
		return programs;
		
	}
	
	public void setPrograms(List<Program> programs) {
		this.programs = programs;
		
	}

	public int getNumberOfPrograms() {
		return programs.size();
	}

	public Primitives getPrimitives() {
		return primitives;
	}

}