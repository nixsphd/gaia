package ie.nix.evolutionary_algorithms.gp.genetic_operators;

import java.util.ArrayList;
import java.util.List;

import ie.nix.util.Randomness;
import ie.nix.evolutionary_algorithms.gp.GeneticProgramming.GeneticOperator;
import ie.nix.evolutionary_algorithms.gp.Programs;
import ie.nix.evolutionary_algorithms.gp.Programs.Program;


public class BinaryTournamentSelection implements GeneticOperator {

	private int numberToSelect;
	private double probabilityOfSelectingFittest;
	
	public BinaryTournamentSelection(int numberToSelect, double pobabilityOfSelectingFittest) {
		this.numberToSelect = numberToSelect;
		this.probabilityOfSelectingFittest = pobabilityOfSelectingFittest;
	}

	@Override
	public void operate(Programs programs) {
		int sizeOfPopulation = programs.getNumberOfPrograms();
		List<Program> selectedPrograms = new ArrayList<Program>(sizeOfPopulation);
		for (int p = 0; p < numberToSelect; p++) {	
			int parentA = Randomness.nextInt(sizeOfPopulation);
			int parentB = Randomness.nextInt(sizeOfPopulation);
			double fittnessA = programs.getProgram(parentA).getFitness();
			double fittnessB = programs.getProgram(parentB).getFitness();
			boolean selectFittest = (Randomness.nextDouble() <= probabilityOfSelectingFittest);
			int selected = 0;
			if (fittnessA < fittnessB) {
				if (selectFittest) {
					// Select A
					selected = parentA;
				} else {
					// Select B
					selected = parentB;
				}
			} else {
				if (selectFittest) {
					// Select B
					selected = parentB;
				} else {
					// Select A
					selected = parentA;
				}
			}
			Program selectedProgram = programs.getProgram(selected);
			Program clonedSelectedProgram = new Program(selectedProgram.getNumberOfVariables());
			clonedSelectedProgram.cloneProgram(programs.getPrimitives(), selectedProgram);
			selectedPrograms.add(p, clonedSelectedProgram);
			
		}
		programs.setPrograms(selectedPrograms);
	}
	
}