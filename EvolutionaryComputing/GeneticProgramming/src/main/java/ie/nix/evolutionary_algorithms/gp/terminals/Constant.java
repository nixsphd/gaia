package ie.nix.evolutionary_algorithms.gp.terminals;

import ie.nix.evolutionary_algorithms.gp.Primitives.Terminal;

public class Constant extends Terminal {
	
	public Constant() {
		super("");
	}
	
	public Constant(double constant) {
		super(Double.toString(constant));
		this.evaluation = constant;
	}
	
}
