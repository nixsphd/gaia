package ie.nix.evolutionary_algorithms.gp;

import ie.nix.evolutionary_algorithms.gp.terminals.Constant;
import ie.nix.evolutionary_algorithms.gp.terminals.Variable;
import ie.nix.util.Randomness;

import java.util.ArrayList;

public class Primitives {
	
	protected static int nextID = 1;
	
	public abstract static class Primitive {

		protected int id = -1;
		protected String name;
		protected double evaluation;
		protected Primitive parent;
		
		public Primitive(String name) {
			this.id = nextID++;
			this.name = name;
			this.evaluation = Double.NaN;
			this.parent = null;
//			System.out.println("NDB::Primitive("+name+")~id="+this.id);
		}

		public String getName() {
			return name;
		}

		public Primitive getParent() {
			return parent;
		}

		protected double evaluate() {
			return evaluation;
		}

		public void copy(Primitive primitive) {
			this.name = primitive.name;
			this.evaluation = primitive.evaluation;
		}
		
		public int getDepth() {
			if (parent != null) {
				return parent.getDepth() + 1;
			} else {
				return 1;
			}
		}

		@Override
		public String toString() {
			if (parent != null) {
				return "[id=" + id + ", name=" + name + ", parent="+ parent.id + "]";
			} else {
				return "[id=" + id + ", name=" + name + ", parent=null]";
			}
		}
		
	}
	
	public static class Terminal extends Primitive {
		
		public Terminal(String name) {
			super(name);
		}
		
	}

	public static class Function extends Primitive {
		
		private Primitive inputs[];
		protected double inputEvaluations[];
		
		public Function(String name, int arity) {
			super(name);
			this.inputs = new Primitive[arity];
			this.inputEvaluations = new double[arity];
		}
		
		public void evaluateInputs() {
			for (int i = 0; i < getArity(); i++) {
				inputEvaluations[i] = getInput(i).evaluate();
			}
			
		}

		public int getArity() {
			return inputs.length;
		}

		public Primitive getInput(int a) {
			return inputs[a];
		}
		
		protected void setInput(int a, Primitive primitive) {
			inputs[a] = primitive;
			// Set the parent
			primitive.parent = this;
		}


		public void replace(Primitive function, Primitive newFunction) {
			for (int i = 0; i < getArity(); i++) {
				if (getInput(i) == function) {
					setInput(i, newFunction);
					// Don't fix the parent of function because this might 
					// be a swap ait's it's already been changed.
					return;
				}
			}
			// Didn't find the function to be replaced, what should I do?
			System.out.println("NDB Didn't find the function to be replaced, what should I do?");
		}

		public void copyInputs(Primitive primitive) {
			for (int i = 0; i < getArity(); i++) {
				setInput(i, ((Function)primitive).getInput(i));
			}
		}

		// TODO - This is not tested!
		public int getMinDepth() {
			if (getArity() == 0) {
				return 1;
			} else {
				int minDepth = Integer.MAX_VALUE;
				for (int i = 0; i < getArity(); i++) {
					if (getInput(i) instanceof Function) {
						int challengeDepth = ((Function)getInput(i)).getMinDepth();
						if (challengeDepth < minDepth) {
							minDepth = challengeDepth;
						}
					} else {
						// TODO - What to do here???
//						if (minDepth < Integer.MAX_VALUE) {
//							minDepth = 1;
//						}
					}
				}
				return minDepth + 1; // It's the max depth of the subtree plus on for this node.
			}
		}
		
		public int getMaxDepth() {
			if (getArity() == 0) {
				return 1;
			} else {
				int maxDepth = 0;
				for (int a = 0; a < getArity(); a++) {
					if (getInput(a) instanceof Function) {
						int challengeDepth = ((Function)getInput(a)).getMaxDepth();
						if (challengeDepth > maxDepth) {
							maxDepth = challengeDepth;
						}
					} else {
						if (maxDepth < 1) {
							maxDepth = 1;
						}
					}
				}
				return maxDepth + 1; // It's the max depth of the subtree plus on for this node.
			}
		}

		public ArrayList<Primitive> getPrimitivesAtDepth(int depth) {
			
			int currentDepth = getDepth();
			ArrayList<Primitive> primitivesAtDepth = new ArrayList<Primitive>();
			
			if (currentDepth == depth) {
				// This can happen when we start the search for depth 1 at the root node. 
				primitivesAtDepth.add(this);
			} 
			for (int a = 0; a < getArity(); a++) {
				 if (currentDepth == depth - 1) {
					// Add all the inputs...
					primitivesAtDepth.add(getInput(a));
				} else {
					// Search the inputs, if there are functions
					if (getInput(a) instanceof Function) {
						primitivesAtDepth.addAll(((Function)getInput(a)).getPrimitivesAtDepth(depth));
					} else {
						// return nothing as this part of the tree isn't deep enough.
					}
				}
			}
			return primitivesAtDepth;
		}

		public Primitive getParent(Primitive primitive) {
			return parent;
		}
	}
	
	protected int numberOfVariables;
	protected double[] constants;
	@SuppressWarnings("rawtypes")
	protected Class[] functions;

//	public Primitives() {
//		this.nextID = 1;
//	}
	
	protected void initTerminals(int numberOfVariables, double[] constants) {
		this.numberOfVariables = numberOfVariables;
		this.constants = constants;
	}
	
	@SuppressWarnings("rawtypes")
	public void initFunctions(Class[] functions) {
		for (int f = 0; f < functions.length; f++) {
			if (!Function.class.isAssignableFrom(functions[f])) {
				//  Error
				throw new RuntimeException("The "+f+" function, "+functions[f].getSimpleName()+", in the Koza Tableau is not a subclass of Function.");
			}
		}
		this.functions = functions;
	}

	public Function getRandomFunction() {
		try {
			Function randomFunction = (Function)functions[Randomness.nextInt(functions.length)].newInstance();
			return randomFunction;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Terminal getRandomTerminal() {
		int ramdonIndex = Randomness.nextInt(numberOfVariables+constants.length);
		if (ramdonIndex < numberOfVariables) {
			return  new Variable(ramdonIndex + 1); // The variable labels start form 1 not 0.
			
		} else {
			return new Constant(constants[ramdonIndex-numberOfVariables]);
			
		}
	}
	
	public Terminal getRandomVariable() {
		return  new Variable(Randomness.nextInt(numberOfVariables));
	}
	
	public Terminal getRandomConstant() {
		return new Constant(constants[Randomness.nextInt(constants.length)]);
	}

	public Function cloneFunction(Function function) {
		try {
			Function clonedFunction  =  function.getClass().newInstance();
			clonedFunction.copy(function);
			return clonedFunction;
		} catch (InstantiationException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public Terminal cloneTerminal(Terminal terminal) {
		try {
			Terminal clonedTerminal = terminal.getClass().newInstance();
			clonedTerminal.copy(terminal);
			return clonedTerminal;
		} catch (InstantiationException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}
