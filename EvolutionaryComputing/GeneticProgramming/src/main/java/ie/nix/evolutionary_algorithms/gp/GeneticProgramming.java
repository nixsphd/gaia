package ie.nix.evolutionary_algorithms.gp;

import java.util.ArrayList;
import java.util.List;

import ie.nix.evolutionary_algorithms.gp.functions.Add;
import ie.nix.evolutionary_algorithms.gp.functions.Divide;
import ie.nix.evolutionary_algorithms.gp.functions.Multiply;
import ie.nix.evolutionary_algorithms.gp.functions.Subtract;
import ie.nix.evolutionary_algorithms.gp.genetic_operators.BinaryTournamentSelection;
import ie.nix.evolutionary_algorithms.gp.genetic_operators.FunctionMutation;
import ie.nix.evolutionary_algorithms.gp.genetic_operators.Crossover;
import ie.nix.evolutionary_algorithms.gp.genetic_operators.InviableCull;
import ie.nix.evolutionary_algorithms.gp.Programs.Program;


public class GeneticProgramming {

	public interface GeneticOperator {
		public void operate(Programs programs);
	}
	
	public static class KozaTableau {
		// Primitives
		public int numberOfVariables = 5;
		public double[] constants = {-5,-4, -3, -2,-1,0,1,2,3,4,5};
		@SuppressWarnings("rawtypes")
		public Class[] functions = {
			Add.class, 
			Subtract.class, 
			Multiply.class, 
			Divide.class};
		
		// Programs
		public int numberOfPrograms = 10;
		public int maxProgramDepth = 5;
		
		// Generations
		public int maxNumberOfGenerations = 100;
		public boolean autostop = false;
		
		// Genetic Operators
		public double probabilityOfFunctionMutation = 0.1;
		public double probabilotyOfSelectFittest = 0.9;
		public double probabilotyOfCrossover = 0.5;

	}
	
	protected KozaTableau kozaTableau;
	protected Primitives primitives;
	protected Programs programs;
	protected List<GeneticOperator> geneticOperators;
	
	public GeneticProgramming(KozaTableau kozaTableau) {
		this(kozaTableau, new ArrayList<GeneticOperator>());
		
		// Add the default genetic operator
		// Selection
		geneticOperators.add(new BinaryTournamentSelection(kozaTableau.numberOfPrograms, kozaTableau.probabilotyOfSelectFittest));
		// Cross-over
		geneticOperators.add(new Crossover(kozaTableau.probabilotyOfCrossover));
		// Mutation
		geneticOperators.add(new FunctionMutation(kozaTableau.probabilityOfFunctionMutation));
		// Inviable program cull
		geneticOperators.add(new InviableCull());
	
	}
	
	public GeneticProgramming(KozaTableau kozaTableau, List<GeneticOperator> geneticOperators) {
		this.kozaTableau = kozaTableau;
		this.geneticOperators = geneticOperators;
		
		primitives = new Primitives();
		primitives.initTerminals(kozaTableau.numberOfVariables, kozaTableau.constants);
		primitives.initFunctions(kozaTableau.functions);
		
		programs = new Programs(kozaTableau.numberOfPrograms, kozaTableau.maxProgramDepth, primitives);
		programs.initRandomPrograms();
		
	}

	public Primitives getPrimitives() {
		return primitives;
	}
	
	public Programs getPrograms() {
		return programs;
	}


	public int evolve(double[][] inputs, double[] desiredResults) {
		return evolve(kozaTableau.maxNumberOfGenerations, inputs, desiredResults);
		
	}
	
	public int evolve(int numberOfGenerations, double[][] inputs, double[] desiredResults) {
		
		int g = 0;
		
		evaluate(inputs, desiredResults);
		
		// For the desired number of generations or until the termination condition is reached.
		while (g < numberOfGenerations) {
				
			// Evolve the programs
			// Apply the evolutionary workflow
			for (GeneticOperator operator: geneticOperators) {
				operator.operate(programs);
			}

			g++;
			 
			evaluate(inputs, desiredResults);
	    	
	    	Program bestProgram = programs.getBestProgram();
	    	
	    	if (kozaTableau.autostop && bestProgram.getFitness() == 0.0) {
	    		return g;
	    	}
		}
		return g;
		
	}

	public void evaluate(double[][] inputs, double[] desiredResults) {
		// Evaluate the programs
		for (int p = 0; p < programs.programs.size(); p++) { 
			double[] actualResults = programs.getProgram(p).evaluate(inputs);
			programs.getProgram(p).setFitness(calculateErrorFitness(actualResults, desiredResults));
//			System.out.println("NDB::evaluate()~Program["+p+"].fitness()="+getProgram(p).fitness);
		}
	}

	public double calculateErrorFitness(double[] actualResults, double[] desiredResults) {
		double fitness = 0;
		for (int r = 0; r < actualResults.length; r++) { 
			double resultFitness = Math.abs(actualResults[r] - desiredResults[r]);
//			System.out.println("NDB::calculateErrorFitness()~act="+actualResults[r]+", des="+desiredResults[r]+", fit="+resultFitness+", ");
			fitness += resultFitness;
		}
		return fitness;
	}
	
}
