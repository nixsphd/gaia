package ie.nix.ea;

import ie.nix.ea.Population.Genotype;

/**
 * @author Nicola Mc Donnell
 *
 */
public interface Agent<T> {
	
	/**
	 * @param genotype - the genotype to be decoded by the Agent.
	 */
	public void decode(Genotype<T> genotype);
	
	/**
	 * @return a positive number greater than zero which represents the evalutaion. The higher the number the better the evaluation
	 */
	public double evaluation();

}
