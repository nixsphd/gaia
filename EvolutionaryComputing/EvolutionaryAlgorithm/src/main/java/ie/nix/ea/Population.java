package ie.nix.ea;

import java.util.ArrayList;
import java.util.List;


public abstract class Population<T> {

	public static abstract class Genotype<T> {

		private final int lenghtOfGenotype;
		protected T[] chromosome;
		
		private double evaluation;
		
		public Genotype(Genotype<T> chromosomeToClone) {
			this(chromosomeToClone.getChromosome());
		}
		
		public Genotype(int lenghtOfGenotype) {
			this.lenghtOfGenotype = lenghtOfGenotype;
			initRandomChromosome();
		}
		
		protected Genotype(T[] chromosomeToClone) {
			this.lenghtOfGenotype = chromosomeToClone.length;
			initChromosome();
			System.arraycopy(chromosomeToClone, 0, chromosome, 0, chromosomeToClone.length);
		}
			
		public int getLenghtOfGenotype() {
			return lenghtOfGenotype;
		}

		public T[] getChromosome() {
			return chromosome;
		}

		public double getEvaluation() {
			return evaluation;
		}

		public void setEvaluation(double evaluation) {
			this.evaluation = evaluation;
		}

		@Override
		public String toString() {
			return toString(0, chromosome.length-1);
		}
		
		public String toString(int fromIndex, int toIndex) {
			StringBuilder string = new StringBuilder();
			string.append(String.format("%.2f",evaluation)+"<-");
			for (int g = fromIndex; g <= toIndex; g++) {
				string.append(chromosome[g]+", ");
			}
			string.replace(string.length()-2, string.length(),"");
			return string.toString();
		}

		protected abstract void initChromosome();

		protected abstract void initRandomChromosome();
		
	}

	private final int lenghtOfGenotype;
	private List<Genotype<T>> genotypes;

	public abstract Genotype<T> createGenotype();
	
	public abstract Genotype<T> createGenotype(Genotype<T> genotypeToClone);
	
	public abstract Genotype<T> createGenotype(T[] genotypeToClone);
	
	public List<Genotype<T>> createGenotype(int numberOfGenotypes) {
		ArrayList<Genotype<T>> genotypes = new ArrayList<Genotype<T>>(numberOfGenotypes);			
		// Initialise all the genotyoess randomly
		for (int n = 0; n < numberOfGenotypes; n++) {
			genotypes.add(n, createGenotype());
		}			
		return genotypes;
    }

	public List<Genotype<T>> createGenotype(T[][] preCannedPopulation) {
		ArrayList<Genotype<T>> genotypes = new ArrayList<Genotype<T>>();
		// Initialise all the genotyoess based on the passed pre canned values
		for (int n = 0; n < preCannedPopulation.length; n++) {
			genotypes.add(n, createGenotype(preCannedPopulation[n]));
		}			
		return genotypes;
	}

	public int getLenghtOfGenotype() {
		return lenghtOfGenotype;
	}

	public int getNumberOfGenotypes() {
		return genotypes.size();
	}

	public List<Genotype<T>> getGenotypes() {
		return genotypes;
	}

	protected Population(int lenghtOfGenotype) {
		this(lenghtOfGenotype, new ArrayList<Genotype<T>>());
	}
	
	protected Population(int lenghtOfGenotype, List<Genotype<T>> genotypes) {
		this.lenghtOfGenotype = lenghtOfGenotype;
		setGenotypes(genotypes);
	}

	public void initRandomGenotype() {
		addGenotype(createGenotype());
	}

	public void initRandomGenotypes(int numberOfGenotypes) {
		addGenotypes(createGenotype(numberOfGenotypes));
	}
	
	public void initGenotypes(T[][] preCannedPopulation) {
		setGenotypes(createGenotype(preCannedPopulation));
	}

	public void setGenotypes(List<Genotype<T>> genotypes) {
		this.genotypes = genotypes;
		
	}

	protected Genotype<T> getGenotype(int index) {
		return genotypes.get(index);
	}

	protected void addGenotype(Genotype<T> genotype) {
		genotypes.add(genotype);
	}
	
	protected void addGenotypes(List<Genotype<T>> genotypes) {
		this.genotypes.addAll(genotypes);
	}
}
