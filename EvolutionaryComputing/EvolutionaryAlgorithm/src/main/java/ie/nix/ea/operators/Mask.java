package ie.nix.ea.operators;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Mask<T> implements GeneticOperator<T> {
	
	public enum STATE {
		MASKED,
		UNMASKED;
	} STATE state = STATE.UNMASKED;
	
	private boolean[] mask;
	List<Genotype<T>> maskedGenotypes;
	
	public Mask(boolean[] mask) {
		this.mask = mask;
	}

	public Population<T> operate(Population<T> population) {
		int sizeOfPopulation = population.getNumberOfGenotypes();
		if (state == STATE.UNMASKED) {
			// MASKING
			List<Genotype<T>> genotypes = population.getGenotypes();
			maskedGenotypes = new ArrayList<Genotype<T>>(sizeOfPopulation);
			
			List<Genotype<T>> unmaskedGenotypes = new ArrayList<Genotype<T>>(sizeOfPopulation);
			for (int g = 0; g < mask.length; g++) {
				if (mask[g]) {
					maskedGenotypes.add(genotypes.get(g));
				} else {
					unmaskedGenotypes.add(genotypes.get(g));
				}
			}			
			
			population.setGenotypes(unmaskedGenotypes);
			state = STATE.MASKED;
		} else {
			// UNMASKING
			List<Genotype<T>> genotypes = new ArrayList<Genotype<T>>(sizeOfPopulation);
			Iterator<Genotype<T>> maskedGenotypesIt = maskedGenotypes.iterator();
			Iterator<Genotype<T>> unmaskedGenotypesIt = population.getGenotypes().iterator();
			for (int g = 0; g < mask.length; g++) {
				if (mask[g]) {
					genotypes.add(maskedGenotypesIt.next());
				} else {
					genotypes.add(unmaskedGenotypesIt.next());
				}
			}	
			population.setGenotypes(genotypes);
			state = STATE.UNMASKED;
			
		}
		return population;
	}
	
}