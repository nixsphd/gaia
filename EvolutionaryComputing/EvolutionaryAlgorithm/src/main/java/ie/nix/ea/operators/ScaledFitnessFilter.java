package ie.nix.ea.operators;

import java.util.List;

import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;

public class ScaledFitnessFilter<T> extends BestWorstEvaluation<T> {
	
	private double averageEvaluation;
	
	@Override
	public Population<T>  operate(Population<T>  population) {
		// Get the worst evaluation
		population = super.operate(population);

		// Get the average evaluation
		AverageEvaluation<T> averageEvaluationOperator = new AverageEvaluation<T>();
		population = averageEvaluationOperator.operate(population);
		averageEvaluation = averageEvaluationOperator.getAverage();

		// Calculate their fitness
		List<Genotype<T> > genotypes = population.getGenotypes();
		for (int agentIndex = 0; agentIndex < genotypes.size(); agentIndex++) {
			// Scale the fitness, if requested
			double evaluation = genotypes.get(agentIndex).getEvaluation();
			genotypes.get(agentIndex).setEvaluation((evaluation-worstEvaluation)/averageEvaluation);
		}
		return population;
		
	}

	public double getAverageEvaluation() {
		return averageEvaluation;
	}
	
}