package ie.nix.ea.operators;

import java.util.List;

import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;

public class FitnessFilter<T> extends AverageEvaluation<T> {

	@Override
	public Population<T> operate(Population<T> population) {		
		population = super.operate(population);

		// Calculate their fitness
		List<Genotype<T>> genotypes = population.getGenotypes();
		for (int agentIndex = 0; agentIndex < genotypes.size(); agentIndex++) {
			// Scale the fitness, if requested
			double initialEvaluation = genotypes.get(agentIndex).getEvaluation();
			genotypes.get(agentIndex).setEvaluation(initialEvaluation/average);
		}
		return population;
	}

}