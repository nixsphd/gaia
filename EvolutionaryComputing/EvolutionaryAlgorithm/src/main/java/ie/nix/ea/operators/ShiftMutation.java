package ie.nix.ea.operators;

import java.util.List;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;
import ie.nix.util.Randomness;

public class ShiftMutation implements GeneticOperator<Double> {

	private double mutationProbabillity;
	private double mutationSpread; 
	private boolean loop;
	private boolean clamp;
	
	
	public ShiftMutation(double mutationProbabillity, double mutationSpread) {
		this.mutationProbabillity = mutationProbabillity;
		this.mutationSpread = mutationSpread;
		this.loop = false;
		this.clamp = false;
	}
	
	public Population<Double> operate(Population<Double> population) { //, double[] evaluations) {
		// Get the exiting genotypes
		List<Genotype<Double>> genotypes = population.getGenotypes();		
		for (Genotype<Double> genotype : genotypes) {
			Double[] chromosome = genotype.getChromosome();			
			for (int gene = 0; gene < chromosome.length; gene++) {	
				if (Randomness.nextDouble() <= mutationProbabillity) {
					// Mutate
					double shift = Randomness.nextGaussian(mutationSpread);
					
//					System.out.print("NDB::ShiftMutation()~"+chromosome[gene]+" + shift="+shift);
					if (loop) {
						chromosome[gene] = loopValue(chromosome[gene]+shift);
					} else if (clamp) {
						chromosome[gene] = clampValue(chromosome[gene]+shift);
					} else {
						chromosome[gene] = chromosome[gene]+shift;
					}
//					System.out.println(" -> "+chromosome[gene]);			
				}
			}
		}
		return population;
	}
	
	public boolean loop() {
		return loop;
	}

	public void loop(boolean loop) {
		this.loop = loop;
	}

	protected double loopValue(double value) {
		double wrap = (value%1.0);
		if (value < 0.0) {
			return 1.0 + wrap;
			
		} else if (value > 1.0) {
			return wrap;
			
		} else {
			return value; 
			
		}
	}

	public boolean clamp() {
		return clamp;
	}

	public void clamp(boolean clamp) {
		this.clamp = clamp;
	}

	protected double clampValue(double value) {
		if (value < 0.0) {
			return 0.0;			
			
		} else if (value > 1.0) {
			return 1.0;
			
		} else {
			return value;
		}
	}
	
}