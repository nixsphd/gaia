package ie.nix.ea.operators;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("rawtypes")
public class EliteSelection<T> implements GeneticOperator<T> {

	private int numberOfElites;
	
	public EliteSelection(int numberOfElites) {
		this.numberOfElites = numberOfElites;
	}
	
	@SuppressWarnings("unchecked")
	public Population operate(Population population) {
		// Get the exiting genotypes
		int sizeOfPopulation = population.getNumberOfGenotypes();
		List<Genotype> parentGenotypes = population.getGenotypes();		
		List<Genotype> selectedGenotypes = new ArrayList<Genotype>(sizeOfPopulation);

//		// reversed to get the highest fitness first.
		Comparator<Genotype> compareEvaluations = (Genotype parnet1, Genotype parent2)->
			Double.compare(parent2.getEvaluation(),parnet1.getEvaluation());
		Collections.sort(parentGenotypes, compareEvaluations);
//				parnet1.getName().compareTo(parent2.getName());
				
		
		for (int p = 0; p < sizeOfPopulation; p++) {
			int parent = p%numberOfElites;
//			System.out.println("NDB~parent="+parent);
			Genotype selected = parentGenotypes.get(parent);
			Genotype selectedGenotype = population.createGenotype(selected);
			selectedGenotypes.add(selectedGenotype);
		}
		
		// Assign the selectedGenotypes to the population
		population.setGenotypes(selectedGenotypes);
		// And return it.
		return population;
	}

	public boolean[] generateMask(int sizeOfPopulation) {
		boolean[] mask = new boolean[sizeOfPopulation];
		for (int p = 0; p < sizeOfPopulation; p++) {
			if (p < numberOfElites) {
				mask[p] = true;
			} else {
				mask[p] = false;
			}
		}
		return mask;
	}
}