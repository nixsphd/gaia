package ie.nix.ea.operators;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;
import ie.nix.util.Randomness;

import java.util.List;

public class FlippingMutation implements GeneticOperator<Integer> {
	
	private double mutationProbabillity;
	
	public FlippingMutation(double mutationProbabillity) {
		this.mutationProbabillity = mutationProbabillity;
	}
		
	public Population<Integer> operate(Population<Integer> population) {
		// Get the exiting genotypes
		List<Genotype<Integer>> genotypes = population.getGenotypes();
		
		for (Genotype<Integer> genotype : genotypes) {
			Integer[] chromosome = genotype.getChromosome();
			
			for (int gene = 0; gene < chromosome.length; gene++) {
				if (mutationProbabillity > Randomness.nextDouble()) {
					// Mutate
					chromosome[gene] = (chromosome[gene] + 1) % 2;
//						System.out.println("NDB::mutateByFlipping()~Flipping "+genotype+" at "+g);
				}
			}
		}
		return population;
	}
}