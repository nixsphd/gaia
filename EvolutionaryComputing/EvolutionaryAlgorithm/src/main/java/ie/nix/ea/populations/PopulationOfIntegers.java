package ie.nix.ea.populations;

import ie.nix.ea.Population;
import ie.nix.util.Randomness;

public class PopulationOfIntegers extends Population<Integer> {

	public class GenotypeOfIntegers extends Genotype<Integer> {
		
		public GenotypeOfIntegers(int lenghtOfGenotype) {
			super(lenghtOfGenotype);
		}

		public GenotypeOfIntegers(Genotype<Integer> genotype) {
			super(genotype);
			
		}
		
		public GenotypeOfIntegers(Integer[] chromosomeToClone) {
			super(chromosomeToClone);
		}

		@Override
		protected void initChromosome() {
			chromosome = new Integer[getLenghtOfGenotype()];
		}

		@Override
		protected void initRandomChromosome() {
			initChromosome();
			for (int g = 0; g < getLenghtOfGenotype(); g++) {
				chromosome[g] = Randomness.nextBinary();
			}
			
		}
		
	}

	public PopulationOfIntegers(int lenghtOfGenotype) {
		super(lenghtOfGenotype);
	}

	public Genotype<Integer> createGenotype() {
		return new GenotypeOfIntegers(getLenghtOfGenotype());
    }

	@Override
	public Genotype<Integer> createGenotype(Genotype<Integer> genotypeToClone) {
		return new GenotypeOfIntegers(genotypeToClone);
	}

	@Override
	public Genotype<Integer> createGenotype(Integer[] genotypeToClone) {
		return new GenotypeOfIntegers(genotypeToClone);
	}
	
}
