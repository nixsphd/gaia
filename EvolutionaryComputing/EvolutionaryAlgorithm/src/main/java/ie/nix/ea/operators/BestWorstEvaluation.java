package ie.nix.ea.operators;

import java.util.List;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;

public class BestWorstEvaluation<T> implements GeneticOperator<T> {

	protected double bestEvaluation;
	protected double worstEvaluation;

	protected int bestAgentIndex;
	protected int worstAgentIndex;
	
	public Population<T>  operate(Population<T> population) {	
		bestEvaluation = Double.NEGATIVE_INFINITY;
		worstEvaluation = Double.POSITIVE_INFINITY;
		bestAgentIndex = 0;
		worstAgentIndex = 0;
		
		List<Genotype<T>> genotypes = population.getGenotypes();

		// Evaluate all agents
		for (int agentIndex = 0; agentIndex < genotypes.size(); agentIndex++) {
			double evaluation = genotypes.get(agentIndex).getEvaluation();
			if (bestEvaluation < evaluation) {
				bestEvaluation = evaluation;
				bestAgentIndex = agentIndex;
			}	
			if (worstEvaluation > evaluation) {
				worstEvaluation = evaluation;
				worstAgentIndex = agentIndex;
			}	
		}
		return population;
	}

	public double getBestEvaluation() {
		return bestEvaluation;
	}

	public double getWorstEvaluation() {
		return worstEvaluation;
	}

	public int getBestAgentIndex() {
		return bestAgentIndex;
	}

	public int getWorstAgentIndex() {
		return worstAgentIndex;
	}
	
}