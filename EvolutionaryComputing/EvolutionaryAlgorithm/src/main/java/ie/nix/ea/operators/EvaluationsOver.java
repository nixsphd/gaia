package ie.nix.ea.operators;

import java.util.List;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;

@SuppressWarnings("rawtypes")
public class EvaluationsOver implements GeneticOperator {

	protected double averageEvalutaion;
	protected double over;
	protected int evaluationsOver;

	public EvaluationsOver(double over) {
		this.over = over;
	}
	
	@SuppressWarnings("unchecked")
	public Population operate(Population population) {		
		List<Genotype> genotypes = population.getGenotypes();
		// Evaluate all agents
		evaluationsOver = 0;
		for (int agentIndex = 0; agentIndex < genotypes.size(); agentIndex++) {
			if (genotypes.get(agentIndex).getEvaluation() < over) {
				evaluationsOver++;
			}
		}
//		System.out.println("NDB::EvaluationsOver("+over+").operate()~evaluationsOver="+evaluationsOver);
		return population;
	}

	public int getEvaluationsOver() {
		return evaluationsOver;
	}

}