package ie.nix.ea.operators;

import java.util.List;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;

@SuppressWarnings("rawtypes")
public class EvaluationsUnder implements GeneticOperator {

	protected double averageEvalutaion;
	protected double under;
	protected int evaluationsUnder;

	public EvaluationsUnder(double under) {
		this.under = under;
	}
	
	@SuppressWarnings("unchecked")
	public Population operate(Population population) {		
		List<Genotype> genotypes = population.getGenotypes();
		// Evaluate all agents
		evaluationsUnder = 0;
		for (int agentIndex = 0; agentIndex < genotypes.size(); agentIndex++) {
			if (genotypes.get(agentIndex).getEvaluation() < under) {
				evaluationsUnder++;
			}
		}
//		System.out.println("NDB::EvaluationsUnder.operate("+under+")~evaluationsUnder="+evaluationsUnder);
		return population;
	}

	public int getEvaluationsUnder() {
		return evaluationsUnder;
	}

}