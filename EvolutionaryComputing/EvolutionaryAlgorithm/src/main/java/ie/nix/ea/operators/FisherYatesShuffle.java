package ie.nix.ea.operators;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;
import ie.nix.util.Randomness;

import java.util.List;

public class FisherYatesShuffle<T> implements GeneticOperator<T> {

	private double shuffleProbabillity;
	
	public FisherYatesShuffle(double shuffleProbabillity) {
		this.shuffleProbabillity = shuffleProbabillity;
	}
	
	public Population<T> operate(Population<T> population) {
		// Get the exiting genotypes
		List<Genotype<T>> genotypes = population.getGenotypes();
		
		for (int g = genotypes.size() - 1; g > 0; g--) {
			if (shuffleProbabillity > Randomness.nextDouble()) {				
				// This should be g-1 according to stsckoverflow
				// (http://stackoverflow.com/questions/1519736/random-shuffling-of-an-array)
				// which means the a chance that the genotype shuffles with itself.
				// I decided I didn't like this :)
				int index = Randomness.nextInt(g);
				// Simple swap
				Genotype<T> tmp = genotypes.get(index);
				genotypes.set(index, genotypes.get(g));
				genotypes.set(g, tmp);
			}
		}
		return population;
	}

	
}