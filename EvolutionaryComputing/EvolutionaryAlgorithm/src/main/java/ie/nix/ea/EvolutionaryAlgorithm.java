package ie.nix.ea;

import ie.nix.ea.Population.Genotype;

import java.util.ArrayList;
import java.util.List;

public abstract class EvolutionaryAlgorithm<T> {

	public interface GeneticOperator<T> {
		public Population<T> operate(Population<T> population); //, double[] evaluations);
	}
	
	public enum STATE {
		EVOLVED,
		EVALUATED;
	} STATE state = STATE.EVOLVED;
	
	private final int lenghtOfGenotype;
	private Population<T> population;
	private Population<T> previousPopulation;
	private List<Agent<T>> agents;
	private ArrayList<GeneticOperator<T>> geneticOperators;
	
	public EvolutionaryAlgorithm(int lenghtOfGenotype) {
		this.lenghtOfGenotype = lenghtOfGenotype;
		population = initPopulation();
		agents = new ArrayList<Agent<T>>();
		geneticOperators = new ArrayList<GeneticOperator<T>>();
	}
	

	public EvolutionaryAlgorithm(int lenghtOfGenotype, int numberOfAgents) {
		this(lenghtOfGenotype);
		
		// Initialise the populations
		getPopulation().initRandomGenotypes(numberOfAgents);	
		
		// Initialise the agents
		setAgents(initialiseAgents(numberOfAgents));

	}
	
	public EvolutionaryAlgorithm(T[][] preCannedPopulation) {
		this(preCannedPopulation[0].length);
		
		// Initialise the populations		
		List<Genotype<T>> genotypes = getPopulation().createGenotype(preCannedPopulation);
		getPopulation().setGenotypes(genotypes);
		
		// Initialise the agents
		setAgents(initialiseAgents(preCannedPopulation.length));
	}
	
	public int getNumberOfAgents() {
		return agents.size();
	}

	public void addAgent(Agent<T> newAgent) {
		agents.add(newAgent);
		if (population.getNumberOfGenotypes() < agents.size()) {
			population.initRandomGenotype();
		}
		Genotype<T> genotype = population.getGenotype(agents.size() - 1);
		newAgent.decode(genotype);
	}

	public void addAgents(List<Agent<T>> newAgents) {
		agents.addAll(newAgents);
		if (population.getNumberOfGenotypes() < agents.size()) {
			population.initRandomGenotypes(agents.size() - population.getNumberOfGenotypes());
		}
		decodeAgents();
	}

	public void setAgents(List<Agent<T>> agents) {
		this.agents.clear();
		addAgents(agents);
	}

	public List<Agent<T>> getAgents() {
		return agents;
	}

	protected abstract List<Agent<T>> initialiseAgents(int numberOfAgents);

	public Population<T> getPopulation() {
		return population;
	}
	
	public Population<T> getPreviousPopulation() {
		return previousPopulation;
	}


	public void addGeneticOperator(GeneticOperator<T> action) {
		geneticOperators.add(action);
	}

	public void evaluate() {
		if (state == STATE.EVALUATED) {
			System.err.println("NDB The Evolutionary Algorithm is trying to evaluate without out having first evolved??");
		}
		// Evaluate all agents
		int agentIndex = 0;
		for (Agent<T> agent : agents) {
			Genotype<T> genotype = population.getGenotype(agentIndex);
			genotype.setEvaluation(agent.evaluation());
			agentIndex++;
		}
		state = STATE.EVALUATED;
	}


	public void evolve() {
		if (state == STATE.EVOLVED) {
			System.err.println("NDB The Evolutionary Algorithm is trying to evolve without out having first evaluated??");
		}
		// Save the old population for the logging
		previousPopulation = initPopulation();
		previousPopulation.setGenotypes(population.getGenotypes());
		
		// Apply the evolutionary workflow
		for (GeneticOperator<T> operator: geneticOperators) {
			population = operator.operate(population);
		}
		
		// Decode the agents
		decodeAgents();
		state = STATE.EVOLVED;
	}


	public void evaluateAndEvolve(int numberOfGenerations) {
		while (numberOfGenerations > 0) {
			evaluateAndEvolve();
			numberOfGenerations--;
		}
	
	}

	public void evaluateAndEvolve() {
		evaluate();	
		evolve();
	
	}
	
	public void evolveAndEvaluate(int numberOfGenerations) {
		while (numberOfGenerations > 0) {
			evolveAndEvaluate();
			numberOfGenerations--;
		}
	
	}

	public void evolveAndEvaluate() {
		evolve();
		evaluate();	
	
	}

	protected ArrayList<GeneticOperator<T>> getWorkflow() {
		return geneticOperators;
	}

	protected abstract Population<T> initPopulation();

	protected void setPopulation(Population<T> population) {
		this.population = population;
	}

	protected int getLenghtOfGenotype() {
		return lenghtOfGenotype;
	}

	protected void decodeAgents() {
		for (Agent<T> agent : agents) {
			Genotype<T> genotype = population.getGenotype(agents.indexOf(agent));
			agent.decode(genotype);
		}
	}

}
