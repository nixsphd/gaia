package ie.nix.ea.operators;

import java.util.List;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population;
import ie.nix.ea.Population.Genotype;

public class AverageEvaluation<T> implements GeneticOperator<T>  {
 
	protected SummaryStatistics stats;
	protected double average;
	protected double standardDeviation;
	
	public Population<T> operate(Population<T>  population) {
		stats = new SummaryStatistics();
		List<Genotype<T> > genotypes = population.getGenotypes();
		// Evaluate all agents
		for (int agentIndex = 0; agentIndex < genotypes.size(); agentIndex++) {
			stats.addValue(genotypes.get(agentIndex).getEvaluation());
		}

		average =  stats.getMean();
		standardDeviation =  stats.getStandardDeviation();
		return population;
	}

	public double getAverage() {
		return average;
	}

	public Object getStandardDeviation() {
		return standardDeviation;
	}

}