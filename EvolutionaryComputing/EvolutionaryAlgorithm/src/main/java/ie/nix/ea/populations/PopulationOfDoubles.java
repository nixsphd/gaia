package ie.nix.ea.populations;

import ie.nix.ea.Population;
import ie.nix.util.Randomness;

public class PopulationOfDoubles extends Population<Double> {

	public class GenotypeOfDoubles extends Genotype<Double> {
		
		public GenotypeOfDoubles(int lenghtOfGenotype) {
			super(lenghtOfGenotype);
		}
		
		public GenotypeOfDoubles(Genotype<Double> genotype) {
			super(genotype);
		}
		
		public GenotypeOfDoubles(Double[] chromosomeToClone) {
			super(chromosomeToClone);
		}

		@Override
		protected void initChromosome() {
			chromosome = new Double[getLenghtOfGenotype()];
		}
		
		@Override
		protected void initRandomChromosome() {
			initChromosome();
			for (int g = 0; g < getLenghtOfGenotype(); g++) {
				chromosome[g] = Randomness.nextDouble();
			}
		}
		
	}

	public PopulationOfDoubles(int lenghtOfGenotype) {
		super(lenghtOfGenotype);
	}

	@Override
	public Genotype<Double> createGenotype() {
		return new GenotypeOfDoubles(getLenghtOfGenotype());
    }

	@Override
	public Genotype<Double> createGenotype(Genotype<Double> genotype) {
		return new GenotypeOfDoubles(genotype);
	}
	
	@Override
	public Genotype<Double> createGenotype(Double[] genotypeToClone) {
		return new GenotypeOfDoubles(genotypeToClone);
	}

}
