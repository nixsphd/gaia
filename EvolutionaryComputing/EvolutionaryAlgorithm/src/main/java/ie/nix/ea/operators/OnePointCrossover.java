package ie.nix.ea.operators;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;
import ie.nix.util.Randomness;

import java.util.List;

public class OnePointCrossover<T> implements GeneticOperator<T> {
	private double crossoverProbabillity;
	
	public OnePointCrossover(double crossoverProbabillity) {
		this.crossoverProbabillity = crossoverProbabillity;
	}

	public Population<T> operate(Population<T> population) { //, double[] evaluations) {
		// Get the exiting genotypes
		List<Genotype<T>> genotypes = population.getGenotypes();
		
		for (int genotype = 1; genotype < genotypes.size(); genotype = genotype + 2) {
			if (crossoverProbabillity > Randomness.nextDouble()) {					
				T[] chromosomeA = genotypes.get(genotype-1).getChromosome();
				T[] chromosomeB = genotypes.get(genotype).getChromosome();		
				// We need to cross between 1 and length -1
				int crossoverPoint = Randomness.nextInt(chromosomeA.length-1);	
				for (int gene = 0; gene < chromosomeA.length; gene++) {
					if (gene <= crossoverPoint) {
						// Swap
						T temp = chromosomeA[gene];
						chromosomeA[gene] = chromosomeB[gene];
						chromosomeB[gene] = temp;

					}
				}				
			}
		}			
		return population;
	}
	
}