package ie.nix.ea.operators;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;
import ie.nix.util.Randomness;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("rawtypes")
public class BinaryTournamentSelection<T> implements GeneticOperator<T> {

	private double probabilityOfSelectingFittest;
	
	public BinaryTournamentSelection(double pobabilityOfSelectingFittest) {
		this.probabilityOfSelectingFittest = pobabilityOfSelectingFittest;
	}
	
	@SuppressWarnings("unchecked")
	public Population operate(Population population) {
		// Get the exiting genotypes
		int sizeOfPopulation = population.getNumberOfGenotypes();
		List<Genotype> parentGenotypes = population.getGenotypes();		
		List<Genotype> selectedGenotypes = new ArrayList<Genotype>(sizeOfPopulation);

		for (int p = 0; p < sizeOfPopulation; p++) {
			int parentA = Randomness.nextInt(sizeOfPopulation-1);
			int parentB = Randomness.nextInt(sizeOfPopulation-1);
			// Ensure the parents aren't the same.
			while (parentA == parentB) {
//				System.out.println("NDB::operate()~A="+parentA+" and B="+ parentB+
//						"] are the same.");
				parentB = Randomness.nextInt(sizeOfPopulation-1);
			}
			Genotype genotypeA = parentGenotypes.get(parentA);
			Genotype genotypeB = parentGenotypes.get(parentB);
			double fittnessA = genotypeA.getEvaluation();
			double fittnessB = genotypeB.getEvaluation();
			double randomDouble = Randomness.nextDouble();
			boolean selectFittest = (randomDouble <= probabilityOfSelectingFittest);

//			System.out.println("NDB::operate()~selecting fittest = "+selectFittest+
//					" with probabilityOfSelectingFittest = "+probabilityOfSelectingFittest+
//					" and randomDouble = "+randomDouble);
			int selected = 0;
			if (fittnessA > fittnessB) {
				if (selectFittest) {
					// Select A
					selected = parentA;
				} else {
					// Select B
					selected = parentB;
				}
			} else {
				if (selectFittest) {
					// Select B
					selected = parentB;
				} else {
					// Select A
					selected = parentA;
				}
			}
			Genotype selectedGenotype = population.createGenotype(parentGenotypes.get(selected));
//			System.out.println("NDB::operate()~A["+genotypeA+"] vs B["+ genotypeB+
//					"] -> ["+selectedGenotype+"]");
			selectedGenotypes.add(selectedGenotype);
		}
		
		// Assign the selectedGenotypes to the population
		population.setGenotypes(selectedGenotypes);
		// And return it.
		return population;
	}
	
}