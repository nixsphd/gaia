package ie.nix.ea.operators;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.Population;
import ie.nix.util.Randomness;

import java.util.ArrayList;
import java.util.List;

public class StocasticUniversalSamplingSelection<T> implements GeneticOperator<T> {

	public StocasticUniversalSamplingSelection() {
	}
	
	public Population<T> operate(Population<T> population) { //, double[] evaluations) {
		// Get the exiting genotypes
		List<Genotype<T>> genotypes = population.getGenotypes();
		
		// Set the population size
		int populationSize = genotypes.size();
		
		// Set the number of agents to keep the number of agents we start with
		int numberToSelect = populationSize;

		// The intermediate population
		ArrayList<Genotype<T>> selectedGenotypes = new ArrayList<Genotype<T>>();

		// Calculate the accumulated fitnesses.
		double[] accumulatedFitnesses = new double[populationSize];
		accumulatedFitnesses[0] = genotypes.get(0).getEvaluation();
		for (int agentIndex = 1; agentIndex < populationSize; agentIndex++) {
			accumulatedFitnesses[agentIndex] = accumulatedFitnesses[agentIndex - 1] + genotypes.get(agentIndex).getEvaluation();
			
		}

		// Calculate the total fitness of population
		double totalFitness = accumulatedFitnesses[populationSize-1];

		// Calculate the distance between the pointers
		double distanceBetweenPointers = totalFitness/numberToSelect;

		// Select the start, using a random number between 0 and the distance
		// between the pointers
		double start = Randomness.nextDouble(0.0, distanceBetweenPointers);

		// For each of the new agents
		int oldAgentIndex = 0;
		for (int newAgentIndex = 0; newAgentIndex < numberToSelect; newAgentIndex++) {
			// Set the pointer
			double pointer = start + (newAgentIndex * distanceBetweenPointers);
			// System.out.println("NDB::selectionUsingStocasticUniversalSampling()~Pointer at "+pointer);
			while (accumulatedFitnesses[oldAgentIndex] < pointer) {
				// System.out.println("NDB::selectionUsingStocasticUniversalSampling()~Not selecting "+oldAgentIndex+" with Max acc fitness "+accumulatedFitnesses[oldAgentIndex]);
				oldAgentIndex++;
			}
			// Keep oldAgentIndex
			// System.out.println("NDB::selectionUsingStocasticUniversalSampling()~Selected "+oldAgentIndex+" with Max acc fitness "+accumulatedFitnesses[oldAgentIndex]);
			selectedGenotypes.add(population.createGenotype(genotypes.get(oldAgentIndex).getChromosome()));

//			System.out.println("NDB::operate()~Selecting "+oldAgentIndex+"=["+genotypes.get(oldAgentIndex)+
//					"] with fitness = "+accumulatedFitnesses[oldAgentIndex]);
		}
		// Assign the selectedGenotypes to the population
		population.setGenotypes(selectedGenotypes);
		// And return it.
		return population;
	}
	
}