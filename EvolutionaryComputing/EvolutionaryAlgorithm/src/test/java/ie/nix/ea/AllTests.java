package ie.nix.ea;

import ie.nix.ea.Population.Genotype;
import ie.nix.ea.TestModel.TestAgent;

import java.util.Arrays;
import java.util.List;

//@RunWith(Suite.class)
//@SuiteClasses({UnitTests.class})
public class AllTests {
	
	public static String toString(TestAgent[] actualAgents) {
		StringBuilder string = new StringBuilder("{\n");
		for (int a = 0; a < actualAgents.length; a++) {
			string.append(actualAgents[a]+",\n");
		}
		string.replace(string.length()-1, string.length(),"\n};");
		return string.toString();
	}

	public static boolean equals(TestAgent[] expectedAgents, TestAgent[] actualAgents) {	
		boolean equals = (expectedAgents.length == actualAgents.length);
		int agentNumber = 0;
		while (equals && (agentNumber < expectedAgents.length)) {
			equals = (expectedAgents[agentNumber].equals(actualAgents[agentNumber]));
			agentNumber++;
		}
		return equals;
	}	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static boolean equals(Number[][] expectedPopulation, Population population) {	
		return equals(expectedPopulation, population.getGenotypes());
	}	

	@SuppressWarnings("rawtypes")
	public static boolean equals(Number[][] expectedPopulation, List<Genotype> genotypes) {
		int g = 0;
		boolean equals = true;
		while (equals && g < expectedPopulation.length) {
			equals = equals(expectedPopulation[g], genotypes.get(g));
			g++;
		}		
		return equals;
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean equals(Number[] expectedGenotype, Genotype genotype) {		
		return Arrays.equals(expectedGenotype, genotype.getChromosome());
	}
	

	@SuppressWarnings({"rawtypes", "unchecked" })
	public static String toString(Population population) {		
		return toString(population.getGenotypes());
	}	
	
	@SuppressWarnings("rawtypes")
	public static String toString(List<Genotype> genotypes) {		
		StringBuilder string = new StringBuilder("{\n");
		for (int g = 0; g < genotypes.size(); g++) {
			
			string.append(toString(genotypes.get(g)));
			
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.length()-2, string.length(),"\n};");
		return string.toString();
	}
	
	@SuppressWarnings("rawtypes")
	public static String toString(Genotype genotype) {		
		StringBuilder string = new StringBuilder("{");
		for (int g = 0; g < genotype.getLenghtOfGenotype(); g++) {
			string.append(genotype.getChromosome()[g]+",");
		}
		string.replace(string.length()-1, string.length(),"}");
		return string.toString();
	}

}
