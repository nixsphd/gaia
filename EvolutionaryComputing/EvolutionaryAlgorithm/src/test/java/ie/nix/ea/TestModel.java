package ie.nix.ea;

import java.util.ArrayList;
import java.util.List;

import ie.nix.ea.Population.Genotype;
import ie.nix.ea.populations.PopulationOfDoubles;

public class TestModel extends EvolutionaryAlgorithm<Double> {

	public static class TestAgent implements Agent<Double> {

		double evaluation;
		Double[] genotype;
		
		public TestAgent() {
			this(1.0);
		}
		
		public TestAgent(double evaluation) {
			this.evaluation = evaluation;
		}

		public TestAgent(Double[] genotype, double evaluation) {
			this(evaluation);
			this.genotype = genotype;
		}

		public void decode(Genotype<Double> genotype) {
			this.genotype = genotype.getChromosome();
			
		}

		protected void setEvaluation(double evaluation) {
			this.evaluation = evaluation;
			
		}

		public double evaluation() {
			return evaluation;
		}
		
		@Override
		public String toString() {
			return "new TestAgent("+ 
					"new Double[]{"+genotype[0]+","+genotype[1]+","+genotype[2]+"}, "+
					evaluation+")";
		}
		
		public boolean equals(TestAgent TestAgent) {
			return ((Double.compare(genotype[0], TestAgent.genotype[0]) == 0) &&
					(Double.compare(genotype[1], TestAgent.genotype[1]) == 0) &&
					(Double.compare(genotype[2], TestAgent.genotype[2]) == 0) &&
					(Double.compare(evaluation, TestAgent.evaluation) == 0));
		}
		
	}
	
	public TestModel(int lenghtOfGenotype, int numberOfAgents) {
		super(lenghtOfGenotype, numberOfAgents);
	}
	
	public TestModel(Double[][] preCannedPopulation) {
		super(preCannedPopulation);
	}

	@Override
	protected Population<Double> initPopulation() {
		return new PopulationOfDoubles(getLenghtOfGenotype());
		
	}

	@Override
	protected List<Agent<Double>> initialiseAgents(int numberOfAgents) {
		List<Agent<Double>> agents = new ArrayList<Agent<Double>>();
		for (int a = 0; a < numberOfAgents; a++) {
			agents.add(new TestAgent(a+1));
		}
		return agents;
	}
	
	public void setEvaluations(double[] evaluations) {
		List<Agent<Double>> agents = this.getAgents();
		for (int a = 0; a < evaluations.length; a++) {
			TestAgent testAgent = (TestAgent)agents.get(a);
			testAgent.setEvaluation(evaluations[a]);
		}
	}

}
