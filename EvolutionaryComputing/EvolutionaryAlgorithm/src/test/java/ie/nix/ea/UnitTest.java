package ie.nix.ea;

import ie.nix.ea.EvolutionaryAlgorithm.GeneticOperator;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.TestModel.TestAgent;
import ie.nix.ea.operators.BinaryTournamentSelection;
import ie.nix.ea.operators.EliteSelection;
import ie.nix.ea.operators.FitnessFilter;
import ie.nix.ea.operators.Mask;
import ie.nix.ea.operators.OnePointCrossover;
import ie.nix.ea.operators.ScaledFitnessFilter;
import ie.nix.ea.operators.ShiftMutation;
import ie.nix.ea.operators.StocasticUniversalSamplingSelection;
import ie.nix.ea.populations.PopulationOfDoubles;
import ie.nix.ea.populations.PopulationOfIntegers;
import ie.nix.util.ToString;
import ie.nix.util.Randomness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UnitTest extends AllTests {

	@Before
	public void setUp() throws Exception {
		Randomness.setSeed(0);
	}
	
	@Test
	public void testCreateRandomPopulationWithIntegerGenes() {
		int numberOfGenotypes = 10;
		int lenghtOfGenotype = 30;
		
		PopulationOfIntegers population = new PopulationOfIntegers(lenghtOfGenotype);
		population.initRandomGenotypes(numberOfGenotypes);		
		
		int actualNumberOfGenotypes = population.getNumberOfGenotypes();
		Assert.assertEquals(numberOfGenotypes, actualNumberOfGenotypes);
		
		List<Genotype<Integer>> genotypes = population.getGenotypes();
		Assert.assertEquals(numberOfGenotypes, genotypes.size());
		
		Genotype<Integer> genotype = genotypes.get(0);
		int actualLenghtOfGenotype = genotype.getLenghtOfGenotype();
		Assert.assertEquals(lenghtOfGenotype, actualLenghtOfGenotype);
		
		Integer[][] expectedPopulation = {
			{1,1,1,0,0,0,1,0,1,0,0,0,1,0,1,1,0,0,0,0,1,0,1,1,1,1,1,1,0,0},
			{1,0,0,1,0,1,0,1,1,0,0,1,0,1,1,0,0,1,1,1,0,1,0,1,1,1,0,0,1,1},
			{0,1,0,1,0,1,0,1,1,1,0,0,0,0,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1},
			{0,0,1,1,1,1,1,0,0,0,1,1,1,1,0,0,0,1,1,0,0,0,0,0,1,1,0,1,1,0},
			{0,1,0,1,0,0,1,0,0,1,0,0,1,1,0,0,0,1,0,1,0,1,0,1,1,1,0,1,1,1},
			{0,0,0,1,0,1,1,0,0,0,1,0,1,1,0,1,0,0,0,1,1,1,0,1,0,0,0,0,0,1},
			{0,1,1,1,0,0,1,1,0,0,1,0,1,0,1,1,0,0,0,1,1,0,0,0,0,0,1,0,1,0},
			{0,0,0,1,1,1,0,0,0,0,0,0,1,0,0,1,1,1,1,1,1,1,0,1,0,0,0,0,1,1},
			{1,0,0,0,0,1,0,0,1,1,1,1,1,1,1,0,0,1,1,1,1,0,1,0,0,1,1,1,0,1},
			{0,1,1,1,1,1,1,0,0,1,1,0,0,0,1,1,0,1,0,1,0,0,1,1,1,1,1,0,0,0}};
//		System.out.println("NDB::testCreateRandomPopulationWithIntegerGenes()~population=\n"+toString(population));		
		Assert.assertTrue(equals(expectedPopulation, population));
		
	}
	
	@Test
	public void testCreatePreCannedPopulationWithIntegerGenes() {		
		Integer[][] preCannedPopulation = {
			{1,1,0,1,1,0,1,0,1,1,0,0,0,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1,0},
			{1,1,0,0,1,0,0,0,1,1,1,1,0,1,1,1,1,1,1,0,1,0,1,1,1,1,0,1,0,0},
			{1,0,1,1,1,0,0,0,0,0,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,0,0,0,0},
			{1,0,0,0,1,0,1,1,1,1,0,0,1,0,1,1,0,1,0,1,1,0,1,1,1,0,1,0,0,0},
			{1,0,1,1,1,0,1,1,0,0,0,0,1,0,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0},
			{1,0,1,0,1,1,1,1,0,0,0,1,1,1,0,0,0,0,0,1,0,1,1,1,0,1,0,1,1,0},
			{1,0,0,1,1,1,1,1,1,0,0,1,1,0,0,0,1,0,1,0,0,0,0,1,1,0,0,0,0,1},
			{1,1,1,1,1,0,0,1,0,0,1,0,0,1,0,1,0,0,1,1,0,0,0,0,1,0,0,1,0,0},
			{1,0,1,1,1,1,1,0,1,0,0,1,1,1,1,0,1,0,0,1,1,1,1,0,1,1,1,0,1,0},
			{0,1,1,0,0,0,0,1,1,0,1,1,0,1,1,0,0,1,0,0,1,1,0,1,1,0,1,0,1,0}};
		
		PopulationOfIntegers population = 
				new PopulationOfIntegers(preCannedPopulation[0].length);
		population.initGenotypes(preCannedPopulation);
		
		int actualNumberOfGenotypes = population.getNumberOfGenotypes();
		Assert.assertEquals(preCannedPopulation.length, actualNumberOfGenotypes);
		
		List<Genotype<Integer>> genotypes = population.getGenotypes();
		Assert.assertEquals(preCannedPopulation.length, genotypes.size());
		
		Genotype<Integer> genotype = genotypes.get(0);
		int actualLenghtOfGenotype = genotype.getLenghtOfGenotype();
		Assert.assertEquals(preCannedPopulation[0].length, actualLenghtOfGenotype);
		
//		System.out.println("NDB::testCreatePreCannedPopulationWithIntegerGenes()~population=\n"+toString(population));		
		Assert.assertTrue(equals(preCannedPopulation, population));
		
	}
	
	@Test
	public void testCreateRandomPopulationWithDoubleGenes() {
		int numberOfGenotypes = 10;
		int lenghtOfGenotype = 30;
		
		PopulationOfDoubles population = new PopulationOfDoubles(lenghtOfGenotype);
		population.initRandomGenotypes(numberOfGenotypes);		
		
		int actualNumberOfGenotypes = population.getNumberOfGenotypes();
		Assert.assertEquals(numberOfGenotypes, actualNumberOfGenotypes);
		
		List<Genotype<Double>> genotypes = population.getGenotypes();
		Assert.assertEquals(numberOfGenotypes, genotypes.size());
		
		Genotype<Double> genotype = genotypes.get(0);
		int actualLenghtOfGenotype = genotype.getLenghtOfGenotype();
		Assert.assertEquals(lenghtOfGenotype, actualLenghtOfGenotype);
		
		Double[][] expectedPopulation = {
			{0.730967787376657,0.24053641567148587,0.6374174253501083,0.5504370051176339,0.5975452777972018,0.3332183994766498,0.3851891847407185,0.984841540199809,0.8791825178724801,0.9412491794821144,0.27495396603548483,0.12889715087377673,0.14660165764651822,0.023238122483889456,0.5467397571984656,0.9644868606768501,0.10449068625097169,0.6251463634655593,0.4107961954910617,0.7763122912749325,0.990722785714783,0.4872328470301428,0.7462414053223305,0.7331520701949938,0.8172970714093244,0.8388903500470183,0.5266994346048661,0.8993350116114935,0.13393984058689223,0.0830623982249149},
			{0.9785743401478403,0.7223571191888487,0.7150310138504744,0.14322038530059678,0.4629578184224229,0.004485602182885184,0.07149831487989411,0.34842022979166454,0.3387696535357536,0.859356551354648,0.9715469888517128,0.8657458802140383,0.6125811047098682,0.17898798452881726,0.21757041220968598,0.8544871670422907,0.009673497300974332,0.6922930069529333,0.7713129661706796,0.7126874281456893,0.2112353749298962,0.7830924897671794,0.945333238959629,0.014236355103667941,0.3942035527773311,0.8537907753080728,0.7860424508145526,0.993471955005814,0.883104405981479,0.17029153024770394},
			{0.9620689182075386,0.7242950335788688,0.6773541612498745,0.8043954172246357,0.44142677367579175,0.46208799028599445,0.8528274665994607,0.501834850205735,0.9919429804102169,0.9692699099404161,0.35310607217911816,0.047265869196129406,0.0716236234178006,0.02910751272163581,0.48367019010510015,0.9719501209537452,0.9891171507514055,0.7674421030154899,0.5013973510122299,0.2555253108964435,0.30915818724818767,0.8482805002723425,0.052084538173983286,0.010175454536229256,0.35385296970871194,0.08673785516572752,0.8503115152643057,0.0036769023557003955,0.3078931676344727,0.5316085562487977},
			{0.9188142018385732,0.27721002606871137,0.8742622102831944,0.6098815135127635,0.9086392096967358,0.04449062015679506,0.6467239010388895,0.4968037636226561,0.5067015959528527,0.5206888198929495,0.36636074451399603,0.47763691175692136,0.7039697053426346,0.3227677982432213,0.011654838276547785,0.7010389381824046,0.7453528603915509,0.6072882485626178,0.2350817258641661,0.38904318355362855,0.9114683618429857,0.16738908858440693,0.003913530617220884,0.26489582745247164,0.8928169571561851,0.2975682649107815,0.4289044793297375,0.6875516381838638,0.4084964347010259,0.3530524994432197},
			{0.6717585674995369,0.7404171801693827,0.8944352245063432,0.5799577832686672,0.5107554799482598,0.3119697369337915,0.5402294041714198,0.9731076511749595,0.6136376025272587,0.027169061868886568,0.689017514541342,0.6302892759562045,0.8178375060850521,0.7411183464344743,0.631430080731879,0.3148923485089553,0.7503516379449476,0.19021973625862254,0.12250132750475151,0.7132889960353932,0.9330431321065386,0.31110877676339577,0.5295327756124881,0.17897794451820814,0.11388202463766328,0.6839413314680614,0.3792500319840173,0.5924290426242587,0.9406092652171198,0.5341455346895269},
			{0.21419469447284145,0.33372967718458435,0.25053617648864046,0.7355403649811287,0.09114656192592707,0.264547384486324,0.18866825776959184,0.29776138182005185,0.7584708799551577,0.04658802908190873,0.08606856789830353,0.8922236965205332,0.534638761596543,0.08564231896368091,0.5313525456947269,0.07339056097526375,0.17676904746799038,0.8834777753721988,0.3504328270340461,0.985905826227015,0.5758925384383466,0.6042474596168478,0.27310863493266946,0.5131868476049705,0.9447402599490898,0.21775045026242468,0.9098217394832929,0.019364114853117287,0.27420937493282704,0.6687934837703366},
			{0.2878767979158957,0.063992386430468,0.22626210633700294,0.1400669939745831,0.6036507737135993,0.7277478060617948,0.3038640241451339,0.625363038800921,0.035792771372967325,0.4773636348492961,0.09267023264241692,0.7106264668386636,0.434141935561723,0.8193475095058459,0.6045190667188953,0.6722448819053354,0.4500677802499238,0.34456078306876614,0.26939477859012284,0.745147167878201,0.2061639133327101,0.14953103222906605,0.9203722061518866,0.6876936745361386,0.91670273943429,0.03540663722934834,0.19515775498712384,0.5760065883903744,0.9649847470362273,0.33175207550407504},
			{0.13819289816426283,0.09730143918724832,0.8196935615755725,0.6699216111553905,0.42888780302905216,0.47994618771306174,0.8295934229254344,0.5395378113075877,0.5844042282429451,0.5973873157847135,0.37438313211457463,0.0016406267297417454,0.6895084228544839,0.7137473695267416,0.06924654490924442,0.5033618480974998,0.6913665065140029,0.800811424198559,0.7216166190450864,0.3415185461780098,0.06375595609712459,0.34928498115978324,0.520896327541072,0.9563085926419765,0.6930188442039097,0.1216193434713968,0.5053563416219522,0.9013300778200583,0.16718374502158095,0.34936439387004814},
			{0.6951562782217547,0.032435450509692876,0.2600348306642277,0.46492352836969997,0.4064122322397178,0.18547080092621315,0.2935084475188867,0.5757485983300507,0.16846059725291262,0.3717389688289575,0.23464858267117816,0.5384276448622121,0.3549442572568958,0.31006505007010876,0.8374548634922726,0.6707815148496379,0.10907374533481273,0.9356312223638416,0.8753074563380077,0.8520002717912566,0.3943931811673369,0.37828737167086945,0.5262556914936047,0.004013632188466398,0.034606348706039625,0.7868327923842013,0.2713078349126373,0.7217042409140645,0.4194957642222633,0.29080347938106976},
			{0.2449621762602091,0.12523646506488062,0.14869372015568383,0.33758840840971804,0.8375698134119287,0.8446875782921442,0.26104062144226003,0.6202372645509784,0.49072217777121396,0.4153318473559232,0.8544932182594116,0.820111624656587,0.04970748393769653,0.26134283329587416,0.8400210640907794,0.4285696915679812,0.7411815703190666,0.8763425138421781,0.2926567007183887,0.9831647875314927,0.8295508288903154,0.7711404676296971,0.36384127820832357,0.1563762609615572,0.27216568847986944,0.25217005454740304,0.001434241998354291,0.26868044993466456,0.24127553104428134,0.11854287457826251}
		};
//		System.out.println("NDB::testCreateRandomPopulationWithDoubleGenes()~population=\n"+toString(population));		
		Assert.assertTrue(equals(expectedPopulation, population));
		
	}
	
	@Test
	public void testCreatePreCannedPopulationWithDoubleGenes() {		
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2, 1.3},	
				{2.0, 2.1, 2.2, 2.3},	
				{3.0, 3.1, 3.2, 3.3},	
				{4.0, 4.1, 4.2, 4.3},
			};
		
		PopulationOfDoubles population = 
				new PopulationOfDoubles(preCannedPopulation[0].length);
		population.initGenotypes(preCannedPopulation);
		
		int actualNumberOfGenotypes = population.getNumberOfGenotypes();
		Assert.assertEquals(preCannedPopulation.length, actualNumberOfGenotypes);
		
		List<Genotype<Double>> genotypes = population.getGenotypes();
		Assert.assertEquals(preCannedPopulation.length, genotypes.size());
		
		Genotype<Double> genotype = genotypes.get(0);
		int actualLenghtOfGenotype = genotype.getLenghtOfGenotype();
		Assert.assertEquals(preCannedPopulation[0].length, actualLenghtOfGenotype);
		
//		System.out.println("NDB::testCreatePreCannedPopulationWithDoubleGenes()~population=\n"+toString(population));		
		Assert.assertTrue(equals(preCannedPopulation, population));
		
	}
	
	@Test
	public void testModelDoubleGenesRandom() {
		int numberOfAgents = 10;
		int lenghtOfGenotype = 3;
		TestAgent[] expectedAgents = {
				new TestAgent(new Double[]{0.730967787376657,0.24053641567148587,0.6374174253501083}, 1.0),
				new TestAgent(new Double[]{0.5504370051176339,0.5975452777972018,0.3332183994766498}, 2.0),
				new TestAgent(new Double[]{0.3851891847407185,0.984841540199809,0.8791825178724801}, 3.0),
				new TestAgent(new Double[]{0.9412491794821144,0.27495396603548483,0.12889715087377673}, 4.0),
				new TestAgent(new Double[]{0.14660165764651822,0.023238122483889456,0.5467397571984656}, 5.0),
				new TestAgent(new Double[]{0.9644868606768501,0.10449068625097169,0.6251463634655593}, 6.0),
				new TestAgent(new Double[]{0.4107961954910617,0.7763122912749325,0.990722785714783}, 7.0),
				new TestAgent(new Double[]{0.4872328470301428,0.7462414053223305,0.7331520701949938}, 8.0),
				new TestAgent(new Double[]{0.8172970714093244,0.8388903500470183,0.5266994346048661}, 9.0),
				new TestAgent(new Double[]{0.8993350116114935,0.13393984058689223,0.0830623982249149}, 10.0),
			};
		
		TestModel model  = new TestModel(lenghtOfGenotype, numberOfAgents);
		
		int actualLenghtOfGenotype = model.getLenghtOfGenotype();
		Assert.assertEquals(lenghtOfGenotype, actualLenghtOfGenotype);
		
		actualLenghtOfGenotype = model.getPopulation().getLenghtOfGenotype();
		Assert.assertEquals(lenghtOfGenotype, actualLenghtOfGenotype);
		
		List<Agent<Double>> agents = new ArrayList<Agent<Double>>();
		for (int a = 0; a < numberOfAgents; a++) {
			agents.add(new TestAgent(a+1));
		}		
		model.setAgents(agents);
		
		int actualNumberOfAgents = model.getNumberOfAgents();
		Assert.assertEquals(numberOfAgents, actualNumberOfAgents);
	
		int actualNumberOfGenotypes = model.getPopulation().getNumberOfGenotypes();
		Assert.assertEquals(numberOfAgents, actualNumberOfGenotypes);
		
		TestAgent[] actualAgents = model.getAgents().toArray(new TestAgent[numberOfAgents]);
//		System.out.println("NDB::testModelDoubleGenesRandom()~actualAgents=\n"+toString(actualAgents));
//		System.out.println("NDB::testModelDoubleGenesRandom()~expectedAgents=\n"+toString(expectedAgents));		
		Assert.assertTrue(equals(expectedAgents, actualAgents));
		
	}
	
	@Test
	public void testModelPreCanned() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		int lenghtOfGenotype = preCannedPopulation[0].length;
		int numberOfAgents = preCannedPopulation.length;
		TestAgent[] expectedAgents = {
				new TestAgent(new Double[]{1.0,1.1,1.2}, 1.0),
				new TestAgent(new Double[]{2.0,2.1,2.2}, 2.0),
				new TestAgent(new Double[]{3.0,3.1,3.2}, 3.0),
				new TestAgent(new Double[]{4.0,4.1,4.2}, 4.0),
			};
		
		TestModel model  = new TestModel(preCannedPopulation);
		
		int actualLenghtOfGenotype = model.getLenghtOfGenotype();
		Assert.assertEquals(lenghtOfGenotype, actualLenghtOfGenotype);
		
		actualLenghtOfGenotype = model.getPopulation().getLenghtOfGenotype();
		Assert.assertEquals(lenghtOfGenotype, actualLenghtOfGenotype);
		
		int actualNumberOfAgents = model.getNumberOfAgents();
		Assert.assertEquals(numberOfAgents, actualNumberOfAgents);

		int actualNumberOfGenotypes = model.getPopulation().getNumberOfGenotypes();
		Assert.assertEquals(numberOfAgents, actualNumberOfGenotypes);
		
		TestAgent[] actualAgents = model.getAgents().toArray(new TestAgent[numberOfAgents]);
//		System.out.println("NDB::testModelPreCanned()~actualAgents=\n"+toString(actualAgents));
//		System.out.println("NDB::testModelPreCanned()~expectedAgents=\n"+toString(expectedAgents));		
		Assert.assertTrue(equals(expectedAgents, actualAgents));
		
	}
	
	@Test
	public void testModelEvaluation() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double[] expectedEvaluations = {
				1.0,
				2.0,
				3.0,
				4.0
			};
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		
		// Get the evaluations
		model.evaluate();
		
		List<Genotype<Double>> genotypes = model.getPopulation().getGenotypes();
		double[] actualEvaluations = new double[genotypes.size()];
		for (int g = 0; g < genotypes.size(); g++) {
			actualEvaluations[g] = genotypes.get(g).getEvaluation();
		}
//		System.out.println("NDB::testModelEvaluation()~actualEvaluations=\n"+ToString.toString(actualEvaluations));
//		System.out.println("NDB::testModelEvaluation()~expectedEvaluations=\n"+ToString.toString(expectedEvaluations));		
		Assert.assertArrayEquals(expectedEvaluations, actualEvaluations, 0.0000001);
		
		model  = new TestModel(preCannedPopulation);
		model.evaluateAndEvolve();
		genotypes = model.getPopulation().getGenotypes();
		actualEvaluations = new double[genotypes.size()];
		for (int g = 0; g < genotypes.size(); g++) {
			actualEvaluations[g] = genotypes.get(g).getEvaluation();
		}	
//		System.out.println("NDB::testModelEvaluation()~actualEvaluations=\n"+ToString.toString(actualEvaluations));
//		System.out.println("NDB::testModelEvaluation()~expectedEvaluations=\n"+ToString.toString(expectedEvaluations));		
		Assert.assertArrayEquals(expectedEvaluations, actualEvaluations, 0.0000001);
		
	}
	
	@Test
	public void testModelFitness() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double[] expectedFitness = {
				0.4,
				0.8,
				1.2,
				1.6
			};
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		model.evaluate();
		
		FitnessFilter<Double> fitnessFilter = new FitnessFilter<Double>();		
		fitnessFilter.operate(model.getPopulation());
		// Get the fitness
		List<Genotype<Double>> genotypes = model.getPopulation().getGenotypes();
		double[] actualFitness = new double[genotypes.size()];
		for (int g = 0; g < genotypes.size(); g++) {
			actualFitness[g] = genotypes.get(g).getEvaluation();
		}
//		System.out.println("NDB::testModelFitness()~actualFitness=\n"+ToString.toString(actualFitness));
//		System.out.println("NDB::testModelFitness()~expectedFitness=\n"+ToString.toString(expectedFitness));		
		Assert.assertArrayEquals(expectedFitness, actualFitness, 0.0000001);
		
		model  = new TestModel(preCannedPopulation);
		model.addGeneticOperator(fitnessFilter);
		model.evaluateAndEvolve();
		// Get the fitness
		genotypes = model.getPopulation().getGenotypes();
		actualFitness = new double[genotypes.size()];
		for (int g = 0; g < genotypes.size(); g++) {
			actualFitness[g] = genotypes.get(g).getEvaluation();
		}
//		System.out.println("NDB::testModelFitness()~actualFitness=\n"+ToString.toString(actualFitness));
//		System.out.println("NDB::testModelFitness()~expectedFitness=\n"+ToString.toString(expectedFitness));		
		Assert.assertArrayEquals(expectedFitness, actualFitness, 0.0000001);
		
	}
	
	@Test
	public void testModelScaledFitness() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double[] expectedFitness = {
				0.0,
				0.4,
				0.7999999999999999,
				1.2000000000000002
			};
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		model.evaluate();
		
		// Get the fitness
		ScaledFitnessFilter<Double> scaledFitnessFilter = new ScaledFitnessFilter<Double>();
		scaledFitnessFilter.operate(model.getPopulation());
		// Get the fitness
		List<Genotype<Double>> genotypes = model.getPopulation().getGenotypes();
		double[] actualFitness = new double[genotypes.size()];
		for (int g = 0; g < genotypes.size(); g++) {
			actualFitness[g] = genotypes.get(g).getEvaluation();
		}
//		System.out.println("NDB::testModelFitness()~actualFitness=\n"+ToString.toString(actualFitness));
//		System.out.println("NDB::testModelFitness()~expectedFitness=\n"+ToString.toString(expectedFitness));		
		Assert.assertArrayEquals(expectedFitness, actualFitness, 0.0000001);
		
		model  = new TestModel(preCannedPopulation);
		model.addGeneticOperator(scaledFitnessFilter);
		model.evaluateAndEvolve();
		// Get the fitness
		genotypes = model.getPopulation().getGenotypes();
		actualFitness = new double[genotypes.size()];
		for (int g = 0; g < genotypes.size(); g++) {
			actualFitness[g] = genotypes.get(g).getEvaluation();
		}
//		System.out.println("NDB::testModelFitness()~actualFitness=\n"+ToString.toString(actualFitness));
//		System.out.println("NDB::testModelFitness()~expectedFitness=\n"+ToString.toString(expectedFitness));		
		Assert.assertArrayEquals(expectedFitness, actualFitness, 0.0000001);
		
	}
	
	@Test
	public void testModelStocasticUniversalSamplingSelection() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		Double[][] expectedSelectedPopulation = {
				{2.0,2.1,2.2},
				{3.0,3.1,3.2},
				{4.0,4.1,4.2},
				{4.0,4.1,4.2}
			};
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		// Get the fitness
		model.evaluate();
		GeneticOperator<Double> selectionOperator = new StocasticUniversalSamplingSelection<Double>();
		
		Population<Double> actualSelectedPopulation = selectionOperator.operate(model.getPopulation());
//		System.out.println("NDB::testModelSelection()~actualSelectedPopulation=\n"+toString(actualSelectedPopulation));
//		System.out.println("NDB::testModelSelection()~expectedSelectedPopulation=\n"+ToString.toString(expectedSelectedPopulation));		
		Assert.assertTrue(equals(expectedSelectedPopulation, actualSelectedPopulation));

		// Set up the model
		model  = new TestModel(preCannedPopulation);
		// Add the action
		model.addGeneticOperator(selectionOperator);		
		// Evolve
		model.evaluateAndEvolve();
		
//		System.out.println("NDB::testModelSelection()~actualSelectedPopulation=\n"+toString(actualSelectedPopulation));
//		System.out.println("NDB::testModelSelection()~expectedSelectedPopulation=\n"+ToString.toString(expectedSelectedPopulation));		
		Assert.assertTrue(equals(expectedSelectedPopulation, actualSelectedPopulation));
		
	}
	
	@Test
	public void testModelBinaryTournamentSelectionAlwaysFittest() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double pobabilityOfSelectingFittest = 1.0;
		Double[][] expectedSelectedPopulation = {
				{4.0,4.1,4.2},
				{3.0,3.1,3.2},
				{2.0,2.1,2.2},
				{3.0,3.1,3.2}
			};
		testModelBinaryTournamentSelection(preCannedPopulation, pobabilityOfSelectingFittest, 
				 expectedSelectedPopulation);
	}
	
	@Test
	public void testModelBinaryTournamentSelectionAlwaysUnfittest() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double pobabilityOfSelectingFittest = 0.0;
		Double[][] expectedSelectedPopulation = {
				{3.0,3.1,3.2},
				{1.0,1.1,1.2},
				{1.0,1.1,1.2},
				{2.0,2.1,2.2}
			};
		testModelBinaryTournamentSelection(preCannedPopulation, pobabilityOfSelectingFittest, 
				 expectedSelectedPopulation);
	}
		
	@Test
	public void testModelBinaryTournamentSelection55() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double pobabilityOfSelectingFittest = 0.55;
		Double[][] expectedSelectedPopulation = {
				{3.0,3.1,3.2},
				{3.0,3.1,3.2},
				{1.0,1.1,1.2},
				{2.0,2.1,2.2}
			};
		testModelBinaryTournamentSelection(preCannedPopulation, pobabilityOfSelectingFittest, 
				 expectedSelectedPopulation);
	}
	
	public void testModelBinaryTournamentSelection(Double[][] preCannedPopulation, 
			double pobabilityOfSelectingFittest, Double[][] expectedSelectedPopulation) {
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		
		// Get the fitness
		model.evaluate();
		GeneticOperator<Double> selectionOperator = new BinaryTournamentSelection<Double>(pobabilityOfSelectingFittest);
		
		Population<Double> actualSelectedPopulation = selectionOperator.operate(model.getPopulation());
//		System.out.println("NDB::testModelSelection()~actualSelectedPopulation=\n"+toString(actualSelectedPopulation));
//		System.out.println("NDB::testModelSelection()~expectedSelectedPopulation=\n"+ToString.toString(expectedSelectedPopulation));		
		Assert.assertTrue(equals(expectedSelectedPopulation, actualSelectedPopulation));

		// Set up the model
		Randomness.setSeed(0);
		model  = new TestModel(preCannedPopulation);
		
		// Add the action
		model.evaluate();
		selectionOperator = new BinaryTournamentSelection<Double>(pobabilityOfSelectingFittest);
		model.addGeneticOperator(selectionOperator);		
		
		// Evolve
		model.evolve();
		actualSelectedPopulation = model.getPopulation();
		
//		System.out.println("NDB::testModelSelection()~actualSelectedPopulation=\n"+toString(actualSelectedPopulation));
//		System.out.println("NDB::testModelSelection()~expectedSelectedPopulation=\n"+ToString.toString(expectedSelectedPopulation));		
		Assert.assertTrue(equals(expectedSelectedPopulation, actualSelectedPopulation));
		
	}
	
	@Test
	public void testEliteSelection1() {	
		Double[][] preCannedPopulation = {	
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},			
		};	
		int numberOfElites = 1;
		Double[][] expectedSelectedPopulation = {
				{4.0,4.1,4.2},
				{4.0,4.1,4.2},
				{4.0,4.1,4.2},
				{4.0,4.1,4.2},
			};
		testEliteSelection(preCannedPopulation, null, 
				numberOfElites, expectedSelectedPopulation);
	}
	
	@Test
	public void testEliteSelection2() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		int numberOfElites = 2;
		Double[][] expectedSelectedPopulation = {
				{4.0,4.1,4.2},
				{3.0,3.1,3.2},
				{4.0,4.1,4.2},
				{3.0,3.1,3.2}
			};
		testEliteSelection(preCannedPopulation, null, 
				numberOfElites, expectedSelectedPopulation);
	}
	
	@Test
	public void testEliteSelection2Reverse() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},			
		};	
		double[] evaluations = {
			0.50,
			0.25,
			1.0, 
			0.75,
		};
		int numberOfElites = 2;
		Double[][] expectedSelectedPopulation = {
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},	
			};
		testEliteSelection(preCannedPopulation, evaluations, numberOfElites, 
				 expectedSelectedPopulation);
	}
	
	@Test
	public void testEliteSelection3() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		int numberOfElites = 3;
		Double[][] expectedSelectedPopulation = {
				{4.0,4.1,4.2},
				{3.0,3.1,3.2},
				{2.0,2.1,2.2},
				{4.0,4.1,4.2}
			};
		testEliteSelection(preCannedPopulation, null, 
				numberOfElites, expectedSelectedPopulation);
	}

	public void testEliteSelection(Double[][] preCannedPopulation, double[] evaluations,
			int numberOfElites, Double[][] expectedSelectedPopulation) {
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		if (evaluations != null) {
			model.setEvaluations(evaluations);
		}
		
		// Get the fitness
		model.evaluate();
		GeneticOperator<Double> selectionOperator = new EliteSelection<Double>(numberOfElites);
		
		Population<Double> actualSelectedPopulation = selectionOperator.operate(model.getPopulation());
//		System.out.println("NDB::testEliteSelection()~actualSelectedPopulation=\n"+toString(actualSelectedPopulation));
//		System.out.println("NDB::testEliteSelection()~expectedSelectedPopulation=\n"+ToString.toString(expectedSelectedPopulation));		
		Assert.assertTrue(equals(expectedSelectedPopulation, actualSelectedPopulation));

		// Set up the model
		Randomness.setSeed(0);
		model  = new TestModel(preCannedPopulation);
		if (evaluations != null) {
			model.setEvaluations(evaluations);
		}
		
		// Add the action
		model.evaluate();
		selectionOperator = new EliteSelection<Double>(numberOfElites);
		model.addGeneticOperator(selectionOperator);		
		
		// Evolve
		model.evolve();
		actualSelectedPopulation = model.getPopulation();
		
//		System.out.println("NDB::testModelSelection()~actualSelectedPopulation=\n"+toString(actualSelectedPopulation));
//		System.out.println("NDB::testModelSelection()~expectedSelectedPopulation=\n"+ToString.toString(expectedSelectedPopulation));		
		Assert.assertTrue(equals(expectedSelectedPopulation, actualSelectedPopulation));
		
	}
	
	@Test
	public void testEliteSelectionMask1() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		int numberOfElites = 1;
		boolean[] expectedMask = new boolean[]{
			true,
			false,
			false,
			false,
		};
		testEliteSelectionMask(preCannedPopulation, null, numberOfElites, expectedMask);
	}
	
	@Test
	public void testEliteSelectionMask3() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		int numberOfElites = 3;
		boolean[] expectedMask = new boolean[]{
			true,
			true,
			true,
			false,
		};
		testEliteSelectionMask(preCannedPopulation, null, numberOfElites, expectedMask);
	}
	
	public void testEliteSelectionMask(Double[][] preCannedPopulation, double[] evaluations,
			int numberOfElites, boolean[] expectedMask) {
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		if (evaluations != null) {
			model.setEvaluations(evaluations);
		}
		
		// Get the fitness
		model.evaluate();
		EliteSelection<Double> selectionOperator = new EliteSelection<Double>(numberOfElites);
		selectionOperator.operate(model.getPopulation());
		boolean[] actualMask = selectionOperator.generateMask(preCannedPopulation.length);
		
		System.out.println("NDB::testEliteSelection()~actualMask=\n"+Arrays.toString(actualMask));
		System.out.println("NDB::testEliteSelection()~expectedMask=\n"+Arrays.toString(expectedMask));		
		Assert.assertTrue(Arrays.equals(expectedMask, actualMask));
	}
	
	@Test
	public void testModelShiftMutation0p05() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double mutationProbability = 1.0;
		double mutationSpread = 0.05;
		Double[][] expectedSelectedPopulation = {
				{0.9978259428412312,1.0488329501174598,1.1645403303148387},
				{2.0440669227615857,2.1523019356668898,2.1243273563921066},
				{2.946239243063231,3.083926081917888,3.2318100536159204},
				{4.06719107278922,4.123902451332247,4.187722005743856}
			};
		testModelShiftMutation(preCannedPopulation, mutationProbability, mutationSpread, 
				 expectedSelectedPopulation);
	}
	
	@Test
	public void testModelShiftMutation0p5() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double mutationProbability = 1.0;
		double mutationSpread = 0.5;
		Double[][] expectedSelectedPopulation = {
				{0.9782594284123116,0.5883295011745979,0.8454033031483885},
				{2.4406692276158584,2.623019356668898,1.4432735639210637},
				{2.4623924306323133,2.939260819178881,3.5181005361592024},
				{4.671910727892204,4.339024513322477,4.0772200574385575}
			};
		testModelShiftMutation(preCannedPopulation, mutationProbability, mutationSpread, 
				 expectedSelectedPopulation);
	}
	
	public void testModelShiftMutation(Double[][] preCannedPopulation, 
			double mutationProbability, double mutationSpread, Double[][] expectedSelectedPopulation) {
//		System.out.println("NDB::testModelSelection()~preCannedPopulation=\n"+ToString.toString(preCannedPopulation));
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		
		GeneticOperator<Double> mutationOperator = new ShiftMutation(mutationProbability, mutationSpread);
		
		Population<Double> actualMutatedPopulation = mutationOperator.operate(model.getPopulation());
//		System.out.println("NDB::testModelSelection()~actualMutatedPopulation=\n"+toString(actualMutatedPopulation));
//		System.out.println("NDB::testModelSelection()~expectedSelectedPopulation=\n"+ToString.toString(expectedSelectedPopulation));		
		Assert.assertTrue(equals(expectedSelectedPopulation, actualMutatedPopulation));

		Randomness.setSeed(0);
		
//		System.out.println("NDB::testModelSelection()~preCannedPopulation=\n"+ToString.toString(preCannedPopulation));
		// Set up the model
		model  = new TestModel(preCannedPopulation);
		mutationOperator = new ShiftMutation(mutationProbability, mutationSpread);
		// Add the action
		model.addGeneticOperator(mutationOperator);	
		
		// Evolve
		model.evaluateAndEvolve();
		actualMutatedPopulation = model.getPopulation();
		
//		System.out.println("NDB::testModelSelection()~actualMutatedPopulation=\n"+toString(actualMutatedPopulation));
//		System.out.println("NDB::testModelSelection()~expectedSelectedPopulation=\n"+ToString.toString(expectedSelectedPopulation));		
		Assert.assertTrue(equals(expectedSelectedPopulation, actualMutatedPopulation));
		
	}
	
	// testShuffle
	
	// testUniformCrossover
	@Test
	public void testUniformCrossoverOn() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double crossoverProbabillity = 1;
		Double[][] expectedPopulation = {
				{2.0,2.1,2.2},
				{1.0,1.1,1.2},
				{4.0,4.1,4.2},
				{3.0,3.1,3.2}
			};
		testUniformCrossover(preCannedPopulation, crossoverProbabillity, expectedPopulation);
	}
	
	@Test
	public void testUniformCrossoverOff() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		double crossoverProbabillity = 0;
		Double[][] expectedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},	
			};
		testUniformCrossover(preCannedPopulation, crossoverProbabillity, expectedPopulation);
	}
	
	@Test
	public void testUniformCrossover5050() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},					
		};	
		double crossoverProbabillity = 0.5;
		Double[][] expectedPopulation = {
				{1.0,1.1,1.2},
				{2.0,2.1,2.2},
				{4.0,4.1,4.2},
				{3.0,3.1,3.2}	
			};
		testUniformCrossover(preCannedPopulation, crossoverProbabillity, expectedPopulation);
	}
	
	public void testUniformCrossover(Double[][] preCannedPopulation, 
			double crossoverProbabillity, Double[][] expectedPopulation) {
//		System.out.println("NDB::testUniformCrossover()~preCannedPopulation=\n"+ToString.toString(preCannedPopulation));
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		
		GeneticOperator<Double> mutationOperator = new OnePointCrossover<Double>(crossoverProbabillity);
		
		Population<Double> actualMutatedPopulation = mutationOperator.operate(model.getPopulation());
//		System.out.println("NDB::testUniformCrossover()~actualMutatedPopulation=\n"+toString(actualMutatedPopulation));
//		System.out.println("NDB::testUniformCrossover()~expectedSelectedPopulation=\n"+ToString.toString(expectedPopulation));		
		Assert.assertTrue(equals(expectedPopulation, actualMutatedPopulation));

		Randomness.setSeed(0);
		
//		System.out.println("NDB::testUniformCrossover()~preCannedPopulation=\n"+ToString.toString(preCannedPopulation));
		// Set up the model
		model  = new TestModel(preCannedPopulation);
		mutationOperator = new OnePointCrossover<Double>(crossoverProbabillity);
		// Add the action
		model.addGeneticOperator(mutationOperator);	
		
		// Evolve
		model.evaluateAndEvolve();
		actualMutatedPopulation = model.getPopulation();
		
//		System.out.println("NDB::testUniformCrossover()~actualMutatedPopulation=\n"+toString(actualMutatedPopulation));
//		System.out.println("NDB::testUniformCrossover()~expectedPopulation=\n"+ToString.toString(expectedPopulation));		
		Assert.assertTrue(equals(expectedPopulation, actualMutatedPopulation));
		
	}
	
	// testOnePointCrossover
	
	@Test
	public void testMaskFirst() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		boolean[] mask = new boolean[]{
			true,
			false,
			false,
			false,
		};
		Double[][] expectedPopulation = {
				{1.0,1.1,1.2},
				{1.9999956518856825,2.099897665900235,2.1999290806606298},
				{3.000088133845523,3.100104603871334,3.1998486547127842},
				{3.9998924784861263,4.099967852163836,4.200063620107232},
			};
		testMask(preCannedPopulation, mask, expectedPopulation);
	}
	
	@Test
	public void testMaskLast() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		boolean[] mask = new boolean[]{
			false,
			false,
			false,
			true,
		};
		Double[][] expectedPopulation = {
				{0.9999956518856825,1.099897665900235,1.1999290806606295},
				{2.000088133845523,2.100104603871334,2.1998486547127842},
				{2.9998924784861263,3.099967852163836,3.200063620107232},
				{4.0,4.1,4.2}
			};
		testMask(preCannedPopulation, mask, expectedPopulation);
	}
	
	@Test
	public void testMaskAll() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		boolean[] mask = new boolean[]{
			true,
			true,
			true,
			true,
		};
		Double[][] expectedPopulation = {
				{1.0,1.1,1.2},
				{2.0,2.1,2.2},
				{3.0,3.1,3.2},
				{4.0,4.1,4.2}
			};
		testMask(preCannedPopulation, mask, expectedPopulation);
	}
	
	@Test
	public void testMaskNone() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		boolean[] mask = new boolean[]{
				false,
				false,
				false,
				false,
		};
		Double[][] expectedPopulation = {
				{0.9999956518856825,1.099897665900235,1.1999290806606295},
				{2.000088133845523,2.100104603871334,2.1998486547127842},
				{2.9998924784861263,3.099967852163836,3.200063620107232},
				{4.000134382145578,4.1000478049026645,4.199975444011488}
			};
		testMask(preCannedPopulation, mask, expectedPopulation);
	}
	
	@Test
	public void testMaskToggle() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},				
		};	
		boolean[] mask = new boolean[]{
				false,
				true,
				false,
				true,
		};
		Double[][] expectedPopulation = {
				{0.9999956518856825,1.099897665900235,1.1999290806606295},
				{2.0,2.1,2.2},
				{3.000088133845523,3.100104603871334,3.1998486547127842},
				{4.0,4.1,4.2}
			};
		testMask(preCannedPopulation, mask, expectedPopulation);
	}
	
	public void testMask(Double[][] preCannedPopulation, boolean[] mask, Double[][] expectedPopulation) {
//		System.out.println("NDB::testModelMask()~preCannedPopulation=\n"+ToString.toString(preCannedPopulation));
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		
		GeneticOperator<Double> maskOperator = new Mask<Double>(mask);
		GeneticOperator<Double> mutationOperator = new ShiftMutation(1, 0.0001);
		
		Population<Double> actualPopulation = maskOperator.operate(model.getPopulation());
		actualPopulation = mutationOperator.operate(actualPopulation);
		actualPopulation = maskOperator.operate(actualPopulation);
		
//		System.out.println("NDB::testModelMask()~actualPopulation=\n"+toString(actualPopulation));
//		System.out.println("NDB::testModelMask()~expectedPopulation=\n"+ToString.toString(expectedPopulation));		
		Assert.assertTrue(equals(expectedPopulation, actualPopulation));

		Randomness.setSeed(0);
		
//		System.out.println("NDB::testModelMask()~preCannedPopulation=\n"+ToString.toString(preCannedPopulation));
		// Set up the model
		model  = new TestModel(preCannedPopulation);
		maskOperator = new Mask<Double>(mask);
		mutationOperator = new ShiftMutation(1, 0.0001);
		
		// Add the action
		model.addGeneticOperator(maskOperator);	
		model.addGeneticOperator(mutationOperator);	
		model.addGeneticOperator(maskOperator);	
		
		// Evolve
		model.evaluateAndEvolve();
		actualPopulation = model.getPopulation();
		
//		System.out.println("NDB::testModelMask()~actualPopulation=\n"+toString(actualPopulation));
//		System.out.println("NDB::testModelMask()~expectedSelectedPopulation=\n"+ToString.toString(expectedPopulation));		
		Assert.assertTrue(equals(expectedPopulation, actualPopulation));
		
	}
	
	
	@Test
	public void testEliteMask() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},			
		};	
		double[] evaluations = {
			0.50,
			1.0, 
			0.75,
			0.25,
		};
		int numberOfElites = 2;
		Double[][] expectedPopulation = {
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},
				{1.999888130371809,2.0998655180640706,2.1999927007678615},
				{3.0002225159911013,3.1001524087739982,3.199824098724272}
			};
		testEliteMask(preCannedPopulation, evaluations, numberOfElites, expectedPopulation);
	}
	
	@Test
	public void testEliteMaskBig() {	
		Double[][] preCannedPopulation = {
				{1.0, 1.1, 1.2},	
				{2.0, 2.1, 2.2},	
				{3.0, 3.1, 3.2},	
				{4.0, 4.1, 4.2},	
				{5.0, 5.1, 5.2},	
				{6.0, 6.1, 6.2},	
				{7.0, 7.1, 7.2},			
		};	
		double[] evaluations = {
			0.50,
			1.1, 
			0.85,
			0.15,
			1.0, 
			0.75,
			0.25,
		};
		int numberOfElites = 2;
		Double[][] expectedPopulation = {
				{2.0, 2.1, 2.2},		
				{5.0, 5.1, 5.2},
				{2.0000456304893164,2.0998181129788884,2.2000918331508315},
				{5.000188368954129,5.100148962361294,5.199945970170152},
				{2.000081479900376,2.099926840891812,2.1998504356064963},
				{4.99999753179099,5.09991188098616,5.199956089310773},
				{2.000066925130638,2.099875360483531,2.1999709863394066}
			};
		testEliteMask(preCannedPopulation, evaluations, numberOfElites, expectedPopulation);
	}
	
	
	public void testEliteMask(Double[][] preCannedPopulation, double[] evaluations, int numberOfElites,
			Double[][] expectedPopulation) {
//		System.out.println("NDB::testModelMask()~preCannedPopulation=\n"+ToString.toString(preCannedPopulation));
		
		// Set up the model
		TestModel model  = new TestModel(preCannedPopulation);
		if (evaluations != null) {
			model.setEvaluations(evaluations);
		}
		
		// Get the fitness
		model.evaluate();
		EliteSelection<Double> selectionOperator = new EliteSelection<Double>(numberOfElites);
		Mask<Double> maskOperator = new Mask<Double>(selectionOperator.generateMask(preCannedPopulation.length));
		ShiftMutation mutationOperator = new ShiftMutation(1, 0.0001);
		
		Population<Double> actualopulation = selectionOperator.operate(model.getPopulation());		
		actualopulation = maskOperator.operate(actualopulation);
		actualopulation = mutationOperator.operate(actualopulation);
		actualopulation = maskOperator.operate(actualopulation);
		
		Population<Double> actualPopulation = maskOperator.operate(model.getPopulation());
		actualPopulation = mutationOperator.operate(actualPopulation);
		actualPopulation = maskOperator.operate(actualPopulation);
		
		System.out.println("NDB::testModelMask()~actualPopulation=\n"+toString(actualPopulation));
		System.out.println("NDB::testModelMask()~expectedPopulation=\n"+ToString.toString(expectedPopulation));		
		Assert.assertTrue(equals(expectedPopulation, actualPopulation));

		Randomness.setSeed(0);
		
////		System.out.println("NDB::testModelMask()~preCannedPopulation=\n"+ToString.toString(preCannedPopulation));
//		// Set up the model
//		model  = new TestModel(preCannedPopulation);
//		maskOperator = new Mask<Double>(mask);
//		mutationOperator = new ShiftMutation(1, 0.0001);
//		
//		// Add the action
//		model.addGeneticOperator(maskOperator);	
//		model.addGeneticOperator(mutationOperator);	
//		model.addGeneticOperator(maskOperator);	
//		
//		// Evolve
//		model.evaluateAndEvolve();
//		actualPopulation = model.getPopulation();
//		
////		System.out.println("NDB::testModelMask()~actualPopulation=\n"+toString(actualPopulation));
////		System.out.println("NDB::testModelMask()~expectedSelectedPopulation=\n"+ToString.toString(expectedPopulation));		
//		Assert.assertTrue(equals(expectedPopulation, actualPopulation));
		
	}
}
