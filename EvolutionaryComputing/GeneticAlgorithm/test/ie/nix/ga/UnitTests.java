package ie.nix.ga;

import ie.nix.util.Randomness;
import ie.nix.util.ToString;

import java.awt.Color;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UnitTests extends AllTests {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		Randomness.setSeed(0);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testCreateRandomPopulation() {
		int numberOfGenotypes = 10;
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.lenghtOfGenotype = 30;
		
		GeneticAlgorithm ga = new GeneticAlgorithm();
		
		Population population = ga.getPopulation();
		
		population.initRandomGenotypes(numberOfGenotypes);		
		
		int actualNumberOfGenotypes = population.getNumberOfGenotypes();
		Assert.assertEquals(numberOfGenotypes, actualNumberOfGenotypes);
		
		List<Genotype> genotypes = population.getGenotypes();
		Assert.assertEquals(numberOfGenotypes, genotypes.size());
		
		Genotype genotype = genotypes.get(0);
		int actualLenghtOfGenotype = genotype.getLenghtOfGenotype();
		Assert.assertEquals(params.lenghtOfGenotype, actualLenghtOfGenotype);
		
		int[][] expectedPopulation = {
			{1,1,0,1,1,0,1,0,1,1,0,0,0,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1,0},
			{1,1,0,0,1,0,0,0,1,1,1,1,0,1,1,1,1,1,1,0,1,0,1,1,1,1,0,1,0,0},
			{1,0,1,1,1,0,0,0,0,0,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,0,0,0,0},
			{1,0,0,0,1,0,1,1,1,1,0,0,1,0,1,1,0,1,0,1,1,0,1,1,1,0,1,0,0,0},
			{1,0,1,1,1,0,1,1,0,0,0,0,1,0,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0},
			{1,0,1,0,1,1,1,1,0,0,0,1,1,1,0,0,0,0,0,1,0,1,1,1,0,1,0,1,1,0},
			{1,0,0,1,1,1,1,1,1,0,0,1,1,0,0,0,1,0,1,0,0,0,0,1,1,0,0,0,0,1},
			{1,1,1,1,1,0,0,1,0,0,1,0,0,1,0,1,0,0,1,1,0,0,0,0,1,0,0,1,0,0},
			{1,0,1,1,1,1,1,0,1,0,0,1,1,1,1,0,1,0,0,1,1,1,1,0,1,1,1,0,1,0},
			{0,1,1,0,0,0,0,1,1,0,1,1,0,1,1,0,0,1,0,0,1,1,0,1,1,0,1,0,1,0}};
//		System.out.println("NDB::testCreateRandomPopulation()~population=\n"+toStringpopulation));		
		Assert.assertTrue(equals(expectedPopulation, population));
		
	}
	
	@Test
	public void testCreatePreCannedPopulation() {

		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		int[][] preCannedPopulation = {
			{1,1,0,1,1,0,1,0,1,1,0,0,0,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1,0},
			{1,1,0,0,1,0,0,0,1,1,1,1,0,1,1,1,1,1,1,0,1,0,1,1,1,1,0,1,0,0},
			{1,0,1,1,1,0,0,0,0,0,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1,0,0,0,0,0},
			{1,0,0,0,1,0,1,1,1,1,0,0,1,0,1,1,0,1,0,1,1,0,1,1,1,0,1,0,0,0},
			{1,0,1,1,1,0,1,1,0,0,0,0,1,0,1,1,1,1,1,1,0,0,0,1,0,0,0,0,0,0},
			{1,0,1,0,1,1,1,1,0,0,0,1,1,1,0,0,0,0,0,1,0,1,1,1,0,1,0,1,1,0},
			{1,0,0,1,1,1,1,1,1,0,0,1,1,0,0,0,1,0,1,0,0,0,0,1,1,0,0,0,0,1},
			{1,1,1,1,1,0,0,1,0,0,1,0,0,1,0,1,0,0,1,1,0,0,0,0,1,0,0,1,0,0},
			{1,0,1,1,1,1,1,0,1,0,0,1,1,1,1,0,1,0,0,1,1,1,1,0,1,1,1,0,1,0},
			{0,1,1,0,0,0,0,1,1,0,1,1,0,1,1,0,0,1,0,0,1,1,0,1,1,0,1,0,1,0}};
		
		GeneticAlgorithm ga = new GeneticAlgorithm(params);
		Population population = ga.getPopulation();
		population.initGenotypes(preCannedPopulation);
		
		int actualNumberOfGenotypes = population.getNumberOfGenotypes();
		Assert.assertEquals(preCannedPopulation.length, actualNumberOfGenotypes);
		
		List<Genotype> genotypes = population.getGenotypes();
		Assert.assertEquals(preCannedPopulation.length, genotypes.size());
		
		Genotype genotype = genotypes.get(0);
		int actualLenghtOfGenotype = genotype.getLenghtOfGenotype();
		Assert.assertEquals(preCannedPopulation[0].length, actualLenghtOfGenotype);
		
//		System.out.println("NDB::testCreateRandomPopulation()~population=\n"+toString(population));		
		Assert.assertTrue(equals(preCannedPopulation, population));
		
	}
	
	@Test
	public void testRandomGenotype() {
		int lenghtOfGenotype = 30;
		int[] expetedChromosome = {1,1,0,1,1,0,1,0,1,1,0,0,0,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1,0};
		
		Genotype genotype = new Genotype(lenghtOfGenotype);		
		int[] actualChromosome = genotype.getChromosome();		
//		System.out.println("NDB::testGenoType()~genotype="+toString(genotype));
		Assert.assertArrayEquals(expetedChromosome, actualChromosome);
		
	}
	
	@Test
	public void testPreCannedGenotype() {
		int[] preCannedGenotype = {1,1,0,1,1,0,1,0,1,1,0,0,0,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1,0};
		
		Genotype genotype = new Genotype(preCannedGenotype);		
		int[] actualChromosome = genotype.getChromosome();		
//		System.out.println("NDB::testGenoType()~genotype="+toString(genotype));
		Assert.assertArrayEquals(preCannedGenotype, actualChromosome);
		
	}
	
	@Test
	public void testAddAgent() {
		String expetedAgent = "RGBColourAgent[218,199,160]";

		GeneticAlgorithm ga = new GeneticAlgorithm(); 
		
		RGBColourAgent agent = new RGBColourAgent();
		ga.addAgent(agent);
		
		String actualAgent = agent.toString();	
//		System.out.println("NDB::testAddAgent()~actualAgent="+actualAgent);
		Assert.assertEquals(expetedAgent, actualAgent);
		
	}
	
	@Test
	public void testAddAgents() {
		int numberOfAgents = 10;
		String[] expetedAgents = {
				"RGBColourAgent[218,199,160]",
				"RGBColourAgent[200,247,235]",
				"RGBColourAgent[184,21,127]",
				"RGBColourAgent[139,203,91]",
				"RGBColourAgent[187,11,241]",
				"RGBColourAgent[175,28,23]",
				"RGBColourAgent[159,152,161]",
				"RGBColourAgent[249,37,48]",
				"RGBColourAgent[190,158,158]",
				"RGBColourAgent[97,182,77]"
				};

		GeneticAlgorithm ga = new GeneticAlgorithm();
		
		for (int a = 0; a < numberOfAgents; a++) {
			RGBColourAgent agent = new RGBColourAgent();
			ga.addAgent(agent);
		}

		String[] actualAgents = RGBColourAgent.toStringArray(ga.getAgents());
		
		System.out.println("NDB::testAddAgent()~actualAgents="+ToString.toString(actualAgents));
		Assert.assertArrayEquals(expetedAgents, actualAgents);
		
	}
	
	@Test
	public void testEvaluationPerfectScore() {
		Color targetColour = Color.WHITE;
		int[] chromosome = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
		double expetedEvaluation = 765;
		testEvaluation(targetColour, chromosome, expetedEvaluation);
		
	}	

	@Test
	public void testEvaluationFlunkieScore() {
		Color targetColour = Color.WHITE;
		int[] chromosome = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		double expetedEvaluation = 0;
		testEvaluation(targetColour, chromosome, expetedEvaluation);
		
	}
	
	@Test
	public void testEvaluationMiddlingScore() {
		Color targetColour = Color.WHITE;
		int[] chromosome = {1,1,0,1,1,0,1,0,1,1,0,0,0,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1,0};
		double expetedEvaluation = 577;
		testEvaluation(targetColour, chromosome, expetedEvaluation);
		
	}
	
	@Test
	public void testEvaluationWhiteTargetGray() {
		Color targetColour = Color.GRAY;
		int[] chromosome = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
		double expetedEvaluation = 384;
		testEvaluation(targetColour, chromosome, expetedEvaluation);
		
	}	
	
	@Test
	public void testEvaluationRandomTargetGray() {
		Color targetColour = Color.GRAY;
		int[] chromosome = {1,1,0,1,1,0,1,0,1,1,0,0,0,1,1,1,1,0,1,0,0,0,0,0,0,1,0,0,1,0};
		double expetedEvaluation = 572;
		testEvaluation(targetColour, chromosome, expetedEvaluation);
		
	}
	
	public void testEvaluation(Color targetColour, int[] chromosome, double expetedEvaluation) {		
		Genotype genotype = new Genotype(chromosome);			
		RGBColourAgent agent = new RGBColourAgent();
		RGBColourAgent.setTargetColour(targetColour);
		agent.decode(genotype);
		
		double actualEvaluation = agent.evaluation();
//		System.out.println("NDB::testEvaluation()~actualEvaluation="+actualEvaluation);
		Assert.assertEquals(expetedEvaluation, actualEvaluation,0.0000001);
		
	}

	@Test
	public void testFitness() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		Color targetColour = Color.WHITE;
		int[][] preCannedPopulation = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Perfect
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, // Last 6 bits are not used
				{0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Red
				{1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Blue
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1}, // Green
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // Worse score possible
				};
		double[] expectedFitness = {
				1.5,
				1.5,
				1.0,
				1.0,
				1.0,
				0.0};
		testFitness(params, targetColour, preCannedPopulation, expectedFitness);
	}
	
	@Test
	public void testScaledFitness() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.fitnessScaling = true;
		Color targetColour = Color.WHITE;
		int[][] preCannedPopulation = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Perfect
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0}, // Last 6 bits are not used
				{0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Red
				{1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Blue
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,1,1}, // Green
				{1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1}, // WorstFitness
				};
		double[] expectedFitness = {
				0.923076923076923,
				0.923076923076923,
				0.46153846153846156,
				0.46153846153846156,
				0.46153846153846156,
				0.0
				};
		testFitness(params, targetColour, preCannedPopulation, expectedFitness);
	}
	
	public void testFitness(GeneticAlgorithm.Parameters params, Color targetColour, int[][] preCannedPopulation, double[] expectedFitness) {		
		GeneticAlgorithm ga = new GeneticAlgorithm();
		ga.getPopulation().initGenotypes(preCannedPopulation);
		
		RGBColourAgent.setTargetColour(targetColour);
		for (int a = 0; a < preCannedPopulation.length; a++) {
			RGBColourAgent agent = new RGBColourAgent();
			ga.addAgent(agent);
		}
		
		double[] actualFitness = ga.calculateFitnesses();		
		System.out.println("NDB::testFitness()~actualFitness="+ToString.toString(actualFitness));
		Assert.assertArrayEquals(expectedFitness, actualFitness,0.0000001);
		
	}
	
	@Test
	public void testSelectionKeepFirst() {		
		int[][] preCannedPopulation = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Perfect
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // Worse score possible
				};
		double[] preCannedFitnesses = {
				1.5,
				0.0
				};
		int[][] expectedNewGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1} 
				};
		testSelection(preCannedPopulation, preCannedFitnesses, expectedNewGenotypes);	
		
	}
	
	@Test
	public void testSelectionKeepSecond() {		
		int[][] preCannedPopulation = {
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // Worse score possible
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}  // Perfect
				};
		double[] preCannedFitnesses = {
				0.0,
				1.5
				};
		int[][] expectedNewGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1} 
				};
		testSelection(preCannedPopulation, preCannedFitnesses, expectedNewGenotypes);	
		
	}
	
	@Test
	public void testSelectionKeepMiddle() {		
		int[][] preCannedPopulation = {
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // Worse score possible
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Perfect
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // Worse score possible
				};
		double[] preCannedFitnesses = {
				0.0,
				1.5,
				0.0
				};
		int[][] expectedNewGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1} 
				};
		testSelection(preCannedPopulation, preCannedFitnesses, expectedNewGenotypes);	
		
	}
	
	@Test
	public void testSelectionNormal() {		
		int[][] preCannedPopulation = {
				{1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // One
				{1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Two
				{0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // Three
				{1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Four
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0}, // Five
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1}, // Six
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0}, // Seven
				};
		double[] preCannedFitnesses = {
				0.2,
				1.3,
				0.2,
				1.3,
				0.2,
				1.3,
				0.2
				};
		int[][] expectedNewGenotypes = {
				{1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Two
				{1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Two
				{1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Four
				{1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Four
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0}, // Five
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1}, // Six
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0}  // Seven
				};
		testSelection(preCannedPopulation, preCannedFitnesses, expectedNewGenotypes);	
		
	}
	
	public void testSelection(int[][] preCannedPopulation, double[] preCannedFitnesses, int[][] expectedNewGenotypes) {		
	
		GeneticAlgorithm ga = new GeneticAlgorithm();
		Population population = ga.getPopulation();
		population.initGenotypes(preCannedPopulation);
		
		List<Genotype> actualNewGenotypes = ga.selectionUsingStocasticUniversalSampling(population, preCannedFitnesses);
//		System.out.println("NDB::testSelection()~actualNewGenotypes="+toString(actualNewGenotypes));
		Assert.assertTrue(equals(expectedNewGenotypes, actualNewGenotypes));
		
	}
	
	@Test
	public void testNoCrossover() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.crossoverProbabillity = 0.0;
		int[][] intermediateGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Perfect
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // Worse score possible
				};
		testCrossover(params, intermediateGenotypes, intermediateGenotypes);	
		
	}

	@Test
	public void testOnePointCrossoverNormal() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.crossoverAlgorithm = GeneticAlgorithm.Parameters.CrossoverAlgorithm.OnePoint;	
		int[][] intermediateGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Perfect
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // Worse score possible
				};
		int[][] expectedGenotypes = {
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0}
				};
		testCrossover(params, intermediateGenotypes, expectedGenotypes);	
		
	}
	
	@Test
	public void testUniformCrossoverNormal() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.crossoverAlgorithm = GeneticAlgorithm.Parameters.CrossoverAlgorithm.Uniform;	
		int[][] intermediateGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Perfect
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}  // Worse score possible
				};
		int[][] expectedGenotypes = {
				{0,1,1,1,0,0,1,1,1,0,0,0,0,1,1,0,1,0,1,1,0,1,1,1,1,1,1,0,0,1},
				{1,0,0,0,1,1,0,0,0,1,1,1,1,0,0,1,0,1,0,0,1,0,0,0,0,0,0,1,1,0}
				};
		testCrossover(params, intermediateGenotypes, expectedGenotypes);	
		
	}
	
	@Test
	public void testUniformCrossoverBigPopulation() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.crossoverAlgorithm = GeneticAlgorithm.Parameters.CrossoverAlgorithm.Uniform;	
		int[][] intermediateGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},  
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} 
				};
		int[][] expectedGenotypes = {
				{0,1,1,1,0,0,1,1,1,0,0,0,0,1,1,0,1,0,1,1,0,1,1,1,1,1,1,0,0,1},
				{1,0,0,0,1,1,0,0,0,1,1,1,1,0,0,1,0,1,0,0,1,0,0,0,0,0,0,1,1,0},
				{1,0,0,0,0,0,0,1,1,1,1,0,0,1,0,1,1,1,0,1,1,0,0,1,1,1,1,0,1,1},
				{0,1,1,1,1,1,1,0,0,0,0,1,1,0,1,0,0,0,1,0,0,1,1,0,0,0,0,1,0,0},
				{1,0,0,1,1,1,1,0,0,0,0,0,1,1,1,1,0,0,1,0,0,0,0,1,0,0,1,1,0,1},
				{0,1,1,0,0,0,0,1,1,1,1,1,0,0,0,0,1,1,0,1,1,1,1,0,1,1,0,0,1,0},
				{1,0,1,0,1,1,0,0,1,0,0,1,1,1,0,0,1,0,0,0,1,0,0,1,0,0,1,1,1,1},
				{0,1,0,1,0,0,1,1,0,1,1,0,0,0,1,1,0,1,1,1,0,1,1,0,1,1,0,0,0,0},
				{0,1,1,1,0,1,1,1,1,1,0,1,0,0,1,1,0,1,0,0,1,0,1,1,1,0,0,0,1,0},
				{1,0,0,0,1,0,0,0,0,0,1,0,1,1,0,0,1,0,1,1,0,1,0,0,0,1,1,1,0,1},
				{0,0,1,0,0,1,1,0,1,0,0,1,0,1,1,1,0,1,1,0,1,0,0,1,0,0,0,0,1,1},
				{1,1,0,1,1,0,0,1,0,1,1,0,1,0,0,0,1,0,0,1,0,1,1,0,1,1,1,1,0,0}
				};
		testCrossover(params, intermediateGenotypes, expectedGenotypes);	
		
	}
	
	@Test
	public void testUniformCrossoverOddPopulationSize() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.crossoverAlgorithm = GeneticAlgorithm.Parameters.CrossoverAlgorithm.Uniform;	
		int[][] intermediateGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},  
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
				};
		int[][] expectedGenotypes = {
				{0,1,1,1,0,0,1,1,1,0,0,0,0,1,1,0,1,0,1,1,0,1,1,1,1,1,1,0,0,1},
				{1,0,0,0,1,1,0,0,0,1,1,1,1,0,0,1,0,1,0,0,1,0,0,0,0,0,0,1,1,0},
				{1,0,0,0,0,0,0,1,1,1,1,0,0,1,0,1,1,1,0,1,1,0,0,1,1,1,1,0,1,1},
				{0,1,1,1,1,1,1,0,0,0,0,1,1,0,1,0,0,0,1,0,0,1,1,0,0,0,0,1,0,0},
				{1,0,0,1,1,1,1,0,0,0,0,0,1,1,1,1,0,0,1,0,0,0,0,1,0,0,1,1,0,1},
				{0,1,1,0,0,0,0,1,1,1,1,1,0,0,0,0,1,1,0,1,1,1,1,0,1,1,0,0,1,0},
				{1,0,1,0,1,1,0,0,1,0,0,1,1,1,0,0,1,0,0,0,1,0,0,1,0,0,1,1,1,1},
				{0,1,0,1,0,0,1,1,0,1,1,0,0,0,1,1,0,1,1,1,0,1,1,0,1,1,0,0,0,0},
				{0,1,1,1,0,1,1,1,1,1,0,1,0,0,1,1,0,1,0,0,1,0,1,1,1,0,0,0,1,0},
				{1,0,0,0,1,0,0,0,0,0,1,0,1,1,0,0,1,0,1,1,0,1,0,0,0,1,1,1,0,1},
				{0,0,1,0,0,1,1,0,1,0,0,1,0,1,1,1,0,1,1,0,1,0,0,1,0,0,0,0,1,1},
				{1,1,0,1,1,0,0,1,0,1,1,0,1,0,0,0,1,0,0,1,0,1,1,0,1,1,1,1,0,0},
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}  // This one is the extra one so not crossed.
				};
		testCrossover(params, intermediateGenotypes, expectedGenotypes);	
		
	}
	
	@Test
	public void testUniformCrossoverTincyPopulationSize() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.crossoverAlgorithm = GeneticAlgorithm.Parameters.CrossoverAlgorithm.Uniform;	
		int[][] intermediateGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				};
		int[][] expectedGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // There's no one to cross with :(
				};
		testCrossover(params, intermediateGenotypes, expectedGenotypes);	
		
	}
	
	public void testCrossover(GeneticAlgorithm.Parameters params, int[][] intermediateGenotypes, int[][] expectedGenotypes) {			
		GeneticAlgorithm ga = new GeneticAlgorithm(params);		
		Population population = ga.getPopulation();
		population.initGenotypes(intermediateGenotypes);
		
		List<Genotype> actualCrossedGenotypes = ga.crossover(population.getGenotypes());

		System.out.println("NDB::testCrossover()~expectedGenotypes="+ToString.toString(expectedGenotypes));
		System.out.println("NDB::testCrossover()~actualCrossedGenotypes="+toString(actualCrossedGenotypes));
		Assert.assertTrue(equals(expectedGenotypes, actualCrossedGenotypes));
		
	}
	
	@Test
	public void testShuffle() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		int[][] selectedGenotypes = {
				{1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // One
				{1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Two
				{0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // Three
				{1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Four
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0}, // Five
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1}, // Six
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0}, // Seven
				};
		int[][] expectedShuffledGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1},
				{1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0},
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0},
				{0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
				{1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				{1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
				};
		testShuffle(params, selectedGenotypes, expectedShuffledGenotypes);	
		
	}
	
	@Test
	public void testNoShuffle() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.shuffleProbabillity = 0.0;
		int[][] selectedGenotypes = {
				{1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // One
				{1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Two
				{0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // Three
				{1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Four
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0}, // Five
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1}, // Six
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0}, // Seven
				};
		int[][] expectedShuffledGenotypes = {
				{1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // One
				{1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Two
				{0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, // Three
				{1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, // Four
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0}, // Five
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,1,1,1,1,1}, // Six
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,0,0}, // Seven
				};
		testShuffle(params, selectedGenotypes, expectedShuffledGenotypes);	
		
	}

	public void testShuffle(GeneticAlgorithm.Parameters params, int[][] selectedGenotypes, int[][] expectedShuffledGenotypes) {			
		GeneticAlgorithm ga = new GeneticAlgorithm(params);		
		Population population = ga.getPopulation();
		population.initGenotypes(selectedGenotypes);
		
		List<Genotype> actualShiuffledGenotypes = ga.shuffle(population.getGenotypes());
		
//		System.out.println("NDB::testCrossover()~actualShiuffledGenotypes="+toString(actualShiuffledGenotypes));
		Assert.assertTrue(equals(expectedShuffledGenotypes, actualShiuffledGenotypes));
		
	}
	
	@Test
	public void testMutationNoone() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.mutationProbabillity = 0.0;
		int[][] crossedGenotype = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				};
		int[][] expectedMutatedGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				};
		testMutation(params, crossedGenotype, expectedMutatedGenotypes);	
		
	}
	
	@Test
	public void testMutationCertain() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.mutationProbabillity = 1.0;
		int[][] crossedGenotype = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				};
		int[][] expectedMutatedGenotypes = {
				{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
				};
		testMutation(params, crossedGenotype, expectedMutatedGenotypes);	
		
	}
	
	@Test
	public void testMutationNormal() {		
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.mutationProbabillity = 0.1;
		int[][] crossedGenotype = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
				};
		int[][] expectedMutatedGenotypes = {
				{1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0}
				};
		testMutation(params, crossedGenotype, expectedMutatedGenotypes);	
		
	}
	
	public void testMutation(GeneticAlgorithm.Parameters params, int[][] crossedGenotype, int[][] expectedMutatedGenotypes) {			
		GeneticAlgorithm ga = new GeneticAlgorithm(params);		
		Population population = ga.getPopulation();
		population.initGenotypes(crossedGenotype);
		
		List<Genotype> actualMutatedGenotypes = ga.mutate(population.getGenotypes());
		
//		System.out.println("NDB::testMutation()~actualMutatedGenotypes="+toString(actualMutatedGenotypes));
		Assert.assertTrue(equals(expectedMutatedGenotypes, actualMutatedGenotypes));
		
	}
	
}
