package ie.nix.ga;

import java.util.Arrays;
import java.util.List;

//@RunWith(Suite.class)
//@SuiteClasses({UnitTests.class})
public class AllTests {
	
	protected boolean equals(int[][] expectedPopulation, Population population) {	
		return equals(expectedPopulation, population.getGenotypes());
	}	

	protected boolean equals(int[][] expectedPopulation, List<Genotype> genotypes) {
		int g = 0;
		boolean equals = true;
		while (equals && g < genotypes.size()) {
			equals = equals(expectedPopulation[g], genotypes.get(g));
			g++;
		}		
		return equals;
	}
	
	protected boolean equals(int[] expectedGenotype, Genotype genotype) {		
		return Arrays.equals(expectedGenotype, genotype.getChromosome());
	}
	
	protected String toString(Population population) {		
		return toString(population.getGenotypes());
	}	
	
	protected String toString(List<Genotype> genotypes) {		
		StringBuilder string = new StringBuilder("{\n");
		for (int g = 0; g < genotypes.size(); g++) {
			
			string.append(toString(genotypes.get(g)));
			
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.length()-2, string.length(),"\n};");
		return string.toString();
	}
	
	
	protected String toString(Genotype genotype) {		
		StringBuilder string = new StringBuilder("{");
		for (int g = 0; g < genotype.chromosome.length; g++) {
			string.append(genotype.chromosome[g]+",");
		}
		string.replace(string.length()-1, string.length(),"}");
		return string.toString();
	}

}
