package ie.nix.ga;

import ie.nix.util.Randomness;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SystemTests extends AllTests {

	@Before
	public void setUp() throws Exception {
		Randomness.setSeed(0);
	}
	

	@Test
	public void test2LWithBitAgent() {
		int[] targetChromosome = {1,1};
		testWithBitAgent(targetChromosome);
	}
	
	@Test
	public void test3LWithBitAgent() {
		int[] targetChromosome = {1,1,1};
		testWithBitAgent(targetChromosome);
	}
	
	@Test
	public void test5LWithBitAgent() {
		int[] targetChromosome = {1,1,1,1,1};
		testWithBitAgent(targetChromosome);
	}
	
	@Test
	public void test8LWithBitAgent() {
		int[] targetChromosome = {1,1,1,1,1,1,1,1};
		testWithBitAgent(targetChromosome);
	}
	
	@Test
	public void test13LWithBitAgent() {
		int[] targetChromosome = {1,1,1,1,1,1,1,1,1,1,1,1,1};
		testWithBitAgent(targetChromosome);
	}
	
	@Test
	public void test21LWithBitAgent() {
		int[] targetChromosome = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
		testWithBitAgent(targetChromosome);
	}
	
	@Test
	public void test34LWithBitAgent() {
		int[] targetChromosome = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
		testWithBitAgent(targetChromosome);
	}
	
	@Test
	public void testRandomWithBitAgent() {
		int[] targetChromosome = {1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0};
		testWithBitAgent(targetChromosome);
	}	
	
	public void testWithBitAgent(int[] targetChromosome) {
		// Set up the GA
		GeneticAlgorithm.Parameters params = new GeneticAlgorithm.Parameters();
		params.lenghtOfGenotype = targetChromosome.length;
		GeneticAlgorithm ga = new GeneticAlgorithm(params);
		Population population = ga.getPopulation();
		
		population.initRandomGenotypes((int)Math.pow(targetChromosome.length,2));
		System.out.println("NDB::testWithBitAgent()~Initial population="+toString(population.getGenotypes()));
		
		// Initialis the agents
		Genotype targetGenotype = new Genotype(targetChromosome);
		BitAgent.setTargetGenotype(targetGenotype);
		List<Agent> agents = new ArrayList<Agent>();
		for (int g = 0; g < population.getNumberOfGenotypes(); g++) {
			agents.add(new BitAgent());
		}
		ga.addAgents(agents);
		
		// Evolve the system
		int numberOfGenerations = (int)Math.pow(targetChromosome.length,2);
		ga.evolve(numberOfGenerations);		
		System.out.println("NDB::testWithBitAgent()~actualGenotypes="+toString(population.getGenotypes()));		
		
		BitAgent fittestAgent = (BitAgent)ga.getFittestAgent();
		Assert.assertArrayEquals(targetChromosome, fittestAgent.getGenotype().getChromosome());
	
	}
	
}
