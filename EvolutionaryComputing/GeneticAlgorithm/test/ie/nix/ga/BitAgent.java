package ie.nix.ga;

import java.util.List;

public class BitAgent implements Agent {

	static Genotype targetGenotype;
	
	public static void setTargetGenotype(Genotype targetGenotype) {
		BitAgent.targetGenotype = targetGenotype;
	}

	public static String[] toStringArray(List<Agent> agents) {
		String[] agentStrings = new String[agents.size()];
		for (int a = 0; a < agents.size(); a++) {
			agentStrings[a] = agents.get(a).toString();
		}
		return agentStrings;
	}

	Genotype genotype;

	public Genotype getGenotype() {
		return genotype;
	}

	public Genotype getTargetGenotype() {
		return targetGenotype;
	}

	@Override
	public void decode(Genotype genotype) {		
		this.genotype = genotype;
		
	}

	@Override
	public double evaluation() {
		int evaluation = 0;
		for (int g = 0; g < targetGenotype.chromosome.length; g++) {
			if (targetGenotype.chromosome[g] == genotype.chromosome[g]) {
				evaluation++;				
			}
		}
		return (double)evaluation;
	}

	@Override
	public String toString() {
		if (genotype == null) {
			return "BitAgent[NOT-ENCODED]";
		} else {
			return "BitAgent["+genotype+"]";
		}
	}

}
