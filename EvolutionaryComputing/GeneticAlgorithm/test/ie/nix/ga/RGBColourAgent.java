package ie.nix.ga;

import java.awt.Color;
import java.util.List;

public class RGBColourAgent implements Agent {

	static Color targetColour = Color.WHITE;
	
	public static void setTargetColour(Color targetColour) {
		RGBColourAgent.targetColour = targetColour;
	}

	public static String[] toStringArray(List<Agent> agents) {
		String[] agentStrings = new String[agents.size()];
		for (int a = 0; a < agents.size(); a++) {
			agentStrings[a] = agents.get(a).toString();
		}
		return agentStrings;
	}

	Color colour;

	public Color getColour() {
		return colour;
	}

	public Color getTargetColour() {
		return targetColour;
	}

	@Override
	public void decode(Genotype genotype) {		
		colour = new Color(
				Integer.parseInt(genotype.toString(0, 7), 2),
				Integer.parseInt(genotype.toString(8, 15), 2),
				Integer.parseInt(genotype.toString(16, 23), 2));		
//		System.out.println("NDB::RGBColourAgent.decode()~colour="+colour);
		
	}

	@Override
	public double evaluation() {		
		int evaluation = (Color.WHITE.getRed() 
				          + Color.WHITE.getBlue() 
				          + Color.WHITE.getGreen())
					   - (Math.abs(targetColour.getRed() - colour.getRed())
					      + Math.abs(targetColour.getBlue() - colour.getBlue())
					      + Math.abs(targetColour.getGreen() - colour.getGreen()));
//		System.out.println("NDB::"+this+".evaluation()~evaluation="+evaluation);

//		int evaluation = Color.WHITE.getRed() -Math.abs(targetColour.getRed() - colour.getRed());
		return (double)evaluation;
	}

	@Override
	public String toString() {
		if (colour == null) {
			return "RGBColourAgent[NOT-ENCODED]";
		} else {
			return "RGBColourAgent["+colour.getRed()+","+colour.getGreen()+","+colour.getBlue()+"]";
		}
	}

}
