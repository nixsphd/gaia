package ie.nix.ga;

import ie.nix.util.Randomness;

public class Genotype {

	int[] chromosome;
	
	public Genotype(int lenght) {
		chromosome = new int[lenght];
		initChromosome();
	}
	
	public Genotype(int[] chromosome) {
		this.chromosome = chromosome;
	}

	public Genotype(Genotype genotype) {
		chromosome = genotype.chromosome.clone();
	}
		
	public int getLenghtOfGenotype() {
		return chromosome.length;
	}

	public int[] getChromosome() {
		return chromosome;
	}
	
	protected void initChromosome() {
		for (int g = 0; g < chromosome.length; g++) {
			chromosome[g] = Randomness.nextBinary();
		}
	}

	@Override
	public String toString() {
		return toString(0, chromosome.length-1);
	}
	
	public String toString(int fromIndex, int toIndex) {
		StringBuilder string = new StringBuilder();
		for (int g = fromIndex; g <= toIndex; g++) {
			string.append(chromosome[g]);
		}
		return string.toString();
	}
	
}
