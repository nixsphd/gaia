package ie.nix.ga;

import java.util.ArrayList;
import java.util.List;


public class Population {

	private GenotypeFactory genotypeFactory;
	private List<Genotype> genotypes;
	
	protected Population(int lenghtOfGenotype) {
		genotypeFactory = new GenotypeFactory(lenghtOfGenotype);
		genotypes = new ArrayList<Genotype>();
	}
	
	protected Population(List<Genotype> genotypes) {
		setGenotypes(genotypes);
	}

	protected void initRandomGenotype() {
		addGenotype(genotypeFactory.createGenotype());
	}

	protected void initRandomGenotypes(int numberOfGenotypes) {
		addGenotypes(genotypeFactory.createGenotype(numberOfGenotypes));
	}
	
	protected void initGenotypes(int[][] preCannedPopulation) {
		setGenotypes(genotypeFactory.createGenotype(preCannedPopulation));
	}

	protected int getNumberOfGenotypes() {
		return genotypes.size();
	}

	protected Genotype getGenotype(int index) {
		return genotypes.get(index);
	}

	protected List<Genotype> getGenotypes() {
		return genotypes;
	}

	protected void addGenotype(Genotype genotype) {
		genotypes.add(genotype);
	}
	
	protected void addGenotypes(List<Genotype> genotypes) {
		this.genotypes.addAll(genotypes);
	}
	
	protected void setGenotypes(List<Genotype> genotypes) {
		this.genotypes = genotypes;
		
	}
}
