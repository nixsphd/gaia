package ie.nix.ga;

import ie.nix.util.DataFrame;
import ie.nix.util.Randomness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

public class GeneticAlgorithm {

	public static class Parameters {

		// Genotype
		public int lenghtOfGenotype = 30;

		// Fitness 
		public boolean fitnessScaling = true;
		
		// Selection
		public enum SelectionAlgorithm {
			StocasticUniversalSampling, // SUS
			// FitnessProportionateSelection, // FPS
			// RouletteWheel
		};
		public SelectionAlgorithm selectionAlgorithm = SelectionAlgorithm.StocasticUniversalSampling;

		// Shuffle
		public double shuffleProbabillity = 1.0;
		public enum ShuffleAlgorithm {
			FisherYates, // Fisher-Yates shuffle
		}
		public ShuffleAlgorithm shuffleAlgorithm = ShuffleAlgorithm.FisherYates;

		// Crossover
		public double crossoverProbabillity = 1.0;
		public enum CrossoverAlgorithm {
			Uniform,
			OnePoint,
			TwoPoint
		}
		public CrossoverAlgorithm crossoverAlgorithm = CrossoverAlgorithm.OnePoint;

		// Mutation
		public double mutationProbabillity = 0.001;

		public enum musationAlgorithm {
			Flipping,
			// RandomRegeneration
		}
	};

	private Parameters params = new Parameters();
	private Population population;
	private List<Agent> agents;
	private double[] fitnesses;

	public GeneticAlgorithm() {
		this(new Parameters());
	}

	public GeneticAlgorithm(Parameters params) {
		agents = new ArrayList<Agent>();
		population = new Population(params.lenghtOfGenotype);
		this.params = params;

	}

	public Parameters getParams() {
		return params;
	}

	public int getNumberOfAgents() {
		return agents.size();
	}

	public void addAgent(Agent newAgent) {
		agents.add(newAgent);
		if (population.getNumberOfGenotypes() < agents.size()) {
			population.initRandomGenotype();
		}
		Genotype genotype = population.getGenotype(agents.size() - 1);
		newAgent.decode(genotype);
	}

	public void addAgents(List<? extends Agent> newAgents) {
		agents.addAll(newAgents);
		if (population.getNumberOfGenotypes() < agents.size()) {
			population.initRandomGenotypes(agents.size()
					- population.getNumberOfGenotypes());
		}
		decodeAgents(newAgents);
	}

	public List<Agent> getAgents() {
		return agents;
	}

	public void evolve() {
		// Select the intermediate population of genotypes
		List<Genotype> selectedPopulation = selection();
	
		// Shuffle Ps
		List<Genotype> shuffeledPopulation = shuffle(selectedPopulation);
	
		// Cross-over Pc
		List<Genotype> crossedPopulation = crossover(shuffeledPopulation);
	
		// Mutation Pm
		List<Genotype> mutatedPopulation = mutate(crossedPopulation);
	
		// Inversion, requires Linkage support
		// TODO Support inversion and so linkage
	
		// Send the new genotypes to the agents
		getPopulation().setGenotypes(mutatedPopulation);
		
		// Fitness will no longer be valid
		setFitnesses(null);
		
		// Decode the agents
		decodeAgents(agents);
	
	}

	public void evolve(int numberOfGenerations) {
		while (numberOfGenerations > 0) {
			evolve();
			numberOfGenerations--;
		}
		
	}
	
	public Agent getFittestAgent() {
		
		double[] fitnesses = getFitnesses();
		
		// Find the highest.
		int fittestAgentIndex = 0;
		double highestFitness = fitnesses[fittestAgentIndex];
		for (int agentIndex = 1; agentIndex < getNumberOfAgents(); agentIndex++) {
			if (fitnesses[agentIndex] > highestFitness) {
				highestFitness = fitnesses[agentIndex];
				fittestAgentIndex = agentIndex;
			}
		}
		Agent fittestAgent = agents.get(fittestAgentIndex);
		return fittestAgent;
	}

	public double getAvergarFitness() {
		double[] fitnesses = getFitnesses();
		double averageFitness = 0;
		for (int a = 1; a < getNumberOfAgents(); a++) {
			averageFitness += fitnesses[a];
		}
		averageFitness /= getNumberOfAgents();
		return averageFitness;
	}

	public void addGenomesToDataFrame(DataFrame gdf, String columnName) {
		List<Object> chromosomes = new ArrayList<Object>();
		Population population = getPopulation();
		for (int g = 0; g < population.getNumberOfGenotypes(); g++) {
			chromosomes.addAll(Arrays.asList(ArrayUtils.toObject(
					population.getGenotype(g).getChromosome())));
		}
		gdf.addColumn(columnName, chromosomes);
	
	}

	protected List<Genotype> shuffle(List<Genotype> selectedPopulation) {
		List<Genotype> shuffledPopulation = shuffleUsingFisherYates(selectedPopulation);
		return shuffledPopulation;
	}
	
	protected List<Genotype> shuffleUsingFisherYates(List<Genotype> selectedPopulation) {
		for (int g = selectedPopulation.size() - 1; g > 0; g--) {
			if (params.shuffleProbabillity > Randomness.nextDouble()) {				
				// This should be g-1 according to stsckoverflow
				// (http://stackoverflow.com/questions/1519736/random-shuffling-of-an-array)
				// which means the a chance that the genotype shuffles with itself.
				// I decided I didn't like this :)
				int index = Randomness.nextInt(g);
				// Simple swap
				Genotype tmp = selectedPopulation.get(index);
				selectedPopulation.set(index, selectedPopulation.get(g));
				selectedPopulation.set(g, tmp);
			}
		}
		return selectedPopulation;
	}

	protected List<Genotype> selection() {
		// Calculate their fitness
		double[] fitnesses = getFitnesses();

		// Sample intermediate population based on fitness using Stochastic
		// Universal Sampling
		return selectionUsingStocasticUniversalSampling(population, fitnesses);

	}

	protected double[] getFitnesses() {
		if (fitnesses == null) {
			setFitnesses(calculateFitnesses());
		}
		return fitnesses;
	}

	protected void setFitnesses(double[] fitnesses) {
		this.fitnesses = fitnesses;
	}

	protected double[] calculateFitnesses() {
		// Evaluate all agents
		double[] evaluations = new double[getNumberOfAgents()];
		double averageEvalutaion = 0;
		double worstEvaluation = Double.MAX_VALUE;
		int agentIndex = 0;
		for (Agent agent : getAgents()) {
			evaluations[agentIndex] = agent.evaluation();
			averageEvalutaion += evaluations[agentIndex];
			// Scale the fitness, if requested
			if (params.fitnessScaling && worstEvaluation > evaluations[agentIndex]) {
				worstEvaluation = evaluations[agentIndex];
			}	
			agentIndex++;
		}
		// Get average evaluation
		averageEvalutaion /= getNumberOfAgents();

		// Get the worst fitness
		double worstFitness = worstEvaluation / averageEvalutaion;	
		
		// Calculate their fitness
		double[] fitnesses = new double[getNumberOfAgents()];
		for (agentIndex = 0; agentIndex < getNumberOfAgents(); agentIndex++) {
			// Scale the fitness, if requested
			if (params.fitnessScaling) {
				fitnesses[agentIndex] = (evaluations[agentIndex] / averageEvalutaion) - worstFitness;
			} else {
				fitnesses[agentIndex] = evaluations[agentIndex] / averageEvalutaion;
			}
		}

//		for (agentIndex = 0; agentIndex < getNumberOfAgents(); agentIndex++) {
//			System.out.println("NDB::calculateFitnesses()~evaluation "+agents.get(agentIndex)+"="+evaluations[agentIndex]);
//			System.out.println("NDB::calculateFitnesses()~fitness "+agents.get(agentIndex)+"="+fitnesses[agentIndex]);
//		}
		return fitnesses;
	}

	protected List<Genotype> selectionUsingStocasticUniversalSampling(Population population, double[] fitnesses) {

		// Set the number of agents to keep the number of agents we start with
		int numberToSelect = population.getNumberOfGenotypes();

		// The intermediate population
		ArrayList<Genotype> selectedGenotypes = new ArrayList<Genotype>();

		// Calculate the accumulated fitnesses.
		double[] accumulatedFitnesses = new double[population
				.getNumberOfGenotypes()];
		accumulatedFitnesses[0] = fitnesses[0];
		for (int agentIndex = 1; agentIndex < population.getNumberOfGenotypes(); agentIndex++) {
			accumulatedFitnesses[agentIndex] = accumulatedFitnesses[agentIndex - 1]
					+ fitnesses[agentIndex];
		}

		// Calculate the total fitness of population
		double totalFitness = accumulatedFitnesses[population
				.getNumberOfGenotypes() - 1];

		// Calculate the distance between the pointers
		double distanceBetweenPointers = totalFitness / numberToSelect;

		// Select the start, using a ramsomnumber between 0 and the distance
		// between the pointers
		double start = Randomness.nextDouble(0.0, distanceBetweenPointers);

		// For each of the new agents
		int oldAgentIndex = 0;
		for (int newAgentIndex = 0; newAgentIndex < numberToSelect; newAgentIndex++) {
			// Set the pointer
			double pointer = start + (newAgentIndex * distanceBetweenPointers);
			// System.out.println("NDB::selectionUsingStocasticUniversalSampling()~Pointer at "+pointer);
			while (accumulatedFitnesses[oldAgentIndex] < pointer) {
				// System.out.println("NDB::selectionUsingStocasticUniversalSampling()~Not selecting "+oldAgentIndex+" with Max acc fitness "+accumulatedFitnesses[oldAgentIndex]);
				oldAgentIndex++;
			}
			// Keep oldAgentIndex
			// System.out.println("NDB::selectionUsingStocasticUniversalSampling()~Selected "+oldAgentIndex+" with Max acc fitness "+accumulatedFitnesses[oldAgentIndex]);
			selectedGenotypes.add(new Genotype(population.getGenotype(oldAgentIndex)));
		}
		return selectedGenotypes;
	}

	protected List<Genotype> crossover(List<Genotype> intermediatePopulation) {
		
		for (int g = 1; g < intermediatePopulation.size(); g = g + 2) {
			if (params.crossoverProbabillity > Randomness.nextDouble()) {
				// Crossover
				switch (params.crossoverAlgorithm) {
					case Uniform:
						uniformCorssover(intermediatePopulation.get(g - 1), intermediatePopulation.get(g));
						break;
					case OnePoint:
						onePointCorssover(intermediatePopulation.get(g - 1), intermediatePopulation.get(g));
						break;
					case TwoPoint:
						twoPointCorssover(intermediatePopulation.get(g - 1), intermediatePopulation.get(g));
						break;
				}
				
			}
		}
		return intermediatePopulation;

	}

	protected void twoPointCorssover(Genotype genotypeA, Genotype genotypeB) {
		// TODO Auto-generated method stub
		
	}

	protected void onePointCorssover(Genotype genotypeA, Genotype genotypeB) {
		int crossoverPoint = Randomness.nextInt(genotypeA.getLenghtOfGenotype());	
//		System.out.println("NDB::onePointCorssover()~Cross at "+crossoverPoint);
		for (int g = 0; g < genotypeA.chromosome.length; g++) {
			if (g <= crossoverPoint) {
				// Swap
				int temp = genotypeA.chromosome[g];
				genotypeA.chromosome[g] = genotypeB.chromosome[g];
				genotypeB.chromosome[g] = temp;

			}
		}		
	}

	protected void uniformCorssover(Genotype genotypeA, Genotype genotypeB) {
		for (int g = 0; g < genotypeA.chromosome.length; g++) {
			if (0.5 > Randomness.nextDouble()) {
				// Swap
				int temp = genotypeA.chromosome[g];
				genotypeA.chromosome[g] = genotypeB.chromosome[g];
				genotypeB.chromosome[g] = temp;

			}
		}
	}

	protected List<Genotype> mutate(List<Genotype> crossedPopulation) {
		List<Genotype> mutatedPopulation = mutateByFlipping(crossedPopulation);
		return mutatedPopulation;

	}

	protected List<Genotype> mutateByFlipping(List<Genotype> crossedPopulation) {
		for (Genotype genotype : crossedPopulation) {
			for (int g = 0; g < genotype.chromosome.length; g++) {
				if (params.mutationProbabillity > Randomness.nextDouble()) {
					// Mutate
					genotype.chromosome[g] = (genotype.chromosome[g] + 1) % 2;
//					System.out.println("NDB::mutateByFlipping()~Flipping "+genotype+" at "+g);
				}
			}
		}
		return crossedPopulation;
	}

	protected void setParams(Parameters params) {
		this.params = params;
	}

	protected void setPopulation(Population population) {
		this.population = population;
	}

	protected Population getPopulation() {
		return population;
	}

	protected void setAgents(List<Agent> agents) {
		this.agents.clear();
		addAgents(agents);
	}


	protected void decodeAgents(List<? extends Agent> agents) {
		for (Agent agent : agents) {
			Genotype genotype = population.getGenotype(agents.indexOf(agent));
			agent.decode(genotype);
		}
	}

	// TODO Should delete this if I didn't use it.
	// RGBColourAgent[] agents = new RGBColourAgent[numberOfAgents];
	// for (int a = 0; a < agents.length; a++) {
	// RGBColourAgent newAgent = new RGBColourAgent();
	// newAgent.decode(population.getGenotypes().get(a));
	// System.out.println("NDB::test()~newAgent="+newAgent);
	// }

}
