package ie.nix.ga;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenotypeFactory {

	private int lenghtOfGenotype;
	
	public GenotypeFactory(int lenght) {
		this.lenghtOfGenotype = lenght;
	}
	
	public Genotype createGenotype() {
		return new Genotype(lenghtOfGenotype);
    }
	
	public List<Genotype> createGenotype(int numberOfGenotypes) {
		Genotype[] genotypes = new Genotype[numberOfGenotypes];
		
		// Initialise all the genotyoess randomly
		for (int n = 0; n < numberOfGenotypes; n++) {
			genotypes[n] = new Genotype(lenghtOfGenotype);
		}
		
		return new ArrayList<Genotype>(Arrays.asList(genotypes));
    }

	public List<Genotype> createGenotype(int[][] preCannedPopulation) {
		Genotype[] genotypes = new Genotype[preCannedPopulation.length];
		
		// Initialise all the genotyoess based on the passed pre canned values
		for (int n = 0; n < preCannedPopulation.length; n++) {
			genotypes[n] = new Genotype(preCannedPopulation[n]);
		}
		
		return new ArrayList<Genotype>(Arrays.asList(genotypes));
	}
}
