package ie.nix.ga;

/**
 * @author Nicola Mc Donnell
 *
 */
public interface Agent {
	
	/**
	 * @param genotype - the genotype to be decoded by the Agent.
	 */
	public void decode(Genotype genotype);
	
	/**
	 * @return a positive number greater than zero which represents the evalutaion. The higher the number the better the evaluation
	 */
	public double evaluation();

}
