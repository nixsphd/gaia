package ie.nix.ipd;

import ie.nix.pd.PrisonersDilemma;

public class IteratedPrisonersDilemma extends PrisonersDilemma {

	public interface Player {
		
		public PrisonersDilemma.Play play(Play opponentsLastPlay);
		
		public void playScore(double score);
		
	}
	
	public static IteratedPrisonersDilemma AxelrodsTournaments = new IteratedPrisonersDilemma(3,0,5,1);
	
	// Always cooperates
	public static Strategy Cooperator = new Strategy(1.0, 1.0, 1.0);
	// Always defects
	public static Strategy Defector = new Strategy(0.0, 0.0, 0.0);
	// Cooperates on the first round and then mimics after that
	public static Strategy TitForTat = new Strategy(1.0, 1.0, 0.0);
	// Cooperates on the first round, then mimics, but is more likely 
	// to cooperate after an opponents defection. 
	public static Strategy GenerousTitForTat = new Strategy(1.0, 1.0, 1.0/3);

	public static class Strategy {

		// The probability of cooperating on the first round
		protected double i;
		// Probability of cooperation if the opponents last play was cooperation
		protected double p;
		// Probability of cooperation if the opponents last play was defection
		protected double q;
		
		public Strategy(double i, double p, double q) {
			this.i = i;
			this.p = p;
			this.q = q;
		}
		
		public PrisonersDilemma.Play play(Play opponentsLastPlay) {
			if (opponentsLastPlay == null) {
				if (i > 0.5) {
					return Play.Cooperate;					
				} else {
					return Play.Defect;					
				}	
			} else if (opponentsLastPlay == Play.Cooperate) {
				if (p > 0.5) {
					return Play.Cooperate;					
				} else {
					return Play.Defect;					
				}
			} else if (opponentsLastPlay == Play.Defect) {
				if (q > 0.5) {
					return Play.Cooperate;					
				} else {
					return Play.Defect;					
				}
			} else {
				return null;
			}
		}
		
	}
	
	public IteratedPrisonersDilemma(double reward, double sucker, double temptation, double punishment) {
		super(reward, sucker, temptation, punishment);
	}

	public void play(Player playerA, Player playerB, int numberOfGames) {
		
		// The last plays are null for the first round
		Play lastPlayA = null;
		Play lastPlayB = null;
		
		double playerAScore = 0;
		double playerBScore = 0;
		
		while (numberOfGames > 0) {
			// Play a game
			
			Play playA = playerA.play(lastPlayB);
			Play playB = playerB.play(lastPlayA);
			
			switch (playA) {
				case Cooperate:		
					switch (playB) {
						case Cooperate:
							playerAScore += reward;
							playerBScore += reward;
							break;
						case Defect:
							playerAScore += sucker;
							playerBScore += temptation;
							break;
					}
					break;
				case Defect:
					switch (playB) {
						case Cooperate:		
							playerAScore += temptation;
							playerBScore += sucker;		
							break;
						case Defect:
							playerAScore += punishment;
							playerBScore += punishment;
							break;
					}
					break;
			}	
			

			// Set up the last plays for the next round
			lastPlayA = playA;
			lastPlayB = playB;
			
			numberOfGames--;
		}

		
		playerA.playScore(playerAScore);
		playerB.playScore(playerBScore);
	}
	
//	public void play(Player playerA, List<Player> playerBs) {
//		for (Player playerB : playerBs) {
//			play(playerA, playerB);			
//		}		
//	}
	
}
