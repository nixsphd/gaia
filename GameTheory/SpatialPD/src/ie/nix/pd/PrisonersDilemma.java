package ie.nix.pd;

public class PrisonersDilemma {
	
	public interface Player {
		
		public PrisonersDilemma.Play play();
		
		public void playScore(double score);
		
	}
	
	public static PrisonersDilemma AxelrodsTournaments = new PrisonersDilemma(3,0,5,1);
	
	public static Strategy Cooperator = new Strategy(1.0);
	public static Strategy Defector = new Strategy(0.0);

	public static class Strategy {
		// The probability of cooperating
		protected double i = 0.9;
	
		public Strategy(double i) {
			this.i = i;
		}
		
		public PrisonersDilemma.Play play() {
			if (i > 0.5) {
				return Play.Cooperate;
				
			} else {
				return Play.Defect;
				
			}			
		}
		
	}

	protected double reward;
	protected double sucker;
	protected double temptation;	
	protected double punishment;
	
	public PrisonersDilemma(double reward, double sucker, double temptation, double punishment) {
		this.reward = reward;
		this.sucker = sucker;
		this.temptation = temptation;
		this.punishment = punishment;
	}

	public enum Play {
		Cooperate, Defect
	}

	public void play(Player playerA, Player playerB) {		
		Play playA = playerA.play();
		Play playB = playerB.play();
		
		switch (playA) {
			case Cooperate:		
				switch (playB) {
					case Cooperate:
						playerA.playScore(reward);
						playerB.playScore(reward);
						break;
					case Defect:
						playerA.playScore(sucker);
						playerB.playScore(temptation);
						break;
				}
				break;
			case Defect:
				switch (playB) {
					case Cooperate:		
						playerA.playScore(temptation);
						playerB.playScore(sucker);		
						break;
					case Defect:
						playerA.playScore(punishment);
						playerB.playScore(punishment);
						break;
				}
				break;
		}
		
	}
	
}
