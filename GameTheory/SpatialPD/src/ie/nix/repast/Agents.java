package ie.nix.repast;

import repast.simphony.context.Context;
import repast.simphony.context.space.grid.GridFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.grid.MooreQuery;
import repast.simphony.query.space.grid.VNQuery;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridAdder;
import repast.simphony.space.grid.GridBuilderParameters;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.space.grid.GridPointTranslator;
import repast.simphony.space.grid.RandomGridAdder;
import repast.simphony.space.grid.StrictBorders;

// TODO Make A extend Agent, not just Agent
public abstract class Agents implements ContextBuilder<Agent> {
	
	private Context<Agent> context;
	private Grid<Agent> grid;
	private String gridName = "Grid";
	private GridPointTranslator gridPointTranslator = new StrictBorders();
	private GridAdder<Agent> gridAdder = new RandomGridAdder<Agent>();
	private boolean multi = false; 
	private int gridWidth;
	private int gridHeight;

	@Override
	public Context<Agent> build(Context<Agent> context) {
		
		init();

		Parameters parms = RunEnvironment.getInstance().getParameters();
		if (parms.getValue("GridSize") != null) {
			int gridSize = ((Integer)parms.getValue("GridSize")).intValue();
			return build(context, gridSize, gridSize);
			
		} else if ((parms.getValue("GridWidth") != null) && 
				   (parms.getValue("GridHeight") != null)) {
			int gridWidth = ((Integer)parms.getValue("GridWidth")).intValue();
			int gridHeight = ((Integer)parms.getValue("GridHeight")).intValue();
			return build(context, gridWidth, gridHeight);	
			
		} else {
			return build(context, gridWidth, gridHeight);	
			
		}
		
	}

	public Context<Agent> build(Context<Agent> context, int gridWidth, int gridHeight) {
		this.context = context;		
		this.gridWidth = gridWidth;
		this.gridHeight = gridHeight;

		// Create the repast stuff
		initGrid();
	
		// Create the agents
		initAgents();
		
		return context;
	}

	public Context<Agent> build(Context<Agent> context, char[][] agents) {
		this.context = context;		
		this.gridWidth = agents[0].length;
		this.gridHeight = agents.length;

		// Create the repast stuff
		initGrid();
	
		// Create the agents
		initAgents(agents);
		
		return context;
	}

	public String getGridName() {
		return gridName;
	}

	public void setGridName(String gridName) {
		this.gridName = gridName;
	}

	public GridPointTranslator getGridPointTranslator() {
		return gridPointTranslator;
	}

	public void setGridPointTranslator(GridPointTranslator gridPointTranslator) {
		this.gridPointTranslator = gridPointTranslator;
	}

	public GridAdder<Agent> getGridAdder() {
		return gridAdder;
	}

	public void setGridAdder(GridAdder<Agent> gridAdder) {
		this.gridAdder = gridAdder;
	}

	public boolean isMulti() {
		return multi;
	}

	public void setMulti(boolean multi) {
		this.multi = multi;
	}
	
	public Context<Agent> getContext() {
		return context;
	}

	public Grid<Agent> getGrid() {
		return grid;
	}

	public Integer getGridWidth() {
		return gridWidth;
	}

	public Integer getGridHeight() {
		return gridHeight;
	}

	public Iterable<Agent> getListOfAgents() {
		return grid.getObjects();
	}

	public Agent getAgent(GridPoint location) {
		return grid.getObjectAt(location.getX(), location.getY());
	}

	public Agent getAgent(int x, int y) {
		return grid.getObjectAt(x, y);
	}

	public Iterable<Agent> getVNNeighbours(Agent agent, int... extent) {
		// Query the von Neuman neighborhood
		VNQuery<Agent> query = new VNQuery<Agent>(grid, agent, extent);
		return query.query();
	}
	
	public Iterable<Agent> getMooreNeighbours(Agent agent, int... extent) {
		// Query the von Neuman neighborhood
		MooreQuery<Agent> query = new MooreQuery<Agent>(grid, agent, extent);
		return query.query();
	}

	protected void initGrid() {
		this.grid = GridFactoryFinder.createGridFactory(null).createGrid(gridName, context, 
				new GridBuilderParameters<Agent>(gridPointTranslator, gridAdder, multi, 
						getGridWidth(), getGridHeight()));
	}

	protected abstract void init();
	
	protected abstract void initAgents();

	protected abstract Agent initAgent(char agentChar);

	protected void initAgents(char[][] agents) {	
		for (int y = 0; y < agents.length; y++) {
			for (int x = 0; x < agents[y].length; x++) {
				// The grid is inverted in the y direction in Repast, 
				// so this inversion keeps everything right.
				Agent newAgent = initAgent(agents[agents.length-1-y][x]);
				if (newAgent != null) {
					addAgent(newAgent,new GridPoint(x,y));
				}
			}
		}
		
	}

	protected void addAgent(Agent newAgent) {
		if (!context.add(newAgent)) {
			throw new RuntimeException("Probelm adding "+newAgent+"");
		}
		newAgent.moveTo(grid.getLocation(newAgent));
	}
	
	protected void addAgent(Agent newAgent, GridPoint location) {
		if (!context.add(newAgent)) {
			throw new RuntimeException("Probelm adding "+newAgent+"");
		}
		newAgent.moveTo(location);
	}
	
	public Agent[][] getAgents() {
		Agent[][] agents = new Agent[getGridHeight()][getGridWidth()];
		for (int y = 0; y < agents.length; y++) {
			for (int x = 0; x < agents[y].length; x++) {
				Agent agent = getAgent(x,y);
				agents[agents.length-1-y][x] = agent;
			}
		}
		return agents;		
	}
	
	public char[][] getState() {
		char[][] state = new char[getGridHeight()][getGridWidth()];
		for (int y = 0; y < state.length; y++) {
			for (int x = 0; x < state[y].length; x++) {
				Agent agent = getAgent(x,y);
				if (agent == null) {
					// The grid is inverted in the y direction in Repast, 
					// so this inversion keeps everything right.
					state[state.length-1-y][x] = ' ';					
				} else {
					// The grid is inverted in the y direction in Repast, 
					// so this inversion keeps everything right.
					state[state.length-1-y][x]  = agent.getChar();
				}
			}
		}
		return state;		
	}
	
}
