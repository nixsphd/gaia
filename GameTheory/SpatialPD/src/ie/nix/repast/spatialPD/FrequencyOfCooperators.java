package ie.nix.repast.spatialPD;

import ie.nix.pd.PrisonersDilemma;

import repast.simphony.data2.AggregateDataSource;

public class FrequencyOfCooperators implements AggregateDataSource {

	@Override
	public String getId() {
		return "Frequency of Cooperators";
	}

	@Override
	public Class<?> getDataType() {
		return double.class;
	}

	@Override
	public Class<?> getSourceType() {
		return PDAgent.class;
	}

	@Override
	public Object get(Iterable<?> objs, int size) {
		int numberOfCooperators = 0;		
		for (Object object : objs) {
			PDAgent agent = (PDAgent)object;
			if (agent.getStrategy() == PrisonersDilemma.Cooperator) {
				numberOfCooperators++;
			}
		}
		double frequencyOfCooperators = (double)(numberOfCooperators)/size;			
		return frequencyOfCooperators;
	}

	@Override
	public void reset() {}

}
