package ie.nix.repast.spatialPD;

import repast.simphony.data2.NonAggregateDataSource;

public class ConvergedAt implements NonAggregateDataSource {

	@Override
	public String getId() {
		return "Converged At";
	}

	@Override
	public Class<?> getDataType() {
		return double.class;
	}

	@Override
	public Class<?> getSourceType() {
		return ConvergenceChecker.class;
	}

	@Override
	public Object get(Object object) {
		ConvergenceChecker convergenceChecker = (ConvergenceChecker)object;
		return convergenceChecker.getConvergenceAt();
	}
}
