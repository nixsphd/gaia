package ie.nix.repast.spatialPD;

import ie.nix.pd.PrisonersDilemma;

import java.awt.Color;

import repast.simphony.visualizationOGL2D.DefaultStyleOGL2D;
import saf.v3d.scene.VSpatial;

public class PDStyle extends DefaultStyleOGL2D {
	
	/**
	 * @return a circle of radius 4.
	 */
	public VSpatial getVSpatial(Object agent, VSpatial spatial) {
	  if (spatial == null) {
	    spatial = shapeFactory.createRectangle(13, 13);
	  }
	  return spatial;
	}

	/**
	 * @return Color.BLUE.
	 */
	public Color getColor(Object agent) {
		PDAgent pdAgent = (PDAgent) agent;
		if (pdAgent.getStrategy() == PrisonersDilemma.Cooperator) {
			if (pdAgent.evolving()) {
				return Color.GREEN;
			} else {
				return Color.BLUE;				
			}
		} else {
			if (pdAgent.evolving()) {
				return Color.YELLOW;
			} else {
				return Color.RED;		
			}
			
		}
	}
}