package ie.nix.repast.spatialPD;

import repast.simphony.context.Context;
import ie.nix.repast.Agent;
import ie.nix.repast.Agents;
import ie.nix.repast.Model;

public class SpatialPDModel extends Model {
	
	public SpatialPDModel() {
		super(new SpatialPDAgents());
	}
	
	public SpatialPDModel(Agents agents) {
		super(agents);
	}

	@Override
	protected void initContexts(Context<Object> context, Context<Agent> agentContext) {
		super.initContexts(context, agentContext);
		
		// Initialise the ExitTester
		ConvergenceChecker exitTester = new ConvergenceChecker(this);
		getContext().add(exitTester);
		
	}
			
}
