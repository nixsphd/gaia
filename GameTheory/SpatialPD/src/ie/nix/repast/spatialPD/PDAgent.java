package ie.nix.repast.spatialPD;

import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import ie.nix.repast.Agent;
import ie.nix.repast.Agents;
import ie.nix.pd.PrisonersDilemma;
import ie.nix.pd.PrisonersDilemma.Play;
import ie.nix.pd.PrisonersDilemma.Strategy;

public class PDAgent extends Agent implements PrisonersDilemma.Player {
	
	protected PrisonersDilemma pd;
	protected Strategy strategy;
	protected double playScore;
	
	protected boolean playSelf;
	protected Strategy newStrategy;
	
	public PDAgent(Agents agents, char agentChar, Strategy strategy, boolean playSelf) {
		super(agents, agentChar);
		
		// Set-up the PD for the player
		pd = ((SpatialPDAgents)agents).getPd();
		this.strategy = strategy;
		
		// Tell the agent whether to play itself in the PD games.
		this.playSelf = playSelf;
		
		// Initialise this to null so we say we haven't evolved yet
		this.newStrategy = null;
	}
	
	@ScheduledMethod(start=1, interval=1, priority=ScheduleParameters.FIRST_PRIORITY)
	public void compete() {
//		System.out.println("NDB::PDAgent "+this+".compete()");		
		
		if (playSelf) {
//			System.out.println("NDB::PDAgent.compete()~Playing self");
			pd.play(this, this);
		}
		// Get the neighbors;
		for (Agent agent : getModel().getMooreNeighbours(this, 1, 1)) {
			// Play each agent
			PDAgent otherPlayer = (PDAgent)agent;
//			System.out.println("NDB::PDAgent.compete()~"+this+" versus "+otherPlayer);
			pd.play(this, otherPlayer);
		}
		
	}
	
	@ScheduledMethod(start=1, interval=1)
	public void select() {
//		System.out.println("NDB::PDAgent "+this+".select()");
		double bestScore = getScore();
		Strategy bestStrategy = getStrategy();	
		for (Agent agent : getModel().getMooreNeighbours(this, 1, 1)) {
			// Play each agent
			PDAgent otherPlayer = (PDAgent)agent;
			double otherPlayerScore = otherPlayer.getScore();
//			System.out.println("NDB::PDAgent.evolve()~otherPlayerScore for "+otherPlayer+" is "+otherPlayerScore);
			if (otherPlayerScore > bestScore) {
				bestScore = otherPlayerScore;
				bestStrategy = otherPlayer.getStrategy();	
			}
		}
		if (getStrategy() != bestStrategy) {	
			newStrategy = bestStrategy;
//			System.out.println("NDB::PDAgent "+this+".select()~Will evolve");
		} else {
			newStrategy = null;
//			System.out.println("NDB::PDAgent "+this+".select()~Won't evolve");
		}

	}
	
	@ScheduledMethod(start=1, interval=1, priority=ScheduleParameters.LAST_PRIORITY)
	public void evolve() {
//		System.out.println("NDB::PDAgent "+this+".evolve()");
		if (evolving()) {			
			setStrategy(newStrategy);
//			System.out.println("NDB::PDAgent "+this+".select()~Evolved");
		}

		// Reset scores
		this.playScore = 0.0;
	}
	
	public Play play() {
		return strategy.play();
	}

	public void playScore(double playScore) {
	//		System.out.println("NDB::PDAgent.playScore()~"+this+"playScore "+this.playScore+" updated by "+playScore);
			this.playScore += playScore;
		}

	public boolean evolving() {
		return (newStrategy != null);
	}

	public double getScore() {
		return playScore;
	}

	public Strategy getStrategy() {
		return strategy;
	}

	protected void setStrategy(Strategy strategy) {
		this.strategy = strategy;
		if (strategy == PrisonersDilemma.Cooperator) {
			setAgentChar('c');
		} else {
			setAgentChar('d');			
		}
		
	}
	
}
