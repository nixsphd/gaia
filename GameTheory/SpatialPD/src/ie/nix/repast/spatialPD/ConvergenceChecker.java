package ie.nix.repast.spatialPD;

import ie.nix.pd.PrisonersDilemma;
import ie.nix.repast.Agent;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;

public class ConvergenceChecker {

	private SpatialPDModel model;
	private double convergenceAt = -1;
	
	public ConvergenceChecker(SpatialPDModel model) {
		this.model = model;
	}
	
	@ScheduledMethod(start=1, interval=1, priority=ScheduleParameters.FIRST_PRIORITY)
	public boolean converged() {		
		boolean allD = true;			
		boolean allC = true;			
		for (Agent agent : model.getAgents().getListOfAgents()) {
			PDAgent pdAgent = (PDAgent)agent;
			if (pdAgent.getStrategy() == PrisonersDilemma.Cooperator) {
				allD = false;
			} else {
				allC = false;
			}
		}	
		if (allD || allC) {
			convergenceAt = RunEnvironment.getInstance().getCurrentSchedule().getTickCount();
			RunEnvironment.getInstance().endRun();	
			return true;
		} else {
			return false;
		}
		
		
	}

	public double getConvergenceAt() {
		return convergenceAt;
	}

}