package ie.nix.repast.spatialPD;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.random.RandomHelper;
import repast.simphony.space.grid.WrapAroundBorders;
import repast.simphony.space.grid.GridPoint;
import ie.nix.pd.PrisonersDilemma;
import ie.nix.repast.Agent;
import ie.nix.repast.Agents;

public class SpatialPDAgents extends Agents {
	
	protected enum InitialState {
		 Random,
		 InvadingDefector,
		 InvadingCooperators
	} InitialState initialState;
	
	protected PrisonersDilemma pd;
	protected boolean playSelf;

	public Context<Agent> build(Context<Agent> context, int gridWidth, int gridHeight, int numberOfSteps,
			InitialState initialState,  double defectorPayoff, boolean playSelf) {
		init(initialState, defectorPayoff, playSelf);		
		return build(context, gridWidth, gridHeight);
		
	}

	public Context<Agent> build(Context<Agent> context, char[][] initialAgents, double defectorPayoff, boolean playSelf) {
		init(defectorPayoff, playSelf);
		return build(context, initialAgents);
	}

	public InitialState getInitialState() {
		return initialState;
	}

	public PrisonersDilemma getPd() {
		return pd;
	}

	@Override
	protected void init() {
		Parameters params = RunEnvironment.getInstance().getParameters();
		InitialState initialState = getInitialState((String)(params.getValue("InitialState")));
		double defectorPayoff = ((Double)params.getValue("DefectorPayoff")).doubleValue();
		boolean playSelf = ((Boolean)params.getValue("PlaySelf")).booleanValue();
		
		init(initialState, defectorPayoff, playSelf);		
	}
	
	protected void init(InitialState initialState, double defectorPayoff, boolean playSelf) {
		// Initialise the state
		this.initialState = initialState; 
		
		init(defectorPayoff, playSelf);
	}
	
	protected void init(double defectorPayoff, boolean playSelf) {
		// Initialise the Prisoners Dilemma
		pd = new PrisonersDilemma(1, 0, defectorPayoff, 0);
		
		// Initialise repast
		setGridPointTranslator(new WrapAroundBorders());
		
		// Initialise playSelf
		this.playSelf = playSelf;
	}

	@Override
	protected Agent initAgent(char agentChar) {
		if ('c' == agentChar) {
			return new PDAgent(this, 'c',PrisonersDilemma.Cooperator, playSelf);
		} else if ('d' == agentChar) {
			return new PDAgent(this, 'd', PrisonersDilemma.Defector, playSelf);
		}
		return null;
	}

	@Override
	protected void initAgents() {
		switch (initialState) {
			case Random:		
				initRandom();
				break;		
			case InvadingDefector:		
				initInvadingDefector();
				break;
			case InvadingCooperators:		
				initInvadingCooperators();
				break;	
		}

	}

	protected InitialState getInitialState(String initalState) {
		if (initalState.equals("Random")) {
			return InitialState.Random;
		} else if (initalState.equals("Invading Defector")) {
			return InitialState.InvadingDefector;
		} else if (initalState.equals("Invading Cooperators")) {
			return InitialState.InvadingCooperators;
		}
		return InitialState.Random;
	}

	protected void initRandom() {
		// This is hard coded for now
		double initialRatioOfCooperators = 0.9;
		
		for (int y = 0; y < getGridHeight(); y++) {
			for (int x = 0; x < getGridWidth(); x++) {
				if (RandomHelper.nextDoubleFromTo(0, 1) < initialRatioOfCooperators) {
					addAgent(new PDAgent(this, 'c', PrisonersDilemma.Cooperator, playSelf), new GridPoint(x,y));
				} else {
					addAgent(new PDAgent(this, 'd', PrisonersDilemma.Defector, playSelf), new GridPoint(x,y));
				}
			}
		}
	}

	protected void initInvadingCooperators() {
		int centreX = getGridWidth()/2;
		int centreY = getGridHeight()/2;
		
		for (int y = 0; y < getGridHeight(); y++) {
			for (int x = 0; x < getGridWidth(); x++) {
				if ((x >= centreX -1) && (x <= centreX) && 
					(y >= centreY -1) && (y <= centreY)) {
					addAgent(new PDAgent(this, 'c', PrisonersDilemma.Cooperator, playSelf), new GridPoint(x,y));
				} else {
					addAgent(new PDAgent(this, 'd', PrisonersDilemma.Defector, playSelf), new GridPoint(x,y));
				}
			}
		}
		
	}

	protected void initInvadingDefector() {
		int centreX = (getGridWidth()-1)/2;
		int centreY = (getGridHeight()-1)/2;
		
		for (int y = 0; y < getGridHeight(); y++) {
			for (int x = 0; x < getGridWidth(); x++) {
				if (x == centreX && y == centreY) {
					addAgent(new PDAgent(this, 'd', PrisonersDilemma.Defector, playSelf), new GridPoint(x,y));
				} else {
					addAgent(new PDAgent(this, 'c', PrisonersDilemma.Cooperator, playSelf), new GridPoint(x,y));
				}
			}
		}		
	}	
}
