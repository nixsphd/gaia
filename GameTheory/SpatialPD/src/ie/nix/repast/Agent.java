package ie.nix.repast;

import repast.simphony.space.grid.GridPoint;

public abstract class Agent {
	
	private Agents model;
	private GridPoint location;
	private char agentChar;

	public Agent(Agents model, char agentChar) {
		this.model = model;
		this.agentChar = agentChar;
	}

	public char getAgentChar() {
		return agentChar;
	}

	public Agents getModel() {
		return model;
	}

	public GridPoint getLocation() {
		return location;
	}

	public char getChar() {
		return agentChar;
	}

	@Override
	public String toString() {
		return "Agent [" + agentChar+ "@" + location + "]";
	}

	protected void setAgentChar(char agentChar) {
		this.agentChar = agentChar;
	}

	protected void moveTo(GridPoint newLocation) {
		model.getGrid().moveTo(this, newLocation.getX(), newLocation.getY());
		setLocation(newLocation);
		
	}

	protected void setLocation(GridPoint location) {
		this.location = location;
	}
	
	
}
