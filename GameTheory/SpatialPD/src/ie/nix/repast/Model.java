package ie.nix.repast;

import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;

// TODO Make A extend Agent, not just Agent
public abstract class Model implements ContextBuilder<Object> {

	private Context<Object> context;
	private Context<Agent> agentContext;
	private Agents agents;
	private int numberOfSteps;
	
	public Model(Agents agents) {
		this.agents = agents;
	}

	@Override
	public Context<Object> build(Context<Object> context) {

		Parameters parmas = RunEnvironment.getInstance().getParameters();
		int numberOfSteps = ((Integer)parmas.getValue("NumberOfSteps")).intValue();
		
		return build(context, numberOfSteps);
		
	}

	public Context<Object> build(Context<Object> context, int numberOfSteps) {
		
		// TODO - Try and remove the name setting here
		initContexts(context, new DefaultContext<Agent>("Agents")); 

		buildAgents();
		
		this.numberOfSteps = numberOfSteps;
		
		// Set the number of steps the model should take and then exit.
		if (numberOfSteps != -1) {
			RunEnvironment.getInstance().endAt(numberOfSteps);
		}
		
		return context;
	}
	
	public Context<Object> build(Context<Object> context, Agents agents, int numberOfSteps) {
		// TODO - Try and remove the name setting here
		initContexts(context, new DefaultContext<Agent>("Agents")); 
		
		// Assume agents are already build, just need to wire them up.
		setAgents(agents);
		
		this.numberOfSteps = numberOfSteps;
		
		// Set the number of steps the model should take and then exit.
		if (numberOfSteps != -1) {
			RunEnvironment.getInstance().endAt(numberOfSteps);
		}
		
		return context;
	}

	public Context<Object> getContext() {
		return context;
	}

	public Context<Agent> getAgentContext() {
		return agentContext;
	}

	public Agents getAgents() {
		return agents;
	}

	public int getNumberOfSteps() {
		return numberOfSteps;
	}
	
	protected void initContexts(Context<Object> context, Context<Agent> agentContext) {
		setContext(context);
		setAgentContext(agentContext);
		context.addSubContext(agentContext);
		context.add(agentContext);
	}

	protected void buildAgents() {
		getAgents().build(getAgentContext());
	}

	protected void setContext(Context<Object> context) {
		this.context = context;
	}

	protected void setAgentContext(Context<Agent> agentContext) {
		this.agentContext = agentContext;
		context.addSubContext(agentContext);
		context.add(agentContext);
	}

	protected void setAgents(Agents agents) {
		this.agents = agents;
	}

	protected void setNumberOfSteps(int numberOfSteps) {
		this.numberOfSteps = numberOfSteps;
	}

}
