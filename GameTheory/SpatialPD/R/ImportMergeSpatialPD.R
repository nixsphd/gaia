# Open the data files and merge the data Convergence.csv
Convergence.batch_param_map <- read.csv("~/Desktop/Gaia/Repast_2.2/SpatialPD/output/Convergence.batch_param_map.csv")
Convergence <- read.csv("~/Desktop/Gaia/Repast_2.2/SpatialPD/output/Convergence.csv")

Convergence_Merged=merge(Convergence.batch_param_map, Convergence, by="run")
write.csv(Convergence_Merged, file = "~/Desktop/Gaia/Repast_2.2/SpatialPD/R/Convergence.csv")

badDataIndex = {}
runs = length(unique(Convergence_Merged$GridSize))
for (runNumber in 1:runs) {
  runData = subset(Convergence_Merged, (Convergence_Merged$run == runNumber))
    badDataIndex = which.min(unlist(runData['Converged.At']))
    append(badDataIndex,which.min(unlist(runData['Converged.At'])))
}

# Write the plots
# svg('~/Desktop/Gaia/Repast_2.2/SpatialPD/R/ConvergenceByGridSize.svg', sep = "")
# gridWidth = 3
# par(mar=c(4,4,2,1), mfrow=c(ceiling(runs/gridWidth), gridWidth))
# for (runNumber in 1:runs) {
#    runData = subset(CooperationOverTimeByPayoff, run==runNumber)
#   View(runData)
#   plot(runData$tick, runData$Frequency.of.Cooperators*100, type="l", ylab="% of Cooperators", 
#        xlab="Time",  ylim=c(0,100), main=paste("Defector Payoff = ", payoffs[runNumber]))
# }
# dev.off()