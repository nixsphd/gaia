# Open the data files and merge the data CooperationOverTime.2014.Nov.02.20_17_59.csv
RunTime = "2014.Nov.02.20_17_59"
SpatialPD.batch_param_map <- 
  read.csv(paste("~/Desktop/Gaia/Repast_2.2/SpatialPD/output/CooperationOverTime.",RunTime,".batch_param_map.csv", sep = ""))
SpatialPD <- 
  read.csv(paste("~/Desktop/Gaia/Repast_2.2/SpatialPD/output/CooperationOverTime.",RunTime,".csv", sep = ""))

CooperationOverTimeByPayoff=merge(SpatialPD.batch_param_map, SpatialPD, by="run")
write.csv(CooperationOverTimeByPayoff, 
  file = paste("~/Desktop/Gaia/Repast_2.2/SpatialPD/R/CooperationOverTime.",RunTime,".csv"))

# Write the plots
svg(paste('~/Desktop/Gaia/Repast_2.2/SpatialPD/R/CooperationOverTimeByPayoff.',RunTime,'.svg', sep = ""))
gridWidth = 3
runs = length(unique(CooperationOverTimeByPayoff$run))
payoffs = unique(CooperationOverTimeByPayoff$DefectorPayoff)
par(mar=c(4,4,2,1), mfrow=c(ceiling(runs/gridWidth), gridWidth))
for (runNumber in 1:runs) {
  runData = subset(CooperationOverTimeByPayoff, run==runNumber)
  View(runData)
  plot(runData$tick, runData$Frequency.of.Cooperators*100, type="l", ylab="% of Cooperators", 
       xlab="Time",  ylim=c(0,100), main=paste("Defector Payoff = ", payoffs[runNumber]))
}
dev.off()
