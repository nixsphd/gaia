package ie.nix.pd;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrisonersDilemmaTests {

	private final static Logger log = Logger.getLogger(PrisonersDilemmaTests.class.getName()); 
	protected PrisonersDilemma pd;
	
	@Before
	public void setUp() throws Exception {	
		log.setLevel(Level.FINEST);
		pd = new PrisonersDilemma(3, 1, 4, 2);
	}
	
	@Test
	public void testPrisonersDilemmaCC() {
		TestPlayer playerA = new TestPlayer(PrisonersDilemma.Cooperator);
		TestPlayer playerB = new TestPlayer(PrisonersDilemma.Cooperator);
		double expectedPlayerAScore = 3;
		double expectedPlayerBScore = 3;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaDD() {
		TestPlayer playerA = new TestPlayer(PrisonersDilemma.Defector);
		TestPlayer playerB = new TestPlayer(PrisonersDilemma.Defector);
		double expectedPlayerAScore = 2;
		double expectedPlayerBScore = 2;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaCD() {
		TestPlayer playerA = new TestPlayer(PrisonersDilemma.Cooperator);
		TestPlayer playerB = new TestPlayer(PrisonersDilemma.Defector);
		double expectedPlayerAScore = 1;
		double expectedPlayerBScore = 4;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaDC() {
		TestPlayer playerA = new TestPlayer(PrisonersDilemma.Defector);
		TestPlayer playerB = new TestPlayer(PrisonersDilemma.Cooperator);
		double expectedPlayerAScore = 4;
		double expectedPlayerBScore = 1;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}

	public void testPrisonersDilemma(TestPlayer playerA, TestPlayer playerB, 
			double expectedPlayerAScore, double expectedPlayerBScore) {
		
		pd.play(playerA, playerB);
		
		double actualPlayerAScore = playerA.getScore();	
		log.finest("NDB::testPrisonersDilemma()~actualPlayerAScore="+actualPlayerAScore);
		Assert.assertEquals(expectedPlayerAScore, actualPlayerAScore, 0.0000001);
		
		double actualPlayerBScore = playerB.getScore();
		log.log(Level.FINEST, "NDB::testPrisonersDilemma()~actualPlayerBScore="+actualPlayerBScore);
		Assert.assertEquals(expectedPlayerBScore, actualPlayerBScore, 0.0000001);
		
	}
}
