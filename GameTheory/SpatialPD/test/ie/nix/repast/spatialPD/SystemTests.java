package ie.nix.repast.spatialPD;

import ie.nix.repast.Agent;
import ie.nix.repast.Tests;
import ie.nix.repast.spatialPD.SpatialPDAgents.InitialState;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.DefaultContext;
import repast.simphony.engine.environment.DefaultScheduleRunner;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.Schedule;

public class SystemTests extends Tests {

	@Before
	public void setUp() throws Exception {
		super.setUp();
		setContext(new DefaultContext<Object>());	
		setAgentsContext(new DefaultContext<Agent>());	
		setAgents(new SpatialPDAgents());
		setModel(new SpatialPDModel(getAgents()));
		setSchedule(new Schedule()); 		
		setRunner(new DefaultScheduleRunner());
		RunEnvironment.init(getSchedule(), getRunner(), null, true);

	}

	@Test
	public void testAllC() {	
		double defectorPayoff = 2;
		boolean playSelf = false;
		char[][] initialAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'}
			};
		int numberOfSteps = 1;
		char[][] expectedAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'}
			};
		test(defectorPayoff, playSelf, initialAgents, numberOfSteps, expectedAgents);
	}
	
	@Test
	public void testAllCOneD() {	
		double defectorPayoff = 2;
		boolean playSelf = false;
		char[][] initialAgents = {
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','d','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'}
			};
		int numberOfSteps = 1;
		char[][] expectedAgents = {
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','d','d','d','c','c'},
				{'c','c','d','d','d','c','c'},
				{'c','c','d','d','d','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'}
			};
		test(defectorPayoff, playSelf, initialAgents, numberOfSteps, expectedAgents);
	}

	@Test
	public void testAllCOneD2Steps() {	
		double defectorPayoff = 2;
		boolean playSelf = false;
		char[][] initialAgents = {
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','d','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c'}
			};
		int numberOfSteps = 2;
		char[][] expectedAgents = {
				{'c','c','c','c','c','c','c'},
				{'c','d','d','d','d','d','c'},
				{'c','d','d','d','d','d','c'},
				{'c','d','d','d','d','d','c'},
				{'c','d','d','d','d','d','c'},
				{'c','d','d','d','d','d','c'},
				{'c','c','c','c','c','c','c'}
			};
		test(defectorPayoff, playSelf, initialAgents, numberOfSteps, expectedAgents);
	}
	
	@Test
	public void testAllCOneD3Steps() {	
		double defectorPayoff = 2;
		boolean playSelf = false;
		char[][] initialAgents = {
				{'c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','d','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c'}
			};
		int numberOfSteps = 3;
		char[][] expectedAgents = {
				{'c','c','c','c','c','c','c','c','c'},
				{'c','d','d','d','c','d','d','d','c'},
				{'c','d','d','d','d','d','d','d','c'},
				{'c','d','d','d','d','d','d','d','c'},
				{'c','c','d','d','d','d','d','c','c'},
				{'c','d','d','d','d','d','d','d','c'},
				{'c','d','d','d','d','d','d','d','c'},
				{'c','d','d','d','c','d','d','d','c'},
				{'c','c','c','c','c','c','c','c','c'}
			};
		test(defectorPayoff, playSelf, initialAgents, numberOfSteps, expectedAgents);
	}
	
	@Test
	public void testAllDInvasionGrid15() {	
		double defectorPayoff = 1.85;
		boolean playSelf = true;
		char[][] initialAgents = {
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','d','d','d','c','d','d','d','c','c','c','c'},
				{'c','c','c','c','d','d','d','c','d','d','d','c','c','c','c'},
				{'c','c','c','c','d','d','d','d','d','d','d','c','c','c','c'},
				{'c','c','c','c','c','c','d','d','d','c','c','c','c','c','c'},
				{'c','c','c','c','d','d','d','d','d','d','d','c','c','c','c'},
				{'c','c','c','c','d','d','d','c','d','d','d','c','c','c','c'},
				{'c','c','c','c','d','d','d','c','d','d','d','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'}				};
		int numberOfSteps = 1;
		char[][] expectedAgents = {
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','d','d','d','d','d','d','d','d','d','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'},
				{'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c'}
			};
		test(defectorPayoff, playSelf, initialAgents, numberOfSteps, expectedAgents);
	}

	public void test(double defectorPayoff, boolean playSelf, char[][] initialAgents, int numberOfSteps, char[][] expectedAgents) {
		
		((SpatialPDAgents)getAgents()).build(getAgentsContext(), initialAgents, defectorPayoff, playSelf);
//		System.out.println("NDB::test()~initialAgents="+toString(initialAgents));

		Iterable<Agent> agents = getAgents().getListOfAgents();
		for (Agent agent : agents) {
			getSchedule().schedule(agent);
		}

		while (numberOfSteps > 0) {
			getSchedule().execute();
			numberOfSteps--;
		}
		
		char[][] actualAgents = getAgents().getState();
		
//		System.out.println("NDB::test()~actualAgents="+toString(actualAgents));	
//		System.out.println("NDB::test()~expectedAgents="+toString(expectedAgents));			
		Assert.assertEquals(toString(expectedAgents),toString(actualAgents));
		
	}
	
	@Test
	public void testRunStop() {	
		double defectorPayoff = 1.85;
		boolean playSelf = false;
		char[][] initialAgents = {
				{'d','d',},
				{'d','d',}
				};		
		int numberOfSteps = 2;		
		boolean expectedFinishing = true;
		testRunStop(defectorPayoff, playSelf, initialAgents, numberOfSteps, expectedFinishing);
	}
	
	@Test
	public void testRunGo() {	
		double defectorPayoff = 1.85;
		boolean playSelf = false;
		char[][] initialAgents = {
				{'c','d',},
				{'d','c',}
				};		
		int numberOfSteps = 2;		
		boolean expectedFinishing = false;
		
		testRunStop(defectorPayoff, playSelf, initialAgents, numberOfSteps, expectedFinishing);
	}
	
	public void testRunStop(double defectorPayoff, boolean playSelf, 
			char[][] initialAgents, int numberOfSteps, boolean expectedFinishing) {		

		boolean actualFinishing = !getRunner().go();
		
		// Initialise agents
		((SpatialPDAgents)getAgents()).build(getAgentsContext(), initialAgents, defectorPayoff, playSelf);
		// Iitialis the model
		((SpatialPDModel)getModel()).build(getContext(), getAgents(), numberOfSteps);

		// Initialise and exectue the scheduled actions
		Iterable<Agent> agents = getAgents().getListOfAgents();
		for (Agent agent : agents) {
			getSchedule().schedule(agent);
		}	
		for (Object object : getModel().getContext().getObjects(ConvergenceChecker.class)) {
			getSchedule().schedule(object);
		}
		getSchedule().execute();
		
		actualFinishing = !getRunner().go();
		System.out.println("NDB::test()~actualFinishing="+actualFinishing);			
		Assert.assertEquals(expectedFinishing, actualFinishing);
		
	}


	@Test
	public void testBuildRandom() {		
			InitialState initialState = InitialState.InvadingDefector;
			int gridWidth = 15;
			int gridHeight = 15;
			double defectorPayoff = 1.85;
			boolean playSelf = false;
			int numberOfSteps = -1;
			
			((SpatialPDAgents)getAgents()).build(getAgentsContext(), gridWidth, gridHeight, numberOfSteps,
					initialState, defectorPayoff, playSelf);
			
//			char[][] actualAgents = getAgents().getState();
	//		System.out.println("NDB::testRandomBuild()~actualAgents="+toString(actualAgents));	
		}

}
