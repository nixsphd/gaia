package ie.nix.repast.spatialPD;

import ie.nix.repast.Agent;
import ie.nix.repast.Tests;
import ie.nix.repast.spatialPD.SpatialPDAgents.InitialState;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import repast.simphony.context.DefaultContext;
import repast.simphony.space.grid.GridPoint;

public class UnitTests extends Tests {

	@Before
	public void setUp() throws Exception {
		super.setUp();
		setAgentsContext(new DefaultContext<Agent>());	
		setAgents(new SpatialPDAgents());
	
	}
	
	@Test
	public void testBuild() {		
		char[][] initialAgents = {
				{' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' '},
				{' ','d',' ',' ',' ',' '},
				{' ',' ',' ',' ',' ',' '}
				};
		
		getAgents().build(getAgentsContext(), initialAgents);
		

		char[][] actualAgents = getAgents().getState();
//		System.out.println("NDB::testBuild()~initialAgents="+toString(initialAgents));
//		System.out.println("NDB::testBuild()~actualAgents="+toString(actualAgents));		
		Assert.assertEquals(toString(initialAgents),toString(actualAgents));
		
	}
	
	@Test
	public void testBuildRandom() {		
		InitialState initialState = InitialState.Random;
		int gridWidth = 6;
		int gridHeight = 8;
		char[][] expectedAgents = {
				{'c','c','c','c','c','c'},
				{'c','c','c','c','c','c'},
				{'c','c','c','c','c','c'},
				{'c','c','c','c','c','c'},
				{'c','c','c','c','c','c'},
				{'d','c','c','c','c','d'},
				{'c','c','c','c','c','c'},
				{'c','c','c','c','c','c'}
			};
		testBuild(gridWidth, gridHeight, initialState, expectedAgents);
	}
	
	@Test
	public void testBuildInvadingDefector() {		
		InitialState initialState = InitialState.InvadingDefector;
		int gridWidth = 5;
		int gridHeight = 5;
		char[][] expectedAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','d','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'}
			};
		testBuild(gridWidth, gridHeight, initialState, expectedAgents);
	}
	
	@Test
	public void testBuildInvadingDefectorEvenGridSize() {		
		InitialState initialState = InitialState.InvadingDefector;
		int gridWidth = 6;
		int gridHeight = 6;
		char[][] expectedAgents = {
				{'c','c','c','c','c','c'},
				{'c','c','c','c','c','c'},
				{'c','c','c','c','c','c'},
				{'c','c','d','c','c','c'},
				{'c','c','c','c','c','c'},
				{'c','c','c','c','c','c'}
			};
		testBuild(gridWidth, gridHeight, initialState, expectedAgents);
	}
	
	@Test
	public void testBuildInvadingCooperators() {		
		InitialState initialState = InitialState.InvadingCooperators;
		int gridWidth = 6;
		int gridHeight = 6;
		char[][] expectedAgents = {
				{'d','d','d','d','d','d'},
				{'d','d','d','d','d','d'},
				{'d','d','c','c','d','d'},
				{'d','d','c','c','d','d'},
				{'d','d','d','d','d','d'},
				{'d','d','d','d','d','d'}
			};
		testBuild(gridWidth, gridHeight, initialState, expectedAgents);
	}
	
	@Test
	public void testBuildInvadingCooperatorsOddGridSize() {		
		InitialState initialState = InitialState.InvadingCooperators;
		int gridWidth = 7;
		int gridHeight = 7;
		char[][] expectedAgents = {
				{'d','d','d','d','d','d','d'},
				{'d','d','d','d','d','d','d'},
				{'d','d','d','d','d','d','d'},
				{'d','d','c','c','d','d','d'},
				{'d','d','c','c','d','d','d'},
				{'d','d','d','d','d','d','d'},
				{'d','d','d','d','d','d','d'}
			};
		testBuild(gridWidth, gridHeight, initialState, expectedAgents);
	}
	
	public void testBuild(int gridWidth, int gridHeight, InitialState initialState, char[][] expectedAgents) {
	
		double defectorPayoff = 2;
		boolean playSelf = false;
		int numberOfSteps = -1;
		
		((SpatialPDAgents)getAgents()).build(getAgentsContext(), gridWidth, gridHeight, numberOfSteps, 
				initialState, defectorPayoff, playSelf);
		
		char[][] actualAgents = getAgents().getState();
//		System.out.println("NDB::testRandomBuild()~actualAgents="+toString(actualAgents));	
//		System.out.println("NDB::testRandomBuild()~expectedAgents="+toString(expectedAgents));		
		Assert.assertEquals(toString(expectedAgents),toString(actualAgents));
		
	}
	
	@Test
	public void testCompeteBottomLeft() {	
		double defectorPayoff = 2;
		GridPoint competingAgentLocation = new GridPoint(0,0);
		char[][] initialAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'d','c','c','c','c'}
			};
		double expectedScore = 16;
		testCompete(defectorPayoff, competingAgentLocation, initialAgents, expectedScore);
	}
		
	@Test
	public void testCompeteTopRight() {	
		double defectorPayoff = 2;
		GridPoint competingAgentLocation = new GridPoint(4,4);
		char[][] initialAgents = {
				{'c','c','c','c','d'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'}
			};
		double expectedScore = 16;
		testCompete(defectorPayoff, competingAgentLocation, initialAgents, expectedScore);
	}
	
	@Test
	public void testCompeteCentre() {	
		double defectorPayoff = 2;
		GridPoint competingAgentLocation = new GridPoint(2,2);
		char[][] initialAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','d','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'}
			};
		double expectedScore = 16;
		testCompete(defectorPayoff, competingAgentLocation, initialAgents, expectedScore);
	}
	
	public void testCompete(double defectorPayoff, GridPoint competingAgentLocation, 
			char[][] initialAgents, double expectedScore) {
		
		boolean playSelf = false;
		
		((SpatialPDAgents)getAgents()).build(getAgentsContext(), initialAgents, defectorPayoff, playSelf);
		
		PDAgent competingAgent = (PDAgent)getAgents().getAgent(competingAgentLocation);
		competingAgent.compete();
		
		double actualScore = competingAgent.getScore();
		
//		System.out.println("NDB::testRandomBuild()~actualScore="+actualScore);	
//		System.out.println("NDB::testRandomBuild()~expectedScore="+expectedScore);		
		Assert.assertEquals(expectedScore,actualScore, 0.0000001);
		
	}
	
	@Test
	public void testEvolveWithChange() {	
		double defectorPayoff = 2;
		GridPoint evolvingAgentLocation = new GridPoint(1,1);
		char[][] initialAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','d','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'}
			};
		double[][] initialScores = {
				{0.0,0.0,0.0,0.0,0.0},
				{0.0,0.0,0.0,0.0,0.0},
				{0.0,0.0,16.0,0.0,0.0},
				{0.0,0.0,0.0,0.0,0.0},
				{0.0,0.0,0.0,0.0,0.0}
			};
		char[][] expectedAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','d','c','c'},
				{'c','d','c','c','c'},
				{'c','c','c','c','c'}
			};
		testEvolve(defectorPayoff, evolvingAgentLocation, initialAgents, initialScores, expectedAgents);
	}
	
	@Test
	public void testEvolveWithoutChange() {	
		double defectorPayoff = 2;
		GridPoint evolvingAgentLocation = new GridPoint(0,0);
		char[][] initialAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','d','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'}
			};
		double[][] initialScores = {
				{0.0,0.0,0.0,0.0,0.0},
				{0.0,0.0,0.0,0.0,0.0},
				{0.0,0.0,16.0,0.0,0.0},
				{0.0,0.0,0.0,0.0,0.0},
				{0.0,0.0,0.0,0.0,0.0}
			};
		char[][] expectedAgents = {
				{'c','c','c','c','c'},
				{'c','c','c','c','c'},
				{'c','c','d','c','c'},
				{'c','c','c','c','c'},
				{'c','c','c','c','c'}
			};
		testEvolve(defectorPayoff, evolvingAgentLocation, initialAgents, initialScores, expectedAgents);
	}
	
	public void testEvolve(double defectorPayoff, GridPoint evolvingAgentLocation, 
			char[][] initialAgents, double[][] initialScores, char[][] expectedAgents) {
		
		boolean playSelf = false;

		((SpatialPDAgents)getAgents()).build(getAgentsContext(), initialAgents, defectorPayoff, playSelf);
		System.out.println("NDB::testRandomBuild()~initialScores="+toString(initialScores));
		
		setScores(getAgents().getAgents(), initialScores);
		
		PDAgent evolvingAgent = (PDAgent)getAgents().getAgent(evolvingAgentLocation);
		evolvingAgent.select();
		evolvingAgent.evolve();
		
		char[][] actualAgents = getAgents().getState();
		
		System.out.println("NDB::testRandomBuild()~actualAgents="+toString(actualAgents));	
		System.out.println("NDB::testRandomBuild()~expectedAgents="+toString(expectedAgents));			
		Assert.assertEquals(toString(expectedAgents),toString(actualAgents));
		
	}

	protected static void setScores(Agent[][] agents, double[][] initialScores) {
		for (int h = 0; h < agents.length; h++) {
			for (int w = 0; w < agents[h].length; w++) {
				((PDAgent)agents[h][w]).playScore(initialScores[h][w]);
			}
		}
	}
	
	protected static double[][] getScores(Agent[][] agents) {
		double[][] scores = new double[agents.length][agents[0].length];
		for (int h = 0; h < agents.length; h++) {
			for (int w = 0; w < agents[h].length; w++) {
				scores[h][w] = ((PDAgent)agents[h][w]).getScore();
			}
		}
		return scores;
	}
	
}
