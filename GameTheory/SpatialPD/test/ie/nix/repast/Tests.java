package ie.nix.repast;

import org.junit.Before;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.Runner;
import repast.simphony.engine.schedule.ISchedule;
import repast.simphony.random.RandomHelper;

public class Tests {

	private Context<Agent> agentsContext;
	private Context<Object> context;
	private ISchedule schedule;
	private Agents agents;
	private Model model;
	private Runner runner;	

	@Before
	public void setUp() throws Exception {
		RandomHelper.setSeed(1);
	
	}
	
	public Context<Agent> getAgentsContext() {
		return agentsContext;
	}

	public void setAgentsContext(Context<Agent> context) {
		this.agentsContext = context;
	}
	
	public Context<Object> getContext() {
		return context;
	}

	public void setContext(Context<Object> context) {
		this.context = context;
	}

	public Agents getAgents() {
		return agents;
	}

	public void setAgents(Agents agents) {
		this.agents = agents;
	}
	
	public Model getModel() {
		return model;
	}
	
	public void setModel(Model model) {
		this.model = model;
	}
	
	public ISchedule getSchedule() {
		return schedule;
		
	}
	
	public void setSchedule(ISchedule iSchedule) {
		this.schedule = iSchedule;
		
	}

	public void setRunner(Runner runner) {
		this.runner = runner;
	}

	public Runner getRunner() {
		return runner;
	}
	
	public static String toString(double[][] state) {
		StringBuilder string = new StringBuilder("{\n");
		for (int h = 0; h < state.length; h++) {
			string.append("{");
			for (int w = 0; w < state[h].length; w++) {
				string.append(Double.toString(state[h][w])+",");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.lastIndexOf(","), string.length(),"\n}");
		return string.toString();
	}	
	
	public static String toString(char[][] state) {
		StringBuilder string = new StringBuilder("{\n");
		for (int y = 0; y < state.length; y++) {
			string.append("{");
			for (int x = 0; x < state[y].length; x++) {
				string.append("'"+state[y][x]+"',");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.lastIndexOf(","), string.length(),"\n}");
		return string.toString();
	}	
	
	public static String toString(Agent[][] state) {
		StringBuilder string = new StringBuilder("{\n");
		for (int y = 0; y < state.length; y++) {
			string.append("{");
			for (int x = 0; x < state[y].length; x++) {
				Agent agent = state[y][x];
				if (agent == null) {
					string.append("' ',");
				} else {
					string.append("'"+agent.getChar()+"',");
				} 
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.lastIndexOf(","), string.length(),"\n}");
		return string.toString();
	}
	
}
