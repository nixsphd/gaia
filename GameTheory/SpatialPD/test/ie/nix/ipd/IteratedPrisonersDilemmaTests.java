package ie.nix.ipd;

import ie.nix.ipd.TestPlayer;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IteratedPrisonersDilemmaTests {

	protected IteratedPrisonersDilemma ipd;
	
	@Before
	public void setUp() throws Exception {	
		ipd = new IteratedPrisonersDilemma(3, 1, 4, 2);
	}
	
	@Test
	public void testPrisonersDilemmaCvsC() {
		TestPlayer playerA = new TestPlayer(IteratedPrisonersDilemma.Cooperator);
		TestPlayer playerB = new TestPlayer(IteratedPrisonersDilemma.Cooperator);
		int numberOfGames = 10;
		double expectedPlayerAScore = 30;
		double expectedPlayerBScore = 30;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaDvsD() {
		TestPlayer playerA = new TestPlayer(IteratedPrisonersDilemma.Defector);
		TestPlayer playerB = new TestPlayer(IteratedPrisonersDilemma.Defector);
		int numberOfGames = 10;
		double expectedPlayerAScore = 20;
		double expectedPlayerBScore = 20;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaTFTvsTFT() {
		TestPlayer playerA = new TestPlayer(IteratedPrisonersDilemma.TitForTat);
		TestPlayer playerB = new TestPlayer(IteratedPrisonersDilemma.TitForTat);
		int numberOfGames = 10;
		double expectedPlayerAScore = 30;
		double expectedPlayerBScore = 30;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaTFTvsDD() {
		TestPlayer playerA = new TestPlayer(IteratedPrisonersDilemma.TitForTat);
		TestPlayer playerB = new TestPlayer(IteratedPrisonersDilemma.Defector);
		int numberOfGames = 10;
		double expectedPlayerAScore = 19; //1 + (2 * 9) = 19
		double expectedPlayerBScore = 22; //4 + (2 * 9) = 22
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}

	public void testIteratedPrisonersDilemma(TestPlayer playerA, TestPlayer playerB, int numberOfGames,
			double expectedPlayerAScore, double expectedPlayerBScore) {
		
		ipd.play(playerA, playerB, numberOfGames);
		
		double actualPlayerAScore = playerA.getScore();	
//		log.warn("NDB::testPrisonersDilemma()~actualPlayerAScore="+actualPlayerAScore);
		Assert.assertEquals(expectedPlayerAScore, actualPlayerAScore, 0.0000001);
		
		double actualPlayerBScore = playerB.getScore();		
//		log.debug("NDB::testPrisonersDilemma()~actualPlayerBScores="+actualPlayerBScores);
		Assert.assertEquals(expectedPlayerBScore, actualPlayerBScore, 0.0000001);
		
	}
}
