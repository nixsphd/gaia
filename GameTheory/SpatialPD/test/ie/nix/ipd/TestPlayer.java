package ie.nix.ipd;

import ie.nix.ipd.IteratedPrisonersDilemma.Player;
import ie.nix.ipd.IteratedPrisonersDilemma.Strategy;
import ie.nix.pd.PrisonersDilemma.Play;

public class TestPlayer implements Player {

	protected Strategy strategy;
	protected double score;

	public TestPlayer(Strategy strategy) {
		this.strategy = strategy;
	}
	
	@Override
	public Play play(Play opponentsLastPlay) {
		return strategy.play(opponentsLastPlay);
		
	}

	@Override
	public void playScore(double score) {
		this.score += score;
		
	}

	public double getScore() {
		return score;
	}

}
