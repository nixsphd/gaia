setwd("~/Desktop/Gaia/Game Theory/TagIPD/R")
genomes <- read.csv("Genomes.csv")
library(plyr)
library(reshape)
library(ggplot2)

plotGen <- function(gen = 0) {
    genData <- genomes[genomes$Generation == gen,]
    plot(genData$p,genData$q,, ylab="Q", xlab="P",  ylim=c(0,1), xlim=c(0,1), 
         asp=1, main=paste("Generation ", gen))
}

numberOfGenerations <- length(unique(genomes$Generation))
generations <- unique(genomes$Generation)
limit <- generations < 90
generations <- generations[limit]
svg('Genomes.svg')
gridWidth = 4
par(mar=c(4,4,2,1), mfrow=c(ceiling(length(generations)/gridWidth), gridWidth))
for(gen in generations) {
    plotGen(gen)
}
dev.off()

generations <- gl(50, (20000/50))
plot(6:25,rnorm(20),type="b",xlim=c(1,30),ylim=c(-2.5,2.5),col=2)
par(new=T)
plot(rnorm(30),type="b",axes=F,col=3)
par(new=F)

plot(rnorm(100),type="l",col=2)
lines(rnorm(100),col=3)

plot(x=c(0,50),y=c(0,1),xlab="Generations",ylim=c(0,1))
lines(tapply(genomes$i,generations,mean), col="red")
lines(tapply(genomes$p,generations,mean), col="blue")
lines(tapply(genomes$q,generations,mean), col="green")
lines(tapply(genomes$T,generations,mean), col="black")
legend('topright', lty=1,legend=c("i","p","q","T"), col=c("red","blue","green","black"))

splits <- split(genomes[,10:13],generations)
i <- split(genomes[,10],generations)
p <- split(genomes[,11],generations)
q <- split(genomes[,12],generations)
T <- split(genomes[,13],generations)

bins <- seq(0.0, 1, 0.05)
cuts <- list()
cuts$i <- lapply(i, cut, breaks=bins, labels=bins[2:21])
cuts$p <- lapply(p, cut, breaks=bins, labels=bins[2:21])
cuts$q <- lapply(q, cut, breaks=bins, labels=bins[2:21])
cuts$T <- lapply(T, cut, breaks=bins, labels=bins[2:21])

genomesPerGeneration <- split(genomes,genomes$Generation)
numberofGenerations <- length(genomesPerGeneration)
means <- data.frame(i = numeric(),
                    p = numeric(),
                    q = numeric(),
                    T = numeric(),
                    Evaluation = numeric(),
                    stringsAsFactors = FALSE)
for (generation in genomesPerGeneration) {
    newRow <- data.frame(i=mean(generation$i), p=mean(generation$p), q=mean(generation$q), 
                         T=mean(generation$T), Evaluation=mean(generation$Evaluation))
    row.names(newRow) <- "names(generation)"
    means <- rbind(means, newRow)
    str(generation)
}
means

qplot(data=means)

qplot(q, Evaluation, data=genomes, facets=Generation~.)

genomes <- read.csv("Genomes.csv")
qplot(p, q, data=genomes, facets=Generation~., colour=Evaluation)
ggsave("Genomes.svg");

