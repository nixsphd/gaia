library(plyr)
library(reshape2)
library(ggplot2)

setwd("~/Desktop/Gaia/GameTheory/TagIPD/R")
genomes <- read.csv("Genomes.csv",strip.white = TRUE)

qplot(Generation, Evaluation, data=genomes)

qplot(p, q, data=genomes, facets=Generation~., colour=Evaluation)
ggsave("Genomes.svg");

# genomesPlot <- ggplot(genomes)
# genomesPlot <- genomesPlot + geom_point(aes(T, Evaluation, colour=Generation)) + 
#     facet_wrap(~Generation)
# genomesPlot
# ggsave("T.svg");

bins <- seq(0.0, 1, 0.05)
genomes$TBins <- cut(genomes$T, bins, labels=bins[2:21])
evaluationTBins <- genomes[,c(9,14:15)]

genomesPlot <- ggplot(evaluationTBins)
genomesPlot <- genomesPlot + geom_boxplot(aes(TBins, Evaluation, 
                                              colour=Generation)) + 
    facet_wrap(~Generation)
ggsave("TBins.svg");
genomesPlot

TCounts <- count(evaluationTBins,vars=c("Generation", "TBins"))
genomesPlot <- ggplot(TCounts)
genomesPlot <- genomesPlot + geom_point(aes(TBins, freq, colour=Generation)) + 
    facet_wrap(~Generation)
genomesPlot

genomesPlot <- ggplot(genomes)
genomesPlot <- genomesPlot + geom_point(aes(p, q, colour=T, size=Evaluation)) + 
    facet_wrap(~Generation)
genomesPlot

qplot(Evaluation,i,data=genomes, group=NumberOfTagTrials, 
      colour=NumberOfTagTrials)
qplot(Evaluation,p,data=genomes, group=NumberOfTagTrials, 
      colour=NumberOfTagTrials)
qplot(Evaluation,q,data=genomes, group=NumberOfTagTrials, 
      colour=NumberOfTagTrials)

meltedGenomes <- melt(genomes, id=c("NumberOfAgents", "NumberOfGenerations", 
            "SizeOfPlayerGroup", "NumberOfTagTrials", "NumberOfGames", 
            "ProbabilityOfSelectingFittest", "MutationProbability", 
            "MutationSpeed", "Generation"))
castGenomes <- dcast(meltedGenomes, Generation+NumberOfTagTrials~variable, mean)

qplot(Generation,Evaluation,data=castGenomes,geom=("line"), 
      group=NumberOfTagTrials, colour=NumberOfTagTrials)

genomes <- read.csv("AverageFitnesss.csv",strip.white = TRUE)
qplot(Generation, Evaluation, data=genomes, group=Name, colour=Name, 
      geom=("line"))

genomes <- read.csv("AverageFitnesss.csv",strip.white = TRUE)
qplot(Generation, Evaluation, data=genomes, colour=StandardDeviation, 
      facets=Name~., geom=("line"))

genomes <- read.csv("AverageFitnesss.csv",strip.white = TRUE)
meltedGenomes <- melt(genomes, id=c("Name", "Generation"))
qplot(Generation, value, data=meltedGenomes, group=variable, 
      colour=variable, facets=Name~., geom=("line"))
