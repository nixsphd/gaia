package ie.nix.tagIPD;

import ie.nix.ea.Agent;
import ie.nix.ea.Population.Genotype;
import ie.nix.pd.IPDPlayer;
import ie.nix.pd.IteratedPrisonersDilemma.Strategy;
import ie.nix.util.Randomness;

public class TagIPDPlayer extends IPDPlayer implements Agent<Double> {

	private final double searchPickiness = 0.2;
	
	protected double tag;

	public TagIPDPlayer() {
		super(null);
	}
	
	protected TagIPDPlayer(double i, double p, double q, double t) {
		super(new Strategy(i, p, q, true));
		tag = t;
	}

	@Override
	public void decode(Genotype<Double> genotype) {		
		Double[] chromosome = genotype.getChromosome();
		strategy = new Strategy(chromosome[0], chromosome[1], chromosome[2]);
		tag = chromosome[3];
		// clear the old scores and game counts
		reset();
		
	}

	@Override
	public double evaluation() {
		double evalustion = getScore()/getNumberOfGames();
//		System.out.println("NDB::evaluation() evaluation="+evalustion);
		return evalustion;
	}
	
	public boolean equals(TagIPDPlayer tagIPDPlayer) {
		return (strategy.equals(tagIPDPlayer.strategy) &&
				(Double.compare(tag, tagIPDPlayer.tag) == 0));
	}

	public boolean wantsToPlay(TagIPDPlayer candidatePlayer) {
		double wantsToPlay = (1 - Math.pow(Math.abs((tag - candidatePlayer.tag)), searchPickiness));
		return (Randomness.nextDouble() <= wantsToPlay);
	}

	@Override
	public String toString() {
		return "new TagIPDPlayer("+strategy.getI()+","+strategy.getP()+","+strategy.getQ()+","+tag+")";
	}

}
