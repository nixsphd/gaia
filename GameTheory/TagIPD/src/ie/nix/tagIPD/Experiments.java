
package ie.nix.tagIPD;

import java.io.FileNotFoundException;

import ie.nix.ea.Population.Genotype;
import ie.nix.tagIPD.TagIPDModel.Parameters;
import ie.nix.util.DataFrame;
import ie.nix.util.Randomness;

public class Experiments {
	
	public static void main(String[] args) throws FileNotFoundException {
//		flagshipRun();
		populationAverageFitnessRun();
//		flagshipBatch();
//		varingMutationRateBatch();
//		varingMutationSpreadBatch();
//		varingPlayerMovesBatch();
//		varingPlayerGroupBatch();
		System.out.println("Done.");
	}
	
	public static void flagshipRun() {

		DataFrame dataFrame = new DataFrame();
		writeRunHeaders(dataFrame,"Genomes.csv");

		int numberOfAgents = 400;
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.probabilityOfSelectingFittest = 0.9;
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		params.numberOfTagTrials = 5;
		params.searchCost = 0.02;
	
		int numberOfGenerationsToBeRun = 10;	
		
		run(params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);	

		params.numberOfTagTrials = 0;
		run(params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);	
		
		dataFrame.writeRows();

	}

	protected static void writeRunHeaders(DataFrame dataFrame, String filename) {
		dataFrame.initWriter(filename);
		dataFrame.addColumns(new String[]{
				"NumberOfAgents", 
				"NumberOfGenerations", 
				"SizeOfPlayerGroup",
				"NumberOfTagTrials", 
				"NumberOfGames", 
				"ProbabilityOfSelectingFittest", 
				"MutationProbability", 
				"MutationSpread",
				"Generation",
				"i", "p", "q", "T","Evaluation"
		});
		dataFrame.writeHeaders();
	}
	
	protected static void run(Parameters params, int numberOfAgents, DataFrame dataFrame, int numberOfGenerationsToBeRun) {
		Randomness.setSeed(1);
		TagIPDModel model = new TagIPDModel(params, numberOfAgents);	
		// for each generation to be run
		for (int generation = 0; generation <= params.numberOfGenerations; generation += numberOfGenerationsToBeRun) {	
			model.run(numberOfGenerationsToBeRun);
			for (Genotype<Double> genotype : model.getPreviousPopulation().getGenotypes()) {
				Double[] chromosome = genotype.getChromosome();
				dataFrame.addRow(numberOfAgents,params.numberOfGenerations,
						params.sizeOfPlayerGroup,params.numberOfTagTrials,params.numberOfGames,
						params.probabilityOfSelectingFittest,
						params.mutationProbabillity,params.mutationSpread,
						generation,chromosome[0],chromosome[1],chromosome[2],chromosome[3],
						genotype.getEvaluation());
			}
		}
	}
	
	public static void populationAverageFitnessRun() {
		
		int numberOfAgents = 400;
		Parameters params = new Parameters();
		params.numberOfGenerations = 3000;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.probabilityOfSelectingFittest = 0.9;
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		params.numberOfTagTrials = 5;
		params.searchCost = 0.02;	
		
		DataFrame dataFrame = new DataFrame();
		dataFrame.initWriter("AverageFitnesss.csv");
		dataFrame.addColumns(new String[]{
				"Name", "Generation", 
				"AverageI", "AverageP", "AverageQ", 
				"Evaluation", "StandardDeviation"
		});
		dataFrame.writeHeaders();

		Randomness.setSeed(1);

////		Double[][] preCannedPopulationToClone = {
////				{1.0, 1.0, 0.0, 0.0},
////				{1.0, 1.0, 0.0, 0.0},
////				{1.0, 1.0, 0.0, 0.0},
////				{1.0, 1.0, 0.0, 0.0},
////				{1.0, 1.0, 0.0, 0.0},
////				{1.0, 1.0, 0.0, 0.0},
////				{1.0, 1.0, 0.0, 0.0},
////				{1.0, 1.0, 0.0, 0.0},
////				{1.0, 1.0, 0.0, 0.0},
////				{0.0, 0.0, 0.0, 0.0},
////		};
//		Double[][] preCannedPopulationToClone = {
//				{0.0, 0.0, 0.0, 0.0},
//				{0.0, 0.0, 0.0, 0.0},
//				{0.0, 0.0, 0.0, 0.0},
//				{0.0, 0.0, 0.0, 0.0},
//				{0.0, 0.0, 0.0, 0.0},
//				{0.0, 0.0, 0.0, 0.0},
//				{0.0, 0.0, 0.0, 0.0},
//				{0.0, 0.0, 0.0, 0.0},
//				{0.0, 0.0, 0.0, 0.0},
//				{1.0, 1.0, 0.0, 0.0},
//		};
//		int lengthOfGenome = preCannedPopulationToClone[0].length;
//		Double[][] preCannedPopulation = new Double[numberOfAgents][lengthOfGenome];
//		for (int a = 0; a < numberOfAgents; a++) {
//			System.arraycopy(preCannedPopulationToClone[a%preCannedPopulationToClone.length], 0, preCannedPopulation[a], 0, lengthOfGenome);
//		}
		
		int numberOfModels = 30;
		params.numberOfTagTrials = 0;
//		TagIPDModel model = new TagIPDModel(params, preCannedPopulation);	
		TagIPDModel[] models = new TagIPDModel[numberOfModels];
		for (int m = 0; m < numberOfModels; m++) {
			models[m] = new TagIPDModel(params, numberOfAgents);	
		}

		for (int generation = 0; generation <= params.numberOfGenerations; generation++) {	
			
			double averageI = 0;
			double averageP = 0;
			double averageQ = 0;
			double averageFitness = 0;
			double standardDeviation = 0;

			for (int m = 0; m < numberOfModels; m++) {
				models[m].run(1);
				double modelAverageFitness = 0;
				double modelStandardDeviation = 0;
				for (Genotype<Double> genotype : models[m].getPreviousPopulation().getGenotypes()) {
					Double[] chromosome = genotype.getChromosome();
					averageI += chromosome[0];
					averageP += chromosome[1];
					averageQ += chromosome[2];
					modelAverageFitness += genotype.getEvaluation();

				}
				
				modelAverageFitness /= numberOfAgents;
				for (Genotype<Double> genotype : models[m].getPreviousPopulation().getGenotypes()) {
					modelStandardDeviation += Math.pow(modelAverageFitness - genotype.getEvaluation(), 2);
				}
				modelStandardDeviation /= numberOfAgents-1;
				
				averageFitness += modelAverageFitness;
				standardDeviation += modelStandardDeviation;
			}
			averageI /= numberOfAgents*numberOfModels;
			averageP /= numberOfAgents*numberOfModels;
			averageQ /= numberOfAgents*numberOfModels;
			averageFitness /= numberOfModels;
			standardDeviation /= numberOfModels;
			
			dataFrame.addRow("Tag Trials "+params.numberOfTagTrials, generation, averageI, averageP, averageQ, averageFitness, standardDeviation);
		}

//		Randomness.setSeed(1);
//		
//		TagIPDModel tagModel = new TagIPDModel(params, numberOfAgents);
//		for (int generation = 0; generation <= params.numberOfGenerations; generation++) {	
//			tagModel.run(1);
//			double averageI = 0;
//			double averageP = 0;
//			double averageQ = 0;
//			double averageFitness = 0;
//			double standardDeviation = 0;
//			
//			for (Genotype<Double> genotype : tagModel.getOldPopulation().getGenotypes()) {
//				Double[] chromosome = genotype.getChromosome();
//				averageI += chromosome[0];
//				averageP += chromosome[1];
//				averageQ += chromosome[2];
//				averageFitness += genotype.getEvaluation();
//			}
//			averageI /= numberOfAgents;
//			averageP /= numberOfAgents;
//			averageQ /= numberOfAgents;
//			averageFitness /= numberOfAgents;
//			for (Genotype<Double> genotype : tagModel.getOldPopulation().getGenotypes()) {
//				standardDeviation += Math.pow(averageFitness - genotype.getEvaluation(), 2);
//			}
//			standardDeviation /= numberOfAgents-1;
//			dataFrame.addRow("Tag Trials = "+params.numberOfTagTrials, generation, averageI, averageP, averageQ, averageFitness, standardDeviation);
//		}
			
		dataFrame.writeRows();

	}
	
	
//	public static void varingMutationRateAndSpread() {
//		
//		PrintWriter writer = initwriter("MutationRate.csv");
//		
//		Parameters params = new Parameters();
//		params.numberOfGenerations = 5000;
//		params.numberOfTagTrials = 0;
//		params.numberOfGames = 4;
//		params.sizeOfPlayerGroup = 10;	
//		params.mutationProbabillity = 0.1;
//		params.mutationSpread = 0.5;
//
//		int numberOfAgents = 400;
//		int runs = 20;
//
//		for (double mutationSpread = 0.01; mutationSpread < 0.5; mutationSpread += 0.1) {
//			params.mutationSpread = mutationSpread;
//			for (double mutationRate = 0.01; mutationRate < 0.25; mutationRate += 0.05) {
//				params.mutationProbabillity = mutationRate;
//				batch(runs, params, numberOfAgents, writer);	
//				
//			}		
//		}
//		System.out.println("Done.");
//	}
	
	public static void flagshipBatch() {
	
		DataFrame dataFrame = new DataFrame();
		writeBatchHeaders(dataFrame,"Populations.csv");
			
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;

		int numberOfAgents = 400;
		int numberOfGenerationsToBeRun = 1;	
		int runs = 20;
		
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);	
	}

	public static void varingMutationRateBatch() {
		
		DataFrame dataFrame = new DataFrame();
		writeBatchHeaders(dataFrame,"MutationRate.csv");
		
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationSpread = 0.5;
		
		int numberOfAgents = 400;
		int numberOfGenerationsToBeRun = 1;	
		int runs = 20;
		
		params.mutationProbabillity = 0.01;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationProbabillity = 0.05;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationProbabillity = 0.1;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationProbabillity = 0.15;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
	
		params.mutationProbabillity = 0.2;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationProbabillity = 0.25;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
	}
	
	public static void varingMutationSpreadBatch() {
		
		DataFrame dataFrame = new DataFrame();
		writeBatchHeaders(dataFrame,"MutationSpread.csv");
		
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.1;

		int numberOfAgents = 400;
		int numberOfGenerationsToBeRun = 1;	
		int runs = 20;
		
		params.mutationSpread = 0.01;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationSpread = 0.025;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationSpread = 0.05;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationSpread = 0.1;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
	
		params.mutationSpread = 0.3;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationSpread = 0.5;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.mutationSpread = 0.6;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
	}
	
	public static void varingPlayerMovesBatch() {
		
		DataFrame dataFrame = new DataFrame();
		writeBatchHeaders(dataFrame,"PlayerMoves.csv");
		
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.numberOfTagTrials = 0;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;

		int numberOfAgents = 400;
		int numberOfGenerationsToBeRun = 1;	
		int runs = 20;
		
		params.numberOfGames = 3;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);

		params.numberOfGames = 4;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.numberOfGames = 5;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);

		params.numberOfGames = 6;
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
	}
	
	public static void varingPlayerGroupBatch() {
		
		DataFrame dataFrame = new DataFrame();
		writeBatchHeaders(dataFrame,"PlayerGroup.csv");
		
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;

		int numberOfAgents = 400;
		int numberOfGenerationsToBeRun = 1;	
		int runs = 20;

		params.sizeOfPlayerGroup = 1;	
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);

		params.sizeOfPlayerGroup = 5;	
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
		params.sizeOfPlayerGroup = 10;	
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);

		params.sizeOfPlayerGroup = 15;	
		batch(runs, params, numberOfAgents, dataFrame, numberOfGenerationsToBeRun);
		
	}

	protected static void writeBatchHeaders(DataFrame dataFrame, String filename) {
		dataFrame.initWriter(filename);
		dataFrame.addColumns(new String[]{
				"run", "NumberOfAgents", "NumberOfGenerations",
				"SizeOfPlayerGroup", "NumberOfTagTrials", "NumberOfGames",
				"ProbabilityOfSelectingFittest",
				"MutationProbability", "MutationSpread",
				"PopulationAverageEvaluation", "Generations to 2.3",
				"Generations Over 2.3", "Generations Under 1.7"
		});
		dataFrame.writeHeaders();
	}

	protected static void batch(int numberOfRuns, Parameters params, int numberOfAgents, DataFrame dataFrame, int numberOfGenerationsToBeRun) {

		for (int runNumber = 1; runNumber <= numberOfRuns; runNumber++ ) {
			
			double populationAverageEvaluation = 0;
			int generationsTo23 = -1;
			int generationsOver23 = 0;
			int generationsUnder17 = 0;
			Randomness.setSeed(runNumber);
			TagIPDModel model = new TagIPDModel(params, numberOfAgents);	
			// for each generation to be run
			for (int generation = 0; generation <= params.numberOfGenerations; generation += numberOfGenerationsToBeRun) {	
				model.run(numberOfGenerationsToBeRun);
				double averageEvaluation = model.getAverageEvaluation();
				populationAverageEvaluation += averageEvaluation;
				if (generationsTo23 == -1 && averageEvaluation > 2.3) {
					generationsTo23 = generation;
				} else if (averageEvaluation > 2.3) {
					generationsOver23++;
				} else if (averageEvaluation < 1.7) {
					generationsUnder17++;
				}
			}
			populationAverageEvaluation /= params.numberOfGenerations;
			
			dataFrame.addRow(runNumber, numberOfAgents, 
					params.numberOfGenerations, 
					params.sizeOfPlayerGroup, params.numberOfTagTrials, params.numberOfGames,
					params.probabilityOfSelectingFittest, params.mutationProbabillity, params.mutationSpread,
					populationAverageEvaluation, generationsTo23, generationsOver23, generationsUnder17);

			dataFrame.writeLastRow();
		}
	}

}
