package ie.nix.tagIPD;

import ie.nix.ea.Agent;
import ie.nix.ea.EvolutionaryAlgorithm;
import ie.nix.ea.Population;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.operators.AverageEvaluation;
import ie.nix.ea.operators.BinaryTournamentSelection;
import ie.nix.ea.operators.ShiftMutation;
import ie.nix.ea.populations.PopulationOfDoubles;
import ie.nix.pd.GroupIteratedPrisonersDilemma;
import ie.nix.pd.PrisonersDilemma.Payoffs;
import ie.nix.util.Randomness;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TagIPDModel extends EvolutionaryAlgorithm<Double> {

	public static class Parameters {
		
		// The number of generations in the run.
		public int numberOfGenerations = 5000;
		// The max number attempts a player has to find an acceptable playmate. 
		public int numberOfTagTrials = 5;
		
		// Group Iterated Prisoners Dilemma	
		// The number of plays for each game (No)
		public int numberOfGames = 4;
		// Size of the player group
		public int sizeOfPlayerGroup = 10;
		// The cost of searching multiple tags for a player.
		public double searchCost = 0.02;
		
		// Evolutionary Algorithm
		// The probability that the fittest is selected in a Binary Tournament selection
		public double probabilityOfSelectingFittest = 0.9;
		// Probability from 0 to 1 that mutation will occur.
		public double mutationProbabillity = 0.1;
		// The standard deviation of the Gaussian mutation size, or the mutation spread. 
		public double mutationSpread = 0.5;
		
	}
	
	public static class PlayerSearchCost {
		
		public TagIPDPlayer player;
		public double searchCost;
		
		public PlayerSearchCost(TagIPDPlayer player, double searchCost) {
			this.player = player;
			this.searchCost = searchCost;
		}
		
	}

	public static class TagIPDShiftMutation extends ShiftMutation {

		public TagIPDShiftMutation(double mutationProbabillity, double mutationSpread) {
			super(mutationProbabillity, mutationSpread);
		}

		@Override
		public Population<Double> operate(Population<Double> population) {
			Population<Double> mutatedPopulation = super.operate(population);
			// We need to make sure the IPD genes clamps between 0.0 and 1.0, and that the 
			// Tag gene loops, as the tag space is a circle. 
			
			List<Genotype<Double>> genotypes = population.getGenotypes();		
			for (Genotype<Double> genotype : genotypes) {
				Double[] chromosome = genotype.getChromosome();
				// Clamp the IPD genes
				for (int gene = 0; gene < 3; gene++) {	
					chromosome[gene] = clampValue(chromosome[gene]);		
				}
				// Loop the tag gene
				chromosome[3] = loopValue(chromosome[3]);		
			}
			return mutatedPopulation;
		}
		
	}
	
	// Thie is a version of IteratedPrisionersDilemma
	protected class TagIPD extends GroupIteratedPrisonersDilemma {

		public TagIPD(Payoffs payoffs, int numberOfGames) {
			super(payoffs, numberOfGames);
		}

		public void playWithSearchCost(Player playerA, List<PlayerSearchCost> playerBs) {
			// Play all the PlayersB
			for (PlayerSearchCost playerSearchCost : playerBs) {			
				play(playerA, playerSearchCost.player);
				// The last plays are null for the first round
				Play lastPlayA = null;
				Play lastPlayB = null;
				
				double playerAScore = 0;
				double playerBScore = 0;		
				int gameNumber = params.numberOfGames;	
				
				while (gameNumber > 0) {
					// Play a game
					
					Play playA = playerA.play(lastPlayB);
					Play playB = playerSearchCost.player.play(lastPlayA);
					
					switch (playA) {
						case Cooperate:		
							switch (playB) {
								case Cooperate:
									playerAScore += payoffs.reward;
									playerBScore += payoffs.reward;
									break;
								case Defect:
									playerAScore += payoffs.sucker;
									playerBScore += payoffs.temptation;
									break;
							}
							break;
						case Defect:
							switch (playB) {
								case Cooperate:		
									playerAScore += payoffs.temptation;
									playerBScore += payoffs.sucker;		
									break;
								case Defect:
									playerAScore += payoffs.punishment;
									playerBScore += payoffs.punishment;
									break;
							}
							break;
					}	
					
	
					// Set up the last plays for the next round
					lastPlayA = playA;
					lastPlayB = playB;
					
					gameNumber--;
				}
	
				// Discount playerAs score based on the search cost.
				playerA.playScore(playerAScore*playerSearchCost.searchCost);
				playerSearchCost.player.playScore(playerBScore);
			}
		}
	}
	
	protected Parameters params;

	// The GIPD kernel
	protected TagIPD tipd;	
	
	// The Genetic Operators
	protected AverageEvaluation averageEvaluation;
	protected BinaryTournamentSelection selection;
	protected TagIPDShiftMutation mutation;
	
	public TagIPDModel(Parameters params, int numberOfAgents) {		
		super(4, numberOfAgents);		
		initTagIPDModel(params);
	}
	
	protected TagIPDModel(Parameters params, Double[][] preCannedPopulation) {		
		super(4, preCannedPopulation);		
		initTagIPDModel(params);
		
	}
		
	@SuppressWarnings("unchecked")
	protected void initTagIPDModel(Parameters params) {	
		this.params = params;
	
		// Initialise the Group Iterated Prisioner's Dilemma kernel
		tipd = new TagIPD(new Payoffs(3, 0, 5, 1), params.numberOfGames);
		
		// Set up the EA workflow.
		// No fitness filters we use the evaluations just as they are.
		averageEvaluation = new AverageEvaluation();
		// Binary Tournament Selection operator
		selection = new BinaryTournamentSelection(params.probabilityOfSelectingFittest);
		// Shift Mutation operator
		mutation = new TagIPDShiftMutation(params.mutationProbabillity, params.mutationSpread);

		// Add operators
		addGeneticOperator(averageEvaluation);
		addGeneticOperator(selection);
		addGeneticOperator(mutation);
		
	}

	public void run() {	
		run(params.numberOfGenerations);
	}
	
	public void run(int numberOfGenerationcToRun) {		
		
		// for each generation to be run
		for (int g = 0; g < numberOfGenerationcToRun; g++) {		
			// Play the Game
			play();
			// Evolve the population
			evaluateAndEvolve();
		}		
	}

	protected void play() {
		// for each player
		for (Agent<Double> agent : getAgents()) {
			TagIPDPlayer player = (TagIPDPlayer)agent;
			// Select the other agents to play
			List<PlayerSearchCost> otherPlayers = selectPlayGroup(player);
			// play the GIPD
			tipd.playWithSearchCost(player, otherPlayers);
		}
	}

	protected double getAverageEvaluation() {
		return averageEvaluation.getAverageEvaluation();
	}

	protected List<Agent<Double>> initialiseAgents(int numberOfAgents) {
		List<Agent<Double>> agents = new ArrayList<Agent<Double>>();
		for (int a = 0; a < numberOfAgents; a++) {
			agents.add(new TagIPDPlayer());
		}
		return agents;
		
	}
	
	protected List<Agent<Double>> initialiseAgents(TagIPDPlayer[] tagIPDPlayers) {
		List<Agent<Double>> agents = new ArrayList<Agent<Double>>(tagIPDPlayers.length);
		agents.addAll(Arrays.asList(tagIPDPlayers));
		return agents;
	
	}

	protected Population<Double> initPopulation() {
		return new PopulationOfDoubles(getLenghtOfGenotype());
		
	}

	protected List<PlayerSearchCost> selectPlayGroup(TagIPDPlayer player) {
		
//		HashMap<Player, Double> playerSearchCosts = new HashMap<>(sizeOfPlayerGroup);
		ArrayList<PlayerSearchCost> playerSearchCosts = new ArrayList<PlayerSearchCost>(params.sizeOfPlayerGroup);
		
		// For the number of players we need in our player group.
		for (int p = 0; p < params.sizeOfPlayerGroup; p++) {
			// Select random other player
			TagIPDPlayer candidatePlayer = selectRandomOtherPlayer(player);
			// We see if the candidate player is suitable, if not we keep looking for 
			// a number to tag trials, then we stuck with the last candidate.
			int tagTrial = 0;
			while ((tagTrial < params.numberOfTagTrials) && 
				   (!player.wantsToPlay(candidatePlayer) || !(candidatePlayer.wantsToPlay(player)))) {
				candidatePlayer = selectRandomOtherPlayer(player);
				tagTrial++;
			}
			Double searchCost = 1 - (tagTrial*params.searchCost);
//			System.out.println("NDB::selectPlayGroup()~tagTrial="+tagTrial+", searchCost="+searchCost);
			// One way or another we have a candidate player. 
			playerSearchCosts.add(new PlayerSearchCost(candidatePlayer, searchCost));
		}
		// Then return our PlayerGroup
		return playerSearchCosts;
	}

	protected TagIPDPlayer selectRandomOtherPlayer(TagIPDPlayer player) {
		int playerIndex = getAgents().lastIndexOf(player);
		int randomOtherPlayerIndex;
		do {
			randomOtherPlayerIndex = Randomness.nextInt(getAgents().size());
		} while (randomOtherPlayerIndex == playerIndex);
		return (TagIPDPlayer)getAgents().get(randomOtherPlayerIndex);
	}

	protected int getNumberOfGenerations() {
		return params.numberOfGenerations;
	}

	protected int getNumberOfGames() {
		return params.numberOfGames;
	}

	protected double getMutationProbabillity() {
		return params.mutationProbabillity;
	}
	
}
