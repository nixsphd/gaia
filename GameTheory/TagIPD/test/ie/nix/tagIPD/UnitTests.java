package ie.nix.tagIPD;

import ie.nix.ea.AllTests;
import ie.nix.ea.Population;
import ie.nix.tagIPD.TagIPDModel.Parameters;
import ie.nix.tagIPD.TagIPDModel.PlayerSearchCost;
import ie.nix.tagIPD.TagIPDModel.TagIPDShiftMutation;
import ie.nix.util.Randomness;
import ie.nix.util.ToString;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UnitTests {
	
	@Before
	public void setUp() throws Exception {
		Randomness.setSeed(0);
	}
	
	@Test
	public void testCreateTagIPDModelRandom() {
		Parameters params = new Parameters();
		params.numberOfGenerations = 5;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 2;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		
		int numberOfAgents = 4;
		
		TagIPDModel model = new TagIPDModel(params, numberOfAgents);
		
		int actualNumberOfGenerations= model.getNumberOfGenerations();
		Assert.assertEquals(params.numberOfGenerations, actualNumberOfGenerations);
		
		int actualNumberOfAgents = model.getNumberOfAgents();
		Assert.assertEquals(numberOfAgents, actualNumberOfAgents);
		
		int actualNumberOfGames= model.getNumberOfGames();
		Assert.assertEquals(params.numberOfGames, actualNumberOfGames);

		double actualMutationProbabillity= model.getMutationProbabillity();
		Assert.assertEquals(params.mutationProbabillity, actualMutationProbabillity, 0.0000001);

		TagIPDPlayer[] expectedAgents = {
				new TagIPDPlayer(0.730967787376657,0.24053641567148587,0.6374174253501083,0.5504370051176339),
				new TagIPDPlayer(0.5975452777972018,0.3332183994766498,0.3851891847407185,0.984841540199809),
				new TagIPDPlayer(0.8791825178724801,0.9412491794821144,0.27495396603548483,0.12889715087377673),
				new TagIPDPlayer(0.14660165764651822,0.023238122483889456,0.5467397571984656,0.9644868606768501),
		};
		
		TagIPDPlayer[] actualAgents = model.getAgents().toArray(new TagIPDPlayer[numberOfAgents]);
		System.out.println("NDB::testCreateTagIPDModelRandom()~actualAgents=\n"+toString(actualAgents));	
		System.out.println("NDB::testCreateTagIPDModelRandom()~expectedAgents=\n"+toString(expectedAgents));		
		Assert.assertTrue(equals(expectedAgents, actualAgents));
		
	}
	
	@Test
	public void testCreateTagIPDModelPrecanned() {
		Parameters params = new Parameters();
		params.numberOfGenerations = 5;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 2;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		Double[][] preCannedPopulation = {		
				{1.0, 1.1, 1.2, 1.3},	
				{2.0, 2.1, 2.2, 2.3},	
				{3.0, 3.1, 3.2, 3.3},	
				{4.0, 4.1, 4.2, 4.3},
		};
		
		TagIPDModel model = new TagIPDModel(params, preCannedPopulation);
		
		int actualNumberOfGenerations= model.getNumberOfGenerations();
		Assert.assertEquals(params.numberOfGenerations, actualNumberOfGenerations);
		
		int actualNumberOfAgents = model.getNumberOfAgents();
		Assert.assertEquals(preCannedPopulation.length, actualNumberOfAgents);
		
		int actualNumberOfGames= model.getNumberOfGames();
		Assert.assertEquals(params.numberOfGames, actualNumberOfGames);

		double actualMutationProbabillity= model.getMutationProbabillity();
		Assert.assertEquals(params.mutationProbabillity, actualMutationProbabillity, 0.0000001);

		TagIPDPlayer[] expectedAgents = {
				new TagIPDPlayer(1.0, 1.1, 1.2, 1.3),
				new TagIPDPlayer(2.0, 2.1, 2.2, 2.3),
				new TagIPDPlayer(3.0, 3.1, 3.2, 3.3),
				new TagIPDPlayer(4.0, 4.1, 4.2, 4.3),
		};
		
		TagIPDPlayer[] actualAgents = model.getAgents().toArray(new TagIPDPlayer[preCannedPopulation.length]);
		System.out.println("NDB::testCreateTagIPDModelRandom()~actualAgents=\n"+toString(actualAgents));	
		System.out.println("NDB::testCreateTagIPDModelRandom()~expectedAgents=\n"+toString(expectedAgents));		
		Assert.assertTrue(equals(expectedAgents, actualAgents));
		
	}

	@Test 
	public void testWantsToPlayYes1() {
		TagIPDPlayer player = new TagIPDPlayer(0,0,0,1);
		TagIPDPlayer candidatePlayer = new TagIPDPlayer(0,0,0,1);
		boolean expectedWantsToPlay = true;
		testWantsToPlay(player, candidatePlayer, expectedWantsToPlay);
	}
	
	@Test 
	public void testWantsToPlayNo() {
		TagIPDPlayer player = new TagIPDPlayer(0,0,0,1);
		TagIPDPlayer candidatePlayer = new TagIPDPlayer(0,0,0,0);
		boolean expectedWantsToPlay = false;
		testWantsToPlay(player, candidatePlayer, expectedWantsToPlay);
	}
	
	@Test 
	public void testWantsToPlayYesHalf() {
		TagIPDPlayer player = new TagIPDPlayer(0,0,0,0.5);
		TagIPDPlayer candidatePlayer = new TagIPDPlayer(0,0,0,0.5);
		boolean expectedWantsToPlay = true;
		testWantsToPlay(player, candidatePlayer, expectedWantsToPlay);
	}
	
	@Test 
	public void testWantsToPlayMaybe() {
		TagIPDPlayer player = new TagIPDPlayer(0,0,0,0.25);
		TagIPDPlayer candidatePlayer = new TagIPDPlayer(0,0,0,0.75);
		boolean expectedWantsToPlay = false;
		testWantsToPlay(player, candidatePlayer, expectedWantsToPlay);
	}
	
	@Test 
	public void testWantsToPlayMaybeOtherWayAround() {
		TagIPDPlayer player = new TagIPDPlayer(0,0,0,0.75);
		TagIPDPlayer candidatePlayer = new TagIPDPlayer(0,0,0,0.25);
		boolean expectedWantsToPlay = false;
		testWantsToPlay(player, candidatePlayer, expectedWantsToPlay);
	}
		
	public void testWantsToPlay(TagIPDPlayer player, TagIPDPlayer candidatePlayer, 
			boolean expectedWantsToPlay) {
		boolean actualWantsToPlay = player.wantsToPlay(candidatePlayer);
		System.out.println("NDB::testWantsToPlay()~actualWantsToPlay="+actualWantsToPlay);	
		System.out.println("NDB::testWantsToPlay()~expectedWantsToPlay="+expectedWantsToPlay);				
		Assert.assertEquals(expectedWantsToPlay, actualWantsToPlay);
	}
	
	@Test
	public void testSelectRandomOtherPlayer() {
		Parameters params = new Parameters();
		params.numberOfGenerations = 5;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 2;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		Double[][] preCannedPopulation = {		
				{1.0,1.0,1.0,1.0},
				{1.0,1.0,1.0,2.0},
				{1.0,1.0,1.0,3.0},
				{1.0,1.0,1.0,4.0},
				{1.0,1.0,1.0,5.0},
		};
		
		TagIPDModel model = new TagIPDModel(params, preCannedPopulation);
	
		TagIPDPlayer player = (TagIPDPlayer)model.getAgents().get(0);
		TagIPDPlayer expectedSelectedPlayer = (TagIPDPlayer)model.getAgents().get(3);		
		TagIPDPlayer actualSelectedPlayer = model.selectRandomOtherPlayer(player);
		System.out.println("NDB::testWantsToPlay()~actualSelectedPlayer="+actualSelectedPlayer);	
		System.out.println("NDB::testWantsToPlay()~expectedSelectedPlayer="+expectedSelectedPlayer);				
		Assert.assertEquals(expectedSelectedPlayer, actualSelectedPlayer);
		
	}

	@Test
	public void testSelectPlayGroup() {
		Parameters params = new Parameters();
		params.numberOfGenerations = 5;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 2;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		Double[][] preCannedPopulation = {		
				{1.0,1.0,1.0,1.0},
				{2.0,1.0,1.0,0.0},
				{3.0,1.0,1.0,0.0},
				{4.0,1.0,1.0,0.0},
				{5.0,1.0,1.0,0.0},
				{6.0,1.0,1.0,0.0},
				{7.0,1.0,1.0,0.0},
				{8.0,1.0,1.0,1.0},
				{9.0,1.0,1.0,1.0},
			};

		PlayerSearchCost[] extetedPlayGroup = {	
				new PlayerSearchCost(new TagIPDPlayer(9.0,1.0,1.0,1.0), 0.96),
				new PlayerSearchCost(new TagIPDPlayer(9.0,1.0,1.0,1.0), 1.0),
			};
		testSelectPlayGroup(preCannedPopulation, params, extetedPlayGroup);

	}
	
	@Test
	public void testSelectPlayGroupForced() {
		Parameters params = new Parameters();
		params.numberOfGenerations = 5;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 1;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		Double[][] preCannedPopulation = {			
				{1.0,1.0,1.0,1.0},
				{2.0,1.0,1.0,0.0},
				{3.0,1.0,1.0,0.0},
				{4.0,1.0,1.0,0.0},
				{5.0,1.0,1.0,0.0},
				{6.0,1.0,1.0,0.0},
				{7.0,1.0,1.0,0.0},
				{8.0,1.0,1.0,0.0},
				{9.0,1.0,1.0,0.0},
				{10.0,1.0,1.0,0.0},
				{11.0,1.0,1.0,0.0},
				{12.0,1.0,1.0,0.0},
				{13.0,1.0,1.0,0.0},
				{14.0,1.0,1.0,1.0},
			};
		PlayerSearchCost[] extetedPlayGroup = {	
				new PlayerSearchCost(new TagIPDPlayer(11.0,1.0,1.0,0.0), 0.9),
			};
		testSelectPlayGroup(preCannedPopulation, params, extetedPlayGroup);

	}
	
	public void testSelectPlayGroup(Double[][] preCannedPopulation, Parameters params, PlayerSearchCost[] extetedPlayGroup) {				
		TagIPDModel model = new TagIPDModel(params, preCannedPopulation);	
		TagIPDPlayer player = (TagIPDPlayer)model.getAgents().get(0);
		
		PlayerSearchCost[] actualPlayGroup = model.selectPlayGroup(player).toArray(new PlayerSearchCost[extetedPlayGroup.length]);
		System.out.println("NDB::testSelectPlayGroupForced()~actualPlayGroup=\n"+toString(actualPlayGroup));	
		System.out.println("NDB::testSelectPlayGroupForced()~extetedPlayGroup=\n"+toString(extetedPlayGroup));		
		Assert.assertTrue(equals(extetedPlayGroup, actualPlayGroup));
		
	}
	
	@Test
	public void testMutation() {		
		Parameters params = new Parameters();
		params.mutationProbabillity = 1;
		params.mutationSpread = 0.05;
		Double[][] preCannedPopulation = {		
				{0.1,0.1,0.1,0.1},
				{0.2,0.2,0.2,0.2},
				{0.3,0.3,0.3,0.3},
				{0.4,0.4,0.4,0.4},
				{0.5,0.5,0.5,0.5},
				{0.6,0.6,0.6,0.6},
				{0.7,0.7,0.7,0.7},
				{0.8,0.8,0.8,0.8},
				{0.9,0.9,0.9,0.9},
		};
		Double[][] expectedPopulation = {
				{0.03552134380457583,0.13414926586842907,0.02168470172070372,0.04608851807090696},
				{0.20863441657669995,0.19559627236369162,0.1794085316804943,0.20201870215510484},
				{0.38844535777514544,0.23695651886146543,0.29564643111920236,0.3839677351643073},
				{0.4619239385327954,0.4048786698916117,0.4498148074595237,0.4481735354004566},
				{0.4381589448049865,0.43422173195603436,0.5494230142294627,0.5152130149402866},
				{0.6369274381370232,0.6521022211509353,0.5926139706226624,0.598391366510412},
				{0.7586377997073812,0.7463659743985543,0.6937439758729862,0.7582215809810778},
				{0.8123295658477067,0.7961405188840559,0.7756471687667871,0.7991554821637963},
				{0.9790995741396327,0.900413285226016,0.9031423129869538,0.9416357297435269}
		};
		testMutation(preCannedPopulation, params, expectedPopulation);

	}
	
	@Test
	public void testMutationWithClamping() {		
		Parameters params = new Parameters();
		params.mutationProbabillity = 1;
		params.mutationSpread = 1.0;
		Double[][] preCannedPopulation = {		
				{0.0,0.0,0.0,0.0},
				{1.0,1.0,1.0,1.0},
		};
		Double[][] expectedPopulation = {
				{0.0,0.6829853173685814,0.0,0.9217703614181392},
				{1.0,0.9119254472738323,0.5881706336098858,0.04037404310209647}
		};
		testMutation(preCannedPopulation, params, expectedPopulation);

	}
	
	public void testMutation(Double[][] preCannedPopulation, Parameters params, Double[][] expectedPopulation) {	
		TagIPDModel model = new TagIPDModel(params, preCannedPopulation);
		
		TagIPDShiftMutation tagIPDShiftMutation = new TagIPDShiftMutation(params.mutationProbabillity, params.mutationSpread);
		tagIPDShiftMutation.operate(model.getPopulation());
		
		Population<Double> actualPopulation = model.getPopulation();
		
		System.out.println("NDB::testMutation()~actualPopulation=\n"+AllTests.toString(actualPopulation));	
		System.out.println("NDB::testMutation()~expectedPopulation=\n"+ToString.toString(expectedPopulation));		
		Assert.assertTrue(AllTests.equals(expectedPopulation, actualPopulation));
		
	}
	
	@Test
	public void testEvolve() {		
		Parameters params = new Parameters();
		params.numberOfGenerations = 1; // Can only evolve for one generation
		params.numberOfTagTrials = 5;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 1;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		Double[][] preCannedPopulation = {		
				{0.1,1.0,1.0,1.0},
				{0.1,1.0,1.0,0.0},
				{0.3,1.0,1.0,0.0},
				{0.4,1.0,1.0,0.0},
				{0.5,1.0,1.0,0.0},
				{0.6,1.0,1.0,0.0},
				{0.7,1.0,1.0,0.0},
				{0.8,1.0,1.0,0.0},
				{0.9,1.0,1.0,0.0},
		};
		TagIPDPlayer[] expectedAgents = {
				new TagIPDPlayer(0.8,1.0,1.0,0.0),
				new TagIPDPlayer(0.3,1.0,1.0,0.0),
				new TagIPDPlayer(0.3,1.0,1.0,0.22055189551754437),
				new TagIPDPlayer(0.3,0.6340601344200056,0.3815894480498655,0.0),
				new TagIPDPlayer(0.5,1.0,1.0,0.0),
				new TagIPDPlayer(0.1,1.0,0.34221731956034385,1.0),
				new TagIPDPlayer(0.1,1.0,1.0,0.0),
				new TagIPDPlayer(0.1,0.8422738392291489,1.0,1.0),
				new TagIPDPlayer(0.3,1.0,1.0,0.0),
		};
		testEvolve(preCannedPopulation, params, expectedAgents);

	}
	
	public void testEvolve(Double[][] preCannedPopulation, Parameters params, TagIPDPlayer[] expectedAgents) {	
		TagIPDModel model = new TagIPDModel(params, preCannedPopulation);
		model.evolve();		
		TagIPDPlayer[] actualAgents = model.getAgents().toArray(new TagIPDPlayer[model.getNumberOfAgents()]);
		
		System.out.println("NDB::testEvolve()~actualAgents=\n"+toString(actualAgents));	
		System.out.println("NDB::testEvolve()~expectedAgents=\n"+toString(expectedAgents));		
		Assert.assertTrue(equals(expectedAgents, actualAgents));
		
	}
	
	@Test
	public void testRun() {		
		Parameters params = new Parameters();
		params.numberOfGenerations = 1; // Can only evolve for one generation
		params.numberOfTagTrials = 5;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 1;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		Double[][] preCannedPopulation = {		
				{0.1,1.0,1.0,1.0},
				{0.1,1.0,1.0,0.0},
				{0.3,1.0,1.0,0.0},
				{0.4,1.0,1.0,0.0},
				{0.5,1.0,1.0,0.0},
				{0.6,1.0,1.0,0.0},
				{0.7,1.0,1.0,0.0},
				{0.8,1.0,1.0,0.0},
				{0.9,1.0,1.0,0.0},
		};
		TagIPDPlayer[] expectedAgents = {
				new TagIPDPlayer(0.8,1.0,1.0,0.729351987898851),
				new TagIPDPlayer(0.4,1.0,1.0,0.0),
				new TagIPDPlayer(0.8,1.0,1.0,0.0),
				new TagIPDPlayer(0.5,1.0,1.0,0.0),
				new TagIPDPlayer(0.5780046690990288,1.0,1.0,0.0),
				new TagIPDPlayer(0.8,1.0,1.0,0.8689136180180982),
				new TagIPDPlayer(0.1,1.0,1.0,1.0),
				new TagIPDPlayer(0.4,1.0,1.0,0.0),
				new TagIPDPlayer(0.4,1.0,1.0,0.0),
		};
		testRun(preCannedPopulation, params, expectedAgents);

	}
	
	public void testRun(Double[][] preCannedPopulation, Parameters params, TagIPDPlayer[] expectedAgents) {	
		TagIPDModel model = new TagIPDModel(params, preCannedPopulation);
		model.run();		
		TagIPDPlayer[] actualAgents = model.getAgents().toArray(new TagIPDPlayer[model.getNumberOfAgents()]);
		
		System.out.println("NDB::testEvolve()~actualAgents=\n"+toString(actualAgents));	
		System.out.println("NDB::testEvolve()~expectedAgents=\n"+toString(expectedAgents));		
		Assert.assertTrue(equals(expectedAgents, actualAgents));
		
	}
	
	private String toString(PlayerSearchCost[] playerSearcgCosts) {
		StringBuilder string = new StringBuilder("{\n");
		for (int a = 0; a < playerSearcgCosts.length; a++) {
			string.append("new PlayerSearchCost("+playerSearcgCosts[a].player+", "+playerSearcgCosts[a].searchCost+"),\n");
		}
		string.replace(string.length()-1, string.length(),"\n};");
		return string.toString();
	}

	private String toString(TagIPDPlayer[] actualAgents) {
		StringBuilder string = new StringBuilder("{\n");
		for (int a = 0; a < actualAgents.length; a++) {
			string.append(actualAgents[a]+",\n");
		}
		string.replace(string.length()-1, string.length(),"\n};");
		return string.toString();
	}

	protected boolean equals(TagIPDPlayer[] expectedAgents, TagIPDPlayer[] actualAgents) {	
		boolean equals = (expectedAgents.length == actualAgents.length);
		int agentNumber = 0;
		while (equals && (agentNumber < expectedAgents.length)) {
			equals = (expectedAgents[agentNumber].equals(actualAgents[agentNumber]));
			agentNumber++;
		}
		return equals;
	}
	
	protected boolean equals(PlayerSearchCost[] expetedPlayGroup,
			PlayerSearchCost[] actualPlayGroup) {
		boolean equals = (expetedPlayGroup.length == actualPlayGroup.length);
		int agentNumber = 0;
		while (equals && (agentNumber < expetedPlayGroup.length)) {
			equals = ((expetedPlayGroup[agentNumber].player.equals(actualPlayGroup[agentNumber].player)) &&
					  (expetedPlayGroup[agentNumber].searchCost == actualPlayGroup[agentNumber].searchCost));
			agentNumber++;
		}
		return equals;
	}	

}
