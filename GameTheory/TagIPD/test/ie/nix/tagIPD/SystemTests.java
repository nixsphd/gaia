package ie.nix.tagIPD;

import ie.nix.tagIPD.TagIPDModel.Parameters;
import ie.nix.util.Randomness;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SystemTests {

	@Before
	public void setUp() throws Exception {
		Randomness.setSeed(0);
	}
	
	@Test
	public void testAllCs() {		
		Double[][] preCannedPopulation = {		
				{1.0, 1.0, 1.0, 0.0},	
				{1.0, 1.0, 1.0, 0.0},	
				{1.0, 1.0, 1.0, 1.0},	
				{1.0, 1.0, 1.0, 1.0},	
		};
		
		Parameters params = new Parameters();
		params.numberOfGenerations = 10;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 1;
		params.sizeOfPlayerGroup = 2;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		
		test(params, preCannedPopulation);
	}
	
	@Test
	public void testAllDs() {		
		Parameters params = new Parameters();
		params.numberOfGenerations = 10;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 1;
		params.sizeOfPlayerGroup = 2;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		Double[][] preCannedPopulation = {		
				{0.0, 0.0, 0.0, 0.0},	
				{0.0, 0.0, 0.0, 0.0},	
				{0.0, 0.0, 0.0, 1.0},	
				{0.0, 0.0, 0.0, 1.0},	
		};
		
		test(params, preCannedPopulation);
	}
	
	@Test
	public void testAllDsMultipleGames() {		
		Parameters params = new Parameters();
		params.numberOfGenerations = 10;
		params.numberOfTagTrials = 5;
		params.numberOfGames = 10;
		params.sizeOfPlayerGroup = 2;	
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		Double[][] preCannedPopulation = {		
				{0.0, 0.0, 0.0, 0.0},	
				{0.0, 0.0, 0.0, 0.0},	
				{0.0, 0.0, 0.0, 1.0},	
				{0.0, 0.0, 0.0, 1.0},	
		};
		
		test(params, preCannedPopulation);
	}
		
	public void test(Parameters params, Double[][] preCannedPopulation) {
		int runNumber = 1;
		TagIPDModel model = new TagIPDModel(params, preCannedPopulation);
		
		model.run(runNumber);
	}
	
	@Test
	public void testOneGeneration() {	
		int numberOfAgents = 400;
		Parameters params = new Parameters();
		params.numberOfGenerations = 1;
		params.sizeOfPlayerGroup = 10;	
		params.numberOfGames = 4;
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		params.numberOfTagTrials = 5;		
		params.searchCost = 0.0;
		double expectedAverageEvaluation = 2.25;
		
		testRandom(params, numberOfAgents, expectedAverageEvaluation);
	}
	
	@Test
	public void testTwoGeneration() {	
		int numberOfAgents = 400;
		Parameters params = new Parameters();
		params.numberOfGenerations = 35;
		params.sizeOfPlayerGroup = 10;	
		params.numberOfGames = 4;
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;
		params.numberOfTagTrials = 5;		
		params.searchCost = 0.0;
		double expectedAverageEvaluation = 2.25;
		
		testRandom(params, numberOfAgents, expectedAverageEvaluation);
	}
	
	
	@Test
	public void testRandomWithTags() {	
		int numberOfAgents = 400;
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.sizeOfPlayerGroup = 10;	
		params.numberOfGames = 4;
		params.probabilityOfSelectingFittest = 0.9;
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;		
		params.numberOfTagTrials = 5;
		double expectedAverageEvaluation = 2.26;
		
		testRandom(params, numberOfAgents, expectedAverageEvaluation);
	}
	
	@Test
	public void testRandomWithNoTags() {	
		int numberOfAgents = 400;
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.sizeOfPlayerGroup = 10;	
		params.numberOfGames = 4;
		params.mutationProbabillity = 0.1;
		params.mutationSpread = 0.5;		
		params.numberOfTagTrials = 0;
		double expectedAverageEvaluation = 1.73;
		
		testRandom(params, numberOfAgents, expectedAverageEvaluation);
	}
	
	@Test
	public void testMutationOff() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.01;
		params.mutationSpread = 0.01;		
		int numberOfAgents = 400;
		double expectedAverageEvaluation = 1.15;
		
		testRandom(params, numberOfAgents, expectedAverageEvaluation);
	}
	
	@Test
	public void testTagsSelectionMutationOff() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 5000;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.probabilityOfSelectingFittest = 0.5;
		params.mutationProbabillity = 0.0;
		params.mutationSpread = 0.0;		
		int numberOfAgents = 400;
		double expectedAverageEvaluation = 1.15;
		
		testRandom(params, numberOfAgents, expectedAverageEvaluation);
	}
	
	public void testRandom(Parameters params, int numberOfAgents, double expectedAverageEvaluation) {
		double actualAverageEvaluation = 0;
		int runs = 20;
		int runNumber = runs;
		while (runNumber > 0) {
			TagIPDModel model = new TagIPDModel(params, numberOfAgents);		
			model.run();
			actualAverageEvaluation += model.getAverageEvaluation();			
			runNumber--;
			System.out.println("NDB::testRandom()~run="+(runs-runNumber)+
					", actualAverageEvaluation="+(actualAverageEvaluation/(runs-runNumber)));	
			
		}		
		actualAverageEvaluation /= runs;		
		System.out.println("NDB::testRandom()~actualAverageEvaluation="+actualAverageEvaluation);	
		System.out.println("NDB::testRandom()~expectedAverageEvaluation="+expectedAverageEvaluation);
		Assert.assertEquals(expectedAverageEvaluation, actualAverageEvaluation, 0.01);
		
	}
	
	@Test
	public void testPreCannedAllCsNoMutation() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 500;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.0; 
		params.mutationSpread = 0.5;		
		int numberOfAgents = 400;
		Double[][] preCannedPopulationToClone = {
				{1.0, 1.0, 1.0, 0.0},
		};
		double expectedAverageEvaluation = 3.0;
		
		testPreCanned(params, numberOfAgents, preCannedPopulationToClone, expectedAverageEvaluation);
	}

	@Test
	public void testPreCannedAllDsNoMutation() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 500;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.0; 
		params.mutationSpread = 0.5;		
		int numberOfAgents = 400;
		Double[][] preCannedPopulationToClone = {
				{0.0, 0.0, 0.0, 0.0},
		};
		double expectedAverageEvaluation = 1.0;
		
		testPreCanned(params, numberOfAgents, preCannedPopulationToClone, expectedAverageEvaluation);
	}
	
	@Test
	public void testPreCanned5050NoMutation() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 500;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.0; 
		params.mutationSpread = 0.5;		
		int numberOfAgents = 400;
		Double[][] preCannedPopulationToClone = {
				{1.0, 1.0, 1.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
		};
		double expectedAverageEvaluation = 1.0;
		
		testPreCanned(params, numberOfAgents, preCannedPopulationToClone, expectedAverageEvaluation);
	}
	
	@Test
	public void testPreCannedT4TNoMutation() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 500;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.0; 
		params.mutationSpread = 0.5;		
		int numberOfAgents = 400;
		Double[][] preCannedPopulationToClone = {
				{1.0, 1.0, 0.0, 0.0},
		};
		double expectedAverageEvaluation = 3.0;
		
		testPreCanned(params, numberOfAgents, preCannedPopulationToClone, expectedAverageEvaluation);
	}
	
	@Test
	public void testPreCannedAllCs() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 500;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.1; 
		params.mutationSpread = 0.5;		
		int numberOfAgents = 400;
		Double[][] preCannedPopulationToClone = {
				{1.0, 1.0, 1.0, 0.0},
		};
		double expectedAverageEvaluation = 2.89;
		
		testPreCanned(params, numberOfAgents, preCannedPopulationToClone, expectedAverageEvaluation);
	}
	
	@Test
	public void testPreCannedAllDs() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 500;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.0005; 
		params.mutationSpread = 0.5;		
		int numberOfAgents = 400;
		Double[][] preCannedPopulationToClone = {
//				{0.6200061387040614, 0.5566294442370842, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
		};
		double expectedAverageEvaluation = 1.0;
		
		testPreCanned(params, numberOfAgents, preCannedPopulationToClone, expectedAverageEvaluation);
	}
	
	@Test
	public void testPreCannedT4TvD() {	
		Parameters params = new Parameters();
		params.numberOfGenerations = 500;
		params.numberOfTagTrials = 0;
		params.numberOfGames = 4;
		params.sizeOfPlayerGroup = 10;	
		params.mutationProbabillity = 0.005; 
		params.mutationSpread = 0.5;		
		int numberOfAgents = 400;
		Double[][] preCannedPopulationToClone = {
				{1.0, 1.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
				{0.0, 0.0, 0.0, 0.0},
		};
		double expectedAverageEvaluation = 3.0;
		
		testPreCanned(params, numberOfAgents, preCannedPopulationToClone, expectedAverageEvaluation);
	}

	public void testPreCanned(Parameters params, int numberOfAgents, Double[][] preCannedPopulationToClone, double expectedAverageEvaluation) {
		int lengthOfGenome = preCannedPopulationToClone[0].length;
		Double[][] preCannedPopulation = new Double[numberOfAgents][lengthOfGenome];
		for (int a = 0; a < numberOfAgents; a++) {
			System.arraycopy(preCannedPopulationToClone[a%preCannedPopulationToClone.length], 0, preCannedPopulation[a], 0, lengthOfGenome);
		}
		
		double actualAverageEvaluation = 0;
		int runs = 20;
		int runNumber = runs;
		while (runNumber > 0) {
			TagIPDModel model = new TagIPDModel(params, preCannedPopulation);		
			model.run();
			actualAverageEvaluation += model.getAverageEvaluation();
			System.out.println("NDB::testCreateTagIPDModelRandom()~runNumber="+runNumber+", actualAverageEvaluation="+model.getAverageEvaluation());	
			runNumber--;
			
		}		
		actualAverageEvaluation /= runs;		
		System.out.println("NDB::testCreateTagIPDModelRandom()~actualAverageEvaluation="+actualAverageEvaluation);	
		System.out.println("NDB::testCreateTagIPDModelRandom()~expectedAverageEvaluation="+expectedAverageEvaluation);
		Assert.assertEquals(expectedAverageEvaluation, actualAverageEvaluation, 0.01);
		
	}
}