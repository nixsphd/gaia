package ie.nix.pd;

import ie.nix.pd.IteratedPrisonersDilemma.Player;
import ie.nix.pd.PrisonersDilemma.Payoffs;
import ie.nix.util.Randomness;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GroupIteratedPrisonersDilemmaTests {

	protected GroupIteratedPrisonersDilemma gipd;
	
	@Before
	public void setUp() throws Exception {	
		Randomness.setSeed(1);
	}
	
	@Test
	public void testGroupPrisonersDilemmaCvsDDD() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Cooperator);
		Player[] playerBs = {
				new IPDPlayer(IteratedPrisonersDilemma.Defector),
				new IPDPlayer(IteratedPrisonersDilemma.Defector),
				new IPDPlayer(IteratedPrisonersDilemma.Defector)
		};
		int numberOfGames = 1;
		double expectedPlayerAScore = 3.0; 	              // 1 * 3 
		double expectedPlayerBScores[] = {4.0, 4.0, 4.0}; // 1 * 4
		
		testGroupIteratedPrisonersDilemmaCvsDDD(playerA, Arrays.asList(playerBs), numberOfGames, 
				expectedPlayerAScore, expectedPlayerBScores);
	}
	
	@Test
	public void testGroupIteratedPrisonersDilemmaCvsDDD() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Cooperator);
		Player[] playerBs = {
				new IPDPlayer(IteratedPrisonersDilemma.Defector),
				new IPDPlayer(IteratedPrisonersDilemma.Defector),
				new IPDPlayer(IteratedPrisonersDilemma.Defector)
		};
		int numberOfGames = 10;
		double expectedPlayerAScore = 30.0; 	          // 10 * 3 
		double expectedPlayerBScores[] = {40.0, 40.0, 40.0}; // 10 * 4
		
		testGroupIteratedPrisonersDilemmaCvsDDD(playerA, Arrays.asList(playerBs), numberOfGames, 
				expectedPlayerAScore, expectedPlayerBScores);
	}
	
	public void testGroupIteratedPrisonersDilemmaCvsDDD(IPDPlayer playerA, List<Player> playerBs, 
			int numberOfGames,	double expectedPlayerAScore, double[] expectedPlayerBScores) {

		gipd = new GroupIteratedPrisonersDilemma(new Payoffs(3, 1, 4, 2), numberOfGames);
		gipd.play(playerA, playerBs);
		
		double actualPlayerAScore = playerA.getScore();	
		System.out.println("NDB::testPrisonersDilemma()~actualPlayerAScore="+actualPlayerAScore);
		
		double[] actualPlayerBScores = new double[playerBs.size()];
		for (int p = 0; p < actualPlayerBScores.length; p++) {
			actualPlayerBScores[p] = ((IPDPlayer)playerBs.get(p)).getScore();	
		}
		System.out.println("NDB::testPrisonersDilemma()~actualPlayerBScores="+toString(actualPlayerBScores));

		Assert.assertEquals(expectedPlayerAScore, actualPlayerAScore, 0.0000001);
		Assert.assertArrayEquals(expectedPlayerBScores, actualPlayerBScores, 0.0000001);
		
	}

	public static String toString(double[] state) {
		StringBuilder string = new StringBuilder("{");
		for (int w = 0; w < state.length; w++) {
			string.append(Double.toString(state[w])+",");
		}
		string.replace(string.length()-1, string.length(),"},\n");
		return string.toString();
	}
}
