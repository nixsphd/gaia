package ie.nix.pd;

import ie.nix.pd.PrisonersDilemma.Payoffs;
import ie.nix.pd.PrisonersDilemma.Strategy;
import ie.nix.util.Randomness;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PrisonersDilemmaTests {

	protected PrisonersDilemma pd;
	
	@Before
	public void setUp() throws Exception {	
		pd = new PrisonersDilemma(new Payoffs(3, 1, 4, 2));
		Randomness.setSeed(1);
	}
	
	@Test
	public void testPrisonersDilemmaCC() {
		PDPlayer playerA = new PDPlayer(PrisonersDilemma.Cooperator);
		PDPlayer playerB = new PDPlayer(PrisonersDilemma.Cooperator);
		double expectedPlayerAScore = 3;
		double expectedPlayerBScore = 3;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaDD() {
		PDPlayer playerA = new PDPlayer(PrisonersDilemma.Defector);
		PDPlayer playerB = new PDPlayer(PrisonersDilemma.Defector);
		double expectedPlayerAScore = 2;
		double expectedPlayerBScore = 2;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaCD() {
		PDPlayer playerA = new PDPlayer(PrisonersDilemma.Cooperator);
		PDPlayer playerB = new PDPlayer(PrisonersDilemma.Defector);
		double expectedPlayerAScore = 1;
		double expectedPlayerBScore = 4;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaDC() {
		PDPlayer playerA = new PDPlayer(PrisonersDilemma.Defector);
		PDPlayer playerB = new PDPlayer(PrisonersDilemma.Cooperator);
		double expectedPlayerAScore = 4;
		double expectedPlayerBScore = 1;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}

	@Test
	public void testPrisonersDilemmaStochasticCC() {
		PDPlayer playerA = new PDPlayer(PrisonersDilemma.StochasticDefector);
		PDPlayer playerB = new PDPlayer(PrisonersDilemma.StochasticCooperator);
		double expectedPlayerAScore = 4;
		double expectedPlayerBScore = 1;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}

	@Test
	public void testPrisonersDilemmaStochasticDD() {
		PDPlayer playerA = new PDPlayer(PrisonersDilemma.StochasticDefector);
		PDPlayer playerB = new PDPlayer(PrisonersDilemma.StochasticDefector);
		double expectedPlayerAScore = 2;
		double expectedPlayerBScore = 2;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaStochasticCD() {
		PDPlayer playerA = new PDPlayer(PrisonersDilemma.StochasticCooperator);
		PDPlayer playerB = new PDPlayer(PrisonersDilemma.StochasticDefector);
		double expectedPlayerAScore = 1;
		double expectedPlayerBScore = 4;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaStochasticDC() {
		PDPlayer playerA = new PDPlayer(PrisonersDilemma.StochasticDefector);
		PDPlayer playerB = new PDPlayer(PrisonersDilemma.StochasticCooperator);
		double expectedPlayerAScore = 4;
		double expectedPlayerBScore = 1;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaStochasticCLDL() {
		PDPlayer playerA = new PDPlayer(new Strategy(0.75,true)); // Tendency to Cooperate
		PDPlayer playerB = new PDPlayer(new Strategy(0.25,true)); // Tendency to Defect
		double expectedPlayerAScore = 1;
		double expectedPlayerBScore = 4;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaStochastic5050() {
		PDPlayer playerA = new PDPlayer(new Strategy(0.5,true)); // Could go either way
		PDPlayer playerB = new PDPlayer(new Strategy(0.5,true)); // Could go either way
		double expectedPlayerAScore = 4;
		double expectedPlayerBScore = 1;
		
		testPrisonersDilemma(playerA, playerB, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	public void testPrisonersDilemma(PDPlayer playerA, PDPlayer playerB, 
			double expectedPlayerAScore, double expectedPlayerBScore) {
		
		pd.play(playerA, playerB);
		
		double actualPlayerAScore = playerA.getScore();	
		System.out.println("NDB::testPrisonersDilemma()~actualPlayerAScore="+actualPlayerAScore);
		double actualPlayerBScore = playerB.getScore();
		System.out.println("NDB::testPrisonersDilemma()~actualPlayerBScore="+actualPlayerBScore);
		
		Assert.assertEquals(expectedPlayerAScore, actualPlayerAScore, 0.0000001);		
		Assert.assertEquals(expectedPlayerBScore, actualPlayerBScore, 0.0000001);
		
	}

}
