package ie.nix.pd;

import ie.nix.pd.IteratedPrisonersDilemma;
import ie.nix.pd.PrisonersDilemma.Payoffs;
import ie.nix.util.Randomness;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class IteratedPrisonersDilemmaTests {

	protected IteratedPrisonersDilemma ipd;
	
	@Before
	public void setUp() throws Exception {	
		Randomness.setSeed(1);
	}
	
	@Test
	public void testPrisonersDilemmaCvsC() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Cooperator);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.Cooperator);
		int numberOfGames = 10;
		double expectedPlayerAScore = 30;
		double expectedPlayerBScore = 30;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaDvsD() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		int numberOfGames = 10;
		double expectedPlayerAScore = 20;
		double expectedPlayerBScore = 20;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaTFTvsTFT() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.TitForTat);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.TitForTat);
		int numberOfGames = 10;
		double expectedPlayerAScore = 30;
		double expectedPlayerBScore = 30;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaTFTvsDD() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.TitForTat);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		int numberOfGames = 10;
		double expectedPlayerAScore = 19; //1 + (2 * 9) = 19
		double expectedPlayerBScore = 22; //4 + (2 * 9) = 22
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}

	@Test
	public void testPrisonersDilemmaStochasticDvsTFT() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.StochasticTitForTat);
		int numberOfGames = 10;		
		double expectedPlayerAScore = 22; //4 + (2 * 9) Defector wins first
		double expectedPlayerBScore = 19; //1+ (2 * 9)
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}

	@Test
	public void testPrisonersDilemmaStochasticDvsGTFT() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.StochasticGenerousTitForTat);
		int numberOfGames = 10;
		double expectedPlayerAScore = 28; //(4 * 4) + (2 * 6) Defector wins first the three other times
		double expectedPlayerBScore = 16; //(1 * 4) + (2 * 6)
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaNumberOfGameNone() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		int numberOfGames = 0;
		double expectedPlayerAScore = 0;
		double expectedPlayerBScore = 0;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}

	@Test
	public void testPrisonersDilemmaNumberOfGameOne() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		int numberOfGames = 1;
		double expectedPlayerAScore = 2;
		double expectedPlayerBScore = 2;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	@Test
	public void testPrisonersDilemmaNumberOfGameTwo() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		IPDPlayer playerB = new IPDPlayer(IteratedPrisonersDilemma.Defector);
		int numberOfGames = 2;
		double expectedPlayerAScore = 4;
		double expectedPlayerBScore = 4;
		
		testIteratedPrisonersDilemma(playerA, playerB, numberOfGames, expectedPlayerAScore, expectedPlayerBScore);
	}
	
	public void testIteratedPrisonersDilemma(IPDPlayer playerA, IPDPlayer playerB, int numberOfGames,
			double expectedPlayerAScore, double expectedPlayerBScore) {

		ipd = new IteratedPrisonersDilemma(new Payoffs(3, 1, 4, 2), numberOfGames);
		ipd.play(playerA, playerB);
		
		double actualPlayerAScore = playerA.getScore();	
		System.out.println("NDB::testPrisonersDilemma()~actualPlayerAScore="+actualPlayerAScore);
		Assert.assertEquals(expectedPlayerAScore, actualPlayerAScore, 0.0000001);
		
		double actualPlayerBScore = playerB.getScore();		
		System.out.println("NDB::testPrisonersDilemma()~actualPlayerBScores="+actualPlayerBScore);
		Assert.assertEquals(expectedPlayerBScore, actualPlayerBScore, 0.0000001);
		
	}
}
