package ie.nix.pd;

import java.util.Arrays;
import java.util.List;

import ie.nix.pd.IteratedPrisonersDilemma.Player;
import ie.nix.pd.PrisonersDilemma.Payoffs;
import ie.nix.util.Randomness;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class GroupPrisonersDilemmaTests {

	protected GroupPrisonersDilemma gipd;
	
	@Before
	public void setUp() throws Exception {	
		Randomness.setSeed(1);
	}
	
	@Test
	public void testGroupPrisonersDilemmaCvsDDD() {
		IPDPlayer playerA = new IPDPlayer(IteratedPrisonersDilemma.Cooperator);
		Player[] playerBs = {
				new IPDPlayer(IteratedPrisonersDilemma.Defector),
				new IPDPlayer(IteratedPrisonersDilemma.Defector),
				new IPDPlayer(IteratedPrisonersDilemma.Defector)
		};
		int numberOfGames = 1;
		double expectedPlayerAScore = 3.0; 	          // 1 * 3 
		double expectedPlayerBScores[] = {4.0, 4.0, 4.0}; // 1 * 4
		
		testGroupPrisonersDilemmaCvsDDD(playerA, Arrays.asList(playerBs), numberOfGames, 
				expectedPlayerAScore, expectedPlayerBScores);
	}
	
	public void testGroupPrisonersDilemmaCvsDDD(IPDPlayer playerA, List<Player> playerBs, 
			int numberOfGames,	double expectedPlayerAScore, double[] expectedPlayerBScores) {

		gipd = new GroupPrisonersDilemma(new Payoffs(3, 1, 4, 2));
		gipd.play(playerA, playerBs);
		
		double actualPlayerAScore = playerA.getScore();	
		System.out.println("NDB::testPrisonersDilemma()~actualPlayerAScore="+actualPlayerAScore);
		
		double[] actualPlayerBScores = new double[playerBs.size()];
		for (int p = 0; p < actualPlayerBScores.length; p++) {
			actualPlayerBScores[p] = ((IPDPlayer)playerBs.get(p)).getScore();	
		}
		System.out.println("NDB::testPrisonersDilemma()~actualPlayerBScores="+GroupIteratedPrisonersDilemmaTests.toString(actualPlayerBScores));
		
		Assert.assertEquals(expectedPlayerAScore, actualPlayerAScore, 0.0000001);
		Assert.assertArrayEquals(expectedPlayerBScores, actualPlayerBScores, 0.0000001);
		
	}
}
