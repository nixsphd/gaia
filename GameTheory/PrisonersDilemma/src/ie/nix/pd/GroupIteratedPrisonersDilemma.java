package ie.nix.pd;

import java.util.List;

public class GroupIteratedPrisonersDilemma extends IteratedPrisonersDilemma {

	public GroupIteratedPrisonersDilemma(Payoffs payoffs,  int numberOfGames) {
		super(payoffs, numberOfGames);
		
	}
	
	public void play(Player playerA, List<Player> playerBs) {
		// Play all the PlayersB
		for (Player playerB : playerBs) {			
			play(playerA, playerB);

		}
	}

}
