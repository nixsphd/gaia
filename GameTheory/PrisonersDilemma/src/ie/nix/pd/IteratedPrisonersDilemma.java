package ie.nix.pd;

import ie.nix.util.Randomness;

public class IteratedPrisonersDilemma extends PrisonersDilemma {
	
	// Deterministic Strategies
	// Always cooperates
	public static Strategy Cooperator = new Strategy(1.0, 1.0, 1.0);
	// Always defects
	public static Strategy Defector = new Strategy(0.0, 0.0, 0.0);
	// Cooperates on the first round and then mimics after that
	public static Strategy TitForTat = new Strategy(1.0, 1.0, 0.0);
	// Stochastic Strategies
	// Cooperates on the first round and then mimics after that
	public static Strategy StochasticTitForTat = new Strategy(1.0, 1.0, 0.0, true);
	// Cooperates on the first round, then mimics, but is more likely 
	// to cooperate after an opponents defection. 
	public static Strategy StochasticGenerousTitForTat = new Strategy(1.0, 1.0, 1.0/3, true);
	
	public static class Strategy extends PrisonersDilemma.Strategy {
		
		// The probability of cooperating on the first round
		// Probability of cooperation if the opponents last play was cooperation
		protected double p;
		// Probability of cooperation if the opponents last play was defection
		protected double q;
		
		public Strategy(double i, double p, double q) {
			this(i, p, q, false);
		}
		public Strategy(double i, double p, double q, boolean stochastic) {
			super(i, stochastic);
			this.p = p;
			this.q = q;
		}
		
		public double getP() {
			return p;
		}
		public double getQ() {
			return q;
		}
		public PrisonersDilemma.Play play(Play opponentsLastPlay) {
			double likelihood = 0.5;
			if (stochastic) {
				likelihood = Randomness.nextDouble();
			}
			if (opponentsLastPlay == null) {
				if (i > likelihood) {
					return Play.Cooperate;					
				} else {
					return Play.Defect;					
				}	
			} else if (opponentsLastPlay == Play.Cooperate) {
				if (p > likelihood) {
					return Play.Cooperate;					
				} else {
					return Play.Defect;					
				}
			} else if (opponentsLastPlay == Play.Defect) {
				if (q > likelihood) {
					return Play.Cooperate;					
				} else {
					return Play.Defect;					
				}
			} else {
				return null;
			}
		}
		
		public boolean equals(Strategy strategy ) {
			return super.equals(strategy) && 
					(Double.compare(p, strategy.p) == 0) && 
					(Double.compare(q, strategy.q) == 0) ;
		}
	}

	public interface Player {
		
		public PrisonersDilemma.Play play(Play opponentsLastPlay);
		
		public void playScore(double score);
		
	}
	
	private int numberOfGames;

	public IteratedPrisonersDilemma(Payoffs payoffs, 
			int numberOfGames) {
		super(payoffs);
		this.numberOfGames = numberOfGames;
	}

	public void play(Player playerA, Player playerB) {
		
		// The last plays are null for the first round
		Play lastPlayA = null;
		Play lastPlayB = null;
		
		double playerAScore = 0;
		double playerBScore = 0;		
		int gameNumber = numberOfGames;	
		
		while (gameNumber > 0) {
			// Play a game
			
			Play playA = playerA.play(lastPlayB);
			Play playB = playerB.play(lastPlayA);
			
			switch (playA) {
				case Cooperate:		
					switch (playB) {
						case Cooperate:
							playerAScore += payoffs.reward;
							playerBScore += payoffs.reward;
							break;
						case Defect:
							playerAScore += payoffs.sucker;
							playerBScore += payoffs.temptation;
							break;
					}
					break;
				case Defect:
					switch (playB) {
						case Cooperate:		
							playerAScore += payoffs.temptation;
							playerBScore += payoffs.sucker;		
							break;
						case Defect:
							playerAScore += payoffs.punishment;
							playerBScore += payoffs.punishment;
							break;
					}
					break;
			}	
			

			// Set up the last plays for the next round
			lastPlayA = playA;
			lastPlayB = playB;
			
			gameNumber--;
		}

		
		playerA.playScore(playerAScore);
		playerB.playScore(playerBScore);
	}
	
}
