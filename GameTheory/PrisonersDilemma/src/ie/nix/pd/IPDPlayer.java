package ie.nix.pd;

import ie.nix.pd.IteratedPrisonersDilemma.Player;
import ie.nix.pd.IteratedPrisonersDilemma.Strategy;
import ie.nix.pd.PrisonersDilemma.Play;

public class IPDPlayer implements Player {

	protected Strategy strategy;
	protected double score;	
	protected int numberOfGames;
	
	public IPDPlayer(Strategy strategy) {
		this.strategy = strategy;
		this.score = 0;
		this.numberOfGames = 0;
	}
	
	@Override
	public Play play(Play opponentsLastPlay) {
		numberOfGames++;
		return strategy.play(opponentsLastPlay);
	}

	@Override
	public void playScore(double score) {
		this.score += score;
	}

	public double getScore() {
		return score;
	}

	public int getNumberOfGames() {
		return numberOfGames;
	}

	public void reset() {
		score = 0;
		numberOfGames = 0;
	}

	protected void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

}
