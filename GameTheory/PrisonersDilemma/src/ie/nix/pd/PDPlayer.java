package ie.nix.pd;

import ie.nix.pd.PrisonersDilemma.Play;
import ie.nix.pd.PrisonersDilemma.Player;
import ie.nix.pd.PrisonersDilemma.Strategy;

public class PDPlayer implements Player {

	protected Strategy strategy;
	protected double score;

	public PDPlayer(Strategy strategy) {
		this.strategy = strategy;
	}
	
	@Override
	public Play play() {
		return strategy.play();
	}

	@Override
	public void playScore(double score) {
		this.score += score;
		
	}

	public double getScore() {
		return score;
	}

}
