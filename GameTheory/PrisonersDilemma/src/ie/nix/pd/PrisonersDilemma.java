package ie.nix.pd;

import ie.nix.util.Randomness;

public class PrisonersDilemma {
	
	public static class Payoffs {
		
		public final double reward;
		public final double sucker;
		public final double temptation;
		public final double punishment;

		public Payoffs(int reward, int sucker, int temptation, int punishment) {
			this.reward = reward;
			this.sucker = sucker;
			this.temptation = temptation;
			this.punishment = punishment;
		}
	}
	
	// Deterministic Strategies
	public static final Strategy Cooperator = new Strategy(1.0);
	public static final Strategy Defector = new Strategy(0.0);
	// Stochastic Strategies
	public static final Strategy StochasticCooperator = new Strategy(1.0, true);
	public static final Strategy StochasticDefector = new Strategy(0.0, true);
	
	public static class Strategy {

		// The probability of cooperating
		protected double i = 0.9;
		// Stochastic or deterministic
		protected boolean stochastic = false;

		public Strategy(double i) {
			this.i = i;
		}

		public Strategy(double i, boolean stochastic) {
			this(i);
			this.stochastic = stochastic;
		}

		public double getI() {
			return i;
		}

		public boolean isStochastic() {
			return stochastic;
		}

		public PrisonersDilemma.Play play() {
			double likelihood = 0.5;
			if (stochastic) {
				likelihood = Randomness.nextDouble();
			}
			if (i > likelihood) {
				return Play.Cooperate;
				
			} else {
				return Play.Defect;
				
			}			
		}

		public boolean equals(Strategy strategy) {
			return (Double.compare(i, strategy.i) == 0);
		}
		
	}
	
	public interface Player {
		
		public PrisonersDilemma.Play play();
		
		public void playScore(double score);
		
	}
	
	// TODO - Check this is indeed right.
	public static PrisonersDilemma AxelrodsTournaments = new PrisonersDilemma(new Payoffs(3,0,5,1));
	
	protected Payoffs payoffs;
	
	public PrisonersDilemma(Payoffs payoffs) {
		this.payoffs = payoffs;
	}

	public enum Play {
		Cooperate, Defect
	}

	public void play(Player playerA, Player playerB) {		
		Play playA = playerA.play();
		Play playB = playerB.play();
		
		switch (playA) {
			case Cooperate:		
				switch (playB) {
					case Cooperate:
						playerA.playScore(payoffs.reward);
						playerB.playScore(payoffs.reward);
						break;
					case Defect:
						playerA.playScore(payoffs.sucker);
						playerB.playScore(payoffs.temptation);
						break;
				}
				break;
			case Defect:
				switch (playB) {
					case Cooperate:		
						playerA.playScore(payoffs.temptation);
						playerB.playScore(payoffs.sucker);		
						break;
					case Defect:
						playerA.playScore(payoffs.punishment);
						playerB.playScore(payoffs.punishment);
						break;
				}
				break;
		}
		
	}
	
}
