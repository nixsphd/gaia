package ie.nix.util;

import org.junit.Assert;
import org.junit.Test;

public class HaloTest {
	
	@Test
	public void testUpdateHalo1() {
		
		int halo = 1;
		int[] initialArray = {
				0,
				1,2,3,4,5,6,7,8,
				0,
			};
		int[] expectedFinalArray = {
				8,
				1,2,3,4,5,6,7,8,
				1,
			};
		
		testUpdateHalo(halo, initialArray, expectedFinalArray);
	}

	@Test
	public void testUpdateHalo4() {
		
		int halo = 4;
		int[] initialArray = {
				0,0,0,0,
				1,2,3,4,5,6,7,8,
				0,0,0,0,
			};
		int[] expectedFinalArray = {
				5,6,7,8,
				1,2,3,4,5,6,7,8,
				1,2,3,4,
			};
		
		testUpdateHalo(halo, initialArray, expectedFinalArray);
	}

	protected void testUpdateHalo(int halo, int[] initialArray, int[] expectedFinalArray) {
		Halo.updateHalos(initialArray, halo);

//		System.out.println("expectedFinalArray="+Arrays.toString(expectedFinalArray));
//		System.out.println("actualFinalArray="+Arrays.toString(initialArray));
		Assert.assertArrayEquals(expectedFinalArray, initialArray);
	}
	
	@Test
	public void testRemoveHalo1() {
		
		int halo = 1;
		int[] initialArray = {
				0,
				1,2,3,4,5,6,7,8,
				0,
			};
		int[] expectedFinalArray = {
				1,2,3,4,5,6,7,8,
			};
		
		testRemoveHalo(halo, initialArray, expectedFinalArray);
	}
	
	@Test
	public void testRemoveHalo4() {
		
		int halo = 4;
		int[] initialArray = {
				0,0,0,0,
				1,2,3,4,5,6,7,8,
				0,0,0,0,
			};
		int[] expectedFinalArray = {
				1,2,3,4,5,6,7,8,
			};
		
		testRemoveHalo(halo, initialArray, expectedFinalArray);
	}

	protected void testRemoveHalo(int halo, int[] initialArray, int[] expectedFinalArray) {
		int[] actualFinalArray = Halo.removeHalos(initialArray, halo);

//		System.out.println("expectedFinalArray="+Arrays.toString(expectedFinalArray));
//		System.out.println("actualFinalArray="+Arrays.toString(actualFinalArray));
		Assert.assertArrayEquals(expectedFinalArray, actualFinalArray);
	}

		
}
