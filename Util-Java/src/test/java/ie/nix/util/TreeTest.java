package ie.nix.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ie.nix.util.Tree.Branch;
import ie.nix.util.Tree.Leaf;
import ie.nix.util.Tree.Node;

/**
 * Unit test for simple App.
 */
public class TreeTest {

	protected String expectedOutputDirName = "src/test/ie/nix/util/data";
	
	@Before
    public void setUp() throws Exception {
    	Randomness.setSeed(1);
    	Tree.nextID = 0;
	}

	@Test
	public void testGetParentRoot() {
		Tree tree = new Tree(new Branch("A", new Node[]{
				new Branch("AA", new Node[]{	
						new Leaf("AAA"),
						new Leaf("AAB")
				}),
				new Branch("AB", new Node[]{	
						new Leaf("ABA"),
						new Leaf("ABB")
				})}));

		Branch expectedParent = null;
		Node node = tree.getRoot();

		testGetParent(node, expectedParent);
	}
	
	@Test
	public void testGetParentMiddle() {
		Tree tree = new Tree(new Branch("A", new Node[]{
				new Branch("AA", new Node[]{	
						new Leaf("AAA"),
						new Leaf("AAB")
				}),
				new Branch("AB", new Node[]{	
						new Leaf("ABA"),
						new Leaf("ABB")
				})}));

		Branch expectedParent = (Branch)tree.getRoot();
		Node node = expectedParent.getChild(0);

		testGetParent(node, expectedParent);
	}
	
	@Test
	public void testGetParentLeaf() {
		Tree tree = new Tree(new Branch("A", new Node[]{
				new Branch("AA", new Node[]{	
						new Leaf("AAA"),
						new Leaf("AAB")
				}),
				new Branch("AB", new Node[]{	
						new Leaf("ABA"),
						new Leaf("ABB")
				})}));

		Branch expectedParent = (Branch)((Branch)tree.getRoot()).getChild(0);
		Node node = expectedParent.getChild(0);

		testGetParent(node, expectedParent);
	}

	public void testGetParent(Node node, Branch expectedParent) {
		
		Branch actualParent = node.getParent();
//    	System.out.println("NDB::testGetParent()~actualParent="+toString((Branch)actualParent));  	    	
//    	System.out.println("NDB::testGetParent()~expectedParent="+toString(expectedParent));  	
    	
    	Assert.assertTrue(expectedParent == actualParent);
    	
	}
	
//	public void testGetDepthRoot() {
//		Branch root = new Multiply(
//				new Add(
//					new Constant(1.0),
//					new Constant(2.0)),
//				new Subtract(
//					new Constant(3.0),
//					new Constant(4.0)));
//	
//		int expectedDepth = 1;
//		Node node = root;
//	
//		testGetDepth(root, node, expectedDepth);
//	}
//	
//	public void testGetDepthMiddle() {
//		Branch root = new Multiply(
//				new Add(
//					new Constant(1.0),
//					new Constant(2.0)),
//				new Subtract(
//					new Constant(3.0),
//					new Constant(4.0)));
//	
//		int expectedDepth = 2;
//		Node node = root.getChild(0);
//	
//		testGetDepth(root, node, expectedDepth);
//	}
//	
//	public void testGetDepthLeaf() {
//		Branch root = new Multiply(
//				new Add(
//					new Constant(1.0),
//					new Constant(2.0)),
//				new Subtract(
//					new Constant(3.0),
//					new Constant(4.0)));
//	
//		int expectedDepth = 3;
//		Node node = ((Branch)root.getChild(0)).getChild(0);
//	
//		testGetDepth(root, node, expectedDepth);
//	}
//
//	public void testGetDepthNone() {
//		Branch branch = new Multiply();
//	
//		int expectedDepth = 1;
//		Node node = branch;
//	
//		testGetDepth(branch, node, expectedDepth);
//	}
//	
//	public void testGetDepth(Branch root, Node node, int expectedDepth) {
//		
//    	int actualDepth = node.getDepth();
////    	System.out.println("NDB::testGetDepth()~actualParent="+actualDepth);  	    	
////    	System.out.println("NDB::testGetDepth()~expectedParent="+expectedDepth);  	
//    	
//    	Assert.assertTrue(expectedDepth == actualDepth);
//    	
//	}
//	
//	public void testGetNodesAtDepth1() {
//		Branch root = new Multiply(
//				new Add(
//					new Constant(1.0),
//					new Constant(2.0)),
//				new Subtract(
//					new Constant(3.0),
//					new Constant(4.0)));
//	
//		int depth = 1;
//		Node[] expectedNodes = {root};
//	
//		testGetNodesAtDepth(root, depth, expectedNodes);
//	}
//	
//	public void testGetNodesAtDepth2() {
//		Branch root = new Multiply(
//				new Add(
//					new Constant(1.0),
//					new Constant(2.0)),
//				new Subtract(
//					new Constant(3.0),
//					new Constant(4.0)));
//	
//		int depth = 2;
//		Node[] expectedNodes = {root.getChild(0), root.getChild(1)};
//	
//		testGetNodesAtDepth(root, depth, expectedNodes);
//	}
//	
//	public void testGetNodesAtDepth3() {
//		Branch root = new Multiply(
//				new Add(
//					new Constant(1.0),
//					new Constant(2.0)),
//				new Subtract(
//					new Constant(3.0),
//					new Constant(4.0)));
//	
//		int depth = 3;
//		Node[] expectedNodes = {
//				((Branch)root.getChild(0)).getChild(0), 
//				((Branch)root.getChild(0)).getChild(1), 
//				((Branch)root.getChild(1)).getChild(0), 
//				((Branch)root.getChild(1)).getChild(1)};
//	
//		testGetNodesAtDepth(root, depth, expectedNodes);
//	}
//	
//	public void testGetNodesAtDepth(Branch root, int depth, Node[] expectedNodes) {
//		
//    	Node[] actualeNodes = root.getNodesAtDepth(depth).toArray(new Node[0]);
////    	System.out.println("NDB::testGetNodesAtDepth()~actualeNodes="+toString(actualeNodes));  	    	
////    	System.out.println("NDB::testGetNodesAtDepth()~expectedNodes="+toString(expectedNodes));  	
//    	
//    	Assert.assertTrue(TestHelper.equals(expectedNodes, actualeNodes));
//    	
//	}
//
//	public void testPrintTree() {
//		KozaTableau kozaTableau = new KozaTableau();
//		kozaTableau.numberOfTrees = 0;
//		String expectedOutputFileName = expectedOutputDirName+"testPrintTree.dot";
//		
//		testPrintTree(kozaTableau, expectedOutputFileName);
//	}
//	
//	public void testPrintTree(KozaTableau kozaTableau, String expectedOutputFileName) {
//		String expectedTreeAsAString = TestHelper.readFile(expectedOutputFileName);
//		
//		GeneticTreeming gp = new GeneticTreeming(kozaTableau);
//		
//		Tree tree = gp.initRandomTree();
//		
//		String actualTreeAsAString = TestHelper.toDOTString(tree);
////    	System.out.println("NDB::testPrintTree()~actualTreeAsAString=\n"+actualTreeAsAString);
////    	System.out.println("NDB::testPrintTree()~expectedTreeAsAString=\n"+expectedTreeAsAString);
////    	writeFile(expectedOutputFileName+".act", actualTreeAsAString);
//    	
//    	Assert.assertEquals(expectedTreeAsAString, actualTreeAsAString);
//		
//	}

	public static String toString(Node[] nodes) {		
		StringBuilder string = new StringBuilder("{ \n");
		for (int s = 0; s < nodes.length; s++) {
			string.append(""+toString(nodes[s],"\t")+",\n");
		}
		string.replace(string.length()-2, string.length(),"};");
		return string.toString();
	}

	public static String toString(Node node, String tabs) {
		if (node  instanceof Branch) {
			return toString((Branch)node, tabs);
			
		} else if (node  instanceof Leaf) {
			return toString((Leaf)node);
			
		} 
		return null;
	}

	public static String toString(Branch branch) {
		if (branch != null) {
			return toString(branch, "\t");
		} else {
			return "null";
		}
	}
	
	public static String toString(Branch branch, String tabs) {
		StringBuilder string = new StringBuilder("new "+branch.getClass().getSimpleName()+
				"(\""+branch.getLabel()+"\",new Node[]{\n");
		for (int i = 0; i < branch.getDegree(); i++) {
			if (branch.getChild(i) instanceof Branch) {
				Branch subBranch = ((Branch)branch.getChild(i));
				string.append(tabs+toString(subBranch, tabs+"\t")+",\n");
				 
			} else {
				Leaf leaf = (Leaf)branch.getChild(i);
				string.append(tabs+toString(leaf)+",\n");
				
			}
		}
		string.replace(string.length()-2, string.length(), "})");
		return string.toString();
	}

	public static String toString(Leaf leaf) {
		return "new "+leaf.getClass().getSimpleName()+"(\""+leaf.getLabel()+"\")";
			
	}
	
	public static String toString(Tree[] trees) {
		StringBuilder string = new StringBuilder("");
		for (int p = 0; p < trees.length; p++) {
			string.append(toString(trees[p])+"\n");
		}
		return string.toString();
	}

	public static String toString(Tree tree) {
		StringBuilder string = new StringBuilder("new Tree(\n");
		string.append("\t"+toString(tree.getRoot(), "\t\t")+");");
		return string.toString();
	}
	
}
