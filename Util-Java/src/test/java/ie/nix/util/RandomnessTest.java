package ie.nix.util;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.junit.Assert;
import org.junit.Test;

public class RandomnessTest {
		
	@Test
	public void testNextGaussian() {
		Randomness.setSeed(0);
		
		double standardDeviation = 1.0;
		double mean = 0.0;
		int numberOfGaussians = 100000;
		
		testNextGaussian(numberOfGaussians, standardDeviation, mean);
	}
	
	@Test
	public void testNextGaussianWithSD0p01() {
		Randomness.setSeed(0);
		double standardDeviation = 0.01;
		double mean = 0.0;
		int numberOfGaussians = 100000;
		
		testNextGaussian(numberOfGaussians, standardDeviation, mean);
	}
	
	@Test
	public void testNextGaussianWithSD0p01Mean0p5() {
		Randomness.setSeed(0);
		double standardDeviation = 0.01;
		double mean = 0.5;
		int numberOfGaussians = 100000;
		
		testNextGaussian(numberOfGaussians, standardDeviation, mean);
	}
	
	@Test
	public void testNextGaussianWithMean1() {
		Randomness.setSeed(0);
		double standardDeviation = 1.0;
		double mean = 1.0;
		int numberOfGaussians = 100000;
		
		testNextGaussian(numberOfGaussians , standardDeviation, mean);
	}

	protected void testNextGaussian(int numberOfGaussians, double standardDeviation, double mean) {
		
		SummaryStatistics stats = new SummaryStatistics();
//		double[] actualGaussians = new double[10];
		for (int g = 0; g <  numberOfGaussians; g++) {
			stats.addValue(Randomness.nextGaussian(mean, standardDeviation));
		}

		double actualStandardDeviation = stats.getStandardDeviation();
		double actualMean = stats.getMean();
		
//		System.out.println("standardDeviation="+standardDeviation);
//		System.out.println("actualStandardDeviation="+actualStandardDeviation);
//		System.out.println("mean="+mean);
//		System.out.println("actualMean="+actualMean);
		Assert.assertEquals(mean, actualMean, 0.01);
		Assert.assertEquals(standardDeviation, actualStandardDeviation, 0.001);
	}
	
}
