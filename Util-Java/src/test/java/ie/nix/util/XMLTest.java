package ie.nix.util;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import org.junit.Test;

public class XMLTest {

	@XmlRootElement
	public static class Customer {

	    String name;
	    int age;
	    int id;

	    public String getName() {
	        return name;
	    }

	    @XmlElement
	    public void setName(String name) {
	        this.name = name;
	    }

	    public int getAge() {
	        return age;
	    }

	    @XmlElement
	    public void setAge(int age) {
	        this.age = age;
	    }

	    public int getId() {
	        return id;
	    }

	    @XmlAttribute
	    public void setId(int id) {
	        this.id = id;
	    }
	    
	    @XmlElements({
	        @XmlElement(name = "slide", type = Slide.class)
	    })
	    private Slide slide;

		@Override
		public String toString() {
			return "Customer [name=" + name + ", age=" + age + ", id=" + id + " slide=" + slide.name+ "]";
		}

	}

	@XmlRootElement
	public static class Slide {

	    @XmlAttribute
	    String name;
	}
	
	
	@Test
	public void testReadXML() {
//		XMLParser.readXMLFile("src/test/java/ie/nix/util/XMLTest.xml");
		
//		Customer customer = (Customer)XMLParser.readXMLFileInto("src/test/java/ie/nix/util/XMLTest.xml", Customer.class);

//		System.out.println(customer.toString());
		
	}

}
