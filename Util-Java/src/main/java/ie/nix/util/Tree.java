package ie.nix.util;

import java.util.ArrayList;

public class Tree { //extends Node {

	protected static int nextID = 0;
	protected Node root;

	public static class Node {

		protected int id = -1;
		protected String label;
		protected Branch parent;

		public Node(String label) {
			this.id = nextID++;
			this.label = label;
			this.parent = null;
			// System.out.println("NDB::Node("+name+")~id="+this.id);
		}

		public String getLabel() {
			return label;
		}

		public Branch getParent() {
			return parent;
		}

		public void copy(Node node) {
			// You should never change the id!
			this.label = node.label;
		}

		public int getDepth() {
			if (parent != null) {
				return parent.getDepth() + 1;
			} else {
				return 1;
			}
		}

		@Override
		public String toString() {
			if (parent != null) {
				return "[id=" + id + ", name=" + label + ", parent="
						+ parent.id + "]";
			} else {
				return "[id=" + id + ", name=" + label + ", parent=null]";
			}
		}

	}

	public static class Leaf extends Node {

		public Leaf(String label) {
			super(label);
		}

	}

	// TODO - Should i have a branch class at all, maybe having all these functions in
	//        tree is better. So trees are made up of sub Trees not Brnaches?
	public static class Branch extends Node {

		private Node children[];

		public Branch(String label, int degree) {
			this(label, new Node[degree]);
		}
		
		public Branch(String label, Node[] children) {
			super(label);
			this.children = children;
			for (int i = 0; i < getDegree(); i++) {
				this.children[i].parent = this;
			}
		}
		
		public int getDegree() {
			return children.length;
		}

		public Node getChild(int a) {
			return children[a];
		}

		protected void setChild(int a, Node node) {
			children[a] = node;
			// Set the parent
			node.parent = this;
		}

		public void replace(Node node, Node newNode) {
			for (int i = 0; i < getDegree(); i++) {
				if (getChild(i) == node) {
					setChild(i, newNode);
					// Don't fix the parent of function because this might
					// be a swap ait's it's already been changed.
					return;
				}
			}
			// Didn't find the function to be replaced, what should I do?
			System.out
					.println("NDB Didn't find the function to be replaced, what should I do?");
		}

		public void copyNodes(Node nodes) {
			for (int i = 0; i < getDegree(); i++) {
				setChild(i, ((Branch) nodes).getChild(i));
			}
		}

		public int getHeight() {
			if (getDegree() == 0) {
				return 1;
			} else {
				int height = 0;
				for (int a = 0; a < getDegree(); a++) {
					if (getChild(a) instanceof Branch) {
						int challengeDepth = ((Branch) getChild(a)).getHeight();
						if (challengeDepth > height) {
							height = challengeDepth;
						}
					} else {
						if (height < 1) {
							height = 1;
						}
					}
				}
				return height + 1; // It's the max depth of the subtree plus on
									// for this node.
			}
		}

		public ArrayList<Node> getNodesAtDepth(int depth) {

			int currentDepth = getDepth();
			ArrayList<Node> nodesAtDepth = new ArrayList<Node>();

			if (currentDepth == depth) {
				// This can happen when we start the search for depth 1 at the
				// root node.
				nodesAtDepth.add(this);
			}
			for (int a = 0; a < getDegree(); a++) {
				if (currentDepth == depth - 1) {
					// Add all the inputs...
					nodesAtDepth.add(getChild(a));
				} else {
					// Search the inputs, if there are functions
					if (getChild(a) instanceof Branch) {
						nodesAtDepth.addAll(((Branch) getChild(a))
								.getNodesAtDepth(depth));
					} else {
						// return nothing as this part of the tree isn't deep
						// enough.
					}
				}
			}
			return nodesAtDepth;
		}

		public Branch getParent(Node primitive) {
			return parent;
		}
	}

	public Tree(Node root) {
		this.root = root;
	}
	
	public Node getRoot() {
		return root;
	}

	public void setRoot(Node node) {
		this.root = node;
		// Set the parent to null;
		node.parent = null;
	}
	
//	public Branch cloneBranch(Branch branch) {
//		try {
//			Branch clonedBranch = branch.getClass().newInstance();
//			clonedBranch.copy(branch);
//			return clonedBranch;
//		} catch (InstantiationException e) {
//			System.err.println(e.getMessage());
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			System.err.println(e.getMessage());
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	public Leaf cloneLeaf(Leaf leaf) {
//		try {
//			Leaf clonedLeaf = leaf.getClass().newInstance();
//			clonedLeaf.copy(leaf);
//			return clonedLeaf;
//		} catch (InstantiationException e) {
//			System.err.println(e.getMessage());
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			System.err.println(e.getMessage());
//			e.printStackTrace();
//		}
//		return null;
//	}

}
