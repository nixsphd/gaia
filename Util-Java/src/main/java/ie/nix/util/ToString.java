package ie.nix.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ToString {
	
	public static String toString(String fileName) {
		try {
			byte[] encoded = Files.readAllBytes(Paths.get(fileName));
			return new String(encoded);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public static String toString(Object[] objects) {		
		StringBuilder string = new StringBuilder("{");
		for (int s = 0; s < objects.length; s++) {
			string.append(""+objects[s]+", ");
			string.replace(string.length()-2, string.length(),", ");
		}
		string.replace(string.length()-2, string.length(),"};");
		return string.toString();
	}	
	
	public static String toString(double[] doubles) {		
		StringBuilder string = new StringBuilder("{");
		for (int s = 0; s < doubles.length; s++) {
			string.append(""+doubles[s]+",");
			string.replace(string.length()-1, string.length(),",");
		}
		string.replace(string.length()-1, string.length(),"};");
		return string.toString();
	}	
	
	public static String toString(boolean[] booleans) {
		StringBuilder string = new StringBuilder("{");
		for (int s = 0; s < booleans.length; s++) {
			string.append(""+(booleans[s]?"true":"false")+",");
			string.replace(string.length()-1, string.length(),",");
		}
		string.replace(string.length()-1, string.length(),"};");
		return string.toString();
	}
	
	public static String toString(int[] ints) {
		StringBuilder string = new StringBuilder("{");
		for (int s = 0; s < ints.length; s++) {
			string.append(""+ints[s]+",");
			string.replace(string.length()-1, string.length(),",");
		}
		string.replace(string.length()-1, string.length(),"};");
		return string.toString();
	}

	protected String toString(String[] strings) {		
		StringBuilder string = new StringBuilder("{\n");
		for (int s = 0; s < strings.length; s++) {
			string.append("\""+strings[s]+"\",");
			string.replace(string.length()-1, string.length(),",\n");
		}
		string.replace(string.length()-2, string.length(),"};");
		return string.toString();
	}	
	
	public static String toString(boolean[][] booleans) {
		StringBuilder string = new StringBuilder("{\n");
		for (int o = 0; o < booleans.length; o++) {
			string.append("{");
			for (int s = 0; s < booleans[o].length; s++) {
				string.append(""+(booleans[o][s]?"true":"false")+",");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.length()-2, string.length(),"\n};");
		return string.toString();
		
//		StringBuilder string = new StringBuilder("{");
//		for (int s = 0; s < booleans.length; s++) {
//			string.append(""+(booleans[s]?"true":"false")+",");
//			string.replace(string.length()-1, string.length(),",");
//		}
//		string.replace(string.length()-1, string.length(),"};");
//		return string.toString();
	}

	public static String toString(Number[][] numbers) {
		StringBuilder string = new StringBuilder("{\n");
		for (int o = 0; o < numbers.length; o++) {
			string.append("{");
			for (int s = 0; s < numbers[o].length; s++) {
				string.append(""+numbers[o][s]+",");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.length()-2, string.length(),"\n};");
		return string.toString();
	}
	
	public static String toString(int[][] numbers) {
		StringBuilder string = new StringBuilder("{\n");
		for (int o = 0; o < numbers.length; o++) {
			string.append("{");
			for (int s = 0; s < numbers[o].length; s++) {
				string.append(""+numbers[o][s]+",");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.length()-2, string.length(),"\n};");
		return string.toString();
	}
	
	public static String toString(double[][] numbers) {
		StringBuilder string = new StringBuilder("{\n");
		for (int o = 0; o < numbers.length; o++) {
			string.append("{");
			for (int s = 0; s < numbers[o].length; s++) {
				string.append(""+numbers[o][s]+",");
			}
			string.replace(string.length()-1, string.length(),"},\n");
		}
		string.replace(string.length()-2, string.length(),"\n};");
		return string.toString();
	}
}
