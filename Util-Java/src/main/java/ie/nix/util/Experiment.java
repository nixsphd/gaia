package ie.nix.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.time.StopWatch;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class Experiment {
	
	public static final String DEFAULT_EXPERIMENT_DIR = "experiments";
	
	protected static Options options = null;
	protected static CommandLine arguments = null;
	protected static StopWatch stopWatch = null;
	
	protected static Experiment init(String[] args, Class<? extends Experiment> experimentClass) {
		
		initStopWatch();
		
		initOptions();
		initArguments(args);
		
		// Init the Randomness 
		Randomness.setSeed(getSeed());

		// Write the experiment is needed
		Experiment experiment = processExperimentWriteOption(experimentClass);
		
		// If it was not written and so is now null, then we read it.
		if (experiment == null) {
			experiment = processExperimentOption(experimentClass);
		}
		if (arguments.hasOption("experiment")) {
			return experiment;
			// We only want to run the experiment when the -e flag is passed. 
		} else {
			return null;
			
		}
	}

	public static void initStopWatch() {
		stopWatch = new StopWatch();
		stopWatch.start();
	}

	public static Options initOptions() {
		options = new Options();
		// Random number generator seed
		options.addOption(getSeedOption());
		// Experiment file
		options.addOption(getExperimentWriteOption());
		// Experiment file
		options.addOption(getExperimentOption());
		return options;
	
	}

	public static CommandLine initArguments(String[] args) {
		CommandLineParser parser = new DefaultParser();
		try {
			arguments = parser.parse(options, args);
		} catch (ParseException e) {
			help(e.getLocalizedMessage());
		}
		return arguments;
	}

	public static void help(String header) {
		 HelpFormatter formatter = new HelpFormatter();
		 String footer = "\nPlease report issues at nixmcd@gmail.com";
		 formatter.printHelp("MajorityProblem", header, options, footer, true);
			 	
	}

	public static Option getExperimentOption() {
		return Option.builder("e")
				.required(false)
				.longOpt("experiment")
				.hasArg(true)
				.argName("experiment-directory")
				.desc("The directory of the experiment file")
				.build();
	}
	
	private static Option getExperimentWriteOption() {
		return Option.builder("w")
				.required(false)
				.longOpt("write-experiment")
				.hasArg(true)
				.argName("experiment-write-directory")
				.desc("The directory where the experiment file is to be written")
				.build();
	}

	public static String getExperimentDirectory() {
		String experimentDirectory = null;
		// Pull out the arguments
		if (arguments.hasOption("experiment")) {
		    // initialise the member variable
			experimentDirectory = arguments.getOptionValue("experiment");
			
		} else if (arguments.hasOption("write-experiment")) {
		    // initialise the member variable
			experimentDirectory = arguments.getOptionValue("write-experiment");
			
		}
		return experimentDirectory;
	}

	public static String getExperimentFilename() {
		return getExperimentDirectory()+"/Experiment.json";
		
	}
	
	public static Experiment processExperimentWriteOption(Class<? extends Experiment> experimentClass) {
		if(arguments.hasOption("write-experiment")) {
			writeExperiment(experimentClass);
		}
		return null;
	}

	protected static Experiment writeExperiment(Class<? extends Experiment> experimentClass) {
		try {
			Experiment experiment = experimentClass.getConstructor().newInstance();
			writeExperiemtAsJSON(Experiment.getExperimentFilename(), experiment);
			System.out.println("#Wrote new experiment file "+getExperimentFilename()+" with seed "+getSeed());
			return experiment;
		} catch (Exception e) {
			System.err.println("#Error instancisting "+experimentClass.getName());
			e.printStackTrace();
		}
		return null;
	}
	
	public static Experiment processExperimentOption(Class<? extends Experiment> experimentClass) {
		String experimentFilename = getExperimentFilename();
		
		// Process the experiment file
		if (new File(experimentFilename).exists()) {
			return readExperiemtFromJSON(experimentFilename, experimentClass);
			
		} else {
			return writeExperiment(experimentClass);
			
		}
	}
	
	public static Experiment readExperiemtFromXML(String experimentFileName) {
		Experiment experiment = 
				(Experiment)XMLParser.readXMLFileInto(experimentFileName, Experiment.class);
		return experiment;
	
	}

	public static void writeExperiemtAsXML(String experimentFilename, Experiment experiment) {
		try {
			OutputStream os = new FileOutputStream(new File(experimentFilename));
			XMLParser.writeXMLIntoStream(os, Experiment.class, experiment);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void writeAsJSON(String filename, Object object) {
		try {
			File outputFile = new File(filename);
	        ObjectMapper mapper = new ObjectMapper();
	        
	        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
	        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	     
	        //Object to JSON in file
	        mapper.writeValue(outputFile, object);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Object readFromJSON(String filename, Class<? extends Object> clazz) {
		try {
			File inputFile = new File(filename);
			ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
			return mapper.readValue(inputFile, clazz);
		} catch (JsonParseException e) {
			e.printStackTrace();
			return null;
		} catch (JsonMappingException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
//			e.printStackTrace();
			return null;
		}
		
	}

	public static void writeExperiemtAsJSON(String experimentFilename, Experiment experiment) {
		writeAsJSON(experimentFilename, experiment);

	}

	public static Experiment readExperiemtFromJSON(String experimentFilename, Class<? extends Experiment> clazz) {
		return (Experiment)readFromJSON(experimentFilename, clazz);
		
	}
	
	public static Option getSeedOption() {
		return Option.builder("s")
				.required(false)
				.longOpt("seed")
				.hasArg(true)
				.argName("seed")
				.desc("The seed for the random number generator.")
				.build();
	}
	
	public static int getSeed() {
		// Pull out the arguments
		if( arguments.hasOption("seed")) {
			return Integer.valueOf(arguments.getOptionValue("seed"));
			
		} else {
			return 1;
			
		}
	}

	public Experiment() {
		super();
	}

}