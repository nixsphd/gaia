package ie.nix.util;

public class Halo {

	public static void updateHalos(int[] automota, int halo) {
		// Copy the halos
		System.arraycopy(automota, automota.length-(2*halo), automota, 0, halo);
		System.arraycopy(automota, halo, automota, automota.length-halo, halo);
	}
	
	public static int[] removeHalos(int[] automota, int halo) {
		int[] automotaWithoutHalo = new int[automota.length-(2*halo)];
		System.arraycopy(automota, halo, automotaWithoutHalo, 0, automotaWithoutHalo.length);
		return automotaWithoutHalo;
	}
	
	public static void updateHalos(boolean[] automota, int halo) {
		// Copy the halos
		System.arraycopy(automota, automota.length-(2*halo), automota, 0, halo);
		System.arraycopy(automota, halo, automota, automota.length-halo, halo);
	}
	
	public static boolean[] removeHalos(boolean[] automota, int halo) {
		boolean[] automotaWithoutHalo = new boolean[automota.length-(2*halo)];
		System.arraycopy(automota, halo, automotaWithoutHalo, 0, automotaWithoutHalo.length);
		return automotaWithoutHalo;
	}
	
}
