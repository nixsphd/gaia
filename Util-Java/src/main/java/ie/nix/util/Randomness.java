package ie.nix.util;

//import java.util.HashMap;

import java.util.Random;

import org.apache.commons.math3.random.RandomDataGenerator;

//http://download.eclipse.org/eclipse/updates/4.3-P-builds/
//http://dist.springsource.org/snapshot/GRECLIPSE/e4.3-j8
	
public class Randomness {
	
	private static Random rngFromJava;
	private static RandomDataGenerator rngFromApache;
//	private static Normal normal;

//	private static HashMap<Double, PoissonDistribution> poissonDistributions;// (double p)
	
	public static void setSeed(int seed) {
		rngFromJava = new Random(seed);
		rngFromApache = new RandomDataGenerator();
		rngFromApache.reSeed(seed);
//		poissonDistributions = new HashMap<Double, PoissonDistribution>();
//		randomEngine = new MersenneTwister(seed);
//		normal = new Normal(0, 1, randomEngine);
	}

	public static int nextBinary() {
//		System.out.println("NDB::nextRandomGene() ~Generated="+nextRandomGene);
		return nextInt(0,1);		
	}	
	
	public static double nextDouble() {
		return rngFromJava.nextDouble();
//		return nextDouble(0.0, 1.0);
	}

	public static double nextDouble(double start, double end) {
		double nextRandomDouble = rngFromApache.nextUniform(start, end);
		return nextRandomDouble;
	}

	// Generates a uniformly distributed random integer between lower and upper (endpoints included).
	public static int nextInt(int start, int end) {
	    int nextInt = rngFromApache.nextInt(start, end);
		return nextInt;
	}
	
	public static int nextInt(int positiveBound) {
		return nextInt(0, positiveBound);		
	}
	
	public static double nextGaussian() {
		return nextGaussian(0.0, 1.0);
	}
	
	public static double nextGaussian(double standardDeviation) {
		return nextGaussian(0.0, standardDeviation);
	}
	
	public static double nextGaussian(double mean, double standardDeviation) {
		double nextGaussian = rngFromApache.nextGaussian(mean, standardDeviation);
		return nextGaussian;
	}

	public static long nextPoisson(double mean) {
		return rngFromApache.nextPoisson(mean);
	}
}