package ie.nix.util;

import java.util.List;

public class Equals {
	

	public static boolean equals(String fileNameA, String fileNameB) {
		String fileA  = ToString.toString(fileNameA);
		String fileB = ToString.toString(fileNameB);
		return fileA.equals(fileB);
		
	}
	
	public static boolean equals(Object[] expectedSpaceTimeData, List<Object> actualSpaceTimeData) {
		int spd = 0;
		boolean equals = (expectedSpaceTimeData.length == actualSpaceTimeData.size());
		while (equals && spd < expectedSpaceTimeData.length) {
			equals = expectedSpaceTimeData[spd].equals(actualSpaceTimeData.get(spd));
			spd++;
		}		
		return equals;
	}

	public static boolean equals(double valueA, double valueB, double delta) {
		return (Math.abs(valueA - valueB) <= delta);
		
	}
	
	public static boolean greaterThan(double valueA, double valueB, double delta) {
		return ((valueA - valueB) >= delta);
		
	}
	
	public static boolean lessThan(double valueA, double valueB, double delta) {
		return ((valueB - valueA) >= delta);
		
	}
	
	public static boolean equals(double[] valueA, double[] valueB, double delta) {
		boolean equals = valueA.length == valueB.length;
		int d = valueA.length - 1;
		while (equals && d >= 0) {
			equals =  (Math.abs(valueA[d] - valueB[d]) <= delta);
			d--;
		}
		return equals;
		
	}
	
	public static boolean greaterThan(double[] valueA, double[] valueB, double delta) {
		boolean equals = valueA.length == valueB.length;
		int d = valueA.length - 1;
		while (equals && d >= 0) {
			equals =  (valueA[d] - valueB[d] >= delta);
			d--;
		}
		return equals;
		
	}
	
	public static boolean lessThan(double[] valueA, double[] valueB, double delta) {
		boolean equals = valueA.length == valueB.length;
		int d = valueA.length - 1;
		while (equals && d >= 0) {
			equals =  (valueA[d] - valueB[d] >=  delta);
			d--;
		}
		return equals;
		
	}

	public static boolean equals(boolean[] booleansA, boolean[] booleansB) {
		boolean equals = booleansA.length == booleansB.length;
		int d = booleansA.length - 1;
		while (equals && d >= 0) {
			equals = (booleansA[d] == booleansB[d]);
			d--;
		}
		return equals;
	}

	public static boolean equals(boolean[][] booleansA, boolean[][] booleansB) {
		boolean equals = booleansA.length == booleansB.length;
		int d = booleansA.length - 1;
		while (equals && d >= 0) {
			equals = equals(booleansA[d],booleansB[d]);
			d--;
		}
		return equals;
	}
	
}
