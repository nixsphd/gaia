package ie.nix.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DataFrame {
	
	protected boolean isVolatile;
	protected int numberOfColumns;
	protected int numberOfRows;
	protected ArrayList<String> headers;
	protected ArrayList<List<Object>> data;
	protected PrintWriter writer;  
	public String doubleFormat = "%.3f";
	private String outputDirectoryName = "R";
	private String outputFileName;
	private Stream<String> reader;
	
	public DataFrame() {
		this(false);
	}
	
	public DataFrame(boolean isVolatile) {
		headers = new ArrayList<String>();
		data = new ArrayList<List<Object>>();
		numberOfColumns = 0;
		numberOfRows = 0;
		this.isVolatile = isVolatile;
	}
	
	public DataFrame(String[] columnNames) {
		this();
		addColumns(columnNames);
	}
	
	public DataFrame(Stream<String> columnNamesStream) {
		this();
		addColumns(columnNamesStream);
		
	}

	public int getNumberOfColumns() {
		return numberOfColumns;
	}

	public void addColumn(String name) {
		addColumn(name, new ArrayList<Object>());
		
	}
	
	public void addColumns(String[] columnNames) {
		for (int c = 0; c < columnNames.length; c++) {
			addColumn(columnNames[c].trim());
		}
		
	}
	
	public void addColumns(Stream<String> columnNamesStream) {
		columnNamesStream.forEach(columnNames -> addColumn(columnNames));
		
	}

	public void addColumn(String name, Object[] columnData) {
		addColumn(name, Arrays.asList(columnData));
	}

	public void addColumn(String name, List<Object> columnData) {
		if (columnData.size() != numberOfRows) {
			if (numberOfColumns == 0) {
				// This is the first column so we an add freely...
				numberOfRows += columnData.size();
				
			} else {
				// We have a problem
				System.err.println("NDB addColumn()~columnData.size() "+columnData.size()+" != nextRowIndex "+numberOfRows);
				
			}
		}
		headers.add(numberOfColumns, name);
		data.add(numberOfColumns, new ArrayList<Object>(columnData));
		numberOfColumns++;
			
	}
	
	public int getNumberOfRows() {
		return numberOfRows;
	}
	
	public void addRow(Stream<String> rowDataStream) {
		Iterator<String> rowDataIterator = rowDataStream.limit(numberOfColumns).iterator();
		IntStream.range(0,numberOfColumns).forEach(i -> data.get(i).add(rowDataIterator.next().trim()));
		numberOfRows++;
		
	}
	
	public void addRow(String[] rowData) {
		if (rowData.length != numberOfColumns) {
			// We got problems!
			System.err.println("NDB addRow()~rowData.length "+rowData.length+" != nextColumnIndex "+numberOfColumns);
		}
		
		for (int i = 0; i < rowData.length; i++) {
			data.get(i).add(rowData[i].trim());
		}
		numberOfRows++;
		
	}
	
	public void addRow(Object ... rowData) {
		if (rowData.length != numberOfColumns) {
			// We got problems!
			System.err.println("NDB addRow()~rowData.length "+rowData.length+" != nextColumnIndex "+numberOfColumns);
		}
		
		for (int i = 0; i < rowData.length; i++) {
			data.get(i).add(rowData[i]);
		}
		numberOfRows++;
		
	}

	public void addRows(@SuppressWarnings("unchecked") List<Object> ... rowsData) {
		if (rowsData.length != numberOfColumns) {
			// We got problems!
			System.err.println("NDB addRow()~rowsData.length "+rowsData.length+" != nextColumnIndex "+numberOfColumns);
		}
		
		for (int i = 0; i < rowsData.length; i++) {
			data.get(i).addAll(rowsData[i]);
		}
		numberOfRows += rowsData.length;
	
	}
	
	public void addRows(Object[] ... rowsData) {
		if (rowsData.length != numberOfColumns) {
			// We got problems!
			System.err.println("NDB addRow()~rowsData.length "+rowsData.length+" != nextColumnIndex "+numberOfColumns);
		}
		
		for (int i = 0; i < rowsData.length; i++) {
			data.get(i).addAll(Arrays.asList(rowsData[i]));
		}
		numberOfRows += rowsData[0].length;
		
	}

	public String getOutputDirectoryName() {
		return outputDirectoryName;
	}

	public void setOutputDirectoryName(String outputDirectoryName) {
		this.outputDirectoryName = outputDirectoryName;
	}

	public PrintWriter getWriter() {
		return writer;
	}

	public void setWriter(PrintWriter writer) {
		this.writer = writer;
	}
	
	public void read(String inputFileName) {
		initReader(inputFileName);
		readHeaders();
		initReader(inputFileName);
		readRows();
	}

	public void initReader(String fileName) {
		try {
			reader = Files.lines(Paths.get(ClassLoader.getSystemResource(fileName).toURI()));
		
		} catch (IOException e) {
			e.printStackTrace();
			
		} catch (URISyntaxException e) {
			e.printStackTrace();
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}

	public void readHeaders() {
		addColumns(reader.findFirst().get().split(","));
		
	}

	public void readRows() {
		reader.skip(1).forEachOrdered(row -> {
			addRow(row.split(","));
		});
	}

	public List<Object> getRow(int row) {
		return data.get(row);
	}
	
	public Object getData(int row, String header) {
		String headerEscaped = "\""+header+"\"";
		int column = headers.indexOf(headerEscaped);
		if (column != -1) {
			return getData(row, column);
		} else {
			System.err.println("NDB getData()~headerEscaped "+headerEscaped+" not found in headers="+headers);
			return null;
			
		}
	}

	public Object getData(int row, int column) {
		return data.get(column).get(row);
	}

	public void write(String outputFileName) {
		initWriter(outputFileName);
		writeHeaders();
		writeRows();
	}
	
	public void initOutputFile(String outputFileName) {
		File outputDirectory = new File(getOutputDirectoryName());
		if (!outputDirectory.exists()) {
			outputDirectory.mkdirs();
		}
		this.outputFileName = outputFileName;
	}
	
	public PrintWriter initWriter(String outputFileName) {
		File outputDirectory = new File(getOutputDirectoryName());
		if (!outputDirectory.exists()) {
			outputDirectory.mkdirs();
		}
		try {
			writer = new PrintWriter(new String(getOutputDirectoryName()+"/"+outputFileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			writer = new PrintWriter(System.out);
		}
		return writer;
	}
	
	public void setDoubleFormat(String doubleFormat) {
		this.doubleFormat = doubleFormat;
	}

	public void writeHeaders() {
		StringBuffer headerString = new StringBuffer();
		for (String header: headers) {
			headerString.append("\""+header+"\",");
		}
		headerString.replace(headerString.length()-1, headerString.length(),"\n");
		
		if (writer != null) {
			writer.print(headerString);

			// Need to keep flushing the print stream otherwise the output is truncated :(
			writer.flush();
		} else if (outputFileName != null) {
			try {
				Files.write(
						Paths.get(new String(getOutputDirectoryName()+"/"+outputFileName)), 
						headerString.toString().getBytes(), 
						StandardOpenOption.CREATE, 
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void writeRows() {
		if (numberOfRows > 0) {
			for (int row = 0; row < numberOfRows; row++) {
				writeRow(row);
			}
		} 
		
	}
	
	public void writeLastRow() {
		if (numberOfRows > 0) {
			writeRow(numberOfRows-1);
		}
		
	}
	
	public void writeRow(int row) {
		StringBuffer rowString = new StringBuffer();
		for (int c = 0; c < headers.size(); c++) {
			Object value = data.get(c).get(row);
			if (value instanceof Double) {
				rowString.append("\""+String.format(doubleFormat, value)+"\",");
			} else {
				rowString.append("\""+value+"\",");
			}
			if (isVolatile) {
				data.get(c).remove(value);
			}
		}

		rowString.replace(rowString.length()-1, rowString.length(),"\n");
		
		if (writer != null) {
			writer.print(rowString);

			// Need to keep flushing the print stream otherwise the output is truncated :(
			writer.flush();
			
		} else if (outputFileName != null) {
			try {
				Files.write(
						Paths.get(new String(getOutputDirectoryName()+"/"+outputFileName)), 
						rowString.toString().getBytes(), 
						StandardOpenOption.CREATE, 
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (isVolatile) {
			numberOfRows -= 1;
		}
		
	}
	
	public void writeVolatileRow(Object ... rowData) {
		if (!isVolatile) {
			// We got problems!
			System.err.println("NDB writeVolatileRow()~trying to volatile write in a non volatile DataFrame.");
		}
		if (rowData.length != numberOfColumns) {
			// We got problems!
			System.err.println("NDB writeVolatileRow()~rowData.length "+rowData.length+" != nextColumnIndex "+numberOfColumns);
		}

		StringBuffer rowString = new StringBuffer();
		for (int i = 0; i < rowData.length; i++) {
			//data.get(i).add(rowData[i]);
			Object value = rowData[i];
			if (value instanceof Double) {
				rowString.append("\""+String.format(doubleFormat, value)+"\",");
			} else {
				rowString.append("\""+value+"\",");
			}
		}
		
		rowString.replace(rowString.length()-1, rowString.length(),"\n");
		
		if (writer != null) {
			writer.print(rowString);

			// Need to keep flushing the print stream otherwise the output is truncated :(
			writer.flush();
			
		} else if (outputFileName != null) {
			try {
				Files.write(
						Paths.get(new String(getOutputDirectoryName()+"/"+outputFileName)), 
						rowString.toString().getBytes(), 
						StandardOpenOption.APPEND);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
}
