package ie.nix.mp.ecj;

import ie.nix.util.Halo;
import ie.nix.util.ToString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.EvolutionState;
import ec.vector.BitVectorIndividual;

public class Rule extends BitVectorIndividual {

	private static final long serialVersionUID = 6793904500275071109L;

	// TODO make this a parameter
	public static int NEIGHBOURHOOD_SIZE = 3; 
	
	public static final int NUMBER_OF_NIGHBOURS = (2*NEIGHBOURHOOD_SIZE)+1;

	public enum Consensus {
		TRUE(true,true),
		FALSE(true,false),
		NO(false,false);
		
		private boolean hasConsensus;
		private boolean majority;

		Consensus(boolean hasConsensus, boolean majority) {
			this.hasConsensus = hasConsensus;
			this.majority = majority;
		}
		
	    public static Consensus getConsensusIgnoreHalo(boolean[] automota) {
	    	int sum = 0;
			for (int a = NEIGHBOURHOOD_SIZE; a < automota.length-NEIGHBOURHOOD_SIZE; a++) {
				if (automota[a]) {
					sum++;
				}
			}
			if (sum == automota.length-(2*NEIGHBOURHOOD_SIZE)) {
				return TRUE;
			} else if (sum == 0) {
				return FALSE;
			} else {
				return NO;
			}
		}
	    
	    public static Consensus getConsensus(boolean[] automota) {
	    	int sum = 0;
			for (int a = 0; a < automota.length; a++) {
				if (automota[a]) {
					sum++;
				}
			}
			if (sum == automota.length) {
				return TRUE;
			} else if (sum == 0) {
				return FALSE;
			} else {
				return NO;
			}
		}
	    
	    public boolean hasConsensus() {
	    	return hasConsensus;
	    }

	    public boolean getMajority() {
	    	if (hasConsensus) {
	    		return majority;
	    	} else {
				throw new RuntimeException("You cannot get the majority when their is no consensus. Call hasConsensus() first to check.");
	    	}
	    }
		
	};

	// For json to write experiments.
	public Rule() {
	}
	
	// For testing and running rules over IC to generate space-time diagrams
	Rule(boolean[] genome) {
		this.genome = new boolean[genome.length];
		reset(genome);
	}

	public void reset(boolean[] genome) {
		if (genome.length != this.genome.length) {
			throw new RuntimeException("You cannot set a genome of length "+genome.length+
					" to a rule with a genome of length "+this.genome.length);
		}
		System.arraycopy(genome, 0, this.genome, 0, genome.length);
    }
    
    public void reset(EvolutionState state, int thread, double lambda) {
		if (lambda == 0.0) {
			Arrays.fill(genome, false);
		} else if (lambda == 1.0) {
			Arrays.fill(genome, true);
		} else {
			double lambdaCount = lambda * genome.length;
			double remainingLambdaTarget = lambda;
			double remainingLambdaBitCount = genome.length;
//    			System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
//    					+ "lambdaCount="+lambdaCount+", "
//    					+ "remainingLambdaTarget="+remainingLambdaTarget);
			for (int a = 0; a < genome.length; a++) {
				double rand = state.random[thread].nextDouble();
				if (rand < remainingLambdaTarget) {
					genome[a] = true;
					lambdaCount--;
				} else {
					genome[a] = false;
				}
				remainingLambdaBitCount--;
//    				System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
//    						+ "lambdaCount="+lambdaCount+", "
//    						+ "remainingLambdaTarget="+String.format("%.2f", remainingLambdaTarget)+", "
//    						+ "remainingLambdaBitCount="+remainingLambdaBitCount+", "
//    						+ "rand="+String.format("%.2f", rand)+", "
//    						+ "bitset["+a+"]="+bitset[a]); 
				remainingLambdaTarget = lambdaCount/remainingLambdaBitCount;
			}
		}
	}

	public double getLambda() {
		double sum = 0;
		for (boolean g : genome) {
			if (g) {
				sum++;
			}
		}
		return sum/genome.length;
	}

	public boolean getAction(boolean[] automotaWithHalo, int automoton) {
		int neighbourhoodStateIndex = 
				 (automotaWithHalo[automoton+6] ? 1 : 0)  + 
				((automotaWithHalo[automoton+5] ? 1 : 0) << 1) + 
				((automotaWithHalo[automoton+4] ? 1 : 0) << 2) + 
				((automotaWithHalo[automoton+3] ? 1 : 0) << 3) + 
				((automotaWithHalo[automoton+2] ? 1 : 0) << 4) + 
				((automotaWithHalo[automoton+1] ? 1 : 0) << 5) + 
				((automotaWithHalo[automoton] ? 1 : 0) << 6);
		return genome[neighbourhoodStateIndex];
		
	}

	public boolean[] evaluate(InitialConfiguration ic, int maxNumberOfSteps) {
		return evaluate(ic, maxNumberOfSteps, null);
		
	}
	
	public boolean[] evaluate(InitialConfiguration ic, int maxNumberOfSteps, boolean[][] spaceTimeData) {
		// This allocates and clones the initial configuration of the automata
		boolean[] automataState = ic.getTest();
		int step = 0;
		Consensus consensus = Consensus.NO;
		
		// set up the initial configuration into the current step state array and update the halos
		boolean[] automataStateWithHalos = new boolean[automataState.length+(2*NEIGHBOURHOOD_SIZE)];
		System.arraycopy(automataState, 0, automataStateWithHalos, NEIGHBOURHOOD_SIZE, automataState.length);
		Halo.updateHalos(automataStateWithHalos, NEIGHBOURHOOD_SIZE);
		
		// Copy the initial conditions into the space time data
		if (spaceTimeData != null) {
			System.arraycopy(automataState, 0, spaceTimeData[step], 0, automataState.length);
		}
		
		// Set up the array to hold the next calculated state.
		boolean[] nextStepStateWithHalos = new boolean[automataStateWithHalos.length];
		
		while (!consensus.hasConsensus() && step < maxNumberOfSteps) {
			// Calculate the next step state based on rule action and update the halos
			for (int a = 0; a < automataState.length;a++) {
				nextStepStateWithHalos[a+NEIGHBOURHOOD_SIZE] = getAction(automataStateWithHalos, a);
				
			}
			Halo.updateHalos(nextStepStateWithHalos, NEIGHBOURHOOD_SIZE);
			
			// Copy the next step state into the current step state for the next evaluation.
	    	System.arraycopy(nextStepStateWithHalos, 0, automataStateWithHalos, 0, nextStepStateWithHalos.length);
			
	    	// Check for a consensus.
	    	consensus = Consensus.getConsensusIgnoreHalo(automataStateWithHalos);

			// Copy the next step into the space time data
			if (spaceTimeData != null) {
				System.arraycopy(automataStateWithHalos, NEIGHBOURHOOD_SIZE, spaceTimeData[step+1], 0, spaceTimeData[step].length);
			}
			step++;

		}
//		System.out.println("NDB::evaluate()~"+ToString.toString(convert(Halo.removeHalos(automataStateWithHalos, 3))));

		System.arraycopy(automataStateWithHalos, NEIGHBOURHOOD_SIZE, automataState, 0, automataState.length);

		// Pad out the final steps in the space time data
		if (spaceTimeData != null) {
			while (step < maxNumberOfSteps) {
				System.arraycopy(automataState, 0, spaceTimeData[step+1], 0, spaceTimeData[step].length);
				step++;
			}
		}
		return automataState;
		
	}
	
}
