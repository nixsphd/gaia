package ie.nix.mp.ea;

import java.util.ArrayList;
import java.util.List;

import ie.nix.ea.Agent;
import ie.nix.ea.Population;
import ie.nix.ea.operators.EliteSelection;
import ie.nix.ea.operators.FlippingMutation;
import ie.nix.ea.operators.Mask;
import ie.nix.ea.operators.OnePointCrossover;
import ie.nix.ea.populations.PopulationOfIntegers;
import ie.nix.mp.MajorityProblemExperiment;
import ie.nix.util.Experiment;
import ie.nix.util.Randomness;

public class Mitchell extends GeneticAlgorithmExperiment<Integer> {
	
	public static void main(String[] args) {
		
		Mitchell experiment = (Mitchell)init(args, Mitchell.class);
		if (experiment != null) {
			double fitness = experiment.runExperiment();
	
			stopWatch.stop();
	        System.out.println( "#Seed "+Experiment.getSeed()+" gives fitness of best evolved rule is "+String.format("%.3f", fitness)+" on the gaussian evaluation experiment"
	        		+ " in "+String.format("%.2f", (stopWatch.getTime()/1000.0))+" secs");
	        
		} else {
		    System.out.println( "#Done.");
		    
		}
	}
	
	protected static Integer[][] generateGenomes(int lenghtOfGenotype, int numberOfAgents) {
		Integer[][] population = new Integer[numberOfAgents][lenghtOfGenotype];
		for (int a = 0; a < numberOfAgents; a++) {
			for (int g = 0; g < lenghtOfGenotype; g++) {
				population[a][g] = Randomness.nextBinary();
			}
//				System.out.println("NDB~generateUniformPreCannedPopulation() nextBin="+nextBin+" gave run with lambda="+MajorityProblemExperiment.getLambda(population[a])//);
//						+" for automota="+ToString.toString(population[a]));
		}
		return population;
	}

	public static int[][] generateUniformTests(int numberOfTests, int numberOfAutomota) {
		int[][] tests = new int[numberOfTests][];
		for (int t = 0; t < numberOfTests; t++) {
			double lambda;
			if (t%2 == 0) {
				lambda = ((double)Randomness.nextInt(0,numberOfAutomota/2))/numberOfAutomota;
			} else {
				lambda = ((double)Randomness.nextInt((numberOfAutomota/2)+1,numberOfAutomota))/numberOfAutomota;
			}
			int[] automata = new int[numberOfAutomota];
			MajorityProblemExperiment.generateBitsetWithLambda(automata, lambda);
			tests[t] = automata;
//			System.out.println("NDB requested lambda="+String.format("%.2f", lambda)+
//					" gave run with actual lambda="+String.format("%.2f", MajorityProblemExperiment.getLambda(automata)));
		}
		return tests;
	}
	
	public static Integer[][] generateUniformGenomes(int lenghtOfGenotype, int numberOfAgents) {
		Integer[][] population = new Integer[numberOfAgents][];
		for (int a = 0; a < numberOfAgents; a++) {
			double lambda = ((double)Randomness.nextInt(0,lenghtOfGenotype))/lenghtOfGenotype;
			Integer[] genes = new Integer[lenghtOfGenotype];
			MajorityProblemExperiment.generateBitsetWithLambda(genes, lambda);
			population[a] = genes;
//			System.out.println("NDB~generateUniformGenomes() requested lambda="+lambda+
//					" gave run with actual lambda="+MajorityProblemExperiment.getLambda(population[a]));
		}
		return population;
	}
	
	protected int numberOfElites;
	protected double mutationProbabillity;
	protected double crossoverProbabillity;
	
	public Mitchell() {
		this(1000,200);
	}
	

	public Mitchell(int numberOfAgents, int numberOfGenerations) {
		super(numberOfAgents, numberOfGenerations);
		
		setGenomes(generateUniformGenomes(lenghtOfGenotype, numberOfAgents));
		setNumberOfElites(20); // Mitchell selects 20 elites
		setMutationProbabillity(2.0/(Math.pow(2,(neighbourhoodSize*2)+1)));// Mitchell mutates twice per genome
		setCrossoverProbabillity(1.0); // There is always crossover.
	}

	public void initTrainingExperiment() {		
		int numberOfTrainingTests = 100; 
		MajorityProblemExperiment trainingExperiment = new MajorityProblemExperiment(
				Mitchell.generateUniformTests(numberOfTrainingTests*numberOfGenerations, numberOfAutomota)); // From Mitchell
		// Comes from Kaza's Paper;
		trainingExperiment.setNumberOfTests(numberOfTrainingTests);
		trainingExperiment.setNumberOfStepsPoisson(true);
		setTrainingExperiment(trainingExperiment);
	}
	
	public void initGeneticAlgorithm() {
	
		// Set up the GA
		ga = new GeneticAlgorithm<Integer>(getGenomes()) {
			
			@Override
			protected Population<Integer> initPopulation() {
				return new PopulationOfIntegers(getLenghtOfGenotype());
				
			}
			
			@Override
			protected List<Agent<Integer>> initialiseAgents(int numberOfAgents) {
				List<Agent<Integer>> agents = new ArrayList<Agent<Integer>>(numberOfAgents);
				for (int a = 0; a < numberOfAgents; a++) {
					agents.add(new GeneticAlgorithmDeterministicRule());
				}
				return agents;
			}
			
		};
		
		// Set up the agents
		for (Agent<Integer> agent: ga.getAgents()) {
			GeneticAlgorithmDeterministicRule mpAgent = (GeneticAlgorithmDeterministicRule)agent;
			mpAgent.setExperiment(getTrainingExperiment());
		}
		ga.evaluate();
		
		// Selection
		EliteSelection<Integer> eliteSelection = new EliteSelection<Integer>(getNumberOfElites());
		ga.addGeneticOperator(eliteSelection);
		// Add the mask
		Mask<Integer> mask = new Mask<Integer>(eliteSelection.generateMask(getNumberOfAgents()));
		ga.addGeneticOperator(mask);
		
		// Flip Mutation operator
		FlippingMutation flippingMutation = new FlippingMutation(getMutationProbabillity());
		ga.addGeneticOperator(flippingMutation);
		
		// Crossover
		OnePointCrossover<Integer> onePointCrossover = new OnePointCrossover<Integer>(getCrossoverProbabillity());
		ga.addGeneticOperator(onePointCrossover);
		
		// And unmask again finally
		ga.addGeneticOperator(mask);
	
	}

	public int getNumberOfElites() {
		return numberOfElites;
	}

	public void setNumberOfElites(int numberOfElites) {
		this.numberOfElites = numberOfElites;
	}

	public double getMutationProbabillity() {
		return mutationProbabillity;
	}

	public void setMutationProbabillity(double mutationProbabillity) {
		this.mutationProbabillity = mutationProbabillity;
	}

	public double getCrossoverProbabillity() {
		return crossoverProbabillity;
	}

	public void setCrossoverProbabillity(double crossoverProbabillity) {
		this.crossoverProbabillity = crossoverProbabillity;
	}
	
}
