package ie.nix.mp.ecj;

import ec.*;
import ec.simple.SimpleStatistics;
import ec.util.*;
import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;
import ie.nix.util.ToString;

import java.io.*;
import java.util.stream.IntStream;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import ec.vector.*;

/*
	protected static DataFrame getGemonesDataFrame() {
		String genomesFileName = "Genomes.csv";
		// Process the space-time diagrams filename
		DataFrame gdf = new DataFrame();
		gdf.setOutputDirectoryName(Experiment.getExperimentDirectory());
		gdf.initWriter(genomesFileName);
		return gdf;
	}
	
 */

@SuppressWarnings("serial")
public class TestSetStatistics extends Statistics {
	
	// The parameter string and log number of the file for our readable population
    public static final String P_FILE = "file";
    
    public int log;
    
    protected InitialConfiguration[] ics;
    
    protected InitialConfiguration[] getICs(final EvolutionState state) {
	    if (ics == null) {
			ics = ((MajorityProblem)state.evaluator.p_problem).getTestSet().getTests();
		}
	    return ics;
    }
    
    protected String getHeaderRow() {
    	return "\"Generation\", \"IC\", \"Lambda\"";
    }

    protected String getRow(final EvolutionState state, int test) {
		return (state.generation+1)+", "+test+", "+getICs(state)[test].getLambda();
	}

    public void setup(final EvolutionState state, final Parameter base) {
        
    	// DO NOT FORGET to call super.setup(...) !!
        super.setup(state,base);
        
        // We will set up a log called "pop-file". This is done through our Output object. 
        // Of course we could just write to the file stream ourselves; but it is 
        // convenient to use the Output logger because it recovers from checkpoints 
        // and it's threadsafe.
        // set up popFile
        File file = state.parameters.getFile(base.push(P_FILE),null);

        // We tell Output to add a log file writing to "pop-file". We will NOT post 
        // announcements to the log (no fatal, error, warning messages etc.). The log 
        // WILL open via appending (not rewriting) upon a restart after a checkpoint failure. 
        if (file != null) {
        	try {
        		log = state.output.addLog(file,true);
        	} catch (IOException i) {
        		state.output.fatal("An IOException occurred while trying to create the log " + 
        				file + ":\n" + i);
            }
        }
        
//        if (state.evaluator.p_problem instanceof SkillAwareMajorityProblem) {
//        	skillAwareMajorityProblem = (SkillAwareMajorityProblem)state.evaluator.p_problem;
//        	state.output.println("\"Generation\", \"IC\", \"Lambda\", \"Skill\"", log);
//        	
//        } else if (state.evaluator.p_problem instanceof MajorityProblem) {
        	
//        	majorityProblem = (MajorityProblem)state.evaluator.p_problem;
        	state.output.println(getHeaderRow(), log);
//        }
  		
    }

	@Override
	public void postEvaluationStatistics(EvolutionState state) {
		super.postEvaluationStatistics(state);
		for(int ic = 0; ic < getICs(state).length; ic++) {
			state.output.println(getRow(state, ic), log);
	        
		}
		ics = null;
			
	}
    
}
