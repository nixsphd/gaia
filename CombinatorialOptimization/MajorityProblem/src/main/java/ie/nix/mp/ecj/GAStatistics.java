package ie.nix.mp.ecj;

import ec.*;
import ec.simple.SimpleStatistics;
import ec.util.*;
import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;

import java.io.*;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import ec.vector.*;

/*
 * 	protected static DataFrame getGADataFrame() {
		String genomesFileName = "GA.csv";
		// Process the space-time diagrams filename
		DataFrame gadf = new DataFrame(true);
		gadf.setOutputDirectoryName(Experiment.getExperimentDirectory());
		gadf.addColumns(new String[]{"Generation","Training Best Evaluation", "Training Average Evaluation", "Training Standard Deviation", "Best Evaluation"});
		gadf.initWriter(genomesFileName);
		gadf.writeHeaders();
		return gadf;
	}

	protected static DataFrame getBestRulesDataFrame() {
		String bestRulesFileName = "BestRules.csv";
	
	
 */

@SuppressWarnings("serial")
public class GAStatistics extends Statistics {
	
	// The parameter string and log number of the file for our readable population
    public static final String P_FILE = "file";
    
    public int log;
    
    protected TestSet evaluationTestSet;
    
    protected String getHeaderRow() {
    	return "\"Generation\", \"Training Best Evaluation\", \"Training Average Evaluation\","
				+ " \"Training Standard Deviation\", \"Best Evaluation\"";
    }

    protected String getRow(final EvolutionState state) {
		// Get a DescriptiveStatistics instance
		DescriptiveStatistics stats = new DescriptiveStatistics();
		
		Individual bestInd = state.population.subpops[0].individuals[0];
		stats.addValue(bestInd.fitness.fitness());
		
		for(int y = 0; y < state.population.subpops[0].individuals.length; y++) {
			// Add their lambda values to the statistucs object.
			Individual ind =  state.population.subpops[0].individuals[y];
			stats.addValue(ind.fitness.fitness());
			if (ind.fitness.betterThan(bestInd.fitness)) {
				bestInd = ind;
			}
		}
	
		Rule bestRule = (Rule)(bestInd).clone();
		double evaluationFitness = MajorityProblem.getFitness(bestRule, evaluationTestSet, 320);
		
		// Compute some statistics
		double trainingAvgEvaluation = stats.getMean();
		double trainingStandardDeviation = stats.getStandardDeviation();

//		System.out.println("NDB postEvaluationStatistics()~bestInd.fitness.fitness()="+bestInd.fitness.fitness());
//		System.out.println("NDB postEvaluationStatistics()~trainingAvgEvaluation="+trainingAvgEvaluation);
//		System.out.println("NDB postEvaluationStatistics()~trainingStandardDeviation="+trainingStandardDeviation);
//		System.out.println("NDB postEvaluationStatistics()~evaluationFitness="+evaluationFitness);
		
		return state.generation+1+", "+bestInd.fitness.fitness()+", "+trainingAvgEvaluation+", "+trainingStandardDeviation+", "+evaluationFitness;
	
	}

	public void setup(final EvolutionState state, final Parameter base) {
        
    	// DO NOT FORGET to call super.setup(...) !!
        super.setup(state,base);
        
        // We will set up a log called "pop-file". This is done through our Output object. 
        // Of course we could just write to the file stream ourselves; but it is 
        // convenient to use the Output logger because it recovers from checkpoints 
        // and it's threadsafe.
        // set up popFile
        File file = state.parameters.getFile(base.push(P_FILE),null);

        // We tell Output to add a log file writing to "pop-file". We will NOT post 
        // announcements to the log (no fatal, error, warning messages etc.). The log 
        // WILL open via appending (not rewriting) upon a restart after a checkpoint failure. 
        if (file != null) {
        	try {
        		log = state.output.addLog(file,true);
        		
        	} catch (IOException i) {
        		state.output.fatal("An IOException occurred while trying to create the log " + 
        				file + ":\n" + i);
            }
        }
		evaluationTestSet = new TestSet(state, 100000, 149, true);
		
        state.output.println(getHeaderRow(), log);
  		
    }
    
    public void postEvaluationStatistics(final EvolutionState state) {
    	
    	super.postEvaluationStatistics(state);
    	
  		//"Generation","Training Best Evaluation","Training Average Evaluation","Training Standard Deviation","Best Evaluation"
		// In ECJ the generations run gtom 0 to 99 but my R scripts expect them from 1 to 100, so I add one to the log file.
		state.output.println(getRow(state), log);
        
    }
    
}
