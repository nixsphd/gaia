package ie.nix.mp.ecj.gp;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import ec.util.Parameter;

public class And extends GPNode {

	private static final long serialVersionUID = -443255979370514048L;

	// Add needs to specify its printed form ("+"), and a function (expectedChildren(...) ) which give Add a 
	// chance to do a sanity check when first loaded, to verify that the child array size is correct. 
	public String toString() { 
    	return "&&"; 
    }

    public int expectedChildren() { 
    	return 2; 
    }
    
    // Instead of expectedChildren(...), you could have instead written:
    // ECJ will call the checkConstraints() method, which gives you a chance to do more sophisticated checks. 
    // Above we're doing more or less what the default version of checkConstraints() does anyway, which 
    // is just call expectedChildren() and verify that the children.length variable is the same as it. Thus 
    // if you don't need to do anything more than a sanity check on the number of child nodes, just implement 
    // expectedChildren() and don't bother with checkConstraints().
	public void checkConstraints(
			final EvolutionState state,
			final int tree,
			final GPIndividual typicalIndividual,
			final Parameter individualBase) {
		super.checkConstraints(state,tree,typicalIndividual,individualBase);
		if (children.length!=2) {
			state.output.error("Incorrect number of children for node " + toStringForError() + " at " + individualBase);
			
		}
	}

    // Next we will define the central function to the GPNode: the eval method, which is called when the GPNode 
    // must do its part during the execution of the tree as s-expression computer code. eval is called by the 
    // GPNode's parent to execute the GPNode and get its return value.
    //
    //The eval method is passed a bunch of stuff, including the EvolutionState, the current thread number 
    // (so you can get a random number via state.random[thread], an ADFStack (don't worry about this -- just 
    // pass it on to your children), the GPIndividual which is executing, the Problem which is executing the 
    // GPIndividual, and the GPData used to pass information to and from the GPNode's parent.
	@Override
	public void eval(
			EvolutionState state, 
			int thread, 
			GPData input,
			ADFStack stack, 
			GPIndividual individual, 
			Problem problem) {
	  
		// What we're doing here is simple enough: we evaluate our left child, then we evaluate our right child,
		// then we add the two together and return that result. Note that we reuse the GPData object rather than 
		// allocating new ones. You are free to allocate a new GPData object to pass to your children, but you
		// should use the GPData object provided you when you return a value to your parent. But I suggest you 
		// reuse the GPData object to keep from allocating millions of objects during evaluation. 
		boolean result;
		BitData rd = ((BitData)(input));
		
		children[0].eval(state,thread,input,stack,individual,problem);
		result = rd.x;
		
		children[1].eval(state,thread,input,stack,individual,problem);
		rd.x = result && rd.x;
		
	}
    
}
