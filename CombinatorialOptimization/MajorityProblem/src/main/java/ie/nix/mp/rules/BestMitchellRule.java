package ie.nix.mp.rules;

import ie.nix.mp.DeterministicRule;

public class BestMitchellRule extends DeterministicRule {

//	Best rule evolved by genetic programming (in this paper)
//	00000101 00000000 01010101 00000101 00000101 00000000 01010101 00000101 01010101 11111111 01010101 11111111 01010101 11111111 01010101 11111111
	public static final int[] BEST_MITCHELL_POLICY = {
		0,0,0,0,0,1,0,1, 
		0,0,0,0,0,0,0,0, 
		0,1,0,1,0,1,0,1, 
		0,0,0,0,0,1,0,1, 
		0,0,0,0,0,1,0,1, 
		0,0,0,0,0,0,0,0, 
		0,1,0,1,0,1,0,1, 
		0,0,0,0,0,1,0,1, 
		0,1,0,1,0,1,0,1, 
		1,1,1,1,1,1,1,1, 
		0,1,0,1,0,1,0,1, 
		1,1,1,1,1,1,1,1, 
		0,1,0,1,0,1,0,1, 
		1,1,1,1,1,1,1,1, 
		0,1,0,1,0,1,0,1, 
		1,1,1,1,1,1,1,1,
	};

	public BestMitchellRule() {
		super(BEST_MITCHELL_POLICY);
	}
}
