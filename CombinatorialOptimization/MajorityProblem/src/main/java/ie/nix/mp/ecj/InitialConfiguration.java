package ie.nix.mp.ecj;

import ie.nix.util.Equals;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.EvolutionState;

public class InitialConfiguration {
	
	boolean[] test;
	boolean majority;
	
	public static boolean getMajority(boolean[] automata) {
		double lambda = getLambda(automata);
		if (lambda < 0.5) {
			return false;
		} else if (lambda > 0.5) {
			return true;
		} else {
			throw new RuntimeException("The majority for a test with an even number of automata is undefined.");
			//return -1; // Undefined really.
		}
	}

	public static double getLambda(boolean[] automata) {
		double sum = 0;
		for (boolean a : automata) {
			if (a) {
				sum++;
			}
		}
//		System.out.println("NDB getLambda->"+sum/automata.length);
		return sum/automata.length;
	}

	InitialConfiguration(boolean[] initialConditions) {
		this.test = initialConditions;
		this.majority = getMajority(test);
		
	}

	public InitialConfiguration(EvolutionState state, int numberOfAutomata) {
		test = new boolean[numberOfAutomata];
		generateRandomTest(state);
		
	}
	
	public InitialConfiguration(EvolutionState state, int numberOfAutomata, double lambda) {
		test = new boolean[numberOfAutomata];
		generateTestWithLambda(state, lambda);
		
	}
	
	public InitialConfiguration(EvolutionState state, int numberOfAutomata, boolean majority) {
		test = new boolean[numberOfAutomata];
		generateTestWithMajority(state, majority);
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof InitialConfiguration) {
			InitialConfiguration test = (InitialConfiguration)obj;
			return Equals.equals(test.getTest(), this.getTest());
		}
		return super.equals(obj);
	}
	
	public boolean[] getTest() {
		boolean[] ic = new boolean[test.length];
		System.arraycopy(test, 0, ic, 0, test.length);
		return ic;
		
	}
	
	public boolean getMajority() {
		return majority;
	}

	public double getLambda() {
		return getLambda(test);
	}

	protected void generateRandomTest(EvolutionState state) {
		for (int a = 0; a < test.length; a++) {
			test[a] = state.random[0].nextBoolean();
		}
		this.majority = getMajority(test);
		
	}

	protected void generateTestWithMajority(EvolutionState state, boolean majority) {
		do {
			generateRandomTest(state);
		} while (getMajority() != majority); 
		
	}

	protected void generateTestWithLambda(EvolutionState state, double lambda) {
		if (lambda == 0.0) {
			Arrays.fill(test, false);
		} else if (lambda == 1.0) {
			Arrays.fill(test, true);
		} else {
			double lambdaCount = lambda * test.length;
			double remainingLambdaTarget = lambda;
			double remainingLambdaBitCount = test.length;
//			System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
//					+ "lambdaCount="+lambdaCount+", "
//					+ "remainingLambdaTarget="+remainingLambdaTarget);
			for (int a = 0; a < test.length; a++) {
//					double rand = Randomness.nextDouble();
				double rand = state.random[0].nextDouble();
				if (rand < remainingLambdaTarget) {
					test[a] = true;
					lambdaCount--;
				} else {
					test[a] = false;
				}
				remainingLambdaBitCount--;
//				System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
//						+ "lambdaCount="+lambdaCount+", "
//						+ "remainingLambdaTarget="+String.format("%.2f", remainingLambdaTarget)+", "
//						+ "remainingLambdaBitCount="+remainingLambdaBitCount+", "
//						+ "rand="+String.format("%.2f", rand)+", "
//						+ "bitset["+a+"]="+bitset[a]); 
				remainingLambdaTarget = lambdaCount/remainingLambdaBitCount;
			}
		}
		this.majority = getMajority(test);
		
	}

}
