package ie.nix.mp.rules;

import ie.nix.mp.MajorityProblemExperiment;
import ie.nix.mp.Rule;
import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;

public class FitnessAgainstLambda extends Experiment {
	
	protected MajorityProblemExperiment evaluationExperiment;
	protected Rule[] rules;
	protected double lambdaPointsStep; 
	protected int numberOfTestsPerLambdaPoint;
	protected int[] numbersOfAutomota;
	
	// #Done in 392.82 secs, GKL fitness is 81.53% Davis fitness is 81.76% Das fitness is 82.14%
	
	public FitnessAgainstLambda() {
		super();

		lambdaPointsStep = 0.0005; 
		numberOfTestsPerLambdaPoint = 1000;
		numbersOfAutomota = new int[] {
				149, 499, 999
		};
		rules = new Rule[] {
			new GKLRule(),
			new DasRule(),
			new DavisRule(),
			new BestKozaRule(),
		};
		
		evaluationExperiment = new MajorityProblemExperiment();
		evaluationExperiment.setNumberOfTests(numberOfTestsPerLambdaPoint);
		
	}
	
	public MajorityProblemExperiment getEvaluationExperiment() {
		return evaluationExperiment;
	}

	public void setEvaluationExperiment(
			MajorityProblemExperiment evaluationExperiment) {
		this.evaluationExperiment = evaluationExperiment;
	}

	public Rule[] getRules() {
		return rules;
	}

	public void setRules(Rule[] rules) {
		this.rules = rules;
	}

	public double getLambdaPointsStep() {
		return lambdaPointsStep;
	}

	public void setLambdaPointsStep(double lambdaPointsStep) {
		this.lambdaPointsStep = lambdaPointsStep;
	}

	public int getNumberOfTestsPerLambdaPoint() {
		return numberOfTestsPerLambdaPoint;
	}

	public void setNumberOfTestsPerLambdaPoint(int numberOfTestsPerLambdaPoint) {
		this.numberOfTestsPerLambdaPoint = numberOfTestsPerLambdaPoint;
	}

	public int[] getNumbersOfAutomota() {
		return numbersOfAutomota;
	}

	public void setNumberOfAutomota(int[] numbersOfAutomota) {
		this.numbersOfAutomota = numbersOfAutomota;
	}

	protected DataFrame getFitnessVsRulesDataFrame() {
		String dataFrameFileName = "FitnessVsRules.csv";
		// Process the space-time diagrams filename
		DataFrame df = new DataFrame();
		df.setOutputDirectoryName(Experiment.getExperimentDirectory());
		df.setDoubleFormat("%.3f");
		df.initWriter(dataFrameFileName);
		df.addColumns(new String[]{"LambdaPoint", "NumberOfAutomata", "Rule", "Fitness"});
		df.writeHeaders();
		return df;
	}

	public static void main(String[] args) {
		
		FitnessAgainstLambda experiment = (FitnessAgainstLambda)init(args, FitnessAgainstLambda.class);
		if (experiment != null) {
			
			DataFrame df = experiment.getFitnessVsRulesDataFrame();
			for (double lambdaPoint = 0.4; lambdaPoint <= 0.6; lambdaPoint += experiment.getLambdaPointsStep()) {

				for (int numberOfAutomota: experiment.getNumbersOfAutomota()) {

					int[][] tests = MajorityProblemExperiment.generateTestsWithLambda(experiment.getNumberOfTestsPerLambdaPoint(), 
							numberOfAutomota, lambdaPoint);
					experiment.getEvaluationExperiment().setTests(tests);
					
					for (Rule rule: experiment.getRules()) {
						df.addRow(lambdaPoint, numberOfAutomota, rule.getClass().getSimpleName(), 
								MajorityProblemExperiment.getFitnessForRule(rule, experiment.getEvaluationExperiment()));
						df.writeLastRow();
					}
					
				}
				
			}
			
			stopWatch.stop();
		    System.out.println( "#Done in "+String.format("%.2f", (stopWatch.getTime()/1000.0))+" sec");
		    
		} else {
		    System.out.println( "#Done.");
		    
		}
	    
	}

}
