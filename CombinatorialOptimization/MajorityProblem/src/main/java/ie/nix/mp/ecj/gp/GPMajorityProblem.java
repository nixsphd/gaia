package ie.nix.mp.ecj.gp;

import org.apache.commons.math3.random.RandomDataGenerator;

import ie.nix.mp.ecj.InitialConfiguration;
import ie.nix.mp.ecj.Rule;
import ie.nix.mp.ecj.TestSet;
import ie.nix.mp.ecj.Rule.Consensus;
import ie.nix.util.Halo;
import ec.EvolutionState;
import ec.Individual;
import ec.gp.GPIndividual;
import ec.gp.GPProblem;
import ec.simple.SimpleFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;

public class GPMajorityProblem extends GPProblem implements SimpleProblemForm {

	private static final long serialVersionUID = -7609354049349870294L;

	// TODO make this a parameter
	public static int NEIGHBOURHOOD_SIZE = 3; 
	
	public static final int NUMBER_OF_NIGHBOURS = (2*NEIGHBOURHOOD_SIZE)+1;
	
	// majority-problem
	public static final String P_MAJORITY_PROBLEM = "gp-majority-problem";
	
	// majority-problem.number-of-tests
	public static final String P_NUMBER_OF_TESTS = "number-of-tests";
	
	// majority-problem.testset-type
	public static final String P_TESTSET_TYPE = "testset-type";
	public static final String P_TESTSET_TYPE_BALANCED = "~ ";
	public static final String P_TESTSET_TYPE_UNIFORM = "uniform";

	// majority-problem.number-of-automata
	public static final String P_NUMBER_OF_AUTOMATA = "number-of-automata";
	
	// majority-problem.max-number-of-steps
	public static final String P_MAX_NUMBER_OF_STEPS = "max-number-of-steps";
	
	// majority-problem.is-number-of-steps-poisson
	public static final String P_IS_NUMBER_OF_STEPS_POISSON = "is-number-of-steps-poisson";
	
//	public static final String P_DATA = "data";

	protected int numberOfTests; 
	protected String testSetType;
	protected int numberOfAutomata;
	protected int maxNumberOfSteps;
	protected boolean isNumberOfStepsPoisson;

	protected RandomDataGenerator rngFromApache;
	protected TestSet testSet;
	
	private boolean[] automataStateWithHalos;
	private int automoton;
	
	public static Parameter base() {
		return new Parameter(P_MAJORITY_PROBLEM);
	}

	// GPProblem defines an instance variable called input, which contains a GPData instance loaded from our 
	// parameters. Here, we get a chance to verify that this instance is of a class we can use. In our case,
	// we want it to be a DoubleData class or some subclass of that.
    public void setup(final EvolutionState state, final Parameter base) {
		
        // very important, remember this
        super.setup(state,base);
        
        // verify our input is the right class (or subclasses from it)
        if (!(input instanceof BitData)) {
            state.output.fatal("GPData class must subclass from " + BitData.class, base.push(P_DATA), null);
        }
        
		Parameter def = base();
		numberOfTests = state.parameters.getIntWithDefault(
			base.push(P_NUMBER_OF_TESTS), 
			def.push(P_NUMBER_OF_TESTS),
			100);

		testSetType = state.parameters.getStringWithDefault(
				base.push(P_TESTSET_TYPE), 
				def.push(P_TESTSET_TYPE), null);
		
		numberOfAutomata = state.parameters.getIntWithDefault(
			base.push(P_NUMBER_OF_AUTOMATA), 
			def.push(P_NUMBER_OF_AUTOMATA),
			149);
		
		maxNumberOfSteps = state.parameters.getIntWithDefault(
			base.push(P_MAX_NUMBER_OF_STEPS), 
			def.push(P_MAX_NUMBER_OF_STEPS), 
			320);
		
		isNumberOfStepsPoisson = state.parameters.getBoolean(
			base.push(P_IS_NUMBER_OF_STEPS_POISSON), 
			def.push(P_IS_NUMBER_OF_STEPS_POISSON), 
			true);
		// If were using a Poisson distribution we'll need to initialise the Poisson number generator. Using Apache's one.
		if (isNumberOfStepsPoisson) {
			rngFromApache = new RandomDataGenerator();
			rngFromApache.reSeed(state.random[0].nextInt());
		}

		System.out.println("NDB MajorityProblem.setup()~numberOfTests="+numberOfTests);
		System.out.println("NDB MajorityProblem.setup()~testSetType="+testSetType);
		System.out.println("NDB MajorityProblem.setup()~numberOfAutomata="+numberOfAutomata);
		System.out.println("NDB MajorityProblem.setup()~maxNumberOfSteps="+maxNumberOfSteps);
		System.out.println("NDB MajorityProblem.setup()~isNumberOfStepsPoisson="+isNumberOfStepsPoisson);
	}

	protected TestSet getTestSet() {
		return testSet;
	}

	protected void setTestSet(TestSet testSet) {
		this.testSet = testSet;
	}

    /** May be called by the Evaluator prior to a series of individuals to 
    evaluate, and then ended with a finishEvaluating(...).  If this is the
    case then the Problem is free to delay modifying the individuals or their
    fitnesses until at finishEvaluating(...).  If no prepareToEvaluate(...)
    is called prior to evaluation, the Problem must complete its modification
    of the individuals and their fitnesses as they are evaluated as stipulated
    in the relevant evaluate(...) documentation for SimpleProblemForm 
    or GroupedProblemForm.  The default method does nothing.  Note that
    prepareToEvaluate() can be called *multiple times* prior to finishEvaluating()
    being called -- in this case, the subsequent calls may be ignored. */
	public void prepareToEvaluate(final EvolutionState state, final int threadnum) {
		if (testSetType == null) {
			setTestSet(new TestSet(state, numberOfTests, numberOfAutomata));
			
		} else if (testSetType.equals(P_TESTSET_TYPE_BALANCED)) {
			setTestSet(new TestSet(TestSet.generateTests(state, numberOfTests, numberOfAutomata, true)));
			
		} else if (testSetType.equals(P_TESTSET_TYPE_UNIFORM)) {
			setTestSet(new TestSet(TestSet.generateUniformTests(state, numberOfTests, numberOfAutomata)));
			
		} else {
			setTestSet(new TestSet(state, numberOfTests, numberOfAutomata));
			
		}
    }
    
	@Override
	public void evaluate(EvolutionState state, Individual individual, int subpopulation, int threadnum) {
    	// don't bother reevaluating
        if (!individual.evaluated)  {
			if(!(individual instanceof GPIndividual)) {
	            state.output.fatal( "The individuals for this problem should be GPIndividual.");
	            
	    	} else {
	    		GPIndividual gpIndividual = (GPIndividual)individual;
	    		
	    		int maxNumberOfSteps = this.maxNumberOfSteps;
	    		if (isNumberOfStepsPoisson) {
	    			maxNumberOfSteps = (int)rngFromApache.nextPoisson(this.maxNumberOfSteps);
	    		}
		        // Evaluate....
		        int matches = 0; 
	    		
	    		for (InitialConfiguration test : testSet.getTests()) {	
	    			
	    			boolean[] evaluation = evaluate(state, gpIndividual, threadnum, test, maxNumberOfSteps, null);
	    			
	    	    	Consensus result = Rule.Consensus.getConsensus(evaluation);
	    			if (result.hasConsensus()) { 
	    				 if (result.getMajority() == test.getMajority()) {
//	    					System.out.println("NDB~getFitness() Right result="+result.getMajority()+" majority="+test.getMajority());
	    					matches++;
	    				} else {
//	    					System.out.println("NDB~getFitness() Wrong result="+result.getMajority()+" majority="+test.getMajority());
	    				}
	    				 
	    			} else {
//	    				System.out.println("NDB~getFitness() No consensus");
	    				
	    			}
	    	        
	    		}
	    		
	    		double fitness = matches/(double)(testSet.getNumberOfTests());
//	    		System.out.println("NDB~getFitness()->"+fitness);
		        
		        // Set the t=fitness and mark evaluated.
		        ((SimpleFitness)(individual.fitness)).setFitness(state, fitness, false);
		        individual.evaluated = true;
		        
	    	}
        }
	}
	
	public boolean[] evaluate(EvolutionState state, GPIndividual gpIndividual, int threadnum, InitialConfiguration ic, int maxNumberOfSteps, boolean[][] spaceTimeData) {
		// This allocates and clones the initial configuration of the automata
		boolean[] automataState = ic.getTest();
		int step = 0;
		Consensus consensus = Consensus.NO;
		
		// set up the initial configuration into the current step state array and update the halos
		//boolean[] 
		automataStateWithHalos = new boolean[automataState.length+(2*NEIGHBOURHOOD_SIZE)];
		System.arraycopy(automataState, 0, automataStateWithHalos, NEIGHBOURHOOD_SIZE, automataState.length);
		Halo.updateHalos(automataStateWithHalos, NEIGHBOURHOOD_SIZE);
		
		// Copy the initial conditions into the space time data
		if (spaceTimeData != null) {
			System.arraycopy(automataState, 0, spaceTimeData[step], 0, automataState.length);
		}
		
		// Set up the array to hold the next calculated state.
		boolean[] nextStepStateWithHalos = new boolean[automataStateWithHalos.length];
		
		while (!consensus.hasConsensus() && step < maxNumberOfSteps) {
			// Calculate the next step state based on rule action and update the halos
			for (automoton = 0; automoton < automataState.length;automoton++) {
				// evaluate
				gpIndividual.trees[0].child.eval(state, threadnum, input, stack, gpIndividual, this);
				// and assign
				nextStepStateWithHalos[automoton+NEIGHBOURHOOD_SIZE] = ((BitData)input).x;
				
			}
			Halo.updateHalos(nextStepStateWithHalos, NEIGHBOURHOOD_SIZE);
			
			// Copy the next step state into the current step state for the next evaluation.
	    	System.arraycopy(nextStepStateWithHalos, 0, automataStateWithHalos, 0, nextStepStateWithHalos.length);
			
	    	// Check for a consensus.
	    	consensus = Consensus.getConsensusIgnoreHalo(automataStateWithHalos);

			// Copy the next step into the space time data
			if (spaceTimeData != null) {
				System.arraycopy(automataStateWithHalos, NEIGHBOURHOOD_SIZE, spaceTimeData[step+1], 0, spaceTimeData[step].length);
			}
			step++;

		}
//		System.out.println("NDB::evaluate()~"+ToString.toString(convert(Halo.removeHalos(automataStateWithHalos, 3))));

		System.arraycopy(automataStateWithHalos, NEIGHBOURHOOD_SIZE, automataState, 0, automataState.length);

		// Pad out the final steps in the space time data
		if (spaceTimeData != null) {
			while (step < maxNumberOfSteps) {
				System.arraycopy(automataState, 0, spaceTimeData[step+1], 0, spaceTimeData[step].length);
				step++;
			}
		}
		return automataState;
		
	}

	public boolean getCurrentWWW() {
		return automataStateWithHalos[automoton];
	}

	public boolean getCurrentWW() {
		return automataStateWithHalos[automoton+1];
	}
	
	public boolean getCurrenW() {
		return automataStateWithHalos[automoton+2];
	}
	
	public boolean getCurrentX() {
		return automataStateWithHalos[automoton+3];
	}

	public boolean getCurrentE() {
		return automataStateWithHalos[automoton+4];
	}

	public boolean getCurrentEE() {
		return automataStateWithHalos[automoton+5];
	}

	public boolean getCurrentEEE() {
		return automataStateWithHalos[automoton+6];
	}

}
