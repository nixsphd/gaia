package ie.nix.mp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;
import ie.nix.util.Halo;
import ie.nix.util.Randomness;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name="Experiment")
@JsonInclude(value=Include.NON_EMPTY, content=Include.NON_NULL)
public class MajorityProblemExperiment extends Experiment {

	public static double getLambda(int[] automota) {
		double sum = 0;
		for (int a : automota) {
			sum += a;
		}
		return sum/automota.length;
	}

	public static double getLambda(Integer[] automota) {
		double sum = 0;
		for (int a : automota) {
			sum += a;
		}
		return sum/automota.length;
	}

	public static int getMajority(int[] automota) {
		double lambda = getLambda(automota);
		if (lambda < 0.5) {
			return 0;
		} else if (lambda > 0.5) {
			return 1;
		} else {
			return -1; // Undefined really.
		}
	}
	
	public static int getConsensus(int[] automota) {
		int sum = 0;
		for (int a = 0; a < automota.length; a++) {
			sum += automota[a];
		}
		if (sum == automota.length) {
			return 1;
		} else if (sum == 0) {
			return 0;
		} else {
			return -1; // there was no consensus.
		}
	}
	
	public static int getConsensusIgnoreHalos(int[] automota, int haloSize) {
		int sum = 0;
		for (int a = haloSize; a < automota.length-haloSize; a++) {
			sum += automota[a];
		}
		if (sum == automota.length-(2*haloSize)) {
			return 1;
		} else if (sum == 0) {
			return 0;
		} else {
			return -1; // there was no consensus.
		}
	}
	
	public static int[] getNeighbourhoodState(int[] automota, int automoton, int neighbourhoodSize) {
		int numberOfautomota = automota.length;
		int numberOfNeighbours = (2*neighbourhoodSize)+1;
		int[] neighbourhoodState = new int[numberOfNeighbours];
		
		int indexLow = (automoton - neighbourhoodSize + numberOfautomota)%numberOfautomota;
		int indexHigh = (automoton + neighbourhoodSize + numberOfautomota)%numberOfautomota;
		
		if (indexLow < indexHigh) {
			System.arraycopy(automota, indexLow, neighbourhoodState, 0, numberOfNeighbours);
		} else {
			System.arraycopy(automota, indexLow, neighbourhoodState, 0, numberOfautomota-indexLow);
			System.arraycopy(automota, 0, neighbourhoodState, numberOfautomota-indexLow, indexHigh+1);
		}
		return neighbourhoodState;
		
	}
	
	public static int[] getNeighbourhoodStateWithHalo(int[] automotaWithHalo, int automoton, int neighbourhoodSize) {
		int halo = neighbourhoodSize;
		int indexLow = halo + automoton - neighbourhoodSize;
		int numberOfNeighbours = (2*neighbourhoodSize)+1;
		int[] neighbourhoodState = new int[numberOfNeighbours];
		System.arraycopy(automotaWithHalo, indexLow, neighbourhoodState, 0, numberOfNeighbours);
		return neighbourhoodState;
		
	}

	public static void generateBitset(int[] automata) {
		for (int a = 0; a < automata.length; a++) {
			automata[a] = Randomness.nextBinary();
		}
	}

	public static void generateBitsetWithMajority(int[] automata, int majority) {
		do {
			generateBitset(automata);
		} while (getMajority(automata) != majority); 
	}

	public static void generateBitsetWithLambda(int[] bitset, double lambda) {
			if (lambda == 0.0) {
				Arrays.fill(bitset, 0);
			} else if (lambda == 1.0) {
				Arrays.fill(bitset, 1);
			} else {
				double lambdaCount = lambda * bitset.length;
				double remainingLambdaTarget = lambda;
				double remainingLambdaBitCount = bitset.length;
	//			System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
	//					+ "lambdaCount="+lambdaCount+", "
	//					+ "remainingLambdaTarget="+remainingLambdaTarget);
				for (int a = 0; a < bitset.length; a++) {
					double rand = Randomness.nextDouble();
					if (rand < remainingLambdaTarget) {
						bitset[a] = 1;
						lambdaCount--;
					} else {
						bitset[a] = 0;
					}
					remainingLambdaBitCount--;
	//				System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
	//						+ "lambdaCount="+lambdaCount+", "
	//						+ "remainingLambdaTarget="+String.format("%.2f", remainingLambdaTarget)+", "
	//						+ "remainingLambdaBitCount="+remainingLambdaBitCount+", "
	//						+ "rand="+String.format("%.2f", rand)+", "
	//						+ "bitset["+a+"]="+bitset[a]); 
					remainingLambdaTarget = lambdaCount/remainingLambdaBitCount;
				}
			}
		}

	public static void generateBitsetWithLambda(Integer[] bitset, double lambda) {
		if (lambda == 0.0) {
			Arrays.fill(bitset, 0);
		} else if (lambda == 1.0) {
			Arrays.fill(bitset, 1);
		} else {
			double lambdaCount = lambda * bitset.length;
			double remainingLambdaTarget = lambda;
			double remainingLambdaBitCount = bitset.length;
//			System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
//					+ "lambdaCount="+lambdaCount+", "
//					+ "remainingLambdaTarget="+remainingLambdaTarget);
			for (int a = 0; a < bitset.length; a++) {
				double rand = Randomness.nextDouble();
				if (rand < remainingLambdaTarget) {
					bitset[a] = 1;
					lambdaCount--;
				} else {
					bitset[a] = 0;
				}
				remainingLambdaBitCount--;
//				System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
//						+ "lambdaCount="+lambdaCount+", "
//						+ "remainingLambdaTarget="+String.format("%.2f", remainingLambdaTarget)+", "
//						+ "remainingLambdaBitCount="+remainingLambdaBitCount+", "
//						+ "rand="+String.format("%.2f", rand)+", "
//						+ "bitset["+a+"]="+bitset[a]); 
				remainingLambdaTarget = lambdaCount/remainingLambdaBitCount;
			}
		}
	}

	public static void generateBitsetWithLambda(Double[] bitset, double lambda) {
		if (lambda == 0.0) {
			Arrays.fill(bitset, 0.0);
		} else if (lambda == 1.0) {
			Arrays.fill(bitset, 1.0);
		} else {
			double lambdaCount = lambda * bitset.length;
			double remainingLambdaTarget = lambda;
			double remainingLambdaBitCount = bitset.length;
//			System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
//					+ "lambdaCount="+lambdaCount+", "
//					+ "remainingLambdaTarget="+remainingLambdaTarget);
			for (int a = 0; a < bitset.length; a++) {
				double rand = Randomness.nextDouble();
				if (rand < remainingLambdaTarget) {
					bitset[a] = 1.0;
					lambdaCount--;
				} else {
					bitset[a] = 0.0;
				}
				remainingLambdaBitCount--;
//				System.out.println("NDB generateRandomBitsetWithLambda() requested lambda="+lambda+", "
//						+ "lambdaCount="+lambdaCount+", "
//						+ "remainingLambdaTarget="+String.format("%.2f", remainingLambdaTarget)+", "
//						+ "remainingLambdaBitCount="+remainingLambdaBitCount+", "
//						+ "rand="+String.format("%.2f", rand)+", "
//						+ "bitset["+a+"]="+bitset[a]); 
				remainingLambdaTarget = lambdaCount/remainingLambdaBitCount;
			}
		}
		
	}

	public static int[][] generateTests(int numberOfTests, int numberOfAutomota) {
		return generateTests(numberOfTests, numberOfAutomota, false);
	}
	
	public static int[][] generateTests(int numberOfTests, int numberOfAutomota, boolean balanced) {
		int[][] tests = new int[numberOfTests][numberOfAutomota];
		for (int t = 0; t < numberOfTests; t++) {
			int[] automata = new int[numberOfAutomota];
			if (balanced) {
				if (t%2 == 0) {
					generateBitsetWithMajority(automata, 0);	
				} else {
					generateBitsetWithMajority(automata, 1);	
				}
			} else {
				generateBitset(automata);
			}
			tests[t] = automata;
		}
		return tests;
	}

	/* Used for Fitness Against Lambda graphs */
	public static int[][] generateTestsWithLambda(int numberOfTests, int numberOfAutomota, double lambda) {
		int[][] tests = new int[numberOfTests][];
		for (int t = 0; t < numberOfTests; t++) {
			int[] automata = new int[numberOfAutomota];
			MajorityProblemExperiment.generateBitsetWithLambda(automata, lambda);
			tests[t] = automata;
//			System.out.println("NDB requested lambda="+String.format("%.2f", lambda)+
//					" gave run with actual lambda="+String.format("%.2f", getLambda(automata)));
		}
		return tests;
	}

	protected static DataFrame getSpaceTimeDataFrame() {
		String spaceTimeFileName = "SpaceTime.csv";
		// Process the space-time diagrams filename
		DataFrame stdf = new DataFrame();
		stdf.setOutputDirectoryName(Experiment.getExperimentDirectory());
		stdf.initWriter(spaceTimeFileName);
		return stdf;
	}

	public static double getFitness(MajorityProblemExperiment experiment) {
		return getFitnessForRule(experiment.getRule(), experiment);
		
	}
	
	public static double getFitnessForRule(Rule rule, MajorityProblemExperiment experiment) {
			int neighbourhoodSize = experiment.getNeighbourhoodSize();
			int maxNumberOfSteps = experiment.getMaxNumberOfSteps();
			if (experiment.isNumberOfStepsPoisson()) {
				maxNumberOfSteps = (int)Randomness.nextPoisson(maxNumberOfSteps);
			}
			
			int mismatches = 0; 
			
			int nextTest = experiment.nextTest();
			int lastTest = nextTest + experiment.getNumberOfTests();
			for (int test = nextTest; test < lastTest; test++) {
				
		    	double result = getRuleMajorityForTest(rule, experiment.getAutomotaForTest(test), neighbourhoodSize, maxNumberOfSteps);
		    	
				if (result != experiment.getMajority(test)) {
	//				System.out.println("NDB~getFitnessForPolicy MISMATCH result="+result+" majority="+experiment.getMajority(run));
					mismatches++;
				} else {
	//				System.out.println("NDB~getFitnessForPolicy MATCH result="+result+" majority="+experiment.getMajority(run));
					
				}
		        
			}
		    return (experiment.getNumberOfTests()-mismatches)/(double)(experiment.getNumberOfTests());
		        
		}

	public static double getRuleMajorityForTest(Rule rule, int[] initialAutomota, int neighbourhoodSize, int maxNumberOfSteps) {
		boolean consensus = false;
		int step = 0;
		int halo = neighbourhoodSize;
		int[] automotaWithHalo = new int[initialAutomota.length+(2*halo)];
		int[] newAtomota = new int[automotaWithHalo.length];
		
		System.arraycopy(initialAutomota, 0, automotaWithHalo, halo, initialAutomota.length);
		Halo.updateHalos(automotaWithHalo, halo);
		
		while (step < maxNumberOfSteps) {
			for (int a = 0; a < initialAutomota.length;a++) {
				int[] neighbourhoodState = MajorityProblemExperiment.getNeighbourhoodStateWithHalo(automotaWithHalo, a, neighbourhoodSize);
				newAtomota[a+halo] = rule.getAction(neighbourhoodState);
				
			}
			Halo.updateHalos(newAtomota, halo);
			
			if (!Arrays.equals(newAtomota, automotaWithHalo)) {
		    	System.arraycopy(newAtomota, 0, automotaWithHalo, 0, newAtomota.length);
		    	
			} else {
				if (!consensus) {
					consensus = true;
				}
				
			}
			step++;
	
		}
		return MajorityProblemExperiment.getConsensusIgnoreHalos(automotaWithHalo, halo);
		
	}

	public static double getFitnessForRule(Rule rule, MajorityProblemExperiment experiment, DataFrame stdf) {
		int neighbourhoodSize = experiment.getNeighbourhoodSize();
		int maxNumberOfSteps = experiment.getMaxNumberOfSteps();
		if (experiment.isNumberOfStepsPoisson()) {
			maxNumberOfSteps = (int)Randomness.nextPoisson(maxNumberOfSteps);
		}
		
		int mismatches = 0; 
		List<Object> spaceTimeData = null;
	
		if (stdf != null) {
			spaceTimeData = 
				new ArrayList<Object>(experiment.getNumberOfAutomota()*experiment.getNumberOfTests()*maxNumberOfSteps); 
		}
		
		int nextTest = experiment.nextTest();
		int lastTest = nextTest + experiment.getNumberOfTests();
		for (int test = nextTest; test < lastTest; test++) {
			
			int[] ic = experiment.getAutomotaForTest(test);
	    	double result = getRuleMajorityForTest(rule, ic, neighbourhoodSize, maxNumberOfSteps, spaceTimeData);
	    	
	    	if (stdf != null) {
	    		stdf.addColumn("Test"+test, spaceTimeData);
	    	}
	    	
			if (result != MajorityProblemExperiment.getMajority(ic)) {
//				System.out.println("NDB~getFitnessForPolicy MISMATCH result="+result+" majority="+experiment.getMajority(run));
				mismatches++;
			} else {
//				System.out.println("NDB~getFitnessForPolicy MATCH result="+result+" majority="+experiment.getMajority(run));
				
			}
	        
		}
		double fitness = (experiment.getNumberOfTests()-mismatches)/(double)(experiment.getNumberOfTests());
//		System.out.println("NDB::getFitnessForPolicy()~nextTest="+nextTest+" to lastTest="+lastTest+" -> "+fitness);
	    return fitness;
	        
	}

	protected static double getRuleMajorityForTest(Rule rule, int[] initialAutomota, 
			int neighbourhoodSize, int maxNumberOfSteps, List<Object> spaceTimeData) {
		boolean consensus = false;
		int step = 0;
		int halo = neighbourhoodSize;
		int[] automotaWithHalo = new int[initialAutomota.length+(2*halo)];
		int[] newAtomota = new int[automotaWithHalo.length];
		
		System.arraycopy(initialAutomota, 0, automotaWithHalo, halo, initialAutomota.length);
		Halo.updateHalos(automotaWithHalo, halo);
		
		if (spaceTimeData != null) {
			spaceTimeData.clear();
			spaceTimeData.addAll(Arrays.asList(ArrayUtils.toObject(initialAutomota)));
		}
		
		while (step < maxNumberOfSteps) {
			for (int a = 0; a < initialAutomota.length;a++) {
				int[] neighbourhoodState = MajorityProblemExperiment.getNeighbourhoodStateWithHalo(automotaWithHalo, a, neighbourhoodSize);
				newAtomota[a+halo] = rule.getAction(neighbourhoodState);
				
			}
			Halo.updateHalos(newAtomota, halo);
			
			if (!Arrays.equals(newAtomota, automotaWithHalo)) {
		    	System.arraycopy(newAtomota, 0, automotaWithHalo, 0, newAtomota.length);
		    	
			} else {
				if (!consensus) {
					consensus = true;
				}
				
			}
			if (spaceTimeData != null) {
				spaceTimeData.addAll(Arrays.asList(ArrayUtils.toObject(Halo.removeHalos(automotaWithHalo, halo))));
			}
			step++;
	
		}
		return MajorityProblemExperiment.getConsensusIgnoreHalos(automotaWithHalo, halo);
		
	}
	
	public static enum FitnessFunction {
		Preformance,
		Proportional,
	}

	private int neighbourhoodSize;
	private Rule rule;
	private int maxNumberOfSteps;
	private boolean numberOfStepsPoisson;
	private int numberOfTests;
	private int[][] tests;

	private int nextTest;

	public MajorityProblemExperiment() {
		int neighbourhoodSize = 3; // Comes from Kaza's Paper
		setNeighbourhoodSize(neighbourhoodSize);
		
		int maxNumberOfSteps = 320; // Comes Mitchell's Paper
		setMaxNumberOfSteps(maxNumberOfSteps);
		
		nextTest = 0;
	}

	public MajorityProblemExperiment(int numberOfTests, int numberOfAutomota) {
		this();
		this.tests = new int[numberOfTests][numberOfAutomota]; 
	}
	
	public MajorityProblemExperiment(int[][] tests) {
		this();
		this.tests = tests; 
		setNumberOfTests(this.tests.length);
	}
	
	public int getNumberOfTests() {
		return numberOfTests;
	}
	
	public void setNumberOfTests(int numberOfTests) {
		this.numberOfTests = numberOfTests;
	}

	public int getNumberOfAutomota() {
		if (this.tests != null) { 
			return this.tests[0].length;
		} else {
			return -1;
		}
	}

	void setNumberOfAutomota(int numberOfAutomata) {
		// Do nothing.
	}
	
	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	public int[][] getTests() {
		return tests;
	}

	public void setTests(int[][] tests) {
		this.tests = tests;
	}

	public int nextTest() {
		return nextTest;
	}

	public void updateNextTest() {
		nextTest += getNumberOfTests();
	}

	public int[] getAutomotaForTest(int test) {
		// Always returns some automota..
		return tests[test%tests.length];
	}

	public void setNeighbourhoodSize(int neighbourhoodSize) {
		this.neighbourhoodSize = neighbourhoodSize;
	}

	public int getNeighbourhoodSize() {
		return neighbourhoodSize;
	}

	public int getMaxNumberOfSteps() {
		return maxNumberOfSteps;
	}

	public void setMaxNumberOfSteps(int maxNumberOfSteps) {
		this.maxNumberOfSteps = maxNumberOfSteps;
	}

	public void setNumberOfStepsPoisson(boolean numberOfStepsPoisson) {
		this.numberOfStepsPoisson = numberOfStepsPoisson;
	}

	public boolean isNumberOfStepsPoisson() {
		return numberOfStepsPoisson;
	}

	public double getLambda(int run) {
		return MajorityProblemExperiment.getLambda(getAutomotaForTest(run));
		
	}

	public int getMajority(int run) {
		return MajorityProblemExperiment.getMajority(getAutomotaForTest(run));
	}

}
