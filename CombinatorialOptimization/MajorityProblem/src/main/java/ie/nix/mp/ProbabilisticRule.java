package ie.nix.mp;

import ie.nix.util.Randomness;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
	    use = JsonTypeInfo.Id.NAME,
	    include = JsonTypeInfo.As.PROPERTY,
	    property = "type")
public class ProbabilisticRule implements Rule {

	private double[] policy;

	public ProbabilisticRule(double[] policy) {
		this.policy = policy;
	}

	public double[] getPolicy() {
		return policy;
	}

	protected void setPolicy(double[] policy) {
		this.policy = policy;
	}

	public int getAction(int[] neighbourhoodState) {
		int neighbourhoodStateIndex = 
				neighbourhoodState[6] + 
				(neighbourhoodState[5] << 1) + 
				(neighbourhoodState[4] << 2) + 
				(neighbourhoodState[3] << 3) + 
				(neighbourhoodState[2] << 4) + 
				(neighbourhoodState[1] << 5) + 
				(neighbourhoodState[0] << 6);
		double likelihood = policy[neighbourhoodStateIndex];
		return (Randomness.nextDouble() >= likelihood) ? 0 : 1;
	
	}

}
