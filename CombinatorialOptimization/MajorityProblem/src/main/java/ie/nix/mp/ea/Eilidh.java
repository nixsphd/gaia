package ie.nix.mp.ea;

import java.util.ArrayList;
import java.util.List;

import ie.nix.ea.Agent;
import ie.nix.ea.Population;
import ie.nix.ea.operators.EliteSelection;
import ie.nix.ea.operators.Mask;
import ie.nix.ea.operators.OnePointCrossover;
import ie.nix.ea.operators.ShiftMutation;
import ie.nix.ea.populations.PopulationOfDoubles;
import ie.nix.mp.MajorityProblemExperiment;
import ie.nix.util.Experiment;
import ie.nix.util.Randomness;

public class Eilidh extends GeneticAlgorithmExperiment<Double> {
	
	public static void main(String[] args) {
		
		Eilidh experiment = (Eilidh)init(args, Eilidh.class);
		if (experiment != null) {
			double fitness = experiment.runExperiment();
	
			stopWatch.stop();
	        System.out.println( "#Seed "+Experiment.getSeed()+" gives fitness of best evolved rule is "+String.format("%.3f", fitness)+" on the gaussian evaluation experiment"
	        		+ " in "+String.format("%.2f", (stopWatch.getTime()/1000.0))+" secs");
	        
		} else {
		    System.out.println( "#Done.");
		    
		}
	}
	
	public static Double[][] generateUniformGenomes(int lenghtOfGenotype, int numberOfAgents) {
		Double[][] population = new Double[numberOfAgents][];
		for (int a = 0; a < numberOfAgents; a++) {
			double lambda = ((double)Randomness.nextInt(0,lenghtOfGenotype))/lenghtOfGenotype;
			Double[] genes = new Double[lenghtOfGenotype];
			MajorityProblemExperiment.generateBitsetWithLambda(genes, lambda);
			population[a] = genes;
//			System.out.println("NDB~generateUniformGenomes() requested lambda="+lambda+
//					" gave run with actual lambda="+MajorityProblemExperiment.getLambda(population[a]));
		}
		return population;
	}
	
	protected int numberOfElites;
	protected double mutationProbabillity;
	protected double mutationSpread;
	protected double crossoverProbabillity;
	
	public Eilidh() {
		this(100,100);
		
	}

	public Eilidh(int numberOfAgents, int numberOfGenerations) {
		super(numberOfAgents, numberOfGenerations);
		
		setGenomes(generateUniformGenomes(lenghtOfGenotype, numberOfAgents));
		//setNumberOfElites((int)(0.2*numberOfAgents));  // Mitchell selects 20 elites
		setNumberOfElites(20);  // Mitchell selects 20 elites
		setMutationProbabillity(2.0/(Math.pow(2,(neighbourhoodSize*2)+1)));// Mitchell mutates twice per genome
		setMutationSpread(1.0);
		setCrossoverProbabillity(1.0); // There is always crossover.
	}

	public void initTrainingExperiment() {		
		int numberOfTrainingTests = 100; 
		MajorityProblemExperiment trainingExperiment = new MajorityProblemExperiment(
				Mitchell.generateUniformTests(numberOfTrainingTests*numberOfGenerations, numberOfAutomota)); // From Mitchell
		// Comes from Kaza's Paper;
		trainingExperiment.setNumberOfTests(numberOfTrainingTests);
		trainingExperiment.setNumberOfStepsPoisson(true);
		setTrainingExperiment(trainingExperiment);
	}
	
	public void initGeneticAlgorithm() {
		// Set up the GA
		ga = new GeneticAlgorithm<Double>(getGenomes()) {

			@Override
			protected Population<Double> initPopulation() {
				return new PopulationOfDoubles(getLenghtOfGenotype());
				
			}

			@Override
			protected List<Agent<Double>> initialiseAgents(int numberOfAgents) {
				List<Agent<Double>> agents = new ArrayList<Agent<Double>>(numberOfAgents);
				for (int a = 0; a < numberOfAgents; a++) {
					agents.add(new MajorityProblemProbabilityRule());
				}
				return agents;
			}
			
		};
		
		// Set up the agents
		for (Agent<Double> agent: ga.getAgents()) {
			MajorityProblemProbabilityRule mpAgent = (MajorityProblemProbabilityRule)agent;
			mpAgent.setExperiment(getTrainingExperiment());
		}
		ga.evaluate();
		
		// Selection
		EliteSelection<Double> eliteSelection = new EliteSelection<Double>(getNumberOfElites());
		ga.addGeneticOperator(eliteSelection);
		// Add the mask
		Mask<Double> mask = new Mask<Double>(eliteSelection.generateMask(getNumberOfAgents()));
		ga.addGeneticOperator(mask);
		
		// ShiftMutation
		ShiftMutation shiftMutation = new ShiftMutation(
				getMutationProbabillity(), getMutationSpread());
		shiftMutation.loop(true);
		ga.addGeneticOperator(shiftMutation);
		
		// Crossover
		OnePointCrossover<Double> onePointCrossover = new OnePointCrossover<Double>(getCrossoverProbabillity());
		ga.addGeneticOperator(onePointCrossover);
		
		// And unmask again finally
		ga.addGeneticOperator(mask);
	
	}

	public int getNumberOfElites() {
		return numberOfElites;
	}

	public void setNumberOfElites(int numberOfElites) {
		this.numberOfElites = numberOfElites;
	}

	public double getMutationProbabillity() {
		return mutationProbabillity;
	}

	public void setMutationProbabillity(double mutationProbabillity) {
		this.mutationProbabillity = mutationProbabillity;
	}

	public double getMutationSpread() {
		return mutationSpread;
	}

	public void setMutationSpread(double mutationSpread) {
		this.mutationSpread = mutationSpread;
	}

	public double getCrossoverProbabillity() {
		return crossoverProbabillity;
	}

	public void setCrossoverProbabillity(double crossoverProbabillity) {
		this.crossoverProbabillity = crossoverProbabillity;
	}
	
}
