package ie.nix.mp.ecj.gp;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;
import ec.util.Parameter;

public class If extends GPNode {

	private static final long serialVersionUID = -443255979370514048L;

	// Add needs to specify its printed form ("+"), and a function (expectedChildren(...) ) which give Add a 
	// chance to do a sanity check when first loaded, to verify that the child array size is correct. 
	public String toString() { 
    	return "if"; 
    }

    public int expectedChildren() { 
    	return 3; 
    }

    // Next we will define the central function to the GPNode: the eval method, which is called when the GPNode 
    // must do its part during the execution of the tree as s-expression computer code. eval is called by the 
    // GPNode's parent to execute the GPNode and get its return value.
    //
    //The eval method is passed a bunch of stuff, including the EvolutionState, the current thread number 
    // (so you can get a random number via state.random[thread], an ADFStack (don't worry about this -- just 
    // pass it on to your children), the GPIndividual which is executing, the Problem which is executing the 
    // GPIndividual, and the GPData used to pass information to and from the GPNode's parent.
	@Override
	public void eval(
			EvolutionState state, 
			int thread, 
			GPData input,
			ADFStack stack, 
			GPIndividual individual, 
			Problem problem) {
	  
		// What we're doing here is simple enough: we evaluate our left child, then we evaluate our right child,
		// then we add the two together and return that result. Note that we reuse the GPData object rather than 
		// allocating new ones. You are free to allocate a new GPData object to pass to your children, but you
		// should use the GPData object provided you when you return a value to your parent. But I suggest you 
		// reuse the GPData object to keep from allocating millions of objects during evaluation. 
		boolean result;
		BitData rd = ((BitData)(input));
		
		children[0].eval(state,thread,input,stack,individual,problem);;
		if (rd.x) {
			children[1].eval(state,thread,input,stack,individual,problem);
			
		} else {
			children[2].eval(state,thread,input,stack,individual,problem);
			
		}
		
	}
    
}
