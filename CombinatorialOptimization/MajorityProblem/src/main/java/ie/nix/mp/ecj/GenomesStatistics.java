package ie.nix.mp.ecj;

import ec.*;
import ec.simple.SimpleStatistics;
import ec.util.*;
import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;
import ie.nix.util.ToString;

import java.io.*;
import java.util.stream.IntStream;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

import ec.vector.*;

/*
	protected static DataFrame getGemonesDataFrame() {
		String genomesFileName = "Genomes.csv";
		// Process the space-time diagrams filename
		DataFrame gdf = new DataFrame();
		gdf.setOutputDirectoryName(Experiment.getExperimentDirectory());
		gdf.initWriter(genomesFileName);
		return gdf;
	}
	
 */

@SuppressWarnings("serial")
public class GenomesStatistics extends Statistics {
	
	// The parameter string and log number of the file for our readable population
    public static final String P_FILE = "file";
    
    public int log;

    protected TestSet fitnessTestSet;

    public void setup(final EvolutionState state, final Parameter base) {
        
    	// DO NOT FORGET to call super.setup(...) !!
        super.setup(state,base);
        
        // We will set up a log called "pop-file". This is done through our Output object. 
        // Of course we could just write to the file stream ourselves; but it is 
        // convenient to use the Output logger because it recovers from checkpoints 
        // and it's threadsafe.
        // set up popFile
        File file = state.parameters.getFile(base.push(P_FILE),null);

        // We tell Output to add a log file writing to "pop-file". We will NOT post 
        // announcements to the log (no fatal, error, warning messages etc.). The log 
        // WILL open via appending (not rewriting) upon a restart after a checkpoint failure. 
        if (file != null) {
        	try {
        		log = state.output.addLog(file,true);
        	} catch (IOException i) {
        		state.output.fatal("An IOException occurred while trying to create the log " + 
        				file + ":\n" + i);
            }
        }

        fitnessTestSet = new TestSet(state, 100, 149, true);

		state.output.println("\"Generation\", \"Rule\", \"Lambda\", \"Training Fitness\", \"Fitness\"", log);
  		
    }
 
	@Override
	public void postEvaluationStatistics(EvolutionState state) {
		// TODO Auto-generated method stub
		super.postEvaluationStatistics(state);

		for(int y = 0; y < state.population.subpops[0].individuals.length; y++) {
			// Add their lambda values to the statistucs object.
			Rule rule =  (Rule)state.population.subpops[0].individuals[y];

	    	double fitness = MajorityProblem.getFitness(rule, fitnessTestSet, 320);
	    	
			state.output.println((state.generation+1)+", "+y+", "+rule.getLambda()+", "+rule.fitness.fitness()+", "+fitness, log);
//			System.out.println("NDB postEvaluationStatistics()~genome="+toString(rule.genome));
	        
		}
	}

	public static String toString(boolean[] booleanArray) {
		StringBuilder string = new StringBuilder();
	    IntStream.range(0, booleanArray.length).
    		forEach(g -> {
    			string.append(""+(booleanArray[g] ?  1 : 0)+",");
    		}
    	);
		string.replace(string.length()-1, string.length(),"");
		return string.toString();
		
	}
    
}
