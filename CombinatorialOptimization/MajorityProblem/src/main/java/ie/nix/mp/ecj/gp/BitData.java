package ie.nix.mp.ecj.gp;

import ec.gp.*;

public class BitData extends GPData {

	private static final long serialVersionUID = 4750045721579481844L;
	
	public boolean x;    // return value

	// copy my stuff to another DoubleData
    public void copyTo(final GPData gpd) { 
    	((BitData)gpd).x = x; 
    }
    
}