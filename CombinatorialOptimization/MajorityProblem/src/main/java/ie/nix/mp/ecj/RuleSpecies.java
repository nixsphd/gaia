package ie.nix.mp.ecj;

import ec.EvolutionState;
import ec.Individual;
import ec.Subpopulation;
import ec.util.Parameter;
import ec.vector.BitVectorSpecies;
import ec.vector.VectorSpecies;

public class RuleSpecies extends BitVectorSpecies {

	private static final long serialVersionUID = -1763623783209270922L;

	public static final String P_RULES = "rule-species";
	
	// majority-problem.rule-species.is-genome-uniform
	// pop.subpop.0.species.is-genome-uniform		
	public static final String P_IS_GENOME_UNIFORM = "is-genome-uniform";

	// pop.subpop.0.species.genome-size		
	public static final String P_GENOMESIZE = "is-genome-uniform";
   
	int sizeofPopulation;
	int genomeSize;
	boolean isGenomeUniform;
	
	double count = 0; 

	public Parameter defaultBase() {
		return MajorityProblem.base().push(P_RULES);
		 
	 }
	 
    public void setup(final EvolutionState state, final Parameter base) {
	    super.setup(state,base);
	    Parameter def = defaultBase();
	    
	    sizeofPopulation = state.parameters.getInt(
	    		base.pop().push(Subpopulation.P_SUBPOPSIZE), 
	    		def.pop().push(Subpopulation.P_SUBPOPSIZE));
	    
	    genomeSize = state.parameters.getInt(
	    		base.push(VectorSpecies.P_GENOMESIZE), 
	    		def.push(VectorSpecies.P_GENOMESIZE));
	    
	    isGenomeUniform = state.parameters.getBoolean(
	    		base.push(RuleSpecies.P_IS_GENOME_UNIFORM), 
	    		def.push(RuleSpecies.P_IS_GENOME_UNIFORM), false);

//	    System.out.println("NDB RuleSpecies.setup()~sizeofPopulation="+sizeofPopulation);
//	    System.out.println("NDB RuleSpecies.setup()~genomeSize="+genomeSize);
//	    System.out.println("NDB RuleSpecies.setup()~isGenomeUniform="+isGenomeUniform);
	    
    }
  
	public Individual newIndividual(final EvolutionState state, int thread) {
		Rule rule = (Rule)super.newIndividual(state, thread);
		if (isGenomeUniform) {
			// The +1 ensures that a lambda od 1.0 is possible, as nextInt returns from -1 of the value passed.
			double lambda = ((double)state.random[thread].nextInt(genomeSize+1))/genomeSize;
			rule.reset(state, thread, lambda);
//		    System.out.println("NDB RuleSpecies.newIndividual()~lambda="+lambda);
		}
	    return rule;
	    
    }
	
	public Individual newIndividual(final EvolutionState state, int thread, double lambda) {
		Rule rule = (Rule)super.newIndividual(state, thread);
		rule.reset(state, thread, lambda);
	    return rule;
	    
    }
	
}

