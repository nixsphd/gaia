package ie.nix.mp.ecj;

import java.util.stream.DoubleStream;

import ie.nix.mp.ecj.PopulationEvaluator.PopulationProblem;
import ie.nix.util.ToString;
import ec.EvolutionState;
import ec.Individual;
import ec.Statistics;
import ec.Subpopulation;
import ec.simple.SimpleFitness;
import ec.util.Parameter;

@SuppressWarnings("serial")
public class SkillAwareMajorityProblem extends MajorityProblem implements PopulationProblem {
	
	public static class GAStatistics extends ie.nix.mp.ecj.GAStatistics {

		protected String getHeaderRow() {
	    	return super.getHeaderRow()+ ",  \"Skill Weight\"";
	    }

	    protected String getRow(final EvolutionState state) {
	    	return super.getRow(state)+ ", "+
	    	((SkillAwareMajorityProblem)state.evaluator.p_problem).getSkillWeight();
	    }
	
	};
	

	public static class TestSetStatistics extends ie.nix.mp.ecj.TestSetStatistics {

		protected double[] testSetSkills;
		
	    protected double[] getTestSetSkills(final EvolutionState state) {
		    if (testSetSkills == null) {
		     	testSetSkills = ((SkillAwareMajorityProblem)state.evaluator.p_problem).getTestSetSkills();
	        	
		    }
		    return testSetSkills;
	    }
		
	    protected String getHeaderRow() {
	    	return super.getHeaderRow()+", \"Skill\"";
	    }

	    protected String getRow(final EvolutionState state, int test) {
	    	return super.getRow(state, test) + ", "+getTestSetSkills(state)[test];
	    }
	    
		@Override
		public void postEvaluationStatistics(EvolutionState state) {      	
			super.postEvaluationStatistics(state);
			testSetSkills = null;
		}
	}
	
	// majority-problem.skill-min
	public static final String P_SKILL_MIN = "skill-min";
	// majority-problem.skill-max
	public static final String P_SKILL_MAX = "skill-max";
	// majority-problem.skill-max
	public static final String P_SKILL_WEIGHT = "skill-weight";
	
	public static final double SKILL_MIN = 0.0;
	public static final double SKILL_MAX = 1.0;
//	public static final double SKILL_WEIGHT = 0.0;
	public static final double SKILL_WEIGHT = 0.7;
//	public static final double SKILL_WEIGHT = 1.0;

	protected double skillMin;
	protected double skillMax;
	protected double skillWeight;

//	protected TestSet skillAwareTestSet;
	double[] testSetSkills;
	
	public double getSkillMin() {
		return skillMin;
	}

	public double getSkillMax() {
		return skillMax;
	}

	public double getSkillWeight() {
		return skillWeight;
	}

	public double[] getTestSetSkills() {
		// TODO Auto-generated method stub
		return testSetSkills;
	}

	public void setup(final EvolutionState state, final Parameter base) {
        // very important, remember this
        super.setup(state,base);
        
		Parameter def = base();
		skillMin = state.parameters.getDoubleWithDefault(
			base.push(P_SKILL_MIN), def.push(P_SKILL_MIN),SKILL_MIN);
		skillMax = state.parameters.getDoubleWithDefault(
			base.push(P_SKILL_MAX), def.push(P_SKILL_MAX),SKILL_MAX);
		skillWeight = state.parameters.getDoubleWithDefault(
				base.push(P_SKILL_WEIGHT), def.push(P_SKILL_WEIGHT), SKILL_WEIGHT);

		System.out.println("NDB::SkillAwareMajorityProblem.setup()~"
				+ "skillMin="+skillMin+", "
				+ "skillMax="+skillMax+", "
				+ "skillWeight="+skillWeight);
	}

	@Override
	public void prepareToEvaluatePopulation(final EvolutionState state) {
		
	}

	@Override
	public void evaluatePopulation(final EvolutionState state) {

		final int threadnum = 0;
		super.prepareToEvaluate(state, threadnum);
		
        Subpopulation[] subpops = state.population.subpops;
//		System.out.println("NDB::SkillAwareMajorityProblem.evaluatePopulation()~numberOfTests="+numberOfTests);
		
        // Make the TestSet to hold the ICs.
		TestSet skillAwareTestSet = new TestSet(numberOfTests);
		double[] testSetSkills = new double[numberOfTests];
		int testIndex = 0;
		int testAttemptIndex = 0;
		
		// Take the first subpopulation to generate the TestSet
		Individual[] individuals = state.population.subpops[0].individuals;
//		System.out.println("NDB::SkillAwareMajorityProb lem.evaluatePopulation()~individuals="+individuals.length);
		
		// Holds the matched used to calculated fitnesses.
		double[][] skillFitness = new double[individuals.length][numberOfTests];
		
		while (testIndex < numberOfTests) {
			
			// Make an ic to evaluate
			InitialConfiguration ic = new InitialConfiguration(state, numberOfAutomata);
			
			// Count the number of matches for the ic
			double matches = 0;
	
			// How mant steps can be used?
			int maxNumberOfSteps = this.maxNumberOfSteps;
			if (isNumberOfStepsPoisson) {
				maxNumberOfSteps = (int)rngFromApache.nextPoisson(this.maxNumberOfSteps);
			}
			
	        // Evaluate....
			for (int i = 0; i < individuals.length; i++) {
				if (test((Rule)individuals[i], ic, maxNumberOfSteps)) {
					skillFitness[i][testIndex] = 1;
		        	matches++;
		        }
			}
//			System.out.println("NDB::SkillAwareMajorityProblem.evaluatePopulation()~"
//					+ "testAttemptIndex="+testAttemptIndex+", matches="+ matches);
			
			// Calculate the skill of the test.
			matches /= individuals.length;
//			double testSkill = 1 - (matches*matches);
			double testSkill = 1 - matches;
			
			// Check if it's within the Goldilocks range.
			if (skillMin < testSkill && testSkill < skillMax) {
			
				// Keep it!
				testSetSkills[testIndex] = testSkill;	
//				System.out.println("NDB::SkillAwareMajorityProblem.evaluatePopulation()~"
//						+ "testAttemptIndex="+testAttemptIndex+", testIndex="+ testIndex+", ic.lambda="+ic.getLambda()
//						+", skillMin="+skillMin+" < testSkill="+testSkill+" < skillMax="+skillMax);

//				skillFitness[i][testIndex] *= (1 - (skillWeight * testSkill));
//				skillFitness[i][testIndex] -= (skillWeight * (1.0 - testSkill));
				double weigthedSkill = 1 - (skillWeight * (1.0 - testSkill));
//				System.out.println("NDB::SkillAwareMajorityProblem.evaluatePopulation()~"
//						+ "testSkill]="+testSkill+"->weigthedSkill="+weigthedSkill);
				
				// Next scale the fitness based on the skill.
				for (int i = 0; i < individuals.length; i++) {
					skillFitness[i][testIndex] *= weigthedSkill;
				}
				
				// Add it to the TestSet
				skillAwareTestSet.getTests()[testIndex] = ic;
				// Increment the testIndex
				testIndex++;
				
			}
			testAttemptIndex++;
		}
		
		// Set the TestSet so the Statistics can pick it up.
		this.testSet = skillAwareTestSet;
		this.testSetSkills = testSetSkills;
		
		// Finally assign the fitnesses to the rules used so far.
		for (int i = 0; i < individuals.length; i++) {
			// Calcuate the fitness for the rule
			double fitness = DoubleStream.of(skillFitness[i]).sum()/(double)(numberOfTests);
//			System.out.println("NDB::SkillAwareMajorityProblem.evaluatePopulation()~"+ "i="+ i+", fitness="+fitness);

	        // Set the fitness and mark evaluated.
			SimpleFitness indFitness = ((SimpleFitness)(individuals[i].fitness));
			indFitness.setFitness(state, fitness, false);
			
	        individuals[i].evaluated = true;
		}
//		System.out.println("NDB::SkillAwareMajorityProblem.evaluatePopulation()->");
        
	}

	@Override
	public void finishEvaluatingPopulation(EvolutionState state) {
		// TODO Auto-generated method stub
		
	}

}
