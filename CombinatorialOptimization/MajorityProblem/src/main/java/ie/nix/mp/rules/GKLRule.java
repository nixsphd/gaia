package ie.nix.mp.rules;

import ie.nix.mp.DeterministicRule;

// Gacs-Kurdyumov-Levin automaton 
public class GKLRule extends DeterministicRule {
	
//	GKL 1978 human-written
//	00000000 01011111 00000000 01011111 00000000 01011111 00000000 01011111 00000000 01011111 11111111 01011111 00000000 01011111 11111111 01011111
	public static final int[] GKL_POLICY = {
			0,0,0,0,0,0,0,0, 
			0,1,0,1,1,1,1,1, 
			0,0,0,0,0,0,0,0, 
			0,1,0,1,1,1,1,1, 
			0,0,0,0,0,0,0,0, 
			0,1,0,1,1,1,1,1, 
			0,0,0,0,0,0,0,0, 
			0,1,0,1,1,1,1,1, 
			0,0,0,0,0,0,0,0, 
			0,1,0,1,1,1,1,1, 
			1,1,1,1,1,1,1,1, 
			0,1,0,1,1,1,1,1,
			0,0,0,0,0,0,0,0, 
			0,1,0,1,1,1,1,1, 
			1,1,1,1,1,1,1,1, 
			0,1,0,1,1,1,1,1,
		};
	
	public GKLRule() {
		super(GKL_POLICY);
	}

//	public int getAction(int[] neighbourhoodState) {
//		int a = (neighbourhoodState.length)/2;
//		if (neighbourhoodState[a] == 0) {
//			return (int)Math.round((neighbourhoodState[a]+neighbourhoodState[a-1]+neighbourhoodState[a-3])/3.0);
//			
//		} else {
//			return (int)Math.round((neighbourhoodState[a]+neighbourhoodState[a+1]+neighbourhoodState[a+3])/3.0);
//			
//		}
//	}

}
 