package ie.nix.mp;

import ie.nix.mp.rules.BestKozaRule;
import ie.nix.mp.rules.DasRule;
import ie.nix.mp.rules.DavisRule;
import ie.nix.mp.rules.GKLRule;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(
	    use = JsonTypeInfo.Id.NAME,
	    include = JsonTypeInfo.As.PROPERTY,
	    property = "type")
@JsonSubTypes({
	    @Type(value = GKLRule.class, name = "GKLRule"),
	    @Type(value = DavisRule.class, name = "DavisRule"),
	    @Type(value = DasRule.class, name = "DasRule"),
	    @Type(value = BestKozaRule.class, name = "BestKozaRule")})
public interface Rule {

	public int getAction(int[] neighbourhoodState);

}
