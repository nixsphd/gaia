package ie.nix.mp.ea;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ie.nix.ea.Agent;
import ie.nix.ea.EvolutionaryAlgorithm;
import ie.nix.ea.Population;
import ie.nix.ea.Population.Genotype;
import ie.nix.ea.operators.AverageEvaluation;
import ie.nix.ea.operators.BestWorstEvaluation;
import ie.nix.mp.DeterministicRule;
import ie.nix.mp.MajorityProblemExperiment;
import ie.nix.mp.ProbabilisticRule;
import ie.nix.mp.Rule;
import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="Experiment")
public abstract class GeneticAlgorithmExperiment<T> extends Experiment {
	
	public static abstract class GeneticAlgorithm<T> extends EvolutionaryAlgorithm<T> {
		
		public GeneticAlgorithm(T[][] preCannedPopulation) {
			super(preCannedPopulation);
		}

		public GeneticAlgorithm(int lenghtOfGenotype, int numberOfAgents) {
			super(lenghtOfGenotype, numberOfAgents);
		}

		public void addGenomesToDataFrame(DataFrame gdf, String columnName) {
			List<Object> chromosomes = new ArrayList<Object>();
			Population<T> population = getPopulation();
			for (int g = 0; g < population.getNumberOfGenotypes(); g++) {
				chromosomes.addAll(Arrays.asList(population.getGenotypes().get(g).getChromosome()));
			}
			gdf.addColumn(columnName, chromosomes);
		
		}

		public void addGenomeToDataFrame(int genomeIndex, DataFrame brdf, String columnName) {
			List<Object> chromosome = new ArrayList<Object>();
			Population<T> population = getPopulation();
			for (int g = 0; g < population.getNumberOfGenotypes(); g++) {
				chromosome.addAll(Arrays.asList(population.getGenotypes().get(genomeIndex).getChromosome()));
			}
			brdf.addColumn(columnName, chromosome);
			
		}

	}

	public static abstract class GeneticAlgorithmRule<T>  implements Rule, Agent<T> {
		
		MajorityProblemExperiment experiment;
		double evaluation;

		@Override
		public double evaluation() {
			evaluation = MajorityProblemExperiment.getFitnessForRule(this, experiment, null);
//			System.out.println("NDB~evaluation evaluation="+evaluation);
	        return evaluation;
		}
		
	}
	
	public static class GeneticAlgorithmDeterministicRule extends DeterministicRule implements Agent<Integer> {
		
		MajorityProblemExperiment experiment;
		double evaluation;

		public GeneticAlgorithmDeterministicRule() {
			super(null);
		}

		public void decode(Genotype<Integer> genotype) {
			setPolicy(Arrays.stream(genotype.getChromosome()).mapToInt(Integer::intValue).toArray());
			
		}

		public double evaluation() {
			evaluation = MajorityProblemExperiment.getFitnessForRule(this, experiment, null);
//			System.out.println("NDB~evaluation evaluation="+evaluation);
	        return evaluation;

		}

		public void setExperiment(MajorityProblemExperiment experiment) {
			this.experiment = experiment;
		}

		public double getEvaluation() {
			return evaluation;
		}

		@Override
		public String toString() {
			return "DeterministicRule[evaluation=" + evaluation + "]";
		}

	}
	
	public static class MajorityProblemProbabilityRule extends ProbabilisticRule implements Agent<Double> {
		
		MajorityProblemExperiment experiment;
		double evaluation;
		
		public MajorityProblemProbabilityRule() {
			super(null);
			
		}

		public void decode(Genotype<Double> genotype) {
			setPolicy(Arrays.stream(genotype.getChromosome()).mapToDouble(Double::doubleValue).toArray());
			
		}

		public double evaluation() {
			evaluation = MajorityProblemExperiment.getFitnessForRule(this, experiment, null);
//			System.out.println("NDB~evaluation evaluation="+evaluation);
	        return evaluation;

		}

		public void setExperiment(MajorityProblemExperiment experiment) {
			this.experiment = experiment;
		}

		public double getEvaluation() {
			return evaluation;
		}

		@Override
		public String toString() {
			return "ProbabilisticRule[evaluation=" + evaluation + "]";
		}

	}
	
	protected static DataFrame getGADataFrame() {
		String genomesFileName = "GA.csv";
		// Process the space-time diagrams filename
		DataFrame gadf = new DataFrame(true);
		gadf.setOutputDirectoryName(Experiment.getExperimentDirectory());
		gadf.addColumns(new String[]{"Generation","Training Best Evaluation", "Training Average Evaluation", "Training Standard Deviation", "Best Evaluation"});
		gadf.initWriter(genomesFileName);
		gadf.writeHeaders();
		return gadf;
	}

	protected static DataFrame getGemonesDataFrame() {
		String genomesFileName = "Genomes.csv";
		// Process the space-time diagrams filename
		DataFrame gdf = new DataFrame();
		gdf.setOutputDirectoryName(Experiment.getExperimentDirectory());
		gdf.initWriter(genomesFileName);
		return gdf;
	}

	protected static DataFrame getBestRulesDataFrame() {
		String bestRulesFileName = "BestRules.csv";
		// Process the space-time diagrams filename
		DataFrame brdf = new DataFrame(true);
		brdf.setOutputDirectoryName(Experiment.getExperimentDirectory());
		brdf.initWriter(bestRulesFileName);
		return brdf;
	}

	protected int neighbourhoodSize; 
	protected int numberOfAutomota;
	protected MajorityProblemExperiment evaluationExperiment;

	// Evolutionary Algorithm
	protected GeneticAlgorithm<T> ga;
	protected int lenghtOfGenotype; 
	protected int numberOfAgents;
	protected int numberOfGenerations;
	protected T[][] genomes;
	protected MajorityProblemExperiment trainingExperiment;
	
	public GeneticAlgorithmExperiment() {
		this(100,100);
		
	}
	
	public GeneticAlgorithmExperiment(int numberOfAgents, int numberOfGenerations) {
		// MP
		setNeighbourhoodSize(3); // Comes from Kaza's & Mitchell's Papers
		setNumberOfAutomota(149); // Comes from Kaza's Paper
		initEvaluationExperiment();
		
		// GA 
		setLenghtOfGenotype((int)Math.pow(2,(neighbourhoodSize*2)+1)); //128 for neighborhood 3;
		setNumberOfAgents(numberOfAgents);
		setNumberOfGenerations(numberOfGenerations);
		initTrainingExperiment();
		
	}

	public void initEvaluationExperiment() {
		int numberOfEvaluationTests = 1000; 
		MajorityProblemExperiment gaussianEvaluationExperiment = new MajorityProblemExperiment(
				MajorityProblemExperiment.generateTests(numberOfEvaluationTests, numberOfAutomota, true));
		gaussianEvaluationExperiment.setNumberOfStepsPoisson(false);
		setEvaluationExperiment(gaussianEvaluationExperiment);
	}

	public abstract void initGeneticAlgorithm(); 

	public void initTrainingExperiment() {
		int numberOfTrainingTests = 100; 
		MajorityProblemExperiment trainingExperiment = new MajorityProblemExperiment(
				MajorityProblemExperiment.generateTests(numberOfTrainingTests*numberOfGenerations, numberOfAutomota)); // From Mitchell
		// Comes from Kaza's Paper;
		trainingExperiment.setNumberOfTests(numberOfTrainingTests);
		trainingExperiment.setNumberOfStepsPoisson(true);
		setTrainingExperiment(trainingExperiment);
		
	}

	public double runExperiment() {
	
		// Initialise the GA
		initGeneticAlgorithm();
	
		// Get the DataFranes ready for the logging.
		DataFrame gdf = null; //getGemonesDataFrame();
		DataFrame gadf = getGADataFrame();
		DataFrame brdf = getBestRulesDataFrame();

		BestWorstEvaluation<T> bestWorstEvaluation = new BestWorstEvaluation<T>();
		AverageEvaluation<T> averageEvaluationOperator = new AverageEvaluation<T>();
		
		// Evolve the system
		int numberOfGenerations = getNumberOfGenerations();
		int generation = 1;
		int numberOfGeneratuionToEvolve = numberOfGenerations/10;
		int bestRuleIndex;
		Rule bestRule;
		double bestRuleEvaluation;
		do {
			ga.evolveAndEvaluate();

			// We use this one as the ScaledFitnessFilter one used in selection will be out of date after the evolution.
			averageEvaluationOperator.operate(ga.getPopulation());
			bestWorstEvaluation.operate(ga.getPopulation());
			
			bestRuleIndex = bestWorstEvaluation.getBestAgentIndex();
			bestRule = (Rule)ga.getAgents().get(bestRuleIndex);
			bestRuleEvaluation = MajorityProblemExperiment.getFitnessForRule(bestRule, getEvaluationExperiment());

			// DON'T CHECK IN
			System.out.println("generation="+generation+
					", bestTrainingEvaluationIndex="+bestRuleIndex+
					", bestTrainingEvaluation="+String.format("%.2f", bestWorstEvaluation.getBestEvaluation())+
					", averageEvaluation="+String.format("%.3f", averageEvaluationOperator.getAverage())+
					", standardDeviation="+String.format("%.3f", averageEvaluationOperator.getStandardDeviation())+
					", bestEvaluation="+String.format("%.3f", bestRuleEvaluation));
			
			if ((gdf != null) && 
				(numberOfGeneratuionToEvolve == 0 || generation%numberOfGeneratuionToEvolve == 0)) {
				ga.addGenomesToDataFrame(gdf, "Generation"+generation);
			}
			
			if (gadf != null) {
				gadf.writeVolatileRow(generation,
						bestWorstEvaluation.getBestEvaluation(),
						averageEvaluationOperator.getAverage(),
						averageEvaluationOperator.getStandardDeviation(),
						bestRuleEvaluation);
				
			}

			if (brdf != null) {
				ga.addGenomeToDataFrame(bestWorstEvaluation.getBestAgentIndex(), brdf, "Generation"+generation);
			}

			// Here we advance to the next batch of tests
			getTrainingExperiment().updateNextTest();
			
			generation++;
			
		} while (generation <= numberOfGenerations);
			
		if (gdf != null) {
			gdf.writeHeaders();
			gdf.writeRows();
		}
		if (brdf != null) {
			brdf.writeHeaders();
			brdf.writeRows();
		}
		
		System.out.println("\""+Experiment.getSeed()+"\", \""+bestRuleEvaluation+"\"");
		
		return bestRuleEvaluation;
	}

	public int getNeighbourhoodSize() {
		return neighbourhoodSize;
	}

	public void setNeighbourhoodSize(int neighbourhoodSize) {
		this.neighbourhoodSize = neighbourhoodSize;
	}

	public int getNumberOfAutomota() {
		return numberOfAutomota;
	}

	public void setNumberOfAutomota(int numberOfAutomota) {
		this.numberOfAutomota = numberOfAutomota;
	}

	public int getLenghtOfGenotype() {
		return lenghtOfGenotype;
	}

	public void setLenghtOfGenotype(int lenghtOfGenotype) {
		this.lenghtOfGenotype = lenghtOfGenotype;
	}

	public int getNumberOfAgents() {
		return genomes.length;
	}

	public void setNumberOfAgents(int numberOfAgents) {
		// Do nothing
	}

	public T[][] getGenomes() {
		return genomes;
	}

	void setGenomes(T[][] genomes) {
		this.genomes = genomes;
		
	}

	public int getNumberOfGenerations() {
		return numberOfGenerations;
	}

	public void setNumberOfGenerations(int numberOfGenerations) {
		this.numberOfGenerations = numberOfGenerations;
	}

	public MajorityProblemExperiment getTrainingExperiment() {
		return trainingExperiment;
	}

	public void setTrainingExperiment(MajorityProblemExperiment trainingExperiment) {
		this.trainingExperiment = trainingExperiment;
	}

	public MajorityProblemExperiment getEvaluationExperiment() {
		return evaluationExperiment;
	}
	
	public void setEvaluationExperiment(MajorityProblemExperiment evaluationExperiment) {
		this.evaluationExperiment = evaluationExperiment;
		
	}
}
