package ie.nix.mp.rules;

import ie.nix.mp.MajorityProblemExperiment;
import ie.nix.util.Experiment;

public class BestRulesExperiment extends Experiment {
	
	MajorityProblemExperiment gKLExperiment;
	MajorityProblemExperiment davisExperiment;
	MajorityProblemExperiment dasExperiment;
	MajorityProblemExperiment bestKozaExperiment;
	
	// #Done in 392.82 secs, GKL fitness is 81.53% Davis fitness is 81.76% Das fitness is 82.14%
	// #Done in 414.07 secs, GKL fitness is 81.53% Davis fitness is 81.78% Das fitness is 82.36%
	// #Done in 592.28 secs, GKL fitness is 81.273% Davis fitness is 81.918% Das fitness is 82.318% Koza's best fitness is 82.229%

	
	public BestRulesExperiment() {
		
		super();
		int totalOfTests = 2; //1000000; // Comes from Kaza's Paper
		int numberOfAutomota = 149; // Comes from Kaza's Paper
		int numberOfTests = totalOfTests; // Comes from Kaza's Paper
		int[][] tests = MajorityProblemExperiment.generateTests(totalOfTests, numberOfAutomota);
		
		gKLExperiment = new MajorityProblemExperiment();
		gKLExperiment.setTests(tests);
		gKLExperiment.setNumberOfTests(numberOfTests);
		gKLExperiment.setRule(new GKLRule());
		
		davisExperiment = new MajorityProblemExperiment();
		davisExperiment.setTests(tests);
		davisExperiment.setNumberOfTests(numberOfTests);
		davisExperiment.setRule(new DavisRule());

		dasExperiment = new MajorityProblemExperiment();
		dasExperiment.setTests(tests);
		dasExperiment.setNumberOfTests(numberOfTests);
		dasExperiment.setRule(new DasRule());

		bestKozaExperiment = new MajorityProblemExperiment();
		bestKozaExperiment.setTests(tests);
		bestKozaExperiment.setNumberOfTests(numberOfTests);
		bestKozaExperiment.setRule(new BestKozaRule());
		
	}

	public MajorityProblemExperiment getGKLExperiment() {
		return gKLExperiment;
	}

	public void setGKLExperiment(MajorityProblemExperiment gKLExperiment) {
		this.gKLExperiment = gKLExperiment;
	}

	public MajorityProblemExperiment getDavisExperiment() {
		return davisExperiment;
	}

	public void setDavisExperiment(MajorityProblemExperiment davisExperiment) {
		this.davisExperiment = davisExperiment;
	}

	public MajorityProblemExperiment getDasExperiment() {
		return dasExperiment;
	}

	public void setDasExperiment(MajorityProblemExperiment dasExperiment) {
		this.dasExperiment = dasExperiment;
	}

	public MajorityProblemExperiment getBestKozaExperiment() {
		return bestKozaExperiment;
	}
	
	public void setBestKozaExperiment(MajorityProblemExperiment bestKozaExperiment) {
		this.bestKozaExperiment = bestKozaExperiment;
	}
	
	public static void main(String[] args) {
		
		BestRulesExperiment experiment = (BestRulesExperiment)init(args, BestRulesExperiment.class);

		if (experiment != null) {
	
			// Test the experiment
			double gKLFitness = MajorityProblemExperiment.getFitnessForRule(experiment.getGKLExperiment().getRule(), experiment.getGKLExperiment());
			double davisFitness = MajorityProblemExperiment.getFitnessForRule(experiment.getDavisExperiment().getRule(), experiment.getDavisExperiment());
			double dasFitness = MajorityProblemExperiment.getFitnessForRule(experiment.getDasExperiment().getRule(), experiment.getDasExperiment());
			double kozaFitness = MajorityProblemExperiment.getFitnessForRule(experiment.getBestKozaExperiment().getRule(), experiment.getDasExperiment());
			stopWatch.stop();
		    System.out.println( "#Done in "+String.format("%.2f", (stopWatch.getTime()/1000.0))+" secs,"
		    		+ " GKL fitness is "+String.format("%.3f", (gKLFitness*100.0))+"%"
		    		+ " Davis fitness is "+String.format("%.3f", (davisFitness*100.0))+"%"
		    		+ " Das fitness is "+String.format("%.3f", (dasFitness*100.0))+"%"
    				+ " Koza's best fitness is "+String.format("%.3f", (kozaFitness*100.0))+"%");
		    
		} else {
		    System.out.println( "#Done.");
		    
		}
	    
	}

}
