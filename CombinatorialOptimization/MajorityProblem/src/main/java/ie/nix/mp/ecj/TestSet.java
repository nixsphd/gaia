package ie.nix.mp.ecj;

import ec.EvolutionState;

public class TestSet {
	
	public static InitialConfiguration[] generateTests(EvolutionState state, int numberOfTests, int numberOfAutomata) {
		InitialConfiguration[] tests = new InitialConfiguration[numberOfTests];
		tests = generateTests(state, numberOfTests, numberOfAutomata, false);
		return tests;
	}

	public static InitialConfiguration[] generateTests(EvolutionState state, int numberOfTests, int numberOfAutomata, 
			boolean balanced) {
		InitialConfiguration[] tests = new InitialConfiguration[numberOfTests];
		for (int t = 0; t < numberOfTests; t++) {
			if (balanced) {
				if (t%2 == 0) {
					tests[t] = new InitialConfiguration(state, numberOfAutomata, false);	
				} else {
					tests[t] = new InitialConfiguration(state, numberOfAutomata, true);	
				}
			} else {
				tests[t] = new InitialConfiguration(state, numberOfAutomata);	
			}
		}
		return tests;
	}
	
	public static InitialConfiguration[] generateUniformTests(EvolutionState state, int numberOfTests, int numberOfAutomata) {
		InitialConfiguration[] tests = new InitialConfiguration[numberOfTests];
		int halfway = numberOfAutomata/2;
//		System.out.println("NDB TestSet.generateUniformTests()~halfway="+halfway);
		for (int t = 0; t < numberOfTests; t++) {
			double lambda;
			if (t%2 == 0) {
				//lambda = ((double)Randomness.nextInt(0,numberOfAutomota/2))/numberOfAutomota;
				int bin = state.random[0].nextInt(halfway+1);
				lambda = ((double)bin)/numberOfAutomata;
//				System.out.println("NDB TestSet.generateUniformTests()~bin="+bin+", lambda="+lambda);
			} else {
//				lambda = ((double)Randomness.nextInt((numberOfAutomota/2)+1,numberOfAutomota))/numberOfAutomota;
				int bin = state.random[0].nextInt(halfway+1)+halfway+1;
				lambda = ((double)bin)/numberOfAutomata;
//				System.out.println("NDB TestSet.generateUniformTests()~bin="+bin+", lambda="+lambda);
			}
			tests[t] = new InitialConfiguration(state, numberOfAutomata, lambda);
//			System.out.println("NDB requested lambda="+String.format("%.2f", lambda)+
//					" gave run with actual lambda="+String.format("%.2f", InitialConfiguration.getLambda(tests[t].test)));
			
		}
		return tests;
	}

	/* Used for Fitness Against Lambda graphs */
	public static InitialConfiguration[] generateTestsWithLambda(EvolutionState state, int numberOfTests, int numberOfAutomata, 
			double lambda) {
		InitialConfiguration[] tests = new InitialConfiguration[numberOfTests];
		for (int t = 0; t < numberOfTests; t++) {
			tests[t] = new InitialConfiguration(state, numberOfAutomata, lambda);
//			System.out.println("NDB requested lambda="+String.format("%.2f", lambda)+
//					" gave run with actual lambda="+String.format("%.2f", getLambda(automata)));
		}
		return tests;
	}

	int numberOfTests = 100; // default is 100.
	
	private InitialConfiguration[] tests;
	
	// For json to write experiments.
	protected TestSet() {
		tests = new InitialConfiguration[numberOfTests];
	}
	
	protected TestSet(int numberOfTests) {
		this.numberOfTests = numberOfTests;
		tests = new InitialConfiguration[numberOfTests];
	}
	
	public TestSet(EvolutionState state, int numberOfTests, int numberOfAutomata) {
		this.numberOfTests = numberOfTests;
		tests = generateTests(state, numberOfTests, numberOfAutomata);
	}
	
	public TestSet(EvolutionState state, int numberOfTests, int numberOfAutomata, boolean balanced) {
		this.numberOfTests = numberOfTests;
		tests = generateTests(state, numberOfTests, numberOfAutomata, balanced);
	}
	
	public TestSet(EvolutionState state, int numberOfTests, int numberOfAutomata, double lambda) {
		this.numberOfTests = numberOfTests;
		tests = generateTestsWithLambda(state, numberOfTests, numberOfAutomata, lambda);
	}
	
	public TestSet(InitialConfiguration[] tests) {
		this.numberOfTests = tests.length;
		this.tests = tests;
		
	}
	
	public TestSet(boolean[][] initialConditions) {
		this.numberOfTests = initialConditions.length;
		for (int t = 0; t < initialConditions.length; t++) {
			tests[t] = new InitialConfiguration(initialConditions[t]);
		}
		
	}
	
	public int getNumberOfTests() {
		return numberOfTests;
	}
	
	public InitialConfiguration[] getTests() {
		return tests;
	}
	
}
