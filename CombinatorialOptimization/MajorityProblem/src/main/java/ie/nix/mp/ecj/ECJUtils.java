package ie.nix.mp.ecj;

import ec.EvolutionState;
import ec.Evolve;
import ec.util.Parameter;
import ec.util.ParameterDatabase;

public class ECJUtils {

	private static EvolutionState state; 
	
	public static EvolutionState getEvolutionState() {
		if (state == null) {
			ParameterDatabase parameterDatabase = new ParameterDatabase();
			parameterDatabase.set(new Parameter("breedthreads"), "1");
			parameterDatabase.set(new Parameter("evalthreads"), "1");
			parameterDatabase.set(new Parameter("seed.0"), "0");
			parameterDatabase.set(new Parameter("state"), "ec.simple.SimpleEvolutionState");
			parameterDatabase.set(new Parameter("silent"), "true");
					
			state = Evolve.initialize(parameterDatabase, 0);
		}
		return state;
		
	}
	
}