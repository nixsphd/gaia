package ie.nix.mp.ea;

import ie.nix.mp.MajorityProblemExperiment;
import ie.nix.util.Experiment;
import ie.nix.util.Randomness;

public class TowardsUnbiased extends Mitchell {

	public static void main(String[] args) {
		
		TowardsUnbiased experiment = (TowardsUnbiased)init(args, TowardsUnbiased.class);
		if (experiment != null) {
			double fitness = experiment.runExperiment();
			stopWatch.stop();
	        System.out.println( "#Seed "+Experiment.getSeed()+" gives fitness of best evolved rule is "+String.format("%.3f", fitness)+" on the gaussian evaluation experiment"
	        		+ " in "+String.format("%.2f", (stopWatch.getTime()/1000.0))+" secs");
	        
		} else {
		    System.out.println( "#Done.");
		    
		}
    	
    }
	
	public static int[][] generateRandomTests(int numberOfTests, int[] numbersOfAutomata, double[] sigmas, boolean balanced) {
		int[][] tests = new int[numberOfTests][];
		final int testsPerStep = -Math.floorDiv(-numberOfTests,numbersOfAutomata.length);
		final int testsPerSigma = -Math.floorDiv(-numberOfTests,sigmas.length);
		for (int t = 0; t < numberOfTests; t++) {
			int numberOfAutomata = numbersOfAutomata[t/testsPerStep];
			tests[t] = new int[numberOfAutomata];
			double lambda = Randomness.nextGaussian(0.5, sigmas[t/testsPerSigma]);
			// Scale it up to the equivalent number of automata
			lambda = Math.rint(lambda * numberOfAutomata);
			// bring all the values < -numberOfAutomata and > numberOfAutomata back into the range. 
			lambda %= numberOfAutomata; 
			// Any values beloe 0 shhould be moves back into range.
			if (lambda < 0.0) {
				lambda += numberOfAutomata;
			}
			// scale it back to a float betweem 0.0 and 1.0.
			lambda /= tests[t].length;
			// if it needs to be balanced
			if (balanced) {
				if (t%2 == 0 && lambda < 0.5) {
					lambda = 1 - lambda;	
				} else if (t%2 == 1 && lambda > 0.5) {
					lambda = 1 - lambda;
				}
			}
			MajorityProblemExperiment.generateBitsetWithLambda(tests[t], lambda);
			
		}
		return tests;
	}

	protected double[] sigmas;
	protected int[] numbersOfAutomota;
	
	public TowardsUnbiased() {
		//(int numberOfAgents, int numberOfGenerations)
		super(1000,200);
		
	}

	public void initGeneticAlgorithm() {
		super.initGeneticAlgorithm();
	}

	public void initTrainingExperiment() {
		int numberOfTrainingTests = 100; 
		boolean balanced = true;

		numbersOfAutomota = new int[]{149};
		
		sigmas = new double[]{1.0, 0.5, 0.1, 0.075, 0.05, 0.04};
//		sigmas = new double[]{
//				1.0, 0.75, 0.5, 0.25, 0.1, 
//				0.085, 0.07, 0.06, 0.05, 0.04};
		
		MajorityProblemExperiment trainingExperiment = new MajorityProblemExperiment(
				generateRandomTests(numberOfTrainingTests*numberOfGenerations, 
						numbersOfAutomota, getSigmas(), balanced)); // My crazy plan...
		trainingExperiment.setNumberOfTests(numberOfTrainingTests);
		trainingExperiment.setNumberOfStepsPoisson(true);
		setTrainingExperiment(trainingExperiment);
	}
	

	public int[] getNumbersOfAutomota() {
		return numbersOfAutomota;
	}

	public void setNumbersOfAutomota(int[] numbersOfAutomota) {
		this.numbersOfAutomota = numbersOfAutomota;
	}

	public double[] getSigmas() {
		return sigmas;
	}
	

	public void setSigmas(double[] sigmas) {
		this.sigmas = sigmas;
	}

}
