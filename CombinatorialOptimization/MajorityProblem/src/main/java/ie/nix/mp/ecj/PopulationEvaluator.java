package ie.nix.mp.ecj;

import ec.Evaluator;
import ec.EvolutionState;
import ec.Individual;
import ec.Subpopulation;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;

@SuppressWarnings("serial")
public class PopulationEvaluator extends Evaluator {
	
	public interface PopulationProblem {
		
		public void prepareToEvaluatePopulation(EvolutionState state);

		public void evaluatePopulation(EvolutionState state);

		public void finishEvaluatingPopulation(EvolutionState state);
		
	}

	public void setup(final EvolutionState state, final Parameter base) {
		super.setup(state,base);
	        
    }
	
	@Override
	public void evaluatePopulation(EvolutionState state) {
		
		PopulationProblem prob = (PopulationProblem)(p_problem);  // just use the prototype		
        prob.prepareToEvaluatePopulation(state);
        prob.evaluatePopulation(state);
        prob.finishEvaluatingPopulation(state);

	}
	
    /** The SimpleEvaluator determines that a run is complete by asking
    each individual in each population if he's optimal; if he 
    finds an individual somewhere that's optimal,
    he signals that the run is complete. */
	// Taken form SimpleEvaluation
	public boolean runComplete(final EvolutionState state) {
	    for(int x = 0;x<state.population.subpops.length;x++) {
	        for(int y=0;y<state.population.subpops[x].individuals.length;y++) {
	            if (state.population.subpops[x].individuals[y].fitness.isIdealFitness()) {
	                return true;
	            }
	        }
	    }
	    return false;
    }

}
