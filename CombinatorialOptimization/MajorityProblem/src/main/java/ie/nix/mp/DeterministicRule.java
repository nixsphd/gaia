package ie.nix.mp;

public class DeterministicRule implements Rule {

	private int[] policy;

	public DeterministicRule(int[] policy) {
		this.policy = policy;
	}

	public int[] getPolicy() {
		return policy;
	}

	protected void setPolicy(int[] policy) {
		this.policy = policy;
	}

	public int getAction(int[] neighbourhoodState) {
		int neighbourhoodStateIndex = 
				neighbourhoodState[6] + 
				(neighbourhoodState[5] << 1) + 
				(neighbourhoodState[4] << 2) + 
				(neighbourhoodState[3] << 3) + 
				(neighbourhoodState[2] << 4) + 
				(neighbourhoodState[1] << 5) + 
				(neighbourhoodState[0] << 6);
		return policy[neighbourhoodStateIndex];
		
	}

}