package ie.nix.mp.ecj;

import org.apache.commons.math3.random.RandomDataGenerator;

import ie.nix.mp.ecj.Rule.Consensus;
import ie.nix.util.Randomness;
import ec.EvolutionState;
import ec.Individual;
import ec.Problem;
import ec.simple.SimpleFitness;
import ec.simple.SimpleProblemForm;
import ec.util.Parameter;

@SuppressWarnings("serial")
public class MajorityProblem extends Problem implements SimpleProblemForm {

	// majority-problem
	public static final String P_MAJORITY_PROBLEM = "majority-problem";
	
	// majority-problem.number-of-tests
	public static final String P_NUMBER_OF_TESTS = "number-of-tests";
	
	// majority-problem.testset-type
	public static final String P_TESTSET_TYPE = "testset-type";
	public static final String P_TESTSET_TYPE_BALANCED = "balanced";
	public static final String P_TESTSET_TYPE_UNIFORM = "uniform";

	// majority-problem.number-of-automata
	public static final String P_NUMBER_OF_AUTOMATA = "number-of-automata";
	
	// majority-problem.max-number-of-steps
	public static final String P_MAX_NUMBER_OF_STEPS = "max-number-of-steps";
	
	// majority-problem.is-number-of-steps-poisson
	public static final String P_IS_NUMBER_OF_STEPS_POISSON = "is-number-of-steps-poisson";

	protected int numberOfTests; 
	protected String testSetType;
	protected int numberOfAutomata;
	protected int maxNumberOfSteps;
	protected boolean isNumberOfStepsPoisson;

	protected TestSet testSet;
	protected RandomDataGenerator rngFromApache;
	
	public static Parameter base() {
		return new Parameter(P_MAJORITY_PROBLEM);
	}
	
	public static boolean test(Rule rule, InitialConfiguration test, int maxNumberOfSteps) {
    	Consensus result = Rule.Consensus.getConsensus(rule.evaluate(test, maxNumberOfSteps));
		if (result.hasConsensus()) { 
			 if (result.getMajority() == test.getMajority()) {
//					System.out.println("NDB~getFitness() Right result="+result.getMajority()+" majority="+test.getMajority());
				return true;
			} else {
//					System.out.println("NDB~getFitness() Wrong result="+result.getMajority()+" majority="+test.getMajority());
				return false;
			}
			 
		} else {
//				System.out.println("NDB~getFitness() No consensus");
			return false;
			
		}
	}

	public static double getFitness(Rule rule, TestSet testset, int maxNumberOfSteps) {
			
		int matches = 0; 
		
		for (InitialConfiguration test : testset.getTests()) {
	    	if (test(rule, test, maxNumberOfSteps)) {
//				System.out.println("NDB~getFitness() MATCH result="+result.getMajority()+" majority="+test.getMajority());
				matches++;
	    	} else {
//				System.out.println("NDB~getFitness() MISMATCH result="+result.getMajority()+" majority="+test.getMajority());
	    	}
	        
		}
		double fitness = matches/(double)(testset.getNumberOfTests());
//		System.out.println("NDB~getFitness()->"+fitness);
	    return fitness;
	        
	}

	public void setup(final EvolutionState state, final Parameter base) {
		
        // very important, remember this
        super.setup(state,base);
        
		Parameter def = base();
		numberOfTests = state.parameters.getIntWithDefault(
			base.push(P_NUMBER_OF_TESTS), 
			def.push(P_NUMBER_OF_TESTS),
			100);

		testSetType = state.parameters.getStringWithDefault(
				base.push(P_TESTSET_TYPE), 
				def.push(P_TESTSET_TYPE), null);
		
		numberOfAutomata = state.parameters.getIntWithDefault(
			base.push(P_NUMBER_OF_AUTOMATA), 
			def.push(P_NUMBER_OF_AUTOMATA),
			149);
		
		maxNumberOfSteps = state.parameters.getIntWithDefault(
			base.push(P_MAX_NUMBER_OF_STEPS), 
			def.push(P_MAX_NUMBER_OF_STEPS), 
			320);
		
		isNumberOfStepsPoisson = state.parameters.getBoolean(
			base.push(P_IS_NUMBER_OF_STEPS_POISSON), 
			def.push(P_IS_NUMBER_OF_STEPS_POISSON), 
			true);
		// If were using a Poisson distribution we'll need to initialise the Poisson number generator. Using Apache's one.
		if (isNumberOfStepsPoisson) {
			rngFromApache = new RandomDataGenerator();
			rngFromApache.reSeed(state.random[0].nextInt());
		}

//		System.out.println("NDB MajorityProblem.setup()~numberOfTests="+numberOfTests);
//		System.out.println("NDB MajorityProblem.setup()~testSetType="+testSetType);
//		System.out.println("NDB MajorityProblem.setup()~numberOfAutomata="+numberOfAutomata);
//		System.out.println("NDB MajorityProblem.setup()~maxNumberOfSteps="+maxNumberOfSteps);
//		System.out.println("NDB MajorityProblem.setup()~isNumberOfStepsPoisson="+isNumberOfStepsPoisson);
	}
	
	protected TestSet getTestSet() {
		return testSet;
	}

	protected void setTestSet(TestSet testSet) {
		this.testSet = testSet;
	}

    /** May be called by the Evaluator prior to a series of individuals to 
    evaluate, and then ended with a finishEvaluating(...).  If this is the
    case then the Problem is free to delay modifying the individuals or their
    fitnesses until at finishEvaluating(...).  If no prepareToEvaluate(...)
    is called prior to evaluation, the Problem must complete its modification
    of the individuals and their fitnesses as they are evaluated as stipulated
    in the relevant evaluate(...) documentation for SimpleProblemForm 
    or GroupedProblemForm.  The default method does nothing.  Note that
    prepareToEvaluate() can be called *multiple times* prior to finishEvaluating()
    being called -- in this case, the subsequent calls may be ignored. */
	public void prepareToEvaluate(final EvolutionState state, final int threadnum) {
		if (testSetType == null) {
			setTestSet(new TestSet(state, numberOfTests, numberOfAutomata));
			
		} else if (testSetType.equals(P_TESTSET_TYPE_BALANCED)) {
			setTestSet(new TestSet(TestSet.generateTests(state, numberOfTests, numberOfAutomata, true)));
			
		} else if (testSetType.equals(P_TESTSET_TYPE_UNIFORM)) {
			setTestSet(new TestSet(TestSet.generateUniformTests(state, numberOfTests, numberOfAutomata)));
			
		} else {
			setTestSet(new TestSet(state, numberOfTests, numberOfAutomata));
			
		}
    }

	/** Evaluates the individual in ind, if necessary (perhaps
    	not evaluating them if their evaluated flags are true),
    	and sets their fitness appropriately. 
     */
	@Override
	public void evaluate(EvolutionState state, Individual individual, int subpopulation, int threadnum) {
		// TODO - Need to fix this, but now I can't remember how:(
    	if(!(individual instanceof Rule)) {
            state.output.fatal( "The individuals for this problem should be Rule.");
            
    	} else {
    		int maxNumberOfSteps = this.maxNumberOfSteps;
    		if (isNumberOfStepsPoisson) {
    			maxNumberOfSteps = (int)rngFromApache.nextPoisson(this.maxNumberOfSteps);
    		}
	        // Evaluate....
	        double value = getFitness((Rule)individual, getTestSet(), maxNumberOfSteps);
	        
	        // Set the t=fitness and mark evaluated.
	        ((SimpleFitness)(individual.fitness)).setFitness(state, value, false);
	        individual.evaluated = true;
	        
    	}
	}
	
	/** Will be called by the Evaluator after prepareToEvaluate(...) is called
    and then a series of individuals are evaluated.  However individuals may
    be evaluated without prepareToEvaluate or finishEvaluating being called
    at all.  See the documentation for prepareToEvaluate for more information. 
    The default method does nothing.*/
	public void finishEvaluating(final EvolutionState state, final int threadnum) {
//		System.out.println("NDB MajorityProblem.finishEvaluating()");
//		testSet = null;
//		System.out.flush();
    }

}
