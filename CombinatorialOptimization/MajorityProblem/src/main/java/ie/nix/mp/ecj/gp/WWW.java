package ie.nix.mp.ecj.gp;

import ec.EvolutionState;
import ec.Problem;
import ec.gp.ADFStack;
import ec.gp.GPData;
import ec.gp.GPIndividual;
import ec.gp.GPNode;

public class WWW extends GPNode {

	private static final long serialVersionUID = 4587138094783751590L;

	public String toString() { 
    	return "WWW"; 
    }

	// Note that there are no children.
    public int expectedChildren() { 
    	return 0; 
    }
	
    // Now what we'll do is define in our Problem two public doubles called currentX and currentY. 
    // These are the values that these nodes return when evaluated. 
	@Override
	public void eval(
			EvolutionState state, 
			int thread, 
			GPData input,
			ADFStack stack, 
			GPIndividual individual, 
			Problem problem) {
	
        BitData rd = ((BitData)(input));
        rd.x = ((GPMajorityProblem)problem).getCurrentWWW();
        
	}
    
}
