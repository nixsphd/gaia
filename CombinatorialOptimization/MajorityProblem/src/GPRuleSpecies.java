package ie.nix.mp.ecj.gp;

import ec.EvolutionState;
import ec.Individual;
import ec.Subpopulation;
import ec.gp.GPSpecies;
import ec.util.Parameter;

public class GPRuleSpecies extends GPSpecies {

	private static final long serialVersionUID = -1763623783209270922L;

	public static final String P_RULES = "rule-species";
   
	int sizeofPopulation;
	
	double count = 0; 

	public Parameter defaultBase() {
		return GPMajorityProblem.base().push(P_RULES); 
	}
	 
    public void setup(final EvolutionState state, final Parameter base) {
	    super.setup(state,base);
	    Parameter def = defaultBase();
	    
	    sizeofPopulation = state.parameters.getInt(
	    		base.pop().push(Subpopulation.P_SUBPOPSIZE), 
	    		def.pop().push(Subpopulation.P_SUBPOPSIZE));

	    System.out.println("NDB RuleSpecies.setup()~sizeofPopulation="+sizeofPopulation);
	    
    }
  
	public Individual newIndividual(final EvolutionState state, int thread) {
		GPRule rule = (GPRule)super.newIndividual(state, thread);
	    return rule;
	    
    }
	
}

