package ie.nix.mp.ecj;

import ec.EvolutionState;
import ec.Individual;
import ec.Problem;
import ec.simple.SimpleFitness;
import ec.simple.SimpleProblemForm;

@SuppressWarnings("serial")
public class TestProblem extends Problem implements SimpleProblemForm {

	/** Evaluates the individual in ind, if necessary (perhaps
    	not evaluating them if their evaluated flags are true),
    	and sets their fitness appropriately. 
     */
	@Override
	public void evaluate(EvolutionState state, Individual individual, int subpopulation, int threadnum) {
		// TODO - Need to fix this, but now I can't remember how:(
    	if(!(individual instanceof Rule)) {
            state.output.fatal( "The individuals for this problem should be Rule.");
            
    	} else {
	    	TestRule rule = (TestRule)individual;
	        ((SimpleFitness)(rule.fitness)).setFitness(state, (rule.id%5 == 0? 1: 0), false);
	        rule.evaluated = true;
			System.out.println("NDB "+rule);
    	}
	}

}
