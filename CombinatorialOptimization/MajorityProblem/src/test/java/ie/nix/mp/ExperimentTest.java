package ie.nix.mp;

import java.util.Arrays;

import ie.nix.mp.MajorityProblemExperiment;

import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestCase;

public class ExperimentTest extends TestCase {
	
	@Test
	public void testGetNeighbourhoodState0() {
		
		int[] automota = {
				4,5,6,7,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,
			}; 
		int automoton = 0;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodState(automota, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}
	
	@Test
	public void testGetNeighbourhoodState1() {
		
		int[] automota = {
				3,4,5,6,7,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,
			}; 
		int automoton = 1;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodState(automota, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodState4() {
		
		int[] automota = {
				0,1,2,3,4,5,6,7,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			}; 
		int automoton = 4;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodState(automota, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}
	
	@Test
	public void testGetNeighbourhoodState127() {
		
		int[] automota = {
				5,6,7,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,
			}; 
		int automoton = 127;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodState(automota, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}
	
	protected void testGetNeighbourhoodState(int[] automota, int automoton, int neighbourhoodSize, int[] expectedNeighbourhoodState) {
		int[] actualNeighbourhoodState = MajorityProblemExperiment.getNeighbourhoodState(automota, automoton, neighbourhoodSize);
		
//		System.out.println("expectedNeighbourhoodState="+Arrays.toString(expectedNeighbourhoodState));
//		System.out.println("actualNeighbourhoodState="+Arrays.toString(actualNeighbourhoodState));
		Assert.assertTrue(Arrays.equals(expectedNeighbourhoodState, actualNeighbourhoodState));
		
	}
	
	@Test
	public void testGetNeighbourhoodStateWithHalo0() {
		
		int[] automotaWithHalo = {
				1,2,3,
				4,5,6,7,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,
				4,5,6,
			}; 
		int automoton = 0;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodStateWithHalo(automotaWithHalo, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodStateWithHalo1() {
		
		int[] automotaWithHalo = {
				0,1,2,
				3,4,5,6,7,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,
				3,4,5
			}; 
		int automoton = 1;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodStateWithHalo(automotaWithHalo, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodStateWithHalo4() {
		
		int[] automotaWithHalo = {
				0,0,0,
				0,1,2,3,4,5,6,7,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,1,2,
			}; 
		int automoton = 4;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodStateWithHalo(automotaWithHalo, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodStateWithHalo127() {
		
		int[] automotaWithHalo = {
				2,3,4,
				5,6,7,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,
				5,6,7,
			}; 
		int automoton = 127;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodStateWithHalo(automotaWithHalo, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	protected void testGetNeighbourhoodStateWithHalo(int[] automota, int automoton, int neighbourhoodSize, int[] expectedNeighbourhoodState) {
		int[] actualNeighbourhoodState = MajorityProblemExperiment.getNeighbourhoodStateWithHalo(automota, automoton, neighbourhoodSize);
		
//		System.out.println("expectedNeighbourhoodState="+Arrays.toString(expectedNeighbourhoodState));
//		System.out.println("actualNeighbourhoodState="+Arrays.toString(actualNeighbourhoodState));
		Assert.assertTrue(Arrays.equals(expectedNeighbourhoodState, actualNeighbourhoodState));
		
	}
		
}
