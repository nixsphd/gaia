package ie.nix.mp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ie.nix.mp.MajorityProblemExperiment;
import ie.nix.mp.MajorityProblemExperiment.FitnessFunction;
import ie.nix.mp.rules.GKLRule;
import ie.nix.util.DataFrame;
import ie.nix.util.Equals;
import ie.nix.util.Experiment;
import ie.nix.util.Randomness;
import ie.nix.util.ToString;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestCase;

@SuppressWarnings("unused")
public class MajorityProblemExperimentTest extends TestCase {
	
	protected static String experimentDir = "src/test/java/ie/nix/mp/";

	@Test
	public void testGetLambda0p0() {
		
		int[] automota = {
				0,0,0,0,0,0,0,0,0,0
			}; 
		double expectedLambda = 0.0;
		
		testGetLambda(automota, expectedLambda);
	}
	
	@Test
	public void testGetLambda1p0() {
		
		int[] automota = {
				1,1,1,1,1,1,1,1,1,1
			}; 
		double expectedLambda = 1.0;
		
		testGetLambda(automota, expectedLambda);
	}
	
	@Test
	public void testGetLambda0p5() {
		
		int[] automota = {
				1,1,1,1,1,0,0,0,0,0
			}; 
		double expectedLambda = 0.5;
		
		testGetLambda(automota, expectedLambda);
	}
	
	@Test
	public void testGetLambda0p55() {
		
		int[] automota = {
				1,1,1,1,1,0,0,0,0
			}; 
		double expectedLambda = 0.5555555;
		
		testGetLambda(automota, expectedLambda);
	}

	protected void testGetLambda(int[] automota, double expectedLambda) {
		
		double actualLambda = MajorityProblemExperiment.getLambda(automota);
		
//		System.out.println("expectedLambda="+expectedLambda);
//		System.out.println("actualLambda="+actualLambda);
		Assert.assertEquals(expectedLambda, actualLambda, 0.0000001);
		
	}
	
	@Test
	public void testGetMajorityOn0p55() {
		
		int[] automota = {
				1,1,1,1,1,0,0,0,0
			}; 
		int expectedMajority = 1;
		
		testGetMajority(automota, expectedMajority);
	}
	
	@Test
	public void testGetMajorityOn0p45() {
		
		int[] automota = {
				1,1,1,1,0,0,0,0,0
			}; 
		int expectedMajority = 0;
		
		testGetMajority(automota, expectedMajority);
	}

	protected void testGetMajority(int[] automota, double expectedMajority) {
		
		double actualMajority = MajorityProblemExperiment.getMajority(automota);
		
//		System.out.println("expectedMajority="+expectedMajority);
//		System.out.println("actualMajority="+actualMajority);
		Assert.assertEquals(expectedMajority, actualMajority);
		
	}
	
	@Test
	public void testGetConsensus1() {
		
		int[] automota = {
				1,1,1,1,1,1,1,1,1,1
			}; 
		int expectedConsensus = 1;
		
		testGetConsensus(automota, expectedConsensus);
	}
	
	@Test
	public void testGetConsensus0() {
		
		int[] automota = {
				0,0,0,0,0,0,0,0,0,0
			}; 
		int expectedConsensus = 0;
		
		testGetConsensus(automota, expectedConsensus);
	}
	
	@Test
	public void testGetConsensusInconclusive() {
		
		int[] automota = {
				1,1,1,1,0,0,0,0,0
			}; 
		int expectedConsensus = -1;
		
		testGetConsensus(automota, expectedConsensus);
	}

	protected void testGetConsensus(int[] automota, double expectedConsensus) {
		
		double actualConsensus = MajorityProblemExperiment.getConsensus(automota);
		
//		System.out.println("expectedConsensus="+expectedConsensus);
//		System.out.println("actualConsensus="+actualConsensus);
		Assert.assertEquals(expectedConsensus, actualConsensus);
		
	}
	
	@Test
	public void testGetConsensusIgnoreHalos0() {
		int[] automota = {
				1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1
			}; 
		int expectedConsensus = 0;
	
		testGetConsensusIgnoreHalos(automota, expectedConsensus);
	}
	@Test
	public void testGetConsensusIgnoreHalos1() {
		int[] automota = {
				0,0,0,1,1,1,1,1,1,1,1,1,1,0,0,0
			}; 
		int expectedConsensus = 1;
	
		testGetConsensusIgnoreHalos(automota, expectedConsensus);
	}
	
	protected void testGetConsensusIgnoreHalos(int[] automota, double expectedConsensus) {
		
		double actualConsensus = MajorityProblemExperiment.getConsensusIgnoreHalos(automota, 3);
		
//		System.out.println("expectedConsensus="+expectedConsensus);
//		System.out.println("actualConsensus="+actualConsensus);
		Assert.assertEquals(expectedConsensus, actualConsensus);
		
	}

	@Test
	public void testGetNeighbourhoodState0() {
		
		int[] automota = {
				4,5,6,7,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,
			}; 
		int automoton = 0;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodState(automota, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodState1() {
		
		int[] automota = {
				3,4,5,6,7,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,
			}; 
		int automoton = 1;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodState(automota, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodState4() {
		
		int[] automota = {
				0,1,2,3,4,5,6,7,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			}; 
		int automoton = 4;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodState(automota, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodState127() {
		
		int[] automota = {
				5,6,7,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,
			}; 
		int automoton = 127;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodState(automota, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	protected void testGetNeighbourhoodState(int[] automota, int automoton, int neighbourhoodSize, int[] expectedNeighbourhoodState) {
		int[] actualNeighbourhoodState = MajorityProblemExperiment.getNeighbourhoodState(automota, automoton, neighbourhoodSize);
		
//		System.out.println("expectedNeighbourhoodState="+Arrays.toString(expectedNeighbourhoodState));
//		System.out.println("actualNeighbourhoodState="+Arrays.toString(actualNeighbourhoodState));
		Assert.assertTrue(Arrays.equals(expectedNeighbourhoodState, actualNeighbourhoodState));
		
	}

	@Test
	public void testGetNeighbourhoodStateWithHalo0() {
		
		int[] automotaWithHalo = {
				1,2,3,
				4,5,6,7,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,
				4,5,6,
			}; 
		int automoton = 0;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodStateWithHalo(automotaWithHalo, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodStateWithHalo1() {
		
		int[] automotaWithHalo = {
				0,1,2,
				3,4,5,6,7,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,
				3,4,5
			}; 
		int automoton = 1;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodStateWithHalo(automotaWithHalo, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodStateWithHalo4() {
		
		int[] automotaWithHalo = {
				0,0,0,
				0,1,2,3,4,5,6,7,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,1,2,
			}; 
		int automoton = 4;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodStateWithHalo(automotaWithHalo, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	@Test
	public void testGetNeighbourhoodStateWithHalo127() {
		
		int[] automotaWithHalo = {
				2,3,4,
				5,6,7,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,
				5,6,7,
			}; 
		int automoton = 127;
		int neighbourhoodSize = 3;
		int[] expectedNeighbourhoodState = {1,2,3,4,5,6,7};
		
		testGetNeighbourhoodStateWithHalo(automotaWithHalo, automoton, neighbourhoodSize, expectedNeighbourhoodState);
	}

	protected void testGetNeighbourhoodStateWithHalo(int[] automota, int automoton, int neighbourhoodSize, int[] expectedNeighbourhoodState) {
		int[] actualNeighbourhoodState = MajorityProblemExperiment.getNeighbourhoodStateWithHalo(automota, automoton, neighbourhoodSize);
		
//		System.out.println("expectedNeighbourhoodState="+Arrays.toString(expectedNeighbourhoodState));
//		System.out.println("actualNeighbourhoodState="+Arrays.toString(actualNeighbourhoodState));
		Assert.assertTrue(Arrays.equals(expectedNeighbourhoodState, actualNeighbourhoodState));
		
	}
	
	@Test
	public void testGenerateAutomotaWithLambda0p0() {
		double lambda = 0.0;
		int numberOfAutomota = 149;
		testGenerateAutomotaWithLambda(lambda, numberOfAutomota);
		
	}
	
	@Test
	public void testGenerateAutomotaWithLambda1p0() {
		double lambda = 1.0;
		int numberOfAutomota = 149;
		testGenerateAutomotaWithLambda(lambda, numberOfAutomota);
		
	}
	
	@Test
	public void testGenerateAutomotaWithLambda0p1() {
		double lambda = 0.1;
		int numberOfAutomota = 149;
		testGenerateAutomotaWithLambda(lambda, numberOfAutomota);
		
	}
	
	@Test
	public void testGenerateAutomotaWithLambda0p5() {
		double lambda = 0.5;
		int numberOfAutomota = 149;
		testGenerateAutomotaWithLambda(lambda, numberOfAutomota);
		
	}
	
	@Test
	public void testGenerateAutomotaWithLambda0p25() {
		double lambda = 0.25;
		int numberOfAutomota = 149;
		testGenerateAutomotaWithLambda(lambda, numberOfAutomota);
		
	}
	
	@Test
	public void testGenerateAutomotaWithLambda0p9() {
		double lambda = 0.9;
		int numberOfAutomota = 149;
		testGenerateAutomotaWithLambda(lambda, numberOfAutomota);
		
	}
	
	protected void testGenerateAutomotaWithLambda(double lambda, int numberOfAutomota) {

		Randomness.setSeed(0);
	
		int[] actualAutomata = new int[numberOfAutomota];
		MajorityProblemExperiment.generateBitsetWithLambda(actualAutomata, lambda);
//		System.out.println("actualAutomata="+Arrays.toString(actualAutomata));
		
		double actualLambda = MajorityProblemExperiment.getLambda(actualAutomata);
//		System.out.println("lambda="+lambda);
//		System.out.println("actualLambda="+actualLambda);
		Assert.assertTrue(Equals.equals(lambda, actualLambda, 0.007));
		
	}

	@Test
	public void testGenerateRandomExperiment() {
		int numberOfTests = 10;
		int numberOfAutomota = 149;
		double expectedMean = 0.5; 
		double expectedSD = 0.04; 
		testGenerateRandomExperiment(numberOfTests, numberOfAutomota, expectedMean, expectedSD);
		
	}

	public void testGenerateRandomExperiment(int numberOfTests, int numberOfAutomota, double expectedMean, double expectedSD) {
		
		Randomness.setSeed(0);
		int[][] actualTests  = 
				MajorityProblemExperiment.generateTests(numberOfTests, numberOfAutomota);

		SummaryStatistics stats = new SummaryStatistics();
		for (int t = 0; t < numberOfTests; t++ ) {
			double actualLambda = MajorityProblemExperiment.getLambda(actualTests[t]);
			stats.addValue(actualLambda);
			
		}
		double actualMean = stats.getMean();
		double actualSD = stats.getStandardDeviation();
//		System.out.println("expectedMean="+expectedMean);
//		System.out.println("actualMean="+actualMean);
//		System.out.println("expectedSD="+expectedSD);
//		System.out.println("actualSD="+actualSD);
		Assert.assertEquals(expectedMean, actualMean, 0.01);
		Assert.assertEquals(expectedSD, actualSD, 0.05);
		
	}

	@Test
	public void testGetFitnessForPolicy() throws IOException {

		Randomness.setSeed(0);
		String experimentFileName = "TestExperiment.json";
    	String expectedSpaceTimeFileName = "ExpectedSpaceTime.csv";
    	double expectedFitness = 1.0;
    	Rule gklRule = new GKLRule();
    	
    	MajorityProblemExperiment experiment = (MajorityProblemExperiment)Experiment.
    			readExperiemtFromJSON(experimentDir+experimentFileName, MajorityProblemExperiment.class);

		DataFrame stdf = new DataFrame();
		stdf.setOutputDirectoryName(experimentDir);
    	
    	double actualFitness = MajorityProblemExperiment.getFitnessForRule(gklRule, experiment, stdf);

//        System.out.println( "expectedFitness="+expectedFitness);
//        System.out.println( "actualFitness="+actualFitness);
        Assert.assertEquals(expectedFitness, actualFitness, 0.0000001);
        
    	String actualSpaceTimeFileName = "ActualSpaceTime.csv";
        stdf.write(actualSpaceTimeFileName);
        Assert.assertTrue(Equals.equals(experimentDir+actualSpaceTimeFileName, experimentDir+expectedSpaceTimeFileName));
        Files.delete(Paths.get(experimentDir,actualSpaceTimeFileName));
        
	}
	
//	@Test
//	public void testGetMajorityForPolicy1Step() throws IOException {
//		
//		Rule rule = new GKLRule();
//    	FitnessFunction fitnessFunction = FitnessFunction.Proportional;
//		int[] initialAutomota = {
//				1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1,
//		};
//		int neighbourhoodSize = 3;
//		double expectedMajority = 1.0;
//		Object[] expectedSpaceTimeData  = new Integer[]{
//				1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 
//				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
//		};
//		int maxNumberOfSteps = (expectedSpaceTimeData.length/initialAutomota.length) - 1 ;
//	
//		testGetMajorityForPolicy(rule, fitnessFunction, 
//				initialAutomota, neighbourhoodSize, maxNumberOfSteps,
//				expectedMajority, expectedSpaceTimeData);
//	    
//	}
//
//	@Test
//	public void testGetMajorityForPolicy() throws IOException {
//    	
//    	Rule rule = new GKLRule();
//    	FitnessFunction fitnessFunction = FitnessFunction.Proportional;
//    	int[] initialAutomota = {
//    			1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1
//    	};
//    	int neighbourhoodSize = 3;
//
//    	double expectedMajority = 1.0;
//    	Object[] expectedSpaceTimeData  = new Integer[]{
//    			1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 
//    			1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 
//    			1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 
//    			0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 
//    			0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 
//    			0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 
//    			0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 
//    			1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 
//    			0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 
//    			1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 
//    			0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 
//    			1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 
//    			1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 
//    			1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 
//    			1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 
//    			1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 
//    			1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
//    			0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
//    			1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
//    	};
//		int maxNumberOfSteps = (expectedSpaceTimeData.length/initialAutomota.length) - 1 ;
//    
//    	testGetMajorityForPolicy(rule, fitnessFunction, initialAutomota, 
//    			neighbourhoodSize, maxNumberOfSteps, 
//    			expectedMajority, expectedSpaceTimeData);
//        
//	}
//
//	protected void testGetMajorityForPolicy(Rule rule, FitnessFunction fitnessFunction,
//			int[] initialAutomota, int neighbourhoodSize, int maxNumberOfSteps, 
//			double expectedMajority, Object[] expectedSpaceTimeData) {
//    	
//    	List<Object> actualSpaceTimeData = new ArrayList<Object>(
//    			initialAutomota.length*maxNumberOfSteps);
//    	
//    	double actualMajority = MajorityProblemExperiment.getMajorityForPolicy(rule, fitnessFunction, initialAutomota,  
//    			neighbourhoodSize, maxNumberOfSteps, actualSpaceTimeData);
//        
////        System.out.println( "expectedSpaceTimeData="+ToString.toString(expectedSpaceTimeData));
////        System.out.println( "actualSpaceTimeData  ="+actualSpaceTimeData);
//        Assert.assertTrue(Equals.equals(expectedSpaceTimeData, actualSpaceTimeData));
//    	
////        System.out.println( "expectedMajority="+expectedMajority);
////        System.out.println( "actualMajority="+actualMajority);
//        Assert.assertEquals(expectedMajority, actualMajority, 0.0);
//	}
		
	@Test
	public void testGetMajorityForPolicy1StepWithHalo() throws IOException {
		
		Rule rule = new GKLRule();
		int[] initialAutomota = {
				1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1,
		};
		int neighbourhoodSize = 3;
		double expectedMajority = 1.0;
		Object[] expectedSpaceTimeData  = new Integer[]{
				1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
		};
		int maxNumberOfSteps = (expectedSpaceTimeData.length/initialAutomota.length) - 1;
		
		testGetMajorityForPolicyWithHalo(rule, 
				initialAutomota, neighbourhoodSize, maxNumberOfSteps, 
				expectedMajority, expectedSpaceTimeData);
	    
	}

	@Test
	public void testGetMajorityForPolicyWithHalo() throws IOException {
		
		Rule rule = new GKLRule();
		int[] initialAutomota = {
				1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1
		};
		int neighbourhoodSize = 3;
		double expectedMajority = 1.0;
		Object[] expectedSpaceTimeData  = new Integer[]{
				1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 
				1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 
				1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 
				0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1, 
				0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 
				0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 
				0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 
				1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 
				0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 
				1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 
				0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 
				1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 
				1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 
				1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 
				1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 
				1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 
				1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
				0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
				1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 
		};
		int maxNumberOfSteps = (expectedSpaceTimeData.length/initialAutomota.length) - 1;
	
		testGetMajorityForPolicyWithHalo(rule, 
				initialAutomota, neighbourhoodSize, maxNumberOfSteps, 
				expectedMajority, expectedSpaceTimeData);
	    
	}

	protected void testGetMajorityForPolicyWithHalo(Rule rule, int[] initialAutomota, int neighbourhoodSize, int maxNumberOfSteps,
			double expectedMajority, Object[] expectedSpaceTimeData) {
    	
    	List<Object> actualSpaceTimeData = new ArrayList<Object>(
    			initialAutomota.length*maxNumberOfSteps);
    	
    	double actualMajority = MajorityProblemExperiment.getRuleMajorityForTest(rule, 
    			initialAutomota, neighbourhoodSize, maxNumberOfSteps, actualSpaceTimeData);
        
//        System.out.println( "expectedSpaceTimeData="+ToString.toString(expectedSpaceTimeData));
//        System.out.println( "actualSpaceTimeData  = "+actualSpaceTimeData);
        Assert.assertTrue(Equals.equals(expectedSpaceTimeData, actualSpaceTimeData));
    	
//		System.out.println( "expectedMajority="+expectedMajority);
//		System.out.println( "actualMajority="+actualMajority);
		Assert.assertEquals(expectedMajority, actualMajority, 0.0);
		
	}
	
//	@Test
//	public void testGetMajorityForPolicyWithProportionalFitnessFunction() throws IOException {
//		
//		Rule rule = new GKLRule();
//		FitnessFunction fitnessFunction = FitnessFunction.Proportional;
//		int[] initialAutomota = {
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0,
//		};
//		int neighbourhoodSize = 3;
//	
//		double expectedMajority = 1.0;
//		Object[] expectedSpaceTimeData  = new Integer[]{
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0,
//		};
//		int maxNumberOfSteps = (expectedSpaceTimeData.length/initialAutomota.length) - 1 ;
//	
//		testGetMajorityForPolicy(rule, fitnessFunction, initialAutomota, 
//				neighbourhoodSize, maxNumberOfSteps, 
//				expectedMajority, expectedSpaceTimeData);
//	
//	}
	
//	@Test
//	public void testGetMajorityForPolicyWithPreformanceFitnessFunction() throws IOException {
//		
//		Rule rule = new GKLRule();
//		FitnessFunction fitnessFunction = FitnessFunction.Preformance;
//		int[] initialAutomota = {
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0,
//		};
//		int neighbourhoodSize = 3;
//	
//		double expectedMajority = -1.0;
//		Object[] expectedSpaceTimeData  = new Integer[]{
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 
//				1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0,
//		};
//		int maxNumberOfSteps = (expectedSpaceTimeData.length/initialAutomota.length) - 1 ;
//	
//		testGetMajorityForPolicy(rule, fitnessFunction, initialAutomota, 
//				neighbourhoodSize, maxNumberOfSteps, 
//				expectedMajority, expectedSpaceTimeData);
//	
//	}
	
	@Test
	public void testGenerateTestsBalanced() throws IOException {

		Randomness.setSeed(0);
		
		boolean balanced = true;
		int numberOfAutomota = 149;
		int numberOfTests = 10;
		
		int[][] tests = MajorityProblemExperiment.generateTests(numberOfTests, numberOfAutomota, balanced);
		
		int majorityOn = 0;
		int majorityOff = 0;
		
		for (int t = 0; t < tests.length ; t++) {
			if (MajorityProblemExperiment.getMajority(tests[t]) == 0) {
				majorityOn++;
			} else {
				majorityOff++;
			}
		}
//		System.out.println( "NDB::testGenerateTestsBalanced()~majorityOn="+majorityOn);
//		System.out.println( "NDB::testGenerateTestsBalanced()~majorityOff="+majorityOff);
		Assert.assertTrue(majorityOn == majorityOff);
		
	}

	@Test
	public void testProbabilisticRule0() {
		Randomness.setSeed(0);
		double[] policy = {
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		};
	
		int[] neighbourhoodState = {0,0,0,0,0,0,0};
		int expectedAction = 0;

		testProbabilisticRule(policy, neighbourhoodState, expectedAction);
	}
	@Test
	public void testProbabilisticRule1() {
		Randomness.setSeed(0);
		double[] policy = {
			1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		};
	
		int[] neighbourhoodState = {0,0,0,0,0,0,0};
		int expectedAction = 1;

		testProbabilisticRule(policy, neighbourhoodState, expectedAction);
	}
	
	@Test
	public void testProbabilisticRule0p1() {
		Randomness.setSeed(0);
		double[] policy = {
			0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		};
	
		int[] neighbourhoodState = {0,0,0,0,0,0,0};
		int expectedAction = 0;

		testProbabilisticRule(policy, neighbourhoodState, expectedAction);
	}
	
	@Test
	public void testProbabilisticRule0p9() {
		Randomness.setSeed(0);
		double[] policy = {
			0.9, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		};
	
		int[] neighbourhoodState = {0,0,0,0,0,0,0};
		int expectedAction = 1;

		testProbabilisticRule(policy, neighbourhoodState, expectedAction);
	}
	
	@Test
	public void testProbabilisticRule0p5() {
		double[] policy = {
			0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
			0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
		};
	
		int[] neighbourhoodState = {0,0,0,0,0,0,0};

		Randomness.setSeed(0);
		int expectedAction = 0;
		testProbabilisticRule(policy, neighbourhoodState, expectedAction);

		expectedAction = 1;
		testProbabilisticRule(policy, neighbourhoodState, expectedAction);

		expectedAction = 0;
		testProbabilisticRule(policy, neighbourhoodState, expectedAction);

		expectedAction = 0;
		testProbabilisticRule(policy, neighbourhoodState, expectedAction);
	}
	
	public void testProbabilisticRule(double[] policy, int[] neighbourhoodState, int expectedAction) {
		
		ProbabilisticRule rule = new ProbabilisticRule(policy);
		int actualAction = rule.getAction(neighbourhoodState);

//		System.out.println( "NDB::testProbabilisticRule()~actualAction="+actualAction);
//		System.out.println( "NDB::testProbabilisticRule()~expectedAction="+expectedAction);

		Assert.assertTrue(actualAction == expectedAction);

	}
	
	
}
