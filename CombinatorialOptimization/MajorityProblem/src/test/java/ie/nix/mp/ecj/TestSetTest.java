package ie.nix.mp.ecj;

import ie.nix.util.ToString;

import java.util.Arrays;

import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestCase;
import ec.EvolutionState;
import ec.Evolve;
import ec.util.ParameterDatabase;

@SuppressWarnings("unused")
public class TestSetTest  extends TestCase {

	public String PARAMS_FILE =  "src/test/java/ie/nix/mp/ecj/test.params";
	
    protected EvolutionState state;
    protected ParameterDatabase parameters;
		
	public static String toString(TestSet testSet) {	
		return toString(testSet.getTests());
	
	}

	public static String toString(InitialConfiguration[] tests) {
		StringBuilder string = new StringBuilder("\n"+"new TestSet(new Test[]{");
		for (int s = 0; s < tests.length; s++) {
			string.append("\n\t"+InitialConfigurationTest.toString(tests[s])+", ");
			string.replace(string.length()-2, string.length(),", ");
		}
		string.replace(string.length()-2, string.length(),"}\n);");
		return string.toString();
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		parameters = Evolve.loadParameterDatabase(new String[]{
				"-file", PARAMS_FILE});
		state = Evolve.initialize(parameters, 0);  
	}

//	public TestSet(EvolutionState state)
	
	@Test
	public void testRandomTestSet() {
		InitialConfiguration[] expectedTests = {
			new InitialConfiguration(new boolean[]{true,true,true,true,true,true,true,true,false}), 
			new InitialConfiguration(new boolean[]{true,true,false,false,false,true,false,true,false}), 
			new InitialConfiguration(new boolean[]{false,false,true,true,true,false,true,false,true}),
			new InitialConfiguration(new boolean[]{true,false,false,false,true,false,false,true,true})
		};
		testRandomTestSet(expectedTests);
	}
	
	public void testRandomTestSet(InitialConfiguration[] expectedTests) {

		state.random[0].setSeed(0);
		TestSet testSet = new TestSet(state, 4, 9);	
		InitialConfiguration[] actualTests = testSet.getTests();
		
//		System.out.println("NDB::testRandomTest()~expectedTests="+TestSetTest.toString(expectedTests));
//		System.out.println("NDB::testRandomTest()~actualTests=  "+TestSetTest.toString(actualTests));
		
		Assert.assertTrue(Arrays.equals(expectedTests, actualTests));

	}

//	public TestSet(EvolutionState state, boolean balanced)
	
	@Test
	public void testBalancedTestSetTrue() {
		boolean balanced = true;
		testBalancedTest(balanced);
	}
	
	@Test
	public void testBalancedTestSetFalse() {
		boolean balanced = false;
		testBalancedTest(balanced);
	}
	
	public void testBalancedTest(boolean balanced) {

		// Need a lot to tests for this test.
		TestSet testSet = new TestSet(state, 100, 9, balanced);	
		InitialConfiguration[] actualTests = testSet.getTests();
		int swing = 0;
		for (InitialConfiguration test : actualTests) {
			if (InitialConfiguration.getMajority(test.getTest()) == true) {
				swing++;
			} else {
				swing--;
			}
		}
		boolean actualBalanced = (swing == 0);
		
//		System.out.println("NDB::testBalancedTest()~swing="+swing);
//		System.out.println("NDB::testBalancedTest()~balanced="+balanced);
//		System.out.println("NDB::testBalancedTest()~actualBalanced="+actualBalanced);
		
		Assert.assertEquals(balanced, actualBalanced);

	}
	
//	public TestSet(EvolutionState state, double lambda)
	
	@Test
	public void testLambdaTest0p0() {
		double lambda = 0.0;
		testLambdaTest(lambda);
	}
	
	@Test
	public void testLambdaTest1p0() {
		double lambda = 1.0;
		testLambdaTest(lambda);
	}

	@Test
	public void testLambdaTest0p5() {
		double lambda = 0.5;
		testLambdaTest(lambda);
	}
	
	
	public void testLambdaTest(double lambda) {
		int numberOfAutomata = 9;
		TestSet testSet = new TestSet(state, 4, numberOfAutomata, lambda);	
		InitialConfiguration[] actualTests = testSet.getTests();

//		System.out.println("NDB::testLambdaTest()~lambda="+lambda);
		for (InitialConfiguration test : actualTests) {
			double actualLambda = InitialConfiguration.getLambda(test.getTest());
//			System.out.println("NDB::testLambdaTest()~actualLambda="+actualLambda);
			Assert.assertEquals(lambda, actualLambda, 1.0/numberOfAutomata);
		}
	}
}

