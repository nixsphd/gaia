package ie.nix.mp.ecj;

import ie.nix.mp.ecj.Rule;
import ie.nix.mp.ecj.Rule.Consensus;
import ie.nix.mp.rules.GKLRule;
import ie.nix.util.Equals;
import ie.nix.util.ToString;

import java.util.Arrays;
import java.util.stream.IntStream;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestCase;
import ec.EvolutionState;
import ec.Evolve;
import ec.util.ParameterDatabase;

public class RuleSpeciesTest  extends TestCase {

	public String PARAMS_FILE =  "src/test/java/ie/nix/mp/ecj/rule.params";
	
    protected EvolutionState state;
    protected ParameterDatabase parameters;
    protected RuleSpecies ruleSpecies;
    protected Rule rule;
		
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		parameters = Evolve.loadParameterDatabase(new String[]{"-file", PARAMS_FILE});
		state = Evolve.initialize(parameters, 0);  
		state.startFresh();
		
		ruleSpecies = (RuleSpecies)state.population.subpops[0].species;
		rule = (Rule)state.population.subpops[0].individuals[0];
		
//		Rule.NEIGHBOURHOOD_SIZE = 3;		
//		InitialConfiguration.NUMBER_OF_AUTOMATA = 9;
		
	}

	@Test
	public void testUniformGenomes() {
		boolean uniform = true;
		double expectedMean = 0.5;
		double expectedStd = 0.3;

		testUniformGenomes(uniform, expectedMean, expectedStd);
	}

	@Test
	public void testUniformGenomesNot() {
		boolean uniform = false;
		double expectedMean = 0.5;
		double expectedStd = 0.04;
		
		testUniformGenomes(uniform, expectedMean, expectedStd);
	}
		 
	public void testUniformGenomes(boolean uniform, double expectedMean, double expectedStd) {
		 int numerOfIndividuls = 100000;
		// make uniform
		ruleSpecies.isGenomeUniform = uniform;
		
		// Get a DescriptiveStatistics instance
		DescriptiveStatistics stats = new DescriptiveStatistics();

		// Make several individuals
		for( int i = 0; i < numerOfIndividuls; i++) {
			// Add their lambda values to the statistucs object.
			stats.addValue(((Rule)ruleSpecies.newIndividual(state, 0)).getLambda());
		}

		// Compute some statistics
		double actualMean = stats.getMean();
		double actualStd = stats.getStandardDeviation();
		
//		System.out.println("NDB testUniformGenomes()~expectedMean="+expectedMean);
//		System.out.println("NDB testUniformGenomes()~actualMean="+actualMean);
//		System.out.println("NDB testUniformGenomes()~expectedStd="+expectedStd);
//		System.out.println("NDB testUniformGenomes()~actualStd="+actualStd);
		
		Assert.assertEquals(expectedMean, actualMean, 0.01);
		Assert.assertEquals(expectedStd, actualStd, 0.01);
		
	}
	
}

