package ie.nix.mp.ecj;

import ie.nix.util.ToString;

import java.util.Arrays;

import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestCase;
import ec.EvolutionState;
import ec.Evolve;
import ec.util.ParameterDatabase;

public class InitialConfigurationTest  extends TestCase {

	public String PARAMS_FILE =  "src/test/java/ie/nix/mp/ecj/test.params";
	
    protected EvolutionState state;
    protected ParameterDatabase parameters;
    protected int numberOfAutomata = 9;
    
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		parameters = Evolve.loadParameterDatabase(new String[]{"-file", PARAMS_FILE});
		state = Evolve.initialize(parameters, 0);  
		
	}

	// static boolean getMajority(boolean[] automata) 
	@Test
	public void testGetMajorityAllTrue() {
		boolean[] automata = {
			true, true, true,	
		};
		boolean expectedMajority = true;
		
		testGetMajority(automata, expectedMajority);
		
	}
	@Test
	public void testGetMajoritySomeFalse() {
		boolean[] automata = {
			true, true, false,	
		};
		boolean expectedMajority = true;
		
		testGetMajority(automata, expectedMajority);
		
	}
	@Test
	public void testGetMajorityAllFalse() {
		boolean[] automata = {
			false, false, false,	
		};
		boolean expectedMajority = false;
		
		testGetMajority(automata, expectedMajority);
		
	}

	@Test
	public void testGetMajorityErrorEven() {
		boolean[] automata = {
			false, true,	
		};
		boolean expectedMajority = false;
		boolean expectedRuntimeExcepton = true;
		
		testGetMajority(automata, expectedMajority, expectedRuntimeExcepton);
		
	}
	
	public void testGetMajority(boolean[] automata, boolean expectedMajority) {
		testGetMajority(automata, expectedMajority, false);
	}
	
	public void testGetMajority(boolean[] automata, boolean expectedMajority, boolean expectedRuntimeExcepton) {
		
		try {
			boolean actualMajority = InitialConfiguration.getMajority(automata);	
			Assert.assertFalse(expectedRuntimeExcepton);
			Assert.assertEquals(expectedMajority, actualMajority);
			
		} catch (RuntimeException ex) {
			Assert.assertTrue(expectedRuntimeExcepton);
		}
		
		
	}
	
	// static double getLambda(boolean[] automata) 
	
	@Test
	public void testGetLambdaAllTrue() {
		boolean[] automata = {
			true, true, true,	
		};
		double expectedLambda = 1.0;
		
		testGetLambda(automata, expectedLambda);
		
	}
	@Test
	public void testGetLambdaSomeFalse() {
		boolean[] automata = {
			true, true, false,	
		};
		double expectedLambda = 2.0/3.0;
		
		testGetLambda(automata, expectedLambda);
		
	}
	@Test
	public void testGetLambdaAllFalse() {
		boolean[] automata = {
			false, false, false,	
		};
		double expectedLambda = 0.0;
		
		testGetLambda(automata, expectedLambda);
		
	}
	
	public void testGetLambda(boolean[] automata, double expectedLambda) {
		double actualLambda = InitialConfiguration.getLambda(automata);	
		Assert.assertEquals(expectedLambda, actualLambda);

	}
	
//	public Test(EvolutionState state)

	@Test
	public void testRandomTest() {
		boolean[] expectedInitialConditions = new boolean[]
				{true,true,true,true,true,true,true,true,false};
		testRandomTest(expectedInitialConditions);
	}
	
	public void testRandomTest(boolean[] expectedInitialConditions) {

		state.random[0].setSeed(0);
		InitialConfiguration test = new InitialConfiguration(state, numberOfAutomata);	
		boolean[] actualInitialConditions = test.getTest();
		
//		System.out.println("NDB::testRandomTest()~expectedInitialConditions="
//				+ToString.toString(expectedInitialConditions));
//		System.out.println("NDB::testRandomTest()~actualInitialConditions=  "
//				+ToString.toString(actualInitialConditions));
		
		Assert.assertTrue(Arrays.equals(expectedInitialConditions, actualInitialConditions));

	}
	
//	public Test(EvolutionState state, double lambda)

	@Test
	public void testLambdaTest0p0() {
		double lambda = 0.0;
		testLambdaTest(lambda);
	}
	
	@Test
	public void testLambdaTest1p0() {
		double lambda = 1.0;
		testLambdaTest(lambda);
	}
	
	@Test
	public void testLambdaTest0p5() {
		double lambda = 0.5;
		testLambdaTest(lambda);
	}
	
	@Test
	public void testLambdaTest1() {
		double lambda = 1.0/numberOfAutomata;
		testLambdaTest(lambda);
	}	
	
	@Test
	public void testLambdaTestLess1() {
		double lambda = (numberOfAutomata - 1.0)/numberOfAutomata;
		testLambdaTest(lambda);
	}
	
	public void testLambdaTest(double lambda) {
		InitialConfiguration test = new InitialConfiguration(state, numberOfAutomata, lambda);	
		double actualLambda = InitialConfiguration.getLambda(test.getTest());
		
//		System.out.println("NDB::testLambdaTest()~lambda="+lambda);
//		System.out.println("NDB::testLambdaTest()~actualLambda=  "+actualLambda);
		
		Assert.assertEquals(lambda, actualLambda, 1.0/numberOfAutomata);

	}
	
//	public Test(EvolutionState state, boolean majority)
	@Test
	public void testMajorityTestTrue() {
		boolean majority = true;
		testMajorityTest(majority);
	}
	
	@Test
	public void testMajorityTestFalse() {
		boolean majority = false;
		testMajorityTest(majority);
	}
	
	public void testMajorityTest(boolean majority) {
		InitialConfiguration test = new InitialConfiguration(state, numberOfAutomata, majority);	
		boolean actualMajority = InitialConfiguration.getMajority(test.getTest());
		
//		System.out.println("NDB::testMajorityTest()~majority="+majority);
//		System.out.println("NDB::testMajorityTest()~actualMajority=  "+actualMajority);
		
		Assert.assertEquals(majority, actualMajority);

	}
	
	public static String toString(InitialConfiguration test) {	

		StringBuilder string = new StringBuilder("new ie.nix.mp.ecj.Test(new boolean[]");
		string.append(ToString.toString(test.getTest()));
		string.replace(string.length()-2, string.length(),"})");
		return string.toString();
		
	}
}

