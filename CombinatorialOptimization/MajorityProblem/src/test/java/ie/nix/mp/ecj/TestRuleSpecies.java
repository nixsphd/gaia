package ie.nix.mp.ecj;

import java.util.Arrays;

import ec.EvolutionState;
import ec.Individual;
import ec.Subpopulation;
import ec.util.Parameter;
import ec.vector.BitVectorSpecies;

public class TestRuleSpecies extends BitVectorSpecies {

	private static final long serialVersionUID = -1763623783209270922L;
    
	public Individual newIndividual(final EvolutionState state, int thread) {
		TestRule rule = (TestRule)super.newIndividual(state, thread);
		Arrays.fill(rule.genome, false);
//		System.out.println("NDB TestRuleSpecies().newIndividual()~rule="+rule);
	    return rule;
	    
    }
	
}

