package ie.nix.mp.ecj;

import ie.nix.mp.rules.GKLRule;

import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestCase;
import ec.EvolutionState;
import ec.Evolve;
import ec.util.ParameterDatabase;

public class MajorityProblemTest  extends TestCase {
	
	protected static Rule gKLRule;	
	protected static Rule offRule;
	protected static Rule onRule;

	public String PARAMS_FILE =  "src/test/java/ie/nix/mp/ecj/rule.params";
	
    EvolutionState state;
    ParameterDatabase parameters;
    MajorityProblem majorityProblem;
    Rule rule;
    
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		parameters = Evolve.loadParameterDatabase(new String[]{"-file", PARAMS_FILE});
		state = Evolve.initialize(parameters, 0);  
		state.startFresh();
		
		// Wire up the Majority Problem
		majorityProblem = (MajorityProblem)state.evaluator.p_problem;
		// Wire up the Rule
		rule = (Rule)state.population.subpops[0].individuals[0];
		
	}
	
	public static Rule getGKLRule() {
		if (gKLRule == null) {
			gKLRule = new Rule(RuleTest.convert(GKLRule.GKL_POLICY));
		}
		return gKLRule;
	}
	
	public static Rule getOFFRule() {
		if (offRule == null) {
			int[] genome  = {
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
					0,0,0,0,0,0,0,0, 
				};
			offRule = new Rule(RuleTest.convert(genome));
		}
		return offRule;
	}
	
	public static Rule getONRule() {
		if (onRule == null) {
			int[] genome  = {
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
					1,1,1,1,1,1,1,1,
				};
			onRule = new Rule(RuleTest.convert(genome));
		}
		return onRule;
	}
	
	public static InitialConfiguration getMajorityOffInitialConfiguration() {
		int[] initialConditions = new int[] {
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 
				0,0,0,0,0
		};
		return new InitialConfiguration(RuleTest.convert(initialConditions));
		
	}
	
	public static InitialConfiguration getMajorityOnInitialConfiguration() {
		int[] initialConditions = new int[] {
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
				1,1,1,1,1
		};
		return new InitialConfiguration(RuleTest.convert(initialConditions));
		
	}
	
	@Test
	public void testGetFitness0p5() {
		Rule rule = getOFFRule();
		InitialConfiguration[] initialConfigurations = {
				getMajorityOnInitialConfiguration(),
				getMajorityOnInitialConfiguration(),
				getMajorityOffInitialConfiguration(),
				getMajorityOffInitialConfiguration(),
		};
		double expectedFitness = 0.5;
		
		testGetFitness(rule, initialConfigurations, expectedFitness);
		
	}
	
	@Test
	public void testGetFitness1p0() {
		Rule rule = getOFFRule();
		InitialConfiguration[] initialConfigurations = {
				getMajorityOnInitialConfiguration(),
				getMajorityOnInitialConfiguration(),
				getMajorityOnInitialConfiguration(),
				getMajorityOnInitialConfiguration(),
		};
		double expectedFitness = 0.0;
		
		testGetFitness(rule, initialConfigurations, expectedFitness);
		
	}
	
	@Test
	public void testGetFitness0p0() {
		Rule rule = getOFFRule();
		InitialConfiguration[] initialConfigurations = {
				getMajorityOffInitialConfiguration(),
				getMajorityOffInitialConfiguration(),
				getMajorityOffInitialConfiguration(),
				getMajorityOffInitialConfiguration(),
		};
		double expectedFitness = 1.0;
		
		testGetFitness(rule, initialConfigurations, expectedFitness);
		
	}
	
	public void testGetFitness(Rule rule, InitialConfiguration[] initialConfigurations, double expectedFitness) {

		TestSet testSet = new TestSet(initialConfigurations);
		
		double actualFitness = MajorityProblem.getFitness(rule, testSet, 320);
//		System.out.println("NDB testGetFitness()~expectedFitness="+expectedFitness);
//		System.out.println("NDB testGetFitness()~actualFitness="+actualFitness);
		
		Assert.assertEquals(expectedFitness, actualFitness, 0.0);
	}
	
	
	@Test
	public void testEvaluate1p0() {
		Rule rule = getONRule();
		InitialConfiguration[] initialConfigurations = {
				getMajorityOnInitialConfiguration(),
				getMajorityOnInitialConfiguration(),
				getMajorityOnInitialConfiguration(),
				getMajorityOnInitialConfiguration(),
		};
		double expectedFitness = 1.0;
		
		testEvaluate(rule.genome, initialConfigurations, expectedFitness);
		
	}
	
	@Test
	public void testEvaluate0p5() {
		Rule rule = getOFFRule();
		InitialConfiguration[] initialConfigurations = {
				getMajorityOnInitialConfiguration(),
				getMajorityOnInitialConfiguration(),
				getMajorityOffInitialConfiguration(),
				getMajorityOffInitialConfiguration(),
		};
		double expectedFitness = 0.5;
		
		testEvaluate(rule.genome, initialConfigurations, expectedFitness);
		
	}

	public void testEvaluate(boolean[] ruleGenome, InitialConfiguration[] initialConfigurations, double expectedFitness) {

		rule.reset(ruleGenome);
		
		TestSet testSet = new TestSet(initialConfigurations);
		majorityProblem.setTestSet(testSet);
		
		majorityProblem.evaluate(state, rule, 0, 1);
		Assert.assertTrue(rule.evaluated);
	
		double actualFitness = rule.fitness.fitness();
//		System.out.println("NDB testGetFitness()~expectedFitness="+expectedFitness);
//		System.out.println("NDB testGetFitness()~actualFitness="+actualFitness);
		
		Assert.assertEquals(expectedFitness, actualFitness);
	}
			
}

