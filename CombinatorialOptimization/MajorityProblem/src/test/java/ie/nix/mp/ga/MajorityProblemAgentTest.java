package ie.nix.mp.ga;

import ie.nix.ea.populations.PopulationOfIntegers;
import ie.nix.mp.MajorityProblemExperiment;
import ie.nix.mp.ea.GeneticAlgorithmExperiment.GeneticAlgorithmDeterministicRule;
import ie.nix.mp.ea.Mitchell;
import ie.nix.mp.ea.TowardsUnbiased;
import ie.nix.util.Equals;
import ie.nix.util.Randomness;
import ie.nix.util.ToString;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.junit.Test;

import junit.framework.Assert;
import junit.framework.TestCase;

@SuppressWarnings("unused")
public class MajorityProblemAgentTest extends TestCase {
	
	@Test
	public void testGetAction1() {

		Integer[][] preCannedPopulation = {{
				1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		}};
		int[] neighbourhoodState = {0,0,0,0,0,0,0};
		int expectedAction = 1;
		
		testGetAction(preCannedPopulation, neighbourhoodState, expectedAction);
		
	}
	
	@Test
	public void testGetAction2() {
		
		Integer[][] preCannedPopulation = {{
				0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		}};
		int[] neighbourhoodState = {0,0,0,0,0,0,1};
		int expectedAction = 1;
		
		testGetAction(preCannedPopulation, neighbourhoodState, expectedAction);
		
	}
	
	@Test
	public void testGetAction12() {

		Integer[][] preCannedPopulation = {{
				0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
		}};
		int[] neighbourhoodState = {0,0,0,1,1,0,0};
		int expectedAction = 1;
		
		testGetAction(preCannedPopulation, neighbourhoodState, expectedAction);
		
	}
	
	@Test
	public void testGetAction128() {
		
		Integer[][] preCannedPopulation = {{
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,
		}};
		int[] neighbourhoodState = {1,1,1,1,1,1,1};
		int expectedAction = 1;
		
		testGetAction(preCannedPopulation, neighbourhoodState, expectedAction);
		
	}

	protected void testGetAction(Integer[][] preCannedPopulation, int[] neighbourhoodState, int expectedAction) {
		
		GeneticAlgorithmDeterministicRule agent = new GeneticAlgorithmDeterministicRule();
		
		PopulationOfIntegers population = 
				new PopulationOfIntegers(preCannedPopulation[0].length);
		population.initGenotypes(preCannedPopulation);
		agent.decode(population.getGenotypes().get(0));
		
		int actualAction = agent.getAction(neighbourhoodState);
		
		Assert.assertEquals(expectedAction, actualAction);
	}
	
	
	@Test
	public void testGenerateRandomUniformExperiment() {
		int numberOfAutomota = 149;
		int numberOfTests = 10000;
		double expectedMean = 0.5; 
		double expectedSD = 0.3; // This seems and odd value. Really is hould find at test to see if a sample set is uniform?
		testGenerateRandomUniformExperiment(numberOfAutomota, numberOfTests, expectedMean, expectedSD);
		
	}

	public void testGenerateRandomUniformExperiment(int numberOfAutomota, int numberOfTests, 
			double expectedMean, double expectedSD) { // double[] expectedLambdas) {
		
		Randomness.setSeed(0);
		int[][] actualTests  = 
				Mitchell.generateUniformTests(numberOfTests, numberOfAutomota);
//		System.out.println("actualTests="+ToString.toString(actualTests));
		
		SummaryStatistics stats = new SummaryStatistics();
		for (int t = 0; t < numberOfTests; t++ ) {
			double actualLambda = MajorityProblemExperiment.getLambda(actualTests[t]);
			stats.addValue(actualLambda);
		}
		double actualMean = stats.getMean();
		double actualSD = stats.getStandardDeviation();
		
//		System.out.println("expectedMean="+expectedMean);
//		System.out.println("actualMean="+actualMean);
//		System.out.println("expectedSD="+expectedSD);
//		System.out.println("actualSD="+actualSD);
		Assert.assertEquals(expectedMean, actualMean, 0.01);
		Assert.assertEquals(expectedSD, actualSD, 0.05);
	}
	
//	@Test
//	public void testGenerateRandomDecreasingSigmaExperiment() {
//		int numberOfTests = 100;
//		int numberOfAutomota = 149;
//		double sigma = 0.4;
//		
//		testGenerateRandomAutomotaWithSigma(numberOfTests, numberOfAutomota, sigma);
//		
//	}
//
//	public void testGenerateRandomAutomotaWithSigma(int numberOfTests, int numberOfAutomota, double sigma) {
//		
//		Randomness.setSeed(0);
//		SummaryStatistics stats = new SummaryStatistics();
//		int maxSigmaDelta = ;
//	
//		int[][] actualTests = new int[numberOfTests][numberOfAutomota];
//		for (int t = 0; t < numberOfTests; t++ ) {
//			Nix.generateRandomAutomotaWithSigma(sigma, actualTests[t]);
//			stats.addValue(MajorityProblemExperiment.getLambda(actualTests[t]));
//			
//		}
//
//		double actualMean = stats.getMean();
//		double actualSigma = stats.getStandardDeviation();
//		Assert.assertTrue(Equals.equals(0.5, actualMean, 0.0000001));
//		Assert.assertTrue(Equals.equals(sigma, actualSigma, maxSigmaDelta));
//		
////		System.out.println("actualTests["+t+"]getLambda()="+lambda);
////		Assert.assertTrue(Equals.equals(expectedLambdas[t], lambda, 0.1));
//	}
	
	@Test
	public void testGenerateRandomAutomotaWithSigma1p0() {
		double sigma = 1.0;
		int numberOfAutomota = 149;
		double expectedMean = 0.5;
		double maxSigmaDelta = 0.8; // this breaks down due to being clapmet between 0.0 and 1.0
		double maxMeanDelta = 0.01;
		int sizeOfTest = 10000;
		
		testGenerateLambdaForSigma(numberOfAutomota, sizeOfTest, sigma, maxSigmaDelta, expectedMean, maxMeanDelta, false);
		
	}
	
	@Test
	public void testGenerateRandomAutomotaWithSigma0p5() {
		double sigma = 0.5;
		int numberOfAutomota = 149;
		double expectedMean = 0.5;
		double maxSigmaDelta = 0.25;
		double maxMeanDelta = 0.01;
		int sizeOfTest = 10000;
		
		testGenerateLambdaForSigma(numberOfAutomota, sizeOfTest, sigma, maxSigmaDelta, expectedMean, maxMeanDelta, false);
		
	}
	
	@Test
	public void testGenerateRandomAutomotaWithSigma0p1() {
		double sigma = 0.1;
		int numberOfAutomota = 149;
		double expectedMean = 0.5;
		double maxSigmaDelta = 0.05;
		double maxMeanDelta = 0.005;
		int sizeOfTest = 1000;
		
		testGenerateLambdaForSigma(numberOfAutomota, sizeOfTest, sigma, maxSigmaDelta, expectedMean, maxMeanDelta, false);
		
	}
	
	@Test
	public void testGenerateRandomAutomotaWithSigma0p05() {
		double sigma = 0.05;
		int numberOfAutomota = 149;
		double expectedMean = 0.5;
		double maxSigmaDelta = 0.001;
		double maxMeanDelta = 0.01;
		int sizeOfTest = 1000;
		
		testGenerateLambdaForSigma(numberOfAutomota, sizeOfTest, sigma, maxSigmaDelta, expectedMean, maxMeanDelta, false);
		
	}
	
	@Test
	public void testGenerateRandomAutomotaWithSigma0p01() {
		int numberOfAutomata = 149;
		int numberOfTests = 1000;
		double sigma = 0.01;
		double maxSigmaDelta = 0.005;
		double expectedMean = 0.5;
		double maxMeanDelta = 0.01;
		
		testGenerateLambdaForSigma(numberOfAutomata, numberOfTests, sigma, maxSigmaDelta, expectedMean, maxMeanDelta, false);
		
	}
	
	@Test
	public void testGenerateRandomAutomotaWithSigma0p01Balanced() {
		int numberOfAutomata = 149;
		int numberOfTests = 1000;
		double sigma = 0.01;
		double maxSigmaDelta = 0.005;
		double expectedMean = 0.5;
		double maxMeanDelta = 0.01;
		
		testGenerateLambdaForSigma(numberOfAutomata, numberOfTests, sigma, maxSigmaDelta, expectedMean, maxMeanDelta, true);
		
	}

	protected void testGenerateLambdaForSigma(int numberOfAutomata, int numberOfTests, double sigma, 
		double maxSigmaDelta, double expectedMean, double maxMeanDelta, boolean balanced) {
		
		Randomness.setSeed(0);
		SummaryStatistics stats = new SummaryStatistics();
		int[] numbersOfAutomata = new int[]{numberOfAutomata};
		double[] sigmas = {sigma};
		
		int[][] actualTests = TowardsUnbiased.generateRandomTests(numberOfTests, numbersOfAutomata, sigmas, balanced);
		
		int majorityOn = 0;
		int majorityOff = 0;
		
		for (int t = 0; t < numberOfTests; t++) {
			double lambda = MajorityProblemExperiment.getLambda(actualTests[t]);
			stats.addValue(lambda);
			if (lambda > 0.5) {
				majorityOn++;
			} else {
				majorityOff++;
			}
		}
		
		double actualMean = stats.getMean();
		double actualSigma = stats.getStandardDeviation();
//		System.out.println("actualMean="+actualMean);
//		System.out.println("expectedMean="+expectedMean);
//		System.out.println("actualSigma="+actualSigma);
//		System.out.println("sigma="+sigma);
		Assert.assertTrue(Equals.equals(expectedMean, actualMean, maxMeanDelta));
		Assert.assertTrue(Equals.equals(sigma, actualSigma, maxSigmaDelta));

		if (balanced) {
//			System.out.println("majorityOn="+majorityOn);
//			System.out.println("majorityOff="+majorityOff);
			Assert.assertTrue((majorityOn == majorityOff));
		}
		
	}

		
}
