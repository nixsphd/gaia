package ie.nix.mp.ecj;

import java.util.stream.*;
import ie.nix.util.ToString;
import ec.EvolutionState;

@SuppressWarnings("serial")
public class TestRule extends Rule {
	
	public static int NEXT_ID = 0; 
	public int id;
	public TestRule parent;

	public TestRule() {
		super();
    	this.id = NEXT_ID++;
//		System.out.println("NDB new "+this);
	}
	
	public void reset(boolean[] genome) {
		super.reset(genome);
		System.out.println("NDB TestRule.reset("+id+").reset()");
		
	}
    
    public void reset(EvolutionState state, int thread, double lambda) {
		super.reset(state, thread, lambda);
		System.out.println("NDB TestRule.reset("+id+").reset()");
    }
    
    public Object clone() {
    	TestRule clonedTestRule = (TestRule)super.clone();
    	clonedTestRule.id = NEXT_ID++;
    	//clonedTestRule.genome[clonedTestRule.id%clonedTestRule.genome.length] = true;
    	clonedTestRule.parent = this;
		return clonedTestRule;
    }
    
    public String toString() {
    	int genomeSum = -1;
    	if (genome != null) {
    		genomeSum = IntStream.of(RuleTest.convert(genome)).sum();
    	}
    	double fitnessValue = -1;
    	if (fitness != null) {
    		fitnessValue = fitness.fitness();
    	}
    	//return new String("TestRule("+id+")"+ToString.toString(Rule.convert(genome))+"");
    	if (parent != null) {
    		return new String("TestRule("+id+","+fitnessValue+","+genomeSum+")<="+parent.toString());
    	} else {
        	return new String("TestRule("+id+","+fitnessValue+","+genomeSum+")");
    	}
    }
}