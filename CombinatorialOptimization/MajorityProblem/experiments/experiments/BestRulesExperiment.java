package ie.nix.mp.ecj.experiments;

import java.util.stream.IntStream;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ie.nix.mp.ecj.ECJUtils;
import ie.nix.mp.ecj.InitialConfiguration;
import ie.nix.mp.ecj.MajorityProblem;
import ie.nix.mp.ecj.Rule;
import ie.nix.mp.ecj.TestSet;
import ie.nix.mp.rules.GKLRule;
import ie.nix.mp.rules.DasRule;
import ie.nix.mp.rules.DavisRule;
import ie.nix.mp.rules.BestKozaRule;
import ie.nix.mp.rules.BestMitchellRule;
import ie.nix.util.Experiment;
import ie.nix.util.ToString;

@SuppressWarnings("unused")
@JsonIgnoreProperties({ "numberOfTests"})
public class BestRulesExperiment extends Experiment {

//	public Rule bestMitchellRule;
	public Rule gKLRule;
	public Rule davisRule;
	public Rule dasRule;
	public Rule bestKozaRule;
	
	public int numberOfAutomota;
	
	public int numberOfTests;
	public String testSet10n6Filename;
	public String testSet10n7Filename;
	
	public int maxNumberOfSteps;
	
	private TestSet testSet10n6; // 10^6
	private TestSet testSet10n7; // 10^7
	
	// #Done in 392.82 secs, GKL fitness is 81.53% Davis fitness is 81.76% Das fitness is 82.14%
	// #Done in 414.07 secs, GKL fitness is 81.53% Davis fitness is 81.78% Das fitness is 82.36%
	// #Done in 592.28 secs, GKL fitness is 81.273% Davis fitness is 81.918% Das fitness is 82.318% Koza's best fitness is 82.229%

	public BestRulesExperiment() {
		super();
		
		numberOfAutomota = 149; // Comes from Kaza's Paper
		
		numberOfTests = 1000000;
		testSet10n6Filename = "TestSet10n6.json";
		testSet10n6 = (TestSet)Experiment.readFromJSON(Experiment.getExperimentDirectory()+"/"+testSet10n6Filename, TestSet.class);
		if (testSet10n6 == null) {
			// Make the TestSet
			testSet10n6 = new TestSet(ECJUtils.getEvolutionState(), numberOfTests, numberOfAutomota);
			Experiment.writeAsJSON(Experiment.getExperimentDirectory()+"/"+testSet10n6Filename, testSet10n6);
	
		}
		testSet10n7Filename = "TestSet10n7.json";

		numberOfTests = 1000000;
		testSet10n7 = (TestSet)Experiment.readFromJSON(Experiment.getExperimentDirectory()+"/"+testSet10n7Filename, TestSet.class);
		if (testSet10n7 == null) {
			// Make the TestSet
			TestSet.NUMBER_OF_TESTS = numberOfTests;
			testSet10n7 = new TestSet(ECJUtils.getEvolutionState());
			Experiment.writeAsJSON(Experiment.getExperimentDirectory()+"/"+testSet10n7Filename, testSet10n7);
	
		}
	    		
		maxNumberOfSteps = 320; // Comes from Mitchell's Paper

//		bestMitchellRule = Rule.createRule(BestMitchellRule.BEST_MITCHELL_POLICY);
		gKLRule = Rule.createRule(GKLRule.GKL_POLICY);
		davisRule = Rule.createRule(DavisRule.DAVIS_POLICY);
		dasRule = Rule.createRule(DasRule.DAS_POLICY);
		bestKozaRule = Rule.createRule(BestKozaRule.BEST_KOZA_POLICY);
	
	}
	
	public static void main(String[] args) {
		
		BestRulesExperiment experiment = (BestRulesExperiment)init(args, BestRulesExperiment.class);

		if (experiment != null) {
			
			MajorityProblem.MAX_NUMBER_OF_STEPS = experiment.maxNumberOfSteps;
			MajorityProblem majorityProblem = new MajorityProblem();

//			double bestMitchellFitness = majorityProblem.getFitness(experiment.bestMitchellRule, experiment.testSet10n6);
			double gKLFitness = majorityProblem.getFitness(experiment.gKLRule, experiment.testSet10n6);
			double davisFitness = majorityProblem.getFitness(experiment.davisRule, experiment.testSet10n6);
//			double dasFitness = majorityProblem.getFitness(experiment.dasRule, experiment.testSet10n7);
//			double bestKozaFitness = majorityProblem.getFitness(experiment.bestKozaRule, experiment.testSet10n7);
			
			stopWatch.stop();
		    System.out.println( "#Done in "+String.format("%.2f", (stopWatch.getTime()/1000.0))+" secs,\n\t"
//    				+ "Micthell's best fitness is "+String.format("%.1f", (bestMitchellFitness*100.0))+"%, compared to 76.9%\n\t"
		    		+ "GKL fitness is "+String.format("%.2f", (gKLFitness*100.0))+"%, compared to 81.6%\n\t"
		    		+ "Davis fitness is "+String.format("%.2f", (davisFitness*100.0))+"%, compared to 81.8%\n\t"
//		    		+ "Das fitness is "+String.format("%.3f", (dasFitness*100.0))+"%, compared to 82.178%\n\t"
//    				+ "Koza's best fitness is "+String.format("%.3f", (bestKozaFitness*100.0))+"%, compared to 82.526%"
		    		);
		    
		} else {
		    System.out.println( "#Done.");
		    
		}
	    
	}

}
