package ie.nix.mp.ecj;
 
import ec.EvolutionState;
import ie.nix.mp.rules.BestKozaRule;
import ie.nix.mp.rules.DasRule;
import ie.nix.mp.rules.DavisRule;
import ie.nix.mp.rules.GKLRule;
import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;

public class FitnessAgainstLambdaExperiment extends Experiment {
	
	class TestSet extends ie.nix.mp.ecj.TestSet {
		public TestSet(double lambdaStep, int numberOfTestsPerLambda) {
			for (double lambda = 0.0; lambda <= 1.0; lambda += lambdaStep) {
				InitialConfiguration[] tests = generateTestsWithLambda(ECJUtils.getEvolutionState(), lambda, numberOfTestsPerLambda);
					
			}
		}
	}
	
//	public Rule bestMitchellRule;
	public Rule gKLRule;
	public Rule davisRule;
	public Rule dasRule;
	public Rule bestKozaRule;
	
	public double lambdaStep; 
	public int numberOfTestsPerLambda;
	protected int numbersOfAutomota;
	
	// #Done in 392.82 secs, GKL fitness is 81.53% Davis fitness is 81.76% Das fitness is 82.14%
	
	public FitnessAgainstLambdaExperiment() {
		super();

		lambdaStep = 0.2; 
		numberOfTestsPerLambda = 100;
		numbersOfAutomota = 149;
//			= new int[] {
//				149//, 499, 999
//		};
		
//		bestMitchellRule = Rule.createRule(BestMitchellRule.BEST_MITCHELL_POLICY);
		gKLRule = Rule.createRule(GKLRule.GKL_POLICY);
		davisRule = Rule.createRule(DavisRule.DAVIS_POLICY);
		dasRule = Rule.createRule(DasRule.DAS_POLICY);
		bestKozaRule = Rule.createRule(BestKozaRule.BEST_KOZA_POLICY);
		
//		evaluationExperiment = new MajorityProblemExperiment();
//		evaluationExperiment.setNumberOfTests(numberOfTestsPerLambda);
		
	}

	protected DataFrame getFitnessVsRulesDataFrame() {
		String dataFrameFileName = "FitnessVsRules.csv";
		// Process the space-time diagrams filename
		DataFrame df = new DataFrame();
		df.setOutputDirectoryName(Experiment.getExperimentDirectory());
		df.setDoubleFormat("%.3f");
		df.initWriter(dataFrameFileName);
		df.addColumns(new String[]{"Lambda", "NumberOfAutomata", "Rule", "Fitness"});
		df.writeHeaders();
		return df;
	}

	public static void main(String[] args) {
		
		FitnessAgainstLambdaExperiment experiment = (FitnessAgainstLambdaExperiment)init(args, FitnessAgainstLambdaExperiment.class);
		if (experiment != null) {
			
			DataFrame df = experiment.getFitnessVsRulesDataFrame();
			for (double lambda = 0.0; lambda <= 1.0; lambda += experiment.lambdaStep) {

//				for (int numberOfAutomota: experiment.getNumbersOfAutomota()) {

					int[][] tests = MajorityProblemExperiment.generateTestsWithLambda(experiment.getNumberOfTestsPerLambdaPoint(), 
							numberOfAutomota, lambdaPoint);
					experiment.getEvaluationExperiment().setTests(tests);
					
					for (Rule rule: experiment.getRules()) {
						df.addRow(lambdaPoint, numberOfAutomota, rule.getClass().getSimpleName(), 
								MajorityProblemExperiment.getFitnessForRule(rule, experiment.getEvaluationExperiment()));
						df.writeLastRow();
					}
					
//				}
				
			}
			
			stopWatch.stop();
		    System.out.println( "#Done in "+String.format("%.2f", (stopWatch.getTime()/1000.0))+" sec");
		    
		} else {
		    System.out.println( "#Done.");
		    
		}
	    
	}

}
