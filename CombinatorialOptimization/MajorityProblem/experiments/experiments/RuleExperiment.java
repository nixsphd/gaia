package ie.nix.mp.ecj.experiments;

import java.util.stream.IntStream;

import ec.EvolutionState;
import ec.Evolve;
import ec.util.Parameter;
import ec.util.ParameterDatabase;
import ie.nix.mp.ecj.ECJUtils;
import ie.nix.mp.ecj.InitialConfiguration;
import ie.nix.mp.ecj.Rule;
import ie.nix.mp.ecj.RuleTest;
import ie.nix.mp.rules.GKLRule;
import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;
import ie.nix.util.ToString;

public class RuleExperiment extends Experiment {
	
	public Rule rule;
	public InitialConfiguration initialConfiguration;
	public int maxNumberOfSteps;

	public RuleExperiment() {
//		setRule(new Rule(new boolean[]{
//				true,  false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//			}));
		
		setRule(RuleTest.createRule(GKLRule.GKL_POLICY));

//		setInitialConfiguration(new InitialConfiguration(new boolean[] {
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
//				false, false, false, false, false, 
//		}));
		
		setInitialConfiguration(new InitialConfiguration(ECJUtils.getEvolutionState()));
		
		setMaxNumberOfSteps(320);
		
	}
	
	public Rule getRule() {
		return rule;
	}

	public InitialConfiguration getInitialConfiguration() {
		return initialConfiguration;
	}

	public void setInitialConfiguration(InitialConfiguration initialConfiguration) {
		this.initialConfiguration = initialConfiguration;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	public void setMaxNumberOfSteps(int maxNumberOfSteps) {
		this.maxNumberOfSteps = maxNumberOfSteps;
	}

	public int getMaxNumberOfSteps() {
		return maxNumberOfSteps;
	}


	public static void main(String[] args) {
		
		RuleExperiment experiment = (RuleExperiment)init(args, RuleExperiment.class);
		if (experiment != null) {
	
			Rule rule = experiment.getRule();
			InitialConfiguration ic = experiment.getInitialConfiguration();
			int maxNumberOfSteps = experiment.getMaxNumberOfSteps();
			
			boolean[][] spaceTimeData = new boolean[maxNumberOfSteps+1][ic.getTest().length];
			
			boolean[] result = rule.evaluate(ic, maxNumberOfSteps, spaceTimeData);
			
		    System.out.println( "result="+ToString.toString(result));
		    System.out.println( "spaceTimeData="+ToString.toString(spaceTimeData));
			
		    String outputFileName = "SpaceTime.csv";
		    DataFrame df = new DataFrame();
		    df.initWriter(outputFileName);
		    
		    Integer[] spaceTimeDataCol = new Integer[spaceTimeData.length*spaceTimeData[0].length];
		    IntStream.range(0, spaceTimeData.length).
	    		forEach(r -> {
				    IntStream.range(0, spaceTimeData[r].length).
				    	forEach(x -> {
				    		spaceTimeDataCol[(spaceTimeData[0].length * r) + x] = (spaceTimeData[r][x]? 1: 0);
						}
				    );
	    		}
	    	);
		    df.addColumn("Test0", spaceTimeDataCol);
		    
		    df.writeHeaders();
		    df.writeRows();
		    
			stopWatch.stop();
		    System.out.println( "#Done in "+(stopWatch.getTime()/1000.0)+" secs");
		    
		} else {
		    System.out.println( "#Done.");
		    
		}
	    
	}

}
