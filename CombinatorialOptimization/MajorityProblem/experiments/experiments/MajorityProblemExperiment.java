package ie.nix.mp.ecj.experiments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ie.nix.ea.operators.AverageEvaluation;
import ie.nix.ea.operators.BestWorstEvaluation;
import ie.nix.mp.rules.BestKozaRule;
import ie.nix.mp.rules.BestRulesExperiment;
import ie.nix.mp.rules.GKLRule;
import ie.nix.util.DataFrame;
import ie.nix.util.Experiment;
import ie.nix.util.Halo;
import ie.nix.util.Randomness;
import ie.nix.util.ToString;

import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.ArrayUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name="Experiment")
@JsonInclude(value=Include.NON_EMPTY, content=Include.NON_NULL)
public class MajorityProblemExperiment extends Experiment {

	public static void main(String[] args) {
		
		MajorityProblemExperiment experiment = (MajorityProblemExperiment)init(args, MajorityProblemExperiment.class);

		if (experiment != null) {
	
			// Test the experiment
			double fitness = experiment.runExperiment();
			stopWatch.stop();
		    System.out.println( "#Done in "+String.format("%.2f", (stopWatch.getTime()/1000.0))+" secs,"
		    		+ " Fitness is "+String.format("%.3f", (fitness*100.0))+"%");
		    
		} else {
		    System.out.println( "#Done.");
		    
		}
	    
	}

	public double runExperiment() {
	
		// Get the DataFranes ready for the logging.
		DataFrame stdf = getSpaceTimeDataFrame();

		// Test the experiment
		double fitness = MajorityProblemExperiment.getFitnessForRule(getRule(), this, stdf);

		if (stdf != null) {
			stdf.writeHeaders();
			stdf.writeRows();
		}
		
		return fitness;
	}
}
