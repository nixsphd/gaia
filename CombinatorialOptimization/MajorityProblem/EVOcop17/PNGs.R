# setwd("~/Desktop/Gaia/MajorityProblem/MajorityProblem/EVOcop17")

#script.dir <- dirname(sys.frame(1)$ofile)
#source(file.path(script.dir,"Functions.R"))

source("Functions.R")

mitchell <- readGAsForExperiment(dir = "Mitchell");
mitchell_1KA <- readGAsForExperiment(dir = "Mitchell-1KA");
towardsUnbiased <- readGAsForExperiment(dir = "TowardsUnbiased");
towardsUnbiased_1KA <- readGAsForExperiment(dir = "TowardsUnbiased-1KA");
towardsUnbiased_1KA6S <- readGAsForExperiment(dir = "TowardsUnbiased-1KA6S");
towardsUnbiased_1KA10S <- readGAsForExperiment(dir = "TowardsUnbiased-1KA10S");

mitchellsAvgs <- getAvgsForRuns(data=mitchell)
mitchells_1KAAvgs <- getAvgsForRuns(data=mitchell_1KA)
towardsUnbiasedAvgs <- getAvgsForRuns(data=towardsUnbiased)
towardsUnbiased_1KAAvgs <- getAvgsForRuns(data=towardsUnbiased_1KA)
towardsUnbiased_1KA6SAvgs <- getAvgsForRuns(data=towardsUnbiased_1KA6S)
towardsUnbiased_1KA10SAvgs <- getAvgsForRuns(data=towardsUnbiased_1KA10S)

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.25, 0.75)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=mitchellsAvgs, aes(x=Generation, y=value, colour="100 Agents")) +
    geom_point(data=mitchellsAvgs, aes(x=Generation, y=value, colour="100 Agents")) +
    geom_line(data=mitchells_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents")) +
    geom_point(data=mitchells_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents"))
ggsave("mitchell_100A_vs_1KA.png", width = 20, height = 12, units = "cm")

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.25, 0.75)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=towardsUnbiasedAvgs, aes(x=Generation, y=value, colour="100 Agents")) +
    geom_point(data=towardsUnbiasedAvgs, aes(x=Generation, y=value, colour="100 Agents")) +
    geom_line(data=towardsUnbiased_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents, 4 steps")) +
    geom_point(data=towardsUnbiased_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents, 4 steps")) +
    geom_line(data=towardsUnbiased_1KA6SAvgs, aes(x=Generation, y=value, colour="1,000 Agents, 6 Steps")) +
    geom_point(data=towardsUnbiased_1KA6SAvgs, aes(x=Generation, y=value, colour="1,000 Agents, 6 Steps")) +
    geom_line(data=towardsUnbiased_1KA10SAvgs, aes(x=Generation, y=value, colour="1,000 Agents, 10 Steps")) +
    geom_point(data=towardsUnbiased_1KA10SAvgs, aes(x=Generation, y=value, colour="1,000 Agents, 10 Steps"))
ggsave("towardsUnbiased_100A_vs_1KA.png", width = 20, height = 12, units = "cm")

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.25, 0.75)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=mitchells_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents")) +
    geom_point(data=mitchells_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents")) +
    geom_line(data=towardsUnbiased_1KAAvgs, aes(x=Generation, y=value,
        colour="1,000 Agents (Towards Unbiased)")) +
    geom_point(data=towardsUnbiased_1KAAvgs, aes(x=Generation, y=value,
        colour="1,000 Agents (Towards Unbiased)"))
ggsave("mitchell_1KA_vs_towardsUnbiased_1KA.png", width = 20, height = 12, units = "cm")

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.25, 0.75)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=mitchells_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents")) +
    geom_point(data=mitchells_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents")) +
    geom_line(data=towardsUnbiased_1KA6SAvgs, aes(x=Generation, y=value, colour="1,000 Agents (Towards Unbiased in 6 Steps)")) +
    geom_point(data=towardsUnbiased_1KA6SAvgs, aes(x=Generation, y=value, colour="1,000 Agents (Towards Unbiased in 6 Steps)"))
ggsave("mitchell_1KA_vs_towardsUnbiased_1KA6S.png", width = 20, height = 12, units = "cm")

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.25, 0.75)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=mitchells_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents")) +
    geom_point(data=mitchells_1KAAvgs, aes(x=Generation, y=value, colour="1,000 Agents")) +
    geom_line(data=towardsUnbiased_1KA10SAvgs, aes(x=Generation, y=value, colour="1,000 Agents (Towards Unbiased in 10 Steps)")) +
    geom_point(data=towardsUnbiased_1KA10SAvgs, aes(x=Generation, y=value, colour="1,000 Agents (Towards Unbiased in 10 Steps)"))
ggsave("mitchell_1KA_vs_towardsUnbiased_1KA10S.png", width = 20, height = 12, units = "cm")

mitchellsFitnessEvalAvgs <- getFitnessEvalAvgsForRuns(data=mitchell)
mitchells_1KAFitnessEvalAvgs <- getFitnessEvalAvgsForRuns(data=mitchell_1KA)
towardsUnbiasedFitnessEvalAvgs <- getFitnessEvalAvgsForRuns(data=towardsUnbiased)
towardsUnbiased_1KAFitnessEvalAvgs <- getFitnessEvalAvgsForRuns(data=towardsUnbiased_1KA)
towardsUnbiased_1KA6SFitnessEvalAvgs <- getFitnessEvalAvgsForRuns(data=towardsUnbiased_1KA6S)
towardsUnbiased_1KA10SFitnessEvalAvgs <- getFitnessEvalAvgsForRuns(data=towardsUnbiased_1KA10S)

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.0, 1.0)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=mitchellsAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_point(data=mitchellsAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_line(data=mitchellsFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation")) +
    geom_point(data=mitchellsFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation"))
ggsave("mitchell_fitness_vs_best.png", width = 20, height = 12, units = "cm")

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.0, 1.0)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=towardsUnbiased_1KAAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_point(data=towardsUnbiased_1KAAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_line(data=towardsUnbiased_1KAFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation")) +
    geom_point(data=towardsUnbiased_1KAFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation"))
ggsave("towardsUnbiased_fitness_vs_best.png", width = 20, height = 12, units = "cm")

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.0, 1.0)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=towardsUnbiased_1KAAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_point(data=towardsUnbiased_1KAAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_line(data=towardsUnbiased_1KAFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation")) +
    geom_point(data=towardsUnbiased_1KAFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation"))
ggsave("towardsUnbiased_1KA_fitness_vs_best.png", width = 20, height = 12, units = "cm")
    
ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.0, 1.0)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=towardsUnbiased_1KA6SAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_point(data=towardsUnbiased_1KA6SAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_line(data=towardsUnbiased_1KA6SFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation")) +
    geom_point(data=towardsUnbiased_1KA6SFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation"))
ggsave("towardsUnbiased_1KA6S_fitness_vs_best.png", width = 20, height = 12, units = "cm")

ggplot() +
    labs(colour = "") + theme(legend.position = "top") +
    coord_cartesian(ylim = c(0.0, 1.0)) + xlab("Generation") + ylab("Accuracy") +
    geom_line(data=towardsUnbiased_1KA10SAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_point(data=towardsUnbiased_1KA10SAvgs, aes(x=Generation, y=value, colour="Unbiased Evaluation")) +
    geom_line(data=towardsUnbiased_1KA10SFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation")) +
    geom_point(data=towardsUnbiased_1KA10SFitnessEvalAvgs, aes(x=Generation, y=value, colour="Fitness Evaluation"))
ggsave("towardsUnbiased_1KA10S_fitness_vs_best.png", width = 20, height = 12, units = "cm")
