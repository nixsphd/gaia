# install.packages("reshape2")
# install.packages("ggplot2")
# install.packages("sp") 
library(reshape2)
library(ggplot2)
library(sp)
library(plyr)

plotGeneration <- function(run, genomesFileName="Genomes.csv", lengthOfGenome=128) {
    dev.off();
    genomes <- read.csv(genomesFileName, strip.white = TRUE)
    genomes.generation <- subset(genomes, select = run)
    colnames(genomes.generation)<-c("status")
    gt = GridTopology(cellcentre=c(0,0), cellsize=c(1,1), 
                      cells=c(lengthOfGenome,nrow(genomes)/lengthOfGenome))
    sgdf = SpatialGridDataFrame(gt, genomes.generation)
    image(sgdf, col=c("red", "black"))
    mtext(line=1, side=1, run , outer=F)
}

plotGenomes <- function(genomesFileName="Genomes.csv", lengthOfGenome=128) {
    genomes <- read.csv(genomesFileName, strip.white = TRUE)
    columnNames <- names(genomes)
    gt = GridTopology(cellcentre=c(0,0), cellsize=c(1,1), 
                      cells=c(lengthOfGenome,nrow(genomes)/lengthOfGenome))
    numberOfGeneration <- ncol(genomes)
    numberOfImageRows <- 2
    numberOfImageColumns <- ceiling(numberOfGeneration/numberOfImageRows)
    par(mfrow = c(numberOfImageRows,numberOfImageColumns))
    for (i in 1:numberOfGeneration) {
        run <- columnNames[i]
        genomes.generation <- subset(genomes, select = run)
        colnames(genomes.generation)<-c("status")
        sgdf = SpatialGridDataFrame(gt, genomes.generation)
        image(sgdf, col=c("white", "black"))
        mtext(line=1, side=1, run , outer=F)
    }
}

plotGA <- function(gaFileName="GA.csv") {
    data <- read.csv(gaFileName,strip.white = TRUE)
    meltedData <- melt(data, id=c("Generation"))
    qplot(Generation, value, 
          data=meltedData, 
          group=variable, color=variable,
          geom=c("point", "line"), ylim=c(0,1))
}

readGAForRuns <- function(dir=".", file="GA.csv", numberOfRuns=12) {
    fileName <- paste(dir, "/", sprintf("%02d", 1), "/", file, sep="")
    data <- read.csv(fileName, strip.white = TRUE)
    data$Run <- 1
    for (run in 2:numberOfRuns) {
        fileName <- paste(dir, "/", sprintf("%02d", run), "/", file, sep="")
        if (file.exists(fileName)) {
            runData <- read.csv(fileName, strip.white = TRUE)
            runData$Run <- run
            data <- rbind(data, runData)
        }
    }
    return(data)
}

getAvgsForRuns <- function(data=data) {
    meltedData <- melt(data[,c("Generation", "Run", "Best.Evaluation")], 
                       id=c("Generation", "Run"))
    avgOfRuns <- dcast(meltedData, Generation~variable, mean)
    meltedAvgs <- melt(avgOfRuns, id=c("Generation"))
}

getFitnessEvalAvgsForRuns <- function(data=data) {
    meltedData <- melt(data[,c("Generation", "Run", "Training.Best.Evaluation")], 
                       id=c("Generation", "Run"))
    avgOfRuns <- dcast(meltedData, Generation~variable, mean)
    meltedAvgs <- melt(avgOfRuns, id=c("Generation"))
}

plotGAForRuns <- function(data=data) {
    meltedData <- melt(data[,c("Generation", "Run", "Best.Evaluation", 
        "Training.Best.Evaluation")], id=c("Generation", "Run"))
    avgOfRuns <- dcast(meltedData, Generation~variable, mean)
    meltedAvgs <- melt(avgOfRuns, id=c("Generation"))
    qplot(Generation, value, 
          data=meltedAvgs, 
          group=variable, color=variable,
          geom=c("point", "line"), ylim=c(0,1),
          ylab = "Accuracy")
    # ggplot(data=meltedAvgs, aes(x=Generation, y=value, colour=supp)) + 
    #     geom_errorbar(aes(ymin=value-se, ymax=value+se), width=.1) +
    #     geom_line() +
    #     geom_point()
}

readPBS <- function(file) {
    data <- read.csv(file, strip.white = TRUE, 
                     col.names = c("Seed","Uniform","Unbiased"), 
                     comment.char = "#")
    sortData <- data[order(data$Seed),]
    return (sortData)
}
    
plotPBS <- function(data = data) {
    meltedData <- melt(data, id=c("Seed"))
    qplot(Seed, value, 
          data=meltedData, 
          group=variable, color=variable,
          geom=c("point"), ylim=c(0.45,1))
}
