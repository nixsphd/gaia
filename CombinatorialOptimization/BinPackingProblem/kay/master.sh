#!/bin/bash

echo "Experiment (Master $MASTER_IPADDRESS:$MASTER_PORT) $ECJ_CONFIG_FILE $RESULTS_DIR"

export COMMAND="java -cp $CLASSPATH ec.Evolve\
 -file $ECJ_CONFIG_FILE\
 -p seed.0=$SEED\
 -p checkpoint-directory=$RESULTS_DIR\
 -p stat.file=$RESULTS_DIR/master.stat\
 -p stat.child.0.file=$RESULTS_DIR/koza.stat\
 -p stat.child.1.file=$RESULTS_DIR/egc.stat\
 -p stat.child.2.file=$RESULTS_DIR/occam.stat\
 -p eval.masterproblem=ec.eval.MasterProblem\
 -p eval.masterproblem.max-jobs-per-slave=1\
 -p eval.master.host=$MASTER_IPADDRESS\
 -p eval.master.port=$MASTER_PORT\
 -p silent=true"

echo $COMMAND
$COMMAND