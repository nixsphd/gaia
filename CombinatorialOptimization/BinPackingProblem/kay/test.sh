#!/bin/bash

if [[ -z "$EXP" ]]; then
    echo "Must provide EXP in environment, for example export EXP=exp1" 1>&2
    exit 1
fi
if [[ -z "SEED" ]]; then
    echo "Must provide SEED, for example export SEED=0" 1>&2
    exit 1
fi

export GAIA_ROOT=/Users/nicolamcdonnell/Desktop/Gaia
export GAIA_ROOT=/ichec/home/users/nix/gaia
export PROJECT_ROOT=$GAIA_ROOT/CombinatorialOptimization/BinPackingProblem
export GC_PROJECT_ROOT=$GAIA_ROOT/GossipContracts
export UTIL_PROJECT_ROOT=$GAIA_ROOT/Util
export CLASSPATH=$PROJECT_ROOT/build/classes/main:$PROJECT_ROOT/build/resources/main:$PROJECT_ROOT/lib/*:$GC_PROJECT_ROOT/build/classes/main:$GC_PROJECT_ROOT/build/resources/main::$UTIL_PROJECT_ROOT/build/classes/main:$UTIL_PROJECT_ROOT/build/resources/main

export RESULTS_DIR=$PROJECT_ROOT/results/$EXP/$SEED

cd $PROJECT_ROOT
mkdir -p $RESULTS_DIR
cp $ECJ_CONFIG_FILE $RESULTS_DIR
cp $PEERSIM_CONFIG_FILE $RESULTS_DIR

echo "EXP=$EXP, SEED=$SEED"
echo "Experiment (Test) CLASSPATH $CLASSPATH"
echo "Experiment (Test) RESULTS_DIR $RESULTS_DIR"
echo "Experiment (Test) ECJ_CONFIG_FILE $ECJ_CONFIG_FILE"
echo "Experiment (Test) PEERSIM_CONFIG_FILE $PEERSIM_CONFIG_FILE"

java -cp $CLASSPATH ec.Evolve\
	-file $ECJ_CONFIG_FILE\
	-p seed.0=$SEED\
	-p checkpoint=false\
	-p stat.file=$RESULTS_DIR/master.stat\
	-p stat.child.0.file=$RESULTS_DIR/koza.stat\
	-p stat.child.1.file=$RESULTS_DIR/egc.stat\
	-p eval.problem.properties=$PEERSIM_CONFIG_FILE\
  -p generations=1\
	-p pop.subpop.0.size=1\
	-p gp.problem.number_of_runs=5\
	-p gp.problem.logging=console\
  -p silent=false