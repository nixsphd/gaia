
export GAIA_ROOT=/Users/nicolamcdonnell/Desktop/Gaia
export GAIA_ROOT=/ichec/home/users/nix/gaia
export PROJECT_ROOT=$GAIA_ROOT/CombinatorialOptimization/BinPackingProblem
export ECJ_CONFIG_FILE=$PROJECT_ROOT/src/main/resources/ie/nix/bpp/exp/$EXP.params
export PEERSIM_CONFIG_FILE=$PROJECT_ROOT/src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties

if [[ -z "$EXP" ]]; then
    echo "Must provide EXP in environment, for example export EXP=exp1" 1>&2
    exit 1
fi

if test -f "$ECJ_CONFIG_FILE"; then
    echo "$ECJ_CONFIG_FILE exists."
else
    echo "$ECJ_CONFIG_FILE does not exist for $EXP. Did tou set $EXP correctly?"
    exit 1
fi

if test -f "$PEERSIM_CONFIG_FILE"; then
    echo "$PEERSIM_CONFIG_FILE exists."
else
    echo "$PEERSIM_CONFIG_FILE does not exist."
    exit 1
fi


for seed in {0..19}
do
	export SEED=$seed
	echo "sbatch --job-name=$EXP.$SEED --output=$EXP.$SEED.out $PROJECT_ROOT/kay/egc-prodq.sbatch"
	sbatch --job-name=$EXP.$SEED --output=$EXP.$SEED.out $PROJECT_ROOT/kay/egc-prodq.sbatch
done
