package ie.nix.bpp

import ie.nix.bpp.dataset.DataSet.{loadQueuesFromFile, saveQueuesToFile}
import ie.nix.bpp.dataset.{DataSet, QueueGenerator}
import ie.nix.bpp.item.{Item1D, Item2D}
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.size.{Size1D, Size2D}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterEach, PrivateMethodTester}
import org.scalatestplus.junit.JUnitRunner

import java.nio.file.{FileSystems, Files}
import scala.util.Random

@RunWith(classOf[JUnitRunner])
class DataSetTest extends AnyFlatSpec with BeforeAndAfterEach with PrivateMethodTester with Logging {

  implicit val sizeLike1D: Size[Size1D] = SizeLikeSize1D
  implicit val sizeLike2D: Size[Size2D] = SizeLikeSize2D
  implicit val toItem1D: String => Item1D = Item1D.toItem[Size1D]
  implicit val toItem2D: String => Item2D = Item2D.toItem[Size2D]

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "DataSet"

  it should "loadQueuesFromFile" in {}

  it should "create DataSet" in {
    val bpp = DataSet[Size1D, Item1D](numberOfRuns, numberOfItems)(sizeLike1D, queueGenerator)

    assert(bpp.numberOfQueues === numberOfRuns)
    assert(bpp.numberOfItems === numberOfItems)
    assert(bpp.itemsForQueue(0).head.size === queueGenerator.generateItems.head.size)
  }

  val TMP_DIR = "/tmp"

  it should "saveQueues & loadQueues" in {

    val fileName = "Test-saveQueues.csv"
    saveQueuesToFile[Size1D, Item1D](bpp, TMP_DIR, fileName)

    val path = FileSystems.getDefault.getPath(s"$TMP_DIR/$fileName")
    assert(Files.exists(path))

    val actualQueues: Vector[Vector[Item1D]] = loadQueuesFromFile[Size1D, Item1D](TMP_DIR, fileName)

    assert(actualQueues.length === numberOfRuns)
    assert(actualQueues(0).length === numberOfItems)
    assert(actualQueues(0)(0).size === queueGenerator.generateItems.head.size)

  }

  def numberOfItems: Int = 10
  def numberOfRuns: Int = 2
  def queueGenerator: QueueGenerator[Size1D, Item1D] = new QueueGenerator[Size1D, Item1D] {
    override def generateItems: Vector[Item1D] = Vector(Item1D(5))
  }
  def bpp: DataSet[Size1D, Item1D] = DataSet[Size1D, Item1D](numberOfRuns, numberOfItems)(sizeLike1D, queueGenerator)

}
