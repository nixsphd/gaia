import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import seaborn as sns
from scipy.stats import shapiro, wilcoxon, iqr

def model_to_df(results_dir, dataset_name, model, observer_file='BinObserver'): 
    model_dir = f"{results_dir}/{dataset_name}/{model}"
    observer_data = f"{model_dir}/{observer_file}.csv"
    df = pd.read_csv(observer_data)
    return (df)

def model_data_list(results_dir, dataset_name, models, observer_file='BinObserver'):
    for model in models:
        df = model_to_df(results_dir, dataset_name, model)
        yield df['Open Bins']    

def model_data(results_dir, dataset_name, models, models_names, observer_file='BinObserver'):
    df = pd.DataFrame()
    for (model, models_name) in zip(models, models_names):
        model_df = model_to_df(results_dir, dataset_name, model)
        model_df['Model'] = models_name
        df = df.append(model_df, ignore_index=True)
    return(df)

def figure_models(results_dir, dataset, dataset_name, models, models_names, observer_file='BinObserver', figure_name='OpenBins'):
    df = model_data(results_dir, dataset, models, models_names, observer_file)
    fig, axes = plt.subplots()
    fig.set_size_inches(5, 4)
    axes.set_title(f"{dataset_name}")
    axes.yaxis.set_major_locator(MaxNLocator(integer=True))
    sns.set_theme(style="whitegrid")
    box_plot = sns.boxplot(y="Open Bins", x="Model", data=df)
    box_plot.set(ylabel='Open Bins')
    plt.savefig(f'{results_dir}/{dataset}/{figure_name}.eps', format='eps')
    
def table_egc(results_dir, dataset, models, models_names, observer_file='BinObserver', table_name='OpenBins'):
    df = model_data(results_dir, dataset, models, models_names, observer_file)
    grouped_data = df.groupby(['Model']).agg({'Open Bins': [ 'median', iqr]})['Open Bins']
    main_model = models_names[0]
    main_model_data = df[df['Model'] == main_model]['Open Bins']
    for other_model in models_names[1:]:
        other_model_data = df[df['Model'] == other_model]['Open Bins']  
        pvalue = wilcoxon(main_model_data, other_model_data, alternative="two-sided").pvalue
        grouped_data.at[other_model,'p-value'] = pvalue
    with open(f'{results_dir}/{dataset}/{table_name}.tex', 'w') as f:
        print(grouped_data.to_latex(), file=f)
    return(grouped_data)
              