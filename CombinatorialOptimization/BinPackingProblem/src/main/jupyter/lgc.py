import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from open_bins import model_to_df

rolling_count = 5
default_avf_file= 'LGCAVF'
default_policy_file = "LGCPolicy"
default_ticks_per_episode = 300 * 45

def bin_data(results_dir, dataset_name, model):
    df = model_to_df(results_dir, dataset_name, model, observer_file='BinObserver')
    df.index.name='Episode'
    df.reset_index(inplace=True)
    df['Open Bins (Rolling Mean)'] = df['Open Bins'].rolling(rolling_count).mean()
    return(df)

def plot_bin_data(df):
    sns_plot = sns.lineplot(data=df, x="Episode", y='Open Bins (Rolling Mean)', ci=None)
    sns_plot = sns.lineplot(data=df, x="Episode", y='Open Bins', ci=None)
    sns_plot.set(ylim=(105, None))
    return(sns_plot)

def avf_data(results_dir, dataset_name, model, filename=default_avf_file, ticks_per_episode=default_ticks_per_episode):
    model_dir = f"{results_dir}/{dataset_name}/{model}"
    file = f"{model_dir}/{filename}.csv"
    df = pd.read_csv(file)
    df["Episode"] = (df["Time"]/(ticks_per_episode)).apply(np.int64)
    return(df)

def last_avf_data(df):    
    last_df = df[df["Episode"] == max(df["Episode"])]
    mean_last_df = pd.pivot_table(last_df, index=['StateAction'], values=['Value'], aggfunc=(np.mean, np.std))
    mean_last_df = mean_last_df.sort_values(by=('Value','mean'), ascending=False)
    return(mean_last_df)

def plot_avf(avf_df):
    sns_plot = sns.lineplot(data=avf_df, x="Episode", y='Value', hue="StateAction", ci=None)
    sns_plot.legend(loc='upper left', bbox_to_anchor=(1, 1))

def read_policy_data(results_dir, dataset_name, model, filename=default_policy_file, ticks_per_episode=default_ticks_per_episode):
    model_dir = f"{results_dir}/{dataset_name}/{model}"
    file = f"{model_dir}/{filename}.csv"
    policy_df = pd.read_csv(file, skipinitialspace=True)
    policy_df["Episode"] = (policy_df["Time"]/(ticks_per_episode)).apply(np.int64)
    policy_df['StateAction'] = policy_df["State"].astype(str) + " " + policy_df["Action"].astype(str)
    return(policy_df)

def plot_policy_actions_data(df):
    policy_actions_df = df.groupby(['Episode', 'Time', 'State', 'Action', 'StateAction'], as_index=False).size()
    plt.figure()
    sns_plot = sns.lineplot(data=policy_actions_df, x="Episode", y='size', hue=("StateAction"), ci=None)
    sns_plot.set_title("Policy")
    plt.ylim([0, 160])
    plt.legend(loc='upper left', bbox_to_anchor=(1, 1))
    return(policy_actions_df)

def last_policy_data(df):    
    last_df = df[df["Episode"] == max(df["Episode"])]    
    return(last_df[['Episode','StateAction','size']])
#     mean_last_df = pd.pivot_table(last_df, index=['StateAction'], values=['Value'], aggfunc=(np.mean, np.std))
#     mean_last_df = mean_last_df.sort_values(by=('Value','mean'), ascending=False)
#     return(mean_last_df)