import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.stats import shapiro, iqr, wilcoxon

def egc_model_to_df(results_dir, exp_name, seed):
    egc_data = f"{results_dir}/{exp_name}/{seed}/egc.stat"
    df = pd.read_csv(egc_data)
    return (df)

def egc_model_data(results_dir, exp_name, max_seed=20):
    df = pd.DataFrame()
    for seed in range(0, max_seed):
        seed_df = egc_model_to_df(results_dir, exp_name, seed)
        seed_df['Seed'] = seed
        df = df.append(seed_df, ignore_index = True)
    return(df)

def figure_egc_box(results_dir, exp_name, seed,  y='openBins', figure_name='OpenBins'):
    data = egc_model_to_df(results_dir, exp_name, seed)
    fig, axes = plt.subplots()
    fig.set_size_inches(12, 3.5)
    axes.set_title(f"{exp_name} (seed={seed})")
    sns.set_theme(style="whitegrid")
    box_plot = sns.boxplot(data=data, x="generation", y=y)
    box_plot.set(xlabel='Generarions',ylabel='Open Bins')
    plt.savefig(f'{results_dir}/{exp_name}/{seed}/{figure_name}.eps', format='eps')

def figure_run_times(results_dir, exp_name, seed):
    df = egc_model_to_df(results_dir, exp_name, seed)
    fig, axs = plt.subplots(ncols=2, figsize=(15,5))
    sns.boxplot(ax=axs[0], data=df, x='generation', y='Mean Run Time')
    sns.scatterplot(ax=axs[1], data=df, x="evaluation", y="Mean Run Time", hue='generation')

def min_open_bins_per_generation(results_dir, exp_name, seed):
    df = egc_model_to_df(results_dir, exp_name, seed)
    df.groupby('generation').max()["Evaluation Time"].sum()/60/60
    min_open_bins = df.groupby(['generation']).min()['openBins']
    return(min_open_bins)

def min_open_bins_by_seed(results_dir, exp, max_seed=20, y='openBins'):
    df = egc_model_data(results_dir, exp, max_seed)
    min_open_bins = df.groupby(['Seed']).min()[y]
    return (min_open_bins)

def min_open_bins_per_exp(results_dir, exp, max_seed=20, y='openBins'):
    df = egc_model_data(results_dir, exp, max_seed)
    min_open_bins = df.groupby(['Seed', 'generation']).min()[y]
    overall_min = min_open_bins.min()
    (overall_min_seed, overall_min_generation)= min_open_bins[min_open_bins == overall_min].index[0]
    print(f"For {exp} overall min is {overall_min} in seed {overall_min_seed} in generation {overall_min_generation}")
    return (overall_min_seed, overall_min_generation, min_open_bins)

def figure_min_open_bins_generation(results_dir, exp_name, seed, y='openBins', figure_name='OpenBins'):
    min_open_bins = min_open_bins_per_generation(results_dir, exp_name, seed)
    fig, axes = plt.subplots()
    fig.set_size_inches(12, 4)
    axes.set_title(f"{exp_name} (seed={seed})")
    sns.set_theme(style="whitegrid")
    lineplot = sns.lineplot(data=min_open_bins, alpha=1)
    lineplot.set(xlabel='Generarions',ylabel='Open Bins')
    plt.savefig(f'{results_dir}/{exp_name}/{seed}/Min{figure_name}.eps', format='eps')
      
def table_min_open_bins_seed(results_dir, exp_name, median, max_seed=20, y='openBins', table_name='OpenBins'):
    min_open_bins = min_open_bins_by_seed(results_dir,exp_name, max_seed, y)
    print(f"{exp_name} with {max_seed} different seeds median is {min_open_bins.median()} IQR is {iqr(min_open_bins)}, normal={shapiro(min_open_bins).pvalue > 0.5}")
    pvalue = wilcoxon(min_open_bins-median, alternative="less", mode='approx').pvaluex
    print(f"{exp_name} Alternatic hypothesis that median, {min_open_bins.median()}, < {median} with {pvalue}")
    with open(f'{results_dir}/{exp_name}/{table_name}.tex', 'w') as f:
        min_open_bins_df = pd.DataFrame(min_open_bins)
        print(min_open_bins_df.T)
        print(min_open_bins_df.T.to_latex(), file=f)
        
def figure_min_open_bins_seed(results_dir, exp_name, max_seed=20, y='openBins', figure_name='OpenBins'):
    min_open_bins = min_open_bins_by_seed(results_dir, exp_name, max_seed, y)
    fig, axes = plt.subplots()
    fig.set_size_inches(3, 2.5)
    axes.set_title(f"{exp_name} Minimum Open Bins")
    sns.set_theme(style="whitegrid")
    sns.boxplot(data=min_open_bins)
    plt.savefig(f'{results_dir}/{exp_name}/{figure_name}.eps', format='eps')
    
def figure_open_bins_by_exps(results_dir, exp_names, max_seed, y='openBins', figure_name='OpenBins'):
    fig, axes = plt.subplots()
    fig.set_size_inches(12, 4)
    sns.set_theme(style="whitegrid")
    df = pd.DataFrame()
    for exp_name in exp_names:
        min_open_bins = min_open_bins_by_seed(results_dir, exp_name, max_seed, y)
        df[exp_name] = min_open_bins
    box_plot = sns.boxplot(data=df, orient="h")
    box_plot.set(xlabel='Open Bins')
    plt.savefig(f'{results_dir}/{figure_name}.eps', format='eps') 