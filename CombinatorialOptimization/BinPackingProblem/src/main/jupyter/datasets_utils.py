import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def read_csv(path, mumber_of_items):
    columns_names = list(range(mumber_of_items))
    df = pd.read_csv(path, names=columns_names, index_col=False)
    return(df)

def mapToTuples(queue): 
    for index, item in queue.items():
        (size, arrive, depart) = map(int, item.strip("()").split(' '))
        queue[index] = (size, arrive, depart)
    return(queue)

def add_time_value(time_series, time, value):
    if time in time_series.keys():
        current_value = time_series[time]
        time_series[time] = current_value + value
    else:
        time_series[time] = value

def to_time_dict(queue):
    time_dict = {}
    for (size, arrive, depart) in queue:
        add_time_value(time_dict, arrive, size)
        add_time_value(time_dict, depart, -size)
    return(time_dict)

def accululative_values(time_series):
    accululative_value = 0
    time_series[0] = accululative_value
    for (time, value) in sorted(time_series.items()):
        accululative_value += value
        time_series[time] = accululative_value
    return(time_series)

def to_series(df, queue_number, number_of_time_steps=4500):
    queue = df.loc[queue_number].copy()
    queue = mapToTuples(queue)
    queue_time_dict = to_time_dict(queue)
    queue_time_dict = accululative_values(queue_time_dict)
    series = pd.Series(queue_time_dict).sort_index()
    all_time_steps = list(range(number_of_time_steps))
    series = series.reindex(index=all_time_steps, method='pad')
    return(series)

def to_timeseries_df(df, number_of_time_steps=4500):
    series = {}
    number_of_queues = df.shape[0]
    for queue_number in range(number_of_queues):
        series[queue_number] = to_series(df, queue_number, number_of_time_steps)
    timeseries_df = pd.DataFrame(series)        
    timeseries_df['mean'] = timeseries_df.mean(axis=1)
    timeseries_df['std'] = timeseries_df.std(axis=1)
    return(timeseries_df)

def dataset_name_for(dataset_type, number_of_items, dataset):
    return(f'{dataset_type}-{number_of_items}-{dataset}')
    
def dataset_to_df(datasets_dir, dataset_name, number_of_items):
    dataset_path = f'{datasets_dir}/{dataset_name}.csv'
    dataset_df = read_csv(dataset_path, number_of_items)
    return(dataset_df)

def figure(datasets_dir, dataset_name, dataset_timeseries_df, figure_name):
    dataset_timeseries_plot = dataset_timeseries_df.plot.line(y = "mean", 
                                              title = "Total size of items over time", 
                                              legend = False)
    dataset_timeseries_plot.set_ylabel("Total size of items")
    dataset_timeseries_plot.set_xlabel("Tims step")
    plt.savefig(f'{datasets_dir}/{dataset_name}-{figure_name}.eps', format='eps')

def dataset_figures(datasets_dir, dataset_type, number_of_items, dataset, number_of_time_steps=4500):
    dataset_name = dataset_name_for(dataset_type, number_of_items, dataset)

    dataset_df = dataset_to_df(datasets_dir, dataset_name, number_of_items)
    dataset_timeseries_df = to_timeseries_df(dataset_df, number_of_time_steps)
    
    figure(datasets_dir, dataset_name, dataset_timeseries_df, "OpenBins")

def models_data(results_dir, dataset_name, models):
    data = {}
    for model in models:
        df = model_to_df(results_dir, dataset_name, model)
        data[model] = df['Open Bins']
    return(pd.DataFrame(data))

def model_to_df(results_dir, dataset_name, model, observer_file='BinObserver'): 
    model_dir = f"{results_dir}/{dataset_name}/{model}"
    observer_data = f"{model_dir}/{observer_file}.csv"
    df = pd.read_csv(observer_data)
    return (df)


def results_figure(results_dir, dataset_name, dataset_timeseries_df, models_df, figure_name):
    fig, axs = plt.subplots(ncols=2, figsize=(15,5))
    fig.suptitle(dataset_name, fontsize=16)
    dataset_plot = dataset_timeseries_df.plot.line(y = "mean", 
                                              title = "Total size of items over time", 
                                              legend = False, 
                                              ax=axs[0])
    dataset_plot.set_ylabel("Total size of items")
    dataset_plot.set_xlabel("Tims step")
    sns.boxplot(data=models_df, ax=axs[1])
    axs[1].set_xticklabels(axs[1].get_xticklabels(), rotation=40, ha="right")
    plt.savefig(f'{results_dir}/{dataset_name}/{figure_name}s.eps', format='eps')
    
    
def results_figures(datasets_dir, dataset_type, number_of_items, dataset, models, results_dir):
    dataset_name = dataset_name_for(dataset_type, number_of_items, dataset)

    dataset_df = dataset_to_df(datasets_dir, dataset_name, number_of_items)
    dataset_timeseries_df = to_timeseries_df(dataset_df)
    
    models_df = models_data(results_dir, dataset_name, models)
    results_figure(results_dir, dataset_name, dataset_timeseries_df, models_df, "Models")