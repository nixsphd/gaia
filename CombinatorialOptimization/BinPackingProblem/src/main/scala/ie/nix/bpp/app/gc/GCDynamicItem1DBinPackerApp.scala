package ie.nix.bpp.app.gc

import ie.nix.bpp.app.dynamic.DynamicItem1DPackingApp
import org.apache.logging.log4j.scala.Logging

object GCDynamicItem1DBinPackerApp extends DynamicItem1DPackingApp with Logging {

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties",
                    "protocol.agent=ie.nix.bpp.packer.dynamicitem1d.GCDynamicItem1DBinPacker")

  silent()
  initRandom()
  packProblems()

  override def resultsSetName = "GCBinDynamicPacker"
}
