package ie.nix.bpp.packer.item1d

import ie.nix.bpp.Size
import ie.nix.bpp.app.online.OnlineItem1DBinPackingApp
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.GenericBestFitPacker
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D

object BestFitItem1DPackerApp extends OnlineItem1DBinPackingApp {

  initRandom()
  packProblems()

  override def resultsSetName: String = "BestFitItem1DPacker"
  override def initPacker(): Unit =
    packer = BestFitItem1DPacker()

}

case class BestFitItem1DPacker() extends GenericBestFitPacker[Size1D, Item1D] {
  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
}
