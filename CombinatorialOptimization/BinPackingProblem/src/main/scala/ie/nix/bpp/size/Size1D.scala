package ie.nix.bpp.size

import ie.nix.bpp.Size

case class Size1D(size: Int)

object Size1D {

  implicit object SizeLikeSize1D extends Size[Size1D] {

    override def compare(x: Size1D, y: Size1D): Int = x.size compare y.size

    override def zero: Size1D = Size1D(0)
    override def isPositive(size: Size1D): Boolean =
      size.size >= 0
    override def plus(oneSize: Size1D, otherSize: Size1D): Size1D =
      Size1D(oneSize.size + otherSize.size)
    override def minus(oneSize: Size1D, otherSize: Size1D): Size1D =
      Size1D(oneSize.size - otherSize.size)

    override def toString(size: Size1D): String = s"${size.size}"
    override def toSize(size1DAsString: String): Size1D = Size1D(size1DAsString.toInt)

  }

}
