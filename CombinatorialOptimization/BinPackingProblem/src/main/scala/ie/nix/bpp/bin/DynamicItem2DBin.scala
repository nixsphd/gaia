package ie.nix.bpp.bin

import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.size.Size2D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.{Item, Size}
import ie.nix.peersim.Scheduler.Callback
import ie.nix.peersim.Utils.now
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

class DynamicItem2DBin(prefix: String) extends GenericPeerSimBin[Size2D](prefix) with Logging {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D

  override def toString = s"DynamicItem2DBin-$getIndex[$usedCapacity/$capacity]"

  override def capacity: Size2D = Item2DPeerSimBinCapacity.capacity

  override def addItem(item: Item[Size2D]): Unit = item match {
    case dynamicItem: DynamicItem2D => addDynamicIItem(dynamicItem)
    case notADynamicItem            => logger error s"$notADynamicItem is not a DynamicItem2D"
  }

  def addDynamicIItem(item: DynamicItem2D): Unit = {
    super.addItem(item)
    val delay = item.departureDelay.toInt
    logger trace s"[$now] $item will be removed from $this in $delay"
    if (delay > 0)
      addCallback(delay, Callback(() => {
        removeItem(item)
        logger debug s"[$now] Removed $item from $this"
      }))
  }

}

object DynamicItem2DBin {

  implicit val toBin: peersim.core.Node => DynamicItem2DBin =
    node => as[DynamicItem2DBin](node)

}
