package ie.nix.bpp.app

import ie.nix.bpp.app.GenericPeerSimBinPackingAppArgs.{BIN_CAPACITY, NETWORK_SIZE, SEED, configurationInitialised}
import org.apache.logging.log4j.scala.Logging
import peersim.config.{Configuration, ParsedProperties}

trait GenericPeerSimBinPackingAppArgs[S] extends Logging {
  app: App =>

  protected val USAGE = "Please pass in one argument structured as follows: " +
    "dataSetFileName peerSimPropertiesFiles..."

  def checkArgs(): Unit = {
    assert(args != null, s"There were no args; there was ${args} but should be 2.\n$USAGE")
    assert(args.length >= 2, s"There was not enough args; there was ${args.length} but should be 2.\n$USAGE")
  }

  def dataSetFileName: String = { checkArgs(); s"${args(0)}" }
  def properties: Array[String] = { checkArgs(); propertiesOverrides ++ args.slice(1, args.length) }
  def propertiesOverrides: Array[String]

  def initConfiguration(): Unit =
    if (!configurationInitialised) {
      peersim.config.Configuration.setConfig(new ParsedProperties(properties))
      configurationInitialised = true
    }

  def numberOfBins: Int = { initConfiguration(); Configuration.getInt(NETWORK_SIZE) }
  def binCapacityFromConfig: String = { initConfiguration(); Configuration.getString(BIN_CAPACITY) }

  def seed: Long = { initConfiguration(); Configuration.getLong(SEED) }

}

object GenericPeerSimBinPackingAppArgs {

  val NETWORK_SIZE = "network.size"
  val BIN_CAPACITY = "network.node.bin-capacity"
  val SEED = "random.seed"

  private[GenericPeerSimBinPackingAppArgs] var configurationInitialised = false

}
