package ie.nix.bpp.packer.egc

import ie.nix.bpp.packer.gc.GenericGCBinPacker
import ie.nix.bpp.{Item, Queue}
import ie.nix.ecj.PeerSimProblem
import ie.nix.egc.EGCAgent
import ie.nix.gc.message.TenderMessage
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

trait GenericEGCBinPacker[S, I <: Item[S]] extends EGCAgent[I, Void] with Logging {
  this: GenericGCBinPacker[S, I] =>

  def queue: Queue[S, I] = PeerSimProblem.gpProblem match {
    case egcBinPackerProblem: GenericEGCBinPackerProblem[S, I] => egcBinPackerProblem.queue
    case _ =>
      logger error s"Expected the gpProblem to be an EGCBinPackerProblem but was a " +
        s"${PeerSimProblem.gpProblem.getClass.getSimpleName}"
      exit
  }

  override def shouldTender(node: Node): Boolean =
    hasTask && super[EGCAgent].shouldTender(node)

  override def shouldBid(tenderMessage: TenderMessage[I]): Boolean =
    doesNotHaveATask &&
      bin(tenderMessage.to).canFit(tenderMessage.task) &&
      super[EGCAgent].shouldBid(tenderMessage)

}
