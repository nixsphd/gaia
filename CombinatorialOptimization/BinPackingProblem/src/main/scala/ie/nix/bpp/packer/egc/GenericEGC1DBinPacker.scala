package ie.nix.bpp.packer.egc

import ie.nix.bpp.Item
import ie.nix.bpp.packer.gc.GenericGC1DBinPacker
import ie.nix.bpp.size.Size1D
import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Variable
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

trait GenericEGC1DBinPacker[I <: Item[Size1D]] extends GenericEGCBinPacker[Size1D, I] with Logging {

  gcBinPacker: GenericGC1DBinPacker[I] =>

  override protected def defaultShouldTender(node: Node): Boolean =
    gcBinPacker.shouldTender(node)

  override protected def defaultGetGuide(node: Node, task: I): Double =
    gcBinPacker.getGuide(node, task)

  override protected def defaultTenderToDouble(tenderMessage: TenderMessage[I]): Double =
    gcBinPacker.tenderToDouble(tenderMessage)

  override protected def defaultShouldBid(tenderMessage: TenderMessage[I]): Boolean =
    gcBinPacker.shouldBid(tenderMessage)

  override protected def defaultGetOffer(tenderMessage: TenderMessage[I], proposal: Void): Double =
    gcBinPacker.getOffer(tenderMessage, proposal)

  override protected def defaultBidToDouble(bidMessage: BidMessage[I, Void]): Double =
    gcBinPacker.bidToDouble(bidMessage)

  override protected def defaultShouldAward(bidMessage: BidMessage[I, Void]): Boolean =
    gcBinPacker.shouldAward(bidMessage)

  override protected def updateNodes(node: Node): Unit = {
    BinCapacity.value = bin(node).capacity.size.toDouble
    FreeCapacity.value = bin(node).freeCapacity.size.toDouble
    NumberOfItems.value = bin(node).numberOfItems.toDouble
  }

  override protected def updateTaskNode(task: I): Unit =
    ItemSize.value = task.size.size.toDouble

  override protected def updateProposalNode(proposal: Void): Unit = {}

}

class BinCapacity()
    extends Variable("binCapacity", (_: Array[TypedData], result: TypedData) => result.set(BinCapacity.value))
object BinCapacity { var value = 0d }

class FreeCapacity()
    extends Variable("freeCapacity", (_: Array[TypedData], result: TypedData) => result.set(FreeCapacity.value))
object FreeCapacity { var value = 0d }

class ItemSize() extends Variable("itemSize", (_: Array[TypedData], result: TypedData) => result.set(ItemSize.value))
object ItemSize { var value = 0d }

class NumberOfItems()
    extends Variable("numberOfItems", (_: Array[TypedData], result: TypedData) => result.set(NumberOfItems.value))
object NumberOfItems { var value = 0d }
