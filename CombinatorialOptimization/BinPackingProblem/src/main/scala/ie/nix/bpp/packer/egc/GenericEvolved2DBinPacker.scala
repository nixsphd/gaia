package ie.nix.bpp.packer.egc

import ie.nix.bpp.Item
import ie.nix.bpp.packer.gc.GenericGC2DBinPacker
import ie.nix.bpp.size.Size2D
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

trait GenericEvolved2DBinPacker[I <: Item[Size2D]] extends Logging {
  gd2DBinPacker: GenericGC2DBinPacker[I] =>

  override def shouldTender(node: Node): Boolean =
    hasTask &&
      shouldTender(bin(node).freeCapacity.size1.toDouble,
                   bin(node).freeCapacity.size2.toDouble,
                   getTask.size.size1.toDouble,
                   getTask.size.size2.toDouble)

  override def getGuide(node: Node, task: I): Double =
    getGuide(bin(node).freeCapacity.size1.toDouble,
             bin(node).freeCapacity.size2.toDouble,
             getTask.size.size1.toDouble,
             getTask.size.size2.toDouble)

  override def tenderToDouble(tenderMessage: TenderMessage[I]): Double =
    tenderToDouble(
      tenderMessage.guide,
      bin(node).freeCapacity.size1.toDouble,
      bin(node).freeCapacity.size2.toDouble,
      tenderMessage.task.size.size1.toDouble,
      tenderMessage.task.size.size2.toDouble
    )

  override def shouldBid(tenderMessage: TenderMessage[I]): Boolean =
    doesNotHaveATask &&
      bin(tenderMessage.to).canFit(tenderMessage.task) &&
      shouldBid(
        tenderMessage.guide,
        bin(tenderMessage.to).freeCapacity.size1.toDouble,
        bin(tenderMessage.to).freeCapacity.size2.toDouble,
        tenderMessage.task.size.size1.toDouble,
        tenderMessage.task.size.size2.toDouble
      )

  override def getOffer(tenderMessage: TenderMessage[I], proposal: Void): Double =
    getOffer(
      tenderMessage.guide,
      bin(tenderMessage.to).freeCapacity.size1.toDouble,
      bin(tenderMessage.to).freeCapacity.size2.toDouble,
      tenderMessage.task.size.size1.toDouble,
      tenderMessage.task.size.size2.toDouble
    )

  override def bidToDouble(bidMessage: BidMessage[I, Void]): Double =
    bidToDouble(
      bidMessage.offer,
      bin(bidMessage.to).freeCapacity.size1.toDouble,
      bin(bidMessage.to).freeCapacity.size2.toDouble,
      bidMessage.task.size.size1.toDouble,
      bidMessage.task.size.size2.toDouble
    )

  override def shouldAward(bidMessage: BidMessage[I, Void]): Boolean =
    shouldAward(
      bidMessage.offer,
      bin(bidMessage.to).freeCapacity.size1.toDouble,
      bin(bidMessage.to).freeCapacity.size2.toDouble,
      bidMessage.task.size.size1.toDouble,
      bidMessage.task.size.size2.toDouble
    )

  def shouldTender(freeCapacityX: Double, freeCapacityY: Double, itemSizeX: Double, itemSizeY: Double): Boolean
  def getGuide(freeCapacityX: Double, freeCapacityY: Double, itemSizeX: Double, itemSizeY: Double): Double
  def tenderToDouble(guide: Double,
                     freeCapacityX: Double,
                     freeCapacityY: Double,
                     itemSizeX: Double,
                     itemSizeY: Double): Double
  def shouldBid(guide: Double,
                freeCapacityX: Double,
                freeCapacityY: Double,
                itemSizeX: Double,
                itemSizeY: Double): Boolean
  def getOffer(guide: Double,
               freeCapacityX: Double,
               freeCapacityY: Double,
               itemSizeX: Double,
               itemSizeY: Double): Double
  def bidToDouble(offer: Double,
                  freeCapacityX: Double,
                  freeCapacityY: Double,
                  itemSizeX: Double,
                  itemSizeY: Double): Double
  def shouldAward(offer: Double,
                  freeCapacityX: Double,
                  freeCapacityY: Double,
                  itemSizeX: Double,
                  itemSizeY: Double): Boolean

}
