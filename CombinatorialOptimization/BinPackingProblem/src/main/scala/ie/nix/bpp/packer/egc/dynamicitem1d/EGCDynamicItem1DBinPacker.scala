package ie.nix.bpp.packer.egc.dynamicitem1d

import ie.nix.bpp.Queue
import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.packer.egc.GenericEGC1DBinPacker
import ie.nix.bpp.packer.gc.dynamicitem1d.GCDynamicItem1DBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

class EGCDynamicItem1DBinPacker(prefix: String)
    extends GCDynamicItem1DBinPacker(prefix)
    with GenericEGC1DBinPacker[DynamicItem1D]
    with Logging {

  override def queue: Queue[Size1D, DynamicItem1D] =
    super[GenericEGC1DBinPacker].queue

}
