package ie.nix.bpp.app.gc

import ie.nix.bpp.app.dynamic.DynamicItem2DPackingApp
import org.apache.logging.log4j.scala.Logging

object GCDynamicItem2DBinPackerApp extends DynamicItem2DPackingApp with Logging {

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String](
        "src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties",
        "network.node=DynamicItem2DBin",
        "control.bin_observer=DynamicItem2DBinObserver",
        "control.bin_integrity_observer=DynamicItem2DIntegrityObserver",
        "protocol.agent=ie.nix.bpp.packer.dynamicitem2d.GCDynamicItem2DBinPacker"
      )

  silent()
  initRandom()
  packProblems()

  override def resultsSetName = "GCBinDynamicPacker"
}
