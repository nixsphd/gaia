package ie.nix.bpp.dataset

import ie.nix.bpp.dataset.DataSet.saveQueuesToFile
import ie.nix.bpp.item.{Item1D, Item2D}
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.size.{Size1D, Size2D}
import ie.nix.bpp.{Item, Size}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

trait GenericUniformDataSets[S, I <: Item[S]] extends Logging {
  this: App =>

  implicit def sizeLike: Size[S]
  implicit def itemToString: I => String
  implicit def queueGenerator: QueueGenerator[S, I]

  val USAGE = "Please pass in one argument structured as follows: " +
    "numberOfQueues numberOfItems minItemSize-maxItemSize binCapacity"

  def uniformDataSet: DataSet[S, I] = DataSet[S, I](numberOfQueues, numberOfItems)
  def uniformDataSetFilename = s"UniformDataSets-$numberOfItems-${args(2)}.csv"

  def checkArgs(): Unit = {
    assert(args != null, s"There are no args; there should be 3.\n$USAGE")
    assert(args.length >= 3, s"There was not enough args; there was ${args.length} but should be 3.\n$USAGE")
  }
  def numberOfQueues: Int = { checkArgs(); args(0).toInt }
  def numberOfItems: Int = { checkArgs(); args(1).toInt }
  def bppArgs: Vector[String] = { checkArgs(); args(2).split("-").toVector }
  def minItemSize: Int = { checkArgs(); bppArgs(0).toInt }
  def maxItemSize: Int = { checkArgs(); bppArgs(1).toInt }
  def binCapacity: Int = { checkArgs(); args(3).toInt }

}

object UniformItem1DDataSets extends App with GenericUniformDataSets[Size1D, Item1D] with Logging {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
  override implicit def itemToString: Item[Size1D] => String = Item.itemToString[Size1D]
  override implicit def queueGenerator: QueueGenerator[Size1D, Item1D] =
    UniformItem1DQueueGenerator(minItemSize, maxItemSize)

  override val uniformDataSetFilename = s"UniformItem1DDataSets-$numberOfItems-${args(2)}.csv"

  saveQueuesToFile[Size1D, Item1D](uniformDataSet, uniformDataSetFilename)

}
case class UniformItem1DQueueGenerator(minItemSize: Int, maxItemSize: Int) extends QueueGenerator[Size1D, Item1D] {
  override def generateItems: Vector[Item1D] = {
    val size1 = minItemSize + Random.nextInt(maxItemSize - minItemSize)
    Vector(new Item1D(Size1D(size1)))
  }
}

object UniformItem2DDataSets extends App with GenericUniformDataSets[Size2D, Item2D] with Logging {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
  override implicit def itemToString: Item[Size2D] => String = Item.itemToString[Size2D]
  override implicit def queueGenerator: QueueGenerator[Size2D, Item2D] =
    UniformItem2DQueueGenerator(minItemSize, maxItemSize)

  override val uniformDataSetFilename = s"UniformItem2DDataSets-$numberOfItems-${args(2)}.csv"

  saveQueuesToFile[Size2D, Item2D](uniformDataSet, uniformDataSetFilename)

}
case class UniformItem2DQueueGenerator(minItemSize: Int, maxItemSize: Int) extends QueueGenerator[Size2D, Item2D] {
  override def generateItems: Vector[Item2D] = {
    val size1 = minItemSize + Random.nextInt(maxItemSize - minItemSize)
    val size2 = minItemSize + Random.nextInt(maxItemSize - minItemSize)
    Vector(new Item2D(Size2D(size1, size2)))
  }
}
