package ie.nix.bpp.bin

import ie.nix.bpp.app.GenericPeerSimBinPackingAppArgs.BIN_CAPACITY
import ie.nix.bpp.{Bin, Size}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

abstract class GenericPeerSimBin[S](prefix: String) extends Node(prefix) with Bin[S] with Logging {

  override def toString = getIndex match {
    case -1 => s"GenericPeerSimBin-?[$usedCapacity/$capacity]"
    case _  => s"GenericPeerSimBin-$getIndex[$usedCapacity/$capacity]"
  }

}

trait PeerSimBinCapacity[S] {

  implicit def sizeLike: Size[S]

  private var maybeCapacity: Option[S] = None

  def capacity: S = maybeCapacity match {
    case Some(binCapacity) => binCapacity
    case None              => sizeLike.toSize(Configuration.getString(BIN_CAPACITY))
  }

  def capacity(binCapacity: S): Unit =
    maybeCapacity = Some(binCapacity)

}
