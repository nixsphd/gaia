package ie.nix.bpp.packer.item2d

import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.packer.GenericBestFitDecreasingPacker
import ie.nix.bpp.Size
import ie.nix.bpp.app.offline.OfflineItem2DBinPackingApp
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

object BestFitDecreasingItem2DPackerApp extends OfflineItem2DBinPackingApp with Logging {

  override implicit def toItem: String => Item2D = Item2D.toItem

  initRandom()
  packProblems()

  override def resultsSetName = "BestFitDecreasingItem2DPacker"
  override def initPacker(): Unit = packer = new BestFitDecreasingItem2DPacker()
}

class BestFitDecreasingItem2DPacker()
    extends BestFitItem2DPacker
    with GenericBestFitDecreasingPacker[Size2D, Item2D]
    with Logging {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D

}
