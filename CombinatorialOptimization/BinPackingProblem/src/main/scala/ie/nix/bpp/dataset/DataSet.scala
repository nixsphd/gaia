package ie.nix.bpp.dataset

import ie.nix.bpp.dataset.DataSet.{generateQueues, loadQueues}
import ie.nix.bpp.{Item, Size}
import ie.nix.util.{CSVFileReader, CSVFileWriter}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

class DataSet[S, I <: Item[S]](protected val items: Vector[Vector[I]])(implicit val sizeLike: Size[S]) extends Logging {

  def this(numberOfQueues: Int, numberOfItems: Int)(implicit sizeLike: Size[S], queueGenerator: QueueGenerator[S, I]) =
    this(generateQueues[S, I](numberOfQueues, numberOfItems))

  def this(dirName: String, fileName: String)(implicit sizeLike: Size[S], rowValueToItem: String => I) =
    this(loadQueues[S, I](CSVFileReader(s"$dirName", fileName)))

  def numberOfQueues: Int = items.size

  def numberOfItems: Int = items(0).size

  def itemsForQueue(queueNumber: Int): Vector[I] = items(queueNumber)

}

object DataSet extends Logging {

  val RESOURCES_DIR = "src/main/resources"
  val DATASETS_DIR = "ie/nix/bpp/datasets"

  def writer(filename: String): CSVFileWriter =
    CSVFileWriter(s"$RESOURCES_DIR/$DATASETS_DIR", filename)

  def reader(filename: String): CSVFileReader =
    CSVFileReader(s"$RESOURCES_DIR/$DATASETS_DIR", filename)

  /*
   * Generate a new set of items
   */
  def apply[S, I <: Item[S]](items: Vector[Vector[I]])(implicit sizeLike: Size[S]): DataSet[S, I] = {
    new DataSet[S, I](items)
  }

  def apply[S, I <: Item[S]](numberOfQueues: Int, numberOfItems: Int)(
      implicit sizeLike: Size[S],
      queueGenerator: QueueGenerator[S, I]): DataSet[S, I] = {
    new DataSet[S, I](numberOfQueues, numberOfItems)
  }

  def generateQueues[S, I <: Item[S]](numberOfQueues: Int, numberOfItems: Int)(
      implicit queueGenerator: QueueGenerator[S, I]): Vector[Vector[I]] =
    (0 until numberOfQueues).foldLeft(Vector[Vector[I]]())((queues, queueNumber) => {
      queueGenerator.preGenerateQueue(queueNumber)
      val queue = generateQueue[S, I](numberOfItems)
      queues :+ queueGenerator.postGenerateQueue(queueNumber, queue)
    })

  def setRandomSeed(run: Int): Unit = Random.setSeed(run.toLong)

  protected def generateQueue[S, I <: Item[S]](numberOfItems: Int)(
      implicit queueGenerator: QueueGenerator[S, I]): Vector[I] = {
    def _generateQueue(items: Vector[I]): Vector[I] = {
      items.size < numberOfItems match {
        case false => items
        case true  => _generateQueue(items ++ queueGenerator.generateItems)
      }
    }
    _generateQueue(Vector[I]())
  }

  /*
   * Save a set of items from a file
   */
  def saveQueuesToFile[S, I <: Item[S]](bpp: DataSet[S, I], dirName: String, fileName: String)(
      implicit itemToRowValue: I => String): Unit =
    saveQueues[S, I](bpp, CSVFileWriter(dirName, fileName))

  def saveQueuesToFile[S, I <: Item[S]](bpp: DataSet[S, I], filename: String)(
      implicit itemToRowValue: I => String): Unit =
    saveQueues[S, I](bpp, writer(filename))

  protected def saveQueues[S, I <: Item[S]](bpp: DataSet[S, I], writer: CSVFileWriter)(
      implicit itemToRowValue: I => String): Unit =
    bpp.items.foreach(saveQueue[S, I](_, writer))

  protected def saveQueue[S, I <: Item[S]](items: Vector[I], writer: CSVFileWriter)(
      implicit itemToRowValue: I => String): Unit =
    writer.writeRow(items.map(itemToRowValue).toList)

  /*
   * Load a set of items from a file
   */
  def apply[S, I <: Item[S]](dirName: String, fileName: String)(implicit sizeLike: Size[S],
                                                                toItem: String => I): DataSet[S, I] = {
    new DataSet[S, I](dirName: String, fileName: String)
  }

  def apply[S, I <: Item[S]](dataSetName: String)(implicit sizeLike: Size[S], toItem: String => I): DataSet[S, I] = {
    val dirName = s"$RESOURCES_DIR/$DATASETS_DIR/"
    apply[S, I](dirName, dataSetName)
  }

  def loadQueuesFromFile[S, I <: Item[S]](dirName: String, fileName: String)(
      implicit toItem: String => I): Vector[Vector[I]] = {
    val csvReader = CSVFileReader(s"$dirName", fileName)
    loadQueues[S, I](csvReader)
  }

  protected def loadQueues[S, I <: Item[S]](reader: CSVFileReader)(implicit toItem: String => I): Vector[Vector[I]] = {
    reader
      .readData()
      .getRows
      .foldLeft(Vector[Vector[I]]())((items, row) => {
        items :+ loadQueue[S, I](row)
      })
  }

  protected def loadQueue[S, I <: Item[S]](row: Vector[String])(implicit toItem: String => I): Vector[I] = {
    row.foldLeft(Vector[I]())((items, rowValue) => {
      items :+ toItem(rowValue)
    })
  }

}

trait QueueGenerator[S, I <: Item[S]] {
  def preGenerateQueues(): Unit = {}
  def preGenerateQueue(queueNumber: Int): Unit = Random.setSeed(queueNumber.toLong)
  def generateItems: Vector[I]
  def postGenerateQueue(queueNumber: Int, queue: Vector[I]): Vector[I] = queue
  def postGenerateQueues(queues: Vector[Vector[I]]): Vector[Vector[I]] = queues
}
