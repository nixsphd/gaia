package ie.nix.bpp.queue

import ie.nix.bpp.dataset.DataSet
import ie.nix.bpp.{DynamicItem, Queue, Size}
import org.apache.logging.log4j.scala.Logging

class DynamicQueue[S, I <: DynamicItem[S]] private (val items: Seq[I]) extends Queue[S, I] with Logging {

  override def toString: String = s"DynamicQueue[$getNumberOfItems], $numberOfItemsQueued"

  override def isEmpty: Boolean = peek.isEmpty || peek.get.hasArrived

  override def numberOfItemsQueued: Int = queue.count(_.hasArrived)

  override def peek: Option[I] = super.peek match {
    case Some(dynamicItem) if dynamicItem.hasArrived => Some(dynamicItem)
    case _                                           => None
  }

  override def poll: Option[I] = peek match {
    case Some(_) => super.poll
    case None    => None
  }

  def itemsQueued: Vector[I] = queue.filter(_.hasArrived).toVector

}

object DynamicQueue extends Logging {

  def apply[S, I <: DynamicItem[S]](dataset: DataSet[S, I], queueNumber: Int): DynamicQueue[S, I] =
    new DynamicQueue(dataset.itemsForQueue(queueNumber))

  def apply[S, I <: DynamicItem[S]](bppFileName: String, queueNumber: Int)(
      implicit sizeLike: Size[S],
      stringToItem: String => I): DynamicQueue[S, I] =
    new DynamicQueue(DataSet[S, I](bppFileName).itemsForQueue(queueNumber))

  def apply[S, I <: DynamicItem[S]](items: Seq[I]): DynamicQueue[S, I] =
    new DynamicQueue(items)

}
