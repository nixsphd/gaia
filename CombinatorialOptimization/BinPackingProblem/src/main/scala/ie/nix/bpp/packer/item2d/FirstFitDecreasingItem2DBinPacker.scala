package ie.nix.bpp.packer.item2d

import ie.nix.bpp.app.offline.OfflineItem2DBinPackingApp
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.packer.GenericFirstFitBinPacker
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

object FirstFitDecreasingItem2DPackerApp extends OfflineItem2DBinPackingApp with Logging {

  initRandom()
  packProblems()

  override def resultsSetName: String = "FirstFitDecreasingItem2DPackerApp"
  override def initPacker(): Unit =
    packer = FirstFitDecreasingItem2DPacker()

}

case class FirstFitDecreasingItem2DPacker() extends GenericFirstFitBinPacker[Size2D, Item2D]
