package ie.nix.bpp.packer.dynamicitem1d

import ie.nix.bpp.bin.DynamicItem1DBin
import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.Queue
import ie.nix.bpp.app.dynamic.DynamicItem1DPackingApp
import ie.nix.bpp.size.Size1D
import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.Utils.now
import ie.nix.peersim.{Node, Protocol}
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import scala.util.Random

object SmartDynamicItem1DBinPackerApp extends DynamicItem1DPackingApp with Logging {

  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/SmartBinPacker.properties",
                    "protocol.smart_bin_packer=SmartDynamicItem1DBinPacker")

  override def resultsSetName = { initConfiguration(); Configuration.getString("protocol.smart_bin_packer") }

  this.queue
}

class SmartDynamicItem1DBinPacker(prefix: String) extends Protocol(prefix) with Logging {

  override def nextCycle(node: Node): Unit =
    node match {
      case bin: DynamicItem1DBin => nextCycle(bin)
      case notADynamicItemBin =>
        logger error s"Expected node to be a DynamicItemBin, " +
          s"but got a ${notADynamicItemBin.getClass.getSimpleName}"
    }

  protected def nextCycle(bin: DynamicItem1DBin): Unit = {
    queue.peek match {
      case Some(item) =>
        if (bin.canFit(item)) {
          if (shouldPackItem(bin)) {
            val itemPacked = packDynamicItem(item, bin)
            if (itemPacked) queue.poll
          }
        }
      case _ => // Do nothing...
    }
  }

  protected def shouldPackItem(bin: DynamicItem1DBin): Boolean = {
    val relativeUsedCapacity = bin.usedCapacity.size.toDouble / bin.capacity.size.toDouble
    val rand = Random.nextDouble() - 0.15
    val shouldPackItem = rand < relativeUsedCapacity
    logger debug s"[$now] shouldPackItem=$shouldPackItem = rand=$rand < relativeUsedCapacity=$relativeUsedCapacity"
    shouldPackItem
  }

  protected def packDynamicItem(item: DynamicItem1D, bin: DynamicItem1DBin): Boolean =
    if (bin.canFit(item)) {
      bin.addItem(item)
      logger debug s"[$now] Added $item to $bin"
      true
    } else {
      // do nothing
      logger trace s"[$now] $item does not fit in $bin"
      false
    }

  def queue: Queue[Size1D, DynamicItem1D] = SmartDynamicItem1DBinPackerApp.queue

  override def processEvent(node: Node, message: Message): Unit = {}
}
