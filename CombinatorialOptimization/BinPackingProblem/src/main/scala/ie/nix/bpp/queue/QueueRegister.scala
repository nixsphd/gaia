package ie.nix.bpp.queue

import ie.nix.bpp.{Item, Queue}
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

trait QueueRegister[S, I <: Item[S]] extends Logging {

  private var maybeQueue: Option[Queue[S, I]] = None

  def registerQueue(queue: Queue[S, I]): Unit = {
    logger trace s"Registering queue = $queue"
    maybeQueue = Some(queue)
  }

  def registeredQueue: Queue[S, I] = maybeQueue match {
    case Some(queue) =>
      logger trace s"registeredQueue=$queue"
      queue
    case None =>
      logger error s"FATAL: Could not find the queue"
      new Throwable().printStackTrace()
      exit
  }

}
