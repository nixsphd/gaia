package ie.nix.bpp.packer.egc.item1d

import ie.nix.bpp.app.online.OnlineItem1DPeerSimBinPackingApp
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.egc.GenericEGCBinPackerProblem
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

class EGCItem1DBinPackerProblem
    extends GenericEGCBinPackerProblem[Size1D, Item1D]
    with OnlineItem1DPeerSimBinPackingApp
    with Logging {

  override def properties: Array[String] =
    super[GenericEGCBinPackerProblem].properties ++
      super[OnlineItem1DPeerSimBinPackingApp].propertiesOverrides

}
