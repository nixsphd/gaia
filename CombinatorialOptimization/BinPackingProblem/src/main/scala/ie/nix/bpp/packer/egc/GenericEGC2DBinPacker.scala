package ie.nix.bpp.packer.egc

import ie.nix.bpp.Item
import ie.nix.bpp.packer.gc.GenericGC2DBinPacker
import ie.nix.bpp.size.Size2D
import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Variable
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

trait GenericEGC2DBinPacker[I <: Item[Size2D]] extends GenericEGCBinPacker[Size2D, I] with Logging {

  gcBinPacker: GenericGC2DBinPacker[I] =>

  override protected def defaultShouldTender(node: Node): Boolean =
    gcBinPacker.shouldTender(node)

  override protected def defaultGetGuide(node: Node, task: I): Double =
    gcBinPacker.getGuide(node, task)

  override protected def defaultTenderToDouble(tenderMessage: TenderMessage[I]): Double =
    gcBinPacker.tenderToDouble(tenderMessage)

  override protected def defaultShouldBid(tenderMessage: TenderMessage[I]): Boolean =
    gcBinPacker.shouldBid(tenderMessage)

  override protected def defaultGetOffer(tenderMessage: TenderMessage[I], proposal: Void): Double =
    gcBinPacker.getOffer(tenderMessage, proposal)

  override protected def defaultBidToDouble(bidMessage: BidMessage[I, Void]): Double =
    gcBinPacker.bidToDouble(bidMessage)

  override protected def defaultShouldAward(bidMessage: BidMessage[I, Void]): Boolean =
    gcBinPacker.shouldAward(bidMessage)

  override protected def updateNodes(node: Node): Unit = {
    BinCapacityX.value = bin(node).capacity.size1.toDouble
    BinCapacityY.value = bin(node).capacity.size2.toDouble
    FreeCapacityX.value = bin(node).freeCapacity.size1.toDouble
    FreeCapacityY.value = bin(node).freeCapacity.size2.toDouble
    NumberOfItems.value = bin(node).numberOfItems.toDouble
  }

  override protected def updateTaskNode(task: I): Unit = {
    ItemSizeX.value = task.size.size1.toDouble
    ItemSizeY.value = task.size.size2.toDouble
  }

  override protected def updateProposalNode(proposal: Void): Unit = {}

}

class BinCapacityX()
    extends Variable("binCapacityX", (_: Array[TypedData], result: TypedData) => result.set(BinCapacityX.value))
object BinCapacityX { var value = 0d }
class BinCapacityY()
    extends Variable("binCapacityY", (_: Array[TypedData], result: TypedData) => result.set(BinCapacityY.value))
object BinCapacityY { var value = 0d }

class FreeCapacityX()
    extends Variable("freeCapacityX", (_: Array[TypedData], result: TypedData) => result.set(FreeCapacityX.value))
object FreeCapacityX { var value = 0d }
class FreeCapacityY()
    extends Variable("freeCapacityY", (_: Array[TypedData], result: TypedData) => result.set(FreeCapacityY.value))
object FreeCapacityY { var value = 0d }

class ItemSizeX() extends Variable("itemSizeX", (_: Array[TypedData], result: TypedData) => result.set(ItemSizeX.value))
object ItemSizeX { var value = 0d }
class ItemSizeY() extends Variable("itemSizeY", (_: Array[TypedData], result: TypedData) => result.set(ItemSizeY.value))
object ItemSizeY { var value = 0d }
