package ie.nix.bpp.app.lgc

import ie.nix.bpp.app.online.OnlineItem1DPeerSimBinPackingApp
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.{Problem, ProblemResult, ProblemResultSet}
import org.apache.logging.log4j.scala.Logging
import peersim.cdsim.CDSimulator
import peersim.edsim.EDSimulator

object LGCItem1DBinPackerApp extends OnlineItem1DPeerSimBinPackingApp with Logging {

  var problemResults = Vector[ProblemResult[Size1D, Item1D]]()

  def addProblemResult(problemResult: ProblemResult[Size1D, Item1D]): Unit =
    problemResults = problemResults :+ problemResult

//  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      gcBinPackerProperties ++
      lgcBinPackerProperties ++
      qLearningProperties ++
//      qdLambdaLearningProperties ++
      freshStartProperties

  override def packProblems(): Unit = {
    logger info s"Packing ${problemSet.problems.size} problems"
    beforePacking(problemSet, resultsSetName)

    if (CDSimulator.isConfigurationCycleDriven)
      CDSimulator.nextExperiment()
    else if (EDSimulator.isConfigurationEventDriven)
      EDSimulator.nextExperiment()

    afterPacking(problemSet, ProblemResultSet(resultsSetName, problemSet, problemResults))
  }

  override def initBins(problem: Problem[Size1D, Item1D]): Unit = {
    super.initBins(problem)
    bins.foreach(_.empty())
  }

  override def resultsSetName = "LGCItem1DBinPacker"

  def lgcBinPackerProperties: Array[String] =
    Array[String](
      "src/main/resources/ie/nix/bpp/lgc/LGCBinPacker.properties",
      "../../GossipContracts/src/main/resources/ie/nix/lgc/LGCAgentAVFCheckpointer.properties",
      "../../GossipContracts/src/main/resources/ie/nix/lgc/LGCAgentPolicyCheckpointer.properties",
      "../../GossipContracts/src/main/resources/ie/nix/lgc/LGCAgentObserver.properties"
    )

  def qLearningProperties: Array[String] =
    Array[String]("../../GossipContracts/src/main/resources/ie/nix/rl/peersim/QLearning.properties")

  def qdLambdaLearningProperties: Array[String] =
    Array[String]("../../GossipContracts/src/main/resources/ie/nix/rl/peersim/QDLambdaLearning.properties")

  def freshStartProperties: Array[String] =
    Array[String](
      "control.policy_checkpointer.read=false",
      "control.avf_checkpointer.read=false",
      "control.policy_observer.append=false",
      "control.avf_observer.append=false"
    )

}
