package ie.nix.bpp.packer.gc

import ie.nix.bpp.Item
import ie.nix.bpp.size.Size1D
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

abstract class GenericGC1DBinPacker[I <: Item[Size1D]](prefix: String)
    extends GenericGCBinPacker[Size1D, I](prefix)
    with Logging {

  override def getGuide(node: Node, item: I): Double =
    bin(node).freeCapacity.size.toDouble

  override def tenderToDouble(tenderMessage: TenderMessage[I]): Double = {
    val myBin = bin(tenderMessage.to)
    val item = tenderMessage.task
    if (myBin.canFit(item)) {
      -item.size.size.toDouble // it can fit, so the bigger the item being tendered, that fits, the better!
    } else {
      Double.PositiveInfinity
    }
  }

  override def getOffer(tenderMessage: TenderMessage[I], proposal: Void): Double =
    bin(tenderMessage.to).freeCapacity.size.toDouble

  override def shouldAward(bidMessage: BidMessage[I, Void]): Boolean = {
    val myBin = bin(bidMessage.to)
    bidMessage.offer <= myBin.freeCapacity.size
  }

}
