package ie.nix.bpp.packer.egc.item2d

import ie.nix.bpp.Queue
import ie.nix.bpp.app.online.OnlineItem2DPeerSimBinPackingApp
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.packer.egc.GenericEvolved2DBinPacker
import ie.nix.bpp.packer.gc.GenericGC2DBinPacker
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

object DemoEvolvedItem2DBinPackerApp extends OnlineItem2DPeerSimBinPackingApp with Logging {

  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties",
                    "network.node=ie.nix.bpp.bin.Item2DPeerSimBin",
                    "protocol.agent=DemoEvolvedItem2DBinPacker")

  override def resultsSetName = "DemoEvolvedItem2DBinPacker"

}

class DemoEvolvedItem2DBinPacker(prefix: String)
    extends GenericGC2DBinPacker[Item2D](prefix)
    with GenericEvolved2DBinPacker[Item2D]
    with Logging {

  override def queue: Queue[Size2D, Item2D] = DemoEvolvedItem2DBinPackerApp.queue

  override def shouldTender(freeCapacityX: Double,
                            freeCapacityY: Double,
                            itemSizeX: Double,
                            itemSizeY: Double): Boolean = true

  override def getGuide(freeCapacityX: Double, freeCapacityY: Double, itemSizeX: Double, itemSizeY: Double): Double =
    freeCapacityY * freeCapacityY

  override def tenderToDouble(guide: Double,
                              freeCapacityX: Double,
                              freeCapacityY: Double,
                              itemSizeX: Double,
                              itemSizeY: Double): Double =
    ifd(freeCapacityX > itemSizeX && freeCapacityY > itemSizeY, -(itemSizeX * itemSizeY), Double.PositiveInfinity)

  override def shouldBid(guide: Double,
                         freeCapacityX: Double,
                         freeCapacityY: Double,
                         itemSizeX: Double,
                         itemSizeY: Double): Boolean = true
  override def getOffer(guide: Double,
                        freeCapacityX: Double,
                        freeCapacityY: Double,
                        itemSizeX: Double,
                        itemSizeY: Double): Double = freeCapacityY * freeCapacityY
  override def bidToDouble(offer: Double,
                           freeCapacityX: Double,
                           freeCapacityY: Double,
                           itemSizeX: Double,
                           itemSizeY: Double): Double = offer
  override def shouldAward(offer: Double,
                           freeCapacityX: Double,
                           freeCapacityY: Double,
                           itemSizeX: Double,
                           itemSizeY: Double): Boolean = true
}
