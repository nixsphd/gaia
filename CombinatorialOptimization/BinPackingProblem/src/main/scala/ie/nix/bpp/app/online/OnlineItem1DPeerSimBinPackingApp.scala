package ie.nix.bpp.app.online

import ie.nix.bpp.Size
import ie.nix.bpp.bin.Item1DPeerSimBinCapacity
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D

trait OnlineItem1DPeerSimBinPackingApp extends GenericOnlinePeerSimBinPackingApp[Size1D, Item1D] {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
  override implicit def toItem: String => Item1D = Item1D.toItem[Size1D]

  override def setBinCapacity(capacity: Size1D): Unit =
    Item1DPeerSimBinCapacity.capacity(capacity)
}
