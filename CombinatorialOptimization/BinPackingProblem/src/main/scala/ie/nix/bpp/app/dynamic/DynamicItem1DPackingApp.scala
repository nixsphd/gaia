package ie.nix.bpp.app.dynamic

import ie.nix.bpp.Size
import ie.nix.bpp.bin.Item1DPeerSimBinCapacity
import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D

trait DynamicItem1DPackingApp extends GenericDynamicItemBinPackingApp[Size1D, DynamicItem1D] {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
  override implicit def toItem: String => DynamicItem1D = DynamicItem1D.toDynamicItem1D

  override def setBinCapacity(capacity: Size1D): Unit =
    Item1DPeerSimBinCapacity.capacity(capacity)

  override val binObserverName = "DynamicItem1DBinObserver"
  override val integrityObserverName = "DynamicItem1DIntegrityObserver"

}
