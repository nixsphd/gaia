package ie.nix.bpp.app.offline

import ie.nix.bpp.app.GenericPeerSimBinPackingApp
import ie.nix.bpp.queue.OfflineQueue
import ie.nix.bpp.{Item, Problem, Queue}

import scala.sys.exit

trait GenericOfflinePeerSimBinPackingApp[S, I <: Item[S]] extends GenericPeerSimBinPackingApp[S, I] {

  override def initQueue(problem: Problem[S, I]): Unit =
    registerQueue(OfflineQueue[S, I](problem.dataSetFileName, problem.queueNumber))

  override def queue: Queue[S, I]

  def onlineQueue: OfflineQueue[S, I] =
    queue match {
      case queue: OfflineQueue[S, I] => queue
      case _ =>
        logger error s"FATAL: The registeredQueue, $registeredQueue, was not an OfflineQueue"
        exit
    }

  override def initBins(problem: Problem[S, I]): Unit = {
    super[GenericPeerSimBinPackingApp].initBins(problem)
    setBinCapacity(problem.binCapacity)
  }

}
