package ie.nix.bpp.packer

import ie.nix.bpp.{Bin, Item, Queue}

trait GenericSimplePacker[S, I <: Item[S]] {

  override def toString: String = s"${getClass.getSimpleName}"

  def pack(queue: Queue[S, I], bins: Vector[Bin[S]]): Unit
}
