package ie.nix.bpp.app.offline

import ie.nix.bpp.app.GenericSimpleBinPackingApp
import ie.nix.bpp.queue.OfflineQueue
import ie.nix.bpp.{Item, Problem}

trait GenericOfflinePackingApp[S, I <: Item[S]] extends GenericSimpleBinPackingApp[S, I] {

  def initQueue(problem: Problem[S, I]): Unit =
    queue = OfflineQueue[S, I](problem.dataSetFileName, problem.queueNumber)

  def initBins(problem: Problem[S, I]): Unit

}
