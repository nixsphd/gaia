package ie.nix.bpp.packer.dynamicitem1d

import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.packer.GenericFirstFitBinPacker
import ie.nix.bpp.size.Size1D

class FirstFitDynamicItem1DBinPacker(prefix: String)
    extends GenericFirstFitBinPacker[Size1D, DynamicItem1D]
    with SimpleDynamicItem1DBinPacker
