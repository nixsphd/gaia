package ie.nix.bpp.packer.dynamicitem2d

import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.packer.GenericFirstFitBinPacker
import ie.nix.bpp.size.Size2D

class FirstFitDynamicItem2DBinPacker(prefix: String)
    extends GenericFirstFitBinPacker[Size2D, DynamicItem2D]
    with SimpleDynamicItem2DBinPacker
