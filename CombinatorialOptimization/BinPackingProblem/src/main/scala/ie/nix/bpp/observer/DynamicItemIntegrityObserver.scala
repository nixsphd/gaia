package ie.nix.bpp.observer

import ie.nix.bpp.{Bin, DynamicItem}
import ie.nix.peersim.Utils.nodesAs
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

class DynamicItemIntegrityObserver[S, I <: DynamicItem[S]](prefix: String)
    extends ie.nix.peersim.Observer(prefix)
    with Logging {

  implicit def toBinlike: peersim.core.Node => Bin[S] = node => as[Bin[S]](node)

  private var _maxOverloadedBins: Int = 0

  override def observe: Unit =
    if (overloadedBins > _maxOverloadedBins)
      _maxOverloadedBins = overloadedBins

  def overloadedBins: Int = nodesAs[Bin[S]].filter(_.isOverloaded).size
  def maxOverloadedBins: Int = _maxOverloadedBins

}
