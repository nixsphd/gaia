package ie.nix.bpp.packer

import ie.nix.bpp._
import ie.nix.bpp.queue.OfflineQueue
import org.apache.logging.log4j.scala.Logging

trait GenericBestFitDecreasingPacker[S, I <: Item[S]] extends GenericBestFitPacker[S, I] with Logging {

  implicit def sizeLike: Size[S]

  override def pack(queue: Queue[S, I], bins: Vector[Bin[S]]): Unit =
    queue match {
      case offlineQueue: OfflineQueue[S, I] => packOfflineQueue(offlineQueue, bins)
      case _                                =>
    }

  def packOfflineQueue(queue: OfflineQueue[S, I], bins: Vector[Bin[S]]): Unit =
    queue.allItems
      .sortBy(_.size)
      .reverse
      .foreach(item => {
        val itemPacked = packItem(bins, item)
        if (itemPacked) queue.poll
      })
}
