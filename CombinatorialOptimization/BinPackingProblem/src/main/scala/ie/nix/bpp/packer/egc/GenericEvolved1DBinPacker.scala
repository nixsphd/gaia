package ie.nix.bpp.packer.egc

import ie.nix.bpp.Item
import ie.nix.bpp.packer.gc.GenericGC1DBinPacker
import ie.nix.bpp.size.Size1D
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

trait GenericEvolved1DBinPacker[I <: Item[Size1D]] extends Logging {
  gd1DBinPacker: GenericGC1DBinPacker[I] =>

  def rand(): Double = Random.nextDouble()
  def ifb(b: Boolean, x: Boolean, y: Boolean): Boolean = if (b) x else y
  def ifd(b: Boolean, x: Double, y: Double): Double = if (b) x else y
  def positiveInfinity: Double = Double.PositiveInfinity
  def negativeInfinity: Double = Double.NegativeInfinity

  override def shouldTender(node: Node): Boolean =
    hasTask &&
      shouldTender(bin(node).freeCapacity.size.toDouble, getTask.size.size.toDouble)

  override def getGuide(node: Node, task: I): Double =
    getGuide(bin(node).freeCapacity.size.toDouble, getTask.size.size.toDouble)

  override def tenderToDouble(tenderMessage: TenderMessage[I]): Double =
    tenderToDouble(tenderMessage.guide,
                   bin(tenderMessage.to).freeCapacity.size.toDouble,
                   tenderMessage.task.size.size.toDouble)

  override def shouldBid(tenderMessage: TenderMessage[I]): Boolean =
    doesNotHaveATask &&
      bin(tenderMessage.to).canFit(tenderMessage.task) &&
      shouldBid(tenderMessage.guide,
                bin(tenderMessage.to).freeCapacity.size.toDouble,
                tenderMessage.task.size.size.toDouble)

  override def getOffer(tenderMessage: TenderMessage[I], proposal: Void): Double =
    getOffer(tenderMessage.guide,
             bin(tenderMessage.to).freeCapacity.size.toDouble,
             tenderMessage.task.size.size.toDouble)

  override def bidToDouble(bidMessage: BidMessage[I, Void]): Double =
    bidToDouble(bidMessage.offer, bin(bidMessage.to).freeCapacity.size.toDouble, bidMessage.task.size.size.toDouble)

  override def shouldAward(bidMessage: BidMessage[I, Void]): Boolean =
    shouldAward(bidMessage.offer, bin(bidMessage.to).freeCapacity.size.toDouble, bidMessage.task.size.size.toDouble)

  def shouldTender(freeCapacity: Double, itemSize: Double): Boolean
  def getGuide(freeCapacity: Double, itemSize: Double): Double
  def tenderToDouble(guide: Double, freeCapacity: Double, itemSize: Double): Double
  def shouldBid(guide: Double, freeCapacity: Double, itemSize: Double): Boolean
  def getOffer(guide: Double, freeCapacity: Double, itemSize: Double): Double
  def bidToDouble(offer: Double, freeCapacity: Double, itemSize: Double): Double
  def shouldAward(offer: Double, freeCapacity: Double, itemSize: Double): Boolean

}

case class RichDouble(x: Double) {
  def div(y: Double): Double = if (y != 0d) x / y else 0d
  def min(y: Double): Double = if (x <= y) x else y
  def max(y: Double): Double = if (x >= y) x else y
}
object RichDouble {
  implicit val doubleToRichDouble: Double => RichDouble = (x) => RichDouble(x)
}
