package ie.nix.bpp.packer.dynamicitem2d

import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.packer.GenericFirstFitDecreasingBinPacker
import ie.nix.bpp.size.Size2D

class FirstFitDecreasingDynamicItem2DBinPacker(prefix: String)
    extends GenericFirstFitDecreasingBinPacker[Size2D, DynamicItem2D]
    with SimpleDynamicItem2DBinPacker
