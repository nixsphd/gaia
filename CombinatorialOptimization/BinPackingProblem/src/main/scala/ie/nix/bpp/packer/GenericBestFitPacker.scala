package ie.nix.bpp.packer

import ie.nix.bpp.queue.OnlineQueue
import ie.nix.bpp.{Bin, Item, Queue, Size}
import org.apache.logging.log4j.scala.Logging

import scala.annotation.tailrec

trait GenericBestFitPacker[S, I <: Item[S]] extends GenericSimplePacker[S, I] with Logging {

  implicit def sizeLike: Size[S]

  override def pack(queue: Queue[S, I], bins: Vector[Bin[S]]): Unit = {
    queue match {
      case onlineQueue: OnlineQueue[S, I] => packOnlineQueue(bins, onlineQueue)
      case _                              =>
    }
  }

  @tailrec
  final def packOnlineQueue(bins: Vector[Bin[S]], queue: OnlineQueue[S, I]): Unit =
    queue.peek match {
      case Some(item) =>
        val itemPacked = packItem(bins, item)
        if (itemPacked)
          queue.poll
        packOnlineQueue(bins, queue)
      case _ =>
    }

  def packItem(bins: Vector[Bin[S]], item: I): Boolean = {
    getBestBin(bins, item) match {
      case Some(bestBin) => {
        bestBin.addItem(item)
        true
      }
      case None => false // Do nothing
    }
  }

  def getBestBin(bins: Vector[Bin[S]], item: I): Option[Bin[S]] = {
    val possibleBins = bins.filter(_.canFit(item))
    if (possibleBins.nonEmpty) {
      Some(possibleBins.minBy(_.freeCapacity))
    } else {
      logger error s"item=$item can not fit in any bin, " +
        s"bins=${bins.sortBy(_.freeCapacity)}"
      None
    }
  }

}
