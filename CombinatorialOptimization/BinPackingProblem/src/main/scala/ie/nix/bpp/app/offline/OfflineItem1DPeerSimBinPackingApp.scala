package ie.nix.bpp.app.offline

import ie.nix.bpp.Size
import ie.nix.bpp.bin.Item1DPeerSimBinCapacity
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D

trait OfflineItem1DPeerSimBinPackingApp extends GenericOfflinePeerSimBinPackingApp[Size1D, Item1D] {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
  override implicit def toItem: String => Item1D = Item1D.toItem[Size1D]

  override def setBinCapacity(capacity: Size1D): Unit = Item1DPeerSimBinCapacity.capacity(capacity)
}
