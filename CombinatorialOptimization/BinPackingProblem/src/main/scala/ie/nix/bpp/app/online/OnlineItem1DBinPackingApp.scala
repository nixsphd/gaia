package ie.nix.bpp.app.online

import ie.nix.bpp.bin.Item1DBin
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.{Problem, Size}

trait OnlineItem1DBinPackingApp extends GenericOnlinePackingApp[Size1D, Item1D] {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
  override implicit def toItem: String => Item1D = Item1D.toItem[Size1D]

  def initBins(problem: Problem[Size1D, Item1D]): Unit =
    bins = Vector.fill(problem.numberOfBins)(Item1DBin(problem.binCapacity))
}
