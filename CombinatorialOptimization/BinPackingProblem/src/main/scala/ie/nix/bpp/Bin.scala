package ie.nix.bpp

import org.apache.logging.log4j.scala.Logging

trait Bin[S] extends Logging {

  implicit def sizeLike: Size[S]

  protected var _items: Vector[Item[S]] = Vector()
  protected var _usedCapacity: S = sizeLike.zero
  logger trace s"capacity=$capacity, numberOfItems=$numberOfItems, usedCapacity=$usedCapacity"

  override def toString: String = "Bin[" + freeCapacity + "/" + capacity + "]"

  def capacity: S
  def usedCapacity: S = _usedCapacity
  def freeCapacity: S = sizeLike.minus(capacity, usedCapacity)
  def canFit(item: Item[S]): Boolean = sizeLike.isPositive(sizeLike.minus(freeCapacity, item.size))

  def empty(): Unit = {
    _items = Vector()
    _usedCapacity = sizeLike.zero
  }

  def addItem(item: Item[S]): Unit = {
    _items = _items :+ item
    _usedCapacity = sizeLike.plus(_usedCapacity, item.size)
  }
  def removeItem(item: Item[S]): Unit = {
    _items = _items.filterNot(_ == item)
    _usedCapacity = sizeLike.minus(_usedCapacity, item.size)
  }

  def isEmpty: Boolean = numberOfItems == 0
  def isOverloaded: Boolean = !sizeLike.isPositive(freeCapacity)
  def numberOfItems: Int = _items.size
  def getItems: List[Item[S]] = _items.toList
  def hasItem(item: Item[S]): Boolean = _items.contains(item)

}
