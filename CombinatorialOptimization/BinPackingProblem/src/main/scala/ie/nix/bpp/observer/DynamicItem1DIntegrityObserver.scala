package ie.nix.bpp.observer

import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.size.Size1D

class DynamicItem1DIntegrityObserver(prefix: String) extends DynamicItemIntegrityObserver[Size1D, DynamicItem1D](prefix)
