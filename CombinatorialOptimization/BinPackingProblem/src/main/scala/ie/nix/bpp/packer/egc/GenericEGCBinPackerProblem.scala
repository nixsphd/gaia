package ie.nix.bpp.packer.egc

import ec.EvolutionState
import ec.util.Parameter
import ie.nix.bpp.app.GenericPeerSimBinPackingApp
import ie.nix.bpp.dataset.DataSet.{DATASETS_DIR, RESOURCES_DIR}
import ie.nix.bpp.packer.egc.GenericEGCBinPackerProblem.{P_BIN_CAPACITY, P_BPP_FILE, P_NUMBER_OF_BINS}
import ie.nix.bpp.{Item, Problem, ProblemResult, ProblemSet, Queue}
import ie.nix.egc.EGCProblem
import org.apache.logging.log4j.scala.Logging
import peersim.cdsim.CDSimulator
import peersim.edsim.EDSimulator

abstract class GenericEGCBinPackerProblem[S, I <: Item[S]]
    extends EGCProblem
    with GenericPeerSimBinPackingApp[S, I]
    with Logging {

  override def resultsSetName: String = "EGCBinPackerProblem"
  override def properties: Array[String] = super[EGCProblem].properties

  protected var theProblemSet: ProblemSet[S, I] = _

  override def setup(state: EvolutionState, base: Parameter): Unit = {
    super.setup(state, base) // very important, remember this
    theProblemSet = ProblemSet[S, I](dataSetDirName(state, base),
                                     dataSetFileName(state, base),
                                     numberOfBins(state, base),
                                     binCapacity(state, base))
  }

  protected def dataSetDirName(state: EvolutionState, base: Parameter): String = s"$RESOURCES_DIR/$DATASETS_DIR"

  protected def dataSetFileName(state: EvolutionState, base: Parameter): String = {
    val dataSetFileName =
      state.parameters.getStringWithDefault(base.push(P_BPP_FILE), defaultBase.push(P_BPP_FILE), null)
    logger debug s"dataSetFileName=${dataSetFileName}"
    assert(dataSetFileName != null, "Specify BPP file" + P_BPP_FILE + ".")
    dataSetFileName
  }

  protected def numberOfBins(state: EvolutionState, base: Parameter): Int = {
    val numberOfBins =
      state.parameters.getIntWithDefault(base.push(P_NUMBER_OF_BINS), defaultBase.push(P_NUMBER_OF_BINS), 0)
    logger debug s"numberOfBins=$numberOfBins"
    assert((numberOfBins != 0), "Number of bins must be greater than 0, specify " + P_NUMBER_OF_BINS + ".")
    numberOfBins
  }

  protected def binCapacity(state: EvolutionState, base: Parameter): S = {
    val binCapacityAsString =
      state.parameters.getString(base.push(P_BIN_CAPACITY), defaultBase.push(P_BIN_CAPACITY))
    logger debug s"binCapacityAsString=$binCapacityAsString"
    assert((binCapacityAsString != ""), "Bin capacity must be greater than 0, specify " + P_BIN_CAPACITY + ".")
    sizeLike.toSize(binCapacityAsString)
  }

  override protected def initSimulation(): Unit = {
    initConfiguration()
    initRandom()
    initQueue(problem)
    initBins(problem)
  }

  override protected def runSimulation(): Unit = {
    if (CDSimulator.isConfigurationCycleDriven)
      CDSimulator.nextExperiment()
    else if (EDSimulator.isConfigurationEventDriven)
      EDSimulator.nextExperiment()
  }

  override protected def evaluateSimulation: Double = {
    val problemResult = ProblemResult(queue, bins)
    val numberOfOpenBins = problemResult.numberOfOpenBins
    val numberOfItemsQueued = problemResult.numberOfItemsQueued
    val numberOfOverloadedBins = problemResult.numberOfOverloadedBins
    val evaluation = queue.numberOfItemsQueued + numberOfOpenBins
    recordStat(gpIndividual, "openBins", numberOfOpenBins.toDouble)
    recordStat(gpIndividual, "numberOfItemsQueued", numberOfItemsQueued.toDouble)
    recordStat(gpIndividual, "evaluation", evaluation.toDouble)
    recordStat(gpIndividual, "overloadedBins", numberOfOverloadedBins.toDouble)
    logger debug s"generation=${state.generation}, gpIndividual=${gpIndividual.hashCode}, evaluation=$evaluation, " +
      s"numberOfItemsQueued=${numberOfItemsQueued}, numberOfOpenBins=$numberOfOpenBins, " +
      s"numberOfOverloadedBins=$numberOfOverloadedBins"
    evaluation.toDouble
  }

  def problem: Problem[S, I] = theProblemSet.problems(run.toInt)

  override def initQueue(problem: Problem[S, I]): Unit

  override def queue: Queue[S, I]
}

object GenericEGCBinPackerProblem {
  protected val P_MODEL_PROPERTIES: String = "model_properties"
  protected val P_BPP_FILE: String = "bpp_file"
  protected val P_BIN_CAPACITY: String = "bin_capacity"
  protected val P_NUMBER_OF_BINS: String = "number_of_bins"
}
