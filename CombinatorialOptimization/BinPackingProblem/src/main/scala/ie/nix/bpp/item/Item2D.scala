package ie.nix.bpp.item

import ie.nix.bpp.size.Size2D
import ie.nix.bpp.{Item, Size}

class Item2D(val size: Size2D)(implicit val sizeLike: Size[Size2D]) extends Item[Size2D] {

  override def toString: String = s"Item2D[$size]"

}

object Item2D {

  implicit def toItem[S: Size]: String => Item2D =
    itemAsString => new Item2D(implicitly[Size[Size2D]].toSize(itemAsString))

  def apply(size1AsInt: Int, size2AsInt: Int): Item2D =
    new Item2D(Size2D(size1AsInt, size2AsInt))

}
