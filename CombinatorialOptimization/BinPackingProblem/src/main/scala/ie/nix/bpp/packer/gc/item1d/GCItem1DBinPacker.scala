package ie.nix.bpp.packer.gc.item1d

import ie.nix.bpp.Queue
import ie.nix.bpp.app.gc.GCItem1DBinPackerApp
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.gc.GenericGC1DBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

class GCItem1DBinPacker(prefix: String) extends GenericGC1DBinPacker[Item1D](prefix) with Logging {

  override def queue: Queue[Size1D, Item1D] = GCItem1DBinPackerApp.queue

}
