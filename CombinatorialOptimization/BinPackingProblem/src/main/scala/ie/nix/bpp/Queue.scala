package ie.nix.bpp

import org.apache.logging.log4j.scala.Logging

trait Queue[S, I <: Item[S]] extends Logging {

  val items: Seq[I]

  protected var queue: scala.collection.immutable.Queue[I] = scala.collection.immutable.Queue[I](items: _*)

  override def toString: String = s"Queue[${items.size}]"

  def getNumberOfItems: Int = queue.size

  def isEmpty: Boolean = queue.nonEmpty

  def numberOfItemsQueued: Int = queue.size

  def peek: Option[I] =
    try {
      Some(queue.head)
    } catch {
      case _: NoSuchElementException => None
    }

  def poll: Option[I] =
    try {
      val (item, newQueue) = queue.dequeue
      queue = newQueue
      Some(item)
    } catch {
      case _: NoSuchElementException => None
    }

}
