package ie.nix.bpp.packer.dynamicitem1d

import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.packer.egc.RichDouble.doubleToRichDouble
import ie.nix.bpp.Queue
import ie.nix.bpp.app.dynamic.DynamicItem1DPackingApp
import ie.nix.bpp.packer.egc.GenericEvolved1DBinPacker
import ie.nix.bpp.packer.gc.GenericGC1DBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

object Exp2EvolvedDynamicItem1DBinPackerApp extends DynamicItem1DPackingApp with Logging {

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties",
                    "protocol.agent=Exp2EvolvedDynamicItem1DBinPacker")

  silent()
  initRandom()
  packProblems()

  override def resultsSetName = "Exp2EvolvedDynamicItem1DBinPacker"

}

class Exp2EvolvedDynamicItem1DBinPacker(prefix: String)
    extends GenericGC1DBinPacker[DynamicItem1D](prefix)
    with GenericEvolved1DBinPacker[DynamicItem1D] {

  override def queue: Queue[Size1D, DynamicItem1D] =
    Exp2EvolvedDynamicItem1DBinPackerApp.queue

  def shouldTender(freeCapacity: Double, itemSize: Double): Boolean = true

  def getGuide(freeCapacity: Double, itemSize: Double): Double = positiveInfinity

  def tenderToDouble(guide: Double, freeCapacity: Double, itemSize: Double): Double = (0.0 div 0.0)

  def shouldBid(guide: Double, freeCapacity: Double, itemSize: Double): Boolean =
    !(ifb(((guide min guide) < (guide + positiveInfinity)),
          ((true || true) && (positiveInfinity > guide)),
          ((freeCapacity == itemSize) && (0.0 > guide))))

  def getOffer(guide: Double, freeCapacity: Double, itemSize: Double): Double = freeCapacity

  def bidToDouble(offer: Double, freeCapacity: Double, itemSize: Double): Double = (offer * offer)

  def shouldAward(offer: Double, freeCapacity: Double, itemSize: Double) =
    (ifd(true, 0.0, positiveInfinity) == (offer min 0.0))

}
