package ie.nix.bpp.dataset

import ie.nix.bpp.{DynamicItem, Size}
import ie.nix.bpp.dataset.DataSet.{generateQueues, saveQueuesToFile}
import ie.nix.bpp.item.{DynamicItem1D, DynamicItem2D}
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.size.{Size1D, Size2D}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

class UniformDynamicDataSets
trait GenericUniformDynamicDataSets[S, I <: DynamicItem[S]]
    extends GenericUniformDataSets[S, I]
    with DynamicDataSet
    with Logging {
  this: App =>

  override val USAGE = "Please pass in one argument structured as follows: " +
    "numberOfQueues numberOfItems minItemSize-maxItemSize-meanArrivalRate-meanDepartureTime binCapacity (seed)"

  var arrivalTimeSampler: CumulativePoissonSampler = _
  var departureTimeSampler: PoissonSampler = _

  override def uniformDataSet: DataSet[S, I] = {
    val queues: Vector[Vector[I]] =
      generateQueues[S, I](numberOfQueues, numberOfItems)
    DataSet[S, I](queues)
  }

  def dbppArgs: Vector[String] = { checkArgs(); args(2).split("-").toVector }
  override def minItemSize: Int = { checkArgs(); dbppArgs(0).toInt }
  override def maxItemSize: Int = { checkArgs(); dbppArgs(1).toInt }
  def meanArrivalTime: Int = { checkArgs(); dbppArgs(2).toInt }
  def meanDepartureTime: Int = { checkArgs(); dbppArgs(3).toInt }
  def seed: Long = { checkArgs(); if (args.length >= 5) args(4).toLong else 0 }

}

object UniformDynamicItem1DDataSets extends App with GenericUniformDynamicDataSets[Size1D, DynamicItem1D] with Logging {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
  override implicit def itemToString: DynamicItem1D => String = DynamicItem.dynamicItemToString[Size1D]
  override implicit def queueGenerator: QueueGenerator[Size1D, DynamicItem1D] =
    UniformDynamicItem1DQueueGenerator(minItemSize, maxItemSize, meanArrivalTime, meanDepartureTime)

  override val uniformDataSetFilename = s"UniformDynamicItem1DDataSets-$numberOfItems-${args(2)}.csv"

  saveQueuesToFile[Size1D, DynamicItem1D](uniformDataSet, uniformDataSetFilename)
  logger info s"Wrote $uniformDataSetFilename"

  override def uniformDataSet: DataSet[Size1D, DynamicItem1D] = {
    val queues: Vector[Vector[DynamicItem1D]] =
      generateQueues[Size1D, DynamicItem1D](numberOfQueues, numberOfItems)
    DataSet[Size1D, DynamicItem1D](queues)
  }

}

case class UniformDynamicItem1DQueueGenerator(minItemSize: Int,
                                              maxItemSize: Int,
                                              meanArrivalTime: Int,
                                              meanDepartureTime: Int)
    extends DynamicQueueGenerator[Size1D, DynamicItem1D] {

  override def generateItems: Vector[DynamicItem1D] = {
    val size = minItemSize + Random.nextInt(maxItemSize - minItemSize)
    val arrivalTime = arrivalTimeSampler.sample
    val departureTime = arrivalTime + departureTimeSampler.sample
    Vector(new DynamicItem1D(Size1D(size), arrivalTime, departureTime))
  }

}

object UniformDynamicItem2DDataSets extends App with GenericUniformDynamicDataSets[Size2D, DynamicItem2D] with Logging {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
  override implicit def itemToString: DynamicItem[Size2D] => String = DynamicItem.dynamicItemToString[Size2D]
  override implicit def queueGenerator: QueueGenerator[Size2D, DynamicItem2D] =
    UniformDynamicItem2DQueueGenerator(minItemSize, maxItemSize, meanArrivalTime, meanDepartureTime)

  override val uniformDataSetFilename = s"UniformDynamicItem2DDataSets-$numberOfItems-${args(2)}.csv"

  saveQueuesToFile[Size2D, DynamicItem2D](uniformDataSet, uniformDataSetFilename)
  logger info s"Wrote $uniformDataSetFilename"

}

case class UniformDynamicItem2DQueueGenerator(minItemSize: Int,
                                              maxItemSize: Int,
                                              meanArrivalTime: Int,
                                              meanDepartureTime: Int)
    extends DynamicQueueGenerator[Size2D, DynamicItem2D] {

  override def generateItems: Vector[DynamicItem2D] = {
    val size1 = minItemSize + Random.nextInt(maxItemSize - minItemSize)
    val size2 = minItemSize + Random.nextInt(maxItemSize - minItemSize)
    val arrivalTime = arrivalTimeSampler.sample
    val departureTime = arrivalTime + departureTimeSampler.sample
    Vector(new DynamicItem2D(Size2D(size1, size2), arrivalTime, departureTime))
  }

}
