package ie.nix.bpp.dataset

import ie.nix.bpp.dataset.DataSet.saveQueuesToFile
import ie.nix.bpp.item.{Item1D, Item2D}
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.size.{Size1D, Size2D}
import ie.nix.bpp.{Item, Size}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

trait GenericTripletDataSets[S, I <: Item[S]] extends Logging {
  this: App =>

  implicit def sizeLike: Size[S]
  implicit def itemToString: I => String
  implicit def queueGenerator: QueueGenerator[S, I]

  val USAGE = "Please pass in one argument structured as follows: numberOfQueues " +
    "numberOfItems minFirstTriplesItemSize-maxFirstTriplesItemSize-minSecondTriplesItemSize binCapacity"

  def tripletDataSet: DataSet[S, I] = DataSet[S, I](numberOfQueues, numberOfItems)
  def tripletDataSetFilename = s"TripletItem1DDataSets-$numberOfItems-${args(2)}.csv"

  def checkArgs(): Unit = {
    assert(args.length >= 4, s"There was not enough args; there was ${args.length} but should be 3.\n$USAGE")
    assert(numberOfItems % 3 == 0, s"The number of item must be a factor of 3.\n$USAGE")
  }
  def numberOfQueues: Int = { checkArgs(); args(0).toInt }
  def numberOfItems: Int = { args(1).toInt }
  def bppArgs: Vector[String] = { checkArgs(); args(2).split("-").toVector }
  def minFirstTriplesItemSize: Int = bppArgs(0).toInt
  def maxFirstTriplesItemSize: Int = bppArgs(1).toInt
  def minSecondTriplesItemSize: Int = bppArgs(2).toInt
  def binCapacity: S = { checkArgs(); sizeLike.toSize(args(3)) }

}

object TripletItem1DDataSets extends App with GenericTripletDataSets[Size1D, Item1D] with Logging {

  implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
  implicit def itemToString: Item[Size1D] => String = Item.itemToString[Size1D]
  implicit def queueGenerator: QueueGenerator[Size1D, Item1D] =
    TripletItem1DQueueGenerator(binCapacity, minFirstTriplesItemSize, maxFirstTriplesItemSize, minSecondTriplesItemSize)

  override val tripletDataSetFilename = s"TripletItem1DDataSets-$numberOfItems-${args(2)}.csv"

  saveQueuesToFile(tripletDataSet, tripletDataSetFilename)

}

case class TripletItem1DQueueGenerator(binCapacity: Size1D,
                                       minFirstTriplesItemSize: Int,
                                       maxFirstTriplesItemSize: Int,
                                       minSecondTriplesItemSize: Int)
    extends QueueGenerator[Size1D, Item1D] {

  override def generateItems: Vector[Item1D] = {
    val item1Size = minFirstTriplesItemSize + Random.nextInt(maxFirstTriplesItemSize - minFirstTriplesItemSize)
    val maxSecondTriplesItemSize = (binCapacity.size - item1Size) / 2
    val item2Size = minSecondTriplesItemSize + Random.nextInt(maxSecondTriplesItemSize - minSecondTriplesItemSize)
    val item3Size = binCapacity.size - (item1Size + item2Size)
    Vector[Item1D](Item1D(item1Size), Item1D(item2Size), Item1D(item3Size))
  }

  override def postGenerateQueue(queueNumber: Int, queue: Vector[Item1D]): Vector[Item1D] =
    Random.shuffle(queue)
}

object TripletItem2DDataSets extends App with GenericTripletDataSets[Size2D, Item2D] with Logging {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
  override implicit def itemToString: Item[Size2D] => String = Item.itemToString[Size2D]
  override implicit def queueGenerator: QueueGenerator[Size2D, Item2D] =
    TripletItem2DQueueGenerator(binCapacity, minFirstTriplesItemSize, maxFirstTriplesItemSize, minSecondTriplesItemSize)

  override val tripletDataSetFilename = s"TripletItem2DDataSets-$numberOfItems-${args(2)}.csv"

  saveQueuesToFile(tripletDataSet, tripletDataSetFilename)

}

case class TripletItem2DQueueGenerator(binCapacity: Size2D,
                                       minFirstTriplesItemSize: Int,
                                       maxFirstTriplesItemSize: Int,
                                       minSecondTriplesItemSize: Int)
    extends QueueGenerator[Size2D, Item2D] {

  override def generateItems: Vector[Item2D] = {
    val item1Size1 = minFirstTriplesItemSize + Random.nextInt(maxFirstTriplesItemSize - minFirstTriplesItemSize)
    val maxSecondTriplesItemSize1 = (binCapacity.size1 - item1Size1) / 2
    val item2Size1 = minSecondTriplesItemSize + Random.nextInt(maxSecondTriplesItemSize1 - minSecondTriplesItemSize)
    val item3Size1 = binCapacity.size1 - (item1Size1 + item2Size1)

    val item1Size2 = minFirstTriplesItemSize + Random.nextInt(maxFirstTriplesItemSize - minFirstTriplesItemSize)
    val maxSecondTriplesItemSize2 = (binCapacity.size2 - item1Size2) / 2
    val item2Size2 = minSecondTriplesItemSize + Random.nextInt(maxSecondTriplesItemSize2 - minSecondTriplesItemSize)
    val item3Size2 = binCapacity.size2 - (item1Size2 + item2Size2)

    Vector[Item2D](Item2D(item1Size1, item1Size2), Item2D(item2Size1, item2Size2), Item2D(item3Size1, item3Size2))
//    Vector[Item2D](Item2D(item1Size1, item3Size2), Item2D(item2Size1, item1Size2), Item2D(item3Size1, item2Size2))
  }

  override def postGenerateQueue(queueNumber: Int, queue: Vector[Item2D]): Vector[Item2D] =
    Random.shuffle(queue)

}
