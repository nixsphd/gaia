package ie.nix.bpp.packer.item1d

import ie.nix.bpp.app.offline.OfflineItem1DBinPackingApp
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.GenericFirstFitBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

object FirstFitDecreasingItem1DPackerApp extends OfflineItem1DBinPackingApp with Logging {

  initRandom()
  packProblems()

  override def resultsSetName: String = "FirstFitDecreasingItem1DPackerApp"
  override def initPacker(): Unit =
    packer = FirstFitDecreasingItem1DPacker()

}

case class FirstFitDecreasingItem1DPacker() extends GenericFirstFitBinPacker[Size1D, Item1D]
