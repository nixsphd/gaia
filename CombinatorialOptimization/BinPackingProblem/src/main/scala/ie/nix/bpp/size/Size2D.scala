package ie.nix.bpp.size

import ie.nix.bpp.Size
import org.apache.logging.log4j.scala.Logging

case class Size2D(size1: Int, size2: Int)

object Size2D extends Logging {

  private[Size2D] val spacer = "x"

  implicit object SizeLikeSize2D extends Size[Size2D] {

    override def compare(x: Size2D, y: Size2D): Int = x.size1 compare y.size1

    override def zero: Size2D = Size2D(0, 0)
    override def isPositive(size: Size2D): Boolean =
      size.size1 >= 0 && size.size2 >= 0
    override def plus(oneSize: Size2D, otherSize: Size2D): Size2D =
      Size2D(oneSize.size1 + otherSize.size1, oneSize.size2 + otherSize.size2)
    override def minus(oneSize: Size2D, otherSize: Size2D): Size2D =
      Size2D(oneSize.size1 - otherSize.size1, oneSize.size2 - otherSize.size2)

    override def toString(size: Size2D): String = s"${size.size1}$spacer${size.size2}"
    def toSize(size2DAsString: String): Size2D = {
      val sizes = size2DAsString
        .stripPrefix("(")
        .stripSuffix(")")
        .split(spacer)
      Size2D(sizes(0).toInt, sizes(1).toInt)
    }
  }

}
