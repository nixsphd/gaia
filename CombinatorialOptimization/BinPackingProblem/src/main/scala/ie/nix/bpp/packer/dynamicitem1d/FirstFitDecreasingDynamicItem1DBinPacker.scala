package ie.nix.bpp.packer.dynamicitem1d

import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.packer.GenericFirstFitDecreasingBinPacker
import ie.nix.bpp.size.Size1D

class FirstFitDecreasingDynamicItem1DBinPacker(prefix: String)
    extends GenericFirstFitDecreasingBinPacker[Size1D, DynamicItem1D]
    with SimpleDynamicItem1DBinPacker {}
