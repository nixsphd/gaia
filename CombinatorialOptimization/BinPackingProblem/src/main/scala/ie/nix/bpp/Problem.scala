package ie.nix.bpp

import ie.nix.bpp.dataset.DataSet

case class Problem[S, I <: Item[S]](dataSetFileName: String, queueNumber: Int, numberOfBins: Int, binCapacity: S)(
    implicit sizeLike: Size[S],
    toItem: String => I) {
  def numberOfItemsQueued: Int = DataSet[S, I](dataSetFileName).itemsForQueue(queueNumber).size
}
