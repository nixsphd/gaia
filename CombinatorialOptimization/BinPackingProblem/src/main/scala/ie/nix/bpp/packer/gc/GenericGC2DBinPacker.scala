package ie.nix.bpp.packer.gc

import ie.nix.bpp.Item
import ie.nix.bpp.size.Size2D
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

abstract class GenericGC2DBinPacker[I <: Item[Size2D]](prefix: String)
    extends GenericGCBinPacker[Size2D, I](prefix)
    with Logging {

  def rand(): Double = Random.nextDouble()
  def ifb(b: Boolean, x: Boolean, y: Boolean): Boolean = if (b) x else y
  def ifd(b: Boolean, x: Double, y: Double): Double = if (b) x else y
  def positiveInfinity: Double = Double.PositiveInfinity

  override def getGuide(node: Node, item: I): Double =
    binFreeCapacityArea(node)

  override def tenderToDouble(tenderMessage: TenderMessage[I]): Double = {
    val myBin = bin(tenderMessage.to)
    val item = tenderMessage.task
    if (myBin.canFit(item)) {
      -(itemArea(item)) // it can fit, so the bigger the item being tendered, that fits, the better!
    } else {
      Double.PositiveInfinity
    }
  }

  override def getOffer(tenderMessage: TenderMessage[I], proposal: Void): Double =
    binFreeCapacityArea(tenderMessage.to)

  override def shouldAward(bidMessage: BidMessage[I, Void]): Boolean = {
    val myBin = bin(bidMessage.to)
    bidMessage.offer <= binFreeCapacityArea(myBin)
  }

  def binFreeCapacityArea(node: Node): Double =
    bin(node).freeCapacity.size1.toDouble * bin(node).freeCapacity.size2.toDouble

  def itemArea(item: I): Double = item.size.size1.toDouble * item.size.size2.toDouble

}
