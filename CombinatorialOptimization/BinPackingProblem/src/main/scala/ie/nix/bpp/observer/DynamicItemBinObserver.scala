package ie.nix.bpp.observer

import ie.nix.bpp.{Bin, DynamicItem}
import ie.nix.peersim.Utils.nodesAs
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

abstract class DynamicItemBinObserver[S, I <: DynamicItem[S]](prefix: String)
    extends ie.nix.peersim.Observer(prefix)
    with Logging {

  implicit def toBinlike: peersim.core.Node => Bin[S] = node => as[Bin[S]](node)

  private var _maxOpenBins: Int = 0

  override def observe: Unit =
    if (openBins > _maxOpenBins)
      _maxOpenBins = openBins

  def openBins: Int = nodesAs[Bin[S]].filterNot(_.isEmpty).size
  def maxOpenBins: Int = _maxOpenBins

}
