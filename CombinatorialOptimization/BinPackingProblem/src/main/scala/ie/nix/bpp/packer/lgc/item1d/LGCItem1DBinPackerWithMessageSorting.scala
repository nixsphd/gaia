package ie.nix.bpp.packer.lgc.item1d

import ie.nix.bpp.item.Item1D
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.lgc.MessageSorting
import ie.nix.peersim.Node
import ie.nix.rl.Environment.State
import org.apache.logging.log4j.scala.Logging

class LGCItem1DBinPackerWithMessageSorting(prefix: String)
    extends LGCItem1DBinPacker(prefix)
    with MessageSorting[Item1D, Void]
    with Logging {

  override def getGuide(node: Node, item: Item1D): Double = Double.NaN

  override def getOffer(tenderMessage: TenderMessage[Item1D], proposal: Void): Double =
    Double.NaN

  override protected def defaultSortTenderMessages(
      tenderMessages: Vector[TenderMessage[Item1D]]): Vector[(TenderMessage[Item1D], Double)] =
    super[LGCItem1DBinPacker].sortTenderMessages(tenderMessages)

  override protected def defaultSortBidMessages(
      bidMessages: Vector[BidMessage[Item1D, Void]]): Vector[(BidMessage[Item1D, Void], Double)] =
    super[LGCItem1DBinPacker].sortBidMessages(bidMessages)

  override def mapToStateForTenderToDouble(tenderMessage: TenderMessage[Item1D]): State =
    mapToState(tenderMessage.to, tenderMessage.task)

  override def mapToStateForBidToDouble(bidMessage: BidMessage[Item1D, Void]): State =
    mapToState(bidMessage.to, bidMessage.task)

}
