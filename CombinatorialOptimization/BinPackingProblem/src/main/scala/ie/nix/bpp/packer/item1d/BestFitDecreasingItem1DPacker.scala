package ie.nix.bpp.packer.item1d

import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.GenericBestFitDecreasingPacker
import ie.nix.bpp.Size
import ie.nix.bpp.app.offline.OfflineItem1DBinPackingApp
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

object BestFitDecreasingItem1DPackerApp extends OfflineItem1DBinPackingApp with Logging {

  override implicit def toItem: String => Item1D = Item1D.toItem

  initRandom()
  packProblems()

  override def resultsSetName = "BestFitDecreasingItem1DPacker"
  override def initPacker(): Unit = packer = new BestFitDecreasingItem1DPacker()
}

class BestFitDecreasingItem1DPacker()
    extends BestFitItem1DPacker
    with GenericBestFitDecreasingPacker[Size1D, Item1D]
    with Logging {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D

}
