package ie.nix.bpp.item

import ie.nix.bpp.size.Size1D
import ie.nix.bpp.{DynamicItem, Size}
import org.apache.logging.log4j.scala.Logging

class DynamicItem1D(val size: Size1D, val arrivalTime: Long, val departureTime: Long)(
    implicit val sizeLike: Size[Size1D])
    extends DynamicItem[Size1D]

object DynamicItem1D extends Logging {

  implicit def toDynamicItem1D: String => DynamicItem1D =
    rowValue => {
      val dynamicItemData = rowValue
        .stripPrefix("(")
        .stripSuffix(")")
        .split(" ")
      apply(dynamicItemData(0), dynamicItemData(1).toLong, dynamicItemData(2).toLong)
    }

  def apply(sizeAsString: String, arrivalTime: Long, departureTime: Long)(
      implicit sizeLike: Size[Size1D]): DynamicItem1D =
    new DynamicItem1D(sizeLike.toSize(sizeAsString), arrivalTime, departureTime)

}
