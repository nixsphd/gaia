package ie.nix.bpp.decorator

import ie.nix.bpp.{Item, Problem, ProblemResult}
import org.apache.logging.log4j.scala.Logging

case class DynamicItemIntegrity[S, I <: Item[S]]() extends BinPackingDecorator[S, I] with Logging {

  override def afterPacking(problem: Problem[S, I], problemResult: ProblemResult[S, I]): Unit = {
    val hasOverloadedBins = problemResult.numberOfOverloadedBins > 0
    if (hasOverloadedBins)
      logger error s"numberOfOverloadedBins=${problemResult.numberOfOverloadedBins}"
    else
      logger debug s"hasOverloadedBins=$hasOverloadedBins"
  }

}
