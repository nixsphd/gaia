package ie.nix.bpp.packer.gc.dynamicitem1d

import ie.nix.bpp.Queue
import ie.nix.bpp.app.gc.GCDynamicItem1DBinPackerApp
import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.packer.gc.GenericGC1DBinPacker
import ie.nix.bpp.app.gc.GCDynamicItem1DBinPackerApp.dynamicQueue
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

class GCDynamicItem1DBinPacker(prefix: String) extends GenericGC1DBinPacker[DynamicItem1D](prefix) with Logging {

  override def queue: Queue[Size1D, DynamicItem1D] =
    GCDynamicItem1DBinPackerApp.queue

  protected def dynamicQueuePeekItem: DynamicItem1D = dynamicQueue.peek.get
  protected def dynamicQueueHasItem: Boolean = dynamicQueue.peek.isDefined

}
