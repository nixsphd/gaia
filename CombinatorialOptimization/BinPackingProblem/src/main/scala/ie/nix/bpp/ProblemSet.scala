package ie.nix.bpp

import ie.nix.bpp.dataset.DataSet

case class ProblemSet[S, I <: Item[S]](name: String, problems: Vector[Problem[S, I]])

object ProblemSet {

  def apply[S, I <: Item[S]](dataSetFileName: String, numberOfBins: Int, binCapacity: S)(
      implicit sizeLike: Size[S],
      toItem: String => I): ProblemSet[S, I] = {
    val dataSet = DataSet[S, I](dataSetFileName)
    val problems =
      (0 until dataSet.numberOfQueues)
        .map(Problem(dataSetFileName, _, numberOfBins, binCapacity))
        .toVector
    val dataSetName: String = dataSetFileName.dropRight(".csv".length)
    ProblemSet[S, I](dataSetName, problems)
  }

  def apply[S, I <: Item[S]](dataSetDirName: String, dataSetFileName: String, numberOfBins: Int, binCapacity: S)(
      implicit sizeLike: Size[S],
      toItem: String => I): ProblemSet[S, I] = {
    val dataSet = DataSet[S, I](dataSetDirName, dataSetFileName)
    val problems =
      (0 until dataSet.numberOfQueues)
        .map(Problem(dataSetFileName, _, numberOfBins, binCapacity))
        .toVector
    val dataSetName: String = dataSetFileName.dropRight(".csv".length)
    ProblemSet[S, I](dataSetName, problems)
  }

}
