package ie.nix.bpp.queue

import ie.nix.bpp.dataset.DataSet
import ie.nix.bpp.{Item, Queue, Size}
import org.apache.logging.log4j.scala.Logging

case class OnlineQueue[S, I <: Item[S]] private (items: Seq[I]) extends Queue[S, I] with Logging {

  override def toString: String = s"OnlineQueue[${items.size}]"

}

object OnlineQueue {

  def apply[S, I <: Item[S]](bppFileName: String, queueNumber: Int)(implicit sizeLike: Size[S],
                                                                    stringToItem: String => I): OnlineQueue[S, I] =
    OnlineQueue[S, I](DataSet[S, I](bppFileName).itemsForQueue(queueNumber))

}
