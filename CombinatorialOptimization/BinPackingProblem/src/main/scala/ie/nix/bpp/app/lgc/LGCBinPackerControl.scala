package ie.nix.bpp.app.lgc

import ie.nix.bpp.ProblemResult
import ie.nix.bpp.app.lgc.LGCItem1DBinPackerApp.{bins, problemSet, queue}
import ie.nix.peersim.Control
import ie.nix.peersim.Utils.now

class LGCBinPackerControl(prefix: String) extends Control {

  var currentQueueNumber = 0

  override def init(): Boolean = {
    super.init()
    beforePacking(currentQueueNumber)
    logger debug s"[$now]  currentQueueNumber=$currentQueueNumber"
    false
  }

  override def step(): Boolean = {
    afterPacking(currentQueueNumber)
    currentQueueNumber = (currentQueueNumber + 1) % problemSet.problems.size
    beforePacking(currentQueueNumber)
    logger debug s"[$now]  currentQueueNumber=$currentQueueNumber"
    false
  }

  override protected def finalStep(): Boolean = {
    afterPacking(currentQueueNumber)
    logger debug s"[$now]  currentQueueNumber=$currentQueueNumber"
    false
  }

  def beforePacking(queueNumber: Int): Unit = {
    val problem = problemSet.problems(queueNumber)
    logger debug s"[$now] beforePacking $problem"
    LGCItem1DBinPackerApp.beforePacking(problem, LGCItem1DBinPackerApp.resultsSetName)
    LGCItem1DBinPackerApp.initQueue(problem)
    LGCItem1DBinPackerApp.initBins(problem)
    LGCItem1DBinPackerApp.initRandom()
  }

  def afterPacking(queueNumber: Int): Unit = {
    val problemResult = ProblemResult(queue, bins)
    LGCItem1DBinPackerApp.addProblemResult(ProblemResult(queue, bins))
    LGCItem1DBinPackerApp.afterPacking(problemSet.problems(queueNumber), problemResult)
    logger debug s"[$now] afterPacking $problemResult"
  }

}
