package ie.nix.bpp.app.egc

import ie.nix.bpp.app.online.OnlineItem1DPeerSimBinPackingApp
import org.apache.logging.log4j.scala.Logging

object EvolvedItem1DBinPackerApp extends OnlineItem1DPeerSimBinPackingApp with Logging {

  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties",
                    "protocol.gc_agent=EvolvedItem1DBinPacker")

  override def resultsSetName = "EvolvedItem1DBinPacker"

}
