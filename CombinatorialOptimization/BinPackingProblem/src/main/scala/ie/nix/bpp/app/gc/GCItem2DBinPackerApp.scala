package ie.nix.bpp.app.gc

import ie.nix.bpp.app.online.OnlineItem2DPeerSimBinPackingApp
import org.apache.logging.log4j.scala.Logging

object GCItem2DBinPackerApp extends OnlineItem2DPeerSimBinPackingApp with Logging {

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String](
        "src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties",
        "network.node=ie.nix.bpp.bin.Item2DPeerSimBin",
        "protocol.agent ie.nix.bpp.packer.item2d.GCItem2DBinPacker"
      )

  silent()
  initRandom()
  packProblems()

  override def resultsSetName = "GCItem2DBinPacker"
}
