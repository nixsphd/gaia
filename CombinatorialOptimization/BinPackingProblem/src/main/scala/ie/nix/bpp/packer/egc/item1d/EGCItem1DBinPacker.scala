package ie.nix.bpp.packer.egc.item1d

import ie.nix.bpp.Queue
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.egc.GenericEGC1DBinPacker
import ie.nix.bpp.packer.gc.item1d.GCItem1DBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

class EGCItem1DBinPacker(prefix: String)
    extends GCItem1DBinPacker(prefix)
    with GenericEGC1DBinPacker[Item1D]
    with Logging {

  override def queue: Queue[Size1D, Item1D] =
    super[GenericEGC1DBinPacker].queue

}
