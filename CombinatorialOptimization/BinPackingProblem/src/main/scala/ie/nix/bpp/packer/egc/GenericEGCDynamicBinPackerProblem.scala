package ie.nix.bpp.packer.egc

import ie.nix.bpp.DynamicItem
import ie.nix.bpp.app.dynamic.GenericDynamicItemBinPackingApp
import org.apache.logging.log4j.scala.Logging

abstract class GenericEGCDynamicBinPackerProblem[S, I <: DynamicItem[S]]
    extends GenericEGCBinPackerProblem[S, I]
    with GenericDynamicItemBinPackingApp[S, I]
    with Logging {

  override def properties: Array[String] =
    super[GenericEGCBinPackerProblem].properties ++
      super[GenericDynamicItemBinPackingApp].propertiesOverrides

//  override protected def dataSetDirName(state: EvolutionState, base: Parameter): String =
//    s"$RESOURCES_DIR/$DATASETS_DIR"

  override protected def evaluateSimulation: Double = {
    val numberOfItemsQueued = queue.numberOfItemsQueued
    val numberOfItemsPacked = bins.map(_.numberOfItems).sum
    val numberOfOpenBins = dynamicItemBinObserver.maxOpenBins
    val numberOfOverloadedBins = bins.count(_.isOverloaded)

    val evaluation = queue.numberOfItemsQueued + numberOfOpenBins
    recordStat(gpIndividual, "maxOpenBins", numberOfOpenBins.toDouble)
    recordStat(gpIndividual, "numberOfItemsQueued", numberOfItemsQueued.toDouble)
    recordStat(gpIndividual, "numberOfItemsPacked", numberOfItemsPacked.toDouble)
    recordStat(gpIndividual, "evaluation", evaluation.toDouble)
    recordStat(gpIndividual, "overloadedBins", numberOfOverloadedBins.toDouble)
    logger debug s"generation=${state.generation}, gpIndividual=${gpIndividual.hashCode}, evaluation=$evaluation, " +
      s"numberOfItemsQueued=${numberOfItemsQueued}, numberOfOpenBins=$numberOfOpenBins, " +
      s"numberOfOverloadedBins=$numberOfOverloadedBins"
    evaluation.toDouble
  }

}
