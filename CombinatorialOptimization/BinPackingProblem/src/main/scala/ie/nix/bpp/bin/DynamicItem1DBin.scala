package ie.nix.bpp.bin

import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.{Item, Size}
import ie.nix.peersim.Scheduler.Callback
import ie.nix.peersim.Utils.now
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

class DynamicItem1DBin(prefix: String) extends GenericPeerSimBin[Size1D](prefix) with Logging {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D

  override def toString = s"DynamicItem1DBin-$getIndex[$usedCapacity/$capacity]"

  override def capacity: Size1D = Item1DPeerSimBinCapacity.capacity

  override def addItem(item: Item[Size1D]): Unit = item match {
    case dynamicItem: DynamicItem1D => addDynamicIItem(dynamicItem)
    case notADynamicItem            => logger error s"$notADynamicItem is not a DynamicItem1D"
  }

  def addDynamicIItem(item: DynamicItem1D): Unit = {
    super.addItem(item)
    val delay = item.departureDelay.toInt
    logger trace s"[$now] $item will be removed from $this in $delay"
    if (delay > 0)
      addCallback(delay, Callback(() => {
        removeItem(item)
        logger debug s"[$now] Removed $item from $this"
      }))
  }

}

object DynamicItem1DBin {

  implicit val toBin: peersim.core.Node => DynamicItem1DBin =
    node => as[DynamicItem1DBin](node)

}
