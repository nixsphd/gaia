package ie.nix.bpp.decorator

import ie.nix.bpp._
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.logging.log4j.scala.Logging

case class Statistics[S, I <: Item[S]]() extends BinPackingDecorator[S, I] with Logging {

  private val statistics = new DescriptiveStatistics()

  override def afterPacking(problem: Problem[S, I], problemResult: ProblemResult[S, I]): Unit = {
    if (problemResult.numberOfItemsQueued == 0)
      statistics.addValue(problemResult.numberOfOpenBins.toDouble)
    logger info s"${problem.queueNumber} numberOfOpenBins=${problemResult.numberOfOpenBins}, mean=${statistics.getMean} +- ${statistics.getStandardDeviation}"
  }

  override def afterPacking(problemSet: ProblemSet[S, I], problemResultSet: ProblemResultSet[S, I]): Unit = {
    logger info s"numberOfOpenBins=${statistics.getMean} +- ${statistics.getStandardDeviation}, ${statistics.getKurtosis}"
  }

}
