package ie.nix.bpp.packer.gc.dynamicitem2d

import ie.nix.bpp.Queue
import ie.nix.bpp.app.gc.GCDynamicItem2DBinPackerApp
import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.packer.gc.GenericGC2DBinPacker
import ie.nix.bpp.app.gc.GCDynamicItem2DBinPackerApp.dynamicQueue
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

class GCDynamicItem2DBinPacker(prefix: String) extends GenericGC2DBinPacker[DynamicItem2D](prefix) with Logging {

  override def queue: Queue[Size2D, DynamicItem2D] =
    GCDynamicItem2DBinPackerApp.queue

  protected def dynamicQueuePeekItem: DynamicItem2D = dynamicQueue.peek.get
  protected def dynamicQueueHasItem: Boolean = dynamicQueue.peek.isDefined

}
