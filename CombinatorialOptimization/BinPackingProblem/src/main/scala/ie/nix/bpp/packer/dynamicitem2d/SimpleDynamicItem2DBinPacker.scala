package ie.nix.bpp.packer.dynamicitem2d

import ie.nix.bpp.app.dynamic.DynamicItem2DPackingApp
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.packer.dynamicitem1d.{GenericSimpleDynamicBinPacker, GenericSimpleDynamicBinPackerApp}
import ie.nix.bpp.size.Size2D
import ie.nix.bpp.{Bin, Queue, Size}
import org.apache.logging.log4j.scala.Logging

object SimpleDynamicItem2DBinPackerApp
    extends GenericSimpleDynamicBinPackerApp[Size2D, DynamicItem2D]
    with DynamicItem2DPackingApp
    with Logging {

  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String](
        "network.node=DynamicItem2DBin",
        "control.bin_observer=DynamicItem2DBinObserver",
        "control.bin_integrity_observer=DynamicItem2DIntegrityObserver"
      )

}

trait SimpleDynamicItem2DBinPacker extends GenericSimpleDynamicBinPacker[Size2D, DynamicItem2D] with Logging {

  implicit def sizeLike: Size[Size2D] = SizeLikeSize2D

  def queue: Queue[Size2D, DynamicItem2D] = SimpleDynamicItem2DBinPackerApp.queue
  def bins: Vector[Bin[Size2D]] = SimpleDynamicItem2DBinPackerApp.bins

}
