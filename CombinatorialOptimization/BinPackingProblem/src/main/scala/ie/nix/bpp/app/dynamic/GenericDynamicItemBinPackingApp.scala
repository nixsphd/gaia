package ie.nix.bpp.app.dynamic

import ie.nix.bpp.app.GenericPeerSimBinPackingApp
import ie.nix.bpp.decorator.{BinPackingDecorator, DynamicItemIntegrity, Observer, Statistics}
import ie.nix.bpp.observer.{DynamicItemBinObserving, DynamicItemIntegrityObserving}
import ie.nix.bpp.queue.DynamicQueue
import ie.nix.bpp.{DynamicItem, Problem, ProblemResult, Queue}
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

trait GenericDynamicItemBinPackingApp[S, I <: DynamicItem[S]]
    extends GenericPeerSimBinPackingApp[S, I]
    with DynamicItemBinObserving[S, I]
    with DynamicItemIntegrityObserving[S, I]
    with Logging {

  override val decorators: Vector[BinPackingDecorator[S, I]] =
    Vector[BinPackingDecorator[S, I]]() :+
      Statistics[S, I]() :+
      Observer[S, I]() :+
      DynamicItemIntegrity[S, I]()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/DynamicBinPacker.properties")

  override def pack(problem: Problem[S, I]): ProblemResult[S, I] = {
    super.pack(problem)
    val numberOfItemsQueued = queue.numberOfItemsQueued
    val numberOfItemsPacked = bins.map(_.numberOfItems).sum
    val numberOfOpenBins = dynamicItemBinObserver.maxOpenBins
    val numberOfOverloadedBins = dynamicItemIntegrityObserver.maxOverloadedBins
    ProblemResult[S, I](numberOfItemsQueued, numberOfItemsPacked, numberOfOpenBins, numberOfOverloadedBins)
  }

  override def initQueue(problem: Problem[S, I]): Unit =
    registerQueue(DynamicQueue(problem.dataSetFileName, problem.queueNumber))

  override def queue: Queue[S, I] = registeredQueue

  def dynamicQueue: DynamicQueue[S, I] =
    queue match {
      case queue: DynamicQueue[S, I] => queue
      case _ =>
        logger error s"FATAL: The registeredQueue, $queue, was not an DynamicQueue"
        exit
    }

  override def initBins(problem: Problem[S, I]): Unit = {
    super[GenericPeerSimBinPackingApp].initBins(problem)
    setBinCapacity(problem.binCapacity)
  }
  override def setBinCapacity(capacity: S): Unit

}
