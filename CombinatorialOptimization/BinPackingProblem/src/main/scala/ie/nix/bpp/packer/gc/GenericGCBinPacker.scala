package ie.nix.bpp.packer.gc

import ie.nix.bpp.bin.GenericPeerSimBin
import ie.nix.bpp.{Item, Queue}
import ie.nix.gc.GCAgent
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

abstract class GenericGCBinPacker[S, I <: Item[S]](prefix: String) extends GCAgent[I, Void](prefix) with Logging {

  protected var maybeTask: Option[I] = None

  override def toString: String = (node, hasTask) match {
    case (null, false) => s"${getClass.getSimpleName}-? [$node, None]"
    case (null, true)  => s"${getClass.getSimpleName}-? [$node, $getTask]"
    case (_, false)    => s"${getClass.getSimpleName}-$getIndex [$node, None]"
    case _             => s"${getClass.getSimpleName}-$getIndex [$node, $getTask]"
  }
  def hasTask: Boolean = maybeTask.isDefined
  def doesNotHaveATask: Boolean = !hasTask
  def getTask: I = maybeTask.get
  def setTask(item: I): Unit = maybeTask = Some(item)
  def clearTask(): Unit = maybeTask = None

  override def preTenderAwardCycle(node: Node): Unit =
    if (isIdle && doesNotHaveATask && queue.peek.isDefined &&
        bin(node).canFit(queue.peek.get))
      setTask(queue.poll.get)

  override def shouldTender(node: Node): Boolean = hasTask

  override def getTask(node: Node): I = maybeTask.get

  override def shouldBid(tenderMessage: TenderMessage[I]): Boolean = {
    val myBin = bin(tenderMessage.to)
    val item = tenderMessage.task
    doesNotHaveATask && myBin.canFit(item)
  }

  override def getProposal(tenderMessage: TenderMessage[I]): Void = null

  override def bidToDouble(bidMessage: BidMessage[I, Void]): Double = bidMessage.offer

  override def award(awardMessage: AwardMessage[I, Void]): Unit = clearTask()

  override def awarded(awardMessage: AwardMessage[I, Void]): Unit = {
    val myBin = bin(awardMessage.to)
    val item = awardMessage.task
    myBin.addItem(item)
  }

  override def postTenderAwardCycle(node: Node): Unit = {
    clearExpiredMessages()
    if (hasTask) {
      bin(node).addItem(getTask) // So the task was never awarded so we should assign it to ourselves.
      clearTask()
    }
  }

  def bin(node: Node): GenericPeerSimBin[S] = node match {
    case bin: GenericPeerSimBin[S] => bin
    case _ =>
      logger error s"Expected a node of type Bin, but go one of type ${node.getClass.getName}"
      exit
  }

  def queue: Queue[S, I]

}
