package ie.nix.bpp.packer.egc.item2d

import ie.nix.bpp.app.online.OnlineItem2DPeerSimBinPackingApp
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.packer.egc.GenericEGCBinPackerProblem
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

class EGCItem2DBinPackerProblem
    extends GenericEGCBinPackerProblem[Size2D, Item2D]
    with OnlineItem2DPeerSimBinPackingApp
    with Logging {

  override def properties: Array[String] =
    super[GenericEGCBinPackerProblem].properties ++
      super[OnlineItem2DPeerSimBinPackingApp].propertiesOverrides :+
      "network.node=ie.nix.bpp.bin.Item2DPeerSimBin"

}
