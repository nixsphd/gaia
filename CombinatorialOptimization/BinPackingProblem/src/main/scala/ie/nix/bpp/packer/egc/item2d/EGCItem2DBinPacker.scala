package ie.nix.bpp.packer.egc.item2d

import ie.nix.bpp.Queue
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.packer.egc.GenericEGC2DBinPacker
import ie.nix.bpp.packer.gc.item2d.GCItem2DBinPacker
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

class EGCItem2DBinPacker(prefix: String)
    extends GCItem2DBinPacker(prefix)
    with GenericEGC2DBinPacker[Item2D]
    with Logging {

  override def queue: Queue[Size2D, Item2D] =
    super[GenericEGC2DBinPacker].queue

}
