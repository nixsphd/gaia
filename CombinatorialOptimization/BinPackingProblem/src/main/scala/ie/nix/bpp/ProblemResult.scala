package ie.nix.bpp

import org.apache.logging.log4j.scala.Logging

case class ProblemResult[S, I <: Item[S]](numberOfItemsQueued: Int,
                                          numberOfItemsPacked: Int,
                                          numberOfOpenBins: Int,
                                          numberOfOverloadedBins: Int)

object ProblemResult extends Logging {
  def apply[S: Size, I <: Item[S]](queue: Queue[S, I], bins: Vector[Bin[S]]): ProblemResult[S, I] = {
    logger debug s"bins=${bins.sortBy(_.freeCapacity)}"
    val numberOfItemsQueued = queue.getNumberOfItems
    val numberOfItemsPacked = bins.map(_.numberOfItems).sum
    val numberOfOpenBins = bins.filterNot(_.isEmpty).size
    val numberOfOverloadedBins = bins.count(_.isOverloaded)
    ProblemResult[S, I](numberOfItemsQueued, numberOfItemsPacked, numberOfOpenBins, numberOfOverloadedBins)
  }
}
