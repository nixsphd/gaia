package ie.nix.bpp.observer

import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.size.Size2D

class DynamicItem2DBinObserver(prefix: String) extends DynamicItemBinObserver[Size2D, DynamicItem2D](prefix)
