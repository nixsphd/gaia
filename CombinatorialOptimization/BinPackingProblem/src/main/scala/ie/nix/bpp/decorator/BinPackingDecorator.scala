package ie.nix.bpp.decorator

import ie.nix.bpp._
import org.apache.logging.log4j.scala.Logging

trait BinPackingDecorator[S, I <: Item[S]] extends Logging {
  def beforePacking(problemSet: ProblemSet[S, I], resultsSetName: String): Unit = {}

  def beforePacking(problem: Problem[S, I], resultsSetName: String): Unit = {}

  def afterPacking(problem: Problem[S, I], problemResult: ProblemResult[S, I]): Unit = {}

  def afterPacking(problemSet: ProblemSet[S, I], problemResultSet: ProblemResultSet[S, I]): Unit = {}
}
