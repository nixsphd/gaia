package ie.nix.bpp.packer.dynamicitem1d

import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.Queue
import ie.nix.bpp.app.dynamic.DynamicItem1DPackingApp
import ie.nix.bpp.packer.egc.GenericEvolved1DBinPacker
import ie.nix.bpp.packer.gc.GenericGC1DBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

object DemoEvolvedDynamicItem1DBinPackerApp extends DynamicItem1DPackingApp with Logging {

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties",
                    "protocol.agent=DemoEvolvedDynamicItem1DBinPacker")

  silent()
  initRandom()
  packProblems()

  override def resultsSetName = "DemoEvolvedDynamicBinPacker"

}

class DemoEvolvedDynamicItem1DBinPacker(prefix: String)
    extends GenericGC1DBinPacker[DynamicItem1D](prefix)
    with GenericEvolved1DBinPacker[DynamicItem1D] {

  override def queue: Queue[Size1D, DynamicItem1D] =
    DemoEvolvedDynamicItem1DBinPackerApp.queue

  def shouldTender(freeCapacity: Double, itemSize: Double): Boolean = true
  def getGuide(freeCapacity: Double, itemSize: Double): Double = freeCapacity
  def tenderToDouble(guide: Double, freeCapacity: Double, itemSize: Double): Double =
    ifd(freeCapacity > itemSize, -itemSize, Double.PositiveInfinity)
  def shouldBid(guide: Double, freeCapacity: Double, itemSize: Double): Boolean = true
  def getOffer(guide: Double, freeCapacity: Double, itemSize: Double): Double = freeCapacity
  def bidToDouble(offer: Double, freeCapacity: Double, itemSize: Double): Double = offer
  def shouldAward(offer: Double, freeCapacity: Double, itemSize: Double): Boolean = offer <= freeCapacity
}
