package ie.nix.bpp.packer.item1d

import ie.nix.bpp.app.online.OnlineItem1DBinPackingApp
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.GenericFirstFitBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

object FirstFitItem1DPackerApp extends OnlineItem1DBinPackingApp with Logging {

  initRandom()
  packProblems()

  override def resultsSetName: String = "FirstFitItem1DPacker"
  override def initPacker(): Unit =
    packer = FirstFitItem1DBinPacker()

}

case class FirstFitItem1DBinPacker() extends GenericFirstFitBinPacker[Size1D, Item1D]
