package ie.nix.bpp.packer.egc.dynamicitem2d

import ie.nix.bpp.Queue
import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.packer.egc.GenericEGC2DBinPacker
import ie.nix.bpp.packer.gc.dynamicitem2d.GCDynamicItem2DBinPacker
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

class EGCDynamicItem2DBinPacker(prefix: String)
    extends GCDynamicItem2DBinPacker(prefix)
    with GenericEGC2DBinPacker[DynamicItem2D]
    with Logging {

  override def queue: Queue[Size2D, DynamicItem2D] =
    super[GenericEGC2DBinPacker].queue

}
