package ie.nix.bpp.app.dynamic

import ie.nix.bpp.Size
import ie.nix.bpp.bin.Item2DPeerSimBinCapacity
import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.size.Size2D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D

trait DynamicItem2DPackingApp extends GenericDynamicItemBinPackingApp[Size2D, DynamicItem2D] {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
  override implicit def toItem: String => DynamicItem2D = DynamicItem2D.toDynamicItem2D

  override def setBinCapacity(capacity: Size2D): Unit =
    Item2DPeerSimBinCapacity.capacity(capacity)

  override val binObserverName = "DynamicItem2DBinObserver"
  override val integrityObserverName = "DynamicItem2DIntegrityObserver"

}
