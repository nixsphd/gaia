package ie.nix.bpp

case class ProblemResultSet[S, I <: Item[S]](name: String,
                                             problemSet: ProblemSet[S, I],
                                             problemResults: Vector[ProblemResult[S, I]])
