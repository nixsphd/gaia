package ie.nix.bpp.dataset

import ie.nix.bpp.dataset.DataSet.{generateQueues, saveQueuesToFile}
import ie.nix.bpp.item.{DynamicItem1D, DynamicItem2D}
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.size.{Size1D, Size2D}
import ie.nix.bpp.{DynamicItem, Size}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

trait GenericTripletDynamicDataSets[S, I <: DynamicItem[S]]
    extends GenericTripletDataSets[S, I]
    with DynamicDataSet
    with Logging {
  this: App =>

  override val USAGE = "Please pass in one argument structured as follows: " +
    "numberOfQueues numberOfItems minItemSize-maxItemSize-meanArrivalRate-meanDepartureTime binCapacity (seed)"

  override def tripletDataSet: DataSet[S, I] =
    DataSet[S, I](generateQueues[S, I](numberOfQueues, numberOfItems))

  def dbppArgs: Vector[String] = { checkArgs(); args(2).split("-").toVector }
  override def minFirstTriplesItemSize: Int = dbppArgs(0).toInt
  override def maxFirstTriplesItemSize: Int = dbppArgs(1).toInt
  override def minSecondTriplesItemSize: Int = dbppArgs(2).toInt
  def meanArrivalTime: Int = dbppArgs(3).toInt
  def meanDepartureTime: Int = dbppArgs(4).toInt
  def seed: Long = { checkArgs(); if (args.length >= 5) args(4).toLong else 0 }

}

object TripletDynamicItem1DDataSets extends App with GenericTripletDynamicDataSets[Size1D, DynamicItem1D] with Logging {

  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
  override implicit def itemToString: DynamicItem1D => String = DynamicItem.dynamicItemToString[Size1D]
  override implicit def queueGenerator: QueueGenerator[Size1D, DynamicItem1D] =
    TripletDynamicItem1DQueueGenerator(binCapacity,
                                       minFirstTriplesItemSize,
                                       maxFirstTriplesItemSize,
                                       minSecondTriplesItemSize,
                                       meanArrivalTime,
                                       meanDepartureTime)

  override val tripletDataSetFilename = s"TripletDynamicItem1DDataSets-$numberOfItems-${args(2)}.csv"

  saveQueuesToFile[Size1D, DynamicItem1D](tripletDataSet, tripletDataSetFilename)
  logger info s"Wrote $tripletDataSetFilename"

  override def tripletDataSet: DataSet[Size1D, DynamicItem1D] = {
    val queues: Vector[Vector[DynamicItem1D]] =
      generateQueues[Size1D, DynamicItem1D](numberOfQueues, numberOfItems)(queueGenerator)
    DataSet[Size1D, DynamicItem1D](queues)
  }

}

case class TripletDynamicItem1DQueueGenerator(binCapacity: Size1D,
                                              minFirstTriplesItemSize: Int,
                                              maxFirstTriplesItemSize: Int,
                                              minSecondTriplesItemSize: Int,
                                              meanArrivalTime: Int,
                                              meanDepartureTime: Int)
    extends DynamicQueueGenerator[Size1D, DynamicItem1D] {

  override def generateItems: Vector[DynamicItem1D] = {
    val item1Size = minFirstTriplesItemSize + Random.nextInt(maxFirstTriplesItemSize - minFirstTriplesItemSize)
    val item1ArrivalTime = arrivalTimeSampler.sample
    val item1DepartureTime = item1ArrivalTime + departureTimeSampler.sample

    val maxSecondTriplesItemSize = (binCapacity.size - item1Size) / 2
    val item2Size = minSecondTriplesItemSize + Random.nextInt(maxSecondTriplesItemSize - minSecondTriplesItemSize)
    val item2ArrivalTime = arrivalTimeSampler.sample
    val item2DepartureTime = item2ArrivalTime + departureTimeSampler.sample

    val item3Size = binCapacity.size - (item1Size + item2Size)
    val item3ArrivalTime = arrivalTimeSampler.sample
    val item3DepartureTime = item3ArrivalTime + departureTimeSampler.sample

    Vector[DynamicItem1D](
      new DynamicItem1D(Size1D(item1Size), item1ArrivalTime, item1DepartureTime),
      new DynamicItem1D(Size1D(item2Size), item2ArrivalTime, item2DepartureTime),
      new DynamicItem1D(Size1D(item3Size), item3ArrivalTime, item3DepartureTime)
    )

  }

  override def postGenerateQueue(queueNumber: Int, queue: Vector[DynamicItem1D]): Vector[DynamicItem1D] = {
    val shuffledSizes = Random.shuffle(queue.map(_.size))
    for ((originalItem, shuffledSize) <- queue.zip(shuffledSizes))
      yield new DynamicItem1D(shuffledSize, originalItem.arrivalTime, originalItem.departureTime)
  }

}

object TripletDynamicItem2DDataSets extends App with GenericTripletDynamicDataSets[Size2D, DynamicItem2D] with Logging {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
  override implicit def itemToString: DynamicItem2D => String = DynamicItem.dynamicItemToString[Size2D]
  override implicit def queueGenerator: DynamicQueueGenerator[Size2D, DynamicItem2D] =
    TripletDynamicItem2DQueueGenerator(binCapacity,
                                       minFirstTriplesItemSize,
                                       maxFirstTriplesItemSize,
                                       minSecondTriplesItemSize,
                                       meanArrivalTime,
                                       meanDepartureTime)

  override val tripletDataSetFilename = s"TripletDynamicItem2DDataSets-$numberOfItems-${args(2)}.csv"

  saveQueuesToFile[Size2D, DynamicItem2D](tripletDataSet, tripletDataSetFilename)
  logger info s"Wrote $tripletDataSetFilename"

}

case class TripletDynamicItem2DQueueGenerator(binCapacity: Size2D,
                                              minFirstTriplesItemSize: Int,
                                              maxFirstTriplesItemSize: Int,
                                              minSecondTriplesItemSize: Int,
                                              meanArrivalTime: Int,
                                              meanDepartureTime: Int)
    extends DynamicQueueGenerator[Size2D, DynamicItem2D] {

  override def generateItems: Vector[DynamicItem2D] = {
    val item1Size1 = minFirstTriplesItemSize + Random.nextInt(maxFirstTriplesItemSize - minFirstTriplesItemSize)
    val item1Size2 = minFirstTriplesItemSize + Random.nextInt(maxFirstTriplesItemSize - minFirstTriplesItemSize)
    val item1ArrivalTime = arrivalTimeSampler.sample
    val item1DepartureTime = item1ArrivalTime + departureTimeSampler.sample

    val maxSecondTriplesItemSize1 = (binCapacity.size1 - item1Size1) / 2
    val maxSecondTriplesItemSize2 = (binCapacity.size2 - item1Size2) / 2
    val item2Size1 = minSecondTriplesItemSize + Random.nextInt(maxSecondTriplesItemSize1 - minSecondTriplesItemSize)
    val item2Size2 = minSecondTriplesItemSize + Random.nextInt(maxSecondTriplesItemSize2 - minSecondTriplesItemSize)
    val item2ArrivalTime = arrivalTimeSampler.sample
    val item2DepartureTime = item2ArrivalTime + departureTimeSampler.sample

    val item3Size1 = binCapacity.size1 - (item1Size1 + item2Size1)
    val item3Size2 = binCapacity.size2 - (item1Size1 + item2Size2)
    val item3ArrivalTime = arrivalTimeSampler.sample
    val item3DepartureTime = item3ArrivalTime + departureTimeSampler.sample

    Vector[DynamicItem2D](
      new DynamicItem2D(Size2D(item1Size1, item1Size2), item1ArrivalTime, item1DepartureTime),
      new DynamicItem2D(Size2D(item2Size1, item2Size2), item2ArrivalTime, item2DepartureTime),
      new DynamicItem2D(Size2D(item3Size1, item3Size2), item3ArrivalTime, item3DepartureTime)
    )

  }
  override def postGenerateQueue(queueNumber: Int, queue: Vector[DynamicItem2D]): Vector[DynamicItem2D] = {
    val shuffledSizes = Random.shuffle(queue.map(_.size))
    for ((originalItem, shuffledSize) <- queue.zip(shuffledSizes))
      yield new DynamicItem2D(shuffledSize, originalItem.arrivalTime, originalItem.departureTime)
  }

}
