package ie.nix.bpp

import org.apache.logging.log4j.scala.Logging
import peersim.core.CommonState

trait DynamicItem[S] extends Item[S] with Logging {

  protected val arrivalTime: Long
  protected val departureTime: Long

  logger trace s"size=$size, arrivalTime=$arrivalTime, departureTime=$departureTime"
  require(arrivalTime >= 0)
  require(departureTime >= arrivalTime)

  def hasArrived: Boolean = arrivalTime <= CommonState.getTime
  def hasDeparted: Boolean = departureTime <= CommonState.getTime
  def departureDelay: Long = departureTime - CommonState.getTime

}

object DynamicItem {

  implicit def dynamicItemToString[S: Size]: DynamicItem[S] => String =
    dynamicItem =>
      s"(${implicitly[Size[S]].toString(dynamicItem.size)} ${dynamicItem.arrivalTime} ${dynamicItem.departureTime})"

}
