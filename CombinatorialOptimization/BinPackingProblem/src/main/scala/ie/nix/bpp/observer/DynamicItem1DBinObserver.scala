package ie.nix.bpp.observer

import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.size.Size1D

class DynamicItem1DBinObserver(prefix: String) extends DynamicItemBinObserver[Size1D, DynamicItem1D](prefix)
