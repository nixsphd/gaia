package ie.nix.bpp.packer.egc.dynamicitem2d

import ie.nix.bpp.app.dynamic.DynamicItem2DPackingApp
import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.packer.egc.GenericEGCDynamicBinPackerProblem
import ie.nix.bpp.size.Size2D

class EGCDynamicItem2DBinPackerProblem
    extends GenericEGCDynamicBinPackerProblem[Size2D, DynamicItem2D]
    with DynamicItem2DPackingApp {

  override def properties: Array[String] =
    super[GenericEGCDynamicBinPackerProblem].properties ++
      super[DynamicItem2DPackingApp].propertiesOverrides ++
      Vector("network.node=DynamicItem2DBin",
             "control.bin_observer DynamicItem2DBinObserver",
             "control.bin_integrity_observer DynamicItem2DIntegrityObserver")
}
