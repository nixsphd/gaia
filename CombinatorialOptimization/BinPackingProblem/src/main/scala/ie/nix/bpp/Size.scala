package ie.nix.bpp

trait Size[S] extends Ordering[S] {
  def zero: S
  def isPositive(size: S): Boolean
  def plus(oneSize: S, otherSize: S): S
  def minus(oneSize: S, otherSize: S): S

  def toString(size: S): String
  def toSize(size1DAsString: String): S
}
