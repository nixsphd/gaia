package ie.nix.bpp.packer

import ie.nix.bpp.{Bin, Item, Queue, Size}
import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

trait GenericFirstFitDecreasingBinPacker[S, I <: Item[S]] extends GenericSimplePacker[S, I] with Logging {

  implicit def sizeLike: Size[S]

  override def pack(queue: Queue[S, I], bins: Vector[Bin[S]]): Unit = {
    logger trace s"[$now] Queue has ${queue.numberOfItemsQueued} items queued."
    queue.peek match {
      case Some(item) =>
        val packedItems = emptyBins(bins)
        val allItemsSorted = (packedItems :+ item).sortBy(_.size)
        val packedALlItems = allItemsSorted.map(packItem(_, bins)).count(!_) == 0
        if (packedALlItems) {
          queue.poll
          pack(queue, bins)
        } else {
          logger error s"[$now] PANIC! Did not manage to pack all the items."
          exit
        }
      case None => // Nothing in the queue
    }
  }

  private def emptyBins(bins: Vector[Bin[S]]): Vector[Item[S]] = {
    val packedItems: Vector[Item[S]] = bins.flatMap(_.getItems)
    for (bin <- bins)
      bin.empty()
    packedItems
  }

  private def packItem(item: Item[S], bins: Vector[Bin[S]]): Boolean = {
    val maybeFirstBin = bins.find(_.canFit(item))
    maybeFirstBin match {
      case Some(firstBin) => {
        firstBin.addItem(item)
        logger debug s"[$now] Added $item to $firstBin"
        true
      }
      case _ => {
        // do nothing
        logger warn s"[$now] $item does not fit in any bins"
        false
      }
    }
  }
}
