package ie.nix.bpp.observer

import ie.nix.bpp.DynamicItem
import ie.nix.peersim.Control
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

trait DynamicItemIntegrityObserving[S, I <: DynamicItem[S]] extends Logging {

  val integrityObserverName = "DynamicItemIntegrityObserver"

  def dynamicItemIntegrityObserver: DynamicItemIntegrityObserver[S, I] = {
    Control.controlFor(integrityObserverName) match {
      case Some(control: DynamicItemIntegrityObserver[S, I]) => control
      case Some(control) =>
        logger error s"FATAL: Could not find control of type $integrityObserverName, instead ${control.getClass.getSimpleName}"
        exit
      case None =>
        logger error s"FATAL: Could not find the $integrityObserverName in ${Control.controls}"
        exit
    }
  }

}
