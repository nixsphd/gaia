package ie.nix.bpp.packer.gc.item2d

import ie.nix.bpp.Queue
import ie.nix.bpp.app.gc.GCItem2DBinPackerApp
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.packer.gc.GenericGC2DBinPacker
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

class GCItem2DBinPacker(prefix: String) extends GenericGC2DBinPacker[Item2D](prefix) with Logging {

  override def queue: Queue[Size2D, Item2D] = GCItem2DBinPackerApp.queue

}
