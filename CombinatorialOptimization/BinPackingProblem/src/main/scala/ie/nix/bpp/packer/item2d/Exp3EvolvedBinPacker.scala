package ie.nix.bpp.packer.item2d

import ie.nix.bpp.item.Item2D
import ie.nix.bpp.Queue
import ie.nix.bpp.app.online.OnlineItem2DPeerSimBinPackingApp
import ie.nix.bpp.packer.egc.GenericEvolved2DBinPacker
import ie.nix.bpp.packer.gc.GenericGC2DBinPacker
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

object Exp3EvolvedBinPackerApp extends OnlineItem2DPeerSimBinPackingApp with Logging {

  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties",
                    "network.node=ie.nix.bpp.bin.Item2DPeerSimBin",
                    "protocol.agent=Exp3EvolvedBinPacker")

  override def resultsSetName = "Exp3EvolvedBinPacker"
}

class Exp3EvolvedBinPacker(prefix: String)
    extends GenericGC2DBinPacker[Item2D](prefix)
    with GenericEvolved2DBinPacker[Item2D]
    with Logging {

  import ie.nix.bpp.packer.egc.RichDouble.doubleToRichDouble

  override def queue: Queue[Size2D, Item2D] = Exp3EvolvedBinPackerApp.queue

  def shouldTender(freeCapacityX: Double, freeCapacityY: Double, itemSizeX: Double, itemSizeY: Double): Boolean =
    ((freeCapacityY < freeCapacityX) || (itemSizeX < freeCapacityX))
  def getGuide(freeCapacityX: Double, freeCapacityY: Double, itemSizeX: Double, itemSizeY: Double): Double =
    freeCapacityX
  def tenderToDouble(guide: Double,
                     freeCapacityX: Double,
                     freeCapacityY: Double,
                     itemSizeX: Double,
                     itemSizeY: Double): Double = ((freeCapacityX - guide) div freeCapacityY)
  def shouldBid(guide: Double,
                freeCapacityX: Double,
                freeCapacityY: Double,
                itemSizeX: Double,
                itemSizeY: Double): Boolean = true
  def getOffer(guide: Double,
               freeCapacityX: Double,
               freeCapacityY: Double,
               itemSizeX: Double,
               itemSizeY: Double): Double =
    (ifd((true && true), (freeCapacityX div guide), (itemSizeY - itemSizeY)) max (ifd(true, itemSizeX, positiveInfinity) + ifd(
      (true && true),
      (freeCapacityX div guide),
      (itemSizeY - itemSizeY))))
  def bidToDouble(offer: Double,
                  freeCapacityX: Double,
                  freeCapacityY: Double,
                  itemSizeX: Double,
                  itemSizeY: Double): Double =
    (((freeCapacityY + freeCapacityX) + (freeCapacityX min offer)) div (freeCapacityY + freeCapacityX))
  def shouldAward(offer: Double,
                  freeCapacityX: Double,
                  freeCapacityY: Double,
                  itemSizeX: Double,
                  itemSizeY: Double): Boolean = ifb(true, true, true)

}
