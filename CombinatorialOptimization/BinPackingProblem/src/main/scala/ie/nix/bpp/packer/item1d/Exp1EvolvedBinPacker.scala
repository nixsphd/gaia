package ie.nix.bpp.packer.item1d

import ie.nix.bpp.item.Item1D
import ie.nix.bpp.Queue
import ie.nix.bpp.app.online.OnlineItem1DPeerSimBinPackingApp
import ie.nix.bpp.packer.egc.GenericEvolved1DBinPacker
import ie.nix.bpp.packer.gc.GenericGC1DBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

import scala.math.min
import scala.math.abs

object Exp1EvolvedBinPackerApp extends OnlineItem1DPeerSimBinPackingApp with Logging {

  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/GCBinPacker.properties", "protocol.agent=Exp1EvolvedBinPacker")

  override def resultsSetName = "Exp1EvolvedBinPacker"
}

class Exp1EvolvedBinPacker(prefix: String)
    extends GenericGC1DBinPacker[Item1D](prefix)
    with GenericEvolved1DBinPacker[Item1D]
    with Logging {

  override def queue: Queue[Size1D, Item1D] = Exp1EvolvedBinPackerApp.queue

// exp1 seed 19
  def shouldTender(freeCapacity: Double, itemSize: Double): Boolean = true
  def getGuide(freeCapacity: Double, itemSize: Double): Double = 0

  def tenderToDouble(guide: Double, freeCapacity: Double, itemSize: Double): Double = {
    abs(
      min(4 * (itemSize * itemSize) - 2 * (itemSize * freeCapacity) + freeCapacity,
          -(itemSize * itemSize) + (itemSize * freeCapacity)))
  }

  def shouldBid(guide: Double, freeCapacity: Double, itemSize: Double): Boolean = true
  def getOffer(guide: Double, freeCapacity: Double, itemSize: Double): Double = freeCapacity
  def bidToDouble(offer: Double, freeCapacity: Double, itemSize: Double): Double = offer
  def shouldAward(offer: Double, freeCapacity: Double, itemSize: Double) = true

}
