package ie.nix.bpp.app.offline

import ie.nix.bpp.bin.Item2DBin
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.size.Size2D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.{Problem, Size}

trait OfflineItem2DBinPackingApp extends GenericOfflinePackingApp[Size2D, Item2D] {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
  override implicit def toItem: String => Item2D = Item2D.toItem[Size2D]

  def initBins(problem: Problem[Size2D, Item2D]): Unit =
    bins = Vector.fill(problem.numberOfBins)(Item2DBin(problem.binCapacity))

}
