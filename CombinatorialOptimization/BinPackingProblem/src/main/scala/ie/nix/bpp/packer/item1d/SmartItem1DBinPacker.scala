package ie.nix.bpp.packer.item1d

import ie.nix.bpp.app.online.OnlineItem1DPeerSimBinPackingApp
import ie.nix.bpp.bin.Item1DPeerSimBin
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.queue.OnlineQueue
import ie.nix.bpp.size.Size1D
import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.Utils.now
import ie.nix.peersim.{Node, Protocol}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object SmartItem1DBinPackerApp extends OnlineItem1DPeerSimBinPackingApp with Logging {

  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/SmartBinPacker.properties")

  override def resultsSetName = "SmartItem1DBinPacker"
}

class SmartItem1DBinPacker(prefix: String) extends Protocol(prefix) with Logging {

  override def toString: String = s"${getClass.getSimpleName}"

  override def nextCycle(node: Node): Unit = {
    logger trace s"[$now] node=$node"
    node match {
      case bin: Item1DPeerSimBin => nextCycle(bin)
      case _                     => logger warn s"[$now] was expecting $node to be a SmartBin"
    }
  }

  def nextCycle(bin: Item1DPeerSimBin): Unit = {
    logger trace s"[$now] bin=$bin, queue=$queue, ${queue.peek}"
    queue.peek match {
      case Some(item) =>
        if (bin.canFit(item)) {
          if (shouldPackItem(bin)) {
            val itemPacked = packItem(item, bin)
            if (itemPacked) queue.poll
          }
        }
      case _ => logger trace s"[$now] queue was empty"
    }
  }

  def queue: OnlineQueue[Size1D, Item1D] = SmartItem1DBinPackerApp.onlineQueue

  def shouldPackItem(bin: Item1DPeerSimBin): Boolean = {
    val relativeUsedCapacity = bin.usedCapacity.size.toDouble / bin.capacity.size.toDouble
    val rand = Random.nextDouble() - 0.15
    val shouldPackItem = rand < relativeUsedCapacity
    logger debug s"shouldPackItem=$shouldPackItem = rand=$rand < relativeUsedCapacity=$relativeUsedCapacity"
    shouldPackItem
  }

  private def packItem(item: Item1D, bin: Item1DPeerSimBin): Boolean = {
    if (bin.canFit(item)) {
      bin.addItem(item)
      logger debug s"[$now] Added $item to $bin"
      true
    } else {
      // do nothing
      logger debug s"[$now] $item does not fit in this bin, $bin"
      false
    }
  }

  override def processEvent(node: Node, message: Message): Unit = {}
}
