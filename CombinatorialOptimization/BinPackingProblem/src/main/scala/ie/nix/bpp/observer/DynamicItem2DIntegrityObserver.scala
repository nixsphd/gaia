package ie.nix.bpp.observer

import ie.nix.bpp.item.DynamicItem2D
import ie.nix.bpp.size.Size2D

class DynamicItem2DIntegrityObserver(prefix: String) extends DynamicItemIntegrityObserver[Size2D, DynamicItem2D](prefix)
