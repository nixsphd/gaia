package ie.nix.bpp.bin

import ie.nix.bpp.size.Size1D
import ie.nix.bpp.{Bin, Size}
import org.apache.logging.log4j.scala.Logging

case class Item1DBin(var _capacity: Size1D)(implicit val sizeLike: Size[Size1D]) extends Bin[Size1D] with Logging {

  override def toString = s"Item1DBin[$usedCapacity]"

  override def capacity: Size1D = _capacity

}
