package ie.nix.bpp.item

import ie.nix.bpp.size.Size1D
import ie.nix.bpp.{Item, Size}

class Item1D(val size: Size1D)(implicit val sizeLike: Size[Size1D]) extends Item[Size1D] {

  override def toString: String = s"Item1D[$size]"

}
object Item1D {

  implicit def toItem[S: Size]: String => Item1D =
    itemAsString => new Item1D(implicitly[Size[Size1D]].toSize(itemAsString))

  def apply(sizeAsInt: Int): Item1D = new Item1D(Size1D(sizeAsInt))

}
