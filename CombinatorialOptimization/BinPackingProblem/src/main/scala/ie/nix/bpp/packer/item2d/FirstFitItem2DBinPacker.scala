package ie.nix.bpp.packer.item2d

import ie.nix.bpp.app.online.OnlineItem2DBinPackingApp
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.packer.GenericFirstFitBinPacker
import ie.nix.bpp.size.Size2D
import org.apache.logging.log4j.scala.Logging

object FirstFitItem2DPackerApp extends OnlineItem2DBinPackingApp with Logging {

  initRandom()
  packProblems()

  override def resultsSetName: String = "FirstFitItem2DPacker"
  override def initPacker(): Unit =
    packer = FirstFitItem2DBinPacker()

}

case class FirstFitItem2DBinPacker() extends GenericFirstFitBinPacker[Size2D, Item2D]
