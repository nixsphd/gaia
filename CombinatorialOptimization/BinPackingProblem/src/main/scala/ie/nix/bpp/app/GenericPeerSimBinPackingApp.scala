package ie.nix.bpp.app

import ie.nix.bpp._
import ie.nix.bpp.queue.QueueRegister
import ie.nix.peersim.Node.defaultNetworkSize
import ie.nix.peersim.Utils.nodesAs
import ie.nix.util.Utils.as
import org.apache.logging.log4j.core.util.NullOutputStream
import peersim.cdsim.CDSimulator
import peersim.core.CommonState
import peersim.edsim.EDSimulator

import java.io.PrintStream
import scala.util.Random

trait GenericPeerSimBinPackingApp[S, I <: Item[S]]
    extends App
    with BinPacking[S, I]
    with GenericPeerSimBinPackingAppArgs[S]
    with QueueRegister[S, I] {

  implicit def toItem: String => I
  implicit def toBinlike: peersim.core.Node => Bin[S] = node => as[Bin[S]](node)

  override def propertiesOverrides: Array[String] = binPackerProperties

  override def problemSet: ProblemSet[S, I] =
    ProblemSet[S, I](dataSetFileName, numberOfBins, binCapacity)

  override def pack(problem: Problem[S, I]): ProblemResult[S, I] = {
    logger debug s"Packing problem ${problem.queueNumber}"
    initQueue(problem)
    initBins(problem)
    initRandom()
    if (CDSimulator.isConfigurationCycleDriven)
      CDSimulator.nextExperiment()
    else if (EDSimulator.isConfigurationEventDriven)
      EDSimulator.nextExperiment()
    ProblemResult(queue, bins)
  }

  def binPackerProperties: Array[String] =
    Array[String]("src/main/resources/ie/nix/bpp/BinPacking.properties")

  def gcBinPackerProperties: Array[String] =
    Array[String]("src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties")

  def initRandom(): Unit = {
    Random.setSeed(seed)
    CommonState.r.setSeed(seed)
  }

  def initQueue(problem: Problem[S, I]): Unit
  def queue: Queue[S, I]

  def initBins(problem: Problem[S, I]): Unit = defaultNetworkSize(problem.numberOfBins)
  def bins: Vector[Bin[S]] = nodesAs[Bin[S]]
  def binCapacity(implicit sizeLike: Size[S]): S = sizeLike.toSize(binCapacityFromConfig)
  def setBinCapacity(binCapacity: S): Unit

  def silent(): Unit = {
    System.setErr(new PrintStream(NullOutputStream.getInstance()))
    System.setOut(new PrintStream(NullOutputStream.getInstance()))
  }

}
