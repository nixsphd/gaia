package ie.nix.bpp.packer

import ie.nix.bpp.{Bin, Item, Queue}
import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging

trait GenericFirstFitBinPacker[S, I <: Item[S]] extends GenericSimplePacker[S, I] with Logging {

  override def pack(queue: Queue[S, I], bins: Vector[Bin[S]]): Unit = {
    logger debug s"[$now] Queue has ${queue.numberOfItemsQueued} items queued."
    queue.peek match {
      case Some(item) =>
        val itemPacked = packItem(item, bins)
        if (itemPacked) {
          queue.poll
          pack(queue, bins)
        }
      case _ => logger trace s"[$now] queue was empty"
    }
  }

  private def packItem(item: I, bins: Vector[Bin[S]]): Boolean = {
    val maybeFirstBin = bins.find(_.canFit(item))
    maybeFirstBin match {
      case Some(firstBin) =>
        firstBin.addItem(item)
        logger debug s"[$now] Added $item to $firstBin"
        true
      case _ =>
        // do nothing
        logger warn s"[$now] $item does not fit in any bins"
        false
    }
  }
}
