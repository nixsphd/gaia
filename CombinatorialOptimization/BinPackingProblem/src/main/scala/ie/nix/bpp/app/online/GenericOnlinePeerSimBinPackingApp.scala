package ie.nix.bpp.app.online

import ie.nix.bpp.app.GenericPeerSimBinPackingApp
import ie.nix.bpp.queue.{OnlineQueue, QueueRegister}
import ie.nix.bpp.{Item, Problem, Queue}

import scala.sys.exit

trait GenericOnlinePeerSimBinPackingApp[S, I <: Item[S]]
    extends GenericPeerSimBinPackingApp[S, I]
    with QueueRegister[S, I] {

  override def initQueue(problem: Problem[S, I]): Unit =
    registerQueue(OnlineQueue[S, I](problem.dataSetFileName, problem.queueNumber))

  override def queue: Queue[S, I] = registeredQueue

  def onlineQueue: OnlineQueue[S, I] =
    queue match {
      case queue: OnlineQueue[S, I] => queue
      case _ =>
        logger error s"FATAL: The registeredQueue, $registeredQueue, was not an OnlineQueue"
        exit
    }

  override def initBins(problem: Problem[S, I]): Unit = {
    super[GenericPeerSimBinPackingApp].initBins(problem)
    setBinCapacity(problem.binCapacity)
  }

}
