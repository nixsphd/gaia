package ie.nix.bpp.app.online

import ie.nix.bpp._
import ie.nix.bpp.bin.Item2DPeerSimBinCapacity
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.size.Size2D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D

trait OnlineItem2DPeerSimBinPackingApp extends GenericOnlinePeerSimBinPackingApp[Size2D, Item2D] {

  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
  override implicit def toItem: String => Item2D = Item2D.toItem[Size2D]

  override def setBinCapacity(capacity: Size2D): Unit =
    Item2DPeerSimBinCapacity.capacity(capacity)
}
