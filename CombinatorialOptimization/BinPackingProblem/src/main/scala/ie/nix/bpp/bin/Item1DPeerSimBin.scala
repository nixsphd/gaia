package ie.nix.bpp.bin

import ie.nix.bpp.Size
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.util.Utils.as

class Item1DPeerSimBin(prefix: String) extends GenericPeerSimBin[Size1D](prefix) {

  implicit def sizeLike: Size[Size1D] = SizeLikeSize1D

  override def toString = getIndex match {
    case -1 => s"Item1DPeerSimBin-?[$usedCapacity]"
    case _  => s"Item1DPeerSimBin-$getIndex[$usedCapacity]"
  }

  override def capacity: Size1D = Item1DPeerSimBinCapacity.capacity
}

object Item1DPeerSimBin {

  implicit val toBin: peersim.core.Node => Item1DPeerSimBin =
    node => as[Item1DPeerSimBin](node)

}
object Item1DPeerSimBinCapacity extends PeerSimBinCapacity[Size1D] {
  override implicit def sizeLike: Size[Size1D] = SizeLikeSize1D
}
