package ie.nix.bpp.item

import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.bpp.size.Size2D
import ie.nix.bpp.{DynamicItem, Size}
import org.apache.logging.log4j.scala.Logging

class DynamicItem2D(val size: Size2D, val arrivalTime: Long, val departureTime: Long)(
    implicit val sizeLike: Size[Size2D])
    extends DynamicItem[Size2D]

object DynamicItem2D extends Logging {

  implicit def toDynamicItem2D: String => DynamicItem2D =
    rowValue => {
      val dynamicItemData = rowValue
        .stripPrefix("(")
        .stripSuffix(")")
        .split(" ")
      apply(dynamicItemData(0), dynamicItemData(1).toLong, dynamicItemData(2).toLong)
    }

  def apply(sizeAsString: String, arrivalTime: Long, departureTime: Long)(
      implicit sizeLike: Size[Size2D]): DynamicItem2D =
    new DynamicItem2D(sizeLike.toSize(sizeAsString), arrivalTime, departureTime)

}
