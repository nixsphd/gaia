package ie.nix.bpp.dataset

import ie.nix.bpp.DynamicItem
import org.apache.commons.math3.distribution.PoissonDistribution
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

trait DynamicDataSet extends Logging {
  app: App =>

  def meanArrivalTime: Int
  def meanDepartureTime: Int

}

trait DynamicQueueGenerator[S, I <: DynamicItem[S]] extends QueueGenerator[S, I] {
  config: {
    def meanArrivalTime: Int
    def meanDepartureTime: Int
  } =>

  var arrivalTimeSampler: CumulativePoissonSampler = _
  var departureTimeSampler: PoissonSampler = _

  override def preGenerateQueue(queueNumber: Int): Unit = {
    val seed = queueNumber.toLong
    Random.setSeed(seed)
    arrivalTimeSampler = new CumulativePoissonSampler(seed, meanArrivalTime.toDouble)
    departureTimeSampler = new PoissonSampler(seed, meanDepartureTime.toDouble)
  }

}

class PoissonSampler(seed: Long, mean: Double) {

  protected val poisson = new PoissonDistribution(mean)
  poisson.reseedRandomGenerator(seed)

  def sample: Long = poisson.sample().toLong
}

class CumulativePoissonSampler(seed: Long, mean: Double) {

  protected val poisson = new PoissonDistribution(mean)
  protected var accumulation = 0L

  poisson.reseedRandomGenerator(seed)

  def sample: Long = {
    accumulation += poisson.sample().toLong
    accumulation
  }

  def resetAccumulation(): Unit = accumulation = 0L
}

/*
    Coffman DataSet

    An example illustrating the application of FF is shown in Fig. 1.
    Let m be an integer divisible by 6, and let e = 1/2m

    The list Lm is described as follows:
    First, m items of size 2/3 - e, followed by m items of size 1/3 - e,
    followed by m items of size 2e arrive.

    These are packed by FF as shown in Fig. 1(a).

    Then, all the items of size 1/3 - e depart, and
    a sequence of m items of sizes 1/3, 1/3 + e, 1/3, 1/3 + e, ..., 1/3, 1/3 + e arrives.

    Figure l(b) shows the FF packing at this point, with the number of nonempty bins having increased from m to 3m/2.

    Finally, all the items of size 1/3 + e and 2/3 - e depart, and
    5m/6 items of size 1 arrive.

    The final FF packing, now with 7m/3 nonempty birds, is shown in Fig. l(c).

    Thus FF (Lm)= 7m/3, with the maximum being achieved when the last item arrives.

    1/3 - e, 1/3, 1/3 + e, 2/3 - e, 1
 */
