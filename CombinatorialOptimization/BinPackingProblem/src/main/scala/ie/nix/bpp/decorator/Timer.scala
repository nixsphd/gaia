package ie.nix.bpp.decorator

import ie.nix.bpp._
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.logging.log4j.scala.Logging

case class Timer[S, I <: Item[S]]() extends BinPackingDecorator[S, I] with Logging {

  val RESULTS_DIR = "results"

  private var packingSetStartTime: Long = _

  private var packingQueueStartTime: Long = _
  private var packingQueueStatistics: DescriptiveStatistics = _

  override def beforePacking(problemSet: ProblemSet[S, I], resultsSetName: String): Unit = {
    packingSetStartTime = System.currentTimeMillis
    packingQueueStatistics = new DescriptiveStatistics()
  }

  override def beforePacking(problem: Problem[S, I], resultsSetName: String): Unit = {
    packingQueueStartTime = System.nanoTime
  }

  override def afterPacking(problem: Problem[S, I], problemResult: ProblemResult[S, I]): Unit = {
    val packingQueueElapsedTimeMilliSecs = (System.nanoTime - packingQueueStartTime) / 1000000d
    packingQueueStatistics.addValue(packingQueueElapsedTimeMilliSecs)
  }

  override def afterPacking(problemSet: ProblemSet[S, I], problemResultSet: ProblemResultSet[S, I]): Unit = {

    val packingSetElapsedTimeSecs = (System.currentTimeMillis - packingSetStartTime) / 1000d
    logger info s"Packed ${problemResultSet.problemResults.size} queues in ${packingSetElapsedTimeSecs}s, " +
      s"mean ${"%.3f".format(packingQueueStatistics.getMean)} +/- " +
      s"(${"%.2f".format(packingQueueStatistics.getStandardDeviation)}) msecs"

  }
}
