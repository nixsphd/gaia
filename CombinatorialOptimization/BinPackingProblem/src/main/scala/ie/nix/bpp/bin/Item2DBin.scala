package ie.nix.bpp.bin

import ie.nix.bpp.size.Size2D
import ie.nix.bpp.{Bin, Size}

case class Item2DBin(var _capacity: Size2D)(implicit val sizeLike: Size[Size2D]) extends Bin[Size2D] {

  override def toString = s"Item2DBin[$usedCapacity]"

  override def capacity: Size2D = _capacity
}
