package ie.nix.bpp.observer

import ie.nix.bpp.DynamicItem
import ie.nix.peersim.Control
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

trait DynamicItemBinObserving[S, I <: DynamicItem[S]] extends Logging {

  val binObserverName = "DynamicItemBinObserver"

  def dynamicItemBinObserver: DynamicItemBinObserver[S, I] = {
    Control.controlFor(binObserverName) match {
      case Some(control: DynamicItemBinObserver[S, I]) => control
      case Some(control) =>
        logger error s"FATAL: Could not find control of type $binObserverName, instead ${control.getClass.getSimpleName}"
        exit
      case None =>
        logger error s"FATAL: Could not find the $binObserverName in ${Control.controls}"
        exit
    }
  }

}
