package ie.nix.bpp.app.gc

import ie.nix.bpp.app.online.OnlineItem1DPeerSimBinPackingApp
import org.apache.logging.log4j.scala.Logging

object GCItem1DBinPackerApp extends OnlineItem1DPeerSimBinPackingApp with Logging {

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/gc/GCBinPacker.properties")

  silent()
  initRandom()
  packProblems()

  override def resultsSetName = "GCItem1DBinPacker"
}
