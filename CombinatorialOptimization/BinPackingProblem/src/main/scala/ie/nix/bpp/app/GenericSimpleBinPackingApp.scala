package ie.nix.bpp.app

import ie.nix.bpp.packer.GenericSimplePacker
import ie.nix.bpp._
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

trait GenericSimpleBinPackingApp[S, I <: Item[S]]
    extends App
    with BinPacking[S, I]
    with GenericSimpleBinPackingAppArgs[S]
    with Logging {

  var queue: Queue[S, I] = _
  var bins: Vector[Bin[S]] = _
  var packer: GenericSimplePacker[S, I] = _

  def initRandom(): Unit = Random.setSeed(0)

  override def problemSet: ProblemSet[S, I] =
    ProblemSet[S, I](dataSetFileName, numberOfBins, binCapacity)

  override def pack(problem: Problem[S, I]): ProblemResult[S, I] = {
    initQueue(problem)
    initBins(problem)
    initPacker()
    packer.pack(queue, bins)
    ProblemResult(queue, bins)
  }

  def initBins(problem: Problem[S, I]): Unit
  def initQueue(problem: Problem[S, I]): Unit
  def initPacker(): Unit

}
