package ie.nix.bpp.packer.lgc.item1d

import ie.nix.bpp.packer.lgc.item1d.LGCItem1DBinPackerEnvironment.{AlmostFullState, EmptyState, binWithItemStatesSet}
import ie.nix.lgc.LGCEnvironment.{GCState, LGCState}
import ie.nix.lgc.{LGCEnvironment, LGCEnvironmentBuilder}
import ie.nix.rl.Environment.State
import ie.nix.rl.Rewards
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.Transitions.Transition

class LGCItem1DBinPackerEnvironment(prefix: String)
    extends LGCEnvironment(
      LGCEnvironmentBuilder(prefix)
//        .addPreTenderAwardCycleStates(binStatesSet)
        .addShouldTenderStates(binWithItemStatesSet)
        .addGetGuideStates(binWithItemStatesSet)
        .addShouldBidStates(binWithItemStatesSet)
        .addGetOfferStates(binWithItemStatesSet)
        .addShouldAwardStates(binWithItemStatesSet)
        .addAwardStates(binWithItemStatesSet)
        .addAwardedStates(binWithItemStatesSet)
//        .addPostTenderAwardCycleStates(binStatesSet)
    )
    with Rewards {

  logger debug s"number of stateActions=${getStateActions.size}"
  logger debug s"stateActions=${getStateActions.sortBy(_.toString()).mkString("\n", "\n", "\n")}"

  override def toString: String = s"SnapLGCEnvironment"

  override def getReward(transition: Transition): Reward = {
    val reward = transition match {
      case Transition(_, LGCState(_, AlmostFullState, _)) => {
        logger debug s"REWARDING $transition"
        1d
      }
      case Transition(_, LGCState(_, EmptyState, _)) => {
        logger debug s"REWARDING $transition"
        1d
      }
//      case Transition(_, LGCState(PostTenderAwardCycle, AlmostFullState, _)) => {
//        logger debug s"REWARDING $transition"
//        1d
//      }
//      case Transition(_, LGCState(PostTenderAwardCycle, EmptyState, _)) => {
//        logger debug s"REWARDING $transition"
//        1d
//      }
      case _ => {
        logger debug s"Not rewarding $transition"
        0d
      }
    }
    logger debug s"${transition.finalState} gets reward $reward"
    reward
  }

}

object LGCItem1DBinPackerEnvironment {

  case class BinGCState(gcState: GCState, binState: BinState) extends State

  sealed trait BinState extends State
  object EmptyState extends BinState { override def toString: String = "Empty" }
  object LessThanTwoThirdsFullState extends BinState {
    override def toString: String = "LessThanTwoThirdsFull"
  }
  object MoreThanTwoThirdsFullState extends BinState {
    override def toString: String = "MoreThanTwoThirdsFull"
  }
  object AlmostFullState extends BinState {
    override def toString: String = "AlmostFull"
  }
  object CouldBeLessThanTwoThirdsFullState extends BinState {
    override def toString: String = "CouldBeLessThanTwoThirdsFull"
  }
  object CouldBeMoreThanTwoThirdsFullState extends BinState {
    override def toString: String = "CouldBeMoreThanTwoThirdsFull"
  }
  object CouldBeAlmostFullState extends BinState {
    override def toString: String = "CouldBeAlmostFull"
  }

  def binStatesSet: Set[State] =
    Set[State](EmptyState, LessThanTwoThirdsFullState, MoreThanTwoThirdsFullState, AlmostFullState)

  def binWithItemStatesSet: Set[State] =
    Set[State](EmptyState,
               CouldBeLessThanTwoThirdsFullState,
               CouldBeMoreThanTwoThirdsFullState,
               CouldBeAlmostFullState,
               AlmostFullState)

}
