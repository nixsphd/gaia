package ie.nix.bpp.app.online

import ie.nix.bpp.app.GenericSimpleBinPackingApp
import ie.nix.bpp.queue.OnlineQueue
import ie.nix.bpp.{Item, Problem}

trait GenericOnlinePackingApp[S, I <: Item[S]] extends GenericSimpleBinPackingApp[S, I] {

  def initQueue(problem: Problem[S, I]): Unit =
    queue = OnlineQueue[S, I](problem.dataSetFileName, problem.queueNumber)

  def initBins(problem: Problem[S, I]): Unit

}
