package ie.nix.bpp.decorator

import ie.nix.bpp.{Item, Problem, ProblemResult}
import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging

case class Integrity[S, I <: Item[S]]() extends BinPackingDecorator[S, I] with Logging {

  override def afterPacking(problem: Problem[S, I], problemResult: ProblemResult[S, I]): Unit = {
    val hasAllItems = problem.numberOfItemsQueued -
      (problemResult.numberOfItemsQueued + problemResult.numberOfItemsPacked) == 0
    val hasOverloadedBins = problemResult.numberOfOverloadedBins > 0
    val hasIntegrity = hasAllItems && !hasOverloadedBins
    if (!hasIntegrity)
      logger error s"[$now] hasIntegrity=$hasIntegrity, hasAllItems=$hasAllItems, " +
        s"numberOfOverloadedBins=${problemResult.numberOfOverloadedBins}"
    else
      logger debug s"[$now] hasIntegrity=$hasIntegrity"
  }

}
