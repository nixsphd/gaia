package ie.nix.bpp.bin

import ie.nix.bpp.Size
import ie.nix.bpp.size.Size2D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import ie.nix.util.Utils.as

class Item2DPeerSimBin(prefix: String) extends GenericPeerSimBin[Size2D](prefix) { //with PeerSimBinCapacity[Size2D] {

  implicit def sizeLike: Size[Size2D] = SizeLikeSize2D

  override def toString = getIndex match {
    case -1 => s"Item1DPeerSimBin-?[$usedCapacity]"
    case _  => s"Item1DPeerSimBin-$getIndex[$usedCapacity]"
  }

  override def capacity: Size2D = Item2DPeerSimBinCapacity.capacity
}

object Item2DPeerSimBin {

  implicit val toBin: peersim.core.Node => Item2DPeerSimBin =
    node => as[Item2DPeerSimBin](node)

}

object Item2DPeerSimBinCapacity extends PeerSimBinCapacity[Size2D] {
  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
}
