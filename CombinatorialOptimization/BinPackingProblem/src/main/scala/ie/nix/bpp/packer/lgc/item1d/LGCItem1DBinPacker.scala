package ie.nix.bpp.packer.lgc.item1d

import ie.nix.bpp.app.lgc.LGCItem1DBinPackerApp
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.gc.item1d.GCItem1DBinPacker
import ie.nix.bpp.packer.lgc.item1d.LGCItem1DBinPacker.{ALMOST_FULL, EMPTY, TWO_THIRDS}
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.{Queue, Size}
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.lgc.LGCAgent
import ie.nix.peersim.Node
import ie.nix.rl.Environment.{NullState, State}
import org.apache.logging.log4j.scala.Logging

class LGCItem1DBinPacker(prefix: String) extends GCItem1DBinPacker(prefix) with LGCAgent[Item1D, Void] with Logging {

  implicit def sizeLike: Size[Size1D] = SizeLikeSize1D

  override def queue: Queue[Size1D, Item1D] = LGCItem1DBinPackerApp.queue

  override def shouldTender(node: Node): Boolean =
    hasTask && super[LGCAgent].shouldTender(node)

  override def shouldBid(tenderMessage: TenderMessage[Item1D]): Boolean =
    doesNotHaveATask &&
      bidderCanFitItem(tenderMessage) &&
      super[LGCAgent].shouldBid(tenderMessage)

  override protected def defaultPreTenderAwardCycle(node: Node): Unit =
    super[GCItem1DBinPacker].preTenderAwardCycle(node)

  override protected def defaultShouldTender(node: Node): Boolean =
    super[GCItem1DBinPacker].shouldTender(node)

  override protected def defaultGetGuide(node: Node, item: Item1D): Double =
    super[GCItem1DBinPacker].getGuide(node, item)

  override protected def defaultShouldBid(tenderMessage: TenderMessage[Item1D]): Boolean =
    super[GCItem1DBinPacker].shouldBid(tenderMessage)

  override protected def defaultGetOffer(tenderMessage: TenderMessage[Item1D], proposal: Void): Double =
    super[GCItem1DBinPacker].getOffer(tenderMessage, proposal)

  override protected def defaultShouldAward(bidMessage: BidMessage[Item1D, Void]): Boolean =
    super[GCItem1DBinPacker].shouldAward(bidMessage)

  override protected def defaultAward(awardMessage: AwardMessage[Item1D, Void]): Unit =
    super[GCItem1DBinPacker].award(awardMessage)

  override protected def defaultAwarded(awardMessage: AwardMessage[Item1D, Void]): Unit =
    super[GCItem1DBinPacker].awarded(awardMessage)

  override protected def defaultPostTenderAwardCycle(node: Node): Unit =
    super[GCItem1DBinPacker].postTenderAwardCycle(node)

//  override def mapToStateForPreTenderAwardCycle(node: Node): State =
//    mapToState(node)

  override def mapToStateForShouldTender(node: Node): State =
    mapToState(node, getTask)

  override def mapToStateForGetGuide(node: Node, item: Item1D): State =
    mapToState(node, item)

  override def mapToStateForShouldBid(tenderMessage: TenderMessage[Item1D]): State =
    mapToState(tenderMessage.to, tenderMessage.task)

  override def mapToStateForGetOffer(tenderMessage: TenderMessage[Item1D], proposal: Void): State =
    mapToState(tenderMessage.to, tenderMessage.task)

  override def mapToStateForShouldAward(bidMessage: BidMessage[Item1D, Void]): State =
    mapToState(bidMessage.to, bidMessage.task)

  override def mapToStateForAward(awardMessage: AwardMessage[Item1D, Void]): State =
    mapToState(awardMessage.from, awardMessage.task)

  override def mapToStateForAwarded(awardMessage: AwardMessage[Item1D, Void]): State =
    mapToState(awardMessage.to, awardMessage.task)

//  override def mapToStateForPostTenderAwardCycle(node: Node): State =
//    mapToState(node)

  def bidderCanFitItem(tenderMessage: TenderMessage[Item1D]): Boolean =
    bin(tenderMessage.to).canFit(tenderMessage.task)

  def mapToState(node: Node): State = {
    val state = normalisedUsedCapacity(node) match {
      case usedCapacity if usedCapacity == EMPTY =>
        LGCItem1DBinPackerEnvironment.EmptyState
      case usedCapacity if usedCapacity < TWO_THIRDS =>
        LGCItem1DBinPackerEnvironment.LessThanTwoThirdsFullState
      case usedCapacity if usedCapacity < ALMOST_FULL =>
        LGCItem1DBinPackerEnvironment.MoreThanTwoThirdsFullState
      case usedCapacity if usedCapacity >= ALMOST_FULL =>
        LGCItem1DBinPackerEnvironment.AlmostFullState
      case otherSize =>
        logger error s"Can not match free capacity $otherSize to a bin state"; NullState
    }
    logger debug s"$node -> ${normalisedUsedCapacity(node)} -> $state"
    state
  }

  def mapToState(node: Node, item: Item1D): State = {
    val state = (normalisedUsedCapacity(node), normalisedUsedCapacityWithItem(node, item)) match {
      case (usedCapacity, _) if usedCapacity == EMPTY =>
        LGCItem1DBinPackerEnvironment.EmptyState
      case (usedCapacity, _) if usedCapacity >= ALMOST_FULL =>
        LGCItem1DBinPackerEnvironment.AlmostFullState
      case (_, usedCapacityWithItem) if usedCapacityWithItem < TWO_THIRDS =>
        LGCItem1DBinPackerEnvironment.CouldBeLessThanTwoThirdsFullState
      case (_, usedCapacityWithItem) if usedCapacityWithItem < ALMOST_FULL =>
        LGCItem1DBinPackerEnvironment.CouldBeMoreThanTwoThirdsFullState
      case (_, usedCapacityWithItem) if usedCapacityWithItem >= ALMOST_FULL =>
        LGCItem1DBinPackerEnvironment.CouldBeAlmostFullState
      case otherSize =>
        logger error s"Can not match free capacity $otherSize to a bin state"; NullState
    }
    logger debug s"$node -> ${normalisedUsedCapacity(node)} -> $state"
    state
  }

  def normalisedUsedCapacity(node: Node): Double =
    bin(node).usedCapacity.size.toDouble / bin(node: Node).capacity.size.toDouble

  def normalisedUsedCapacityWithItem(node: Node, item: Item1D): Double =
    (bin(node).usedCapacity.size.toDouble + item.size.size.toDouble) / bin(node: Node).capacity.size.toDouble

}

object LGCItem1DBinPacker {
  val EMPTY: Double = 0.0
  val TWO_THIRDS: Double = 0.66666
  val ALMOST_FULL: Double = 0.95
}
