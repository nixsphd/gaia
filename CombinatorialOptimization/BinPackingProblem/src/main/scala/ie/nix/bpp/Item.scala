package ie.nix.bpp

import org.apache.logging.log4j.scala.Logging

trait Item[S] extends Logging {
  implicit val sizeLike: Size[S]
  val size: S

  require(sizeLike.isPositive(size))

  override def toString: String =
    s"${getClass.getSimpleName}-${Item.itemToString[S].apply(this)}"

}

object Item {

  implicit def itemToString[S: Size]: Item[S] => String =
    item => s"${implicitly[Size[S]].toString(item.size)}"

}
