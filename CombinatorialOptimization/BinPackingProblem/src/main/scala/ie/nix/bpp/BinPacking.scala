package ie.nix.bpp

import ie.nix.bpp.decorator.{BinPackingDecorator, Integrity, Observer, Statistics, Timer}
import org.apache.logging.log4j.scala.Logging

trait BinPacking[S, I <: Item[S]] extends BinPackingDecorator[S, I] with Logging {

  implicit def sizeLike: Size[S]
  implicit def toItem: String => I

  val decorators: Vector[BinPackingDecorator[S, I]] =
    Vector[BinPackingDecorator[S, I]]() :+ Integrity[S, I]() :+ Statistics[S, I]() :+ Observer[S, I]() :+ Timer[S, I]()

  def packProblems(): Unit = {
    logger debug s"Packing ${problemSet.problems.size} problems"
    beforePacking(problemSet, resultsSetName)
    val problemResults = problemSet.problems.map(problem => packProblem(problem))
    afterPacking(problemSet, ProblemResultSet(resultsSetName, problemSet, problemResults))
  }
  def problemSet: ProblemSet[S, I]
  def resultsSetName: String

  def packProblem(problem: Problem[S, I]): ProblemResult[S, I] = {
    logger debug s"Packing problem ${problem.queueNumber}"
    beforePacking(problem, resultsSetName)
    val problemResult = pack(problem)
    afterPacking(problem, problemResult)
    problemResult
  }
  def pack(problem: Problem[S, I]): ProblemResult[S, I]

  override def beforePacking(problemSet: ProblemSet[S, I], resultsSetName: String): Unit =
    decorators.foreach(_.beforePacking(problemSet, resultsSetName))

  override def beforePacking(problem: Problem[S, I], resultsSetName: String): Unit =
    decorators.foreach(_.beforePacking(problem, resultsSetName))

  override def afterPacking(problem: Problem[S, I], problemResult: ProblemResult[S, I]): Unit =
    decorators.foreach(_.afterPacking(problem, problemResult))

  override def afterPacking(problemSet: ProblemSet[S, I], problemResultSet: ProblemResultSet[S, I]): Unit =
    decorators.foreach(_.afterPacking(problemSet, problemResultSet))

}
