package ie.nix.bpp.packer.egc.item1d

import ie.nix.bpp.Queue
import ie.nix.bpp.app.egc.EvolvedItem1DBinPackerApp
import ie.nix.bpp.item.Item1D
import ie.nix.bpp.packer.egc.GenericEvolved1DBinPacker
import ie.nix.bpp.packer.gc.GenericGC1DBinPacker
import ie.nix.bpp.size.Size1D
import org.apache.logging.log4j.scala.Logging

class EvolvedItem1DBinPacker(prefix: String)
    extends GenericGC1DBinPacker[Item1D](prefix)
    with GenericEvolved1DBinPacker[Item1D]
    with Logging {

  override def queue: Queue[Size1D, Item1D] = EvolvedItem1DBinPackerApp.queue

  def shouldTender(freeCapacity: Double, itemSize: Double): Boolean = true
  def getGuide(freeCapacity: Double, itemSize: Double): Double = freeCapacity
  def tenderToDouble(guide: Double, freeCapacity: Double, itemSize: Double): Double =
    ifd(freeCapacity > itemSize, -itemSize, Double.PositiveInfinity)
  def shouldBid(guide: Double, freeCapacity: Double, itemSize: Double): Boolean = true
  def getOffer(guide: Double, freeCapacity: Double, itemSize: Double): Double = freeCapacity
  def bidToDouble(offer: Double, freeCapacity: Double, itemSize: Double): Double = offer
  def shouldAward(offer: Double, freeCapacity: Double, itemSize: Double): Boolean = true

}
