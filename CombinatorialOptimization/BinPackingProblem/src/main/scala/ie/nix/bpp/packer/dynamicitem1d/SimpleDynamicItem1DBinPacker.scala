package ie.nix.bpp.packer.dynamicitem1d

import ie.nix.bpp.app.dynamic.{DynamicItem1DPackingApp, GenericDynamicItemBinPackingApp}
import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.size.Size1D
import ie.nix.bpp.size.Size1D.SizeLikeSize1D
import ie.nix.bpp.{Bin, DynamicItem, Queue, Size}
import ie.nix.peersim.Control
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

trait GenericSimpleDynamicBinPackerApp[S, I <: DynamicItem[S]]
    extends GenericDynamicItemBinPackingApp[S, I]
    with Logging {

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String]("src/main/resources/ie/nix/bpp/SimpleDynamicBinPacker.properties")

  override def resultsSetName = { initConfiguration(); Configuration.getString("control.bin_packer") }

}

trait GenericSimpleDynamicBinPacker[S, I <: DynamicItem[S]] extends Control with Logging {

  override def toString: String = s"${getClass.getSimpleName}"

  override def step(): Boolean = {
    pack(queue, bins);
    false
  }

  def queue: Queue[S, I]
  def bins: Vector[Bin[S]]

  def pack(queue: Queue[S, I], bins: Vector[Bin[S]]): Unit

}

object SimpleDynamicItem1DBinPackerApp
    extends GenericSimpleDynamicBinPackerApp[Size1D, DynamicItem1D]
    with DynamicItem1DPackingApp
    with Logging {

  silent()
  initRandom()
  packProblems()

}

trait SimpleDynamicItem1DBinPacker extends GenericSimpleDynamicBinPacker[Size1D, DynamicItem1D] with Logging {

  implicit def sizeLike: Size[Size1D] = SizeLikeSize1D

  def queue: Queue[Size1D, DynamicItem1D] = SimpleDynamicItem1DBinPackerApp.queue
  def bins: Vector[Bin[Size1D]] = SimpleDynamicItem1DBinPackerApp.bins

}
