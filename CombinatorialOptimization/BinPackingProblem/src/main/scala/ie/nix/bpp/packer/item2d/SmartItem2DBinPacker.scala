package ie.nix.bpp.packer.item2d

import ie.nix.bpp.app.online.OnlineItem2DPeerSimBinPackingApp
import ie.nix.bpp.bin.Item2DPeerSimBin
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.queue.OnlineQueue
import ie.nix.bpp.size.Size2D
import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.Utils.now
import ie.nix.peersim.{Node, Protocol}
import org.apache.logging.log4j.scala.Logging

import scala.util.Random

object SmartItem2DBinPackerApp extends OnlineItem2DPeerSimBinPackingApp with Logging {

  silent()
  initRandom()
  packProblems()

  override def propertiesOverrides: Array[String] =
    super.propertiesOverrides ++
      Array[String](
        "src/main/resources/ie/nix/bpp/SmartBinPacker.properties",
        "network.node=ie.nix.bpp.bin.Item2DPeerSimBin",
        "protocol.smart_bin_packer=SmartItem2DBinPacker"
      )

  override def resultsSetName = "SmartItem2DBinPacker"
}

class SmartItem2DBinPacker(prefix: String) extends Protocol(prefix) with Logging {

  override def toString: String = s"${getClass.getSimpleName}"

  override def nextCycle(node: Node): Unit = {
    logger trace s"[$now] node=$node"
    node match {
      case bin: Item2DPeerSimBin => nextCycle(bin)
      case _                     => logger warn s"[$now] was expecting $node to be a SmartBin"
    }
  }

  def nextCycle(bin: Item2DPeerSimBin): Unit = {
    logger trace s"[$now] bin=$bin, queue=$queue, ${queue.peek}"
    queue.peek match {
      case Some(item) =>
        if (bin.canFit(item)) {
          if (shouldPackItem(bin)) {
            val itemPacked = packItem(item, bin)
            if (itemPacked) queue.poll
          }
        }
      case _ => logger trace s"[$now] queue was empty"
    }
  }

  def queue: OnlineQueue[Size2D, Item2D] = SmartItem2DBinPackerApp.onlineQueue

  def shouldPackItem(bin: Item2DPeerSimBin): Boolean = {
    val relativeUsedCapacity =
      ((bin.usedCapacity.size1.toDouble / bin.capacity.size1.toDouble) +
        (bin.usedCapacity.size2.toDouble / bin.capacity.size2.toDouble)) / 2
    val rand = Random.nextDouble() - 0.15
    val shouldPackItem = rand < relativeUsedCapacity
    logger debug s"shouldPackItem=$shouldPackItem = rand=$rand < relativeUsedCapacity=$relativeUsedCapacity"
    shouldPackItem
  }

  private def packItem(item: Item2D, bin: Item2DPeerSimBin): Boolean = {
    if (bin.canFit(item)) {
      bin.addItem(item)
      logger debug s"[$now] Added $item to $bin"
      true
    } else {
      // do nothing
      logger debug s"[$now] $item does not fit in this bin, $bin"
      false
    }
  }

  override def processEvent(node: Node, message: Message): Unit = {}
}
