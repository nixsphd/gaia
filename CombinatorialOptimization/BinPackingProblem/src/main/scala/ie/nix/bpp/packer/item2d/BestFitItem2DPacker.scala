package ie.nix.bpp.packer.item2d

import ie.nix.bpp.Size
import ie.nix.bpp.app.online.OnlineItem2DBinPackingApp
import ie.nix.bpp.item.Item2D
import ie.nix.bpp.packer.GenericBestFitPacker
import ie.nix.bpp.size.Size2D
import ie.nix.bpp.size.Size2D.SizeLikeSize2D
import org.apache.logging.log4j.scala.Logging

object BestFitItem2DPackerApp extends OnlineItem2DBinPackingApp with Logging {

  initRandom()
  packProblems()

  override def resultsSetName: String = "BestFitItem2DPacker"
  override def initPacker(): Unit =
    packer = BestFitItem2DPacker()

}

case class BestFitItem2DPacker() extends GenericBestFitPacker[Size2D, Item2D] {
  override implicit def sizeLike: Size[Size2D] = SizeLikeSize2D
}
