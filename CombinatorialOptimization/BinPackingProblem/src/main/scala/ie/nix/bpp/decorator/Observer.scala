package ie.nix.bpp.decorator

import ie.nix.bpp.{Item, Problem, ProblemResult, ProblemSet}
import ie.nix.util.CSVFileWriter
import org.apache.logging.log4j.scala.Logging

case class Observer[S, I <: Item[S]]() extends BinPackingDecorator[S, I] with Logging {

  val RESULTS_DIR = "results"

  var maybeObserver: Option[CSVFileWriter] = None

  override def beforePacking(problemSet: ProblemSet[S, I], resultsSetName: String): Unit =
    maybeObserver match {
      case None => {
        val theLogDir = logDir(problemSet, resultsSetName)
        val observer = CSVFileWriter(theLogDir, "BinObserver.csv")
        observer.writeHeaders("Number Of Items Queued", "Open Bins")
        maybeObserver = Some(observer)
        logger info s"Logging to $theLogDir"
      }
      case _ =>
    }

  override def afterPacking(problem: Problem[S, I], problemResult: ProblemResult[S, I]): Unit =
    maybeObserver match {
      case Some(observer) =>
        observer.writeRow(problemResult.numberOfItemsQueued, problemResult.numberOfOpenBins)
      case _ =>
    }

  def logDir(problemSet: ProblemSet[S, I], resultsSetName: String): String =
    s"$RESULTS_DIR/${problemSet.name}/${resultsSetName}"

}
