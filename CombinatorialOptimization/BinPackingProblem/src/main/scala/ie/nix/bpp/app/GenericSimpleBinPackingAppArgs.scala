package ie.nix.bpp.app

import ie.nix.bpp.Size

trait GenericSimpleBinPackingAppArgs[S] {
  app: App =>

  protected val USAGE = "Please pass in three argument structured as follows: " +
    "dataSetFileName numberOfBins binCapacity"

  def checkArgs(): Unit = {
    assert(args != null, s"There were no args, but should be 3.\n$USAGE")
    assert(args.length == 3, s"There was not enough args; there was ${args.length} but should be 3.\n$USAGE")
  }
  def dataSetFileName: String = { checkArgs(); s"${args(0)}" }
  def numberOfBins: Int = { checkArgs(); args(1).toInt }
  def binCapacityFromArgs: String = { checkArgs(); args(2) }
  def binCapacity(implicit sizeLike: Size[S]): S = sizeLike.toSize(binCapacityFromArgs)
}
