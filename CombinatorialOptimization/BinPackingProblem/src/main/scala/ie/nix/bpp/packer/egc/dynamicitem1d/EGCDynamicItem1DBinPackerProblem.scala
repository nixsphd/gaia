package ie.nix.bpp.packer.egc.dynamicitem1d

import ie.nix.bpp.app.dynamic.DynamicItem1DPackingApp
import ie.nix.bpp.item.DynamicItem1D
import ie.nix.bpp.packer.egc.GenericEGCDynamicBinPackerProblem
import ie.nix.bpp.size.Size1D

class EGCDynamicItem1DBinPackerProblem
    extends GenericEGCDynamicBinPackerProblem[Size1D, DynamicItem1D]
    with DynamicItem1DPackingApp {

  override def properties: Array[String] =
    super[GenericEGCDynamicBinPackerProblem].properties ++
      super[DynamicItem1DPackingApp].propertiesOverrides
}
