package ie.nix.bpp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class BinTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	@Test
	@DisplayName("Add items")
	public void addItems() {
		Bin bin = new Bin(100);
		
		Item item1 = new Item(10);
		Item item2 = new Item(40);
		Item item3 = new Item(70);
		
		bin.addItem(item1);
		bin.addItem(item2);
		
		boolean caughtAssertionError = false;
		try {
			bin.addItem(item3);
		} catch (AssertionError e) {
			caughtAssertionError = true;
		}
		
		Assertions.assertTrue(caughtAssertionError, "An assertion should have been thrown");
		
		Assertions.assertTrue(bin.hasItem(item1), "Bin should contain " + item1 + ".");
		Assertions.assertTrue(bin.hasItem(item2), "Bin should contain " + item2 + ".");
		Assertions.assertTrue(!bin.hasItem(item3), "Bin should not contain " + item3 + ".");

	}
	
	@Test
	@DisplayName("Sort Bins")
	public void sortBins() {
		
		// Free Space = 90
		Bin bin1 = new Bin(100);
		bin1.addItem(new Item(10));

		// Free Space = 80
		Bin bin2 = new Bin(100);
		bin2.addItem(new Item(10));
		bin2.addItem(new Item(10));
		
		// Free Space = 100
		Bin bin3 = new Bin(100);
		
		List<Bin> actualBins = new ArrayList<Bin>();
		actualBins.add(bin1);
		actualBins.add(bin2);
		actualBins.add(bin3);
		
		List<Bin> expectedBins = new ArrayList<Bin>();
		expectedBins.add(bin2);
		expectedBins.add(bin1);
		expectedBins.add(bin3);

		Collections.sort(actualBins);
		
		LOGGER.debug("expectedTenderMessages={}", expectedBins);
		LOGGER.debug("actualTenderMessages={}", actualBins);

		Assertions.assertEquals(expectedBins, actualBins);

	}
}
