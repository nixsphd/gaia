package ie.nix.bpp.gc;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.bpp.Bin;
import ie.nix.bpp.peersim.Queue;
import peersim.core.CommonState;
import peersim.edsim.EDSimulator;

public class SmartBinTest_2_2 {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String BIN_PACKING_PROBLEM_PROPERTIES = 
			"src/main/resources/ie/nix/bpp/BinPackingProblem.properties";
	private static final String SMART_BIN_PROPERTIES   = 
			"src/main/resources/ie/nix/bpp/gc/SmartBin.properties";

	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				BIN_PACKING_PROBLEM_PROPERTIES,
				SMART_BIN_PROPERTIES,
				"control.queue.bpp_file=UniformBinPackingProblem-20-100-2.csv",
				"control.queue.run=0",
				"network.size=2",
				"simulation.endtime=301"});
		
	}

	@Test
	@DisplayName("Two Bin Two Item")
	public void twoBinTwoItem() {
	
		CommonState.initializeRandom(0);
		EDSimulator.nextExperiment();
		
		Queue queue = SmartBinTests.getQueue();
		Assertions.assertTrue(queue.isEmpty(), "Queue should be empty.");
		
		ArrayList<Bin> bins = SmartBinTests.getSortedBins();
		Bin bin0 = bins.get(0);
		Assertions.assertTrue(bin0.getNumberOfItems() == 2, "Bin should have two items, bin0="+bin0+".");
		
		Stream<SmartBin> binAgents = SmartBinTests.getAgents();
		int numberOfTasks = (int)binAgents.filter(agent -> ((SmartBin)agent).hasTask()).count();
		Assertions.assertTrue(numberOfTasks == 0, "There are tasks in the agents="+numberOfTasks+".");
		
		LOGGER.info("Done.");

	}
}
