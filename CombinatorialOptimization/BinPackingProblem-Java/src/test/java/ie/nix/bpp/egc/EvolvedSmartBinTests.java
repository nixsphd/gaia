package ie.nix.bpp.egc;	

import java.security.Permission;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.ecj.PeerSimProblem;

public class EvolvedSmartBinTests {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String EGC_AGENT_PARAMS = 
			"src/main/resources/ie/nix/bpp/egc/EvolvedSmartBin.params";
	
	/*
	 * Helper methods
	 */
	@BeforeAll
	static void beforeAll() {
			
		System.setSecurityManager(new SecurityManager() {

			@Override
			public void checkPermission(Permission perm) {}

			@Override
			public void checkExit(int status) {
				/* Don't allow exit with any status code. */
				throw new SecurityException();
			}

		});
	}
	
	@Test()
	@DisplayName("Evolved Smart Bin Test")
	public void evolvedSmartBinTest() {
		String[] properties = new String[]{
				"-file", EGC_AGENT_PARAMS,
//				"-file", "src/main/resources/exp1/EvolvedSmartBin.params",
				"-p", "seed.0=0",
				"-p", "checkpoint=false",
//				"-p", "gp.problem.shouldTenderTreeIndex=-1",
//				"-p", "gp.problem.getGuideTreeIndex=-1",
//				"-p", "gp.problem.tenderToDoubleTreeIndex=-1",
//				"-p", "gp.problem.shouldBidTreeIndex=-1",
//				"-p", "gp.problem.getOfferTreeIndex=-1",
//				"-p", "gp.problem.bidToDoubleTreeIndex=-1",
//				"-p", "gp.problem.shouldAwardTreeIndex=-1",
				"-p", "eval.problem.0.bpp_file=UniformBinPackingProblem-20-100-10.csv",
				"-p", "eval.problem.bpp_file.size=1",
				"-p", "eval.problem.number_of_runs=1",
				"-p", "generations=10", //10
				"-p", "pop.subpop.0.size=200", //200
//				"-p", "eval.problem.logging=console",
//				"-p", "silent=false",
			};	
		double expectedFitness = 1.0;
		testProblem(properties, expectedFitness);
	}
	
	@Test()
	@DisplayName("Evolved Smart Bin Problem Test")
	public void evolvedSmartBinProblemTest() {
		String[] properties = new String[]{
				"-file", EGC_AGENT_PARAMS,
				"-p", "seed.0=0",
				"-p", "eval.problem.shouldTenderTreeIndex=-1",
				"-p", "eval.problem.getGuideTreeIndex=-1",
				"-p", "eval.problem.tenderToDoubleTreeIndex=-1",
				"-p", "eval.problem.shouldBidTreeIndex=-1",
				"-p", "eval.problem.getOfferTreeIndex=-1",
				"-p", "eval.problem.bidToDoubleTreeIndex=-1",
				"-p", "eval.problem.shouldAwardTreeIndex=-1",
				"-p", "eval.problem.0.bpp_file=UniformBinPackingProblem-20-100-10.csv",
				"-p", "eval.problem.bpp_file.size=1",
				"-p", "eval.problem.number_of_runs=1",
				"-p", "generations=1",
				"-p", "pop.subpop.0.size=10",
//				"-p", "eval.problem.logging=console",
//				"-p", "silent=false",
			};	
		double expectedFitness = 1.0;
		testProblem(properties, expectedFitness);
	}
	
	public void testProblem(String[] properties, double expectedFitness) {

		Assertions.assertThrows(RuntimeException.class, () -> {
			ec.Evolve.main(properties);
		});
		
		double bestAdjustedFitness = PeerSimProblem.getGPProblem().getBestAdjustedFitness();
		
		LOGGER.debug("bestAdjustedFitness={}", bestAdjustedFitness);		
		
		Assertions.assertEquals(expectedFitness, bestAdjustedFitness, 
				"Did not get a good enough fitness "+bestAdjustedFitness+".");
	}
	
}