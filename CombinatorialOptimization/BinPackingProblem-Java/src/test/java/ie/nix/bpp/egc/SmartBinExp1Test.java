package ie.nix.bpp.egc;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.bpp.Bin;
import ie.nix.bpp.gc.SmartBinTests;
import ie.nix.bpp.peersim.BinAdapter;
import ie.nix.bpp.peersim.Queue;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.edsim.EDSimulator;

public class SmartBinExp1Test {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String BIN_PACKING_PROBLEM_PROPERTIES = 
			"src/main/resources/ie/nix/bpp/BinPackingProblem.properties";
	private static final String SMARTBIN_PROPERTIES   = 
			"src/main/resources/exp1/SmartBinExp1.properties";

	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				BIN_PACKING_PROBLEM_PROPERTIES,
				SMARTBIN_PROPERTIES,
				"control.queue.bpp_file=UniformBinPackingProblem-20-100-10.csv",
				"network.size=10",
			});	
	}

	/*
	 * Helper methods
	 */
	private static int agentProtocol = 0;
	
	public Stream<SmartBinExp1> getAgents() {
		Stream.Builder<SmartBinExp1> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add((SmartBinExp1)getAgent(n));
		}
		return streamBuilder.build();
	}
	
	public static SmartBinExp1 getAgent(int n) {
		return ((SmartBinExp1)((BinAdapter)Network.get(n)).getProtocol(agentProtocol));
	}
	
	/*
	 * System Tests
	 */
	@Test
	@DisplayName("Exp1 SmartBin Test")
	public void exp1SmartBinTest() {
	
		CommonState.initializeRandom(0);
		EDSimulator.nextExperiment();
		
		Queue queue = SmartBinTests.getQueue();
		LOGGER.info("queue.isEmpty()={}", queue.isEmpty());
		Assertions.assertTrue(queue.isEmpty(), 
				"Queue should be empty.");
		
		Stream<SmartBinExp1> binAgents = getAgents();
		int numberOfTasks = (int)binAgents.filter(
				agent -> ((SmartBinExp1)agent).hasTask()).count();
		LOGGER.info("numberOfTasks={}", numberOfTasks);
		Assertions.assertTrue(numberOfTasks == 0, 
				"There are tasks in the agents="+numberOfTasks+".");
		
		ArrayList<Bin> bins = SmartBinTests.getBins();
		long openBins = bins.stream().filter(
				bin -> !bin.isEmpty()).count();
		LOGGER.info("openBins={}", openBins);
		Assertions.assertTrue(openBins <= 4, 
				"There should be no more than 4 openBins, "
				+ "but are "+openBins+".");
		
		LOGGER.info("Done.");

	}
}
