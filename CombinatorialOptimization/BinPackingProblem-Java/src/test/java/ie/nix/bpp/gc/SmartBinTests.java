package ie.nix.bpp.gc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.bpp.Bin;
import ie.nix.bpp.Item;
import ie.nix.bpp.peersim.BinAdapter;
import ie.nix.bpp.peersim.Queue;
import ie.nix.gc.AwardMessage;
import ie.nix.gc.BidMessage;
import ie.nix.gc.TenderMessage;
import peersim.core.CommonState;
import peersim.core.Network;

public class SmartBinTests {


	private static final Logger LOGGER = LogManager.getLogger();

	private static final String BIN_PACKING_PROBLEM_PROPERTIES = 
			"src/main/resources/ie/nix/bpp/BinPackingProblem.properties";
	private static final String BIN_AGENT_PROPERTIES   = 
			"src/main/resources/ie/nix/bpp/gc/SmartBin.properties";

	private static int agentProtocol = 0;

	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				BIN_PACKING_PROBLEM_PROPERTIES,
				BIN_AGENT_PROPERTIES,
				"control.queue.bpp_file=UniformBinPackingProblem-20-100-1.csv",
				"control.queue.run=0",
				"network.size=1",
				"simulation.endtime=1"});
		
	}
	
	@AfterEach
	void afterEach() {
		assert (getAgent(0).getTask() == null) : "This test requires clean-up.";
		
	}
	
	/*
	 * Helper APIs
	 */
	public static Queue initQueue() {
		Queue queue = new Queue("control.queue");
		queue.init();
		return queue;
	}
	
	public static Queue getQueue() {
		return Queue.getQueue();
	}

	public static Bin getBin(int n) {
		return ((BinAdapter)Network.get(n)).getNode();
	}
	
	public static ArrayList<Bin> getBins() {
		ArrayList<Bin> bins = new ArrayList<Bin>();
		for (int n = 0; n < peersim.core.Network.size(); n++) {
			Bin bin = ((BinAdapter)peersim.core.Network.get(n)).getNode();
			bins.add(bin);
		}
		return bins;
	}
	
	public static ArrayList<Bin> getSortedBins() {
		ArrayList<Bin> bins = getBins();
		Collections.sort(bins, Comparator.comparingDouble(bin -> {
			return bin.getFreeCapacity();
		}));
		return bins;
	}
	
	public static SmartBin getAgent(int n) {
		return ((SmartBin)((BinAdapter)Network.get(n)).getProtocol(agentProtocol));
	}
	
	public static Stream<SmartBin> getAgents() {
        Stream.Builder<SmartBin> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(getAgent(n));;
		}
		return streamBuilder.build();
	}
	
	/*
	 * Unit Tests preTender()
	 */
	@Test
	@DisplayName("Pre Tender Can Fit")
	public void preTenderCanFit() {
		initQueue();
		
		Bin bin = getBin(0);
		SmartBin binAgent = getAgent(0);
		
		binAgent.preTender(bin);
		Assertions.assertTrue(binAgent.hasTask(), "BinAgent should have a task, binAgent="+binAgent+".");
		
		// clean-up
		binAgent.clearTask();
	}

	@Test
	@DisplayName("Pre Tender Can't Fit")
	public void preTenderCantFit() {
		initQueue();
		
		Bin bin = getBin(0);
		SmartBin binAgent = getAgent(0);
		Item item = new Item(140);
		
		bin.addItem(item);
		
		binAgent.preTender(bin);
		Assertions.assertTrue(!binAgent.hasTask(), "BinAgent should not have a task, binAgent="+binAgent+".");
		
		// clean-up
		binAgent.clearTask();
		bin.removeItem(item);
	}
	
	/*
	 * Unit Tests shouldTender()
	 */
	@Test
	@DisplayName("Should Tender")
	public void shouldTender() {
		Bin bin = getBin(0);
		SmartBin binAgent = getAgent(0);
		
		binAgent.setTask(new Item(140));
		
		boolean shouldTender = binAgent.shouldTender(bin);
		Assertions.assertTrue(shouldTender, "BinAgent should tender, binAgent="+binAgent+".");
		
		// clean-up
		binAgent.clearTask();
	}
	
	@Test
	@DisplayName("Shouldn't Tender")
	public void shouldntTender() {
		Bin bin = getBin(0);
		SmartBin binAgent = getAgent(0);

		// No task
		
		boolean shouldTender = binAgent.shouldTender(bin);
		Assertions.assertTrue(!shouldTender, "BinAgent shouldn't tender, binAgent="+binAgent+".");

	}
	
	/*
	 * Unit Tests getTenderComparator()
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@DisplayName("Tender Comparator")
	public void tenderComparator() {
		SmartBin binAgent = getAgent(0);
//		Comparator<TenderMessage> tenderComparator = 
//				(Comparator<TenderMessage>)binAgent.getTenderComparator();
		Comparator<TenderMessage> tenderComparator = 
				Comparator.comparingDouble(tenderMessage -> binAgent.tenderToDouble(tenderMessage));
		
		// Agent<N,T,P> from, Agent<N,T,P> to, long expirationTime, int gossipCounter, T task, double guide   
		TenderMessage tm1 = new TenderMessage(null, binAgent, 1, 0, new Item(160), 0d);
		TenderMessage tm2 = new TenderMessage(null, binAgent, 1, 0, new Item(80), 0d);
		TenderMessage tm3 = new TenderMessage(null, binAgent, 1, 0, new Item(60), 0d);
		
		List<TenderMessage> actualTenderMessages = new ArrayList<TenderMessage>();
		actualTenderMessages.add(tm1);
		actualTenderMessages.add(tm2);
		actualTenderMessages.add(tm3);
		
		List<TenderMessage> expectedTenderMessages = new ArrayList<TenderMessage>();
		expectedTenderMessages.add(tm2);
		expectedTenderMessages.add(tm3);
		expectedTenderMessages.add(tm1);

		Collections.sort(actualTenderMessages, tenderComparator);
		
		LOGGER.debug("[{}]~expectedTenderMessages={}", CommonState.getTime(), expectedTenderMessages);
		LOGGER.debug("[{}]~actualTenderMessages={}", CommonState.getTime(), actualTenderMessages);

		Assertions.assertEquals(expectedTenderMessages, actualTenderMessages);
	}
	
	/*
	 * Unit Tests shouldBid()
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	@DisplayName("Should Bid")
	public void shouldBid() {
		SmartBin binAgent = getAgent(0);
		
		// Agent<N,T,P> from, Agent<N,T,P> to, long expirationTime, int gossipCounter, T task, double guide   
		TenderMessage tm1 = new TenderMessage(null, binAgent, 1, 0, new Item(60), 0d);
		
		boolean shouldBid = binAgent.shouldBid(tm1);
		Assertions.assertTrue(shouldBid, "BinAgent should bid, binAgent="+binAgent+".");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@DisplayName("Shouldn't Bid")
	public void shouldntBid() {
		SmartBin binAgent = getAgent(0);

		// Agent<N,T,P> from, Agent<N,T,P> to, long expirationTime, int gossipCounter, T task, double guide   
		TenderMessage tm1 = new TenderMessage(null, binAgent, 1, 0, new Item(160), 0d);
		
		boolean shouldBid = binAgent.shouldBid(tm1);
		Assertions.assertTrue(!shouldBid, "BinAgent shouldn't bid, binAgent="+binAgent+".");
	}
	
	/*
	 * Unit Tests shouldAward()
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	@DisplayName("Should Award")
	public void shouldAward() {
		SmartBin binAgent = getAgent(0); //freeCapacity = 130

		// Agent<N,T,P> from, Agent<N,T,P> to, long expirationTime, int gossipCounter, T task, double guide   
		TenderMessage tm1 = new TenderMessage(binAgent, null, 1, 0, new Item(10), 0d);
		// TenderMessage<N,T,P> tenderMessage, long expirationTime, P proposal, double offer
		BidMessage bm1 = new BidMessage(tm1, 1, null, 125d);
		
		boolean shouldAward = binAgent.shouldAward(bm1);
		Assertions.assertTrue(shouldAward, "BinAgent should award, binAgent="+binAgent+".");
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	@DisplayName("Should Award Free Capacity Same")
	public void shouldAwardSame() {
		SmartBin binAgent = getAgent(0); //freeCapacity = 130

		// Agent<N,T,P> from, Agent<N,T,P> to, long expirationTime, int gossipCounter, T task, double guide   
		TenderMessage tm1 = new TenderMessage(binAgent, null, 1, 0, new Item(10), 0d);
		// TenderMessage<N,T,P> tenderMessage, long expirationTime, P proposal, double offer
		BidMessage bm1 = new BidMessage(tm1, 1, null, 130d);
		
		boolean shouldAward = binAgent.shouldAward(bm1);
		Assertions.assertTrue(shouldAward, "BinAgent should award, binAgent="+binAgent+".");
	}
	
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	@DisplayName("Should'tn Award")
	public void shouldntAward() {
		SmartBin binAgent = getAgent(0); 
		binAgent.getNode().addItem(new Item(20)); //freeCapacity = 130

		// Agent<N,T,P> from, Agent<N,T,P> to, long expirationTime, int gossipCounter, T task, double guide   
		TenderMessage tm1 = new TenderMessage(binAgent, null, 1, 0, new Item(10), 0d);
		// TenderMessage<N,T,P> tenderMessage, long expirationTime, P proposal, double offer
		BidMessage bm1 = new BidMessage(tm1, 1, null, 145d);
		
		boolean shouldAward = binAgent.shouldAward(bm1);
		Assertions.assertTrue(!shouldAward, "BinAgent shouldn't award, binAgent="+binAgent+".");
	}
	
	/*
	 * Unit Tests awarded()
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	@DisplayName("Awarded")
	public void awarded() {

		Bin bin = getBin(0);
		SmartBin binAgent = getAgent(0);
		Item item = new Item(10);

		// Agent<N,T,P> from, Agent<N,T,P> to, long expirationTime, int gossipCounter, T task, double guide   
		TenderMessage tm1 = new TenderMessage(null, binAgent, 0, 0, item, 0d);
		// TenderMessage<N,T,P> tenderMessage, long expirationTime, P proposal, double offer
		BidMessage bm1 = new BidMessage(tm1, 0, item, 125d);
		// BidMessage<N,T, P> bidMessage, long expirationTime
		AwardMessage am1 = new AwardMessage(bm1, 0);
		
		binAgent.awarded(am1);
		Assertions.assertTrue(bin.hasItem(item), "BinAgent should have item "+item+", binAgent="+binAgent+".");
		
		// clean-up
		bin.removeItem(item);
	}
	
	/*
	 * Unit Tests postTender()
	 */
	@Test
	@DisplayName("Post Tender when tender failed")
	public void postTender() {
		
		Bin bin = getBin(0);
		SmartBin binAgent = getAgent(0);
		Item item = new Item(10);
		
		binAgent.setTask(item);
		
		binAgent.postTender(bin);
		Assertions.assertTrue(bin.hasItem(item), "Bin should have a item="+item+", bin="+bin+".");
		
		// clean-up
		bin.removeItem(item);
	}
	
	@Test
	@DisplayName("Post Tender when tender Succeeded")
	public void postTenderSucceeded() {
		
		Bin bin = getBin(0);
		SmartBin binAgent = getAgent(0);
		Item item = new Item(10);
		
		// Not setting task
		
		binAgent.postTender(bin);
		Assertions.assertTrue(!bin.hasItem(item), "Bin should not have a item="+item+", bin="+bin+".");
	}
}
