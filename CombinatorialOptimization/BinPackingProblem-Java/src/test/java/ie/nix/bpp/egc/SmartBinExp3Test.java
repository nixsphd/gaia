package ie.nix.bpp.egc;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.bpp.Bin;
import ie.nix.bpp.gc.SmartBinTests;
import ie.nix.bpp.peersim.BinAdapter;
import ie.nix.bpp.peersim.Queue;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.edsim.EDSimulator;

public class SmartBinExp3Test {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String BIN_PACKING_PROBLEM_PROPERTIES = 
			"src/main/resources/ie/nix/bpp/BinPackingProblem.properties";
	private static final String SMART_BIN_PROPERTIES   = 
			"src/main/resources/exp3/SmartBinExp3.properties";

	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				BIN_PACKING_PROBLEM_PROPERTIES,
				SMART_BIN_PROPERTIES,
				"control.queue.bpp_file=UniformBinPackingProblem-20-100-10.csv",
				"network.size=10",
			});	
	}

	/*
	 * Helper methods
	 */
	private static int agentProtocol = 0;
	
	public Stream<SmartBinExp3> getAgents() {
		Stream.Builder<SmartBinExp3> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add((SmartBinExp3)getAgent(n));
		}
		return streamBuilder.build();
	}
	
	public static SmartBinExp3 getAgent(int n) {
		return ((SmartBinExp3)((BinAdapter)Network.get(n)).getProtocol(agentProtocol));
	}
	
	/*
	 * System Tests
	 */
	@Test
	@DisplayName("Exp3 SmartBin Test")
	public void exp3SmartBinTest() {
	
		CommonState.initializeRandom(0);
		EDSimulator.nextExperiment();
		
		Queue queue = SmartBinTests.getQueue();
		LOGGER.info("queue.isEmpty()={}", queue.isEmpty());
		Assertions.assertTrue(queue.isEmpty(), 
				"Queue should be empty.");
		
		Stream<SmartBinExp3> binAgents = getAgents();
		int numberOfTasks = (int)binAgents.filter(
				agent -> ((SmartBinExp3)agent).hasTask()).count();
		LOGGER.info("numberOfTasks={}", numberOfTasks);
		Assertions.assertTrue(numberOfTasks == 0, 
				"There are tasks in the agents="+numberOfTasks+".");
		
		ArrayList<Bin> bins = SmartBinTests.getBins();
		long openBins = bins.stream().filter(
				bin -> !bin.isEmpty()).count();
		LOGGER.info("openBins={}", openBins);
		Assertions.assertTrue(openBins <= 5, 
				"There should be no more than 5 openBins, "
				+ "but are "+openBins+".");
		
		LOGGER.info("Done.");

	}
}
