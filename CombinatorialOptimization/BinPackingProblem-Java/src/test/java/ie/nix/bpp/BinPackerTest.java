package ie.nix.bpp;

import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ie.nix.bpp.peersim.BinPacker;
import ie.nix.bpp.peersim.Queue;
import ie.nix.gc.NodeAdapter;
import peersim.core.Network;

public class BinPackerTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	private static final String BIN_PACKING_PROBLEM_PROPERTIES = 
			"src/main/resources/ie/nix/bpp/BinPackingProblem.properties";
	protected static final String BINPACKER_PROPERTIES = 
			"src/main/resources/ie/nix/bpp/BestFit.properties";
	private static final String CONTROL_BIN_PACKER = "control.bin_packer";

	@BeforeAll
	static void beforeAll() {
		// Load the propertied file for these tests.
		peersim.Simulator.main(new String[] {
				BIN_PACKING_PROBLEM_PROPERTIES,
				BINPACKER_PROPERTIES,
				"control.queue.bpp_file=UniformBinPackingProblem-20-100-10.csv",
				"control.queue.run=0",
				"network.size=10",
				"simulation.endtime=1"});
	}
	
	/*
	 * Helper methods
	 */
	@SuppressWarnings("unchecked")
	protected List<Bin> getBins() {
		Stream.Builder<Bin> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(((NodeAdapter<Bin>)Network.get(n)).getNode());
		}
		return streamBuilder.build().collect(Collectors.toList());
	}
	
	protected void emptyBins() {
		getBins().parallelStream().forEach(bin -> bin.empty());
	}
	
	public void packTest(BinPacker binPacker, Integer[][] expectedBinsList) {

		Queue queue = Queue.getQueue();
		LOGGER.info("queue={}", queue);
		
		List<Bin> actualBinsList = getBins();
		binPacker.pack(queue, actualBinsList);
		
		LOGGER.info("expectedBinsList={}\n{}", expectedBinsList, expectedBinsList);
		LOGGER.info("actualBinsList={}\n{}", actualBinsList, getBinListString(actualBinsList));
		
		int binIndex = 0;
		for (Bin bin: actualBinsList) {
			int itemIndex = 0;
			for (Item item: bin.getItems()) {
				int expectedItemSize = expectedBinsList[binIndex][itemIndex].intValue();
				int actualItemSize = item.getSize();
				Assertions.assertEquals(expectedItemSize, actualItemSize,
						"binIndex="+binIndex+" itemIndex="+itemIndex+
								" expectedItemSize="+expectedItemSize+" != actualItemSize="+actualItemSize);
				itemIndex++;
			}
			binIndex++;
		}
	}

	public String getBinListString(List<Bin> actualBinsList) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Integer[][] binsList = new Integer[][]{\n");
		for (Bin bin: actualBinsList) {
			StringJoiner joiner = new StringJoiner(",", "{", "},\n");
			for (Item item: bin.getItems()) {
				joiner.add(String.valueOf(item.getSize()));
			}
			stringBuilder.append("\t"+joiner.toString());
		}
		stringBuilder.append("};\n");
		return stringBuilder.toString();
	}
	
	/*
	 * BestFit Tests
	 */
	/*
	 * Unit tests Comparator<? super Entry<Integer, Integer>> sortByValue()
	 */
	@Test
	@DisplayName("Sort By Value")
	public void sortByValue() {
		
		BestFit bestFit = new BestFit(CONTROL_BIN_PACKER);
		Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
		map.put(0, 10);
		map.put(1, 12);
		map.put(2, 5);
		
		int[] expectedOrder = IntStream.of(5,10,12).toArray();
		
		int[] actualOrder = map.entrySet().stream().sorted(bestFit.sortByValue()).
				mapToInt(entry -> entry.getValue()).toArray();
		
		LOGGER.info("expectedOrder={}", expectedOrder);
		LOGGER.info("actualOrder={}", actualOrder);
		
		Assertions.assertArrayEquals(expectedOrder, actualOrder);

	}
	
	/*
	 * Unit tests Predicate<? super Entry<Integer, Integer>> hasSpaceForItem(Item item)
	 */
	@Test
	@DisplayName("Has Space For Item")
	public void hasSpaceForItem() {
		
		BestFit bestFit = new BestFit(CONTROL_BIN_PACKER);
		Item item = new Item(9);
		
		Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
		map.put(0, 10);
		map.put(1, 12);
		map.put(2, 5);
		
		int[] expectedOrder = IntStream.of(10,12).toArray();
		
		int[] actualOrder = map.entrySet().stream().filter(bestFit.hasSpaceForItem(item)).
				mapToInt(entry -> entry.getValue()).toArray();
		
		LOGGER.info("expectedOrder={}", expectedOrder);
		LOGGER.info("actualOrder={}", actualOrder);
		
		Assertions.assertArrayEquals(expectedOrder, actualOrder);

	}
	
	/*
	 * void pack(Queue queue, List<Bin> bins)
	 */
	@Test
	@DisplayName("BestFit Pack")
	public void bestFitPack() {
		
		// 20,48,49,67,95,33,31,21,99,74 
		Integer[][] expectedBinsList = new Integer[][]{
			{20,48,49,33},
			{67,74},
			{95,31,21},
			{99},
			{},
			{},
			{},
			{},
			{},
			{},
		};
		
		BestFit binPacker = new BestFit(CONTROL_BIN_PACKER);
		packTest(binPacker, expectedBinsList);
		
		// Empty the bins
		emptyBins();
	}
	
	/*
	 * NextFit Tests
	 */
	/*
	 * void pack(Queue queue, List<Bin> bins)
	 */
	@Test
	@DisplayName("NextFit Pack")
	public void nextFitPack() {
		
		// 20,48,49,67,95,33,31,21,99,74 
		Integer[][] expectedBinsList = new Integer[][]{
			{20,48,49},
			{67},
			{95,33},
			{31,21},
			{99},
			{74},
			{},
			{},
			{},
			{},
		};

		NextFit binPacker = new NextFit(CONTROL_BIN_PACKER);
		packTest(binPacker, expectedBinsList);
		
		// Empty the bins
		emptyBins();
	}
	
	/*
	 * WorstFit Tests
	 */
	/*
	 * void pack(Queue queue, List<Bin> bins)
	 */
	@Test
	@DisplayName("WorstFit Pack")
	public void worstFitPack() {
		
		// 20,48,49,67,95,33,31,21,99,74 
		Integer[][] expectedBinsList = new Integer[][]{
			{20,48,49},
			{67,33,21},
			{95,31},
			{99},
			{74},
			{},
			{},
			{},
			{},
			{},
		};

		WorstFit binPacker = new WorstFit(CONTROL_BIN_PACKER);
		packTest(binPacker, expectedBinsList);
		
		// Empty the bins
		emptyBins();
	}
	
	/*
	 * AlmostWorstFit Tests
	 */
	/*
	 * void pack(Queue queue, List<Bin> bins)
	 */
	@Test
	@DisplayName("AlmostWorstFit Pack")
	public void almostWorstFitPack() {
		
		// 20,48,49,67,95,33,31,21,99,74 
		Integer[][] expectedBinsList = new Integer[][]{
			{20,48,49,31},
			{67,74},
			{95,33,21},
			{99},
			{},
			{},
			{},
			{},
			{},
			{},
		};

		AlmostWorstFit binPacker = new AlmostWorstFit(CONTROL_BIN_PACKER);
		packTest(binPacker, expectedBinsList);
		
		// Empty the bins
		emptyBins();
	}
}
