package ie.nix.bpp.gc;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Item;
import ie.nix.bpp.peersim.Integrity;
import ie.nix.gc.GCAgent;
import ie.nix.gc.NodeAdapter;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;

public class SmartBinIntegrity extends Integrity {

	private static final Logger LOGGER = LogManager.getLogger();

	private static final String PROTOCOL = "protocol";
	
	private int protocolID;

	public SmartBinIntegrity(String prefix) {
		super(prefix);

		this.protocolID = Configuration.getPid(prefix+"."+PROTOCOL);
		LOGGER.trace("[{}]~protocolID={}", CommonState.getTime(), protocolID);
	}

	public int getNumberOfItemsBeingPacked() {
		Stream<SmartBin> agents = getAgents();
		return (int)agents.filter(agent -> ((SmartBin)agent).hasTask()).count();
	}
	
	public Set<Item> getItemsBeingPacked() {
		Stream<SmartBin> agents = getAgents();
		return agents.filter(agent -> agent.hasTask()).
				map(agent -> agent.getTask()).
				collect(Collectors.toSet());
	}

	protected Stream<SmartBin> getAgents() {
		Stream.Builder<SmartBin> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add((SmartBin)getAgent(n));
		}
		return streamBuilder.build();
	}
	
	protected GCAgent<?, ?, ?> getAgent(int n) {
		GCAgent<?, ?, ?> agent = ((GCAgent<?, ?, ?>)((NodeAdapter<?>)Network.get(n)).getProtocol(protocolID));
		LOGGER.trace("[{}]~agent={}", CommonState.getTime(), agent);
		return agent;
	}

}
