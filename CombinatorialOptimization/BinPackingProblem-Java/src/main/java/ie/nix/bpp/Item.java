package ie.nix.bpp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Item implements Comparable<Item>, Cloneable {
	
	private static final Logger LOGGER = LogManager.getLogger();

	private final int size;
			
	public Item(int size) {
		this.size = size;
	}
	
	@Override
	public Item clone() {
		try {
			Item foo = (Item) super.clone();
			return foo;
		} catch (CloneNotSupportedException e) {
			return null; // never happens
		} 
	}

	@Override
	public String toString() {
		return "Item[" + size + "]";
	}

	public int getSize() {
		return size;
	}
	
	public int compareTo(Item other) {
	    return ((Comparable<Integer>)this.getSize()).compareTo(other.getSize());
	}
	
}
