package ie.nix.bpp.peersim;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Bin;
import ie.nix.gc.Observer;
import peersim.core.CommonState;

public class OpenBins extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();

	public OpenBins(String prefix) {
		super(prefix);
	}

	@Override
	public void init() {
		super.init();

        String headerString = generateCSVString(Stream.of("Time", "Open Bins"));
        writeHeaders(headerString);
	}
	
	@Override
	public boolean observe() {
		Stream<Bin> binStream = getNodes();
		long openBins = binStream.filter(bin -> !bin.isEmpty()).count();
		String rowString = generateCSVString(Stream.of(CommonState.getTime(), openBins));
		writeRow(rowString);
		
		if (isLogTime()) {
			LOGGER.info("[{}]~openBins={}", CommonState.getTime(), openBins);
		} 
		return false;
	}

}
