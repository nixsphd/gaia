package ie.nix.bpp;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringJoiner;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BinPackingProblem {

	private static final String RESOURCES_DIR = "src/main/resources";
	private static final String DATASETS_DIR = "datasets";

	private static final Logger LOGGER = LogManager.getLogger();
	 
	private final List<Queue<Item>> queues;
	
	protected BinPackingProblem() {
		this.queues = new ArrayList<Queue<Item>>(); 
	}

	public BinPackingProblem(String fileName) {
		this();
		LOGGER.trace("[0]~fileName={}", fileName);
		List<String> lines = readFile(fileName);
		lines.stream().forEach(line -> {
			Queue<Item> queue = new LinkedList<Item>();
			Arrays.stream(line.split(",")).forEach(size -> {
				Item item = new Item(Integer.valueOf(size));
				queue.add(item);
			});
			addQueue(queue);
		});
	}
	
	protected BinPackingProblem(List<Queue<Item>> queues) {
		this.queues = queues; 
	}
	
	/*
	 * Queues
	 */
	public Queue<Item> getQueues(int queueNumber) {
		LOGGER.trace("[0]~Reading {}, size={}", queueNumber, queues.size());
		return queues.get(queueNumber);
	}
	
	protected List<Queue<Item>> getQueues() {
		return queues;
	}
	
	
	protected void addQueue(Queue<Item> queue) {
		queues.add(queue);
	}

	/*
	 * Reading and writing to files
	 */
	public List<String> readFile(String fileName) {
		List<String> lines;
		try {
			URL url = ClassLoader.getSystemResource(DATASETS_DIR+"/"+fileName);
			LOGGER.debug("[0]~Reading={} url={}", fileName, url);
			lines = Files.readAllLines(Paths.get(url.toURI()));
			LOGGER.debug("[0]~Reading {} lines from {}", lines.size(), fileName);
			return lines;
			
		} catch (IOException e) {   
			LOGGER.error("[0]~File {} could not be found in the classpath.\n{}", fileName, e);
			
		} catch (URISyntaxException e) {
			LOGGER.error("[0]~URI was dodgy for {}.\n{}", fileName, e);
		}
		return null;
	}
	
	public void newFile(String filename) {
		Path filePath = Paths.get(RESOURCES_DIR+"/"+DATASETS_DIR+"/"+filename);
		try {
			try {
				Files.createFile(filePath);
			} catch (FileAlreadyExistsException x) {
				// If it exists we truncate.
				Files.write(filePath, new byte[0], StandardOpenOption.TRUNCATE_EXISTING);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeToFile(String filename, Stream<? extends Object> data) {
		// Initialise the DataFrame to hold the results
		try {
			StringJoiner joiner = new StringJoiner(",", "", "\n");
			data.forEachOrdered(dataum -> joiner.add(dataum.toString()));
			Files.write(Paths.get(new String(RESOURCES_DIR+"/"+DATASETS_DIR+"/"+filename)), joiner.toString().getBytes(), 
					StandardOpenOption.CREATE, StandardOpenOption.APPEND);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveItemToFile(String filename) {
		newFile(filename);
		// Get the item queue
		Stream<Queue<Item>> streamOfQueuesOfItems = queues.stream();
		streamOfQueuesOfItems.forEachOrdered((queueOfItems) -> { 
			writeToFile(filename, queueOfItems.stream().map(item -> item.getSize()));
		});
	}
	
}
