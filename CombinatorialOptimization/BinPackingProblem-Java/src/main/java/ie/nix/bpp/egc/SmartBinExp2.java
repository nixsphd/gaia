package ie.nix.bpp.egc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SmartBinExp2 extends EvolvedSmartBin {

	private static final Logger LOGGER = LogManager.getLogger();   
	
	public SmartBinExp2(String prefix) {
		super(prefix);
		init();
	}
	
	protected void init() {
		shouldTender = (int freeCapacity, int itemSize) ->true;
		getGuide = (int freeCapacity, int itemSize) ->abs(max(div(itemSize, div(itemSize, freeCapacity)), (itemSize + (0.0 - itemSize))));
		tenderToDouble = (double guide, int freeCapacity, int itemSize) ->(min((div(guide, (itemSize - freeCapacity)) * ((itemSize + freeCapacity) + 0.0)), (freeCapacity + abs((0.0 - itemSize)))) + ((freeCapacity * min(0.0, (div(guide, (itemSize - freeCapacity)) * ((itemSize + freeCapacity) + div(guide, (itemSize - freeCapacity)))))) * (itemSize - freeCapacity)));
		shouldBid = (double guide, int freeCapacity, int itemSize) ->true;
		getOffer = (double guide, int freeCapacity, int itemSize) ->freeCapacity;
		bidToDouble = (double offer, int freeCapacity, int itemSize) ->max(((max(itemSize, offer) + min(itemSize, freeCapacity)) - div((itemSize + itemSize), offer)), abs((0.0 - itemSize)));
		shouldAward = (double offer, int freeCapacity, int itemSize) ->(max(max((0.0 - offer), freeCapacity), itemSize) > ((0.0 - 0.0) * max((0.0 - offer), freeCapacity)));
	}

}