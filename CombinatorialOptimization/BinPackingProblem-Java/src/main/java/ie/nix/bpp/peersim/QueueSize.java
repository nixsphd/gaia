package ie.nix.bpp.peersim;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.gc.Observer;
import peersim.core.CommonState;

public class QueueSize extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();

	public QueueSize(String prefix) {
		super(prefix);
	}

	@Override
	public void init() {
		super.init();

        String headerString = generateCSVString(Stream.of("Time", "Number Of Items Queued"));
        writeHeaders(headerString);
        observe();
	}

	@Override
	public boolean observe() {
		int numberOfItemsQueued = Queue.getQueue().getNumberOfItemsQueued();

		String rowString = generateCSVString(Stream.of(CommonState.getTime(), numberOfItemsQueued));
		writeRow(rowString);

		if (isLogTime()) {
			LOGGER.info("[{}]~numberOfItemsQueued={}", CommonState.getTime(), numberOfItemsQueued);
		}
		return false;

	}

}
