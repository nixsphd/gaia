package ie.nix.bpp.peersim;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Bin;
import ie.nix.gc.Observer;
import peersim.core.CommonState;

public class FreeBinCapacity extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();

	public FreeBinCapacity(String prefix) {
		super(prefix);
	}

	@Override
	public boolean observe() {
		Stream<Bin> binStream = getNodes();
		String rowString = generateCSVString(binStream.sorted().map(bin -> bin.getFreeCapacity()));
		writeRow(rowString);
		
		if (isLogTime()) {
			LOGGER.info("[{}]~bins={}", CommonState.getTime(), rowString);
		}
		return false;
	}

}
