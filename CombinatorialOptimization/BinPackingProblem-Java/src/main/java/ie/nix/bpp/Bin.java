package ie.nix.bpp;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Bin implements Cloneable, Comparable<Bin>{ 

	private static final Logger LOGGER = LogManager.getLogger();

	private static final int DEFAULT_CAPACITY = 100;

	private List<Item> items;
	private final int capacity;

	public Bin() {
		this(DEFAULT_CAPACITY);
	}

	public Bin(int capacity) {
		this.capacity = capacity;
		this.items = new ArrayList<Item>();
		LOGGER.trace("capacity={}", capacity);
	}

	@Override
	public Bin clone() {
		try {
			Bin clonedBin = (Bin) super.clone();
			clonedBin.items = new ArrayList<Item>();
			LOGGER.trace("capacity={}", clonedBin.getCapacity());
			return clonedBin;
		} catch (CloneNotSupportedException e) {
			return null; // never happens
		} 
	}
	
	@Override
	public String toString() {
		return "Bin["+getFreeCapacity()+"/"+getCapacity()+"]";
	}

	@Override
	public int compareTo(Bin otherBin) {
		return this.getFreeCapacity() - otherBin.getFreeCapacity();
	}

	public int getCapacity() {
		return capacity;
	}

	public int getUsedCapacity() {
		return items.stream().map(item -> item.getSize()).reduce(0, Integer::sum);
	}

	public int getFreeCapacity() {
		return capacity - getUsedCapacity();
	}

	public boolean canFit(Item item) {
		return (getFreeCapacity() >= item.getSize());
	}

	public void addItem(Item item) {
		assert(canFit(item)) : "Can't fit this item, "+item+", in the bin, "+this;
		if (canFit(item)) {
			items.add(item);
		}
	}
	
	public void removeItem(Item item) {
		assert(hasItem(item)) : "This item, "+item+", cannot be removed form the bin, "+this+
			"because it's not in it.";
		items.remove(item);
	}
	
	public void empty() {
		items = new ArrayList<Item>();
	}

	public boolean isEmpty() {
		return items.isEmpty();
	}

	public int getNumberOfItems() {
		return items.size();
	}

	public List<Item> getItems() {
		return new ArrayList<Item>(items);
	}

	public boolean hasItem(Item item) {
		return items.contains(item);
	}
	
}