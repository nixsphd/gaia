package ie.nix.bpp;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UniformBinPackingProblem extends BinPackingProblem {

	private static final Logger LOGGER = LogManager.getLogger();

	public UniformBinPackingProblem(int minItemSize, int maxItemSize, 
			int numberOfItems, int numberOfRuns) {
		Random random = new Random();
		IntStream.range(0, numberOfRuns).forEach(run -> {
			random.setSeed(run);
			Queue<Item> queueOItems = new LinkedList<Item>();
			IntStream.range(0, numberOfItems).forEach(i -> {
				int size = minItemSize + random.nextInt(maxItemSize - minItemSize);
				Item item = new Item(size);
				queueOItems.add(item);

			});
			addQueue(queueOItems);
		});
	}

	public static void main(String[] args) {	
		String usage = "Please pass in one argument structured as follows: "
				+ "minItemSize-maxItemSize-numberOfItems-mumberOfRuns.";
		LOGGER.debug("args={}",Arrays.toString(args));
		assert(args.length >= 3) : 
			usage +"There was not enough arge; there was "+args.length+" but should be 3.";

		int binCapacity = Integer.valueOf(args[0]);
		
		String[] ubppArgs = args[1].split("-");	
		LOGGER.debug("ubppArgs={}",Arrays.toString(ubppArgs));
		assert(ubppArgs.length >= 3) : usage+"The argument was not correclty formatted";
		
		int minItemSize = Integer.valueOf(ubppArgs[0]);
		int maxItemSize = Integer.valueOf(ubppArgs[1]);
		int numberOfItems = Integer.valueOf(ubppArgs[2]);

		int numberOfRuns = Integer.valueOf(args[2]);

		String fileName = "UniformBinPackingProblem-"+args[1]+".csv";
		UniformBinPackingProblem uniformBinPackingProblem = new UniformBinPackingProblem(
				minItemSize, maxItemSize, numberOfItems, numberOfRuns);
		uniformBinPackingProblem.saveItemToFile(fileName);
		
	}
}
