package ie.nix.bpp.egc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SmartBinExp1 extends EvolvedSmartBin {

	private static final Logger LOGGER = LogManager.getLogger();   
	
	public SmartBinExp1(String prefix) {
		super(prefix);
		init();
	}
	
	protected void init() {
		shouldTender = (int freeCapacity, int itemSize) ->true;
		getGuide = (int freeCapacity, int itemSize) ->div(max(freeCapacity, freeCapacity), (itemSize * (0.0 - freeCapacity)));
		tenderToDouble = (double guide, int freeCapacity, int itemSize) ->max(div(abs(max(itemSize, guide)), itemSize), div(max(itemSize, freeCapacity), (itemSize - abs((max(itemSize, freeCapacity) - itemSize)))));
		shouldBid = (double guide, int freeCapacity, int itemSize) ->true;
		getOffer = (double guide, int freeCapacity, int itemSize) ->max(max(max(freeCapacity, guide), freeCapacity), freeCapacity);
		bidToDouble = (double offer, int freeCapacity, int itemSize) ->offer;
		shouldAward = (double offer, int freeCapacity, int itemSize) ->true;
	}

}