package ie.nix.bpp;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.peersim.BinPacker;
import ie.nix.bpp.peersim.Queue;
import peersim.core.CommonState;

/*
 * First Fit algorithm is a greedy algorithm which packs 
 * each item into the first, as in lowest indexed, bin 
 * where it fits. It uses an new bin if it does not if in 
 * any of the currently used bins. 
 */
public class FirstFit extends BinPacker {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public FirstFit(String prefix) {
		super(prefix);
	    LOGGER.debug("[{}]", CommonState.getTime());
	}

	@Override
	public void pack(Queue queue, List<Bin> bins) {
		while (!queue.isEmpty()) {
			Item item = queue.poll();
        	LOGGER.trace("[{}] Processing {}", CommonState.getIntTime(), item);
			
	        Optional<Bin> maybeBin = bins.stream().
	        	filter(hasSpaceForItem(item)).
	        	findFirst();
	        
	        if (maybeBin.isPresent()) {
	        	Bin bin  = maybeBin.get();
	        	bin.addItem(item);
	        	LOGGER.info("[{}] Added {} it {}", CommonState.getIntTime(), item, bin);
	        } else {
	        	LOGGER.warn("[{}] Not enough space to add {}", CommonState.getIntTime(), item);
	        }
	        
		}
	}
	
	protected Predicate<Bin> hasSpaceForItem(Item item) {
		return bin -> bin.getFreeCapacity() >= item.getSize();
	}
	
}