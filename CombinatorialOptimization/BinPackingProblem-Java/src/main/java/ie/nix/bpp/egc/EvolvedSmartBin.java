package ie.nix.bpp.egc;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Bin;
import ie.nix.bpp.Item;
import ie.nix.bpp.egc.EvolvedSmartBinNodes.FreeCapacity;
import ie.nix.bpp.egc.EvolvedSmartBinNodes.ItemSize;
import ie.nix.bpp.egc.EvolvedSmartBinNodes.NumberOfItems;
import ie.nix.bpp.peersim.Queue;
import ie.nix.egc.EGCAgent;
import ie.nix.gc.AwardMessage;
import ie.nix.gc.BidMessage;
import ie.nix.gc.TenderMessage;
import peersim.core.CommonState;

public class EvolvedSmartBin extends EGCAgent<Bin, Item, Void> {

	private static final Logger LOGGER = LogManager.getLogger();

	public interface ShouldTender {
		public boolean apply(int freeCapacity, int itemSize);
	} 
	
	public interface GetGuide {
		public double apply(int freeCapacity, int itemSize);
	} 
	
	public interface TenderToDouble {
		public double apply(double guide, int freeCapacity, int itemSize);
	} 
	
	public interface ShouldBid {
		public boolean apply(double guide, int freeCapacity, int itemSize);
	} 
	
	public interface GetOffer {
		public double apply(double guide, int freeCapacity, int itemSize);
	} 
	
	public interface BidToDouble {
		public double apply(double offer, int freeCapacity, int itemSize);
	}  
	
	public interface ShouldAward {
		public boolean apply(double offer, int freeCapacity, int itemSize);
	}
	
	protected ShouldTender shouldTender;
	protected GetGuide getGuide;
	protected TenderToDouble tenderToDouble;
	protected ShouldBid shouldBid;
	protected GetOffer getOffer;
	protected BidToDouble bidToDouble;
	protected ShouldAward shouldAward;

	private Item task;
	
	public EvolvedSmartBin(String prefix) {
		super(prefix);
		init();
	}
	
	protected void init() {
		shouldTender = (int freeCapacity, int itemSize)
				-> true;
			
		getGuide = (int freeCapacity, int itemSize)
				-> freeCapacity;
			
		tenderToDouble = (double guide, int freeCapacity, int itemSize)
				-> (freeCapacity > itemSize ? -itemSize : Double.POSITIVE_INFINITY);
			
		shouldBid = (double guide, int freeCapacity, int itemSize)
				-> true;
		
		getOffer = (double guide, int freeCapacity, int itemSize)
				-> freeCapacity;
			
		bidToDouble = (double offer, int freeCapacity, int itemSize)
				-> offer;
			
		shouldAward = (double offer, int freeCapacity, int itemSize)
				-> offer <= freeCapacity;
	}

	@Override
	public String toString() {
		int index = this.getNodeAdapter().getIndex();
		Bin bin = this.getNodeAdapter().getNode();
		if (hasTask()) {
			return "EvolvedSmartBin-"+index+" ["+bin+", "+task+"]";
		} else {
			return "EvolvedSmartBin-"+index+" ["+bin+", no task]";
		}
	}
	
	/*
	 * Task helper methods
	 */
	public boolean hasTask() {
		return task != null;
	}
	
	protected Item getTask() {
		return task;
	}

	protected void setTask(Item task) {
		this.task = task;
	}

	protected Item clearTask() {
		// Need to temp store the task so clear it and then send it back.
		Item task = this.task;
		this.task = null;
		LOGGER.debug("[{}] {} -> {}", util.getTime(), this, task);
		return task;
	}

	/*
	 * Non evolved methods
	 */
	@Override
	protected void preTender(Bin bin) {
		Optional<Item> maybeItem = Optional.ofNullable(Queue.getQueue().peek());
		if (getState().isIdle() && !hasTask() && maybeItem.isPresent() 
				&& bin.canFit(maybeItem.get())) {
			setTask(Queue.getQueue().poll());
		}
		LOGGER.debug("[{}] agent={}, bin={}, maybeItem={}, task={}", util.getTime(), 
				this, bin, maybeItem, task);
	}

	@Override
	protected Item getTask(Bin bin) {
		Item task = getTask();
		LOGGER.debug("[{}] agent={}, bin={}, task={}", 
				util.getTime(), this, bin, task);
		return task;
	}
	
	@Override
	protected Void getProposal(TenderMessage<Bin, Item, Void> tenderMessage) {
		Void proposal = null;
		LOGGER.debug("[{}] agent={}, proposal={}", util.getTime(), this, proposal);
		return proposal;
	}
	
	@Override
	protected void award(AwardMessage<Bin, Item, Void> awardMessage) {
		Item task = clearTask();
		LOGGER.debug("[{}] agent={}, task={}", 
				util.getTime(), this, task);
	}

	@Override
	protected void awarded(AwardMessage<Bin, Item, Void> awardMessage) {
		Bin myBin = awardMessage.getTo().getNode();
		Item item = awardMessage.getTask();

		LOGGER.debug("[{}] agent={}, added item={} to myBin={}", 
				util.getTime(), this, item, myBin);
		myBin.addItem(item);
		LOGGER.debug("[{}] agent={}, added item={} to myBin={}", 
				util.getTime(), this, item, myBin);
	}

	@Override
	protected void postTender(Bin bin) {
		LOGGER.trace("[{}] agent={},  bin={}, task={}", 
				util.getTime(), this, bin, task);
		if (hasTask()) {
			Item task = clearTask();
			// So the task was never awarded so we should assign it to ourselves.
			bin.addItem(task);
			LOGGER.debug("[{}] agent={}, added item={} to myBin={}", 
					util.getTime(), this, task, bin);
		}
	}
	
	/*
	 * Partially evolved methods
	 */
	@Override
	protected boolean shouldTender(Bin bin) {
		if (evolveShouldTender()) {
			return hasTask() && 
					super.shouldTender(bin);
			
		} else {
			return hasTask() && 
					shouldTender.apply(bin.getFreeCapacity(), getTask().getSize());
			
		}
	}
	
	@Override
	protected boolean shouldBid(TenderMessage<Bin, Item, Void> tenderMessage) {
		Bin myBin = tenderMessage.getTo().getNode();
		Item item = tenderMessage.getTask();
		if (evolveShouldBid()) {
			return !hasTask() && myBin.canFit(item) && 
					super.shouldBid(tenderMessage);
			
		} else {
			int freeCapacity = tenderMessage.getTo().getNode().getFreeCapacity();
			int itemSize = tenderMessage.getTask().getSize();
			double guide = tenderMessage.getGuide();
			return !hasTask() && myBin.canFit(item) && 
					shouldBid.apply(guide, freeCapacity, itemSize);
		}
	}
	
	/*
	 * Evolved methods
	 */
	@Override
	protected double getGuide(Bin bin, Item item) {
		if (evolveGetGuide()) {
			return super.getGuide(bin, item);
			
		} else {
			int freeCapacity = bin.getFreeCapacity();
			int itemSize = item.getSize();
			return getGuide.apply(freeCapacity, itemSize);
			
		}
	}

	@Override
	protected double tenderToDouble(TenderMessage<Bin, Item, Void> tenderMessage)  {
		if (evolveTenderToDouble()) {
			return super.tenderToDouble(tenderMessage);
			
		} else {
			int freeCapacity = tenderMessage.getTo().getNode().getFreeCapacity();
			int itemSize = tenderMessage.getTask().getSize();
			double guide = tenderMessage.getGuide();
			return tenderToDouble.apply(guide, freeCapacity, itemSize);
			
		}
	}

	@Override
	protected double getOffer(TenderMessage<Bin, Item, Void> tenderMessage)  {
		if (evolveGetOffer()) {
			return super.getOffer(tenderMessage);
			
		} else {
			int freeCapacity = tenderMessage.getTo().getNode().getFreeCapacity();
			int itemSize = tenderMessage.getTask().getSize();
			double guide = tenderMessage.getGuide();
			return getOffer.apply(guide, freeCapacity, itemSize);
			
		}
	}
	
	@Override
	protected double bidToDouble(BidMessage<Bin, Item, Void> bidMessage)  {
		if (evolveBidToDouble()) {
			return super.bidToDouble(bidMessage);
			
		} else {
			int freeCapacity = bidMessage.getTo().getNode().getFreeCapacity();
			int itemSize = bidMessage.getTask().getSize();
			double offer = bidMessage.getOffer();
			return bidToDouble.apply(offer, freeCapacity, itemSize);
			
		}
	}

	@Override
	protected boolean shouldAward(BidMessage<Bin, Item, Void> bidMessage) {
		if (evolveShouldAward()) {
			return super.shouldAward(bidMessage);
			
		} else {
			int freeCapacity = bidMessage.getTo().getNode().getFreeCapacity();
			int itemSize = bidMessage.getTask().getSize();
			double offer = bidMessage.getOffer();
			return shouldAward.apply(offer, freeCapacity, itemSize);
			
		}
	}
	
	/*
	 * Update variable functions
	 */
	@Override
	protected void updateNodeNodes(Bin bin) {
		FreeCapacity.value = bin.getFreeCapacity();
		NumberOfItems.value = bin.getNumberOfItems();
	}

	@Override
	protected void updateTaskNode(Item item) {
		ItemSize.value = item.getSize();
	}

	@Override
	protected void updateProposalNode(Void item) {}
	
	/*
	 * GP Functions
	 */
	protected double min(double A, double B) {
		return Math.min(A, B);
	}
	
	protected double max(double A, double B) {
		return Math.max(A, B);
	}
	
	protected double div(double A, double B) {
		return (B != 0 ? A/B : 0);
	}
	
	protected double abs(double A) {
		return Math.abs(A);
	}
	
	protected double rand() {
		return CommonState.r.nextDouble();
	}
}
