package ie.nix.bpp.peersim;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Bin;
import ie.nix.bpp.Item;
import ie.nix.gc.Observer;
import peersim.core.CommonState;

public class Integrity extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();

	protected Set<Item> origionalItems;
	private int origionalTotalNumberOfItems;
	
	public Integrity(String prefix) {
		super(prefix);
	}

	@Override
	public void init() {
		super.init();

        String headerString = generateCSVString(Stream.of("Time", 
        		"Total Number Of Items", "Number Of Items Queued", 
        		"Number Of Items Being Packed", "Number Of Items Packed"));
        writeHeaders(headerString);
        
        // Store the original number to compare with
        origionalItems = getSetOfAllItems();
        origionalTotalNumberOfItems = origionalItems.size();
	}

	public Set<Item> getSetOfAllItems() {
		Set<Item> setOfItems = new HashSet<Item>();
		setOfItems.addAll(getItemsQueued());
		setOfItems.addAll(getItemsBeingPacked());
		setOfItems.addAll(getItemsPacked());
		return setOfItems;
	}

	@Override
	public boolean observe() {
		int numberOfItemsQueued = getNumberOfItemsQueued();
		int numberOfItemsPacked = getNumberOfItemsPacked();
		int numberOfItemsBeingPacked =  getNumberOfItemsBeingPacked();
		
		int totalNumberOfItems = numberOfItemsQueued + numberOfItemsPacked + numberOfItemsBeingPacked;
		LOGGER.info("[{}]~origionalTotalNumberOfItems={}, totalNumberOfItems={}, numberOfItemsQueued={}, "
				+ "numberOfItemsBeingPacked={}, numberOfItemsPacked={}", 
				CommonState.getTime(), origionalTotalNumberOfItems, totalNumberOfItems, numberOfItemsQueued, 
				numberOfItemsBeingPacked, numberOfItemsPacked);
		
//		assert(origionalTotalNumberOfItems == totalNumberOfItems) : 
//			"origionalTotalNumberOfItems="+origionalTotalNumberOfItems+" != "
//					+ "totalNumberOfItems="+totalNumberOfItems+" "
//					+ "missingItems="+getMissingItems();
		
		String rowString = generateCSVString(Stream.of(CommonState.getTime(), totalNumberOfItems, 
				numberOfItemsQueued, numberOfItemsBeingPacked, numberOfItemsPacked));
		writeRow(rowString);

		if (isLogTime()) {
			LOGGER.info("[{}]~totalNumberOfItems={}, numberOfItemsQueued={}, "
					+ "numberOfItemsBeingPacked={}, numberOfItemsPacked={}", 
					CommonState.getTime(), totalNumberOfItems, numberOfItemsQueued, 
					numberOfItemsBeingPacked, numberOfItemsPacked);
		}
		return false;
	}
	
	public Set<Item> getMissingItems() {
		Set<Item> missingItems = new HashSet<Item>(origionalItems);
		missingItems.removeAll(getSetOfAllItems());
		return missingItems;
//		return Sets.difference(origionalItems, getSetOfAllItems()); 
	}

	public int getNumberOfItemsQueued() {
		return Queue.getQueue().getNumberOfItemsQueued();
	}
	
	public Set<Item> getItemsQueued() {
		return Queue.getQueue().getItems().stream().collect(Collectors.toSet());
	}

	public int getNumberOfItemsBeingPacked() {
		return 0;
	} 
	
	public Set<Item> getItemsBeingPacked() {
		return Collections.<Item>emptySet();
	}

	public int getNumberOfItemsPacked() {
		Stream<Bin> binStream = getNodes();
		return binStream.mapToInt(bin -> bin.getNumberOfItems()).sum();
	}
	
	public Set<Item> getItemsPacked() {
		Stream<Bin> binStream = getNodes();
		return binStream.flatMap(bin -> bin.getItems().stream()).collect(Collectors.toSet());
	}

}
