package ie.nix.bpp.peersim;

import java.util.DoubleSummaryStatistics;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Bin;
import ie.nix.gc.Observer;
import peersim.core.CommonState;

public class FreeBinCapacityStats extends Observer {

	private static final Logger LOGGER = LogManager.getLogger();

	public FreeBinCapacityStats(String prefix) {
		super(prefix);
	}

	@Override
	public void init() {
		super.init();
        String headerString = generateCSVString(Stream.of("Time", 
        		"Min Free Space", "Avg Free Space", "Max Free Space"));
        writeHeaders(headerString);
	}

	@Override
	public boolean observe() {
		
		Stream<Bin> binStream = getNodes();
	
		// Record free space statistics.
		DoubleSummaryStatistics binFreeSpaceStats = binStream.
				filter(bin -> !bin.isEmpty()).
				mapToDouble(bin -> bin.getFreeCapacity()).
				summaryStatistics();
		double minFreeSpace = binFreeSpaceStats.getMin();
		double avgFreeSpace = binFreeSpaceStats.getAverage();
		double maxFreeSpace = binFreeSpaceStats.getMax();
			
		String rowString = generateCSVString(Stream.of(CommonState.getTime(), 
				minFreeSpace, avgFreeSpace, maxFreeSpace));
		writeRow(rowString);
		
		if (isLogTime()) {
			LOGGER.info("[{}] minFreeSpace={}, avgFreeSpace={}, maxFreeSpace={}", 
					CommonState.getIntTime(), format(minFreeSpace), 
					format(avgFreeSpace), format(maxFreeSpace));
		}
		return false;
	}

}
