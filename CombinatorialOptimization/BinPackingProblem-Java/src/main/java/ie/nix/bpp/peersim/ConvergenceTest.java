package ie.nix.bpp.peersim;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class ConvergenceTest extends ie.nix.ecj.PeerSimProblem.ConvergenceTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public ConvergenceTest(String prefix) {
		super(prefix);
	}

	@Override
	public boolean isConverged() {
		int numberOfItemsQueued = Queue.getQueue().getNumberOfItemsQueued();
		boolean isConverged = numberOfItemsQueued == 0;
		LOGGER.debug("[{}] numberOfItemsQueued={}", CommonState.getTime(), numberOfItemsQueued);
		return isConverged;
	}
	
}
