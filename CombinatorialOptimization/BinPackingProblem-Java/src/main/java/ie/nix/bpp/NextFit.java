package ie.nix.bpp;

import java.util.List;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.peersim.BinPacker;
import ie.nix.bpp.peersim.Queue;
import peersim.core.CommonState;

/*
 * Next Fit is a bounded-space online algorithm 
 * where only one bin, the most recent, is open 
 * at a time. [Johnson Thesis 1973]
 */
public class NextFit extends BinPacker {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public NextFit(String prefix) {
		super(prefix);
	    LOGGER.debug("[{}]", CommonState.getTime());
	}

	@Override
	public void pack(Queue queue, List<Bin> bins) {
		
		int openBinIndex = getOpenBinIndex(bins);
		Bin openBin = bins.get(openBinIndex);
		
		while (!queue.isEmpty()) {
			Item item = queue.poll();
        	LOGGER.trace("[{}] Processing {}", CommonState.getIntTime(), item);
		
	        if (!openBin.canFit(item)) {
	        	openBinIndex++;
	        	openBin = bins.get(openBinIndex);
	        }
	        openBin.addItem(item);
	        LOGGER.debug("[{}] Added {} it {}", CommonState.getIntTime(), item, openBin);   
	        
		}
	}
	
	protected Predicate<Bin> isOpen() {
		return bin -> !bin.isEmpty();
	}
	
	protected Predicate<Bin> isNotOpen() {
		return bin -> bin.isEmpty();
	}

	protected int getOpenBinIndex(List<Bin> binsList) {
		int openBinIndex = 0;
		for (Bin bin : binsList) {
			if (!bin.isEmpty()) {
				openBinIndex++;
			} else {
				break;
			}
		}
		return openBinIndex;
	}
	
}