package ie.nix.bpp;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TripletBinPackingProblem extends BinPackingProblem {

	private static final Logger LOGGER = LogManager.getLogger();
		
	public TripletBinPackingProblem(int binCapacity, int minFirstTriplesItemSize, int maxFirstTriplesItemSize,
			int minSecondTriplesItemSize, int numberOfItems, int numberOfRuns) {
		Random random = new Random();
		IntStream.range(0, numberOfRuns).forEach(run -> {
			random.setSeed(run);
			LinkedList<Item> queue = new LinkedList<Item>();
			IntStream.range(0, numberOfItems / 3).forEach(i -> {
				int size1 = minFirstTriplesItemSize + random.nextInt(maxFirstTriplesItemSize - minFirstTriplesItemSize);
				int maxSecondTriplesItemSize = (binCapacity - size1) / 2;
				int size2 = minSecondTriplesItemSize
						+ random.nextInt(maxSecondTriplesItemSize - minSecondTriplesItemSize);
				int size3 = binCapacity - (size1 + size2);
				Item item1 = new Item(size1);
				queue.add(item1);
				Item item2 = new Item(size2);
				queue.add(item2);
				Item item3 = new Item(size3);
				queue.add(item3);
				LOGGER.trace("sumOfItems=" + (size1 + size2 + size3));

			});
			// Add a shuffle otherwise it favours packing them in order, so NextFit.
			Collections.shuffle(queue);
			LOGGER.trace("queueSize=" + queue.size());

			addQueue(queue);
		});
	}

	public static void main(String[] args) {	
		String usage = "Please pass in one argument structured as follows: "
				+ "minFirstTriplesItemSize-maxFirstTriplesItemSize-"
				+ "minSecondTriplesItemSize-numberOfItems.";
		LOGGER.debug("args={}",Arrays.toString(args));
		assert(args.length >= 3) : usage +"There was not enough arge; there was "+args.length+" but should be 3.";

		int binCapacity = Integer.valueOf(args[0]);
		
		String[] ubppArgs = args[1].split("-");	
		LOGGER.debug("ubppArgs={}",Arrays.toString(ubppArgs));
		assert(ubppArgs.length >= 4) : usage+"The argument was not correclty formatted";
		
		int minFirstTriplesItemSize = Integer.valueOf(ubppArgs[0]);
		int maxFirstTriplesItemSize = Integer.valueOf(ubppArgs[1]);
		int minSecondTriplesItemSize = Integer.valueOf(ubppArgs[2]);
		int numberOfItems = Integer.valueOf(ubppArgs[3]);
		
		int numberOfRuns = Integer.valueOf(args[2]);

		String fileName = "TripletBinPackingProblem-"+args[1]+".csv";
		TripletBinPackingProblem tripletBinPackingProblem = new TripletBinPackingProblem(
				binCapacity, minFirstTriplesItemSize, maxFirstTriplesItemSize,
				minSecondTriplesItemSize, numberOfItems, numberOfRuns);
		tripletBinPackingProblem.saveItemToFile(fileName);
		
	}
}
