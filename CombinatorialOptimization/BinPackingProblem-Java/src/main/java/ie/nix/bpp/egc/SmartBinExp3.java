package ie.nix.bpp.egc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SmartBinExp3 extends EvolvedSmartBin {

	private static final Logger LOGGER = LogManager.getLogger();   
	
	public SmartBinExp3(String prefix) {
		super(prefix);
		init();
	}
	
	protected void init() {
		shouldTender = (int freeCapacity, int itemSize) ->(itemSize < freeCapacity);
		getGuide = (int freeCapacity, int itemSize) ->min((div((freeCapacity + (0.0 - itemSize)), 1.0) + freeCapacity), (abs(abs(freeCapacity)) - max((freeCapacity + itemSize), 0.0)));
		tenderToDouble = (double guide, int freeCapacity, int itemSize) ->min(min((freeCapacity + min(0.0, div(freeCapacity, guide))), freeCapacity), abs(div(div(itemSize, freeCapacity), 0.0)));
		shouldBid = (double guide, int freeCapacity, int itemSize) ->((((guide + guide) + freeCapacity) * itemSize) < ((guide + guide) + freeCapacity));
		getOffer = (double guide, int freeCapacity, int itemSize) ->freeCapacity;
		bidToDouble = (double offer, int freeCapacity, int itemSize) ->max(0.0, offer);
		shouldAward = (double offer, int freeCapacity, int itemSize) ->true;
	}

}