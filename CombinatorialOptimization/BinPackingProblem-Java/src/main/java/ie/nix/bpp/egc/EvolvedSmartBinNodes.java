package ie.nix.bpp.egc;

import ie.nix.ecj.gp.Variable;

public class EvolvedSmartBinNodes {

	@SuppressWarnings("serial")
	public static class BinCapacity extends Variable {

		public static double value;

		public BinCapacity() {
			super("binCapacity", (results, result) -> result.set(BinCapacity.value));
		}
		
		@Override
		public boolean isPositive() {
			return true;
		}

	}
	
	@SuppressWarnings("serial")
	public static class FreeCapacity extends Variable {

		public static double value;

		public FreeCapacity() {
			super("freeCapacity", (results, result) -> result.set(FreeCapacity.value));
		}

		@Override
		public boolean isPositive() {
			return true;
		}

	}
	
	@SuppressWarnings("serial")
	public static class NumberOfItems extends Variable {

		public static double value;

		public NumberOfItems() {
			super("numberOfItems", (results, result) -> result.set(NumberOfItems.value));
		}
		
		@Override
		public boolean isPositive() {
			return true;
		}

	}
	
	@SuppressWarnings("serial")
	public static class ItemSize extends Variable {

		public static double value;

		public ItemSize() {
			super("itemSize", (results, result) -> result.set(ItemSize.value));
		}
		
		@Override
		public boolean isPositive() {
			return true;
		}

	}
	
}
