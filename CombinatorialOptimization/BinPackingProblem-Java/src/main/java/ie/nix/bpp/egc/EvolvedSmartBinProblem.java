package ie.nix.bpp.egc;

import java.util.DoubleSummaryStatistics;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ec.EvolutionState;
import ec.util.Parameter;
import ie.nix.bpp.Bin;
import ie.nix.bpp.peersim.BinAdapter;
import ie.nix.bpp.peersim.Queue;
import ie.nix.egc.EGCProblem;
import ie.nix.gc.NodeAdapter;
import peersim.core.Network;

@SuppressWarnings("serial")
public class EvolvedSmartBinProblem extends EGCProblem {

	private static final Logger LOGGER = LogManager.getLogger();

	protected static final String P_MODEL_PROPERTIES 			= "model_properties";
	protected static final String P_SIZE 						= "bpp_file.size";
	protected static final String P_BPP_FILE 					= "bpp_file";
	protected static final String P_BIN_CAPACITY 				= "bin_capacity";
	protected static final String P_NUMBER_OF_BINS 				= "number_of_bins";

	protected String modelPropertiesFile;
	protected int size;
	protected String[] bppFiles;
	protected int[] binCapacity;
	protected int[] numberOfBins;

	protected int bpp;

	public static EvolvedSmartBin getAgent(int index) {
		return (EvolvedSmartBin) ((BinAdapter) Network.get(index)).getProtocol(0);
	}
	
	public static Stream<EvolvedSmartBin> getAgents() {
		Stream.Builder<EvolvedSmartBin> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(getAgent(n));
		}
		return streamBuilder.build();
	}

	public static Bin getNode(int index) {
		return ((BinAdapter) Network.get(index)).getNode();
	}
	
	public static Stream<Bin> getNodes() {
		Stream.Builder<Bin> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(getNode(n));
		}
		return streamBuilder.build();
	}

	public void setup(final EvolutionState state, final Parameter base) {
	    // very important, remember this
	    super.setup(state, base);
	    
		modelPropertiesFile = state.parameters.getStringWithDefault(
				base.push(P_MODEL_PROPERTIES),
				defaultBase().push(P_MODEL_PROPERTIES), null);
		LOGGER.debug("modelPropertiesFile={}", modelPropertiesFile);
		assert (modelPropertiesFile != null) : 
			"Specify the properties file in the PeerSim model propertoes file";	
	
		size = state.parameters.getIntWithDefault(
				base.push(P_SIZE), defaultBase().push(P_SIZE), 0);
		bppFiles = new String[size];
		binCapacity = new int[size];
		numberOfBins = new int[size];
		LOGGER.debug("size={}", size);
		assert (size != 0) : "Specify at least 1 BPP file with "+base.push(P_SIZE);
		
		for (int bpp = 0; bpp < size; bpp++) {
			String bppFileParam = bpp+"."+P_BPP_FILE;
			bppFiles[bpp] = state.parameters.getStringWithDefault(
					base.push(bppFileParam), 
					defaultBase().push(bppFileParam), null);
			LOGGER.debug("bppFiles[{}]={}", bpp, bppFiles[bpp]);
			assert (bppFiles[bpp] != null) : 
				"Specify BPP file"+bppFileParam+".";
	
			String bppCapacityParam = bpp+"."+P_BIN_CAPACITY;
			binCapacity[bpp] = state.parameters.getIntWithDefault(
					base.push(bppCapacityParam), 
					defaultBase().push(bppCapacityParam), 0);
			LOGGER.debug("binCapacity={}", binCapacity);
			assert (binCapacity[bpp] != 0) : 
				"Bin capacity must be greater than 0, specify "+P_BIN_CAPACITY+".";
	
			String numberOfBinsParam = bpp+"."+P_NUMBER_OF_BINS;
			numberOfBins[bpp] = state.parameters.getIntWithDefault(
					base.push(numberOfBinsParam), 
					defaultBase().push(numberOfBinsParam), 0);
			LOGGER.debug("numberOfBins={}", numberOfBins);
			assert (numberOfBins[bpp] != 0) : 
				"Number of bins must be greater than 0, specify "+P_NUMBER_OF_BINS+".";
		}
		
		
	}

	public int getNumberOfBins() {
		return  Network.size();
	}

	public int getNumberOfOpenBins() {
		int numberOfOpenBins = 0;
		for (int n = 0; n < Network.size(); n++) {
			Bin bin = getNode(n);
			if (!bin.isEmpty()) {
				numberOfOpenBins++;
			}
		}
		return numberOfOpenBins;
	}
	
	public int getNumberClosedBins() {
		return Network.size() - getNumberOfOpenBins();
	}
	
	public double getFractionClosedBins() {
		return 1d * getNumberClosedBins() / Network.size();
	}

	public int getMinBinsNeeded() {
		double usedBinCapacity = getUsedBinCapacity();
		return (int)Math.ceil(usedBinCapacity/binCapacity[bpp]);
	}

	public int getExtraBins() {
		double minBins = getMinBinsNeeded();
		double openBins = getNumberOfOpenBins();
		return (int)Math.ceil(openBins - minBins);
	}

	public double getNormalisedNumberOfExtraBins() {
		double minBins = getMinBinsNeeded();
		return getExtraBins()/(numberOfBins[bpp] - minBins);
	}

	public double getFreeBinCapacity() {
		DoubleSummaryStatistics binFreeSpaceStats = getNodes().
				filter(bin -> !bin.isEmpty()).
				mapToDouble(bin -> bin.getFreeCapacity()).
				summaryStatistics();
		return binFreeSpaceStats.getSum();
	}
	
	public double getUsedBinCapacity() {
		DoubleSummaryStatistics binFreeSpaceStats = getNodes().
				filter(bin -> !bin.isEmpty()).
				mapToDouble(bin -> bin.getUsedCapacity()).
				summaryStatistics();
		return binFreeSpaceStats.getSum();
	}

	public double getFractionOfItemsQueued() {
		return 1d * getNumberOfItemsQueued() / Queue.getQueue().getInitialNumberOfItems();
	}

	public int getNumberOfItemsQueued() {
		return Queue.getQueue().getNumberOfItemsQueued();
	}

	public void initSimulation() {
		super.initSimulation();
		bpp = run % bppFiles.length;
		String bppFile = bppFiles[bpp];
		int bppRun = run / bppFiles.length;
//		int bppRun = state.random[threadnum].nextInt(bppFiles.length);
		// Simply making a new Queue will set the singleton to it.
		Queue queue = new Queue(bppFile, bppRun);
		BinAdapter.setDefaultBinCapacity(binCapacity[bpp]);
		NodeAdapter.setNetworkSize(numberOfBins[bpp]);
		LOGGER.error("generation={}, gpIndividual={}, bpp={}, bppFile={}, "
				+ "bppRun={}, numberOfItems={}, "
				+ "networkSize={}, binCapacity={}", 
				state.generation, gpIndividual.hashCode(), bpp, bppFile,
				bppRun, queue.getInitialNumberOfItems(), 
				NodeAdapter.getNetworkSize(), BinAdapter.getDefaultBinCapacity());
	}
	
	protected String[] getSimulationProperties() {
		String[] modelProperties = new String[] { 
				modelPropertiesFile};
		String[] properties = 
				super.getSimulationProperties();
		return Stream.of(modelProperties, properties).
				flatMap(Stream::of).
				toArray(String[]::new);   
	}
	
	public double evaluateSimulation() {
		double freeBinCapacity = getFreeBinCapacity();
		recordStat("freeBinCapacity", freeBinCapacity);
		
		int openBins = getNumberOfOpenBins();
		recordStat("openBins", (double)openBins);
		
		double extraBins = getExtraBins();
		recordStat("extraBins", extraBins);
		
		int numberOfItemsQueued = getNumberOfItemsQueued();
		recordStat("numberOfItemsQueued", numberOfItemsQueued);
		
		double evaluation = extraBins + numberOfItemsQueued;
		LOGGER.debug("generation={}, gpIndividual={}, evaluation={}, "
				+ "numberOfItemsQueued={}, openBins={}, extraBins={}", 
				state.generation, gpIndividual.hashCode(), evaluation, 
				numberOfItemsQueued, openBins, extraBins);
		return evaluation;
		
	}
}
