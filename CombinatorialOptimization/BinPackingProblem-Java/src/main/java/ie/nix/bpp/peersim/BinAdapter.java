package ie.nix.bpp.peersim;

import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Bin;
import ie.nix.gc.NodeAdapter;
import peersim.config.Configuration;
import peersim.core.CommonState;

public class BinAdapter extends NodeAdapter<Bin> {
	
	private static final Logger LOGGER = LogManager.getLogger();

	private static final String BIN_CAPACITY = "init.nodes.bin-capacity";
	
	private static int defaultBinCapacity = -1;
	
	public BinAdapter(String prefix) {
		super(prefix);
		if (defaultBinCapacity == -1) {
			defaultBinCapacity = Configuration.getInt(BIN_CAPACITY);
		}
		LOGGER.trace("[{}]~defaultBinCapacity={}", CommonState.getTime(), defaultBinCapacity);
	}
	
	@Override
	public String toString() {
		if (getNode() == null) {
			return "BinAgent.Adapter []";
		} else {
			return "BinAgent.Adapter [node="+getNode()+"]";
		}
	}
	
	public static int getDefaultBinCapacity() {
		return defaultBinCapacity;
	}

	public static void setDefaultBinCapacity(int binCapacity) {
		defaultBinCapacity = binCapacity;
		LOGGER.debug("[0]~defaultBinCapacity={}", defaultBinCapacity);
	}

	public void initNode(NodeAdapter<Bin> nodeAdapter) {
		nodeAdapter.setNode(new Bin(defaultBinCapacity));
	}
}