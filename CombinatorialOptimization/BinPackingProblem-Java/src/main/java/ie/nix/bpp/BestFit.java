package ie.nix.bpp;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.OptionalInt;
import java.util.TreeMap;
import java.util.function.Predicate;
import java.util.stream.IntStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.peersim.BinPacker;
import ie.nix.bpp.peersim.Queue;
import peersim.core.CommonState;

public class BestFit extends BinPacker {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public BestFit(String prefix) {
		super(prefix);
	    LOGGER.debug("[{}]", CommonState.getTime());
	}

	public void pack(Queue queue, List<Bin> bins) {

		Map<Integer, Integer> binsFreeCapacity = new TreeMap<Integer, Integer>();
		
		IntStream.range(0, bins.size()-1).forEach(b -> {
			binsFreeCapacity.put(b, bins.get(b).getFreeCapacity());
		});
		
		while (!queue.isEmpty()) {
			Item item = queue.poll();
        	LOGGER.trace("[{}] Processing {}", CommonState.getIntTime(), item);
			
	        OptionalInt maybeBinIndex = binsFreeCapacity.entrySet().stream().
	        	sorted(sortByValue()).
	        	filter(hasSpaceForItem(item)).
	        	mapToInt(entry -> entry.getKey()).
	        	findFirst();
	        
	        if (maybeBinIndex.isPresent()) {
	        	int binIndex = maybeBinIndex.getAsInt();
	        	Bin bin = bins.get(binIndex);
	        	bin.addItem(item);
	        	binsFreeCapacity.put(binIndex, binsFreeCapacity.get(binIndex) - item.getSize());
	        	LOGGER.debug("[{}] Added {} it {}", CommonState.getIntTime(), item, bin);
	        } else {
	        	LOGGER.warn("[{}] Not enough space to add {}", CommonState.getIntTime(), item);
	        }
	        
		}
	}
	
	protected Predicate<? super Entry<Integer, Integer>> hasSpaceForItem(Item item) {
		return entry -> entry.getValue() >= item.getSize();
	}
	
	protected Comparator<? super Entry<Integer, Integer>> sortByValue() {
		return (entry1, entry2) -> {
			return entry1.getValue().compareTo(entry2.getValue());
		};
	}
}