package ie.nix.bpp;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.peersim.Queue;
import peersim.core.CommonState;

/*
 * Worst Fit algorithm packs the item into the emptiest open bin.
 */
//	public static class WorstFit extends SimpleBinPacker {
//		public WorstFit() {
//			pack = (queue -> {
//				// Open the first bin
//				openBin();
//				
//				// Sort in decreasing order, then for each item in the queue
//				queue.stream().forEach(item -> {
//					// for each bin
//					Optional<Bin> optionalBin = bins.stream().sorted(Collections.reverseOrder()).filter(bin -> bin.canFit(item)).findFirst();
//					if (optionalBin.isPresent()) {
//						// put in the first one it fits in
//						optionalBin.get().addItem(item);
//						
//					} else {
//						// Doesn't fit so open a new bin
//						openBin();
//						getLastBin().addItem(item);
//						
//					}	
//				});
//				return getNumberOfBins();
//			});
//		}
//	}
public class WorstFit extends NextFit {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public WorstFit(String prefix) {
		super(prefix);
	    LOGGER.debug("[{}]", CommonState.getTime());
	}

	@Override
	public void pack(Queue queue, List<Bin> bins) {
		
		while (!queue.isEmpty()) {
			Item item = queue.poll();
        	LOGGER.trace("[{}] Processing {}", CommonState.getIntTime(), item);
			
        	Optional<Bin> maybeBin = bins.stream().
        			filter(isOpen()).
        			filter(hasSpaceForItem(item)).
        			sorted(Collections.reverseOrder()).
        			findFirst();
	        
	        if (maybeBin.isPresent()) {
	        	Bin bin  = maybeBin.get();
	        	bin.addItem(item);
	        	LOGGER.debug("[{}] Added {} it {}", CommonState.getIntTime(), item, bin);
	        } else {
	        	Optional<Bin> maybeNextBin = nextClosedBin(bins);
	        	if (maybeNextBin.isPresent()) {
		        	Bin bin  = maybeNextBin.get();
		        	bin.addItem(item);
		        	LOGGER.debug("[{}] Added {} it {}", CommonState.getIntTime(), item, bin);
	        	} else {
	        		LOGGER.warn("[{}] Not enough space to add {}", CommonState.getIntTime(), item);
	        	}
	        }
	        
	        
		}
	}

	public Optional<Bin> nextClosedBin(List<Bin> bins) {
		return bins.stream().
				filter(isNotOpen()).
				findFirst();
	}

	protected Predicate<Bin> hasSpaceForItem(Item item) {
		return bin -> bin.getFreeCapacity() >= item.getSize();
	}
	
}