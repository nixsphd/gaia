package ie.nix.bpp.gc;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Bin;
import ie.nix.bpp.Item;
import ie.nix.bpp.peersim.Queue;
import ie.nix.gc.AwardMessage;
import ie.nix.gc.BidMessage;
import ie.nix.gc.GCAgent;
import ie.nix.gc.TenderMessage;

public class SmartBin extends GCAgent<Bin, Item, Void> {

	private static final Logger LOGGER = LogManager.getLogger();

	// TODO - Should I pull this model of having a single task into an inner object in Agent?
	// TODO - Should this be an Optional?
	private Item task;
	
	public SmartBin(String prefix) {
		super(prefix);
	}
	
	@Override
	public String toString() {
		int index = this.getNodeAdapter().getIndex();
		Bin bin = this.getNodeAdapter().getNode();
		if (hasTask()) {
			return "SmartBin-"+index+" ["+bin+", "+task+"]";
		} else {
			return "SmartBin-"+index+" ["+bin+", no task]";
		}
	}
	
	/*
	 * Task helper methods
	 */
	public boolean hasTask() {
		return task != null;
	}
	
	protected Item getTask() {
		return task;
	}

	protected void setTask(Item task) {
		this.task = task;
	}

	protected Item clearTask() {
		// Need to temp store the task so clear it and then send it back.
		Item task = this.task;
		this.task = null;
		LOGGER.debug("[{}] {} -> {}", util.getTime(), this, task);
		return task;
	}

	/*
	 * Non evolved methods
	 */
	@Override
	protected void preTender(Bin bin) {
		Optional<Item> maybeItem = Optional.ofNullable(Queue.getQueue().peek());
		if (getState().isIdle() && !hasTask() && maybeItem.isPresent() 
				&& bin.canFit(maybeItem.get())) {
			setTask(Queue.getQueue().poll());
		}
		LOGGER.debug("[{}] agent={}, bin={}, maybeItem={}, task={}", util.getTime(), 
				this, bin, maybeItem, task);
	}

	@Override
	protected Item getTask(Bin bin) {
		Item task = getTask();
		LOGGER.debug("[{}] agent={}, bin={}, task={}", 
				util.getTime(), this, bin, task);
		return task;
	}
	
	@Override
	protected Void getProposal(TenderMessage<Bin, Item, Void> tenderMessage) {
		Void proposal = null;
		LOGGER.debug("[{}] agent={}, proposal={}", util.getTime(), this, proposal);
		return proposal;
	}
	
	@Override
	protected void award(AwardMessage<Bin, Item, Void> awardMessage) {
		Item task = clearTask();
		LOGGER.debug("[{}] agent={}, task={}", 
				util.getTime(), this, task);
	}

	@Override
	protected void awarded(AwardMessage<Bin, Item, Void> awardMessage) {
		Bin myBin = awardMessage.getTo().getNode();
		Item item = awardMessage.getTask();

		LOGGER.debug("[{}] agent={}, added item={} to myBin={}", 
				util.getTime(), this, item, myBin);
		myBin.addItem(item);
		LOGGER.debug("[{}] agent={}, added item={} to myBin={}", 
				util.getTime(), this, item, myBin);
	}

	@Override
	protected void postTender(Bin bin) {
		LOGGER.trace("[{}] agent={},  bin={}, task={}", 
				util.getTime(), this, bin, task);
		if (hasTask()) {
			Item task = clearTask();
			// So the task was never awarded so we should assign it to ourselves.
			bin.addItem(task);
			LOGGER.debug("[{}] agent={}, added item={} to myBin={}", 
					util.getTime(), this, task, bin);
		}
	}

	/*
	 * Evolved methods
	 */
	@Override
	protected boolean shouldTender(Bin bin) {
		boolean shouldTender = hasTask(); 
		LOGGER.debug("[{}] agent={}, bin={}, isIdle={}, hasTask()={}, shouldTender={}", 
				util.getTime(), this, bin, getState().isIdle(), hasTask(), shouldTender);
		return shouldTender;
	}

	@Override
	protected double getGuide(Bin bin, Item item) {
		double guide = bin.getFreeCapacity();
		LOGGER.debug("[{}] agent={}, guide={}", 
				util.getTime(), this, guide);
		return guide;
	}

	@Override
	protected double tenderToDouble(TenderMessage<Bin, Item, Void> tenderMessage) {
		Bin myBin = tenderMessage.getTo().getNode();
		Item item = tenderMessage.getTask();
		double value = Double.POSITIVE_INFINITY;
		if (myBin.canFit(item)) {
			// it can fit, so the bigger the item being tendered, that fits, the better!
			value = -item.getSize();
		} 
		LOGGER.trace("[{}] agent={}, myBin={}. item={}, value={}", 
				util.getTime(), this, myBin, item, value);
		return value;
	}

	@Override
	protected boolean shouldBid(TenderMessage<Bin, Item, Void> tenderMessage) {
		Bin myBin = tenderMessage.getTo().getNode();
		Item item = tenderMessage.getTask();
		boolean shouldBid = !hasTask() && getState().isIdle() && myBin.canFit(item); 
		LOGGER.debug("[{}] agent={}, bin={}, item={} shouldBid={}", util.getTime(), 
				this, myBin, item, shouldBid);
		return shouldBid;
	}

	@Override
	protected double getOffer(TenderMessage<Bin, Item, Void> tenderMessage) {
		Bin myBin = tenderMessage.getTo().getNode();
		double offer = myBin.getFreeCapacity();
		LOGGER.debug("[{}] agent={}, offer={}", 
				util.getTime(), this, offer);
		return offer;
	}

	@Override
	protected double bidToDouble(BidMessage<Bin, Item, Void> bidMessage) {
		return bidMessage.getOffer();
	}

	@Override
	protected boolean shouldAward(BidMessage<Bin, Item, Void> bidMessage) {
		Bin myBin = bidMessage.getTo().getNode();
		boolean shouldAward = bidMessage.getOffer() <= myBin.getFreeCapacity();
		LOGGER.debug("[{}] agent={}, shouldAward={}", 
				util.getTime(), this, shouldAward);
		return shouldAward;
	}

}