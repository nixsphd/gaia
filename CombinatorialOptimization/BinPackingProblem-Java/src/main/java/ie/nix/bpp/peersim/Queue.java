package ie.nix.bpp.peersim;

import ie.nix.bpp.BinPackingProblem;
import ie.nix.bpp.Item;
import java.util.LinkedList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Control;

public class Queue implements Control {
	
	private static final Logger LOGGER = LogManager.getLogger();

	private static final String BIN_PACKING_PROBLEM = "bpp_file";
	private static final String RUN = "run";
	private static final Integer RANDOM_RUN = -1;
	
	private static Queue theQueue;
	
	public static Queue getQueue() {
		return theQueue;
	}
	
	public static void setQueue(Queue theQueue) {
		Queue.theQueue = theQueue;
		LOGGER.trace("[0] theQueue={}", theQueue);
	}
	
	protected String bppFile;
	protected int run;
	protected java.util.Queue<Item> queue;
	protected int initialNumberOfItems;
	protected double initialSizeOfItems;

	public Queue(String prefix) {
		this(Configuration.getString(prefix+"."+BIN_PACKING_PROBLEM), 
				Configuration.getInt(prefix+"."+RUN));
	}

	public Queue(String bppFile, int run) {
		this.bppFile = bppFile;
		this.run = run;
		init();
		setQueue(this);
		LOGGER.trace("[0] bppFile={}, run={}", bppFile, run);	
	}
	
	@Override
	public String toString() {
		if (queue != null) {
			return "Queue [bppFile=" + bppFile + ", run=" + run + ", queueSize=" + queue.size() + "]";
		} else {
			return "Queue [bppFile=" + bppFile + ", run=" + run + ", queueSize=0]";
		}
	}

	@Override
	public boolean execute() {
        if (CommonState.getTime() == 0) {
        	init();
        }
		return false;
	}

	public void init() {
		this.queue = new BinPackingProblem(bppFile).getQueues(run);
		this.initialNumberOfItems = queue.size();
		this.initialSizeOfItems = queue.stream().mapToDouble(item -> item.getSize()).sum();
		LOGGER.debug("[0] Initialized the size={} sumOfItems={}", 
				initialNumberOfItems, initialSizeOfItems);
	}

	public boolean isEmpty() {
		return queue.isEmpty();
	}
	
	public int getInitialNumberOfItems() {
		return initialNumberOfItems;
	}
	
	public int getNumberOfItemsQueued() {
		return queue.size();
	}
	
	public double getInitialSizeOfItems() {
		return initialSizeOfItems;
	}
	
	public double getSizeOfItemsQueued() {
		return queue.stream().mapToDouble(item -> item.getSize()).sum();
	}

	public Item peek() {
		return queue.peek();
	}

	public Item poll() {
		return queue.poll();
	}

	public List<Item> getItems() {
		return new LinkedList<Item>(queue);
	}

}
