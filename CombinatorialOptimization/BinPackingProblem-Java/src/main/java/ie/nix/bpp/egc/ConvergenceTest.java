package ie.nix.bpp.egc;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import peersim.core.CommonState;

public class ConvergenceTest extends ie.nix.bpp.peersim.ConvergenceTest {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public ConvergenceTest(String prefix) {
		super(prefix);
	}

	@Override
	public boolean isConverged() {
		int busySmartBins = (int)EvolvedSmartBinProblem.getAgents().filter(
				smartBin -> !smartBin.getState().isIdle()).count();
		boolean isConverged = super.isConverged() && busySmartBins == 0;
		LOGGER.info("[{}] super.isConverged()={}, busySmartBins={}", 
				CommonState.getTime(), super.isConverged(), busySmartBins);
		return isConverged;
	}
}
