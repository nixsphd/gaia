package ie.nix.bpp;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.peersim.Queue;
import peersim.core.CommonState;

/*
 * Almost Worst Fit algorithm packs the item into the second emptiest bin 
 * unless only one bin in which it fits, in which case it goes into that 
 * bin. Ties are broken favouring the lowest index bin.
 * This modification from the worst fit algorithm is a significant improvement.
 */
//	public static class AlmostWorstFit extends SimpleBinPacker {
//		public AlmostWorstFit() {
//			pack = (queue -> {
//				// Open the first bin
//				openBin();
//				
//				// Sort in decreasing order, then for each item in the queue
//				queue.stream().forEach(item -> {
//					// for each bin
//					Optional<Bin> optionalBin = bins.stream().sorted(Collections.reverseOrder()).filter(bin -> bin.canFit(item)).skip(1).findFirst();
//					if (optionalBin.isPresent()) {
//						// put in the first one it fits in
//						optionalBin.get().addItem(item);
//					} else {
//						optionalBin = bins.stream().sorted(Collections.reverseOrder()).filter(bin -> bin.canFit(item)).findFirst();
//						if (optionalBin.isPresent()) {
//							// put in the first one it fits in
//							optionalBin.get().addItem(item);
//						} else {
//							// Doesn't fit so open a new bin
//							openBin();
//							getLastBin().addItem(item);
//						}
//					}	
//				});
//				return getNumberOfBins();
//			});
//		}
//	}

public class AlmostWorstFit extends WorstFit {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public AlmostWorstFit(String prefix) {
		super(prefix);
	    LOGGER.debug("[{}]", CommonState.getTime());
	}

	@Override
	public void pack(Queue queue, List<Bin> bins) {
		
		while (!queue.isEmpty()) {
			Item item = queue.poll();
        	LOGGER.trace("[{}] Processing {}", CommonState.getIntTime(), item);
			
        	List<Bin> openBinsWithSpaceEmptiestFirst = bins.stream().
        			filter(isOpen()).
        			filter(hasSpaceForItem(item)).
        			sorted(Collections.reverseOrder()).
        			collect(Collectors.toList());
	        
	        if (openBinsWithSpaceEmptiestFirst.size() >= 2) {
	        	Bin bin  = openBinsWithSpaceEmptiestFirst.get(1);
	        	bin.addItem(item);
	        	LOGGER.debug("[{}] Added {} it {}", CommonState.getIntTime(), item, bin);
	        	
	        } else if (openBinsWithSpaceEmptiestFirst.size() >= 1) {
	        	Bin bin  = openBinsWithSpaceEmptiestFirst.get(0);
	        	bin.addItem(item);
	        	LOGGER.debug("[{}] Added {} it {}", CommonState.getIntTime(), item, bin);
	        	
	        } else {
	        	Optional<Bin> maybeNextBin = nextClosedBin(bins);
	        	if (maybeNextBin.isPresent()) {
		        	Bin bin  = maybeNextBin.get();
		        	bin.addItem(item);
		        	LOGGER.debug("[{}] Added {} it {}", CommonState.getIntTime(), item, bin);
		        	
	        	} else {
	        		LOGGER.warn("[{}] Not enough space to add {}", CommonState.getIntTime(), item);
	        		
	        	}
	        }
		}
	}
	
	protected Predicate<Bin> hasSpaceForItem(Item item) {
		return bin -> bin.getFreeCapacity() >= item.getSize();
	}
	
}