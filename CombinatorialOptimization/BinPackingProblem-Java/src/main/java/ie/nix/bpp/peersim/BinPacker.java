package ie.nix.bpp.peersim;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ie.nix.bpp.Bin;
import ie.nix.bpp.peersim.Queue;
import ie.nix.gc.NodeAdapter;
import peersim.core.CommonState;
import peersim.core.Control;
import peersim.core.Network;

public abstract class BinPacker implements Control, Cloneable {

	private static final Logger LOGGER = LogManager.getLogger();
	
	public BinPacker(String prefix) {
	    LOGGER.debug("[{}]", CommonState.getTime());
	}
	
	@Override
	public BinPacker clone() {
		BinPacker foo = null;
		try {
			foo = (BinPacker)super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
        LOGGER.debug("[{}]", CommonState.getTime());
		return foo;
	}
	
	@Override
	public boolean execute() {
	    LOGGER.trace("[{}]", CommonState.getTime());
		if (CommonState.getTime() > 0) {
			pack(Queue.getQueue(), getBins());
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private List<Bin> getBins() {
		Stream.Builder<Bin> streamBuilder = Stream.builder();
		for (int n = 0; n < Network.size(); n++) {
			streamBuilder.add(((NodeAdapter<Bin>)Network.get(n)).getNode());
		}
		return streamBuilder.build().collect(Collectors.toList());
	}
	
	public abstract void pack(Queue queue, List<Bin> bins);

}

/*
	//First Fit Decreasing algorithm is a specialisation of the 
	// First Fit algorithm which can be applied in offline sequences 
	// of items. The items are first sorted in decreasing size order 
	// then packed using the First Fit algorithm.
	public static class FirstFitDecreasing extends SimpleBinPacker {
		public FirstFitDecreasing() {
			pack = (queue -> {
				// Open the first bin
				openBin();
				// Sort in decreasing order, then for each item in the queue
				queue.stream().sorted(Collections.reverseOrder()).forEach(item -> {
					// for each bin
					Optional<Bin> optionalBin = bins.stream().filter(bin -> bin.canFit(item)).findFirst();
					if (optionalBin.isPresent()) {
						// put in the first one it fits in
						optionalBin.get().addItem(item);
					} else {
						// Doesn't fit so open a new bin
						openBin();
						getLastBin().addItem(item);
					}	
				});
				return getNumberOfBins();
			});
		}
	}
*/
/*
	// Best Fit Decreasing algorithm is a specialisation of the 
	// Best Fit algorithm where the items are first sorted in 
	// decreasing size order then packed using the Best Fit algorithm.
	public static class BestFitDecreasing extends SimpleBinPacker {
		public BestFitDecreasing() {
			pack = (queue -> {
				// Open the first bin
				openBin();
				
				// Sort in decreasing order, then for each item in the queue
				queue.stream().sorted(Collections.reverseOrder()).forEach(item -> {
					// for each bin
					Optional<Bin> optionalBin = bins.stream().sorted().filter(bin -> bin.canFit(item)).findFirst();
					if (optionalBin.isPresent()) {
						// put in the first one it fits in
						optionalBin.get().addItem(item);
					} else {
						// Doesn't fit so open a new bin
						openBin();
						getLastBin().addItem(item);
					}	
				});
				return getNumberOfBins();
			});
		}
	}
	
*/