
# setwd("~/Desktop/Gaia/CombinatorialOptimization/BinPackingProblem/src/main/R")

source(file.path("Util.R"))

args = commandArgs(trailingOnly=TRUE)
models = args
# models = c("Exp1", "BestFit")
results_dir="../../../results"

csv_file = "OpenBins.csv"
boxplot_file="OpenBins"
table_file="OpenBins.txt"
ylab="Open Bins"
column_names = c("Time", "Open Bins", "TestCase", "Model", "Seed")
bpp <- "BinPackingProblem"
uniformBPP <- "UniformBinPackingProblem"
tripletBPP <- "TripletBinPackingProblem"

dataFiles <- listCSVFiles(models, csv_file);
data <- readDataFiles(dataFiles, column_names)
modelsData <- getModelData(data)

path <- file.path(results_dir, sprintf("Overview-%s", boxplot_file))
boxPlot <- ggplot(modelsData, aes(x=Model, y=`Open Bins`)) + 
    geom_boxplot() +
    expand_limits(y = 0) +
    labs(x="Model",y=ylab)
boxPlot
savePlot(path, plot=boxPlot)

path <- file.path(results_dir, sprintf("Overview-%s", table_file))
saveOverviewTable(modelsData, 3, path, bpp)

uniformBPPData <- modelsData[grep(uniformBPP, modelsData$TestCase),]
uniformBPPPath <- file.path(results_dir, sprintf("%s-%s",uniformBPP, boxplot_file))
uniformBPPPlots <- getBoxPlots(modelsData=uniformBPPData, ylab, boxplot_file)
saveBigPlot(uniformBPPPath, plot=do.call(grid.arrange, uniformBPPPlots))

uniformBPPPath <- file.path(results_dir, sprintf("%s-%s",uniformBPP, table_file))
saveTable(uniformBPPData, 3, uniformBPPPath, uniformBPP)

tripletBPPData <- modelsData[grep(tripletBPP, modelsData$TestCase),]
tripletBPPPath <- file.path(results_dir, sprintf("%s-%s",tripletBPP, boxplot_file))
tripletBPPPlots <- getBoxPlots(modelsData=tripletBPPData, ylab, boxplot_file)
saveBigPlot(tripletBPPPath, plot=do.call(grid.arrange, tripletBPPPlots))

tripletBPPPath <- file.path(results_dir, sprintf("%s-%s",tripletBPP, table_file))
saveTable(tripletBPPData, 3, tripletBPPPath, tripletBPP)
