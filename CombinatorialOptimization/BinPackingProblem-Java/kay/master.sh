#!/bin/bash

echo "Experiment (Master $MASTER_IPADDRESS:$MASTER_PORT) $ECJ_CONFIG_FILE $LOG_DIR"

java -cp $CLASSPATH ec.Evolve \
	-file $ECJ_CONFIG_FILE\
	-p seed.0=$SEED\
	-p checkpoint-directory=$LOG_DIR\
	-p stat.file=$LOG_DIR/master.stat\
	-p stat.child.0.file=$LOG_DIR/koza.stat\
	-p stat.child.1.file=$LOG_DIR/egc.stat\
	-p eval.masterproblem=ie.nix.ecj.PeerSimProblem\$MasterProblem\
	-p eval.masterproblem.max-jobs-per-slave=1\
	-p eval.master.host=$MASTER_IPADDRESS\
	-p eval.master.port=$MASTER_PORT\
	-p silent=false