#!/bin/bash

export EXP=exp4
export SEED=0
export PROJECT_ROOT=/Users/nicolamcdonnell/Desktop/Gaia/CombinatorialOptimization/BinPackingProblem
export GC_PROJECT_ROOT=/Users/nicolamcdonnell/Desktop/Gaia/GossipContracts
export CLASSPATH=$PROJECT_ROOT/bin/main:$PROJECT_ROOT/build/resources/main:$PROJECT_ROOT/libs/*:$GC_PROJECT_ROOT/bin/main:$GC_PROJECT_ROOT/build/resources/main
export LOG_DIR=$PROJECT_ROOT/results/$EXP/$SEED

export ECJ_CONFIG_FILE=$PROJECT_ROOT/src/main/resources/$EXP/EvolvedSmartBin.params
export PEERSIM_CONFIG_FILE=$PROJECT_ROOT/src/main/resources/ie/nix/bpp/egc/EvolvedSmartBin.properties

mkdir -p $LOG_DIR
cp $PROJECT_ROOT/src/main/resources/$EXP/* $LOG_DIR
cd $PROJECT_ROOT

echo "EXP=$EXP, SEED=$SEED"
echo "Experiment (Test) CLASSPATH $CLASSPATH"
echo "Experiment (Test) LOG_DIR $LOG_DIR"
echo "Experiment (Test) ECJ_CONFIG_FILE $ECJ_CONFIG_FILE"
echo "Experiment (Test) PEERSIM_CONFIG_FILE $PEERSIM_CONFIG_FILE"

java -cp $CLASSPATH ec.Evolve\
	-file $ECJ_CONFIG_FILE\
	-p seed.0=$SEED\
	-p checkpoint=false\
	-p stat.file=$LOG_DIR/master.stat\
	-p stat.child.0.file=$LOG_DIR/koza.stat\
	-p stat.child.1.file=$LOG_DIR/egc.stat\
	-p eval.problem.properties=$PEERSIM_CONFIG_FILE\
  -p generations=1\
	-p pop.subpop.0.size=1\
	-p gp.problem.number_of_runs=5\
	-p gp.problem.logging=console\
  -p silent=false

#	-p eval.problem.logging=console\