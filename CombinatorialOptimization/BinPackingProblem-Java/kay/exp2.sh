PROJECT_ROOT=/ichec/home/users/nix/gaia/CombinatorialOptimization/BinPackingProblem
export EXP=exp2
for seed in {0..19}
do
	export SEED=$seed
	sbatch --job-name=$EXP.$SEED --output=$EXP.$SEED.out $PROJECT_ROOT/kay/esb.sbatch
done
