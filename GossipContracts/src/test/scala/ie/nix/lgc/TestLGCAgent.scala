package ie.nix.lgc

import ie.nix.gc.TestGCAgent
import ie.nix.gc.TestGCAgent.{
  awardBehaviourSetState,
  awardedBehaviourSetState,
  getProposalBehaviourDefault,
  getTaskBehaviourDefault
}
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.lgc.TestLGCEnvironment.{DoState, DontState, HighState, LowState}
import ie.nix.peersim.Node
import ie.nix.rl.Environment.State
import org.apache.logging.log4j.scala.Logging

class TestLGCAgent(prefix: String)
    extends TestGCAgent(prefix)
    with LGCAgent[Int, Int]
    with MessageSorting[Int, Int]
    with Logging {

  override def mapToStateForShouldTender(node: Node): State =
    TestLGCAgent.mapToStateForShouldTender.apply(node)

  override def mapToStateForGetGuide(node: Node, task: Int): State =
    TestLGCAgent.mapToStateForGetGuide.apply(node, task)

  override def mapToStateForTenderToDouble(tenderMessage: TenderMessage[Int]): State =
    TestLGCAgent.mapToStateForTenderToDouble.apply(tenderMessage)

  override def mapToStateForShouldBid(tenderMessage: TenderMessage[Int]): State =
    TestLGCAgent.mapToStateForShouldBid.apply(tenderMessage)

  override def mapToStateForGetOffer(tenderMessage: TenderMessage[Int], proposal: Int): State =
    TestLGCAgent.mapToStateForGetOffer.apply(tenderMessage, proposal)

  override def mapToStateForBidToDouble(bidMessage: BidMessage[Int, Int]): State =
    TestLGCAgent.mapToStateForBidToDouble.apply(bidMessage)

  override def mapToStateForShouldAward(bidMessage: BidMessage[Int, Int]): State =
    TestLGCAgent.mapToStateForShouldAward.apply(bidMessage)

  override protected def defaultShouldTender(node: Node): Boolean =
    TestGCAgent.shouldTenderBehaviour.apply(node)

  override protected def defaultGetGuide(node: Node, task: Int): Double =
    TestGCAgent.getGuideBehaviour.apply(node, task)

  override protected def defaultSortTenderMessages(
      tenderMessages: Vector[TenderMessage[Int]]): Vector[(TenderMessage[Int], Double)] =
    TestGCAgent.sortTenderMessagesBehaviour.apply(tenderMessages)

  override protected def defaultShouldBid(tenderMessage: TenderMessage[Int]): Boolean =
    TestGCAgent.shouldBidBehaviour.apply(tenderMessage)

  override protected def defaultGetOffer(tenderMessage: TenderMessage[Int], proposal: Int): Double =
    TestGCAgent.getOfferBehaviour.apply(tenderMessage, proposal)

  override protected def defaultSortBidMessages(
      bidMessages: Vector[BidMessage[Int, Int]]): Vector[(BidMessage[Int, Int], Double)] =
    TestGCAgent.sortBidMessagesBehaviour.apply(bidMessages)

  override protected def defaultShouldAward(bidMessage: BidMessage[Int, Int]): Boolean =
    TestGCAgent.shouldAwardBehaviour.apply(bidMessage)

  override protected def defaultAward(awardMessage: AwardMessage[Int, Int]): Unit =
    TestGCAgent.awardBehaviour.apply(awardMessage)

  override protected def defaultAwarded(awardMessage: AwardMessage[Int, Int]): Unit =
    TestGCAgent.awardedBehaviour.apply(awardMessage)
}

object TestLGCAgent {

  val mapToStateForShouldTenderToEvenDo: Node => State = node => if (evenIndex(node)) DoState else DontState
  val mapToStateForGetGuideEvenHighOddLow: (Node, Int) => State = (node, _) =>
    if (evenIndex(node)) HighState else LowState
  val mapToStateForTenderToDoubleHighGuideHigh: (TenderMessage[_]) => State = tenderMessage =>
    if (tenderMessage.guide == 1) HighState else LowState
  val mapToStateForShouldBidToEvenDo: TenderMessage[_] => State = tenderMessage =>
    if (evenIndex(tenderMessage.to)) DoState else DontState
  val mapToStateForGetOfferEvenHighOddLow: (TenderMessage[_], Int) => State = (tenderMessage, _) =>
    if (evenIndex(tenderMessage.to)) HighState else LowState
  val mapToStateForBidToDoubleHighOfferHigh: (BidMessage[_, _]) => State = bidMessage =>
    if (bidMessage.offer == 1) HighState else LowState
  val mapToStateForShouldAwardToEvenDo: BidMessage[_, _] => State = bidMessage =>
    if (evenIndex(bidMessage.to)) DoState else DontState

  var mapToStateForShouldTender: Node => State = mapToStateForShouldTenderToEvenDo
  var mapToStateForGetGuide: (Node, Int) => State = mapToStateForGetGuideEvenHighOddLow
  var mapToStateForTenderToDouble: TenderMessage[_] => State = mapToStateForTenderToDoubleHighGuideHigh
  var mapToStateForShouldBid: TenderMessage[_] => State = mapToStateForShouldBidToEvenDo
  var mapToStateForGetOffer: (TenderMessage[_], Int) => State = mapToStateForGetOfferEvenHighOddLow
  var mapToStateForBidToDouble: BidMessage[_, _] => State = mapToStateForBidToDoubleHighOfferHigh
  var mapToStateForShouldAward: BidMessage[_, _] => State = mapToStateForShouldAwardToEvenDo

  TestGCAgent.getTaskBehaviour = getTaskBehaviourDefault
  TestGCAgent.getProposalBehaviour = getProposalBehaviourDefault
  TestGCAgent.awardBehaviour = awardBehaviourSetState
  TestGCAgent.awardedBehaviour = awardedBehaviourSetState

  def evenIndex(node: Node): Boolean = node.getIndex % 2 == 0

}
