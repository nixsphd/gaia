package ie.nix.lgc

import ie.nix.gc.TestGCAgent.{bidMessage, clearMessageStores, resetTestGCAgentBehaviours, tenderMessage}
import ie.nix.lgc.LGCAgentTest.properties
import ie.nix.lgc.LGCEnvironment._
import ie.nix.lgc.TestLGCAgent.evenIndex
import ie.nix.lgc.TestLGCEnvironment.{DoState, DontState, HighState, LowState}
import ie.nix.peersim.Utils._
import ie.nix.rl.Environment.{NullAction, State}
import ie.nix.rl.peersim.{ExposedQLearningAgent, TestNode}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.config.Configuration

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class LGCAgentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = {
    initConfiguration(properties)
    runSimulator()
  }

  override def beforeEach(): Unit = Random.setSeed(0)

  override def afterEach(): Unit = {
    clearMessageStores()
    resetTestGCAgentBehaviours()
  }

  behavior of "LGCAgent"

  it should "mapActionToBoolean True" in {
    val actualBoolean = testGCAgent0.actionToBoolean(returnTrue)
    assert(actualBoolean)
  }

  it should "mapActionToBoolean False" in {
    val actualBoolean = testGCAgent0.actionToBoolean(returnFalse)
    assert(!actualBoolean)
  }

  it should "mapActionToBoolean NullAction" in {
    val actualBoolean = testGCAgent0.actionToBoolean(NullAction)
    assert(!actualBoolean)
  }

  it should "shouldTender True" in {
    val shouldTenderTruePolicy =
      testRLAgent0.policy.updateAction(ShouldTenderState(DoState), returnTrue)
    testRLAgent0.updatePolicy(shouldTenderTruePolicy)
    val shouldTender = testGCAgent0.shouldTender(testNode0)
    assert(shouldTender)
  }

  it should "shouldTender False" in {
    val shouldTenderTruePolicy =
      testRLAgent1.policy.updateAction(ShouldTenderState(DoState), returnFalse)
    testRLAgent1.updatePolicy(shouldTenderTruePolicy)
    val shouldTender = testGCAgent1.shouldTender(testNode0)
    assert(!shouldTender)
  }

  it should "getGuide High" in {
    val shouldBidTruePolicy =
      testRLAgent0.policy.updateAction(GetGuideState(HighState), return0)
    testRLAgent0.updatePolicy(shouldBidTruePolicy)
    val getGuide = testGCAgent0.getGuide(testNode0, 0)
    assert(getGuide == 0)
  }

  it should "getGuide Low" in {
    val shouldBidTruePolicy =
      testRLAgent1.policy.updateAction(GetGuideState(LowState), return1)
    testRLAgent1.updatePolicy(shouldBidTruePolicy)
    val getGuide = testGCAgent1.getGuide(testNode1, 0)
    assert(getGuide == 1)
  }

  // TODO Fix ME SortTenderMessagesState
//  it should "sortTenderMessages High" in {
//    logger info s"testRLAgent0.softPolicy=${testRLAgent0.softPolicy}"
//    val map = Map[MapKey, Double]() +
//      (MapKey(HighState, 0d) -> 1d) +
//      (MapKey(HighState, 1d) -> 1d) +
//      (MapKey(LowState, 0d) -> 0d) +
//      (MapKey(LowState, 1d) -> 0d)
//    val tenderToDoubleHighHighPolicy =
//      testRLAgent0.softPolicy.updateAction(SortTenderMessagesState(HighState), MapAction(map))
//    testRLAgent0.updateSoftPolicy(tenderToDoubleHighHighPolicy)
//    val tenderMessage = testGCAgent0.TenderMessage(testNode0, testNode1, 300L, 0, 1, 1d)
//    val sortedTenderMessages = testGCAgent0.sortTenderMessages(Vector[TenderMessage[Int]](tenderMessage))
//    assert(sortedTenderMessages(0)._2 == 1)
//  }

//  it should "tenderToDouble Low" in {
//    val tenderToDoubleLowLowPolicy =
//      testRLAgent0.softPolicy.updateAction(TenderToDoubleState(LowState), return0)
//    testRLAgent0.updateSoftPolicy(tenderToDoubleLowLowPolicy)
//    val tenderMessage = testGCAgent0.TenderMessage(testNode0, testNode1, 300L, 0, 1, 0d)
//    val tenderToDouble = testGCAgent0.tenderToDouble(tenderMessage)
//    assert(tenderToDouble == 0)
//  }

  it should "shouldBid True" in {
    val shouldBidTruePolicy =
      testRLAgent1.policy.updateAction(ShouldBidState(DontState), returnTrue)
    testRLAgent1.updatePolicy(shouldBidTruePolicy)
    val shouldBid = testGCAgent1.shouldBid(tenderMessage)
    assert(shouldBid)
  }

  it should "shouldBid False" in {
    val shouldBidFalsePolicy =
      testRLAgent1.policy.updateAction(ShouldBidState(DontState), returnFalse)
    testRLAgent1.updatePolicy(shouldBidFalsePolicy)
    val shouldBid = testGCAgent1.shouldBid(tenderMessage)
    assert(!shouldBid)
  }

  it should "getOffer High" in {
    val getOfferHighPolicy =
      testRLAgent0.policy.updateAction(GetOfferState(HighState), return0)
    testRLAgent0.updatePolicy(getOfferHighPolicy)
    val tenderMessage = testGCAgent1.TenderMessage(testNode1, testNode0, 300L, 0, 1, 1d)
    val offer = testGCAgent0.getOffer(tenderMessage, 0)
    assert(offer == 0)
  }

  it should "getOffer Low" in {
    val getOfferLowPolicy =
      testRLAgent1.policy.updateAction(GetOfferState(LowState), return1)
    testRLAgent1.updatePolicy(getOfferLowPolicy)
    val tenderMessage = testGCAgent1.TenderMessage(testNode0, testNode1, 300L, 0, 1, 1d)
    val offer = testGCAgent1.getOffer(tenderMessage, 0)
    assert(offer == 1)
  }

  // TODO Fix ME SortBidMessagesState
//  it should "bidToDouble High" in {
//    val bidToDoubleHighHighPolicy =
//      testRLAgent1.softPolicy.updateAction(BidToDoubleState(HighState), return1)
//    testRLAgent1.updateSoftPolicy(bidToDoubleHighHighPolicy)
//    val bidMessage = testGCAgent1.BidMessage(tenderMessage, 1, 1d)
//    val bidToDouble = testGCAgent1.bidToDouble(bidMessage)
//    assert(bidToDouble == 1)
//  }

//  it should "bidToDouble Low" in {
//    val bidToDoubleLowLowPolicy =
//      testRLAgent1.softPolicy.updateAction(BidToDoubleState(LowState), return0)
//    testRLAgent1.updateSoftPolicy(bidToDoubleLowLowPolicy)
//    val bidMessage = testGCAgent1.BidMessage(tenderMessage, 1, 0d)
//    val bidToDouble = testGCAgent1.bidToDouble(bidMessage)
//    assert(bidToDouble == 0)
//  }

  it should "shouldAward True" in {
    val shouldBidTruePolicy =
      testRLAgent0.policy.updateAction(ShouldAwardState(DoState), returnTrue)
    testRLAgent0.updatePolicy(shouldBidTruePolicy)
    val shouldAward = testGCAgent0.shouldAward(bidMessage)
    assert(shouldAward == true)
  }

  it should "shouldAward False" in {
    val shouldBidFalsePolicy =
      testRLAgent0.policy.updateAction(ShouldAwardState(DoState), returnFalse)
    testRLAgent0.updatePolicy(shouldBidFalsePolicy)
    val shouldAward = testGCAgent0.shouldAward(bidMessage)
    assert(shouldAward == false)
  }

  def ShouldTenderState(state: State): LGCState = LGCState(ShouldTender, state)
  def GetGuideState(state: State): LGCState = LGCState(GetGuide, state)
  def SortTenderMessagesState(state: State): LGCState = LGCState(SortTenderMessages, state)
  def ShouldBidState(state: State): LGCState = LGCState(ShouldBid, state)
  def GetOfferState(state: State): LGCState = LGCState(GetOffer, state)
  def SortBidMessagesState(state: State): LGCState = LGCState(SortBidMessages, state)
  def ShouldAwardState(state: State): LGCState = LGCState(ShouldAward, state)

  def shouldTenderStateDo: LGCState = ShouldTenderState(DoState)
  def shouldTenderStateDont: LGCState = ShouldTenderState(DontState)
  def shouldBidStateDo: LGCState = ShouldBidState(DoState)
  def shouldBidStateDont: LGCState = ShouldBidState(DontState)
  def returnTrue: LGCEnvironment.LGCAction = LGCEnvironment.ReturnTrue
  def returnFalse: LGCEnvironment.LGCAction = LGCEnvironment.ReturnFalse
  def return0: LGCEnvironment.LGCAction = LGCEnvironment.ReturnDouble(0)
  def return1: LGCEnvironment.LGCAction = LGCEnvironment.ReturnDouble(1)

  def testNode0: TestNode = nodeAs[TestNode](index = 0)
  def testNode1: TestNode = nodeAs[TestNode](index = 1)

  def gcAgentProtocolID: Int = Configuration.lookupPid("gc_agent")
  def testGCAgents: Vector[TestLGCAgent] = protocolsAs[TestLGCAgent](gcAgentProtocolID)
  def testGCAgent0: TestLGCAgent = protocolAs[TestLGCAgent](testNode0, gcAgentProtocolID)
  def testGCAgent1: TestLGCAgent = protocolAs[TestLGCAgent](testNode1, gcAgentProtocolID)
  def testGCAgent0sNeighbour: TestLGCAgent = protocolAs[TestLGCAgent](testGCAgent0.neighbour(0), gcAgentProtocolID)

  def rlAgentProtocolID: Int = Configuration.lookupPid("rl_agent")
  def testRLAgents: Vector[ExposedQLearningAgent] = protocolsAs[ExposedQLearningAgent](rlAgentProtocolID)
  def evenIndexTestGCAgents: Vector[ExposedQLearningAgent] = testRLAgents.filter(rlAgent => evenIndex(rlAgent.node))
  def oddIndexTestGCAgents: Vector[ExposedQLearningAgent] = testRLAgents.filter(rlAgent => !evenIndex(rlAgent.node))
  def testRLAgent0: ExposedQLearningAgent = protocolAs[ExposedQLearningAgent](testNode0, rlAgentProtocolID)
  def testRLAgent1: ExposedQLearningAgent = protocolAs[ExposedQLearningAgent](testNode1, rlAgentProtocolID)

}

object LGCAgentTest {

  val properties = Array[String](
    "-file",
    "src/main/resources/ie/nix/gc/GCAgent.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgent.properties",
    "-file",
    "src/test/resources/ie/nix/gc/TestGCAgent.properties",
    "-file",
    "src/test/resources/ie/nix/lgc/TestLGCAgent.properties",
    "-file",
    "src/main/resources/ie/nix/rl/peersim/QLearning.properties",
    "protocol.rl_agent=ExposedQLearningAgent",
    "simulation.endtime=0",
    "log_dir=logs"
  )

}
