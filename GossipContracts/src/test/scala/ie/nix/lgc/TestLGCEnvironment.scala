package ie.nix.lgc

import ie.nix.lgc.LGCEnvironment._
import ie.nix.lgc.TestLGCEnvironment.{doOrDontStates, highOrLowStates}
import ie.nix.rl.Environment.{State, StateAction}
import ie.nix.rl.Rewards
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.Transitions.Transition
import ie.nix.rl.peersim.Environment.emptyStateSet
import org.apache.logging.log4j.scala.Logging

class TestLGCEnvironment(prefix: String)
    extends LGCEnvironment(
      LGCEnvironmentBuilder(prefix)
        .addShouldTenderStates(doOrDontStates)
        .addGetGuideStates(highOrLowStates)
        .addShouldBidStates(doOrDontStates)
        .addGetOfferStates(highOrLowStates)
        .addShouldAwardStates(doOrDontStates)
        .addAwardStates(emptyStateSet)
        .addAwardedStates(emptyStateSet)
    )
    with Rewards {

  override def toString: String = s"TestLGCEnvironment"

  override def getReward(transition: Transition): Reward =
    TestLGCEnvironment.getReward.apply(transition)

}

object TestLGCEnvironment extends Logging {

  object DoState extends State { override def toString: String = s"Do" }
  object DontState extends State { override def toString: String = s"Don't" }
  object HighState extends State { override def toString: String = s"High" }
  object LowState extends State { override def toString: String = s"Low" }

//  val getShouldTenderStates: GetStates = (_) => Set[State](DoState, DontState)
//  val getGetGuideStates: GetStates = (_) => Set[State](HighState, LowState)
//  val getSortTenderStates: GetStates = (_) => Set[State](HighState, LowState)
//  val getTenderStatesToMap: GetStates = (_) => Set[State](HighState, LowState)
//  val getShouldBidStates: GetStates = (_) => Set[State](DoState, DontState)
//  val getGetOfferStates: GetStates = (_) => Set[State](HighState, LowState)
//  val getSortBidStates: GetStates = (_) => Set[State](HighState, LowState)
//  val getBidStatesToMap: GetStates = (_) => Set[State](HighState, LowState)
//  val getShouldAwardStates: GetStates = (_) => Set[State](DoState, DontState)
//  val getAwardStates: GetStates = (_) => Set[State]()
//  val getAwardedStates: GetStates = (_) => Set[State]()

  def doOrDontStates: Set[State] = Set[State](DoState, DontState)
  def highOrLowStates: Set[State] = Set[State](HighState, LowState)

  val getRewardDosTrueDonstFalse: Transition => Reward = {
    case Transition(StateAction(LGCState(ShouldTender, DoState, _), ReturnTrue), _)    => 1d
    case Transition(StateAction(LGCState(ShouldTender, DontState, _), ReturnFalse), _) => 1d
    case Transition(StateAction(LGCState(ShouldBid, DoState, _), ReturnTrue), _)       => 1d
    case Transition(StateAction(LGCState(ShouldBid, DontState, _), ReturnFalse), _)    => 1d
    case Transition(StateAction(LGCState(ShouldAward, DoState, _), ReturnTrue), _)     => 1d
    case Transition(StateAction(LGCState(ShouldAward, DontState, _), ReturnFalse), _)  => 1d
    case _                                                                             => 0d
  }
  val getRewardShouldTenderDoTrueDontFalse: Transition => Reward = {
    case Transition(StateAction(LGCState(ShouldTender, DoState, _), ReturnTrue), _)    => 1d
    case Transition(StateAction(LGCState(ShouldTender, DontState, _), ReturnFalse), _) => 1d
    case _                                                                             => 0d
  }
  val getRewardGetGuideEvenHighOddLow: Transition => Reward = {
    case Transition(StateAction(LGCState(ShouldTender, _, _), ReturnTrue), _)           => 1d
    case Transition(StateAction(LGCState(GetGuide, HighState, _), ReturnDouble(1d)), _) => 1d
    case Transition(StateAction(LGCState(GetGuide, LowState, _), ReturnDouble(0d)), _)  => 1d
    case _                                                                              => 0d
  }
//  val getRewardTenderToDoubleHighGuideHigh: Transition => Reward = {
//    case Transition(StateAction(LGCState(ShouldTender, _), ReturnTrue), _)                 => 1d
//    case Transition(StateAction(LGCState(ShouldBid, _), ReturnTrue), _)                    => 1d
//    case Transition(StateAction(LGCState(TenderToDouble, HighState), ReturnDouble(1d)), _) => 1d
//    case Transition(StateAction(LGCState(TenderToDouble, LowState), ReturnDouble(0d)), _)  => 1d
//    case _                                                                                 => 0d
//  }
  val getRewardShouldBidDoTrueDontFalse: Transition => Reward = {
    case Transition(StateAction(LGCState(ShouldTender, _, _), ReturnTrue), _)       => 1d
    case Transition(StateAction(LGCState(ShouldBid, DoState, _), ReturnTrue), _)    => 1d
    case Transition(StateAction(LGCState(ShouldBid, DontState, _), ReturnFalse), _) => 1d
    case _                                                                          => 0d
  }
  val getRewardGetOfferEvenHighOddLow: Transition => Reward = {
    case Transition(StateAction(LGCState(ShouldTender, _, _), ReturnTrue), _)           => 1d
    case Transition(StateAction(LGCState(ShouldBid, _, _), ReturnTrue), _)              => 1d
    case Transition(StateAction(LGCState(GetOffer, HighState, _), ReturnDouble(1d)), _) => 1d
    case Transition(StateAction(LGCState(GetOffer, LowState, _), ReturnDouble(0d)), _)  => 1d
    case _                                                                              => 0d
  }
//  val getRewardBidToDoubleHighOfferHigh: Transition => Reward = {
//    case Transition(StateAction(LGCState(ShouldTender, _), ReturnTrue), _)              => 1d
//    case Transition(StateAction(LGCState(ShouldBid, _), ReturnTrue), _)                 => 1d
//    case Transition(StateAction(LGCState(BidToDouble, HighState), ReturnDouble(1d)), _) => 1d
//    case Transition(StateAction(LGCState(BidToDouble, LowState), ReturnDouble(0d)), _)  => 1d
//    case _                                                                              => 0d
//  }
  val getRewardShouldAwardDoTrueDontFalse: Transition => Reward = {
    case Transition(StateAction(LGCState(ShouldTender, _, _), ReturnTrue), _)         => 1d
    case Transition(StateAction(LGCState(ShouldBid, _, _), ReturnTrue), _)            => 1d
    case Transition(StateAction(LGCState(ShouldAward, DoState, _), ReturnTrue), _)    => 1d
    case Transition(StateAction(LGCState(ShouldAward, DontState, _), ReturnFalse), _) => 1d
    case _                                                                            => 0d
  }

  var getReward: Transition => Reward = getRewardDosTrueDonstFalse

}
