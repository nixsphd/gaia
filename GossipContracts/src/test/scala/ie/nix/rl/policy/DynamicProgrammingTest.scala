package ie.nix.rl.policy

import ie.nix.rl.peersim.TestEnvironment._
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.Transitions.Transition
import ie.nix.rl.peersim.TestEnvironmentWithTransitions
import ie.nix.rl.policy.DynamicProgramming.{EnvironmentModel, PolicyActionValueFunction}
import ie.nix.rl.policy.DynamicProgrammingTest.environmentWithTransitions
import ie.nix.rl.policy.vf.ActionValueFunction
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class DynamicProgrammingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  behavior of "DynamicProgramming"

  it should "backupStateAction final state" in {
    val actualBackupValue =
      dynamicProgramming.backupStateAction(environment, stateAction22, policy, actionValueFunction)
    val expectedBackupValue = 1d
    logger info s"actualBackupValue=$actualBackupValue, expectedBackupValue=$expectedBackupValue"

    assert(actualBackupValue === expectedBackupValue)
  }

  it should "backupStateAction non final state" in {
    val thePolicy = policy.updateAction(testState2, testAction2)
    val theActionValueFunction = actionValueFunction.updateValue(stateAction22, 1d)
    val actualBackupValue =
      dynamicProgramming.backupStateAction(environment, stateAction11, thePolicy, theActionValueFunction)
    val expectedBackupValue = discount * 1d
    logger info s"actualBackupValue=$actualBackupValue, expectedBackupValue=$expectedBackupValue"

    assert(actualBackupValue === expectedBackupValue)
  }

  it should "backupAllState" in {
    val backupActionValueFunction = dynamicProgramming.backupAllState(environment, policy, actionValueFunction)
    logger info s"backupActionValueFunction=$backupActionValueFunction"

    assert(backupActionValueFunction.getValue(stateAction11) == 0d)
    assert(backupActionValueFunction.getValue(stateAction12) == 0d)
    assert(backupActionValueFunction.getValue(stateAction22) == 1d)
    assert(backupActionValueFunction.getValue(stateAction31) == 1d)
  }

  it should "improvePolicy" in {
    val theActionValueFunction = actionValueFunction
      .updateValue(stateAction22, 1d)
      .updateValue(stateAction31, 1d)

    val improvedPolicy = dynamicProgramming.improvePolicy(policy, theActionValueFunction)
    logger info s"improvedPolicy=$improvedPolicy"

    assert(improvedPolicy.getAction(testState2) == testAction2)
    assert(improvedPolicy.getAction(testState3) == testAction1)
  }

  it should "sweep" in {
    val improvedPolicyActionValueFunction = dynamicProgramming.sweep(environment, policyActionValueFunction)
    logger info s"improvedPolicyActionValueFunction=$improvedPolicyActionValueFunction"

    assert(improvedPolicyActionValueFunction.actionValueFunction.getValue(stateAction22) == 1d)
    assert(improvedPolicyActionValueFunction.actionValueFunction.getValue(stateAction31) == 1d)
    assert(improvedPolicyActionValueFunction.policy.getAction(testState2) == testAction2)
    assert(improvedPolicyActionValueFunction.policy.getAction(testState3) == testAction1)
  }

  it should "evaluateEnvironment" in {
    val policyActionValueFunction = dynamicProgramming.evaluateEnvironment(environmentWithTransitions, 500)
    logger info s"policyActionValueFunction=$policyActionValueFunction"

    assert(policyActionValueFunction.actionValueFunction.getValue(stateAction11) == discount)
    assert(policyActionValueFunction.actionValueFunction.getValue(stateAction12) == discount)
    assert(policyActionValueFunction.actionValueFunction.getValue(stateAction21) == discount)
    assert(policyActionValueFunction.actionValueFunction.getValue(stateAction22) == 1d)
    assert(policyActionValueFunction.actionValueFunction.getValue(stateAction31) == 1d)
    assert(policyActionValueFunction.policy.getAction(testState2) == testAction2)
    assert(policyActionValueFunction.policy.getAction(testState3) == testAction1)
  }

  def testState1: TestState = TestState(1)
  def testState2: TestState = TestState(2)
  def testState3: TestState = TestState(3)
  def testAction1: TestAction = TestAction(1)
  def testAction2: TestAction = TestAction(2)
  def stateAction11: StateAction = StateAction(testState1, testAction1)
  def stateAction12: StateAction = StateAction(testState1, testAction2)
  def stateAction21: StateAction = StateAction(testState2, testAction1)
  def stateAction22: StateAction = StateAction(testState2, testAction2)
  def stateAction31: StateAction = StateAction(testState3, testAction1)
  def transition111: Transition = Transition(stateAction11, testState1)
  def transition112: Transition = Transition(stateAction11, testState2)

  def environment: TestEnvironmentWithTransitions = environmentWithTransitions
  def numberOfSweeps: Int = 10
  def discount: Double = 0.7
  def dynamicProgramming: ExposedDynamicProgramming =
    new ExposedDynamicProgramming(discount)
  def policy: DeterministicPolicy = DeterministicPolicy(environment)
  def actionValueFunction: ActionValueFunction = ActionValueFunction(policy, 0d)
  def policyActionValueFunction: PolicyActionValueFunction = PolicyActionValueFunction(policy, actionValueFunction)

}
object DynamicProgrammingTest {

  def environmentWithTransitions: TestEnvironmentWithTransitions =
    new TestEnvironmentWithTransitions(stateActions, terminalStates, transitionProbabilities)

}

class ExposedDynamicProgramming(discount: Double) extends DynamicProgramming(discount) with Logging {

  def this(dynamicProgramming: DynamicProgramming) =
    this(dynamicProgramming.discount)

  override def sweep(environment: EnvironmentModel,
                     policyActionValueFunction: PolicyActionValueFunction): PolicyActionValueFunction =
    super.sweep(environment, policyActionValueFunction)

  override def improvePolicy(policy: DeterministicPolicy,
                             actionValueFunction: ActionValueFunction): DeterministicPolicy =
    super.improvePolicy(policy, actionValueFunction)

  override def backupAllState(environment: EnvironmentModel,
                              policy: DeterministicPolicy,
                              actionValueFunction: ActionValueFunction): ActionValueFunction =
    super.backupAllState(environment, policy, actionValueFunction)

  override def backupStateAction(environment: EnvironmentModel,
                                 stateAction: StateAction,
                                 policy: DeterministicPolicy,
                                 actionValueFunction: ActionValueFunction): Double =
    super.backupStateAction(environment, stateAction, policy, actionValueFunction)
}
