package ie.nix.rl.policy.vf

import ie.nix.rl.peersim.EnvironmentTest.environment
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.PolicyTest.RandomPolicy
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class ConstantStepSizeTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "ConstantStepSize"

  it should "create ConstantStepSize for a policy" in {

    val policy = randomPolicy
    val stepSize = 0.1
    val constantStepSize = ConstantStepSize(policy, stepSize)
    logger.info(s"policy=$policy, constantStepSize=$constantStepSize")

    assert(constantStepSize.getStateActions.size === policy.getStateActions.size)
    assert(constantStepSize.getValue(StateAction(TestState(1), TestAction(1))) !== 0d)
    assert(constantStepSize.getValue(StateAction(TestState(2), TestAction(1))) !== 0d)
    assert(constantStepSize.getValue(StateAction(TestState(2), TestAction(2))) !== 0d)

  }

  it should "update Value" in {
    val stepSize = 0.1
    val updatedActionValueFunction = actionValueFunction
      .updateValue(stateAction, stepSize * 2d)
    logger.info(s"updatedActionValueFunction=$updatedActionValueFunction")

    assert(updatedActionValueFunction.getValue(stateAction) === 0.2)

  }

  it should "update Value twice" in {
    val stepSize = 0.1
    val firstError = stepSize * 2d
    // V(St) ← V(St) + α􏰖[target − V(St)􏰗] = 0.2 <- 0 + 0.1[2 - 0]
    val expectedFirstUpdateValue = 0.2
    val updatedActionValueFunction = actionValueFunction.updateValue(stateAction, firstError)
    val actualFirstUpdateValue = updatedActionValueFunction.getValue(stateAction)
    logger.info(s"expectedFirstUpdateValue=$expectedFirstUpdateValue, actualFirstUpdateValue=$actualFirstUpdateValue")

    assert(actualFirstUpdateValue === expectedFirstUpdateValue)

    val secondError = stepSize * (3d - expectedFirstUpdateValue)
    // V(St) ← V(St) + α􏰖[target − V(St)􏰗] = 0.48 <- 0.2 + 0.1[3 - 0.2]
    val expectedSecondUpdateValue = 0.48
    val secondUpdatedActionValueFunction = updatedActionValueFunction.updateValue(stateAction, secondError)
    val actualSecondUpdateValue = secondUpdatedActionValueFunction.getValue(stateAction)
    logger.info(s"expectedFirstUpdateValue=$expectedFirstUpdateValue, actualSecondUpdateValue=$actualSecondUpdateValue")

    assert(actualSecondUpdateValue === expectedSecondUpdateValue)

  }

  def actionValueFunction: ConstantStepSize = {
    val initialValue = 0d
    ConstantStepSize(randomPolicy, initialValue)
  }

  def stateAction: StateAction = StateAction(TestState(1), TestAction(1))
  def randomPolicy: Policy = RandomPolicy
}
