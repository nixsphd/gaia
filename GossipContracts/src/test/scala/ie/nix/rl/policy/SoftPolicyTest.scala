package ie.nix.rl.policy

import ie.nix.rl.peersim.EnvironmentTest.environment
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.Environment
import ie.nix.rl.policy.PolicyTest.RandomPolicy
import ie.nix.rl.policy.SoftPolicy.isEquivalent
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class SoftPolicyTest extends AnyFlatSpec with BeforeAndAfterEach {

  private implicit val testEnvironment: Environment = environment
  val probabilityOfRandomAction = 0.1

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "SoftPolicy"

  it should "create SoftPolicy with random action" in {

    val policy = softPolicy

    assert(isSoft(policy))

  }

  def softPolicy: SoftPolicy = {
    SoftPolicy(probabilityOfRandomAction)
  }

  def isSoft(policy: SoftPolicy): Boolean = {
    (for {
      state <- policy.getStates
      actionForState = policy.getAction(state)
      stateActions = policy.getStateActions(state)
      stateAction <- stateActions
      probability = policy.getProbability(stateAction)
    } yield {
      val probabilityOfOtherActions = probabilityOfRandomAction / stateActions.size
      if (stateAction.action === actionForState) {
        probability === probabilityOfOtherActions + 1 - probabilityOfRandomAction
      } else {
        probability === probabilityOfOtherActions
      }
    }).reduce(_ && _)
  }

  it should "create SoftPolicy from a Policy" in {

    val softPolicy = SoftPolicy(probabilityOfRandomAction, RandomPolicy)

    assert(isSoft(softPolicy))

  }

  it should "create SoftPolicy equivalent to a Policy" in {

    val policy = RandomPolicy
    val softPolicy = SoftPolicy(probabilityOfRandomAction, policy)

    assert(isEquivalent(softPolicy, policy))
  }

  it should "get Action For State" in {

    val policy = softPolicy.updateAction(TestState(2), TestAction(2))
    val action = policy.getAction(TestState(2))

    assert(action === TestAction(2))
  }

}
