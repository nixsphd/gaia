package ie.nix.rl.policy

import ie.nix.rl.peersim.EnvironmentTest.environment
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.PolicyTest.{EquiprobablePolicy, RandomPolicy}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class PolicyTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = Random.setSeed(1)

  def randomPolicy: Policy = RandomPolicy

  def equiprobablePolicy: Policy = EquiprobablePolicy

  behavior of "Policy"

  it should "create Policy with equiprobable values" in {

    val policy = equiprobablePolicy

    for {
      state <- policy.getStates
      stateActions = policy.getStateActions(state)
      equiprobability = 1d / stateActions.size
      stateAction <- stateActions
      probability = policy.getProbability(stateAction)
    } {
      assert(probability === equiprobability)
    }

  }

  it should "create Policy with random value" in {

    val policy = randomPolicy

    val probabilities = (for {
      stateAction <- policy.getStateActions
      probability = policy.getProbability(stateAction)
    } yield probability).toSet

    assert(probabilities.size === testEnvironment.getStateActions.size)

  }

  it should "get an ordered toString" in {

    val randomPolicyAsString = randomPolicy.toString
    logger.info(s"randomPolicyAsString=$randomPolicyAsString")

    assert(
      randomPolicyAsString ===
        "TestPolicy(" +
          "(StateAction(TestState(1),TestAction(1)),0.7308781907032909), " +
          "(StateAction(TestState(1),TestAction(2)),0.9677559094241207), " +
          "(StateAction(TestState(2),TestAction(1)),0.20771484130971707), " +
          "(StateAction(TestState(2),TestAction(2)),0.41008081149220166), " +
          "(StateAction(TestState(3),TestAction(1)),0.3327170559595112))")

  }

  it should "get the Action for a State with one possibility" in {

    val policyAction = equiprobablePolicy.getAction(TestState(3))
    logger.info(s"~policyAction=$policyAction")

    assert(policyAction === TestAction(1))

  }

  it should "get the Action for a State from two even choices" in {

    val policyAction = equiprobablePolicy.getAction(TestState(2))
    logger.info(s"~policyAction=$policyAction")

    assert(policyAction === TestAction(1) || policyAction === TestAction(2))

  }

  it should "get the Action for a State from two unbalanced choices" in {

    val policyAction = randomPolicy.getAction(TestState(2))
    logger.info(s"~policyAction=$policyAction")

    assert(policyAction === TestAction(2))

  }

  it should "get Most Probable Action For State" in {

    val policyAction = randomPolicy.getMostProbableAction(TestState(2))
    logger.info(s"~policyAction=$policyAction")

    assert(policyAction === TestAction(2))

  }

  it should "get Actions For State" in {

    val actions = randomPolicy.getActions(TestState(2))
    logger.info(s"~actions=$actions")

    assert(actions.size === 2)
    assert(actions.contains(TestAction(1)))
    assert(actions.contains(TestAction(2)))

  }

  it should "get Action And Probabilities For State" in {

    val actionsAndProbabilities = equiprobablePolicy.getActionAndProbabilities(TestState(2))
    logger.info(s"~actionsAndProbabilities=$actionsAndProbabilities")

    assert(actionsAndProbabilities.size === 2)
    assert(actionsAndProbabilities.contains((TestAction(1), 0.5)))
    assert(actionsAndProbabilities.contains((TestAction(2), 0.5)))

  }

}

object PolicyTest {

  class TestPolicy(val stateActionProbabilityMap: Map[StateAction, Double]) extends Policy {
    override def updateProbability(stateAction: StateAction, probability: Double): TestPolicy =
      new TestPolicy(stateActionProbabilityMap + (stateAction -> probability))
  }

  def EquiprobablePolicy(implicit environment: Environment): Policy =
    new TestPolicy(getEquiprobableStateActionProbabilitiesMap(environment))

  def getEquiprobableStateActionProbabilitiesMap(implicit environment: Environment): Map[StateAction, Double] =
    (for {
      state <- environment.getNonTerminalStates
      actions = environment.getActionsForState(state)
      action <- actions
    } yield {
      (StateAction(state, action), 1d / actions.size)
    }).toMap

  def RandomPolicy(implicit environment: Environment): Policy =
    new TestPolicy(getRandomStateActionProbabilityMap(environment))

  def getRandomStateActionProbabilityMap(implicit environment: Environment): Map[StateAction, Double] =
    environment.getStateActions
      .map(stateAction => (stateAction, Random.nextDouble()))
      .toMap

}
