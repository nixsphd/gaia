package ie.nix.rl.policy.vf

import ie.nix.rl.peersim.EnvironmentTest.environment
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.PolicyTest.RandomPolicy
import ie.nix.rl.policy.vf.EligibilityTracing.{EligibilityTracingValue, valueMap}
import ie.nix.rl.policy.vf.EligibilityTracingTest.ExposedEligibilityTracing
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class EligibilityTracingTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "EligibilityTracing"

  it should "create EligibilityTracing for a policy with random initial values" in {
    val policy = randomPolicy
    val discount = 0.1
    val stepSize = 0.1
    val eligibilityDecay = 0.9
    val eligibilityTracing = EligibilityTracing(policy, discount, stepSize, eligibilityDecay)
    logger.info(s"policy=$policy, eligibilityTracing=$eligibilityTracing")

    assert(eligibilityTracing.getStateActions.size === policy.getStateActions.size)
    assert(eligibilityTracing.getValue(StateAction(TestState(1), TestAction(1))) !== 0d)
    assert(eligibilityTracing.getValue(StateAction(TestState(2), TestAction(1))) !== 0d)
    assert(eligibilityTracing.getValue(StateAction(TestState(2), TestAction(2))) !== 0d)

  }

  it should "create EligibilityTracing for a policy with initial value 0.5" in {

    val policy = randomPolicy
    val discount = 0.1
    val eligibilityDecay = 0.9
    val initialValue = 0.5
    val eligibilityTracing = EligibilityTracing(policy, discount, eligibilityDecay, initialValue)
    logger.info(s"policy=$policy, eligibilityTracing=$eligibilityTracing")

    assert(eligibilityTracing.getStateActions.size === policy.getStateActions.size)
    assert(eligibilityTracing.getValue(StateAction(TestState(1), TestAction(1))) === initialValue)
    assert(eligibilityTracing.getValue(StateAction(TestState(2), TestAction(1))) === initialValue)
    assert(eligibilityTracing.getValue(StateAction(TestState(2), TestAction(2))) === initialValue)

  }

  it should "updateValueAndEligibilityTrace" in {
    val value = 1d
    val eligibilityTrace = 0.1
    val updatedEligibilityTracing =
      eligibilityTracing.updateValueAndEligibilityTrace(stateAction11, value, eligibilityTrace)

    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction11).value === value)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction11).eligibility === eligibilityTrace)
  }

  it should "clearEligibilityForAllStates" in {
    val value = 1d
    val eligibilityTrace = 0.1
    val updatedEligibilityTracing =
      eligibilityTracing
        .updateValueAndEligibilityTrace(stateAction11, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction21, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction22, value, eligibilityTrace)
        .clearEligibilityForAllStates()

    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction11).eligibility === 0d)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction21).eligibility === 0d)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction22).eligibility === 0d)
  }

  it should "decayEligibilityForAllStates with discount" in {
    val value = 1d
    val eligibilityTrace = 1d
    val updatedEligibilityTracing =
      eligibilityTracing
        .updateValueAndEligibilityTrace(stateAction11, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction21, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction22, value, eligibilityTrace)
        .decayEligibilityForAllStates()
    val decayedEligibility = discount * eligibilityTrace * eligibilityDecay

    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction11).eligibility === decayedEligibility +- 0.0001)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction21).eligibility === decayedEligibility +- 0.0001)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction22).eligibility === decayedEligibility +- 0.0001)
  }

  it should "decayEligibilityForAllStates with no discount" in {
    val value = 1d
    val eligibilityTrace = 1d
    val updatedEligibilityTracing =
      eligibilityTracingNoDiscount
        .updateValueAndEligibilityTrace(stateAction11, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction21, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction22, value, eligibilityTrace)
        .decayEligibilityForAllStates()
    val decayedEligibility = eligibilityTrace * eligibilityDecay

    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction11).eligibility === decayedEligibility +- 0.0001)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction21).eligibility === decayedEligibility +- 0.0001)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction22).eligibility === decayedEligibility +- 0.0001)
  }

  it should "updateErrorForAllStates" in {
    val value = 1d
    val eligibilityTrace = 1d
    val stepSize = 0.1
    val error = 0.2
    val updatedEligibilityTracing =
      eligibilityTracingNoDiscount
        .updateValueAndEligibilityTrace(stateAction11, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction21, value, 0.5 * eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction22, value, 0.25 * eligibilityTrace)
        .updateErrorForAllStates(stepSize * error)

    val updatedValue = stepSize * error * eligibilityTrace

    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction11).value === (value + updatedValue) +- 0.0001)
    assert(
      updatedEligibilityTracing.getValueAndEligibility(stateAction21).value === (value + 0.5 * updatedValue) +- 0.0001)
    assert(
      updatedEligibilityTracing.getValueAndEligibility(stateAction22).value === (value + 0.25 * updatedValue) +- 0.0001)
  }

  it should "incrementEligibilityForState" in {
    val value = 1d
    val eligibilityTrace = 0.5
    val updatedEligibilityTracing =
      eligibilityTracingNoDiscount
        .updateValueAndEligibilityTrace(stateAction11, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction21, value, eligibilityTrace)
        .updateValueAndEligibilityTrace(stateAction22, value, eligibilityTrace)
        .incrementEligibilityForState(stateAction11)

    assert(
      updatedEligibilityTracing.getValueAndEligibility(stateAction11).eligibility === (eligibilityTrace + 1) +- 0.0001)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction21).eligibility === eligibilityTrace +- 0.0001)
    assert(updatedEligibilityTracing.getValueAndEligibility(stateAction22).eligibility === eligibilityTrace +- 0.0001)
  }

  it should "updateValue" in {
    val value = 1d
    val eligibility = 1d
    val stepSize = 0.1
    val error = 0.2
    val updatedEligibilityTracing =
      eligibilityTracingNoDiscount
        .updateValueAndEligibilityTrace(stateAction11, value, eligibility)
        .updateValueAndEligibilityTrace(stateAction21, value, 0.5 * eligibility)
        .updateValueAndEligibilityTrace(stateAction22, value, 0.25 * eligibility)
        .updateValue(stateAction11, stepSize * error)

    assert(
      updatedEligibilityTracing
        .getValueAndEligibility(stateAction11)
        .value === (value + (stepSize * error * (eligibility + 1))) +- 0.0001)
    assert(
      updatedEligibilityTracing
        .getValueAndEligibility(stateAction11)
        .eligibility === (eligibility + 1) * eligibilityDecay +- 0.0001)
    assert(
      updatedEligibilityTracing
        .getValueAndEligibility(stateAction21)
        .value === (value + (stepSize * error * (0.5 * eligibility))) +- 0.0001)
    assert(
      updatedEligibilityTracing
        .getValueAndEligibility(stateAction21)
        .eligibility === (0.5 * eligibility * eligibilityDecay) +- 0.0001)
    assert(
      updatedEligibilityTracing
        .getValueAndEligibility(stateAction22)
        .value === (value + (stepSize * error * (0.25 * eligibility))) +- 0.0001)
    assert(
      updatedEligibilityTracing
        .getValueAndEligibility(stateAction22)
        .eligibility === (0.25 * eligibility * eligibilityDecay) +- 0.0001)
  }

  def eligibilityTracing: ExposedEligibilityTracing =
    new ExposedEligibilityTracing(valueMap(randomPolicy, initialValue), discount, eligibilityDecay)

  def eligibilityTracingNoDiscount: ExposedEligibilityTracing =
    new ExposedEligibilityTracing(valueMap(randomPolicy, initialValue), 1d, eligibilityDecay)

  def stateAction11: StateAction = StateAction(TestState(1), TestAction(1))
  def stateAction21: StateAction = StateAction(TestState(2), TestAction(1))
  def stateAction22: StateAction = StateAction(TestState(2), TestAction(2))

  def randomPolicy: Policy = RandomPolicy
  def discount = 0.1
  def eligibilityDecay = 0.9
  def initialValue = 0.5
}

object EligibilityTracingTest {

  class ExposedEligibilityTracing(valueMap: Map[StateAction, EligibilityTracingValue],
                                  discount: Double,
                                  eligibilityDecay: Double)
      extends EligibilityTracing(valueMap, discount, eligibilityDecay) {

    def this(eligibilityTracing: EligibilityTracing) =
      this(eligibilityTracing.valueMap, eligibilityTracing.discount, eligibilityTracing.eligibilityDecay)

    override def updateValue(stateAction: StateAction, error: Double): ExposedEligibilityTracing =
      new ExposedEligibilityTracing(super.updateValue(stateAction, error))

    override def getValueAndEligibility(stateAction: StateAction): EligibilityTracingValue =
      super.getValueAndEligibility(stateAction)

    override def updateValueAndEligibilityTrace(stateAction: StateAction,
                                                value: Double,
                                                eligibilityTrace: Double): ExposedEligibilityTracing =
      new ExposedEligibilityTracing(super.updateValueAndEligibilityTrace(stateAction, value, eligibilityTrace))

    override def clearEligibilityForAllStates(): ExposedEligibilityTracing =
      new ExposedEligibilityTracing(super.clearEligibilityForAllStates())

    override def decayEligibilityForAllStates(): ExposedEligibilityTracing =
      new ExposedEligibilityTracing(super.decayEligibilityForAllStates())

    override def updateErrorForAllStates(error: Double): ExposedEligibilityTracing =
      new ExposedEligibilityTracing(super.updateErrorForAllStates(error))

    override def incrementEligibilityForState(stateAction: StateAction): ExposedEligibilityTracing =
      new ExposedEligibilityTracing(super.incrementEligibilityForState(stateAction))

  }
}
