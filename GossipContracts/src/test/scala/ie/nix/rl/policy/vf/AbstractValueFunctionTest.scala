package ie.nix.rl.policy.vf

import ie.nix.rl.peersim.EnvironmentTest.environment
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.PolicyTest.{EquiprobablePolicy, RandomPolicy}
import ie.nix.rl.policy.vf.AbstractValueFunctionTest.TestActionValueFunction
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class AbstractValueFunctionTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  private implicit val testEnvironment: Environment = environment

  override def beforeEach(): Unit = {
    Random.setSeed(0)
  }

  behavior of "ActionValueFunction"

  it should "create ValueFunction for a policy" in {

    val policy = randomPolicy
    val actionValueFunction = TestActionValueFunction(policy)
    logger.info(s"policy=$policy, actionValueFunction=$actionValueFunction")

    assert(actionValueFunction.getStateActions.size === policy.getStateActions.size)
    assert(actionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) !== 0d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) !== 0d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) !== 0d)

  }

  it should "create ValueFunction for a policy with initial value" in {

    val policy = randomPolicy
    val actionValueFunction = TestActionValueFunction(policy, initialValue = 2d)
    logger.info(s"policy=$policy, actionValueFunction=$actionValueFunction")

    assert(actionValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 2d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(1))) === 2d)
    assert(actionValueFunction.getValue(StateAction(TestState(2), TestAction(2))) === 2d)

  }

  it should "get a Difference between two random functions" in {

    val policy = randomPolicy
    val anActionValueFunction = TestActionValueFunction(policy)
    val anotherActionValueFunction = TestActionValueFunction(policy)

    val theDifference = AbstractValueFunction.difference(anActionValueFunction, anotherActionValueFunction)
    logger.info(
      s"theDifference=$theDifference, anActionValueFunction=$anActionValueFunction, " +
        s"anotherActionValueFunction=$anotherActionValueFunction")

    assert(theDifference !== 0d)

  }

  it should "get no Difference between two identical functions" in {

    val policy = randomPolicy
    val anActionValueFunction = TestActionValueFunction(policy, initialValue = 2d)
    val anotherActionValueFunction = TestActionValueFunction(policy, initialValue = 2d)

    val theDifference = AbstractValueFunction.difference(anActionValueFunction, anotherActionValueFunction)
    logger.info(
      s"theDifference=$theDifference, anActionValueFunction=$anActionValueFunction, " +
        s"anotherActionValueFunction=$anotherActionValueFunction")

    assert(theDifference === 0d)

  }

  it should "get Value" in {

    val actionValueFunction = TestActionValueFunction(randomPolicy, initialValue = 2d)

    val value = actionValueFunction.getValue(StateAction(TestState(1), TestAction(1)))
    logger.info(s"value=$value")

    assert(value === 2d)

  }

  it should "update Value" in {

    val updatedValueFunction = actionValueFunction.updateValue(StateAction(TestState(1), TestAction(1)), 2d)
    logger.info(s"updatedValueFunction=$updatedValueFunction")

    assert(updatedValueFunction.getValue(StateAction(TestState(1), TestAction(1))) === 2d)

  }

  def randomPolicy: Policy = RandomPolicy
  def equiprobablePolicy: Policy = EquiprobablePolicy
  def actionValueFunction: AbstractValueFunction[Double] =
    TestActionValueFunction(randomPolicy, initialValue = 2d)

}

object AbstractValueFunctionTest {

  class TestAbstractValueFunction(val valueMap: Map[StateAction, Double]) extends AbstractValueFunction[Double] {

    override protected def mapValue(value: Double): Double = value

    override def updateValue(stateAction: StateAction, value: Double): AbstractValueFunction[Double] =
      new TestAbstractValueFunction(valueMap + (stateAction -> value))

  }

  def TestActionValueFunction(policy: Policy): TestAbstractValueFunction = {
    new TestAbstractValueFunction(getValueMap[Double](policy)(() => Random.nextDouble()))
  }

  def TestActionValueFunction(policy: Policy, initialValue: Double): TestAbstractValueFunction = {
    new TestAbstractValueFunction(getValueMap[Double](policy)(() => initialValue))
  }

  def getValueMap[V](policy: Policy)(initialValue: () => V): Map[StateAction, V] = {
    (for { stateAction <- policy.getStateActions } yield {
      (stateAction, initialValue())
    }).toMap
  }

}
