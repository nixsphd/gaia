package ie.nix.rl.peersim

import ie.nix.peersim.Node

import scala.reflect.{ClassTag, classTag}

class TestNode(prefix: String) extends Node(prefix) {

  var state: String = "Not Set"

  override def toString: String = s"TestNode[$getIndex, $state]"

}

object TestNode {

  implicit val testNodeClassTag: ClassTag[TestNode] = classTag[TestNode]

}
