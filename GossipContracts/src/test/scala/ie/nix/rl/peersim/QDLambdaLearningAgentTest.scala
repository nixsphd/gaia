package ie.nix.rl.peersim

import ie.nix.peersim.Node
import ie.nix.peersim.Utils.{node, protocolAs}
import ie.nix.rl.Environment.{NullAction, StateAction}
import ie.nix.rl.peersim.Agent.AgentPolicy
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.policy.vf.{AbstractValueFunction, EligibilityTracing}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.config.Configuration

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class QDLambdaLearningAgentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initPeerSim()
  override def beforeEach(): Unit = Random.setSeed(0)
  override def afterEach(): Unit = agentProtocol0.reset()

  behavior of "QDLambdaLearningAgent"

  it should "getConsensus" in {
    val previousStateActionValue = 0.5
    val neighboursPreviousStateActionValues = Vector[Double](0.4, 0.2)

    val actualConsensus = agentProtocol0.getConsensus(previousStateActionValue, neighboursPreviousStateActionValues)
    val expectedConsensus = previousStateActionValue - neighboursPreviousStateActionValues(0) +
      previousStateActionValue - neighboursPreviousStateActionValues(1)
    logger info s"actualConsensus=$actualConsensus, expectedConsensus=$expectedConsensus"

    assert(actualConsensus === expectedConsensus)
  }

  it should "getInnovation" in {
    val stateActionValue = 0.5
    val reward = 1d
    val nextStateActionValue = 0d

    val actualInnovation = agentProtocol0.getInnovation(stateActionValue, reward, nextStateActionValue)
    val expectedInnovation = reward + (discount * nextStateActionValue) - stateActionValue

    assert(actualInnovation === expectedInnovation)
  }

  it should "getError" in {
    val previousStateActionValue = 0.5
    val neighboursPreviousStateActionValues = Vector[Double](0.4, 0.2)
    val reward = 1d
    val maxNextStateActionValue = 0d
    val expectedError =
      (innovationStepSize * (reward + (discount * maxNextStateActionValue) - previousStateActionValue)) -
        (consensusStepSize * (previousStateActionValue - neighboursPreviousStateActionValues(0) +
          previousStateActionValue - neighboursPreviousStateActionValues(1)))
    val actualError = agentProtocol0.getError(previousStateActionValue,
                                              neighboursPreviousStateActionValues,
                                              reward,
                                              maxNextStateActionValue)
    logger info s"expectedError=$expectedError, actualError=$actualError"

    assert(expectedError === actualError +- 0.0001)
  }

  it should "updateActionValueFunction with no consensus" in {
    val neighboursPreviousStateActionValues = Vector[Double](0.5, 0.5)
    val updatedEligibilityTracing =
      agentProtocol0.updateActionValueFunction(agentProtocol0.eligibilityTracing,
                                               stateAction22,
                                               stateAction4N,
                                               neighboursPreviousStateActionValues)

    val actualStateAction22Value = updatedEligibilityTracing.getValue(stateAction22)
    val expectedStateAction22Value = initialValue + (initialValue * innovationStepSize)
    logger info s"${updatedEligibilityTracing}"
    assert(actualStateAction22Value === expectedStateAction22Value +- 0.00001)
  }

  it should "updateActionValueFunction with consensus" in {

    logger info s"${agentProtocol0.actionValueFunction}"
    val neighboursPreviousStateActionValues = Vector[Double](0.4, 0.2)
    val updatedEligibilityTracing =
      agentProtocol0.updateActionValueFunction(agentProtocol0.eligibilityTracing,
                                               stateAction22,
                                               stateAction4N,
                                               neighboursPreviousStateActionValues)

    val actualStateAction22Value = updatedEligibilityTracing.getValue(stateAction22)
    val expectedStateAction22Value = initialValue + (innovationStepSize * initialValue) - (consensusStepSize * 0.4)
    logger info s"${updatedEligibilityTracing}"
    logger info s"actualStateAction22Value=$actualStateAction22Value, expectedStateAction22Value=$expectedStateAction22Value"
    assert(actualStateAction22Value === expectedStateAction22Value +- 0.00001)
    assert(actualStateAction22Value === expectedStateAction22Value +- 0.00001)
  }

  it should "updateActionValueFunction twice" in {

    logger info s"actionValueFunction=${agentProtocol0.actionValueFunction}"
    val firstNeighboursPreviousStateActionValues = Vector[Double](0.2, 0.4)
    val firstError = (innovationStepSize * ((discount * initialValue) - initialValue)) -
      (consensusStepSize * (initialValue - firstNeighboursPreviousStateActionValues(0) +
        initialValue - firstNeighboursPreviousStateActionValues(1)))
    val firstStateAction11ExpectedValue = initialValue + firstError
    val firstStateAction22ExpectedValue = initialValue
    val firstStateAction11ExpectedEligibility = discount * eligibilityDecay
    val firstStateAction22ExpectedEligibility = 0d

    val updatedActionValueFunction =
      agentProtocol0.updateActionValueFunction(agentProtocol0.eligibilityTracing,
                                               stateAction11,
                                               stateAction22,
                                               firstNeighboursPreviousStateActionValues)
    logger info s"updatedActionValueFunction=${updatedActionValueFunction}"

    val firstStateAction11ActualValue = updatedActionValueFunction.getValue(stateAction11)
    val firstStateAction22ActualValue = updatedActionValueFunction.getValue(stateAction22)
    assert(firstStateAction11ActualValue === firstStateAction11ExpectedValue +- 0.0001)
    assert(firstStateAction22ActualValue === firstStateAction22ExpectedValue +- 0.0001)

    val firstStateAction11ActualEligibility = updatedActionValueFunction.valueMap(stateAction11).eligibility
    val firstStateAction22ActualEligibility = updatedActionValueFunction.valueMap(stateAction22).eligibility
    assert(firstStateAction11ActualEligibility === firstStateAction11ExpectedEligibility +- 0.0001)
    assert(firstStateAction22ActualEligibility === firstStateAction22ExpectedEligibility +- 0.0001)

    val secondNeighboursPreviousStateActionValues = Vector[Double](0.4, 0.3)
    val secondError =
      (innovationStepSize * (1d - initialValue)) -
        (consensusStepSize * (firstStateAction22ActualValue - secondNeighboursPreviousStateActionValues(0) +
          firstStateAction22ActualValue - secondNeighboursPreviousStateActionValues(1)))
    val secondStateAction11ExpectedValue = firstStateAction11ExpectedValue + (secondError * eligibilityDecay * discount)
    val secondStateAction22ExpectedValue = initialValue + secondError
    val secondStateAction11ExpectedEligibility = firstStateAction11ExpectedEligibility * discount * eligibilityDecay
    val secondStateAction22ExpectedEligibility = discount * eligibilityDecay

    val secondUpdatedActionValueFunction =
      agentProtocol0.updateActionValueFunction(updatedActionValueFunction,
                                               stateAction22,
                                               stateAction4N,
                                               secondNeighboursPreviousStateActionValues)
    logger info s"secondUpdatedActionValueFunction=${secondUpdatedActionValueFunction}"

    val secondStateAction11ActualValue = secondUpdatedActionValueFunction.getValue(stateAction11)
    val secondStateAction22ActualValue = secondUpdatedActionValueFunction.getValue(stateAction22)
    assert(secondStateAction11ActualValue === secondStateAction11ExpectedValue +- 0.0001)
    assert(secondStateAction22ActualValue === secondStateAction22ExpectedValue +- 0.0001)

    val secondStateAction11ActualEligibility = secondUpdatedActionValueFunction.valueMap(stateAction11).eligibility
    val secondStateAction22ActualEligibility = secondUpdatedActionValueFunction.valueMap(stateAction22).eligibility
    assert(secondStateAction11ActualEligibility === secondStateAction11ExpectedEligibility +- 0.0001)
    assert(secondStateAction22ActualEligibility === secondStateAction22ExpectedEligibility +- 0.0001)

  }

  it should "learn with no consensus" in {

    val neighboursPreviousStateActionValues = Vector[Double](0.5, 0.5)
    agentProtocol0.updatePolicy(agentProtocol0.policy.updateAction(state2, action1))
    logger info s"softPolicy=${agentProtocol0.policy}"
    agentProtocol0.learn(stateAction22, stateAction4N, neighboursPreviousStateActionValues)

    val actualValue = agentProtocol0.actionValueFunction.getValue(stateAction22)
    val expectedValue = initialValue + (innovationStepSize * (1d + (discount * 0d) - initialValue))
    logger info s"expectedValue=$expectedValue, actualValue=$actualValue"

    assert(actualValue === expectedValue +- 0.0001)

    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action2)
  }

  it should "learn with consensus" in {

    val neighboursPreviousStateActionValues = Vector[Double](0.4, 0.2)
    agentProtocol0.updatePolicy(agentProtocol0.policy.updateAction(state2, action1))
    logger info s"softPolicy=${agentProtocol0.policy}"
    agentProtocol0.learn(stateAction22, stateAction4N, neighboursPreviousStateActionValues)

    val actualValue = agentProtocol0.actionValueFunction.getValue(stateAction22)
    val expectedValue = initialValue +
      (innovationStepSize * (1d + (discount * 0d) - initialValue)) -
      (consensusStepSize * (initialValue - neighboursPreviousStateActionValues(0) +
        initialValue - neighboursPreviousStateActionValues(1)))

    logger info s"expectedValue=$expectedValue, actualValue=$actualValue"

    assert(actualValue === expectedValue +- 0.0001)

    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action2)
  }

  def state1: TestState = TestState(1)
  def state2: TestState = TestState(2)
  def state4: TestState = TestState(4)
  def action1: TestAction = TestAction(1)
  def action2: TestAction = TestAction(2)
  def stateAction11: StateAction = StateAction(state1, action1)
  def stateAction12: StateAction = StateAction(state1, action2)
  def stateAction21: StateAction = StateAction(state2, action1)
  def stateAction22: StateAction = StateAction(state2, action2)
  def stateAction4N: StateAction = StateAction(state4, NullAction)

  def agentProtocolID: Int = Configuration.lookupPid("rl_agent")
  def node0: Node = node(index = 0)
  def agentProtocol0: ExposedQDLambdaLearningAgent = protocolAs[ExposedQDLambdaLearningAgent](node0, agentProtocolID)

  def discount = 0.1
  def eligibilityDecay = 0.9
  def initialValue = 0.5
  def innovationStepSize = 0.2
  def consensusStepSize = 0.1

  private def initPeerSim(): Unit = {
    val testProperties = Array[String](
      "-file",
      "src/test/resources/ie/nix/rl/peersim/AgentTest.properties",
      "protocol.rl_agent=ie.nix.rl.peersim.ExposedQDLambdaLearningAgent",
      s"protocol.rl_agent.discount=$discount",
      s"protocol.rl_agent.eligibility_decay=$eligibilityDecay",
      s"protocol.rl_agent.initial_value=$initialValue",
      s"protocol.rl_agent.innovation_step_size=$innovationStepSize",
      s"protocol.rl_agent.consensus_step_size=$consensusStepSize"
    )
    peersim.Simulator.main(testProperties)
  }
}

class ExposedQDLambdaLearningAgent(prefix: String) extends QDLambdaLearningAgent(prefix) {

  override def learn(previousStateAction: StateAction,
                     nextStateAction: StateAction,
                     neighboursStateActionValues: Vector[Double]): Unit =
    super.learn(previousStateAction, nextStateAction, neighboursStateActionValues)

  override def policy: AgentPolicy = super.policy

  override def updatePolicy(policy: AgentPolicy): Unit =
    super.updatePolicy(policy)

  override def actionValueFunction: AbstractValueFunction[_] =
    super.actionValueFunction

  override def getError(previousStateActionValue: Double,
                        neighboursPreviousStateActionValues: scala.Vector[Double],
                        reward: Double,
                        maxNextStateActionValue: Double): Double =
    super.getError(previousStateActionValue, neighboursPreviousStateActionValues, reward, maxNextStateActionValue)

  override def updateActionValueFunction(eligibilityTracing: EligibilityTracing,
                                         previousStateAction: StateAction,
                                         nextStateAction: StateAction,
                                         neighboursPreviousStateActionValues: Vector[Double]): EligibilityTracing =
    super.updateActionValueFunction(eligibilityTracing,
                                    previousStateAction,
                                    nextStateAction,
                                    neighboursPreviousStateActionValues)

  override def getInnovation(stateActionValue: Double, reward: Double, nextStateActionValue: Double): Double =
    super.getInnovation(stateActionValue, reward, nextStateActionValue)

  override def getConsensus(previousStateActionValue: Double,
                            neighboursPreviousStateActionValues: Vector[Double]): Double =
    super.getConsensus(previousStateActionValue, neighboursPreviousStateActionValues)

  override def eligibilityTracing: EligibilityTracing = super.eligibilityTracing

}
