package ie.nix.rl.peersim

import ie.nix.peersim.Node
import ie.nix.peersim.Utils.{node, protocolAs}
import ie.nix.rl.Environment.{NullAction, StateAction}
import ie.nix.rl.peersim.Agent.AgentPolicy
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.policy.vf.{AbstractValueFunction, ConstantStepSize}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.config.Configuration

import scala.util.Random
@RunWith(classOf[JUnitRunner])
class QDLearningAgentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initPeerSim()
  override def beforeEach(): Unit = Random.setSeed(0)
  override def afterEach(): Unit = agentProtocol0.reset()

  behavior of "QDLearningAgent"

  it should "getConsensus" in {
    val previousStateActionValue = 0.5
    val neighboursPreviousStateActionValues = Vector[Double](0.4, 0.2)

    val actualConsensus = agentProtocol0.getConsensus(previousStateActionValue, neighboursPreviousStateActionValues)
    val expectedConsensus = previousStateActionValue - neighboursPreviousStateActionValues(0) +
      previousStateActionValue - neighboursPreviousStateActionValues(1)
    logger info s"actualConsensus=$actualConsensus, expectedConsensus=$expectedConsensus"

    assert(actualConsensus === expectedConsensus)
  }

  it should "getInnovation" in {
    val stateActionValue = 0.5
    val reward = 1d
    val nextStateActionValue = 0d

    val actualInnovation = agentProtocol0.getInnovation(stateActionValue, reward, nextStateActionValue)
    val expectedInnovation = reward + (discount * nextStateActionValue) - stateActionValue

    assert(actualInnovation === expectedInnovation)
  }

  it should "getError" in {
    val previousStateActionValue = 0.5
    val neighboursPreviousStateActionValues = Vector[Double](0.4, 0.2)
    val reward = 1d
    val maxNextStateActionValue = 0d
    val expectedError =
      (innovationStepSize * (reward + (discount * maxNextStateActionValue) - previousStateActionValue)) -
        (consensusStepSize * (previousStateActionValue - neighboursPreviousStateActionValues(0) +
          previousStateActionValue - neighboursPreviousStateActionValues(1)))
    val actualError = agentProtocol0.getError(previousStateActionValue,
                                              neighboursPreviousStateActionValues,
                                              reward,
                                              maxNextStateActionValue)
    logger info s"expectedError=$expectedError, actualError=$actualError"

    assert(expectedError === actualError +- 0.0001)
  }

  it should "updateActionValueFunction with no consensus" in {

    val neighboursPreviousStateActionValues = Vector[Double](0.5, 0.5)
    val updatedConstantStepSize =
      agentProtocol0.updateActionValueFunction(agentProtocol0.constantStepSize,
                                               stateAction22,
                                               stateAction4N,
                                               neighboursPreviousStateActionValues)

    val actualStateAction22Value = updatedConstantStepSize.getValue(stateAction22)
    val expectedStateAction22Value = initialValue + (initialValue * innovationStepSize)
    logger info s"actualStateAction22Value=$actualStateAction22Value, expectedStateAction22Value=$expectedStateAction22Value"
    logger info s"${updatedConstantStepSize}"
    assert(actualStateAction22Value === expectedStateAction22Value +- 0.00001)
  }

  it should "updateActionValueFunction with consensus" in {

    val neighboursPreviousStateActionValues = Vector[Double](0.4, 0.2)
    val updatedConstantStepSize =
      agentProtocol0.updateActionValueFunction(agentProtocol0.constantStepSize,
                                               stateAction22,
                                               stateAction4N,
                                               neighboursPreviousStateActionValues)

    val actualStateAction22Value = updatedConstantStepSize.getValue(stateAction22)
    val expectedStateAction22Value = initialValue + (innovationStepSize * initialValue) - (consensusStepSize * 0.4)
    logger info s"actualStateAction22Value=$actualStateAction22Value, expectedStateAction22Value=$expectedStateAction22Value"
    logger info s"${updatedConstantStepSize}"
    assert(actualStateAction22Value === expectedStateAction22Value +- 0.00001)
  }

  it should "learn with no consensus" in {

    val neighboursPreviousStateActionValues = Vector[Double](0.5, 0.5)
    agentProtocol0.updatePolicy(agentProtocol0.policy.updateAction(state2, action1))
    logger info s"softPolicy=${agentProtocol0.policy}"
    agentProtocol0.learn(stateAction22, stateAction4N, neighboursPreviousStateActionValues)

    val actualValue = agentProtocol0.actionValueFunction.getValue(stateAction22)
    val expectedValue = initialValue + (innovationStepSize * (1d + (discount * 0d) - initialValue))
    logger info s"expectedValue=$expectedValue, actualValue=$actualValue"

    assert(actualValue === expectedValue +- 0.0001)

    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action2)
  }

  it should "learn with consensus" in {

    val neighboursPreviousStateActionValues = Vector[Double](0.4, 0.2)
    agentProtocol0.updatePolicy(agentProtocol0.policy.updateAction(state2, action1))
    logger info s"softPolicy=${agentProtocol0.policy}"
    agentProtocol0.learn(stateAction22, stateAction4N, neighboursPreviousStateActionValues)

    val actualValue = agentProtocol0.actionValueFunction.getValue(stateAction22)
    val expectedValue = initialValue +
      (innovationStepSize * (1d + (discount * 0d) - initialValue)) -
      (consensusStepSize * (initialValue - neighboursPreviousStateActionValues(0) +
        initialValue - neighboursPreviousStateActionValues(1)))

    logger info s"expectedValue=$expectedValue, actualValue=$actualValue"

    assert(actualValue === expectedValue +- 0.0001)

    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action2)
  }

  def state1: TestState = TestState(1)
  def state2: TestState = TestState(2)
  def state4: TestState = TestState(4)
  def action1: TestAction = TestAction(1)
  def action2: TestAction = TestAction(2)
  def stateAction11: StateAction = StateAction(state1, action1)
  def stateAction12: StateAction = StateAction(state1, action2)
  def stateAction21: StateAction = StateAction(state2, action1)
  def stateAction22: StateAction = StateAction(state2, action2)
  def stateAction4N: StateAction = StateAction(state4, NullAction)

  def agentProtocolID: Int = Configuration.lookupPid("rl_agent")
  def node0: Node = node(index = 0)
  def agentProtocol0: ExposedQDLearningAgent = protocolAs[ExposedQDLearningAgent](node0, agentProtocolID)

  def discount = 0.9
  def initialValue = 0.5
  def innovationStepSize = 0.2
  def consensusStepSize = 0.1

  private def initPeerSim(): Unit = {
    val testProperties = Array[String](
      "-file",
      "src/test/resources/ie/nix/rl/peersim/AgentTest.properties",
      "protocol.rl_agent=ie.nix.rl.peersim.ExposedQDLearningAgent",
      s"protocol.rl_agent.discount=$discount",
      s"protocol.rl_agent.initial_value=$initialValue",
      s"protocol.rl_agent.innovation_step_size=$innovationStepSize",
      s"protocol.rl_agent.consensus_step_size=$consensusStepSize"
    )
    peersim.Simulator.main(testProperties)
  }

}

class ExposedQDLearningAgent(prefix: String) extends QDLearningAgent(prefix) {

  override def policy: AgentPolicy = super.policy

  override def updatePolicy(policy: AgentPolicy): Unit =
    super.updatePolicy(policy)

  override def initActionValueFunction(): AbstractValueFunction[_] =
    super.initActionValueFunction()

  override def learn(previousStateAction: StateAction,
                     nextStateAction: StateAction,
                     neighboursStateActionValues: scala.Vector[Double]): Unit =
    super.learn(previousStateAction, nextStateAction, neighboursStateActionValues)

  override def updateActionValueFunction(constantStepSize: ConstantStepSize,
                                         previousStateAction: StateAction,
                                         nextStateAction: StateAction,
                                         neighboursPreviousStateActionValues: scala.Vector[Double]): ConstantStepSize =
    super.updateActionValueFunction(constantStepSize,
                                    previousStateAction,
                                    nextStateAction,
                                    neighboursPreviousStateActionValues)
  override def getError(previousStateActionValue: Double,
                        neighboursPreviousStateActionValues: scala.Vector[Double],
                        reward: Double,
                        maxNextStateActionValue: Double): Double =
    super.getError(previousStateActionValue, neighboursPreviousStateActionValues, reward, maxNextStateActionValue)

  override def getInnovation(stateActionValue: Double, reward: Double, maxNextStateActionValue: Double): Double =
    super.getInnovation(stateActionValue, reward, maxNextStateActionValue)

  override def getConsensus(previousStateActionValue: Double,
                            neighboursPreviousStateActionValues: scala.Vector[Double]): Double =
    super.getConsensus(previousStateActionValue, neighboursPreviousStateActionValues)

  override def constantStepSize: ConstantStepSize =
    super.constantStepSize
}
