package ie.nix.rl.peersim

import ie.nix.peersim.Node
import ie.nix.peersim.Utils.{node, protocolAs}
import QLearningAgent.getConstantStepSize
import TestEnvironment.{TestAction, TestState}
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.vf.AbstractValueFunction
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.config.Configuration

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class NetworkedLearningAgentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initPeerSim()

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "NetworkedLearningAgentTest"

  it should "neighboursStateActionValues" in {
    agentProtocol1.updateActionValueFunction(agentProtocol1.actionValueFunction.updateValue(stateAction11, 1d))
    agentProtocol4.updateActionValueFunction(agentProtocol4.actionValueFunction.updateValue(stateAction11, 4d))
    logger info s"${agentProtocol1.actionValueFunction.getValue(stateAction11)}"
    val actualNeighboursStateActionValues = agentProtocol0.neighboursStateActionValues(stateAction11)
    val expectedNeighboursStateActionValues = Vector(0.5 + 1d, 0.5 + 4d)

    assert(actualNeighboursStateActionValues.toSet === expectedNeighboursStateActionValues.toSet)
  }

  def state1: TestState = TestState(1)
  def action1: TestAction = TestAction(1)
  def stateAction11: StateAction = StateAction(state1, action1)

  def agentProtocolID: Int = Configuration.lookupPid("rl_agent")
  def node0: Node = node(index = 0)
  def node1: Node = node(index = 1)
  def node4: Node = node(index = 4)
  def agentProtocol0: ExposedNetworkedLearningAgent = protocolAs[ExposedNetworkedLearningAgent](node0, agentProtocolID)
  def agentProtocol1: ExposedNetworkedLearningAgent = protocolAs[ExposedNetworkedLearningAgent](node1, agentProtocolID)
  def agentProtocol4: ExposedNetworkedLearningAgent = protocolAs[ExposedNetworkedLearningAgent](node4, agentProtocolID)

  def discount = 0.1
  def stepSize = 0.1
  def initialValue = 0.5

  private def initPeerSim(): Unit = {
    val testProperties = Array[String](
      "-file",
      "src/test/resources/ie/nix/rl/peersim/AgentTest.properties",
      "protocol.rl_agent=ie.nix.rl.peersim.ExposedNetworkedLearningAgent",
      s"protocol.rl_agent.discount=$discount",
      s"protocol.rl_agent.step_size=$stepSize",
      s"protocol.rl_agent.initial_value=$initialValue"
    )
    peersim.Simulator.main(testProperties)
  }

}

class ExposedNetworkedLearningAgent(prefix: String) extends NetworkedLearningAgent(prefix) {

  override def initActionValueFunction(): AbstractValueFunction[_] =
    getConstantStepSize(prefix, policy)

  override def learn(previousStateAction: StateAction, nextStateAction: StateAction): Unit =
    super.learn(previousStateAction, nextStateAction)

  override def learn(previousStateAction: StateAction,
                     nextStateAction: StateAction,
                     neighboursPreviousStateActionValues: Vector[Double]): Unit = {}

  override def neighboursStateActionValues(stateAction: StateAction): Vector[Double] =
    super.neighboursStateActionValues(stateAction)

  override def actionValueFunction: AbstractValueFunction[_] = super.actionValueFunction

  override def updateActionValueFunction(actionValueFunction: AbstractValueFunction[_]): Unit =
    super.updateActionValueFunction(actionValueFunction)

}
