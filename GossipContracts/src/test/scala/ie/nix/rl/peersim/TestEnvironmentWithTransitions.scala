package ie.nix.rl.peersim

import ie.nix.rl.Environment.{State, StateAction}
import ie.nix.rl.Transitions.Transition

class TestEnvironmentWithTransitions(stateActionsSet: Set[StateAction],
                                     terminalStateSet: Set[State],
                                     val _transitionProbabilities: Map[Transition, Double])
    extends TestEnvironment(stateActionsSet, terminalStateSet)
    with TransitionsMap {

  def this(prefix: String) =
    this(TestEnvironment.stateActions, TestEnvironment.terminalStates, TestEnvironment.transitionProbabilities)

}
