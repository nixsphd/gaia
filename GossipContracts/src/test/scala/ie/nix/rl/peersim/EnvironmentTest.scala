package ie.nix.rl.peersim

import EnvironmentTest.{environment, environmentWithTransitions}
import TestEnvironment._
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.Transitions.Transition
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterEach
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class EnvironmentTest extends AnyFlatSpec with BeforeAndAfterEach with Logging {

  override def beforeEach(): Unit =
    Random.setSeed(0)

  behavior of "Environment"

  it should "get Actions For State" in {

    val actions = environment.getActionsForState(TestState(2))

    assert(actions.size === 2)
    assert(actions.contains(TestAction(1)))
    assert(actions.contains(TestAction(2)))

  }

  it should "get StateActions" in {

    val stateActions = environment.getStateActions

    assert(stateActions.size === 5)
    assert(stateActions.contains(StateAction(TestState(1), TestAction(1))))
    assert(stateActions.contains(StateAction(TestState(1), TestAction(2))))
    assert(stateActions.contains(StateAction(TestState(2), TestAction(1))))
    assert(stateActions.contains(StateAction(TestState(2), TestAction(2))))
    assert(stateActions.contains(StateAction(TestState(3), TestAction(1))))

  }

  it should "get StateActions For State" in {

    val stateActions = environment.getStateActionsForState(TestState(2))

    assert(stateActions.size === 2)
    assert(stateActions.contains(StateAction(TestState(2), TestAction(1))))
    assert(stateActions.contains(StateAction(TestState(2), TestAction(2))))

  }

  it should "get States" in {

    val states = environment.getStates

    assert(states.size === 5)
    assert(states.contains(TestState(0)))
    assert(states.contains(TestState(1)))
    assert(states.contains(TestState(2)))
    assert(states.contains(TestState(3)))
    assert(states.contains(TestState(4)))
  }

  it should "get NonTerminal States" in {

    val nonTerminalStates = environment.getNonTerminalStates

    assert(nonTerminalStates.size === 3)
    assert(nonTerminalStates.contains(TestState(1)))
    assert(nonTerminalStates.contains(TestState(2)))
    assert(nonTerminalStates.contains(TestState(3)))
  }

  behavior of "Transitions"

  it should "transitionProbability when probability is 0" in {
    assert(environmentWithTransitions.transitionProbability(transition111) === 0d)
  }

  it should "transitionProbability when probability is 1" in {
    assert(environmentWithTransitions.transitionProbability(transition112) === 1d)
  }

  it should "nextState" in {
    assert(environmentWithTransitions.nextState(stateAction11) === testState2)
  }

  def testState1: TestState = TestState(1)
  def testState2: TestState = TestState(2)
  def testAction1: TestAction = TestAction(1)
  def testAction2: TestAction = TestAction(2)
  def stateAction11: StateAction = StateAction(testState1, testAction1)
  def stateAction12: StateAction = StateAction(testState1, testAction2)
  def transition111: Transition = Transition(stateAction11, testState1)
  def transition112: Transition = Transition(stateAction11, testState2)
}

object EnvironmentTest {

  def environment: TestEnvironment = new TestEnvironment(stateActions, terminalStates)

  def environmentWithTransitions: TestEnvironmentWithTransitions =
    new TestEnvironmentWithTransitions(stateActions, terminalStates, transitionProbabilities)

}
