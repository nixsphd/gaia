package ie.nix.rl.peersim

import ie.nix.peersim.Node
import ie.nix.peersim.Utils.{node, protocolAs}
import ie.nix.rl.Environment.{NullAction, StateAction}
import ie.nix.rl.peersim.Agent.AgentPolicy
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.policy.vf.{AbstractValueFunction, EligibilityTracing}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.config.Configuration

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class QLambdaLearningAgentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initPeerSim()

  override def beforeEach(): Unit = Random.setSeed(2)

  override def afterEach(): Unit = agentProtocol0.reset()

  behavior of "QLambdaLearningAgent"

  it should "getAction" in {
    agentProtocol0.updateStateAction(stateAction22)
    agentProtocol0.getAction(state4)
    agentProtocol0.updateStateAction(stateAction11)
    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action2)

  }

  it should "getError" in {
    val reward = 1d
    val stateActionValue = 0.25
    val nextStateActionValue = 0.5
    val expectedError = stepSize * (reward + (discount * nextStateActionValue) - stateActionValue)
    val actualError = agentProtocol0.getError(stateActionValue, reward, nextStateActionValue)
    logger info s"expectedError=$expectedError, actualError=$actualError"

    assert(expectedError === actualError +- 0.0001)
  }

  it should "updateActionValueFunction" in {

    val firstReward = 0d
    val firstError = firstReward + (discount * initialValue) - initialValue
    val firstStateAction11ExpectedValue = initialValue + (stepSize * firstError * 1d)
    val firstStateAction22ExpectedValue = initialValue
    val firstStateAction11ExpectedEligibility = discount * eligibilityDecay
    val firstStateAction22ExpectedEligibility = 0d

    val updatedActionValueFunction =
      agentProtocol0.updateActionValueFunction(agentProtocol0.eligibilityTracing, stateAction11, stateAction22)
    val firstStateAction11ActualValue = updatedActionValueFunction.getValue(stateAction11)
    val firstStateAction22ActualValue = updatedActionValueFunction.getValue(stateAction22)
    logger info s"firstStateAction11ExpectedValue=$firstStateAction11ExpectedValue, " +
      s"firstStateAction11ActualValue=$firstStateAction11ActualValue"
    logger info s"firstStateAction22ExpectedValue=$firstStateAction22ExpectedValue, " +
      s"firstStateAction22ActualValue=$firstStateAction22ActualValue"
    assert(firstStateAction11ActualValue === firstStateAction11ExpectedValue +- 0.0001)
    assert(firstStateAction22ActualValue === firstStateAction22ExpectedValue +- 0.0001)

    val firstStateAction11ActualEligibility = updatedActionValueFunction.valueMap(stateAction11).eligibility
    val firstStateAction22ActualEligibility = updatedActionValueFunction.valueMap(stateAction22).eligibility
    logger info s"firstStateAction11ActualEligibility=$firstStateAction11ActualEligibility, " +
      s"firstStateAction11ExpectedEligibility=$firstStateAction11ExpectedEligibility"
    logger info s"firstStateAction22ActualEligibility=$firstStateAction22ActualEligibility, " +
      s"firstStateAction22ExpectedEligibility=$firstStateAction22ExpectedEligibility"
    assert(firstStateAction11ActualEligibility === firstStateAction11ExpectedEligibility +- 0.0001)
    assert(firstStateAction22ActualEligibility === firstStateAction22ExpectedEligibility +- 0.0001)

    val secondReward = 1d
    val secondError = secondReward + (discount * updatedActionValueFunction.getMaxActionValueForState(state4)) - firstStateAction22ExpectedValue
    val secondStateAction11ExpectedValue = firstStateAction11ExpectedValue + (stepSize * secondError * firstStateAction11ActualEligibility)
    val secondStateAction22ExpectedValue = initialValue + (stepSize * secondError * 1d)
    val secondStateAction11ExpectedEligibility = firstStateAction11ExpectedEligibility * discount * eligibilityDecay
    val secondStateAction22ExpectedEligibility = discount * eligibilityDecay

    val secondUpdatedActionValueFunction =
      agentProtocol0.updateActionValueFunction(updatedActionValueFunction, stateAction22, stateAction4N)
    val secondStateAction11ActualValue = secondUpdatedActionValueFunction.getValue(stateAction11)
    val secondStateAction22ActualValue = secondUpdatedActionValueFunction.getValue(stateAction22)
    logger info s"secondStateAction11ExpectedValue=$secondStateAction11ExpectedValue, " +
      s"secondStateAction11ActualValue=$secondStateAction11ActualValue"
    logger info s"secondStateAction22ExpectedValue=$secondStateAction22ExpectedValue, " +
      s"secondStateAction22ActualValue=$secondStateAction22ActualValue"
    assert(secondStateAction11ActualValue === secondStateAction11ExpectedValue +- 0.0001)
    assert(secondStateAction22ActualValue === secondStateAction22ExpectedValue +- 0.0001)

    val secondStateAction11ActualEligibility = secondUpdatedActionValueFunction.valueMap(stateAction11).eligibility
    val secondStateAction22ActualEligibility = secondUpdatedActionValueFunction.valueMap(stateAction22).eligibility
    logger info s"secondStateAction11ActualEligibility=$secondStateAction11ActualEligibility, " +
      s"secondStateAction11ExpectedEligibility=$secondStateAction11ExpectedEligibility"
    logger info s"secondStateAction22ActualEligibility=$secondStateAction22ActualEligibility, " +
      s"secondStateAction22ExpectedEligibility=$secondStateAction22ExpectedEligibility"
    assert(secondStateAction11ActualEligibility === secondStateAction11ExpectedEligibility +- 0.0001)
    assert(secondStateAction22ActualEligibility === secondStateAction22ExpectedEligibility +- 0.0001)

  }

  it should "learn" in {
    agentProtocol0.updatePolicy(agentProtocol0.policy.updateAction(state2, action1))
    logger info s"softPolicy=${agentProtocol0.policy}"
    agentProtocol0.learn(stateAction22, stateAction4N)

    val actualValue = agentProtocol0.actionValueFunction.getValue(stateAction22)
    val expectedValue = initialValue + (stepSize * (1d + (discount * 0d) - initialValue))
    logger info s"expectedValue=$expectedValue, actualValue=$actualValue"

    assert(actualValue === expectedValue +- 0.0001)

    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action2)
  }

  def state1: TestState = TestState(1)
  def state2: TestState = TestState(2)
  def state4: TestState = TestState(4)
  def action1: TestAction = TestAction(1)
  def action2: TestAction = TestAction(2)
  def stateAction11: StateAction = StateAction(state1, action1)
  def stateAction12: StateAction = StateAction(state1, action2)
  def stateAction21: StateAction = StateAction(state2, action1)
  def stateAction22: StateAction = StateAction(state2, action2)
  def stateAction4N: StateAction = StateAction(state4, NullAction)

  def agentProtocolID: Int = Configuration.lookupPid("rl_agent")
  def node0: Node = node(index = 0)
  def agentProtocol0: ExposedQLambdaLearningAgent = protocolAs[ExposedQLambdaLearningAgent](node0, agentProtocolID)

  def discount = 0.9
  def stepSize = 0.1
  def eligibilityDecay = 0.8
  def initialValue = 0.5

  private def initPeerSim(): Unit = {
    val testProperties = Array[String](
      "-file",
      "src/test/resources/ie/nix/rl/peersim/AgentTest.properties",
      "protocol.rl_agent=ie.nix.rl.peersim.ExposedQLambdaLearningAgent",
      s"protocol.rl_agent.discount=$discount",
      s"protocol.rl_agent.step_size=$stepSize",
      s"protocol.rl_agent.eligibility_decay=$eligibilityDecay",
      s"protocol.rl_agent.initial_value=$initialValue"
    )
    peersim.Simulator.main(testProperties)
  }

}

class ExposedQLambdaLearningAgent(prefix: String) extends QLambdaLearningAgent(prefix) {

  override def initActionValueFunction(): AbstractValueFunction[_] = super.initActionValueFunction()

  override def policy: AgentPolicy = super.policy

  override def updatePolicy(policy: AgentPolicy): Unit =
    super.updatePolicy(policy)

  override def actionValueFunction: AbstractValueFunction[_] =
    super.actionValueFunction

  override def eligibilityTracing: EligibilityTracing = super.eligibilityTracing

  override def updateStateAction(stateAction: StateAction): Unit =
    super.updateStateAction(stateAction)

  override def updateActionValueFunction(actionValueFunction: AbstractValueFunction[_]): Unit =
    super.updateActionValueFunction(actionValueFunction: AbstractValueFunction[_])

  override def getError(stateActionValue: Double, reward: Double, nextStateActionValue: Double): Double =
    super.getError(stateActionValue, reward, nextStateActionValue)

  override def updateActionValueFunction(eligibilityTracing: EligibilityTracing,
                                         previousStateAction: StateAction,
                                         nextStateAction: StateAction): EligibilityTracing =
    super.updateActionValueFunction(eligibilityTracing, previousStateAction, nextStateAction)

  override def learn(previousStateAction: StateAction, nextStateAction: StateAction): Unit =
    super.learn(previousStateAction, nextStateAction)

}
