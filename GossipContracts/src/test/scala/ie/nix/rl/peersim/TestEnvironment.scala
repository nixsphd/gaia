package ie.nix.rl.peersim

import TestEnvironment.{TestState, stateActions, terminalStates}
import ie.nix.rl.Environment._
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.Transitions.Transition
import org.scalactic.TypeCheckedTripleEquals._

class TestEnvironment(stateActionsSet: Set[StateAction], terminalStateSet: Set[State])
    extends Environment(stateActionsSet, terminalStateSet)
    with RewardsMap {

  def this(prefix: String) = this(stateActions, terminalStates)

  override protected val stateRewards: Map[State, Reward] =
    Map[State, Reward]() + (TestState(4) -> 1d)
}

object TestEnvironment {

  val TARGET_ID = 4

  case class TestState(id: Int) extends State {
    override def toString: String = s"TestState($id)"
    override def isTerminal: Boolean = id === 0 || id >= TARGET_ID
  }

  case class TestAction(id: Int) extends Action {
    override def toString: String = s"TestAction($id)"
  }

  def terminalStates: Set[State] = Set[State](TestState(0), TestState(4))

  def stateActions: Set[StateAction] = Set[StateAction](
    StateAction(TestState(1), TestAction(1)),
    StateAction(TestState(1), TestAction(2)),
    StateAction(TestState(2), TestAction(1)),
    StateAction(TestState(2), TestAction(2)),
    StateAction(TestState(3), TestAction(1)),
  )

  def transitionProbabilities: Map[Transition, Double] =
    Map[Transition, Double](
      (Transition(StateAction(TestState(1), TestAction(1)), TestState(2)) -> 1d),
      (Transition(StateAction(TestState(1), TestAction(2)), TestState(3)) -> 1d),
      (Transition(StateAction(TestState(2), TestAction(1)), TestState(3)) -> 1d),
      (Transition(StateAction(TestState(2), TestAction(2)), TestState(4)) -> 1d),
      (Transition(StateAction(TestState(3), TestAction(1)), TestState(4)) -> 1d)
    )

}
