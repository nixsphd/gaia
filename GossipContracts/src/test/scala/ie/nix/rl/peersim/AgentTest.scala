package ie.nix.rl.peersim

import ie.nix.peersim.Node
import ie.nix.peersim.Utils.{node, protocolAs}
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.Policy.Determinism
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.config.Configuration

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class AgentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initPeerSim()

  override def beforeEach(): Unit = Random.setSeed(2)

  behavior of "Agent"

  it should "getAction" in {
    agentProtocol0.setSoftPolicy(agentProtocol0.policy.updateAction(state2, action1))
    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action1)
  }

  def state1: TestState = TestState(1)
  def state2: TestState = TestState(2)
  def action1: TestAction = TestAction(1)
  def action2: TestAction = TestAction(2)

  def testProtocolID = Configuration.lookupPid("rl_agent")
  def node0: Node = node(index = 0)
  def agentProtocol0: ExposedAgent = protocolAs[ExposedAgent](node0, testProtocolID)

  private def initPeerSim(): Unit = {
    val testProperties =
      Array[String]("-file",
                    "src/test/resources/ie/nix/rl/peersim/AgentTest.properties",
                    "protocol.rl_agent=ie.nix.rl.peersim.ExposedAgent")
    peersim.Simulator.main(testProperties)
  }

}

class ExposedAgent(prefix: String) extends Agent(prefix) {

  override def policy: Policy with Determinism = super.policy

  def setSoftPolicy(softPolicy: Policy with Determinism): Unit =
    updatePolicy(softPolicy: Policy with Determinism)

}
