package ie.nix.rl.peersim

import ie.nix.peersim.Node
import ie.nix.peersim.Utils.{node, protocolAs}
import ie.nix.rl.Environment.{NullAction, StateAction}
import ie.nix.rl.peersim.Agent.AgentPolicy
import ie.nix.rl.peersim.TestEnvironment.{TestAction, TestState}
import ie.nix.rl.policy.vf.{AbstractValueFunction, ConstantStepSize}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.config.Configuration

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class QLearningAgentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initPeerSim()

  override def beforeEach(): Unit = Random.setSeed(0)

  override def afterEach(): Unit = agentProtocol0.reset()

  behavior of "QLearningAgent"

  it should "getAction" in {
    agentProtocol0.updateStateAction(stateAction22)
    agentProtocol0.getAction(state4)
    agentProtocol0.updateStateAction(stateAction11)
    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action2)
  }

  it should "getError" in {
    val previousStateActionValue = 0.5
    val reward = 1d
    val maxNextStateActionValue = 0.5
    val expectedError = stepSize * (reward + (discount * maxNextStateActionValue) - previousStateActionValue)
    val actualError = agentProtocol0.getError(previousStateActionValue, reward, maxNextStateActionValue)
    logger info s"expectedError=$expectedError, actualError=$actualError"

    assert(expectedError === actualError +- 0.0001)
  }

  it should "updateActionValueFunction" in {
    val updatedActionValueFunction =
      agentProtocol0.updateActionValueFunction(agentProtocol0.constantStepSize, stateAction22, stateAction4N)
    val actualValue = updatedActionValueFunction.getValue(stateAction22)
    val expectedValue = initialValue + (stepSize * (1d + (discount * 0d) - initialValue))
    logger info s"expectedValue=$expectedValue, actualValue=$actualValue"

    assert(actualValue === expectedValue +- 0.0001)
  }

  it should "learn" in {
    agentProtocol0.updatePolicy(agentProtocol0.policy.updateAction(state2, action1))
    logger info s"softPolicy=${agentProtocol0.policy}"
    agentProtocol0.learn(stateAction22, stateAction4N)
    val actualValue = agentProtocol0.actionValueFunction.getValue(stateAction22)
    val expectedValue = initialValue + (stepSize * (1d + (discount * 0d) - initialValue))
    logger info s"expectedValue=$expectedValue, actualValue=$actualValue"

    assert(actualValue === expectedValue +- 0.0001)

    val actualAction = agentProtocol0.getAction(state2)
    logger info s"actualAction=$actualAction"

    assert(actualAction === action2)
  }

  def state1: TestState = TestState(1)
  def state2: TestState = TestState(2)
  def state4: TestState = TestState(4)
  def action1: TestAction = TestAction(1)
  def action2: TestAction = TestAction(2)
  def stateAction11: StateAction = StateAction(state1, action1)
  def stateAction12: StateAction = StateAction(state1, action2)
  def stateAction21: StateAction = StateAction(state2, action1)
  def stateAction22: StateAction = StateAction(state2, action2)
  def stateAction4N: StateAction = StateAction(state4, NullAction)

  def agentProtocolID: Int = Configuration.lookupPid("rl_agent")
  def node0: Node = node(index = 0)
  def agentProtocol0: ExposedQLearningAgent = protocolAs[ExposedQLearningAgent](node0, agentProtocolID)

  def discount = 0.9
  def stepSize = 0.1
  def initialValue = 0.5

  private def initPeerSim(): Unit = {
    val testProperties = Array[String](
      "-file",
      "src/test/resources/ie/nix/rl/peersim/AgentTest.properties",
      "protocol.rl_agent=ie.nix.rl.peersim.ExposedQLearningAgent",
      s"protocol.rl_agent.discount=$discount",
      s"protocol.rl_agent.step_size=$stepSize",
      s"protocol.rl_agent.initial_value=$initialValue"
    )
    peersim.Simulator.main(testProperties)
  }

}

class ExposedQLearningAgent(prefix: String) extends QLearningAgent(prefix) {

  override def policy: AgentPolicy = super.policy

  override def updatePolicy(policy: AgentPolicy): Unit =
    super.updatePolicy(policy)

  override def actionValueFunction: AbstractValueFunction[_] =
    super.actionValueFunction

  override def constantStepSize: ConstantStepSize = super.constantStepSize

  override def updateStateAction(stateAction: StateAction): Unit =
    super.updateStateAction(stateAction)

  override def updateActionValueFunction(actionValueFunction: AbstractValueFunction[_]): Unit =
    super.updateActionValueFunction(actionValueFunction: AbstractValueFunction[_])

  override def getError(previousStateAction: Double, reward: Double, maxNextStateActionValue: Double): Double =
    super.getError(previousStateAction, reward, maxNextStateActionValue)

  override def updateActionValueFunction(constantStepSize: ConstantStepSize,
                                         previousStateAction: StateAction,
                                         nextStateAction: StateAction): ConstantStepSize =
    super.updateActionValueFunction(constantStepSize, previousStateAction, nextStateAction)

  override def learn(previousStateAction: StateAction, nextStateAction: StateAction): Unit =
    super.learn(previousStateAction, nextStateAction)

}
