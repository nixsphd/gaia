package ie.nix.examples.rl

import ie.nix.gc.TestGCAgent.resetTestGCAgentBehaviours
import ie.nix.peersim.Utils.{isConfigLoaded, node, protocolAs}
import ie.nix.rl.peersim.Agent
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalactic.Tolerance.convertNumericToPlusOrMinusWrapper
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner
import peersim.config.Configuration

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class EnvironmentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initPeerSim()

  override def beforeEach(): Unit = Random.setSeed(0)

  override def afterEach(): Unit = resetTestGCAgentBehaviours()

  behavior of "ExperimentEnvironment"

  it should "environment stateAction transition probabilities sum top 1" in {
    val environment = experimentEnvironment
    for (stateAction <- environment.getStateActions;
         transitionProbabilities = environment.transitionProbabilities(stateAction);
         sumOfProbabilities = transitionProbabilities.map(_._2).sum) {
      logger info s"stateAction=$stateAction, sumOfProbabilities=$sumOfProbabilities"
      assert(sumOfProbabilities === 1d +- 0.0001)
    }

  }

  def agentProtocolID: Int = Configuration.lookupPid(s"rl_agent")
  def node0 = node(0)
  def agent0 = protocolAs[Agent](node0, agentProtocolID)
  def experimentEnvironment = agent0.environmentAs[Environment]

  def initPeerSim(): Unit =
    if (!isConfigLoaded)
      peersim.Simulator.main(
        Array[String](
          "-file",
          "src/main/resources/ie/nix/examples/rl/Example_s_2.properties",
          "-file",
          "src/main/resources/ie/nix/examples/rl/Example.properties",
          "-file",
          "src/main/resources/ie/nix/rl/peersim/QLearning.properties",
          "protocol.rl_agent.step_size=0.1",
          "simulation.endtime=0",
          "log_dir=logs"
        ))
}
