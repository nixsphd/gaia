package ie.nix.gc

import ie.nix.gc.TestGCAgent._
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.peersim.Utils.{initConfiguration, now, runSimulator}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class BidBehaviourTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initConfiguration(properties)

  override def beforeEach(): Unit = Random.setSeed(0)

  override def afterEach(): Unit = {
    clearMessageStores()
    resetTestGCAgentBehaviours()
  }

  behavior of "BidBehaviourTest"

  it should "isExpiringSoon" in {
    runSimulator()
    val tenderMessage = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val isExpiringSoon = testGCAgent0.isExpiringSoon(tenderMessage)
    assert(isExpiringSoon)
  }

  it should "isExpiringSoon Not!" in {
    runSimulator()
    val tenderMessage = testGCAgent0.TenderMessage(testNode0, testNode1, later, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val isExpiringSoon = testGCAgent0.isExpiringSoon(tenderMessage)
    assert(!isExpiringSoon)
  }

  it should "sortedTenderMessages highest guide" in {
    sortTenderMessagesBehaviour = sortTenderMessagesBehaviourHighestGuide
    runSimulator()
    val tenderMessages = Vector[TenderMessage[Int]](
      testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE),
      testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE + 1),
      testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE + 2)
    )

    val sortedTenderMessages = testGCAgent1.sortTenderMessages(tenderMessages)
    logger trace s"[$now] sortedTenderMessages=$sortedTenderMessages"

    assert(sortedTenderMessages(0)._1.guide == 3d)
    assert(sortedTenderMessages(1)._1.guide == 2d)
    assert(sortedTenderMessages(2)._1.guide == 1d)
  }

  it should "sortedTenderMessages lowest guide" in {
    sortTenderMessagesBehaviour = sortTenderMessagesBehaviourLowestGuide
    runSimulator()
    val tenderMessages = Vector[TenderMessage[Int]](
      testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE),
      testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE + 1),
      testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE + 2)
    )

    val sortedTenderMessages = testGCAgent1.sortTenderMessages(tenderMessages)
    logger trace s"[$now] sortedTenderMessages=$sortedTenderMessages"

    assert(sortedTenderMessages(0)._1.guide == 1d)
    assert(sortedTenderMessages(1)._1.guide == 2d)
    assert(sortedTenderMessages(2)._1.guide == 3d)
  }

  it should "topTenderMessages" in {
    runSimulator()
    val tenderMessage1 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage2 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE + 1)
    val tenderMessage3 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE + 2)
    testGCAgent1.addMessage(tenderMessage1)
    testGCAgent1.addMessage(tenderMessage2)
    testGCAgent1.addMessage(tenderMessage3)

    val topTenderMessages = testGCAgent1.topTenderMessages

    logger info s"[$now] topTenderMessages=$topTenderMessages"
    logger info s"[$now] receivedMessages=${testGCAgent1.receivedMessages}"
    assert(topTenderMessages.size == 1)
    assert(topTenderMessages(0) == tenderMessage3)
  }

  it should "topTenderMessages has none" in {
    runSimulator()
    val topTenderMessages = testGCAgent1.topTenderMessages

    logger trace s"[$now] topTenderMessages=$topTenderMessages"
    assert(topTenderMessages.isEmpty)
  }

  it should "topTenderMessages More Than 1" in {
    runSimulator()
    val tenderMessage1 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage2 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE + 1)
    val tenderMessage3 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE + 1)
    testGCAgent1.addMessage(tenderMessage1)
    testGCAgent1.addMessage(tenderMessage2)
    testGCAgent1.addMessage(tenderMessage3)

    val topTenderMessages = testGCAgent1.topTenderMessages

    logger trace s"[$now] topTenderMessages=$topTenderMessages"
    assert(topTenderMessages.size == 2)
    assert(topTenderMessages.contains(tenderMessage2))
    assert(topTenderMessages.contains(tenderMessage3))
  }

  it should "topTenderMessagesExpiringSoon" in {
    runSimulator()
    val tenderMessage1 = testGCAgent0.TenderMessage(testNode0, testNode1, later, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage2 = testGCAgent0.TenderMessage(testNode0, testNode1, later, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage3 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    testGCAgent1.addMessage(tenderMessage1)
    testGCAgent1.addMessage(tenderMessage2)
    testGCAgent1.addMessage(tenderMessage3)

    val topTenderMessagesExpiringSoon = testGCAgent1.topTenderMessagesExpiringSoon

    logger trace s"[$now] topTenderMessagesExpiringSoon=$topTenderMessagesExpiringSoon"
    assert(topTenderMessagesExpiringSoon.size == 1)
    assert(topTenderMessagesExpiringSoon.contains(tenderMessage3))
  }

  it should "topTenderMessagesExpiringSoon More Than 1" in {
    runSimulator()
    val tenderMessage1 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage2 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage3 = testGCAgent0.TenderMessage(testNode0, testNode1, later, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    testGCAgent1.addMessage(tenderMessage1)
    testGCAgent1.addMessage(tenderMessage2)
    testGCAgent1.addMessage(tenderMessage3)

    val topTenderMessagesExpiringSoon = testGCAgent1.topTenderMessagesExpiringSoon

    logger trace s"[$now] topTenderMessagesExpiringSoon=$topTenderMessagesExpiringSoon"
    assert(topTenderMessagesExpiringSoon.size == 2)
    assert(topTenderMessagesExpiringSoon.contains(tenderMessage1))
    assert(topTenderMessagesExpiringSoon.contains(tenderMessage2))
  }

  it should "maybeTopTenderExpiringSoon" in {
    runSimulator()
    val tenderMessage1 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage2 = testGCAgent0.TenderMessage(testNode0, testNode1, later, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage3 = testGCAgent0.TenderMessage(testNode0, testNode1, later, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    testGCAgent1.addMessage(tenderMessage1)
    testGCAgent1.addMessage(tenderMessage2)
    testGCAgent1.addMessage(tenderMessage3)

    val maybeTopTenderExpiringSoon = testGCAgent1.maybeTopTenderExpiringSoon

    logger trace s"[$now] maybeTopTenderExpiringSoon=$maybeTopTenderExpiringSoon"
    assert(maybeTopTenderExpiringSoon.isDefined)
    assert(maybeTopTenderExpiringSoon.get == tenderMessage1)
  }

  it should "maybeTopTenderExpiringSoon More Than 1" in {
    runSimulator()
    val tenderMessage1 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    val tenderMessage2 = testGCAgent0.TenderMessage(testNode0, testNode1, soon, 0, DEFAULT_TASK, DEFAULT_GUIDE)
    testGCAgent1.addMessage(tenderMessage1)
    testGCAgent1.addMessage(tenderMessage2)

    var maybeTopTendersExpiringSoon = Vector[Option[TenderMessage[Int]]]()
    maybeTopTendersExpiringSoon = maybeTopTendersExpiringSoon :+ testGCAgent1.maybeTopTenderExpiringSoon
    maybeTopTendersExpiringSoon = maybeTopTendersExpiringSoon :+ testGCAgent1.maybeTopTenderExpiringSoon
    maybeTopTendersExpiringSoon = maybeTopTendersExpiringSoon :+ testGCAgent1.maybeTopTenderExpiringSoon
    maybeTopTendersExpiringSoon = maybeTopTendersExpiringSoon :+ testGCAgent1.maybeTopTenderExpiringSoon
    maybeTopTendersExpiringSoon = maybeTopTendersExpiringSoon :+ testGCAgent1.maybeTopTenderExpiringSoon
    maybeTopTendersExpiringSoon = maybeTopTendersExpiringSoon :+ testGCAgent1.maybeTopTenderExpiringSoon

    logger trace s"[$now] maybeTopTendersExpiringSoon=$maybeTopTendersExpiringSoon"
    assert(maybeTopTendersExpiringSoon.size == 6)
    assert(maybeTopTendersExpiringSoon.contains(Some(tenderMessage1)))
    assert(maybeTopTendersExpiringSoon.contains(Some(tenderMessage2)))
  }

  it should "handleTenderMessage" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    runSimulator()

    assert(testGCAgent1.hasReceivedMessage[TenderMessage[Int]])
  }

  it should "evaluateTenders shouldBid" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1

    runSimulator()

    assert(testGCAgent1.hasSentMessage[BidMessage[Int, Int]])
  }

  it should "evaluateTenders shouldBid Not!" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    runSimulator()

    assert(!testGCAgent1.hasSentMessage[BidMessage[Int, Int]])
  }

  it should "bid should store sent message" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    runSimulator()

    assert(testGCAgent1.hasSentMessage[BidMessage[Int, Int]])
  }

  it should "bid other agent should receive message" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    runSimulator()

    assert(testGCAgent0.hasReceivedMessage[BidMessage[Int, Int]])
  }

  it should "getProposal" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    runSimulator()
    val bidMessage = testGCAgent1.sentMessages[BidMessage[Int, Int]].head
    assert(bidMessage.proposal == DEFAULT_PROPOSAL)
  }

  it should "getOffer" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    runSimulator()
    val bidMessage = testGCAgent1.sentMessages[BidMessage[Int, Int]].head
    assert(bidMessage.offer == DEFAULT_OFFER)
  }

}
