package ie.nix.gc.message

import ie.nix.gc.TestGCAgent.properties
import ie.nix.peersim.Utils.{initConfiguration, nodeAs}
import ie.nix.rl.peersim.TestNode
import org.apache.logging.log4j.scala.Logging
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}

import scala.util.Random

class MessageFactoryTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initConfiguration(properties)

  override def beforeEach(): Unit = Random.setSeed(0)

  behavior of "MessageFactory"

  it should "TenderMessage" in {
    val tenderMessage1 = tenderMessage
    val tenderMessage2 = tenderMessage
    assert(tenderMessage1.from == testNode0)
    assert(tenderMessage1.to == testNode1)
    assert(tenderMessage1.expiresAt == 100L)
    assert(tenderMessage1.contractId != tenderMessage2.contractId)
  }

  it should "TenderMessage from TenderMessage" in {
    val tenderMessage1 = messageFactory.TenderMessage(tenderMessage, testNode2, 10)
    assert(tenderMessage1.to == testNode2)
    assert(tenderMessage1.gossipCounter == 10)
  }

  it should "BidMessage" in {
    val aTenderMessage = tenderMessage
    val aBidMessage = messageFactory.BidMessage(aTenderMessage, 1, 1d)
    assert(aBidMessage.from == aTenderMessage.to)
    assert(aBidMessage.to == aTenderMessage.from)
    assert(aBidMessage.expiresAt == aTenderMessage.expiresAt)
    assert(aBidMessage.contractId == aTenderMessage.contractId)
    assert(aBidMessage.task == aTenderMessage.task)
    assert(aBidMessage.proposal == 1)
    assert(aBidMessage.offer == 1d)
  }

  it should "AwardMessage" in {
    val aTenderMessage = tenderMessage
    val aBidMessage = messageFactory.BidMessage(aTenderMessage, 1, 1d)
    val anAwardMessage = messageFactory.AwardMessage(aBidMessage, 110L)
    assert(anAwardMessage.from == aTenderMessage.from)
    assert(anAwardMessage.from == aBidMessage.to)
    assert(anAwardMessage.to == aTenderMessage.to)
    assert(anAwardMessage.to == aBidMessage.from)
    assert(anAwardMessage.contractId == aTenderMessage.contractId)
    assert(anAwardMessage.contractId == aBidMessage.contractId)
    assert(anAwardMessage.task == aTenderMessage.task)
    assert(anAwardMessage.task == aBidMessage.task)
    assert(anAwardMessage.proposal == aBidMessage.proposal)
    assert(anAwardMessage.expiresAt == 110L)
  }

  val messageFactory: MessageFactory[Int, Int] = new MessageFactory[Int, Int]() {}

  def testNode0: TestNode = nodeAs[TestNode](index = 0)
  def testNode1: TestNode = nodeAs[TestNode](index = 1)
  def testNode2: TestNode = nodeAs[TestNode](index = 2)

  def tenderMessage: TenderMessage[Int] =
    messageFactory.TenderMessage(testNode0, testNode1, 100L, 0, 1, 1d)

  def bidMessage: BidMessage[Int, Int] =
    messageFactory.BidMessage(tenderMessage, 1, 1d)

  def awardMessage: AwardMessage[Int, Int] =
    messageFactory.AwardMessage(bidMessage, 100L)

}
