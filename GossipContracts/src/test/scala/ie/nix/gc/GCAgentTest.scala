package ie.nix.gc

import ie.nix.gc.TestGCAgent._
import ie.nix.peersim.Utils.{initConfiguration, runSimulator}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class GCAgentTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = {
    initConfiguration(properties)
    runSimulator()
  }

  override def beforeEach(): Unit = Random.setSeed(0);

  override def afterEach(): Unit = {
    clearMessageStores()
    resetTestGCAgentBehaviours()
  }

  behavior of "GCAgent"

  it should "tenderTimeout" in assert(testGCAgent0.tenderTimeout == tenderTimeout)
  it should "tenderGossipCount" in assert(testGCAgent0.tenderGossipCount == tenderGossipCount)
  it should "bidBuffer" in assert(testGCAgent0.bidBuffer == bidBuffer)
  it should "awardTimeout" in assert(testGCAgent0.awardTimeout == awardTimeout)

  it should "isTendering" in { startTendering(); assert(testGCAgent0.isTendering) }
  it should "isTendering Not" in assert(!testGCAgent0.isTendering)
  it should "isBidding" in { startBidding(); assert(testGCAgent1.isBidding) }
  it should "isBidding Not" in assert(!testGCAgent0.isBidding)
  it should "isAwarding" in { startAwarding(); assert(testGCAgent0.isAwarding) }
  it should "isAwarding Not" in assert(!testGCAgent0.isAwarding)
  it should "isBeingAwarded" in { startBeingAwarded(); assert(testGCAgent1.isBeingAwarded) }
  it should "isBeingAwarded Not" in assert(!testGCAgent0.isBeingAwarded)

  it should "isIdle" in assert(testGCAgent0.isIdle)
  it should "isIdle Not Tendering" in { startTendering(); assert(!testGCAgent0.isIdle) }
  it should "isIdle Not Bidding" in { startBidding(); assert(!testGCAgent1.isIdle) }
  it should "isIdle Not Awarding" in { startAwarding(); assert(!testGCAgent0.isIdle) }
  it should "isIdle Not BeingAwarded" in { startBeingAwarded(); assert(!testGCAgent1.isIdle) }

  it should "isManaging" in assert(!testGCAgent0.isManaging)
  it should "isManaging Not Tendering" in { startTendering(); assert(testGCAgent0.isManaging) }
  it should "isManaging Not Awarding" in { startAwarding(); assert(testGCAgent0.isManaging) }

  it should "isContracting" in assert(!testGCAgent0.isContracting)
  it should "isContracting Not Bidding" in { startBidding(); assert(testGCAgent1.isContracting) }
  it should "isContracting Not BeingAwarded" in { startBeingAwarded(); assert(testGCAgent1.isContracting) }

  def startTendering(): Unit = testGCAgent0.addMessage(tenderMessage)
  def startBidding(): Unit = testGCAgent1.addMessage(bidMessage)
  def startAwarding(): Unit = testGCAgent0.addMessage(awardMessage)
  def startBeingAwarded(): Unit = testGCAgent1.addMessage(awardMessage)

}
