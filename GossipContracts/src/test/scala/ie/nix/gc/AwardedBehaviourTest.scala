package ie.nix.gc

import ie.nix.gc.TestGCAgent.{
  AWARDED_STATE,
  properties,
  resetTestGCAgentBehaviours,
  shouldAwardBehaviour,
  shouldAwardBehaviourAgentOnly0,
  shouldBidBehaviour,
  shouldBidBehaviourAgentOnly1,
  shouldTenderBehaviour,
  shouldTenderBehaviourAgentOnly0,
  testGCAgent1,
  testNode1
}
import ie.nix.gc.message.AwardMessage
import ie.nix.peersim.Utils.{initConfiguration, runSimulator}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random
@RunWith(classOf[JUnitRunner])
class AwardedBehaviourTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initConfiguration(properties)

  override def beforeEach(): Unit = Random.setSeed(0)

  override def afterEach(): Unit = resetTestGCAgentBehaviours()

  behavior of "AwardedBehaviour"

  it should "handleAwardMessage" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    shouldAwardBehaviour = shouldAwardBehaviourAgentOnly0
    runSimulator()

    assert(testGCAgent1.hasReceivedMessage[AwardMessage[Int, Int]])
  }

  it should "awarded" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    shouldAwardBehaviour = shouldAwardBehaviourAgentOnly0
    runSimulator()

    assert(testNode1.state == AWARDED_STATE)
  }

}
