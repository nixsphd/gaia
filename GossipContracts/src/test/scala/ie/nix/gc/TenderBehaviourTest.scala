package ie.nix.gc

import ie.nix.gc.TestGCAgent._
import ie.nix.gc.message.TenderMessage
import ie.nix.peersim.Utils.{initConfiguration, runSimulator}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class TenderBehaviourTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initConfiguration(properties)

  override def beforeEach(): Unit = Random.setSeed(0);

  override def afterEach(): Unit = resetTestGCAgentBehaviours();

  behavior of "TenderBehaviourTest"

  it should "tenderBehaviour should store sent message" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    runSimulator()

    assert(testGCAgent0.hasSentMessage[TenderMessage[Int]])
  }

  it should "tenderBehaviour other agent should receive message" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    runSimulator()

    assert(testGCAgent0sNeighbour.hasReceivedMessage[TenderMessage[Int]])
  }

  it should "tenderBehaviour task" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    runSimulator()
    val tenderMessage = testGCAgent0.sentMessages[TenderMessage[Int]].head
    assert(tenderMessage.task == DEFAULT_TASK)
  }

  it should "tenderBehaviour guide" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    runSimulator()
    val tenderMessage = testGCAgent0.sentMessages[TenderMessage[Int]].head
    assert(tenderMessage.guide == DEFAULT_GUIDE)
  }

  it should "tenderBehaviour Agent 1 should not tender" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    runSimulator()

    assert(!testGCAgent1.hasSentMessage[TenderMessage[Int]])
  }

}
