package ie.nix.gc

import ie.nix.gc.TestGCAgent.{testGCAgent0, _}
import ie.nix.gc.message.{AwardMessage, BidMessage}
import ie.nix.peersim.Utils.{initConfiguration, now, runSimulator}
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}
import org.scalatestplus.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner])
class AwardBehaviourTest extends AnyFlatSpec with BeforeAndAfterAll with BeforeAndAfterEach with Logging {

  override def beforeAll(): Unit = initConfiguration(properties)

  override def beforeEach(): Unit = Random.setSeed(0)

  override def afterEach(): Unit = resetTestGCAgentBehaviours()

  behavior of "AwardBehaviour"

  it should "handleBidMessage" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    runSimulator()

    assert(testGCAgent0.hasReceivedMessage[BidMessage[Int, Int]])
  }

  it should "sortedBidMessages" in {
    runSimulator()
    val aTenderMessage = tenderMessage
    val bidMessages = Vector[BidMessage[Int, Int]](
      testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER),
      testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 1),
      testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 2)
    )

    val sortedBidMessages = testGCAgent0.sortBidMessages(bidMessages)
    logger trace s"[$now] sortedBidMessages=$sortedBidMessages"

    assert(sortedBidMessages(0)._1.offer == 3d)
    assert(sortedBidMessages(1)._1.offer == 2d)
    assert(sortedBidMessages(2)._1.offer == 1d)
  }

  it should "sortedBidMessages lowest offer" in {
    sortBidMessagesBehaviour = sortBidMessagesBehaviourLowestOffer
    runSimulator()
    val aTenderMessage = tenderMessage
    val bidMessages = Vector[BidMessage[Int, Int]](
      testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER),
      testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 1),
      testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 2)
    )

    val sortedBidMessages = testGCAgent0.sortBidMessages(bidMessages)
    logger trace s"[$now] sortedBidMessages=$sortedBidMessages"

    assert(sortedBidMessages(0)._1.offer == 1d)
    assert(sortedBidMessages(1)._1.offer == 2d)
    assert(sortedBidMessages(2)._1.offer == 3d)
  }

  it should "topBidMessages" in {
    runSimulator()
    val aTenderMessage = tenderMessage

    val bidMessage1 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER)
    val bidMessage2 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 1)
    val bidMessage3 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 2)
    testGCAgent0.addMessage(bidMessage1)
    testGCAgent0.addMessage(bidMessage2)
    testGCAgent0.addMessage(bidMessage3)

    val topBidMessages = testGCAgent0.topBidMessages(aTenderMessage.contractId)

    logger trace s"[$now] topBidMessages=$topBidMessages"
    assert(topBidMessages.size == 1)
    assert(topBidMessages(0) == bidMessage3)
  }

  it should "topBidMessages has none" in {
    runSimulator()

    val topBidMessages = testGCAgent0.topBidMessages(tenderMessage.contractId)

    logger trace s"[$now] topBidMessages=$topBidMessages"
    assert(topBidMessages.isEmpty)
  }

  it should "topBidMessages more than 1" in {
    runSimulator()
    val aTenderMessage = tenderMessage

    val bidMessage1 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER)
    val bidMessage2 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 1)
    val bidMessage3 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 1)
    testGCAgent0.addMessage(bidMessage1)
    testGCAgent0.addMessage(bidMessage2)
    testGCAgent0.addMessage(bidMessage3)

    val topBidMessages = testGCAgent0.topBidMessages(aTenderMessage.contractId)

    logger trace s"[$now] topBidMessages=$topBidMessages"
    assert(topBidMessages.size == 2)
    assert(topBidMessages.contains(bidMessage2))
    assert(topBidMessages.contains(bidMessage3))
  }

  it should "maybeTopBid" in {
    runSimulator()
    val aTenderMessage = tenderMessage

    val bidMessage1 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER)
    val bidMessage2 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 1)
    val bidMessage3 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER + 2)
    testGCAgent0.addMessage(bidMessage1)
    testGCAgent0.addMessage(bidMessage2)
    testGCAgent0.addMessage(bidMessage3)

    val maybeTopBid = testGCAgent0.maybeTopBid(aTenderMessage.contractId)

    logger trace s"[$now] maybeTopBid=$maybeTopBid"
    assert(maybeTopBid.isDefined)
    assert(maybeTopBid.get == bidMessage3)
  }

  it should "maybeTopBid more than 1" in {
    runSimulator()
    val aTenderMessage = tenderMessage

    val bidMessage1 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER)
    val bidMessage2 = testGCAgent1.BidMessage(aTenderMessage, DEFAULT_PROPOSAL, DEFAULT_OFFER)
    testGCAgent0.addMessage(bidMessage1)
    testGCAgent0.addMessage(bidMessage2)

    var maybeTopBids = Vector[Option[BidMessage[Int, Int]]]()
    maybeTopBids = maybeTopBids :+ testGCAgent0.maybeTopBid(aTenderMessage.contractId)
    maybeTopBids = maybeTopBids :+ testGCAgent0.maybeTopBid(aTenderMessage.contractId)
    maybeTopBids = maybeTopBids :+ testGCAgent0.maybeTopBid(aTenderMessage.contractId)
    maybeTopBids = maybeTopBids :+ testGCAgent0.maybeTopBid(aTenderMessage.contractId)
    maybeTopBids = maybeTopBids :+ testGCAgent0.maybeTopBid(aTenderMessage.contractId)
    maybeTopBids = maybeTopBids :+ testGCAgent0.maybeTopBid(aTenderMessage.contractId)

    logger trace s"[$now] maybeTopBids=$maybeTopBids"
    assert(maybeTopBids.size == 6)
    assert(maybeTopBids.contains(Some(bidMessage1)))
    assert(maybeTopBids.contains(Some(bidMessage2)))
  }

  it should "evaluateBids shouldAward" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    shouldAwardBehaviour = shouldAwardBehaviourAgentOnly0

    runSimulator()

    assert(testNode0.state == AWARD_STATE)
  }

  it should "evaluateBids shouldAward Not!" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    runSimulator()

    assert(!testGCAgent0.hasSentMessage[AwardMessage[Int, Int]])
  }

  it should "award should store sent message" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    shouldAwardBehaviour = shouldAwardBehaviourAgentOnly0
    runSimulator()

    assert(testGCAgent0.hasSentMessage[AwardMessage[Int, Int]])
  }

  it should "award other agent should receive message" in {
    shouldTenderBehaviour = shouldTenderBehaviourAgentOnly0
    shouldBidBehaviour = shouldBidBehaviourAgentOnly1
    shouldAwardBehaviour = shouldAwardBehaviourAgentOnly0
    runSimulator()

    assert(testGCAgent1.hasReceivedMessage[AwardMessage[Int, Int]])
  }

}
