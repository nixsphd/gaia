package ie.nix.gc

import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.peersim.Utils._
import ie.nix.peersim.{Node}
import ie.nix.rl.peersim.TestNode
import org.apache.logging.log4j.scala.Logging

class TestGCAgent(prefix: String) extends GCAgent[Int, Int](prefix) {

  override def toString: String = s"TestGCAgent[${node.getIndex}, Idle=$isIdle]"

  override def shouldTender(node: Node): Boolean =
    TestGCAgent.shouldTenderBehaviour.apply(node)

  override def getTask(node: Node): Int =
    TestGCAgent.getTaskBehaviour.apply(node)

  override def getGuide(node: Node, task: Int): Double =
    TestGCAgent.getGuideBehaviour.apply(node, task)

  override def sortTenderMessages(tenderMessages: Vector[TenderMessage[Int]]): Vector[(TenderMessage[Int], Double)] =
    TestGCAgent.sortTenderMessagesBehaviour.apply(tenderMessages)

  override def shouldBid(tenderMessage: TenderMessage[Int]): Boolean =
    TestGCAgent.shouldBidBehaviour.apply(tenderMessage)

  override def getProposal(tenderMessage: TenderMessage[Int]): Int =
    TestGCAgent.getProposalBehaviour.apply(tenderMessage)

  override def getOffer(tenderMessage: TenderMessage[Int], proposal: Int): Double =
    TestGCAgent.getOfferBehaviour.apply(tenderMessage, proposal)

  override def sortBidMessages(bidMessages: Vector[BidMessage[Int, Int]]): Vector[(BidMessage[Int, Int], Double)] =
    TestGCAgent.sortBidMessagesBehaviour.apply(bidMessages)

  override def shouldAward(bidMessage: BidMessage[Int, Int]): Boolean =
    TestGCAgent.shouldAwardBehaviour.apply(bidMessage)

  override def award(awardMessage: AwardMessage[Int, Int]): Unit =
    TestGCAgent.awardBehaviour.apply(awardMessage)

  override def awarded(awardMessage: AwardMessage[Int, Int]): Unit =
    TestGCAgent.awardedBehaviour.apply(awardMessage)

  override def clearExpiredMessages(): Unit = {} // Don't want to clean the old messages.

}

object TestGCAgent extends Logging {

  val DEFAULT_TASK = 5
  val DEFAULT_GUIDE = 1d
  val DEFAULT_PROPOSAL = 10
  val DEFAULT_OFFER = 1d
  val AWARD_STATE = "Award"
  val AWARDED_STATE = "Awarded"

  val properties = Array[String]("src/main/resources/ie/nix/gc/GCAgent.properties",
                                 "src/test/resources/ie/nix/gc/TestGCAgent.properties")

  val shouldTenderBehaviourNo: Node => Boolean = (node) => {
    logger trace s"[$now] node=$node, shouldTender=false"
    false
  }
  val shouldTenderBehaviourAgentOnly0: (Node) => Boolean = (node) => {
    logger trace s"[$now] node=$node, shouldTender=${node.getIndex == 0}"
    node.getIndex == 0
  }
  val getTaskBehaviourDefault: (Node) => Int = (node) => {
    logger trace s"[$now] node=$node"
    DEFAULT_TASK
  }
  val getGuideBehaviourDefault: (Node, Int) => Double = (node, task) => {
    logger trace s"[$now] node=$node, task=$task"
    DEFAULT_GUIDE
  }
  val sortTenderMessagesBehaviourHighestGuide: Vector[TenderMessage[Int]] => Vector[(TenderMessage[Int], Double)] =
    (tenderMessages) => {
      logger info s"[$now] tenderMessages=$tenderMessages"
      tenderMessages.map(tenderMessage => (tenderMessage, -tenderMessage.guide)).sortBy(_._2)
    }
  val sortTenderMessagesBehaviourLowestGuide: Vector[TenderMessage[Int]] => Vector[(TenderMessage[Int], Double)] =
    (tenderMessages) => {
      logger trace s"[$now] tenderMessages=$tenderMessages"
      tenderMessages.map(tenderMessages => (tenderMessages, tenderMessage.guide)).sortBy(_._2)
    }

  val shouldBidBehaviourNo: (TenderMessage[Int]) => Boolean = (tenderMessage) => {
    logger trace s"[$now] tenderMessage=$tenderMessage"
    false
  }
  val shouldBidBehaviourAgentOnly1: (TenderMessage[Int]) => Boolean = (tenderMessage) => {
    logger trace s"[$now] tenderMessage=$tenderMessage, shouldBid=${tenderMessage.to.getIndex == 1}"
    tenderMessage.to.getIndex == 1
  }
  val getProposalBehaviourDefault: (TenderMessage[Int]) => Int = (tenderMessage) => {
    logger trace s"[$now] tenderMessage=$tenderMessage"
    DEFAULT_PROPOSAL
  }
  val getOfferBehaviourDefault: (TenderMessage[Int], Int) => Double = (tenderMessage, proposal) => {
    logger trace s"[$now] tenderMessage=$tenderMessage, proposal=$proposal"
    DEFAULT_OFFER
  }
  val sortBidMessagesBehaviourHighestOffer: Vector[BidMessage[Int, Int]] => Vector[(BidMessage[Int, Int], Double)] =
    (bidMessages) => {
      logger trace s"[$now] bidMessages=$bidMessages"
      bidMessages.map(bidMessage => (bidMessage, -bidMessage.offer)).sortBy(_._2)
    }
  val sortBidMessagesBehaviourLowestOffer: Vector[BidMessage[Int, Int]] => Vector[(BidMessage[Int, Int], Double)] =
    (bidMessages) => {
      logger trace s"[$now] bidMessages=$bidMessages"
      bidMessages.map(bidMessage => (bidMessage, bidMessage.offer)).sortBy(_._2)
    }
  val shouldAwardBehaviourNo: (BidMessage[Int, Int]) => Boolean = (bidMessage) => {
    logger trace s"[$now] bidMessage=$bidMessage"
    false
  }
  val shouldAwardBehaviourAgentOnly0: (BidMessage[Int, Int]) => Boolean = (bidMessage) => {
    logger trace s"[$now] bidMessage=$bidMessage"
    bidMessage.to.getIndex == 0
  }
  val awardBehaviourSetState: (AwardMessage[Int, Int]) => Unit = (awardMessage) => {
    awardMessage.fromAs[TestNode].state = AWARD_STATE
    logger trace s"[$now] awardMessage=$awardMessage"
  }
  val awardedBehaviourSetState: (AwardMessage[Int, Int]) => Unit = (awardMessage) => {
    awardMessage.toAs[TestNode].state = AWARDED_STATE
    logger trace s"[$now] awardMessage=$awardMessage"
  }

  var shouldTenderBehaviour: (Node) => Boolean = shouldTenderBehaviourNo
  var getTaskBehaviour: (Node) => Int = getTaskBehaviourDefault
  var getGuideBehaviour: (Node, Int) => Double = getGuideBehaviourDefault
  var sortTenderMessagesBehaviour: Vector[TenderMessage[Int]] => Vector[(TenderMessage[Int], Double)] =
    sortTenderMessagesBehaviourHighestGuide
  var shouldBidBehaviour: (TenderMessage[Int]) => Boolean = shouldBidBehaviourNo
  var getProposalBehaviour: (TenderMessage[Int]) => Int = getProposalBehaviourDefault
  var getOfferBehaviour: (TenderMessage[Int], Int) => Double = getOfferBehaviourDefault
  var sortBidMessagesBehaviour: Vector[BidMessage[Int, Int]] => Vector[(BidMessage[Int, Int], Double)] =
    sortBidMessagesBehaviourHighestOffer
  var shouldAwardBehaviour: (BidMessage[Int, Int]) => Boolean = shouldAwardBehaviourNo
  var awardBehaviour: (AwardMessage[Int, Int]) => Unit = awardBehaviourSetState
  var awardedBehaviour: (AwardMessage[Int, Int]) => Unit = awardedBehaviourSetState

  def resetTestGCAgentBehaviours(): Unit = {
    shouldTenderBehaviour = shouldTenderBehaviourNo
    getTaskBehaviour = getTaskBehaviourDefault
    getGuideBehaviour = getGuideBehaviourDefault
    sortTenderMessagesBehaviour = sortTenderMessagesBehaviourHighestGuide
    shouldBidBehaviour = shouldBidBehaviourNo
    getProposalBehaviour = getProposalBehaviourDefault
    getOfferBehaviour = getOfferBehaviourDefault
    sortBidMessagesBehaviour = sortBidMessagesBehaviourHighestOffer
    shouldAwardBehaviour = shouldAwardBehaviourNo
    awardBehaviour = awardBehaviourSetState
    awardedBehaviour = awardedBehaviourSetState
  }

  def tenderTimeout: Int = GCAgent.DEFAULT_TENDER_TIMEOUT
  def tenderGossipCount: Int = GCAgent.DEFAULT_TENDER_GOSSIP_COUNT
  def bidBuffer: Int = GCAgent.DEFAULT_BID_BUFFER
  def awardTimeout: Int = GCAgent.DEFAULT_AWARD_TIMEOUT

  def past: Long = now - 10
  def soon: Long = now + 1
  def later: Long = now + 10

  def agentProtocolID: Int = peersim.config.Configuration.lookupPid("gc_agent")

  def testNode0: TestNode = nodeAs[TestNode](index = 0)
  def testNode1: TestNode = nodeAs[TestNode](index = 1)

  def testGCAgent0: TestGCAgent = protocolAs[TestGCAgent](testNode0, agentProtocolID)
  def testGCAgent1: TestGCAgent = protocolAs[TestGCAgent](testNode1, agentProtocolID)
  def testGCAgent0sNeighbour: TestGCAgent = protocolAs[TestGCAgent](testGCAgent0.neighbour(0), agentProtocolID)

  def tenderMessage: TenderMessage[Int] =
    testGCAgent0.TenderMessage(testNode0, testNode1, 300L, 0, 1, 1d)

  def bidMessage: BidMessage[Int, Int] =
    testGCAgent0.BidMessage(tenderMessage, 1, 1d)

  def awardMessage: AwardMessage[Int, Int] =
    testGCAgent0.AwardMessage(bidMessage, 300L)

  def clearMessageStores(): Unit =
    protocolsAs[TestGCAgent](agentProtocolID).foreach(_.clearMessages())

}
