package ie.nix.egc

import ie.nix.egc.TestEGCProblem.{BIDDER_1_INDEX, BIDDER_2_INDEX, TENDERER_1_INDEX, TENDERER_2_INDEX}
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.peersim.Node
import ie.nix.peersim.Utils.protocolsAs

class TestEGCProblem extends EGCProblem {

  override def evaluateSimulation: Double = {
    logger trace s"TENDERER_1_INDEX=$TENDERER_1_INDEX, TENDERER_2_INDEX=$TENDERER_2_INDEX"
    logger trace s"BIDDER_1_INDEX=$BIDDER_1_INDEX, BIDDER_2_INDEX=$BIDDER_2_INDEX"
    var wrongBehaviour = 0
    for (testAgent <- protocolsAs[TestEGCAgent](0)) {

      val theNumberOfTenderMessagesSent = numberOfTenderMessagesSent(testAgent)
      if (shouldBeTendering(testAgent) && theNumberOfTenderMessagesSent != 1) {
        wrongBehaviour += 1
        logger debug s"Wrong Behaviour: testAgent=$testAgent, should be tendering and sent " +
          s"wrong number of tender messages, $theNumberOfTenderMessagesSent"
      } else if (!shouldBeTendering(testAgent) && theNumberOfTenderMessagesSent == 1) {
        wrongBehaviour += 1
        logger debug s"Wrong Behaviour: testAgent=$testAgent, shouldn't be tendering but sent " +
          s"tender messages, $theNumberOfTenderMessagesSent"
      } else {
        logger trace s"Right Behaviour: testAgent=$testAgent, should be tendering and sent the right" +
          s"tender messages, $theNumberOfTenderMessagesSent"

        if (shouldBeTendering(testAgent) && numberOfTenderMessagesSent(testAgent) == 1) {
          val theGuide = guide(testAgent)
          if (theGuide == testAgent.getIndex) {
            logger trace s"Right Behaviour: testAgent=$testAgent, sent the right guide, $theGuide"
          } else {
            wrongBehaviour += 1
            logger debug s"Wrong Behaviour: testAgent=$testAgent, sent the wrong guide, $theGuide"
          }

          val theNumberOfBidMessagesReceived = numberOfBidMessagesReceived(testAgent)
          val theNumberOfAwardMessagesSent = numberOfAwardMessagesSent(testAgent)
          if (theNumberOfBidMessagesReceived == 2 && theNumberOfAwardMessagesSent == 1) {
            val awardTo = awardedTo(testAgent)
            if (awardTo.getIndex == Math.max(BIDDER_1_INDEX, BIDDER_2_INDEX)) {
              logger trace s"Right Behaviour: testAgent=$testAgent, awarded to the right agent, $awardTo"
            } else {
              wrongBehaviour += 1
              logger debug s"Wrong Behaviour: testAgent=$testAgent, awarded to the wrong agent, $awardTo"
            }
          }
        }
      }

      if (numberOfTenderMessagesReceived(testAgent) > 1 && shouldBeBidding(testAgent)) {
        val theNumberOfBidMessagesSent = numberOfBidMessagesSent(testAgent)
        if (theNumberOfBidMessagesSent == 1) {
          logger trace s"Right Behaviour: testAgent=$testAgent, sent right number of bids, $theNumberOfBidMessagesSent"
          if (offer(testAgent) != testAgent.getIndex) {
            wrongBehaviour += 1
            logger debug s"Wrong Behaviour: testAgent=$testAgent, sent the wrong offer, ${offer(testAgent)}, " +
              s"should be ${testAgent.getIndex}"
          }
        } else {
          wrongBehaviour += 1
          logger debug s"Wrong Behaviour: testAgent=$testAgent, sent the wrong number of bid messages," +
            s" $theNumberOfBidMessagesSent"
        }
      } else {
        val theNumberOfBidMessagesSent = numberOfBidMessagesSent(testAgent)
        if (numberOfBidMessagesSent(testAgent) == 0) {
          logger trace s"Right Behaviour: testAgent=$testAgent, is not bidding and didn't send any bid messages, " +
            s"$theNumberOfBidMessagesSent"
        }
      }

      if (numberOfTenderMessagesReceived(testAgent) == 2 && shouldBeBidding(testAgent)) {
        val theNumberOfBidMessagesSent = numberOfBidMessagesSent(testAgent)
        if (theNumberOfBidMessagesSent == 1) {
          val biddedToAgent = biddedTo(testAgent)
          if (biddedToAgent.getIndex == Math.min(TENDERER_1_INDEX, TENDERER_2_INDEX)) {
            logger trace s"Right Behaviour: testAgent=$testAgent, bidded to the right agent $biddedToAgent"
          } else {
            wrongBehaviour += 1
            logger debug s"Wrong Behaviour: testAgent=$testAgent, bidded to the wrong agent $biddedToAgent, " +
              s"instead of ${Math.min(TENDERER_1_INDEX, TENDERER_2_INDEX)}"
          }
        } else {
          wrongBehaviour += 1
          logger debug s"Wrong Behaviour: testAgent=$testAgent, did not send a bid messages," +
            s" $theNumberOfBidMessagesSent"
        }
      }
    }
    val result = wrongBehaviour.toDouble
    logger debug s"wrongBehaviour=$wrongBehaviour, result=$result"
    result
  }

  def shouldBeTendering(testAgent: TestEGCAgent): Boolean =
    testAgent.getIndex == TENDERER_1_INDEX || testAgent.getIndex == TENDERER_2_INDEX
  def guide(testAgent: TestEGCAgent): Double =
    testAgent.sentMessages[TenderMessage[_]].head.guide
  def shouldBeBidding(testAgent: TestEGCAgent): Boolean =
    testAgent.getIndex == BIDDER_1_INDEX || testAgent.getIndex == BIDDER_2_INDEX
  def offer(testAgent: TestEGCAgent): Double = testAgent.sentMessages[BidMessage[_, _]].head.offer
  def numberOfTenderMessagesSent(testAgent: TestEGCAgent): Int =
    testAgent.sentMessages[TenderMessage[_]].size
  def numberOfTenderMessagesReceived(testAgent: TestEGCAgent): Int =
    testAgent.receivedMessages[TenderMessage[_]].size
  def numberOfBidMessagesSent(testAgent: TestEGCAgent): Int =
    testAgent.sentMessages[BidMessage[_, _]].size
  def numberOfBidMessagesReceived(testAgent: TestEGCAgent): Int =
    testAgent.receivedMessages[BidMessage[_, _]].size
  def numberOfAwardMessagesSent(testAgent: TestEGCAgent): Int =
    testAgent.sentMessages[AwardMessage[_, _]].size
  def numberOfAwardMessagesReceived(testAgent: TestEGCAgent): Int =
    testAgent.receivedMessages[AwardMessage[_, _]].size
  def biddedTo(testAgent: TestEGCAgent): Node =
    testAgent.sentMessages[BidMessage[_, _]].head.to;
  def awardedTo(testAgent: TestEGCAgent): Node =
    testAgent.sentMessages[AwardMessage[_, _]].head.to;
}

object TestEGCProblem {
  val TENDERER_1_INDEX = 5
  val TENDERER_2_INDEX = 6
  val BIDDER_1_INDEX = 10
  val BIDDER_2_INDEX = 11
}
