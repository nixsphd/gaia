package ie.nix.egc

import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Constant.DoubleConstant
import ie.nix.ecj.gp.Variable
import ie.nix.gc.GCAgent
import ie.nix.gc.GCAgent.{getAwardTimeout, getBidBuffer, getTenderGossipCount, getTenderTimeout}
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.peersim.Node

class TestEGCAgent(prefix: String, tenderTimeout: Int, tenderGossipCount: Int, bidBuffer: Int, awardTimeout: Int)
    extends GCAgent[Int, Int](prefix, tenderTimeout, tenderGossipCount, bidBuffer, awardTimeout)
    with EGCAgent[Int, Int] {

  def this(prefix: String) =
    this(prefix, getTenderTimeout(prefix), getTenderGossipCount(prefix), getBidBuffer(prefix), getAwardTimeout(prefix))

  override def getTask(node: Node): Int = -1
  override def getProposal(tenderMessage: TenderMessage[Int]): Int = -1
  override def awarded(awardMessage: AwardMessage[Int, Int]): Unit = {}
  override def award(awardMessage: AwardMessage[Int, Int]): Unit = {}
  override def clearExpiredMessages(): Unit = {} // Don't want to clean the old messages.

  override def defaultShouldTender(node: Node): Boolean = {
    val index = node.getIndex
    index == TestEGCProblem.TENDERER_1_INDEX || index == TestEGCProblem.TENDERER_2_INDEX
  }
  override def defaultGetGuide(node: Node, task: Int): Double = node.getIndex.toDouble
  override def defaultTenderToDouble(tenderMessage: TenderMessage[Int]): Double =
    tenderMessage.from.getIndex.toDouble
  override def defaultShouldBid(tenderMessage: TenderMessage[Int]): Boolean = {
    val index = tenderMessage.to.getIndex
    index == TestEGCProblem.BIDDER_1_INDEX || index == TestEGCProblem.BIDDER_2_INDEX
  }
  override def defaultGetOffer(tenderMessage: TenderMessage[Int], proposal: Int): Double =
    tenderMessage.to.getIndex.toDouble
  override def defaultBidToDouble(bidMessage: BidMessage[Int, Int]): Double =
    -bidMessage.from.getIndex.toDouble
  override def defaultShouldAward(bidMessage: BidMessage[Int, Int]): Boolean = true

  override protected def updateNodes(node: Node): Unit =
    NodeIndex.value = node.getIndex.toDouble
  override protected def updateNodes(tenderMessage: TenderMessage[Int]): Unit = {
    super.updateNodes(tenderMessage)
    FromIndex.value = tenderMessage.from.getIndex.toDouble
  }
  override protected def updateNodes(bidMessage: BidMessage[Int, Int]): Unit = {
    super.updateNodes(bidMessage)
    FromIndex.value = bidMessage.from.getIndex.toDouble
  }
  override protected def updateTaskNode(task: Int): Unit = {}
  override protected def updateProposalNode(proposal: Int): Unit = {}

}

object NodeIndex { var value: Double = 0 }
class NodeIndex() extends Variable("NodeIndex", (_: Array[TypedData], result: TypedData) => result.set(NodeIndex.value))
object FromIndex { var value: Double = 0d }
class FromIndex() extends Variable("FromIndex", (_: Array[TypedData], result: TypedData) => result.set(FromIndex.value))
class Five() extends DoubleConstant("5", 5d)
class Six() extends DoubleConstant("6", 6d)
class Ten() extends DoubleConstant("10", 10d)
class Eleven() extends DoubleConstant("11", 11d)
