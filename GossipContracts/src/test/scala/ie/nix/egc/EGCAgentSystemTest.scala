package ie.nix.egc

import ie.nix.ecj.PeerSimProblem.bestFitness
import org.apache.logging.log4j.scala.Logging
import org.junit.runner.RunWith
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

import java.security.Permission

@RunWith(classOf[JUnitRunner])
class EGCAgentSystemTest extends AnyFlatSpec with BeforeAndAfterAll with Logging {

  behavior of "EGCAgent"

  it should "have good defaults" in {
    val properties = defaultProperties ++
      Array[String]("-p", "pop.subpop.0.size=1")
    testProblem(properties)
  }

  it should "Tender" in {
    val properties = defaultProperties ++
      Array[String]("-p", "eval.problem.shouldTenderTreeIndex=0", "-p", "pop.subpop.0.size=2048")
    testProblem(properties)
  }

  it should "Get Guide" in {
    val properties = defaultProperties ++
      Array[String]("-p", "eval.problem.getGuideTreeIndex=1")
    testProblem(properties)
  }

  it should "map a tenders to a double" in {
    val properties = defaultProperties ++
      Array[String]("-p", "eval.problem.tenderToDoubleTreeIndex=2")
    testProblem(properties)
  }

  it should "Bid" in {
    val properties = defaultProperties ++
      Array[String]("-p", "eval.problem.shouldBidTreeIndex=3")
    testProblem(properties)
  }

  it should "Get Offer" in {
    val properties = defaultProperties ++
      Array[String]("-p", "eval.problem.getOfferTreeIndex=4")
    testProblem(properties)
  }

  it should "map a bid to a double" in {
    val properties = defaultProperties ++
      Array[String]("-p", "eval.problem.bidToDoubleTreeIndex=5")
    testProblem(properties)
  }

  it should "Award" in {
    val properties = defaultProperties ++
      Array[String]("-p", "eval.problem.shouldAwardTreeIndex=6")
    testProblem(properties)
  }

  def testProblem(properties: Array[String]): Unit = {
    resetSecurityManager()
    assertThrows[SecurityException] {
      ec.Evolve.main(properties)
    }
    val theBestFitness = bestFitness
    logger info s"bestFitness=$theBestFitness"

    assert(theBestFitness === 1d)
  }

  def defaultProperties: Array[String] = Array[String](
    "-file",
    "src/test/resources/ie/nix/egc/TestEGCAgent.params",
    "-p",
    "generations=1",
    "-p",
    "pop.subpop.0.size=1024",
//    "-p",
//    "silent=false",
//    "-p",
//    "gp.problem.logging=console",
  )

  def resetSecurityManager(): Unit =
    System.setSecurityManager(new SecurityManager() {
      override def checkPermission(perm: Permission): Unit = {}
      override def checkExit(status: Int): Unit = throw new SecurityException
    })

}
