package ie.nix.peersim

import ie.nix.peersim.Checkpointing.{
  getCheckpointDirName,
  getCheckpointFileName,
  getReadCheckpoints,
  getWriteCheckpoints
}
import ie.nix.peersim.Utils.now
import ie.nix.util.Utils.as

import java.io.File
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}
import scala.util.{Failure, Success, Try}

trait Checkpointing[T] extends Control with Logging {
  peersim: { val prefix: String } =>

  val checkpointFileName: String = getCheckpointFileName(prefix)
  val checkpointDirName: String = getCheckpointDirName(prefix)
  val readCheckpoints: Boolean = getReadCheckpoints(prefix)
  val writeCheckpoints: Boolean = getWriteCheckpoints(prefix)

  logger info s"checkpointFileName=$checkpointFileName, " +
    s"checkpointDirName=$checkpointDirName, " +
    s"readCheckpoints=$readCheckpoints, " +
    s"writeCheckpoints=$writeCheckpoints"

  var nextCheckpointIndex = lastCheckpointIndex

  override protected def init(): Boolean = {
    if (readCheckpoints) {
      logger debug s"[$now] Loading Checkpoint $nextCheckpointIndex"
      readCheckpoint()
    }
    false
  }

  override protected def step(): Boolean = {
    if (writeCheckpoints) {
      logger debug s"[$now] Saving Checkpoint $nextCheckpointIndex"
      writeCheckpoint()
    }
    false
  }

  def writeCheckpoint(): Unit =
    objectOutputStream(nextCheckpointFile) match {
      case Success(objectOutputStream) =>
        objectOutputStream.writeObject(objectToCheckpoint)
        logger info s"[$now] Writing to $nextCheckpointFile"
        nextCheckpointIndex += 1
      case Failure(exception) =>
        logger error s"[$now] Failure Logging to $nextCheckpointFile, ${exception.toString}"
    }

  def objectOutputStream(checkpointFileName: String): Try[ObjectOutputStream] =
    Try(new ObjectOutputStream(new FileOutputStream(checkpointFileName)))

  def nextCheckpointFile: String = s"$checkpointDirName/$checkpointFileName-$nextCheckpointIndex.ser"

  def readCheckpoint(): Unit = maybeCheckpoint match {
    case Some(checkpoint) => updateObjectFromCheckpoint(checkpoint)
    case None             => logger info s"No Checkpoint file, $nextCheckpointFile, found."
  }

  def maybeCheckpoint: Option[T] =
    objectInputStream(nextCheckpointFile) match {
      case Success(objectInputStream) =>
        logger info s"[$now] Reading from $nextCheckpointFile"
        nextCheckpointIndex += 1
        Some(as[T](objectInputStream.readObject()))
      case Failure(exception) =>
        logger error s"[$now] $nextCheckpointFile not found."
        None
    }

  def objectInputStream(checkpointFileName: String): Try[ObjectInputStream] =
    Try(new ObjectInputStream(new FileInputStream(checkpointFileName)))

  def lastCheckpointIndex: Int = checkpointFileNames.map(toCheckpointNumber).:+(0).max

  def toCheckpointNumber(aCheckpointFileName: String): Int = {
    val checkpointNumberPattern = ".*-([0-9]+).ser".r
    val checkpointNumberPattern(checkpointNumber) = aCheckpointFileName
    logger.trace(s"aCheckpointFileName=$aCheckpointFileName, checkpointNumber=$checkpointNumber")
    checkpointNumber.toInt
  }

  def checkpointFileNames: Vector[String] =
    new File(checkpointDirName)
      .listFiles(file => file.getAbsolutePath.matches(s".*$checkpointFileName-.*.ser"))
      .map(file => file.getAbsolutePath)
      .toVector

  def updateObjectFromCheckpoint(checkpoint: T): Unit

  def objectToCheckpoint(): T

}

object Checkpointing {

  val CHECKPOINT_DIR = "checkpoint_dir"
  val CHECKPOINT_FILE = "checkpoint_file"

  def getCheckpointDirName(prefix: String): String =
    Configuration.getString(s"$prefix.$CHECKPOINT_DIR", Configuration.getString(CHECKPOINT_DIR, "."))

  def getCheckpointFileName(prefix: String): String =
    Configuration.getString(s"$prefix.$CHECKPOINT_FILE", "Checkpoint")

  def getReadCheckpoints(prefix: String): Boolean =
    Configuration.getBoolean(s"$prefix.read", true)

  def getWriteCheckpoints(prefix: String): Boolean =
    Configuration.getBoolean(s"$prefix.write", true)

}
