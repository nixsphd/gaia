package ie.nix.egc

import ec.EvolutionState
import ec.simple.SimpleProblemForm
import ec.util.Parameter
import ie.nix.ecj.PeerSimProblem
import ie.nix.egc.EGCProblem.{
  P_BID_TO_DOUBLE_TREE_INDEX,
  P_GET_GUIDE_TREE_INDEX,
  P_GET_OFFER_TREE_INDEX,
  P_SHOULD_AWARD_TREE_INDEX,
  P_SHOULD_BID_TREE_INDEX,
  P_SHOULD_TENDER_TREE_INDEX,
  P_TENDER_TO_DOUBLE_TREE_INDEX,
  TREE_NOT_SET,
  bidToDoubleTreeIndex,
  getGuideTreeIndex,
  getOfferTreeIndex,
  shouldAwardTreeIndex,
  shouldBidTreeIndex,
  shouldTenderTreeIndex,
  tenderToDoubleTreeIndex
}

abstract class EGCProblem extends PeerSimProblem with SimpleProblemForm {

  override def setup(state: EvolutionState, base: Parameter): Unit = {
    super.setup(state, base) // very important, remember this
    shouldTenderTreeIndex = getTreeIndex(base, P_SHOULD_TENDER_TREE_INDEX)
    logger debug s"shouldTenderTreeIndex=$shouldTenderTreeIndex"
    getGuideTreeIndex = getTreeIndex(base, P_GET_GUIDE_TREE_INDEX)
    logger debug s"getGuideTreeIndex=$getGuideTreeIndex"
    tenderToDoubleTreeIndex = getTreeIndex(base, P_TENDER_TO_DOUBLE_TREE_INDEX)
    logger debug s"tenderToDoubleTreeIndex=$tenderToDoubleTreeIndex"
    shouldBidTreeIndex = getTreeIndex(base, P_SHOULD_BID_TREE_INDEX)
    logger debug s"shouldBidTreeIndex=$shouldBidTreeIndex"
    getOfferTreeIndex = getTreeIndex(base, P_GET_OFFER_TREE_INDEX)
    logger debug s"getOfferTreeIndex=$getOfferTreeIndex"
    shouldAwardTreeIndex = getTreeIndex(base, P_SHOULD_AWARD_TREE_INDEX)
    logger debug s"shouldAwardTreeIndex=$shouldAwardTreeIndex"
    bidToDoubleTreeIndex = getTreeIndex(base, P_BID_TO_DOUBLE_TREE_INDEX)
    logger debug s"bidToDoubleTreeIndex=$bidToDoubleTreeIndex"
  }

  def getTreeIndex(base: Parameter, param: String): Int =
    state.parameters.getIntWithDefault(base.push(param), defaultBase.push(param), TREE_NOT_SET)

}

object EGCProblem {

  protected val P_SHOULD_TENDER_TREE_INDEX = "shouldTenderTreeIndex"
  protected val P_GET_GUIDE_TREE_INDEX = "getGuideTreeIndex"
  protected val P_TENDER_TO_DOUBLE_TREE_INDEX = "tenderToDoubleTreeIndex"
  protected val P_SHOULD_BID_TREE_INDEX = "shouldBidTreeIndex"
  protected val P_GET_OFFER_TREE_INDEX = "getOfferTreeIndex"
  protected val P_BID_TO_DOUBLE_TREE_INDEX = "bidToDoubleTreeIndex"
  protected val P_SHOULD_AWARD_TREE_INDEX = "shouldAwardTreeIndex"

  val TREE_NOT_SET: Int = -1
  var shouldTenderTreeIndex: Int = TREE_NOT_SET
  var getGuideTreeIndex: Int = TREE_NOT_SET
  var tenderToDoubleTreeIndex: Int = TREE_NOT_SET
  var shouldBidTreeIndex: Int = TREE_NOT_SET
  var getOfferTreeIndex: Int = TREE_NOT_SET
  var bidToDoubleTreeIndex: Int = TREE_NOT_SET
  var shouldAwardTreeIndex: Int = TREE_NOT_SET
}
