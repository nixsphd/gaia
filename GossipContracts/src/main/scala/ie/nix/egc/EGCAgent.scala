package ie.nix.egc

import ie.nix.ecj.gp.Variable
import ie.nix.ecj.{PeerSimProblem, TypedData}
import ie.nix.egc.EGCProblem._
import ie.nix.gc.GCAgent
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.peersim.Node

trait EGCAgent[T, P] {
  this: GCAgent[T, P] =>

  override def shouldTender(node: Node): Boolean =
    if (shouldEvolve(shouldTenderTreeIndex)) {
      updateNodes(node)
      PeerSimProblem.gpProblem.evaluateTreeForBoolean(shouldTenderTreeIndex)
    } else defaultShouldTender(node)
  override def getGuide(node: Node, task: T): Double =
    if (shouldEvolve(getGuideTreeIndex)) {
      updateNodes(node)
      updateTaskNode(task)
      PeerSimProblem.gpProblem.evaluateTreeForDouble(getGuideTreeIndex)
    } else defaultGetGuide(node, task)
  override def tenderToDouble(tenderMessage: TenderMessage[T]): Double =
    if (shouldEvolve(tenderToDoubleTreeIndex)) {
      updateNodes(tenderMessage)
      PeerSimProblem.gpProblem.evaluateTreeForDouble(tenderToDoubleTreeIndex)
    } else defaultTenderToDouble(tenderMessage)
  override def shouldBid(tenderMessage: TenderMessage[T]): Boolean =
    if (shouldEvolve(shouldBidTreeIndex)) {
      updateNodes(tenderMessage)
      PeerSimProblem.gpProblem.evaluateTreeForBoolean(shouldBidTreeIndex)
    } else defaultShouldBid(tenderMessage)
  override def getOffer(tenderMessage: TenderMessage[T], proposal: P): Double =
    if (shouldEvolve(getOfferTreeIndex)) {
      updateNodes(tenderMessage)
      updateProposalNode(proposal)
      PeerSimProblem.gpProblem.evaluateTreeForDouble(getOfferTreeIndex)
    } else defaultGetOffer(tenderMessage, proposal)
  override def bidToDouble(bidMessage: BidMessage[T, P]): Double =
    if (shouldEvolve(bidToDoubleTreeIndex)) {
      updateNodes(bidMessage)
      PeerSimProblem.gpProblem.evaluateTreeForDouble(bidToDoubleTreeIndex)
    } else defaultBidToDouble(bidMessage)
  override def shouldAward(bidMessage: BidMessage[T, P]): Boolean =
    if (shouldEvolve(shouldAwardTreeIndex)) {
      updateNodes(bidMessage)
      PeerSimProblem.gpProblem.evaluateTreeForBoolean(shouldAwardTreeIndex)
    } else defaultShouldAward(bidMessage)

  protected def shouldEvolve(treeIndex: Int): Boolean = treeIndex != TREE_NOT_SET

  protected def defaultShouldTender(node: Node): Boolean
  protected def defaultGetGuide(node: Node, task: T): Double
  protected def defaultTenderToDouble(tenderMessage: TenderMessage[T]): Double
  protected def defaultShouldBid(tenderMessage: TenderMessage[T]): Boolean
  protected def defaultGetOffer(tenderMessage: TenderMessage[T], proposal: P): Double
  protected def defaultBidToDouble(bidMessage: BidMessage[T, P]): Double
  protected def defaultShouldAward(bidMessage: BidMessage[T, P]): Boolean

  protected def updateNodes(tenderMessage: TenderMessage[T]): Unit = {
    updateNodes(tenderMessage.to)
    updateTaskNode(tenderMessage.task)
    updateGuideNode(tenderMessage.guide)
  }
  protected def updateNodes(bidMessage: BidMessage[T, P]): Unit = {
    updateNodes(bidMessage.to)
    updateTaskNode(bidMessage.task)
    updateProposalNode(bidMessage.proposal)
    updateOfferNode(bidMessage.offer)
  }
  protected def updateGuideNode(guide: Double): Unit = Guide.value = guide
  protected def updateOfferNode(offer: Double): Unit = Offer.value = offer

  protected def updateNodes(node: Node): Unit
  protected def updateTaskNode(task: T): Unit
  protected def updateProposalNode(proposal: P): Unit

}

object Guide { var value = 0d }
class Guide() extends Variable("guide", (_: Array[TypedData], result: TypedData) => result.set(Guide.value))

object Offer { var value = 0d }
class Offer() extends Variable("offer", (_: Array[TypedData], result: TypedData) => result.set(Offer.value))
