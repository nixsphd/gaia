package ie.nix.lgc

import ie.nix.gc.GCAgent
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.lgc.LGCEnvironment._
import ie.nix.peersim.Node
import ie.nix.rl.Environment.{Action, NullState, State}
import ie.nix.rl.peersim.Learning
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

trait LGCAgent[T, P] extends Learning with Logging {
  gcAgent: GCAgent[T, P] =>

  override def preTenderAwardCycle(node: Node): Unit = mapToStateForPreTenderAwardCycle(node) match {
    case NullState => defaultPreTenderAwardCycle(node)
    case state =>
      executePreTenderAwardCycleAction(nextAction(LGCState(LGCEnvironment.PreTenderAwardCycle, state)), node)
  }

  override def shouldTender(node: Node): Boolean = mapToStateForShouldTender(node) match {
    case NullState => defaultShouldTender(node)
    case state     => actionToBoolean(nextAction(LGCState(LGCEnvironment.ShouldTender, state)))
  }

  override def getGuide(node: Node, task: T): Double = mapToStateForGetGuide(node, task) match {
    case NullState => defaultGetGuide(node, task)
    case state     => actionToDouble(nextAction(LGCState(LGCEnvironment.GetGuide, state)))
  }

  override def tenderToDouble(tenderMessage: TenderMessage[T]): Double =
    tenderMessage.guide

  override def shouldBid(tenderMessage: TenderMessage[T]): Boolean = mapToStateForShouldBid(tenderMessage) match {
    case NullState => defaultShouldBid(tenderMessage)
    case state     => actionToBoolean(nextAction(LGCState(LGCEnvironment.ShouldBid, state)))
  }

  override def getOffer(tenderMessage: TenderMessage[T], proposal: P): Double =
    mapToStateForGetOffer(tenderMessage, proposal) match {
      case NullState => defaultGetOffer(tenderMessage, proposal)
      case state     => actionToDouble(nextAction(LGCState(LGCEnvironment.GetOffer, state)))
    }

  override def bidToDouble(bidMessage: BidMessage[T, P]): Double =
    bidMessage.offer

  override def shouldAward(bidMessage: BidMessage[T, P]): Boolean = mapToStateForShouldAward(bidMessage) match {
    case NullState => defaultShouldAward(bidMessage)
    case state     => actionToBoolean(nextAction(LGCState(LGCEnvironment.ShouldAward, state)))
  }

  override def award(awardMessage: AwardMessage[T, P]): Unit = mapToStateForAward(awardMessage) match {
    case NullState => defaultAward(awardMessage)
    case state     => executeAwardAction(nextAction(LGCState(LGCEnvironment.Award, state)), awardMessage)
  }

  override def awarded(awardMessage: AwardMessage[T, P]): Unit = mapToStateForAwarded(awardMessage) match {
    case NullState => defaultAwarded(awardMessage)
    case state     => executeAwardedAction(nextAction(LGCState(LGCEnvironment.Awarded, state)), awardMessage)
  }

  override def postTenderAwardCycle(node: Node): Unit = mapToStateForPostTenderAwardCycle(node) match {
    case NullState => defaultPostTenderAwardCycle(node)
    case state =>
      executePostTenderAwardCycleAction(nextAction(LGCState(LGCEnvironment.PostTenderAwardCycle, state)), node)
  }

  def nextAction(lgcState: LGCState): Action = {
    if (environmentHasState(lgcState)) {
      rlAgent.getAction(lgcState)
    } else {
      logger error s"The state, $lgcState, has not been added to the environment.";
      exit()
    }
  }

  def environmentHasState(lgcState: LGCState): Boolean = rlAgent.environment.hasState(lgcState)

  def actionToBoolean(action: Action): Boolean = action match {
    case ReturnTrue  => true
    case ReturnFalse => false
    case _           => false
  }

  def actionToDouble(action: Action): Double = action match {
    case ReturnDouble(value) => value
    case _                   => Double.NaN
  }

  def executePreTenderAwardCycleAction(action: Action, node: Node): Unit = action match {
    case Default    => { defaultPreTenderAwardCycle(node); logger debug s"PreTenderAwardCycleAction -> $node" }
    case notDefault => logger error s"Unrecognised PreTenderAwardCycleAction Action, $notDefault, doing nothing"
  }

  def executeAwardAction(action: Action, awardMessage: AwardMessage[T, P]): Unit = action match {
    case Default    => { defaultAward(awardMessage); logger debug s"Award -> ${awardMessage.to}" }
    case notDefault => logger error s"Unrecognised Award Action, $notDefault, doing nothing"
  }

  def executeAwardedAction(action: Action, awardMessage: AwardMessage[T, P]): Unit = action match {
    case Default    => { defaultAwarded(awardMessage); logger debug s"Awarded -> ${awardMessage.to}" }
    case notDefault => logger error s"Unrecognised Awarded Action, $notDefault, doing nothing"
  }

  def executePostTenderAwardCycleAction(action: Action, node: Node): Unit = action match {
    case Default    => { defaultPostTenderAwardCycle(node); logger debug s"PostTenderAwardCycleAction -> $node" }
    case notDefault => logger error s"Unrecognised PostTenderAwardCycleAction Action, $notDefault, doing nothing"
  }

  def mapToStateForPreTenderAwardCycle(node: Node): State = NullState
  def mapToStateForShouldTender(node: Node): State = NullState
  def mapToStateForGetGuide(node: Node, task: T): State = NullState
  def mapToStateForShouldBid(tenderMessage: TenderMessage[T]): State = NullState
  def mapToStateForGetOffer(tenderMessage: TenderMessage[T], proposal: P): State = NullState
  def mapToStateForShouldAward(bidMessage: BidMessage[T, P]): State = NullState
  def mapToStateForAward(awardMessage: AwardMessage[T, P]): State = NullState
  def mapToStateForAwarded(awardMessage: AwardMessage[T, P]): State = NullState
  def mapToStateForPostTenderAwardCycle(node: Node): State = NullState

  protected def defaultPreTenderAwardCycle(node: Node): Unit = {}
  protected def defaultShouldTender(node: Node): Boolean
  protected def defaultGetGuide(node: Node, task: T): Double
  protected def defaultShouldBid(tenderMessage: TenderMessage[T]): Boolean
  protected def defaultGetOffer(tenderMessage: TenderMessage[T], proposal: P): Double
  protected def defaultShouldAward(bidMessage: BidMessage[T, P]): Boolean
  protected def defaultAward(awardMessage: AwardMessage[T, P]): Unit
  protected def defaultAwarded(awardMessage: AwardMessage[T, P]): Unit
  protected def defaultPostTenderAwardCycle(node: Node): Unit = {}

}
