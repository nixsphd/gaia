package ie.nix.lgc

import ie.nix.rl.Environment.{Action, NullState, State}
import ie.nix.rl.peersim.Environment
import org.apache.logging.log4j.scala.Logging

class LGCEnvironment(lgcEnvironmentBuilder: LGCEnvironmentBuilder)
    extends Environment(lgcEnvironmentBuilder.getStateActions, lgcEnvironmentBuilder.getTerminalStates)

object LGCEnvironment extends Logging {

  trait GCState extends State
  object PreTenderAwardCycle extends GCState { override def toString: String = "PreTenderAwardCycle" }
  object ShouldTender extends GCState { override def toString: String = "ShouldTender" }
  object GetGuide extends GCState { override def toString: String = "GetGuide" }
  object SortTenderMessages extends GCState { override def toString: String = "SortTenderMessages" }
  object ShouldBid extends GCState { override def toString: String = "ShouldBid" }
  object GetOffer extends GCState { override def toString: String = "GetOffer" }
  object SortBidMessages extends GCState { override def toString: String = "SortBidMessages" }
  object ShouldAward extends GCState { override def toString: String = "ShouldAward" }
  object Award extends GCState { override def toString: String = "Award" }
  object Awarded extends GCState { override def toString: String = "Awarded" }
  object PostTenderAwardCycle extends GCState { override def toString: String = "PostTenderAwardCycle" }

  trait LGCAction extends Action
  case object Default extends LGCAction { override def toString: String = "Default" }
  case object ReturnTrue extends LGCAction { override def toString: String = "True" }
  case object ReturnFalse extends LGCAction { override def toString: String = "False" }
  case class ReturnDouble(value: Double) extends LGCAction { override def toString: String = s"Return-$value" }
  case class MapAction(map: Map[MapKey, Double]) extends LGCAction {
    override def toString: String = s"MapAction(${map.mkString("(", " ", ")")})"
  }
  case class MapKey(state: State, guideOrOffer: Double = Double.NaN) extends State {
    override def equals(obj: Any): Boolean = obj match {
      case mapKey: MapKey => toString.equals(mapKey.toString)
      case _              => false
    }
    override def toString: String = {
      val guideOrOfferString = if (guideOrOffer.isNaN) "" else s"-$guideOrOffer"
      s"$state$guideOrOfferString"
    }
  }

  case class LGCState(gcState: GCState, state: State = NullState, guideOrOffer: Double = Double.NaN) extends State {

    override def equals(obj: Any): Boolean = obj match {
      case lgcState: LGCState => toString.equals(lgcState.toString)
      case _                  => false
    }

    override def toString: String = {
      val stateString = if (state == NullState) "" else s"-$state"
      val guideOrOfferString = if (guideOrOffer.isNaN) "" else s"-$guideOrOffer"
      s"$gcState$stateString$guideOrOfferString"
    }
  }

}
