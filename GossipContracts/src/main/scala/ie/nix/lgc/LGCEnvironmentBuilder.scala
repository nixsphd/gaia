package ie.nix.lgc

import ie.nix.lgc.LGCEnvironment._
import ie.nix.lgc.LGCEnvironmentBuilder._
import ie.nix.rl.Environment.{Action, State, StateAction}
import peersim.config.Configuration

case class LGCEnvironmentBuilder(private val prefix: String,
                                 private val stateActions: Set[StateAction] = Set[StateAction](),
                                 private val terminalStates: Set[State] = Set[State]()) {

  def addPreTenderAwardCycleStates(preTenderAwardCycleStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(PreTenderAwardCycle, preTenderAwardCycleStates, defaultActionSet)

  def addShouldTenderStates(shouldTenderStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(ShouldTender, shouldTenderStates, booleanActions)

  def addGetGuideStates(getGuideStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(GetGuide, getGuideStates, getGetGuideActions(prefix))
  def addGetGuideStates(getGuideStates: Set[State], getGuideActions: Set[Action]): LGCEnvironmentBuilder =
    addStateActions(GetGuide, getGuideStates, getGuideActions)

  def addTenderToDoubleStates(tenderToDoubleStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(SortTenderMessages, getGuideValues(prefix), tenderToDoubleStates, getTenderValues(prefix))

  def addShouldBidStates(shouldBidStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(ShouldBid, shouldBidStates, booleanActions)

  def addGetOfferStates(getOfferStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(GetOffer, getOfferStates, getGetOfferActions(prefix))
  def addGetOfferStates(getOfferStates: Set[State], getOfferActions: Set[Action]): LGCEnvironmentBuilder =
    addStateActions(GetOffer, getOfferStates, getOfferActions)

  def addBidToDoubleStates(bidToDoubleStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(SortBidMessages, getOfferValues(prefix), bidToDoubleStates, getBidValues(prefix))

  def addShouldAwardStates(shouldAwardStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(ShouldAward, shouldAwardStates, booleanActions)

  def addAwardStates(awardStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(Award, awardStates, defaultActionSet)
  def addAwardStates(awardStates: Set[State], awardActions: Set[Action]): LGCEnvironmentBuilder =
    addStateActions(Award, awardStates, awardActions)

  def addAwardedStates(awardedStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(Awarded, awardedStates, defaultActionSet)
  def addAwardedStates(awardedStates: Set[State], awardActions: Set[Action]): LGCEnvironmentBuilder =
    addStateActions(Awarded, awardedStates, awardActions)

  def addPostTenderAwardCycleStates(postTenderAwardCycleStates: Set[State]): LGCEnvironmentBuilder =
    addStateActions(PostTenderAwardCycle, postTenderAwardCycleStates, defaultActionSet)

  def getStateActions: Set[StateAction] = stateActions
  def getTerminalStates: Set[State] = terminalStates

  def addStateActions(gcState: GCState, states: Set[State], actions: Set[Action]): LGCEnvironmentBuilder =
    LGCEnvironmentBuilder(prefix, stateActions ++ getStateActions(gcState, states, actions))

  private def getStateActions(gcState: GCState, states: Set[State], actions: Set[Action]): Set[StateAction] =
    for { state <- states; action <- actions } yield StateAction(LGCState(gcState, state), action)

  def addStateActions(gcState: GCState,
                      guideOrOffers: Set[Double],
                      statesToMap: Set[State],
                      values: Set[Double]): LGCEnvironmentBuilder =
    LGCEnvironmentBuilder(prefix, stateActions ++ getStateActions(gcState, guideOrOffers, statesToMap, values))

  private def getStateActions(gcState: GCState,
                              guideOrOffers: Set[Double],
                              statesToMap: Set[State],
                              values: Set[Double]): Set[StateAction] = {
    val theActionMapKeys =
      if (guideOrOffers.size <= 1)
        actionMapKeys(statesToMap, Set[Double](Double.NaN))
      else
        actionMapKeys(statesToMap, guideOrOffers)
    val theKeyValueCombinations = keyValueCombinations(theActionMapKeys.size, values.toVector)
    val setOfMaps = (for (combination <- theKeyValueCombinations) yield (theActionMapKeys zip combination).toMap).toSet
    for { map <- setOfMaps } yield StateAction(LGCState(gcState), MapAction(map))
  }

  private def actionMapKeys(statesToMap: Set[State], guideOrOffers: Set[Double]): Set[MapKey] =
    for {
      stateToMap <- statesToMap
      guideOrOffer <- guideOrOffers
    } yield MapKey(stateToMap, guideOrOffer)

  private def keyValueCombinations(numberOfCombinations: Int, values: Vector[Double]): Vector[Vector[Double]] =
    numberOfCombinations match {
      case 0 => Vector[Vector[Double]]()
      case 1 => for (value <- values) yield Vector(value)
      case _ =>
        val newCombinations = for {
          value <- values
          newCombination <- keyValueCombinations(numberOfCombinations - 1, values)
        } yield newCombination :+ value
        newCombinations
    }

}

object LGCEnvironmentBuilder {

  private val NUMBER_OF_GUIDE_VALUES: String = "number_of_guide_values"
  private val DEFAULT_NUMBER_OF_GUIDE_VALUES: Int = 2

  private val NUMBER_OF_TENDER_VALUES: String = "number_of_tender_values"
  private val DEFAULT_NUMBER_OF_TENDER_VALUES: Int = 2

  private val NUMBER_OF_OFFER_VALUES: String = "number_of_offer_values"
  private val DEFAULT_NUMBER_OF_OFFER_VALUES: Int = 2

  private val NUMBER_OF_BID_VALUES: String = "number_of_bid_values"
  private val DEFAULT_NUMBER_OF_BID_VALUES: Int = 2

  def getGuideValues(prefix: String): Set[Double] = Range(0, getNumberOfGuideValues(prefix)).map(_.toDouble).toSet
  def getTenderValues(prefix: String): Set[Double] = Range(0, getNumberOfTenderValues(prefix)).map(_.toDouble).toSet
  def getOfferValues(prefix: String): Set[Double] = Range(0, getNumberOfOfferValues(prefix)).map(_.toDouble).toSet
  def getBidValues(prefix: String): Set[Double] = Range(0, getNumberOfBidValues(prefix)).map(_.toDouble).toSet

  private def booleanActions: Set[Action] = Set(ReturnTrue, ReturnFalse)
  private def getGetGuideActions(prefix: String): Set[Action] = getGuideValues(prefix).map(value => ReturnDouble(value))
  private def getGetOfferActions(prefix: String): Set[Action] = getOfferValues(prefix).map(value => ReturnDouble(value))

  private def getNumberOfGuideValues(prefix: String): Int =
    Configuration.getInt(s"$prefix.$NUMBER_OF_GUIDE_VALUES", DEFAULT_NUMBER_OF_GUIDE_VALUES)
  private def getNumberOfTenderValues(prefix: String): Int =
    Configuration.getInt(s"$prefix.$NUMBER_OF_TENDER_VALUES", DEFAULT_NUMBER_OF_TENDER_VALUES)
  private def getNumberOfOfferValues(prefix: String): Int =
    Configuration.getInt(s"$prefix.$NUMBER_OF_OFFER_VALUES", DEFAULT_NUMBER_OF_OFFER_VALUES)
  private def getNumberOfBidValues(prefix: String): Int =
    Configuration.getInt(s"$prefix.$NUMBER_OF_BID_VALUES", DEFAULT_NUMBER_OF_BID_VALUES)

  def defaultActionSet: Set[Action] = Set[Action](Default)

}
