package ie.nix.lgc

import ie.nix.gc.GCAgent
import ie.nix.gc.message.{BidMessage, TenderMessage}
import ie.nix.lgc.LGCEnvironment.{GCState, LGCState, MapAction, MapKey, SortBidMessages, SortTenderMessages}
import ie.nix.rl.Environment.{Action, NullState, State}
import org.apache.logging.log4j.scala.Logging

trait MessageSorting[T, P] extends Logging {
  lgcAgent: LGCAgent[T, P] with GCAgent[T, P] =>

  override def sortTenderMessages(tenderMessages: Vector[TenderMessage[T]]): Vector[(TenderMessage[T], Double)] =
    if (tenderMessages.nonEmpty) {
      mapToStateForTenderToDouble(tenderMessages.head) match {
        case NullState => defaultSortTenderMessages(tenderMessages)
        case _ => {
          val tenderMessagesMap = actionToMap(nextAction(SortTenderMessages))
          logger trace s"tenderMessagesMap=${tenderMessagesMap}"
          val sortedTenderMessages = tenderMessages
            .map(tenderMessage => {
              val tenderMessageKey = MapKey(mapToStateForTenderToDouble(tenderMessage), tenderMessage.guide)
              val tenderMessageValue = tenderMessagesMap.get(tenderMessageKey) match {
                case Some(tenderMessageValue) => tenderMessageValue
                case None =>
                  logger error s"Error could not find key, $tenderMessageKey, in map, $tenderMessagesMap";
                  Double.NaN
              }
              logger trace s"tenderMessageKey=${tenderMessageKey}, tenderMessageValue=$tenderMessageValue"
              (tenderMessage, tenderMessageValue)
            })
            .sortBy(_._2)
          logger trace s"node=$node, sortedTenderMessages=${sortedTenderMessages.map(tenderMessage =>
            (tenderMessage._1.task, tenderMessage._2))}"
          sortedTenderMessages
        }
      }
    } else {
      Vector[(TenderMessage[T], Double)]()
    }

  override def sortBidMessages(bidMessages: Vector[BidMessage[T, P]]): Vector[(BidMessage[T, P], Double)] =
    if (bidMessages.nonEmpty) {
      mapToStateForBidToDouble(bidMessages.head) match {
        case NullState => defaultSortBidMessages(bidMessages)
        case _ => {
          val bidMessagesMap = actionToMap(nextAction(SortBidMessages))
          bidMessages
            .map(
              bidMessage =>
                (bidMessage,
                 bidMessagesMap.getOrElse(MapKey(mapToStateForBidToDouble(bidMessage), bidMessage.offer),
                                          Double.MaxValue)))
            .sortBy(_._2)
        }
      }
    } else {
      Vector[(BidMessage[T, P], Double)]()
    }

  def nextAction(gcState: GCState): Action =
    rlAgent.getAction(LGCState(gcState))

  def actionToMap(mapAction: Action): Map[MapKey, Double] =
    mapAction match {
      case MapAction(map) => map
      case notAMapAction =>
        logger error s"Expected MapAction but got a ${notAMapAction.getClass.getSimpleName}."
        Map[MapKey, Double]()
    }

  def mapToStateForTenderToDouble(tenderMessage: TenderMessage[T]): State = NullState
  def mapToStateForBidToDouble(bidMessage: BidMessage[T, P]): State = NullState

  protected def defaultSortTenderMessages(tenderMessages: Vector[TenderMessage[T]]): Vector[(TenderMessage[T], Double)]
  protected def defaultSortBidMessages(bidMessages: Vector[BidMessage[T, P]]): Vector[(BidMessage[T, P], Double)]

}
