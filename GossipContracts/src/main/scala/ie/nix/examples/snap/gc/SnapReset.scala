package ie.nix.examples.snap.gc

import ie.nix.peersim.NodeInit
import ie.nix.peersim.Utils.{nodesAs, now, numberOfNodes}

import scala.util.Random

class SnapReset(prefix: String) extends NodeInit(prefix) {

  override def init(): Boolean = {
    super.init()
    resetNodes()
    false
  }

  override def step(): Boolean = {
    resetNodes()
    false
  }

  protected def resetNodes(): Unit = {
    val shuffledCardNumbers = Random.shuffle(Range(0, numberOfNodes).toVector)
    nodesAs[Snap]
      .zip(shuffledCardNumbers)
      .foreach(snapAndSwappableCard => {
        val snap = snapAndSwappableCard._1
        val swappableCard = snapAndSwappableCard._2
        snap.myCard = snap.getIndex
        snap.swapCard(swappableCard)
      })
    logger debug s"[$now]"
  }

}
