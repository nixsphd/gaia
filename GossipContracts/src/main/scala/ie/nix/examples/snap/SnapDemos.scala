package ie.nix.examples.snap

import ie.nix.ecj.PeerSimProblem.bestFitness
import ie.nix.examples.snap.gc.Snap
import ie.nix.peersim.Utils._
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import scala.util.Random

object SnapGCDemo extends App with SnapDemo with Logging {

  val properties = Array[String]("src/main/resources/ie/nix/examples/snap/gc/Snap.properties",
                                 "control.convergence_check=SnapConvergenceCheck",
                                 "control.convergence_check.step=STEP")

  initConfiguration(properties)
  Random.setSeed(seed)
  runSimulator()
  logger info s"numberOfNodes=$numberOfNodes, numberSnapped=$numberSnapped"

}

object SnapEGCDemo extends App with Logging {

  val logDir = "/Users/nicolamcdonnell/Desktop/Gaia/GossipContracts/results/rl/examples/snap/egc"
  val properties: Array[String] = Array[String](
    "-file",
    "src/main/resources/ie/nix/examples/snap/egc/Snap.params",
    "-p",
    s"stat.file=$logDir/master.stat",
    "-p",
    s"stat.child.0.file=$logDir/koza.stat",
    "-p",
    s"stat.child.1.file=$logDir/egc.stat",
    "-p",
    "gp.problem.number_of_runs=5",
    "-p",
    "generations=10",
    "-p",
    "pop.subpop.0.size=20",
//    "-p",
//    "gp.problem.shouldTenderTreeIndex=-1",
//    "-p",
//    "gp.problem.getGuideTreeIndex=-1",
//    "-p",
//    "gp.problem.tenderToDoubleTreeIndex=-1",
//    "-p",
//    "gp.problem.shouldBidTreeIndex=-1",
//    "-p",
//    "gp.problem.getOfferTreeIndex=-1",
//    "-p",
//    "gp.problem.bidToDoubleTreeIndex=-1",
//    "-p",
//    "gp.problem.shouldAwardTreeIndex=-1",
//    "-p",
//    "silent=false",
//    "-p",
//    "gp.problem.logging=console",
  )

  runECJ()
  logger info s"bestFitness=$bestFitness"

  def runECJ(): Unit = ec.Evolve.main(properties)
}

object SnapLGCDemo extends App with SnapDemo with Logging {

  val defaultProperties = Array[String](
    "-file",
    "src/main/resources/ie/nix/gc/GCAgent.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgent.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgentObserver.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgentPolicyCheckpointer.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgentAVFCheckpointer.properties",
    "-file",
    "src/main/resources/ie/nix/examples/snap/gc/Snap.properties",
    "-file",
    "src/main/resources/ie/nix/examples/snap/lgc/LearningSnap.properties",
//    "control.policy_checkpointer.read=false",
//    "control.avf_checkpointer.read=false",
    "control.policy_observer.append=true",
    "control.avf_observer.append=true",
    "control.0-snap_logger.append=true"
  )

//-file src/main/resources/ie/nix/rl/peersim/QLearning.properties
//random.seed=0
//log_dir=results/rl/examples/snap/lgc/0/QLearning
//checkpoint_dir=results/rl/examples/snap/lgc/0/QLearning

  initConfiguration(properties)
  Random.setSeed(seed)
  runSimulator()
  logger info s"numberSnapped=$numberSnapped out of $numberOfNodes"

  protected def properties: Array[String] = defaultProperties ++ args

}

object SnapLearnedDemo extends App with SnapDemo with Logging {

  val defaultProperties = Array[String](
    "-file",
    "src/main/resources/ie/nix/gc/GCAgent.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgent.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgentPolicyCheckpointer.properties",
    "-file",
    "src/main/resources/ie/nix/examples/snap/gc/Snap.properties",
    "-file",
    "src/main/resources/ie/nix/examples/snap/lgc/LearningSnap.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/Learned.properties",
    "control.0-snap_logger.append=true"
  )

  //-file src/main/resources/ie/nix/rl/peersim/QLearning.properties
  //random.seed=0
  //log_dir=results/rl/examples/snap/lgc/0/QLearning
  //checkpoint_dir=results/rl/examples/snap/lgc/0/QLearning

  initConfiguration(properties)
  Random.setSeed(seed)
  runSimulator()
  logger info s"numberSnapped=$numberSnapped out of $numberOfNodes"

  protected def properties: Array[String] = defaultProperties ++ args

}

object SnapLGCDemoWithMessageSorting extends App with SnapDemo with Logging {

  val defaultProperties = Array[String](
    "-file",
    "src/main/resources/ie/nix/gc/GCAgent.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgent.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgentObserver.properties",
    "-file",
    "src/main/resources/ie/nix/lgc/LGCAgentCheckpointer.properties",
    "-file",
    "src/main/resources/ie/nix/examples/snap/gc/Snap.properties",
    "-file",
    "src/main/resources/ie/nix/examples/snap/lgc/LearningSnap.properties",
    "-file",
    "src/main/resources/ie/nix/examples/snap/lgc/MessageSorting.properties"
  )

  //-file src/main/resources/ie/nix/rl/peersim/QLearning.properties
  //random.seed=0
  //log_dir=results/rl/examples/snap/lgc/0/QLearning
  //checkpoint_dir=results/rl/examples/snap/lgc/0/QLearning

  initConfiguration(properties)
  Random.setSeed(seed)
  runSimulator()
  logger info s"numberSnapped=$numberSnapped out of $numberOfNodes"

  protected def properties: Array[String] = defaultProperties ++ args

}

trait SnapDemo {
  protected def seed: Long = Configuration.getLong("random.seed")
  protected def numberSnapped: Int = nodesAs[Snap].count(_.snapped)
  protected def logDir: String = Configuration.getString("log_dir")
}
