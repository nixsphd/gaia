package ie.nix.examples.snap.gc

import ie.nix.gc.GCAgent
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

class SnapGCAgent(prefix: String) extends GCAgent[Int, Int](prefix) with Logging {

  override def shouldTender(node: Node): Boolean =
    !snap(node).snapped

  override def getTask(node: Node): Int = snap(node).swappableCard

  override def getGuide(node: Node, task: Int): Double = 0d

  override def tenderToDouble(tenderMessage: TenderMessage[Int]): Double = {
    val card = tenderMessage.task
    Math.abs(card - snap(tenderMessage.to).myCard).toDouble
  }

  override def shouldBid(tenderMessage: TenderMessage[Int]): Boolean =
    !snap(node).snapped

  override def getProposal(tenderMessage: TenderMessage[Int]): Int =
    snap(node).swappableCard

  override def getOffer(tenderMessage: TenderMessage[Int], proposal: Int): Double = 0d

  override def bidToDouble(bidMessage: BidMessage[Int, Int]): Double = {
    val myCard = snap(node).myCard
    val card = bidMessage.task
    Math.abs(card - myCard).toDouble
  }

  override def shouldAward(bidMessage: BidMessage[Int, Int]): Boolean = true

  override def award(awardMessage: AwardMessage[Int, Int]): Unit =
    snap(node).swapCard(awardMessage.proposal)

  override def awarded(awardMessage: AwardMessage[Int, Int]): Unit =
    snap(node).swapCard(awardMessage.task)

  def snap(node: Node): Snap = node match {
    case snap: Snap => snap
    case _          => exit
  }

}
