package ie.nix.examples.snap.egc

import ie.nix.egc.EGCProblem
import ie.nix.examples.snap.gc.Snap
import ie.nix.peersim.Utils.{nodesAs, numberOfNodes}

class SnapEGCProblem extends EGCProblem {

  def numberOfSnaps: Int = {
    logger debug s"numberOfSnaps=${nodesAs[Snap].count(_.snapped)}"
    nodesAs[Snap].count(_.snapped)
  }

  def numberNotSnapped: Int = {
    logger debug s"numberOfNodes=$numberOfNodes"
    numberOfNodes - numberOfSnaps
  }

  def distance: Int =
    nodesAs[Snap].map(snap => Math.abs(snap.myCard - snap.swappableCard)).sum

  def normalizedDistance: Double = {
    val maxDistance: Int = numberOfNodes * numberOfNodes / 2
    1d * distance / maxDistance
  }

  override def evaluateSimulation: Double = {
    val evaluation: Double = numberNotSnapped.toDouble
    recordStat(gpIndividual, "numberOfSnaps", numberOfSnaps.toDouble)
    recordStat(gpIndividual, "distance", distance.toDouble)
    recordStat(gpIndividual, "evaluation", evaluation)
    logger trace s"evaluate with number of snaps, generation=${state.generation}, " +
      s"gpIndividual=${gpIndividual.hashCode}, evaluation=$evaluation, numberOfSnaps=$numberOfSnaps, distance=$distance"
    evaluation
  }
}
