package ie.nix.examples.snap.gc

import ie.nix.peersim.ConvergenceCheck
import ie.nix.peersim.Utils.nodesAs

class SnapConvergenceCheck(prefix: String) extends ConvergenceCheck(prefix) {

  override def isConverged: Boolean = numberNotSnapped == 0

  def numberNotSnapped: Int = nodesAs[Snap].count(!_.snapped)

}
