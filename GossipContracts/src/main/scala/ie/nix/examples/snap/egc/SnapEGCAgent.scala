package ie.nix.examples.snap.egc

import ie.nix.ecj.TypedData
import ie.nix.ecj.gp.Variable
import ie.nix.egc.EGCAgent
import ie.nix.examples.snap.gc.Snap
import ie.nix.gc.GCAgent
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.peersim.Node
import ie.nix.peersim.Utils.now
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

class SnapEGCAgent(prefix: String) extends GCAgent[Int, Int](prefix) with EGCAgent[Int, Int] with Logging {

  logger debug s"[$now]~prefix=$prefix"

  override def toString: String =
    s"SnapEGCAgent-[${snap(node)}]"

  override def getTask(node: Node): Int = snap(node).swappableCard
  override def getProposal(tenderMessage: TenderMessage[Int]): Int =
    snap(node).swappableCard
  override def award(awardMessage: AwardMessage[Int, Int]): Unit =
    snap(node).swapCard(awardMessage.proposal)
  override def awarded(awardMessage: AwardMessage[Int, Int]): Unit =
    snap(node).swapCard(awardMessage.task)

  override def defaultShouldTender(node: Node): Boolean = !snap(node).snapped
  override def defaultGetGuide(node: Node, task: Int): Double = 0
  override def defaultTenderToDouble(tenderMessage: TenderMessage[Int]): Double = {
    val card = tenderMessage.task
    val myCard = snap(tenderMessage.to).myCard
    Math.abs(card - myCard).toDouble
  }
  override def defaultShouldBid(tenderMessage: TenderMessage[Int]): Boolean =
    !snap(tenderMessage.to).snapped
  override def defaultGetOffer(tenderMessage: TenderMessage[Int], proposal: Int): Double = 0
  override def defaultBidToDouble(bidMessage: BidMessage[Int, Int]): Double = {
    val card = bidMessage.task
    val myCard = snap(node).myCard
    Math.abs(card - myCard).toDouble
  }
  override def defaultShouldAward(bidMessage: BidMessage[Int, Int]): Boolean = true

  def snap(node: Node): Snap = node match {
    case snap: Snap => snap
    case _          => logger error s"Expected node of type Snap, but git ${node.getClass.getName}"; exit
  }

  override def updateNodes(node: Node): Unit = {
    MyCard.value = snap(node).myCard.toDouble
    SwappableCard.value = snap(node).swappableCard.toDouble
  }

  override def updateTaskNode(task: Int): Unit =
    Task.value = task.toDouble

  override def updateProposalNode(proposal: Int): Unit =
    Proposal.value = proposal.toDouble
}

object MyCard { var value = 0d }
object Task { var value = 0d }
object Proposal { var value = 0d }
object SwappableCard { var value = 0d }

class MyCard() extends Variable("myCard", (_: Array[TypedData], result: TypedData) => result.set(MyCard.value))
class SwappableCard()
    extends Variable("swappableCard", (_: Array[TypedData], result: TypedData) => result.set(SwappableCard.value))
class Task() extends Variable("task", (_: Array[TypedData], result: TypedData) => result.set(Task.value))
class Proposal() extends Variable("proposal", (_: Array[TypedData], result: TypedData) => result.set(Proposal.value))
