package ie.nix.examples.snap.gc

import ie.nix.examples.snap.gc.Snap.NOT_SET
import ie.nix.peersim.Node
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging

class Snap(prefix: String, var myCard: Int, var swappableCard: Int) extends Node(prefix) with Logging {

  def this(prefix: String) =
    this(prefix, NOT_SET, NOT_SET)

  logger trace s"myCard=$myCard, swappableCard=$swappableCard"

  override def toString: String = s"Snap[$myCard, $swappableCard]"

  def swapCard(swappableCard: Int): Unit = this.swappableCard = swappableCard

  def snapped: Boolean = myCard == swappableCard

}

object Snap {

  implicit def toSnap: Node => Snap = (node: Node) => as[Snap](node)

  val NOT_SET: Int = -1
}
