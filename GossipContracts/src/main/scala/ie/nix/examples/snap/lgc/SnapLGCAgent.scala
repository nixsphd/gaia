package ie.nix.examples.snap.lgc

import ie.nix.examples.snap.gc.SnapGCAgent
import ie.nix.gc.message.{AwardMessage, BidMessage, TenderMessage}
import ie.nix.lgc.LGCAgent
import ie.nix.peersim.Node
import ie.nix.rl.Environment.State
import org.apache.logging.log4j.scala.Logging

class SnapLGCAgent(prefix: String) extends SnapGCAgent(prefix) with LGCAgent[Int, Int] with Logging {

  override def getTask(node: Node): Int = snap(node).swappableCard

  override def getProposal(tenderMessage: TenderMessage[Int]): Int =
    snap(node).swappableCard

  override def mapToStateForShouldTender(node: Node): State =
    snap(node: Node).snapped match {
      case true  => SnapLGCEnvironment.SnappedState
      case false => SnapLGCEnvironment.NotSnappedState
    }

  override def mapToStateForGetGuide(node: Node, task: Int): State =
    snap(node).snapped match {
      case true  => SnapLGCEnvironment.SnappedState
      case false => SnapLGCEnvironment.NotSnappedState
    }

  override def mapToStateForShouldBid(tenderMessage: TenderMessage[Int]): State =
    snap(node).snapped match {
      case true                                             => SnapLGCEnvironment.SnappedState
      case false if snap(node).myCard == tenderMessage.task => SnapLGCEnvironment.WillSnapState
      case false                                            => SnapLGCEnvironment.WillNotSnapState
    }

  override def mapToStateForGetOffer(tenderMessage: TenderMessage[Int], proposal: Int): State =
    snap(node).snapped match {
      case true                                             => SnapLGCEnvironment.SnappedState
      case false if snap(node).myCard == tenderMessage.task => SnapLGCEnvironment.WillSnapState
      case false                                            => SnapLGCEnvironment.WillNotSnapState
    }

  override def mapToStateForShouldAward(bidMessage: BidMessage[Int, Int]): State =
    snap(node).snapped match {
      case true                                              => SnapLGCEnvironment.SnappedState
      case false if snap(node).myCard == bidMessage.proposal => SnapLGCEnvironment.WillSnapState
      case false                                             => SnapLGCEnvironment.WillNotSnapState
    }

//  override def mapToStateForAward(awardMessage: AwardMessage[Int, Int]): State = snap(node).snapped match {
//    case true  => SnapLGCEnvironment.SnappedState
//    case false => SnapLGCEnvironment.NotSnappedState
//  }

//  override def mapToStateForAwarded(awardMessage: AwardMessage[Int, Int]): State = snap(node).snapped match {
//    case true  => SnapLGCEnvironment.SnappedState
//    case false => SnapLGCEnvironment.NotSnappedState
//  }

//  override def mapToStateForTenderToDouble(tenderMessage: TenderMessage[Int]): State =
//    snap(node).snapped match {
//      case true                                             => SnapLGCEnvironment.SnappedState
//      case false if snap(node).myCard == tenderMessage.task => SnapLGCEnvironment.WillSnapState
//      case false                                            => SnapLGCEnvironment.WillNotSnapState
//    }
//
//  override def mapToStateForBidToDouble(bidMessage: BidMessage[Int, Int]): State =
//    snap(node).snapped match {
//      case true                                              => SnapLGCEnvironment.SnappedState
//      case false if snap(node).myCard == bidMessage.proposal => SnapLGCEnvironment.WillSnapState
//      case false                                             => SnapLGCEnvironment.WillNotSnapState
//    }

  override def defaultShouldTender(node: Node): Boolean = !snap(node).snapped

  override def defaultGetGuide(node: Node, task: Int): Double = 0
//    snap(node).snapped match {
//      case true  => 1
//      case false => 0
//    }

  override def defaultShouldBid(tenderMessage: TenderMessage[Int]): Boolean =
    !snap(tenderMessage.to).snapped

  override def defaultGetOffer(tenderMessage: TenderMessage[Int], proposal: Int): Double = 0
//    snap(node).snapped match {
//      case true  => 1
//      case false => 0
//    }

  override def defaultShouldAward(bidMessage: BidMessage[Int, Int]): Boolean =
    true

  override def defaultAward(awardMessage: AwardMessage[Int, Int]): Unit = {
    snap(node).swapCard(awardMessage.proposal)
    logger debug s"Award complete, ${snap(node)}"
  }

  override def defaultAwarded(awardMessage: AwardMessage[Int, Int]): Unit = {
    snap(node).swapCard(awardMessage.task)
    logger debug s"Awarded complete, ${snap(node)}"
  }

}
