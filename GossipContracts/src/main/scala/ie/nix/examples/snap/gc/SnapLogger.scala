package ie.nix.examples.snap.gc

import ie.nix.peersim.{Logger, Times}
import ie.nix.peersim.Utils.{nodesAs, now}
import org.apache.logging.log4j.scala.Logging

class SnapLogger(prefix: String) extends Logger(prefix) with Times with Logging {

  override def observe(): Unit = logger info s"[$now]~numberOfSnaps=$numberOfSnaps"

  override def headers: String = "NumberOfSnaps"
  override def log(): Unit = writeRow(s"$numberOfSnaps")

  def numberOfSnaps: Int = nodesAs[Snap].count(_.snapped)

}
