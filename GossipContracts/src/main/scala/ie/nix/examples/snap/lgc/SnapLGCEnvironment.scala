package ie.nix.examples.snap.lgc

import ie.nix.examples.snap.lgc.SnapLGCEnvironment._
import ie.nix.lgc.LGCEnvironment.{GCState, LGCState}
import ie.nix.lgc.{LGCEnvironment, LGCEnvironmentBuilder}
import ie.nix.rl.Environment.State
import ie.nix.rl.Rewards
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.Transitions.Transition

class SnapLGCEnvironment(prefix: String)
    extends LGCEnvironment(
      LGCEnvironmentBuilder(prefix)
        .addShouldTenderStates(snappedOrNotSnappedStates)
        .addGetGuideStates(snappedOrNotSnappedStates)
//        .addTenderToDoubleStates(snappedOrWillSnapOrWontSnapStates)
        .addShouldBidStates(snappedOrWillSnapOrWontSnapStates)
        .addGetOfferStates(snappedOrWillSnapOrWontSnapStates)
//        .addBidToDoubleStates(snappedOrWillSnapOrWontSnapStates)
        .addShouldAwardStates(snappedOrWillSnapOrWontSnapStates)
//        .addAwardStates(snappedOrNotSnappedStates)
//        .addAwardedStates(snappedOrNotSnappedStates)
    )
    with Rewards {

  logger debug s"number of stateActions=${getStateActions.size}"
  logger debug s"stateActions=${getStateActions.sortBy(_.toString()).mkString("\n", "\n", "\n")}"

  override def toString: String = s"SnapLGCEnvironment"

  override def getReward(transition: Transition): Reward =
    transition match {
      case Transition(_, LGCState(_, SnappedState, _)) => {
        logger debug s"Rewarding ${transition.stateAction}"
        1d
      }
      case _ => -0.01d
    }

}

object SnapLGCEnvironment {

  case class SnapGCState(gcState: GCState, snapState: SnapState) extends State

  sealed trait SnapState extends State
  object SnappedState extends SnapState { override def toString: String = "Snapped" }
  object NotSnappedState extends SnapState { override def toString: String = "NotSnapped" }
  object WillSnapState extends SnapState { override def toString: String = "WillSnap" }
  object WillNotSnapState extends SnapState { override def toString: String = "WillNotSnap" }

  def snappedOrNotSnappedStates: Set[State] = Set[State](SnappedState, NotSnappedState)
  def snappedOrWillSnapOrWontSnapStates: Set[State] = Set[State](SnappedState, WillSnapState, WillNotSnapState)

}
