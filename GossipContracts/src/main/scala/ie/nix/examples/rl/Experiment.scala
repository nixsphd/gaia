package ie.nix.examples.rl

import ie.nix.peersim.Node
import ie.nix.peersim.Utils.{initConfiguration, node, protocolAs, protocolsAs, runSimulator}
import ie.nix.rl.peersim.LearningAgent
import ie.nix.rl.policy.DynamicProgramming
import ie.nix.rl.policy.vf.ActionValueFunction.asCSV
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import java.nio.file.{FileSystems, Files, StandardOpenOption}
import scala.util.Random

object Experiment extends App with Logging {

//  -file src/main/resources/ie/nix/examples/rl/Example.properties
//  -file src/main/resources/ie/nix/examples/rl/Example_s_8.properties
//  -file src/main/resources/ie/nix/examples/rl/QLearning.properties
//  random.seed=0
//  log_dir=results/rl/examples/example/0/QLearning

  protected val rlAgent: String = "rl_agent"
  protected val rlAgentPrefix: String = s"protocol.$rlAgent"

//  silent()
  initConfiguration(properties ++ args)
  Random.setSeed(seed)
  runSimulator()
  logEnvironments()

  protected def properties: Array[String] =
    Array[String](
      "src/main/resources/ie/nix/examples/rl/Example.properties",
      "src/main/resources/ie/nix/rl/peersim/ErrorObservers.properties",
      "src/main/resources/ie/nix/rl/peersim/Checkpointers.properties",
//      "control.policy_observer.append=false",
//      "control.avf_observer.append=false",
//      "control.policy_checkpointer.read=false",
//      "control.avf_checkpointer.read=false",
    )

  protected def seed: Long = Configuration.getLong("random.seed")

  protected def logDir: String = Configuration.getString("log_dir")

  protected def discount: Double = Configuration.getDouble(s"$rlAgentPrefix.discount")

  def agentProtocolID: Int = Configuration.lookupPid(rlAgent)

  def agents: Vector[LearningAgent] = protocolsAs[LearningAgent](agentProtocolID)

  def theEnvironment: Environment = anAgent.environmentAs[Environment]

  def anAgent: LearningAgent = protocolAs[LearningAgent](aNode, agentProtocolID)

  def aNode: Node = node(index = 0)

  def logEnvironments(): Unit = {
    logger debug s"step_size=${Configuration.getDouble(s"$rlAgentPrefix.step_size")}"
    logger debug s"eligibility_decay=${Configuration.getDouble(s"$rlAgentPrefix.eligibility_decay")}"
    logger debug s"innovation_step_size=${Configuration.getDouble(s"$rlAgentPrefix.innovation_step_size")}"
    logger debug s"consensus_step_size=${Configuration.getDouble(s"$rlAgentPrefix.consensus_step_size")}"

    val environmentFile = FileSystems.getDefault.getPath(logDir).resolve("Environment.txt")
    logger trace s"Logging environment to $environmentFile"
    Files.write(
      environmentFile,
      theEnvironment.toString.getBytes,
      StandardOpenOption.CREATE,
      StandardOpenOption.TRUNCATE_EXISTING
    )
    val dpAVF = DynamicProgramming(discount)
      .evaluateEnvironment(theEnvironment, 0.001, 1000)
      .actionValueFunction
    val dpAVFFile = FileSystems.getDefault.getPath(logDir).resolve("DP_AVF.csv")
    logger info s"Logging environment dynamic programming AVF to $dpAVFFile"
    Files.write(
      dpAVFFile,
      asCSV(dpAVF).getBytes,
      StandardOpenOption.CREATE,
      StandardOpenOption.TRUNCATE_EXISTING
    )

    val transitionProbabilitiesFile = FileSystems.getDefault.getPath(logDir).resolve("TransitionProbabilities.csv")
    logger info s"Logging transition probabilities to $transitionProbabilitiesFile"
    Files.write(
      transitionProbabilitiesFile,
      theEnvironment.transitionProbabilitiesAsCSV.getBytes,
      StandardOpenOption.CREATE,
      StandardOpenOption.TRUNCATE_EXISTING
    )

    val rewardsFile = FileSystems.getDefault.getPath(logDir).resolve("Rewards.csv")
    logger info s"Logging rewards to $rewardsFile"
    Files.write(
      rewardsFile,
      theEnvironment.stateRewardsAsCSV.getBytes,
      StandardOpenOption.CREATE,
      StandardOpenOption.TRUNCATE_EXISTING
    )
  }
}
