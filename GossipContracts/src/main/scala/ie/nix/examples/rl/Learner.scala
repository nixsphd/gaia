package ie.nix.examples.rl

import ie.nix.examples.rl.Environment.ExperimentState
import ie.nix.peersim.{EventHandler, Node, Protocol}
import ie.nix.rl.Environment.{State, StateAction}
import ie.nix.rl.peersim.Learning

class Learner(prefix: String) extends Protocol(prefix) with Learning {

  private var state: State = ExperimentState(1)

  override def nextCycle(node: Node): Unit = {
    val action = rlAgent.getAction(state)
    val stateAction = StateAction(state, action)
    val nextState = rlAgent.environmentAs[Environment].nextState(stateAction)
    logger debug s"node=$node, stateAction=$stateAction, nextState=$nextState"
    state = nextState
  }

  override def processEvent(node: Node, message: EventHandler.Message): Unit = {}
}
