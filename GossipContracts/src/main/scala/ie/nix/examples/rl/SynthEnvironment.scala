package ie.nix.examples.rl

import ie.nix.examples.rl.Environment.{ExperimentAction, ExperimentState, getNumberOfStates}
import ie.nix.rl.Environment.{State, StateAction}
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.Transitions.Transition
import ie.nix.rl.peersim.{RewardsMap, TransitionsMap}

class SynthEnvironment(prefix: String) extends Environment(prefix) with TransitionsMap with RewardsMap {

  val numberOfStates = getNumberOfStates(prefix)

  override protected val _transitionProbabilities: Map[Transition, Double] =
    experimentStates
      .map(mapToTransitionProbabilities)
      .reduce(_ ++ _)

  protected def mapToTransitionProbabilities(state: ExperimentState): Map[Transition, Reward] = {
    val nextIndex = if (state.index != numberOfStates) state.index + 1 else 1
    val previousIndex = if (state.index != 1) state.index - 1 else numberOfStates
    Map[Transition, Reward]() +
      (Transition(StateAction(state, ExperimentAction('A')), ExperimentState(nextIndex)) -> 1d) +
      (Transition(StateAction(state, ExperimentAction('B')), ExperimentState(previousIndex)) -> 1d)
  }

  override protected val stateRewards: Map[State, Reward] =
    Map[State, Reward]() + (ExperimentState(numberOfStates) -> 5)

}
