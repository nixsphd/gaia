package ie.nix.examples.rl

import ie.nix.examples.rl.Environment.{
  ExperimentState,
  getGreedyStateRewards,
  getGreedyTransitionProbabilities,
  getStateActions,
  getTerminalStates
}
import ie.nix.rl.Environment.{Action, State, StateAction}
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.Transitions.Transition
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration
import ie.nix.rl.peersim
import ie.nix.rl.peersim.RewardsMap.GetStateRewards
import ie.nix.rl.peersim.TransitionsMap.GetTransitionProbabilities
import ie.nix.rl.peersim.{RewardsMap, TransitionsMap}

import scala.util.Random

class Environment(prefix: String)
    extends peersim.Environment(getStateActions(prefix), getTerminalStates(prefix))
    with TransitionsMap
    with RewardsMap
    with Logging {

  val experimentStates: Vector[ExperimentState] =
    getStates.collect { case experimentState: ExperimentState => experimentState }

  override protected val _transitionProbabilities: Map[Transition, Double] =
    getGreedyTransitionProbabilities(prefix, getStateActions.toSet)

  override protected val stateRewards: Map[State, Reward] =
    getGreedyStateRewards(prefix, getStates.toSet)

  logger debug s"number of stateActions=${getStateActions.size}"
  logger debug s"stateActions=${getStateActions.sortBy(_.toString()).mkString("\n", "\n", "\n")}"

  override def toString: String = {
    def transitionProbabilitiesAsString =
      _transitionProbabilities.toVector
        .sortBy(_._1.toString)
        .mkString(start = "Transitions:\n\t", sep = "\n\t", end = "\n")
    def stateRewardsAsString =
      stateRewards.toVector
        .sortBy(_._1.toString)
        .mkString(start = "Rewards:\n\t", sep = "\n\t", end = "\n")
    s"ExperimentEnvironment $stateRewardsAsString $transitionProbabilitiesAsString"
  }

  def transitionProbabilitiesAsCSV: String =
    _transitionProbabilities.toVector
      .sortBy(_._1.toString)
      .map(tp => List(tp._1.stateAction.state, tp._1.stateAction.action, tp._1.finalState, tp._2).mkString(","))
      .mkString(start = "State,Action,FinalState,Transition Probability\n", sep = "\n", end = "\n")

  def stateRewardsAsCSV: String =
    stateRewards.toVector
      .sortBy(_._1.toString)
      .map(r => List(r._1, r._2).mkString(","))
      .mkString(sep = "\n")

}

object Environment extends Logging {

  type GetStateActions = (String) => Set[StateAction]
  type GetStates = (String) => Set[State]
  type GetActions = (String) => Set[Action]

  case class ExperimentState(index: Int) extends State {
    override def toString: String = s"State$index"
  }

  val MAX_NUMBER_OF_STATES = 256
  val MAX_ACTION = 'D'

  val allStates: Vector[ExperimentState] =
    (for (index <- 1 to MAX_NUMBER_OF_STATES) yield ExperimentState(index)).toVector

  case class ExperimentAction(index: Char) extends Action {
    override def toString: String = s"Action$index"
  }

  val allActions: Vector[ExperimentAction] =
    (for (index <- 'A' to MAX_ACTION) yield ExperimentAction(index)).toVector

  val getStateActions: GetStateActions = prefix =>
    (for {
      state <- allStates.slice(0, getNumberOfStates(prefix))
      action <- allActions.slice(0, getNumberOfActions(prefix))
    } yield StateAction(state, action)).toSet

  val getTerminalStates: GetStates = _ => Set[State]()

  val getUniformRandomTransitionProbabilities: GetTransitionProbabilities = (_, stateActions) => {
    val states = stateActions.map(_.state).toVector.sortBy(_.toString)
    (for (stateAction <- stateActions.toVector;
          statesAndNormalisedProbabilities = states zip getNormalisedRandomProbabilities(states.size);
          (state, probability) <- statesAndNormalisedProbabilities;
          transition = Transition(stateAction, state))
      yield transition -> probability).toMap
  }

  def getNumberOfStates(prefix: String): Int =
    Configuration.getInt(s"$prefix.number_of_states")

  def getNumberOfActions(prefix: String): Int =
    Configuration.getInt(s"$prefix.number_of_actions")

  protected def getNormalisedRandomProbabilities(number: Int): Vector[Double] = {
    require(number >= 0)
    val probabilities = Vector.tabulate[Double](number)(_ => Random.nextDouble())
    val sumOfProbabilities = probabilities.sum
    probabilities.map(_ / sumOfProbabilities)
  }

  val getGreedyTransitionProbabilities: GetTransitionProbabilities = (_, stateActions) => {
    val states = stateActions.map(_.state).toVector.sortBy(_.toString)
    (for (stateAction <- stateActions.toVector;
          statesAndNormalisedProbabilities = states zip getNormalisedGreedyProbabilities(states.size);
          (state, probability) <- statesAndNormalisedProbabilities;
          transition = Transition(stateAction, state))
      yield transition -> probability).toMap
  }

  protected def getNormalisedGreedyProbabilities(number: Int): Vector[Double] = {
    require(number >= 0)
    val randomIndex = Random.nextInt(number)
    val probabilities = Vector.tabulate[Double](number)(
      index =>
        if (index == randomIndex)
          Random.nextDouble() * 50 //TODO make this a param.
        else
          Random.nextDouble())
    val sumOfProbabilities = probabilities.sum
    probabilities.map(_ / sumOfProbabilities)
  }

  val getUniformRandomStateRewards: GetStateRewards = (_, states) =>
    states.toSeq
      .sortBy(_.toString)
      .map(_ -> ((Random.nextDouble() * 2) - 1))
      .toMap

  val getGreedyStateRewards: GetStateRewards = (_, states) => {
    val randomState: State = states.toVector(Random.nextInt(states.size))
    states.toSeq
      .sortBy(_.toString)
      .map(
        state =>
          if (state == randomState)
            state -> 1d // TODO Make this a param
          else
            state -> 0d // TODO Make this a param
      )
      .toMap
  }

}
