package ie.nix.examples.balancer

import ie.nix.examples.balancer.BalancerPolicyWithErrorObserver.getIdealPolicy
import ie.nix.examples.balancer.BalancerProtocol.{AcceptMessage, OfferMessage}
import ie.nix.examples.balancer.SmartBalancerEnvironment.{getStateActions, getStateRewards}
import ie.nix.examples.balancer.SmartBalancerProtocol._
import ie.nix.examples.rl.Environment.GetStateActions
import ie.nix.peersim.Utils.{initConfiguration, now, runSimulator, silent}
import ie.nix.peersim.automata.{Automata, AutomataProtocol, AutomataWithMeanValue, MeanValue}
import ie.nix.peersim.{EventHandler, Node}
import ie.nix.rl.Environment.{Action, State, StateAction}
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.peersim.Environment.emptyStateSet
import ie.nix.rl.peersim.ProtocolLearning.{NextCycle, ProcessEvent, ProtocolState, UnknownState}
import ie.nix.rl.peersim.RewardsMap.GetStateRewards
import ie.nix.rl.peersim.observer.AbstractPolicyWithErrorObserver
import ie.nix.rl.peersim.{Environment, ProtocolLearning, RewardsMap}
import ie.nix.rl.policy.DeterministicPolicy
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import scala.util.Random

object SmartBalancerProblem extends App with Logging {

  protected def properties: Array[String] =
    Array[String](
      "-file",
      "../Util/src/main/resources/ie/nix/peersim/automata/Automata.properties",
      "-file",
      "src/main/resources/ie/nix/examples/balancer/Balancer.properties",
      "-file",
      "src/main/resources/ie/nix/examples/balancer/SmartBalancer.properties",
      "log_dir=logs"
    ) ++ args

  silent()
  initConfiguration(properties)
  Random.setSeed(seed)
  runSimulator()

  def seed: Long = Configuration.getLong("random.seed")

}

class SmartBalancerProtocol(prefix: String) extends BalancerProtocol(prefix) with ProtocolLearning with Logging {

  import AutomataProtocol.Message

  /*
   * Next Cycle
   */
  override def nextCycle(automata: AutomataWithMeanValue): Unit = {
    logger trace s"[$now] automata=$automata, maxValue=${Automata.maxValue}, meanValue=${MeanValue.meanValue}"
    nextCycleWithLearning(node)
  }

  override def getNextCycleState(node: Node): State = node match {
    case automata: Automata => BalancerState(NextCycle, automata.getValue)
    case _                  => logger error s"Unexpected node, $node, expected Automata"; BadBalancerState
  }

  override def takeNextCycleAction(node: Node, action: Action): Unit = (node, action) match {
    case (_: Automata, Offer)     => sendMessage(OfferMessage(automata, randomNeighbour))
    case (_: Automata, DoNothing) => // Do Nothing
    case (_: Automata, unexpectedAction) =>
      logger error s"Unexpected action, $unexpectedAction, expected Offer or DoNothing"
    case _ => logger error s"Unexpected node, $node, expected Automata"
  }

  /*
   * Process Event
   */
  override def processEvent(automata: AutomataWithMeanValue, message: Message): Unit = {
    logger trace s"[$now] $this $automata received message $message"
    message match {
      case _: AcceptMessage =>
        decrementAutomataValue()
        logger trace s"[$now] $automata decrementAutomataValue"
      case _ => processEventWithLearning(node, message)
    }
  }

  override def getProcessEventState(node: Node, message: EventHandler.Message): BalancerState =
    node match {
      case automata: Automata => BalancerState(ProcessEvent, automata.getValue)
      case _                  => logger error s"Unexpected node, $node, expected Automata"; BadBalancerState
    }

  override def takeProcessEventAction(node: Node, message: EventHandler.Message, action: Action): Unit = {
    logger trace s"[$now] $automata action $action"
    (node, action) match {
      case (automata: Automata, Accept) =>
        incrementAutomataValue()
        logger trace s"[$now] $automata incrementedAutomataValue"
        sendMessage(AcceptMessage(automata, message.from))
      case (_: Automata, Reject) => // Do Nothing
      case (_: Automata, _)      => logger error s"Unexpected action, $action, expected Accept or Reject"
      case _                     => logger error s"Unexpected node, $node, expected Automata"
    }
  }

}

object SmartBalancerProtocol {

  case class BalancerState(protocolState: ProtocolState, automataValue: Int) extends State {
    override def toString: String = s"$protocolState-$automataValue"
    override def isTerminal: Boolean = false
  }

  object BadBalancerState extends BalancerState(UnknownState, -1) {
    override def toString: String = "BadBalancerState"
  }

  case object Offer extends Action {
    override def toString: String = "Offer"
  }

  case object DoNothing extends Action {
    override def toString: String = "DoNothing"
  }

  case object Accept extends Action {
    override def toString: String = "Accept"
  }

  case object Reject extends Action {
    override def toString: String = "Reject"
  }

}

class SmartBalancerEnvironment(prefix: String)
    extends Environment(getStateActions(prefix), emptyStateSet)
    with RewardsMap
    with Logging {

  logger trace s"prefix=$prefix"

  val reward: Double = SmartBalancerEnvironment.getReward(prefix)
  val penalty: Double = SmartBalancerEnvironment.getPenalty(prefix)

  val meanValue: Int = MeanValue.meanValue

  override protected val stateRewards: Map[State, Reward] =
    getStateRewards(prefix, getStates.toSet)

}

object SmartBalancerEnvironment extends Logging {

  val REWARD: String = "reward"
  val DEFAULT_REWARD: Double = 1d
  val PENALTY: String = "penalty"
  val DEFAULT_PENALTY: Double = 0d

  val maxValue: Int = Automata.maxValue
  val meanValue: Int = MeanValue.meanValue

  val getStateActions: GetStateActions = _ => {
    val stateActions = Set[StateAction]() +
      StateAction(BalancerState(NextCycle, 0), DoNothing) ++
      (for {
        value <- 1 to maxValue
        action <- List(Offer, DoNothing)
      } yield {
        StateAction(BalancerState(NextCycle, value), action)
      }) ++
      (for {
        value <- 0 until maxValue
        action <- List(Accept, Reject)
      } yield {
        StateAction(BalancerState(ProcessEvent, value), action)
      }) +
      StateAction(BalancerState(ProcessEvent, maxValue), Reject)
    stateActions
  }

  val getStateRewards: GetStateRewards = (prefix, states) => {
    val stateRewards = Map[State, Reward]() ++
      (for {
        state <- states
        balancerState = toBalancerState(state)
        rewardOrPenalty = if (balancerState.automataValue == meanValue)
          getReward(prefix)
        else getPenalty(prefix)
      } yield balancerState -> rewardOrPenalty).toMap
    stateRewards
  }

  def getReward(prefix: String): Double = Configuration.getDouble(s"$prefix.$REWARD", DEFAULT_REWARD)
  def getPenalty(prefix: String): Double = Configuration.getDouble(s"$prefix.$PENALTY", DEFAULT_PENALTY)

  def toBalancerState(state: State): BalancerState = state match {
    case balancerState: BalancerState => balancerState
    case notABalancerState =>
      logger error s"Expected a balancerState, " +
        s"but got a ${notABalancerState.getClass.getSimpleName}"; BadBalancerState
  }

}

class BalancerPolicyWithErrorObserver(prefix: String) extends AbstractPolicyWithErrorObserver(prefix) {

  override def initIdealPolicy(): DeterministicPolicy = {
    val smartBalancerEnvironment: SmartBalancerEnvironment = as[SmartBalancerEnvironment](environment)
    getIdealPolicy(smartBalancerEnvironment)
  }

}

object BalancerPolicyWithErrorObserver extends Logging {

  def getIdealPolicy(smartBalancerEnvironment: SmartBalancerEnvironment): DeterministicPolicy = {
    smartBalancerEnvironment.getNonTerminalStates.foldLeft(DeterministicPolicy(smartBalancerEnvironment))(
      (policy, state) => {
        state match {
          case BalancerState(NextCycle, automataValue) if (automataValue <= smartBalancerEnvironment.meanValue) =>
            policy updateAction StateAction(state, DoNothing)
          case BalancerState(NextCycle, automataValue) if (automataValue > smartBalancerEnvironment.meanValue) =>
            policy updateAction StateAction(state, Offer)
          case BalancerState(ProcessEvent, automataValue) if (automataValue < smartBalancerEnvironment.meanValue) =>
            policy updateAction StateAction(state, Accept)
          case BalancerState(ProcessEvent, automataValue) if (automataValue >= smartBalancerEnvironment.meanValue) =>
            policy updateAction StateAction(state, Reject)
          case _ =>
            logger error s"Expected $state to be of type AutomataState"
            policy
        }
      })
  }

}
