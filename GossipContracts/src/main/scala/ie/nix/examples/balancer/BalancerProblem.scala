package ie.nix.examples.balancer

import ie.nix.examples.balancer.BalancerProtocol.{AcceptMessage, OfferMessage}
import ie.nix.peersim.Utils.{nodes, nodesAs, now, numberOfNodes}
import ie.nix.peersim.automata.{Automata, AutomataProtocol, AutomataWithMeanValue, MeanValue}
import org.apache.logging.log4j.scala.Logging

object BalancerProblem extends App with Logging {

  val properties = Array[String](
    "-file",
    "../Util/src/main/resources/ie/nix/peersim/automata/Automata.properties",
    "-file",
    "src/main/resources/ie/nix/examples/balancer/Balancer.properties",
  )

  peersim.Simulator.main(properties)

  val maxValue = Automata.maxValue
  val meanValue = MeanValue.meanValue
  val realMeanValue = nodesAs[AutomataWithMeanValue].map(_.getValue).sum / numberOfNodes
  val numberCorrect = nodesAs[Automata].count(_.getValue == meanValue)
  logger info s"[$now] numberOfNodes=$numberOfNodes, maxValue=$maxValue, meanValue=$meanValue, realMeanValue=$realMeanValue, numberCorrect=$numberCorrect"
  logger info nodes.mkString(",")
}

class BalancerProtocol(prefix: String) extends AutomataProtocol(prefix) with Logging {

  import AutomataProtocol.Message

  override def nextCycle(node: Automata): Unit = node match {
    case automata: AutomataWithMeanValue => nextCycle(automata)
    case _                               =>
  }

  def nextCycle(automata: AutomataWithMeanValue): Unit = {
    logger trace s"[$now] automata=$automata, maxValue=${Automata.maxValue}, meanValue=${MeanValue.meanValue}"
    if (automata.getValue > automata.getMeanValue) {
      val randomNeighbour = randomNeighbourAs[Automata]
      logger trace s"[${now}] $automata sending OfferMessage messages to $randomNeighbour"
      sendMessage(OfferMessage(automata, randomNeighbour))
    }
  }

  override def processEvent(node: Automata, message: Message): Unit =
    node match {
      case automata: AutomataWithMeanValue => processEvent(automata, message)
      case notAnAutomataWithMeanValue =>
        logger error s"Expected AutomataWithMeanValue, but got ${notAnAutomataWithMeanValue.getClass.getSimpleName}"
    }

  def processEvent(automata: AutomataWithMeanValue, message: Message): Unit = {
    logger trace s"[$now] $this $automata received message $message"
    message match {
      case offerMessage: OfferMessage if (automata.getValue < automata.getMeanValue) => {
        logger trace s"[$now] $this $offerMessage accepted by $automata => incrementAutomataValue"
        incrementAutomataValue()
        logger trace s"[$now] $this AcceptMessage ${automata.getValue}"
        sendMessage(AcceptMessage(automata, offerMessage.from))
      }
      case offerMessage: OfferMessage => {
        logger trace s"[$now] $this $offerMessage ignored by $automata"
      }
      case _: AcceptMessage => {
        logger trace s"[$now] $this AcceptMessage => decrementAutomataValue"
        decrementAutomataValue()
      }
    }
  }
}

object BalancerProtocol {

  import AutomataProtocol.Message

  case class OfferMessage(override val from: Automata, override val to: Automata) extends Message(from, to) {
    override def toString: String = "OfferMessage"
  }

  case class AcceptMessage(override val from: Automata, override val to: Automata) extends Message(from, to) {
    override def toString: String = "AcceptMessage"
  }
}
