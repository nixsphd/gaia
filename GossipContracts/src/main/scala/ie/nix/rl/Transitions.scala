package ie.nix.rl

import ie.nix.rl.Environment.{State, StateAction}
import ie.nix.rl.Transitions.Transition

trait Transitions {
  environment: Environment =>

  def nextState(stateAction: StateAction): State

  def transitionProbabilities(stateAction: StateAction): Vector[(Transition, Double)]

}

object Transitions {
  case class Transition(stateAction: StateAction, finalState: State) {
    def isTerminal: Boolean = stateAction.isTerminal || finalState.isTerminal
  }
}
