package ie.nix.rl.peersim

import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.{EventHandler, Node}
import ie.nix.rl.Environment.{Action, State}
import ie.nix.rl.peersim.Agent.{AgentPolicy, getEnvironment, getPolicy}
import ie.nix.rl.peersim.LearningAgent.logger
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.Policy.Determinism
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import scala.sys.exit

// Todo - Move the Protocol to a trait and move to NetworkedLearningAgent?
class Agent(prefix: String) extends EventHandler(prefix) with Logging {

  implicit val environment: Environment = getEnvironment(prefix)

  private var _policy: AgentPolicy = initPolicy()

  override def clone: AnyRef = {
    val clonedAgent: Agent = as[Agent](super.clone)
    clonedAgent._policy = initPolicy()
    clonedAgent
  }

  override def processEvent(node: Node, message: Message): Unit = {}

  def reset(): Unit = resetPolicy()

  def getAction(state: State): Action = policy.getAction(state)

  def environmentAs[T]: T = as[T](environment)

  def policy: AgentPolicy = this._policy

  def updatePolicy(policy: AgentPolicy): Unit = _policy = policy

  protected def initPolicy(): AgentPolicy = getPolicy(prefix)
  protected def resetPolicy(): Unit = this._policy = initPolicy()

}

object Agent {

  type AgentPolicy = Policy with Determinism

  val ENVIRONMENT: String = "environment"
  val POLICY: String = "policy"

  def getEnvironment(prefix: String): Environment = {
    val maybeEnvironment = Configuration.getInstance(s"$prefix.$ENVIRONMENT")
    maybeEnvironment match {
      case environment: Environment => environment
      case notAnEnvironment =>
        logger error s"Error, expected environment of tyoe Environment," +
          s" but got ${notAnEnvironment.getClass.getSimpleName}"
        exit
    }
  }

  def getPolicy(prefix: String): AgentPolicy = {
    val policy = Configuration.getInstance(s"$prefix.$POLICY")
    as[AgentPolicy](policy)
  }

}
