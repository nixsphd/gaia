package ie.nix.rl.peersim

import ie.nix.peersim.EventHandler.Message
import ie.nix.peersim.Utils.now
import ie.nix.peersim.{Node, Protocol}
import ie.nix.rl.Environment.{Action, State}
import org.apache.logging.log4j.scala.Logging

trait ProtocolLearning extends Learning with Logging {
  protocol: Protocol =>

  logger trace s"[$now] protocolID=$protocolID"

  def nextCycleWithLearning(node: Node): Unit = {
    logger trace s"[$now] $this $node"

    val state = getNextCycleState(node)
    logger trace s"[$now] $this $node state=$state"

    val action = rlAgent.getAction(state)
    logger trace s"[$now] $this $node state=$state, action=$action"

    takeNextCycleAction(node, action)
  }
  def getNextCycleState(node: Node): State
  def takeNextCycleAction(node: Node, action: Action): Unit

  def processEventWithLearning(node: Node, message: Message): Unit = {
    logger trace s"[$now] $this $node received message $message"

    val state = getProcessEventState(node, message)
    logger trace s"[$now] $this $node state=$state"

    val action = rlAgent.getAction(state)
    logger debug s"[$now] $this $node state=$state, action=$action"

    takeProcessEventAction(node, message, action)
  }
  def getProcessEventState(node: Node, message: Message): State
  def takeProcessEventAction(node: Node, message: Message, action: Action): Unit

}

object ProtocolLearning extends Logging {

  sealed trait ProtocolState
  object NextCycle extends ProtocolState {
    override def toString: String = "NextCycle"
  }
  object ProcessEvent extends ProtocolState {
    override def toString: String = "ProcessEvent"
  }
  object UnknownState extends ProtocolState {
    override def toString: String = "UnknownState"
  }

}
