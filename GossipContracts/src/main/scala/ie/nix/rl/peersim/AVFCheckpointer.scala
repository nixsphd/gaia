package ie.nix.rl.peersim

import ie.nix.peersim.Checkpointing
import ie.nix.peersim.Utils.protocolsAs
import ie.nix.rl.peersim.Learning.getAgentProtocolID
import ie.nix.rl.policy.vf.AbstractValueFunction
import org.apache.logging.log4j.scala.Logging

class AVFCheckpointer(val prefix: String) extends Checkpointing[Vector[AbstractValueFunction[_]]] with Logging {

  def updateObjectFromCheckpoint(avfs: Vector[AbstractValueFunction[_]]): Unit =
    learningAgents
      .zip(avfs)
      .foreach(agentAndAVFs => {
        val (agent, avf) = agentAndAVFs
        agent.updateActionValueFunction(avf)
      })

  def objectToCheckpoint(): Vector[AbstractValueFunction[_]] = {
    learningAgents.map(_.actionValueFunction)
  }

  def learningAgents: Vector[LearningAgent] = protocolsAs[LearningAgent](agentProtocolID)

  def agentProtocolID: Int = getAgentProtocolID(prefix)

}
