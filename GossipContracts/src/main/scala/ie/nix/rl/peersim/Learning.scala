package ie.nix.rl.peersim

import ie.nix.peersim.Protocol
import ie.nix.peersim.Utils.protocolAs
import ie.nix.rl.peersim.Learning.getAgentProtocolID
import peersim.config.Configuration

trait Learning {
  protocol: Protocol =>

  val rlAgentProtocolID = getAgentProtocolID(prefix)

  def rlAgent: Agent = protocolAs[Agent](node, rlAgentProtocolID)

}

object Learning {
  private val AGENT_PROTOCOL = "rl_agent"
  def getAgentProtocolID(prefix: String): Int = Configuration.lookupPid(AGENT_PROTOCOL)
}
