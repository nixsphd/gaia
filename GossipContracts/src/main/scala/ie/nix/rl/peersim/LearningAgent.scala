package ie.nix.rl.peersim

import ie.nix.rl.Environment.{Action, NullStateAction, State, StateAction}
import ie.nix.rl.Rewards
import ie.nix.rl.peersim.Agent.{AgentPolicy, ENVIRONMENT}
import ie.nix.rl.peersim.LearningAgent.getDiscount
import ie.nix.rl.policy.PolicyImprover
import ie.nix.rl.policy.vf.AbstractValueFunction
import ie.nix.util.Utils.as
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import scala.sys.exit

abstract class LearningAgent(prefix: String) extends Agent(prefix) {

  val discount: Double = getDiscount(prefix: String)

  private var _stateAction: StateAction = initStateAction()
  private var _actionValueFunction: AbstractValueFunction[_] = initActionValueFunction()

  override def clone: AnyRef = {
    val clonedAgent: LearningAgent = as[LearningAgent](super.clone)
    clonedAgent._stateAction = initStateAction()
    clonedAgent._actionValueFunction = initActionValueFunction()
    clonedAgent
  }

  override def toString: String = s"LearningAgent[$getIndex]"

  override def reset(): Unit = {
    super.reset()
    resetStateAction()
    resetActionValueFunction()
  }

  override def getAction(state: State): Action = {
    val action = policy.getAction(state)
    val nextStateAction = StateAction(state, action)
    stateAction match {
      case NullStateAction => // Do Nothing
      case _               => learn(stateAction, nextStateAction)
    }
    updateStateAction(nextStateAction)
    action
  }

  protected def learn(previousStateAction: StateAction, nextStateAction: StateAction): Unit

  def environmentWithRewards: Environment with Rewards = environment match {
    case environmentWithRewards: Environment with Rewards => environmentWithRewards
    case environmentWithoutRewards =>
      logger error s"Error, expected environment of type Environment with ie.nix.rl.Rewards," +
        s" but got ${environmentWithoutRewards.getClass.getSimpleName}"
      exit
  }

  def stateAction: StateAction = this._stateAction
  protected def initStateAction(): StateAction = NullStateAction
  protected def resetStateAction(): Unit = this._stateAction = initStateAction
  protected def updateStateAction(stateAction: StateAction): Unit = this._stateAction = stateAction

  def actionValueFunction: AbstractValueFunction[_] = this._actionValueFunction
  def updateActionValueFunction(actionValueFunction: AbstractValueFunction[_]): Unit =
    this._actionValueFunction = actionValueFunction

  protected def initActionValueFunction(): AbstractValueFunction[_]
  protected def resetActionValueFunction(): Unit = this._actionValueFunction = initActionValueFunction

  protected def improvePolicy(policy: AgentPolicy, actionValueFunction: AbstractValueFunction[_]): AgentPolicy = {
    PolicyImprover.improvePolicy(policy, actionValueFunction)
  }

}

object LearningAgent extends Logging {

  val DISCOUNT: String = "discount"

  def getDiscount(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$DISCOUNT")

  def getEnvironmentWithReward(prefix: String): Environment with Rewards = {
    val environment = Configuration.getInstance(s"$prefix.$ENVIRONMENT")
    environment match {
      case environmentWithRewards: Environment with Rewards => environmentWithRewards
      case environmentWithoutRewards =>
        logger error s"Error, expected environment of type Environment with Rewards," +
          s" but got ${environmentWithoutRewards.getClass.getSimpleName}"
        exit
    }
  }

}
