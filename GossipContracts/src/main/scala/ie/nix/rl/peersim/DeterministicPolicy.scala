package ie.nix.rl.peersim

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.peersim.DeterministicPolicy.getEnvironment
import ie.nix.rl.policy.DeterministicPolicy.getRandomStateActionProbabilityMap

class DeterministicPolicy(stateActionProbabilityMap: Map[StateAction, Double])
    extends ie.nix.rl.policy.DeterministicPolicy(stateActionProbabilityMap) {

  def this(prefix: String) =
    this(getRandomStateActionProbabilityMap(getEnvironment(prefix)))

}

object DeterministicPolicy {

  val POLICY: String = "policy"

  def getEnvironment(prefix: String): Environment =
    Agent.getEnvironment(parentPrefix(prefix))

  def parentPrefix(prefix: String): String =
    prefix.substring(0, prefix.size - POLICY.size - 1)

}
