package ie.nix.rl.peersim

import ie.nix.peersim.Utils.now
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.Transitions.Transition
import ie.nix.rl.peersim.LearningAgent.getDiscount
import ie.nix.rl.peersim.QLambdaLearningAgent.getEligibilityTracing
import ie.nix.rl.peersim.QLearningAgent.getStepSize
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.vf.{AbstractValueFunction, EligibilityTracing}
import peersim.config.Configuration

import scala.sys.exit

class QLambdaLearningAgent(prefix: String) extends LearningAgent(prefix) {

  override def toString: String = s"QLambdaLearningAgent"

  val stepSize: Double = getStepSize(prefix)

  override protected def initActionValueFunction(): AbstractValueFunction[_] =
    getEligibilityTracing(prefix, policy)

  override protected def learn(previousStateAction: StateAction, nextStateAction: StateAction): Unit = {
    logger trace s"[$now] $this previousStateAction=$previousStateAction, nextStateAction=$nextStateAction"

    val updatedActionValueFunction =
      updateActionValueFunction(eligibilityTracing, previousStateAction, nextStateAction)
    logger trace s"[$now] $this updatedActionValueFunction=$updatedActionValueFunction"
    updateActionValueFunction(updatedActionValueFunction)

    val updatedPolicy = improvePolicy(policy, updatedActionValueFunction)
    logger trace s"[$now] $this updatedPolicy=$updatedPolicy"
    updatePolicy(updatedPolicy)
  }

  protected def updateActionValueFunction(eligibilityTracing: EligibilityTracing,
                                          previousStateAction: StateAction,
                                          nextStateAction: StateAction): EligibilityTracing = {
    val previousStateActionValue = eligibilityTracing.getValue(previousStateAction)
    val nextStateActionValue = eligibilityTracing.getValue(nextStateAction)
    val maxNextStateActionValue = eligibilityTracing.getMaxActionValueForState(nextStateAction.state)
    logger trace s"[$now] $this previousStateAction=$previousStateAction, " +
      s"nextStateAction=$nextStateAction, maxNextStateActionValue=$maxNextStateActionValue"

    // Get the reward for the nes state
    val transition = Transition(previousStateAction, nextStateAction.state)
    val reward = environmentWithRewards.getReward(transition)
    // Get the error
    val error = getError(previousStateActionValue, reward, maxNextStateActionValue)
    logger trace s"[$now] $this error=$error <= " +
      s"previousStateAction=$previousStateAction with " +
      s"reward=$reward + maxNextStateActionValue=$maxNextStateActionValue - " +
      s"previousStateActionValue=$previousStateActionValue"

    // Update the error for all states
    val updatedEligibilityTracing = eligibilityTracing.updateValue(previousStateAction, error)
    logger trace s"[$now] $this previousStateAction=$previousStateAction => $updatedEligibilityTracing"

    // if took exploratory action zero traces. The state-action pairs you visited before the random exploratory step
    // deserve no credit/blame for future rewards, hence you delete the whole eligibility trace.
    if (nextStateActionValue != maxNextStateActionValue)
      updatedEligibilityTracing.clearEligibilityForAllStates()
    else
      updatedEligibilityTracing
  }

  protected def getError(stateActionValue: Double, reward: Double, maxNextStateActionValue: Double): Double =
    // step size * δ ← R + γ max V(S′) − V(S)
    stepSize * (reward + (discount * maxNextStateActionValue) - stateActionValue)

  protected def eligibilityTracing: EligibilityTracing = actionValueFunction match {
    case eligibilityTracing: EligibilityTracing => eligibilityTracing
    case notAnEligibilityTraces =>
      logger error s"Fatal error, expected actionValueFunction to be of type, " +
        s"EligibilityTraces, but was of type ${notAnEligibilityTraces.getClass.getName}"
      exit
  }

}

object QLambdaLearningAgent {

  val STEP_SIZE: String = "step_size"
  val INITIAL_VALUE: String = "initial_value"
  val INITIAL_VALUE_NOT_SET: Double = -1d
  val ELIGIBILITY_DECAY: String = "eligibility_decay"

  def getStepSize(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$STEP_SIZE")

  def getInitialValue(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$INITIAL_VALUE", INITIAL_VALUE_NOT_SET)

  def getEligibilityDecay(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$ELIGIBILITY_DECAY")

  def getEligibilityTracing(prefix: String, policy: Policy): EligibilityTracing =
    getInitialValue(prefix) match {
      case INITIAL_VALUE_NOT_SET =>
        EligibilityTracing(policy, getDiscount(prefix), getEligibilityDecay(prefix))
      case initialValue =>
        EligibilityTracing(policy, getDiscount(prefix), getEligibilityDecay(prefix), initialValue)
    }

}
