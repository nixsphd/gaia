package ie.nix.rl.peersim

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.peersim.SoftPolicy.{getEnvironment, getProbabilityOfRandomAction}
import ie.nix.rl.policy.SoftPolicy.getRandomStateActionProbabilityMap
import peersim.config.Configuration

class SoftPolicy(stateActionProbabilityMap: Map[StateAction, Double], probabilityOfRandomAction: Double)
    extends ie.nix.rl.policy.SoftPolicy(stateActionProbabilityMap, probabilityOfRandomAction) {

  def this(prefix: String) =
    this(getRandomStateActionProbabilityMap(getProbabilityOfRandomAction(prefix))(getEnvironment(prefix)),
         getProbabilityOfRandomAction(prefix))

}

object SoftPolicy {

  val POLICY: String = "policy"
  val PROBABILITY_OF_RANDOM_ACTION: String = "probability_of_random_action"

  def getProbabilityOfRandomAction(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$PROBABILITY_OF_RANDOM_ACTION")

  def getEnvironment(prefix: String): Environment =
    Agent.getEnvironment(parentPrefix(prefix))

  def parentPrefix(prefix: String): String =
    prefix.substring(0, prefix.size - POLICY.size - 1)

}
