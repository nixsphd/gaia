package ie.nix.rl.peersim

import ie.nix.peersim.Checkpointing
import ie.nix.peersim.Utils.protocolsAs
import ie.nix.rl.peersim.Agent.AgentPolicy
import ie.nix.rl.peersim.Learning.getAgentProtocolID
import org.apache.logging.log4j.scala.Logging

class PolicyCheckpointer(val prefix: String) extends Checkpointing[Vector[AgentPolicy]] with Logging {

  override def updateObjectFromCheckpoint(checkpointedPolicies: Vector[AgentPolicy]): Unit =
    agents
      .zip(checkpointedPolicies)
      .foreach(agentAndCheckpointedPolicies => {
        val (agent, checkpointedPolicy) = agentAndCheckpointedPolicies
        val mostProbablyActionsForStates = checkpointedPolicy.getMostProbableActions()
        val newPolicy = mostProbablyActionsForStates.foldLeft(agent.policy)((policy, stateAction) => {
          policy.updateAction(stateAction.state, stateAction.action)
        })
        agent.updatePolicy(newPolicy)
      })

  def objectToCheckpoint(): Vector[AgentPolicy] =
    agents.map(_.policy)

  def agents: Vector[Agent] = protocolsAs[Agent](agentProtocolID)

  def agentProtocolID: Int = getAgentProtocolID(prefix)

}
