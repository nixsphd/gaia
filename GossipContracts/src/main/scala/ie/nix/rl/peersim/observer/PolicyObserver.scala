package ie.nix.rl.peersim.observer

import ie.nix.peersim.{Logger, Times}
import ie.nix.peersim.Utils.{node, now, protocolAs, protocolsAs}
import ie.nix.rl.peersim.observer.PolicyObserver.getAgentProtocolID
import ie.nix.rl.peersim.Agent
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

class PolicyObserver(prefix: String) extends Logger(prefix) with Times with Logging {

  val agentProtocolID: Int = getAgentProtocolID(prefix)
  logger trace s"[$now] agentProtocolID=$agentProtocolID"

  override def headers: String = "Agent,State,Action"

  override protected def log(): Unit = {
    agents.foreach(logAgent)
  }

  protected def logAgent(agent: Agent): Unit = {
    val policy = agent.policy
    val states = policy.getStates.sortBy(_.toString)
    states.foreach(state => {
      val action = policy.getMostProbableAction(state)
      writeRow(s"${agent.getIndex}, $state, $action")
    })
  }

  override protected def finalLog(): Unit = log()

  def agents: Vector[Agent] = protocolsAs[Agent](agentProtocolID)
  def Agent: Agent = protocolAs[Agent](node, agentProtocolID)

}

object PolicyObserver {

  private val AGENT_PROTOCOL = "rl_agent"
  def getAgentProtocolID(prefix: String): Int = Configuration.getPid(s"$prefix.$AGENT_PROTOCOL")

}
