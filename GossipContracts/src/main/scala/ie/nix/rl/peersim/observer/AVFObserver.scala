package ie.nix.rl.peersim.observer

import ie.nix.peersim.{Logger, Times}
import ie.nix.peersim.Utils.{node, now, protocolAs, protocolsAs}
import ie.nix.rl.peersim.LearningAgent
import ie.nix.rl.peersim.observer.AVFObserver.getAgentProtocolID
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

class AVFObserver(prefix: String) extends Logger(prefix) with Times with Logging {

  val agentProtocolID: Int = getAgentProtocolID(prefix)
  logger trace s"[$now] agentProtocolID=$agentProtocolID"

  override def headers: String = "Agent,StateAction,Value"

  override protected def log(): Unit = {
    learningAgents.foreach(logLearningAgent)
  }

  protected def logLearningAgent(learningAgent: LearningAgent): Unit = {
    val avf = learningAgent.actionValueFunction
    val stateActions = avf.getStateActions.sortBy(_.toString)
    stateActions.foreach(stateAction => {
      val stateActionString = s"${stateAction.state} ${stateAction.action}"
      val value = avf.getValue(stateAction)
      writeRow(s"${learningAgent.getIndex}, $stateActionString, $value")
    })
  }

  override protected def finalLog(): Unit = log()

  def learningAgents: Vector[LearningAgent] = protocolsAs[LearningAgent](agentProtocolID)
  def learningAgent: LearningAgent = protocolAs[LearningAgent](node, agentProtocolID)

}

object AVFObserver {

  private val AGENT_PROTOCOL = "rl_agent"
  def getAgentProtocolID(prefix: String): Int = Configuration.getPid(s"$prefix.$AGENT_PROTOCOL")

}
