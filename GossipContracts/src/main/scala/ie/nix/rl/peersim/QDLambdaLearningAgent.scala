package ie.nix.rl.peersim

import ie.nix.peersim.Utils.now
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.Transitions.Transition
import ie.nix.rl.peersim.QDLambdaLearningAgent.{getConsensusStepSize, getInnovationStepSize}
import ie.nix.rl.peersim.QLambdaLearningAgent.getEligibilityTracing
import ie.nix.rl.policy.vf.{AbstractValueFunction, EligibilityTracing}
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

import scala.sys.exit

class QDLambdaLearningAgent(prefix: String) extends NetworkedLearningAgent(prefix) with Logging {

  override def toString: String = s"QDLambdaLearningAgent"

  val innovationStepSize: Double = getInnovationStepSize(prefix)
  val consensusStepSize: Double = getConsensusStepSize(prefix)

  override protected def initActionValueFunction(): AbstractValueFunction[_] =
    getEligibilityTracing(prefix, policy)

  override protected def learn(previousStateAction: StateAction,
                               nextStateAction: StateAction,
                               neighboursStateActionValues: Vector[Double]): Unit = {
    logger trace s"[$now] $this previousStateAction=$previousStateAction, nextStateAction=$nextStateAction"

    val updatedActionValueFunction =
      updateActionValueFunction(eligibilityTracing, previousStateAction, nextStateAction, neighboursStateActionValues)
    logger trace s"[$now] $this updatedActionValueFunction=$updatedActionValueFunction"
    updateActionValueFunction(updatedActionValueFunction)

    val updatedPolicy = improvePolicy(policy, updatedActionValueFunction)
    logger trace s"[$now] $this updatedPolicy=$updatedPolicy"
    updatePolicy(updatedPolicy)

  }

  protected def updateActionValueFunction(eligibilityTracing: EligibilityTracing,
                                          previousStateAction: StateAction,
                                          nextStateAction: StateAction,
                                          neighboursPreviousStateActionValues: Vector[Double]): EligibilityTracing = {

    val previousStateActionValue = eligibilityTracing.getValue(previousStateAction)
    val maxNextStateActionValue = eligibilityTracing.getMaxActionValueForState(nextStateAction.state)
    logger trace s"[$now] $this previousStateAction=$previousStateAction, " +
      s"nextStateAction=$nextStateAction, maxNextStateActionValue=$maxNextStateActionValue"

    // Get the reward for the nes state
    val transition = Transition(previousStateAction, nextStateAction.state)
    val reward = environmentWithRewards.getReward(transition)
    val error = getError(previousStateActionValue, neighboursPreviousStateActionValues, reward, maxNextStateActionValue)

    // Update the error for all states
    val updatedEligibilityTracing = eligibilityTracing.updateValue(previousStateAction, error)
    logger trace s"previousStateAction=$previousStateAction, error=$error"

    // if took exploratory action zero traces. The state-action pairs you visited before the random exploratory step
    // deserve no credit/blame for future rewards, hence you delete the whole eligibility trace.
    val nextStateActionValue = eligibilityTracing.getValue(nextStateAction)
    if (nextStateActionValue != maxNextStateActionValue)
      updatedEligibilityTracing.clearEligibilityForAllStates()
    else
      updatedEligibilityTracing
  }

  protected def getError(previousStateActionValue: Double,
                         neighboursPreviousStateActionValues: Vector[Double],
                         reward: Double,
                         maxNextStateActionValue: Double) = {
    val innovation = getInnovation(previousStateActionValue, reward, maxNextStateActionValue)
    val consensus = getConsensus(previousStateActionValue, neighboursPreviousStateActionValues)
    val error = (innovationStepSize * innovation) - (consensusStepSize * consensus)
    logger trace s"previousStateActionValue=$previousStateActionValue, " +
      s"neighboursPreviousStateActionValues=$neighboursPreviousStateActionValues, reward=$reward, " +
      s"maxNextStateActionValue=$maxNextStateActionValue, error=$error, innovation=$innovation, consensus=$consensus"
    error
  }

  protected def getInnovation(stateActionValue: Double, reward: Double, maxNextStateActionValue: Double): Double = {
    // δ ← R + γV(S′) − V(S)
    reward + (discount * maxNextStateActionValue) - stateActionValue
  }

  protected def getConsensus(previousStateActionValue: Double,
                             neighboursPreviousStateActionValues: Vector[Double]): Double = {
    // i ← ∑  V(S) − Vn(S)
    neighboursPreviousStateActionValues
      .map(previousStateActionValue - _)
      .sum
  }

  protected def eligibilityTracing: EligibilityTracing = actionValueFunction match {
    case eligibilityTracing: EligibilityTracing => eligibilityTracing
    case notAnEligibilityTraces =>
      logger error s"Fatal error, expected actionValueFunction to be of type, " +
        s"EligibilityTraces, but was of type ${notAnEligibilityTraces.getClass.getName}"
      exit
  }

}

object QDLambdaLearningAgent {

  val INITIAL_VALUE: String = "initial_value"
  val INITIAL_VALUE_NOT_SET: Double = -1d
  val ELIGIBILITY_DECAY: String = "eligibility_decay"
  val INNOVATION_STEP_SIZE = "innovation_step_size"
  val CONSENSUS_STEP_SIZE = "consensus_step_size"

  def getEligibilityDecay(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$ELIGIBILITY_DECAY")

  def getInitialValue(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$INITIAL_VALUE", INITIAL_VALUE_NOT_SET)

  def getInnovationStepSize(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$INNOVATION_STEP_SIZE")

  def getConsensusStepSize(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$CONSENSUS_STEP_SIZE")

}
