package ie.nix.rl.peersim.observer

import ie.nix.peersim.Utils.{node, now, protocolAs}
import ie.nix.rl.peersim.{Agent, Environment}
import ie.nix.rl.policy.Policy.difference
import ie.nix.rl.policy.{DeterministicPolicy, Policy}
import org.apache.logging.log4j.scala.Logging

abstract class AbstractPolicyWithErrorObserver(prefix: String) extends PolicyObserver(prefix) with Logging {

  protected lazy val idealPolicy: Policy = initIdealPolicy()

  override def headers: String = "Agent,Error"

  override protected def logAgent(agent: Agent): Unit = {
    val error = difference(agent.policy, idealPolicy)
    writeRow(s"${agent.getIndex}, $error")
  }

  override protected def finalLog(): Unit = {
    super.finalLog()
    val policyDifferences = (for {
      agent <- agents
      policyDifference = difference(agent.policy, idealPolicy)
    } yield policyDifference).sum
    logger info s"[$now]~policyDifferences=$policyDifferences"
  }

  def environment: Environment =
    protocolAs[Agent](node, agentProtocolID).environmentAs[Environment]

  def initIdealPolicy(): DeterministicPolicy

  def policyDifferences: Vector[(Agent, Int)] = agents.map(agent => (agent, difference(agent.policy, idealPolicy)))
  def totalLearnerDifferences: Int = policyDifferences.map(_._2).sum
  def numberCorrect: Int = policyDifferences.count(_._2 == 0)

}

class PolicyWithErrorObserver(val prefix: String)
    extends AbstractPolicyWithErrorObserver(prefix)
    with DynamicProgrammingIdeals
