package ie.nix.rl.peersim

import ie.nix.rl.Environment.{State, StateAction}
import ie.nix.rl.Transitions.Transition
import ie.nix.util.Selector

trait TransitionsMap extends ie.nix.rl.Transitions with Selector {
  environment: Environment =>

  protected val _transitionProbabilities: Map[Transition, Double]

  def transitionProbability(transition: Transition): Double =
    _transitionProbabilities.getOrElse(transition, 0d)

  def nextState(stateAction: StateAction): State =
    selectItem[Transition](transitionProbabilities(stateAction)).finalState

  def transitionProbabilities(stateAction: StateAction): Vector[(Transition, Double)] =
    _transitionProbabilities.filter(_._1.stateAction == stateAction).toVector

}

object TransitionsMap {
  type GetTransitionProbabilities = (String, Set[StateAction]) => Map[Transition, Double]

//  def getActionValueFunction(transitions: Transitions with Rewards): ActionValueFunction[_] = {}

}
//State1=209.7935689294457
//State2=305.48268365365067
