package ie.nix.rl.peersim.observer

import ie.nix.peersim.Utils.{node, protocolAs}
import ie.nix.rl.peersim.LearningAgent
import ie.nix.rl.peersim.observer.DynamicProgrammingIdeals.{getDelta, getDiscount, getMaxNumberOfSweeps}
import ie.nix.rl.policy.DynamicProgramming.EnvironmentModel
import ie.nix.rl.policy.vf.ActionValueFunction
import ie.nix.rl.policy.{DeterministicPolicy, DynamicProgramming}
import peersim.config.Configuration
import scala.language.reflectiveCalls

trait DynamicProgrammingIdeals {
  observer: {
    val prefix: String;
    val agentProtocolID: Int
  } =>

  lazy val discount = getDiscount(observer.prefix)
  lazy val maxNumberOfSweeps = getMaxNumberOfSweeps(prefix)
  lazy val delta = getDelta(prefix)

  def initIdealActionValueFunction(): ActionValueFunction = {
    DynamicProgramming(discount).evaluateEnvironment(environmentModel, delta, maxNumberOfSweeps).actionValueFunction
  }

  def initIdealPolicy(): DeterministicPolicy = {
    DynamicProgramming(discount).evaluateEnvironment(environmentModel, delta, maxNumberOfSweeps).policy
  }

  def environmentModel: EnvironmentModel =
    protocolAs[LearningAgent](node, agentProtocolID).environmentAs[EnvironmentModel]

}

object DynamicProgrammingIdeals {

  val AGENT_PROTOCOL = "rl_agent"
  val DEFAULT_MAX_NUMBER_OF_SWEEPS = 1000
  val MAX_NUMBER_OF_SWEEPS: String = "max_number_of_sweeps"
  val DEFAULT_DELTA = 0.001
  val DELTA: String = "delta"

  def getMaxNumberOfSweeps(prefix: String): Int =
    Configuration.getInt(s"$prefix.$MAX_NUMBER_OF_SWEEPS", DEFAULT_MAX_NUMBER_OF_SWEEPS)

  def getDelta(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$DELTA", DEFAULT_DELTA)

  def getAgent(prefix: String): LearningAgent =
    protocolAs[LearningAgent](node, Configuration.getPid(s"$prefix.$AGENT_PROTOCOL"))

  def getDiscount(prefix: String): Double = getAgent(prefix).discount

}
