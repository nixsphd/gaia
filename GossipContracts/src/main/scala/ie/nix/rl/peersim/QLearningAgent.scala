package ie.nix.rl.peersim

import ie.nix.peersim.Utils.now
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.Transitions.Transition
import ie.nix.rl.peersim.QLearningAgent.{getConstantStepSize, getStepSize}
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.vf.{AbstractValueFunction, ConstantStepSize}
import peersim.config.Configuration

import scala.sys.exit

class QLearningAgent(prefix: String) extends LearningAgent(prefix) {

  val stepSize: Double = getStepSize(prefix)

  override def toString: String = s"QLearningAgent $actionValueFunction"

  override protected def initActionValueFunction(): AbstractValueFunction[_] =
    getConstantStepSize(prefix, policy)

  override protected def learn(previousStateAction: StateAction, nextStateAction: StateAction): Unit = {
    logger trace s"[$now] $this previousStateAction=$previousStateAction, nextStateAction=$nextStateAction"

    val updatedActionValueFunction =
      updateActionValueFunction(constantStepSize, previousStateAction, nextStateAction)
    logger trace s"[$now] $this updatedActionValueFunction=$updatedActionValueFunction"
    updateActionValueFunction(updatedActionValueFunction)

    val updatedPolicy = improvePolicy(policy, updatedActionValueFunction)
    logger trace s"[$now] $this updatedPolicy=$updatedPolicy"
    updatePolicy(updatedPolicy)
  }

  protected def updateActionValueFunction(constantStepSize: ConstantStepSize,
                                          previousStateAction: StateAction,
                                          nextStateAction: StateAction): ConstantStepSize = {

    val previousStateActionValue = constantStepSize.getValue(previousStateAction)
    val maxNextStateActionValue = constantStepSize.getMaxActionValueForState(nextStateAction.state)
    logger trace s"[$now] $this previousStateAction=$previousStateAction, " +
      s"nextStateAction=$nextStateAction, maxNextStateActionValue=$maxNextStateActionValue"

    // Get the reward for the nes state
    val transition = Transition(previousStateAction, nextStateAction.state)
    val reward = environmentWithRewards.getReward(transition)
    // Get the error
    val error = getError(previousStateActionValue, reward, maxNextStateActionValue)
    logger trace s"[$now] $this error=$error <= " +
      s"previousStateAction=$previousStateAction with " +
      s"reward=$reward + maxNextStateActionValue=$maxNextStateActionValue - " +
      s"previousStateActionValue=$previousStateActionValue"
    // Update the error for all states
    val updatedConstantStepSize = constantStepSize.updateValue(previousStateAction, error)
    logger trace s"[$now] $this previousStateAction=$previousStateAction => ${updatedConstantStepSize.getValue(previousStateAction)}"

    logger debug s"[$now] $getIndex $previousStateAction $error " +
      s"${updatedConstantStepSize.getValue(previousStateAction)}"
    updatedConstantStepSize
  }

  protected def getError(stateActionValue: Double, reward: Double, maxNextStateActionValue: Double): Double =
    stepSize * (reward + (discount * maxNextStateActionValue) - stateActionValue) // aδ ← a(R + γ max V(S′) − V(S))

  protected def constantStepSize: ConstantStepSize = actionValueFunction match {
    case constantStepSize: ConstantStepSize => constantStepSize
    case notAnConstantStepSize =>
      logger error s"Fatal error, expected actionValueFunction to be of type, " +
        s"ConstantStepSize, but was of type ${notAnConstantStepSize.getClass.getName}"
      exit
  }

}

object QLearningAgent {

  val STEP_SIZE: String = "step_size"
  val INITIAL_VALUE: String = "initial_value"
  val INITIAL_VALUE_NOT_SET: Double = -1d

  def getStepSize(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$STEP_SIZE")

  def getInitialValue(prefix: String): Double =
    Configuration.getDouble(s"$prefix.$INITIAL_VALUE", INITIAL_VALUE_NOT_SET)

  def getConstantStepSize(prefix: String, policy: Policy): ConstantStepSize =
    getInitialValue(prefix) match {
      case INITIAL_VALUE_NOT_SET => ConstantStepSize(policy)
      case initialValue          => ConstantStepSize(policy, initialValue)
    }

}
