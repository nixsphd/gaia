package ie.nix.rl.peersim

import ie.nix.peersim.Utils.now
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.Transitions.Transition
import ie.nix.rl.peersim.QDLambdaLearningAgent.{getConsensusStepSize, getInnovationStepSize}
import ie.nix.rl.peersim.QLearningAgent.getConstantStepSize
import ie.nix.rl.policy.vf.{AbstractValueFunction, ConstantStepSize}

import scala.sys.exit

class QDLearningAgent(prefix: String) extends NetworkedLearningAgent(prefix) {

  val innovationStepSize: Double = getInnovationStepSize(prefix)
  val consensusStepSize: Double = getConsensusStepSize(prefix)

  override def toString: String = s"QLearningAgent $actionValueFunction"

  override protected def initActionValueFunction(): AbstractValueFunction[_] =
    getConstantStepSize(prefix, policy)

  override protected def learn(previousStateAction: StateAction,
                               nextStateAction: StateAction,
                               neighboursStateActionValues: Vector[Double]): Unit = {
    logger trace s"[$now] $this previousStateAction=$previousStateAction, nextStateAction=$nextStateAction"

    val updatedActionValueFunction =
      updateActionValueFunction(constantStepSize, previousStateAction, nextStateAction, neighboursStateActionValues)
    logger trace s"[$now] $this updatedActionValueFunction=$updatedActionValueFunction"
    updateActionValueFunction(updatedActionValueFunction)

    val updatedPolicy = improvePolicy(policy, updatedActionValueFunction)
    logger trace s"[$now] $this updatedPolicy=$updatedPolicy"
    updatePolicy(updatedPolicy)
  }

  protected def updateActionValueFunction(constantStepSize: ConstantStepSize,
                                          previousStateAction: StateAction,
                                          nextStateAction: StateAction,
                                          neighboursPreviousStateActionValues: Vector[Double]): ConstantStepSize = {

    val previousStateActionValue = constantStepSize.getValue(previousStateAction)
    val maxNextStateActionValue = constantStepSize.getMaxActionValueForState(nextStateAction.state)
    logger trace s"[$now] $this previousStateAction=$previousStateAction, " +
      s"nextStateAction=$nextStateAction, maxNextStateActionValue=$maxNextStateActionValue"

    // Get the reward for the nes state
    val transition = Transition(previousStateAction, nextStateAction.state)
    val reward = environmentWithRewards.getReward(transition)
    val error = getError(previousStateActionValue, neighboursPreviousStateActionValues, reward, maxNextStateActionValue)
    logger trace s"[$now] $this error=$error <= " +
      s"previousStateAction=$previousStateAction with " +
      s"reward=$reward + maxNextStateActionValue=$maxNextStateActionValue - " +
      s"previousStateActionValue=$previousStateActionValue"

    // Update the error for all states
    val updatedConstantStepSize = constantStepSize.updateValue(previousStateAction, error)
    logger trace s"[$now] $this previousStateAction=$previousStateAction => ${updatedConstantStepSize.getValue(previousStateAction)}"

    val meanNeighboursValues = neighboursPreviousStateActionValues.sum / neighboursPreviousStateActionValues.size
    logger debug s"[$now] $getIndex $previousStateAction value=$previousStateActionValue " +
      s"meanNeighbours=$meanNeighboursValues error=$error newValue=${updatedConstantStepSize.getValue(previousStateAction)}"

    updatedConstantStepSize
  }

  protected def getError(previousStateActionValue: Double,
                         neighboursPreviousStateActionValues: Vector[Double],
                         reward: Double,
                         maxNextStateActionValue: Double) = {
    val innovation = getInnovation(previousStateActionValue, reward, maxNextStateActionValue)
    val consensus = getConsensus(previousStateActionValue, neighboursPreviousStateActionValues)
    val error = (innovationStepSize * innovation) - (consensusStepSize * consensus)
    logger trace s"previousStateActionValue=$previousStateActionValue, " +
      s"neighboursPreviousStateActionValues=$neighboursPreviousStateActionValues, reward=$reward, " +
      s"maxNextStateActionValue=$maxNextStateActionValue, error=$error, innovation=$innovation, consensus=$consensus"
    error
  }

  protected def getInnovation(stateActionValue: Double, reward: Double, maxNextStateActionValue: Double): Double = {
    // δ ← R + γV(S′) − V(S)
    reward + (discount * maxNextStateActionValue) - stateActionValue
  }

  protected def getConsensus(previousStateActionValue: Double,
                             neighboursPreviousStateActionValues: Vector[Double]): Double = {
    // i ← ∑ V(S) − Vn(S)
    neighboursPreviousStateActionValues
      .map(previousStateActionValue - _)
      .sum
  }

  protected def constantStepSize: ConstantStepSize = actionValueFunction match {
    case constantStepSize: ConstantStepSize => constantStepSize
    case notAnConstantStepSize =>
      logger error s"Fatal error, expected actionValueFunction to be of type, " +
        s"ConstantStepSize, but was of type ${notAnConstantStepSize.getClass.getName}"
      exit
  }

}
