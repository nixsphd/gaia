package ie.nix.rl.peersim

import ie.nix.rl.Environment.{Action, State, StateAction}
import org.scalactic.TypeCheckedTripleEquals._

abstract class Environment(stateActionsSet: Set[StateAction], terminalStateSet: Set[State])
    extends ie.nix.rl.Environment {

  override def toString: String = s"Environment"

  override def getStates: Vector[State] = (stateActionsSet.map(_.state) ++ terminalStateSet).toVector
  override def getTerminalStates: Vector[State] = terminalStateSet.toVector
  override def getNonTerminalStates: Vector[State] = stateActionsSet.map(_.state).toVector
  override def getStateActions: Vector[StateAction] = stateActionsSet.toVector
  def hasState(state: State): Boolean =
    stateActionsSet.map(_.state).contains(state) || terminalStateSet.contains(state)

  override def getActionsForState(state: State): Vector[Action] =
    stateActionsSet.filter(_.state === state).map(_.action).toVector
  override def getStateActionsForState(state: State): Vector[StateAction] =
    stateActionsSet.filter(_.state === state).toVector
}

object Environment {
  def emptyStateSet: Set[State] = Set[State]()
}
