package ie.nix.rl.peersim.observer

import ie.nix.peersim.Utils.now
import ie.nix.rl.peersim.LearningAgent
import ie.nix.rl.policy.vf.ActionValueFunction
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics
import org.apache.logging.log4j.scala.Logging

abstract class AbstractAVFWithErrorObserver(val prefix: String) extends AVFObserver(prefix) with Logging {

  protected lazy val idealActionValueFunction: ActionValueFunction = initIdealActionValueFunction()

  override def headers: String = "Agent,StateAction,Value,Error"

  override protected def logLearningAgent(learningAgent: LearningAgent): Unit =
    for {
      stateAction <- learningAgent.actionValueFunction.getStateActions
      value = learningAgent.actionValueFunction.getValue(stateAction)
      error = idealActionValueFunction.getValue(stateAction) - value
    } writeRow(s"${learningAgent.getIndex}, ${stateAction.state} ${stateAction.action}, $value, $error")

  override protected def finalLog(): Unit = {
    super.finalLog()
    val statistics = new DescriptiveStatistics()
    for {
      learningAgent <- learningAgents
      stateAction <- learningAgent.actionValueFunction.getStateActions
      value = learningAgent.actionValueFunction.getValue(stateAction)
      error = idealActionValueFunction.getValue(stateAction) - value
      squaredError = Math.pow(error, 2)
    } statistics.addValue(squaredError)

    val rmse = Math.sqrt(statistics.getMean)
    logger info s"[$now]~RMSE=$rmse"
  }

  def initIdealActionValueFunction(): ActionValueFunction

}

class AVFWithErrorObserver(prefix: String) extends AbstractAVFWithErrorObserver(prefix) with DynamicProgrammingIdeals
