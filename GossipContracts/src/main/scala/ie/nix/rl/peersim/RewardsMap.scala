package ie.nix.rl.peersim

import ie.nix.rl.Environment.State
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.Transitions.Transition

trait RewardsMap extends ie.nix.rl.Rewards {
  environment: Environment =>

  protected val stateRewards: Map[State, Reward]

  override def getReward(state: State): Reward = stateRewards.getOrElse(state, 0d)
  override def getReward(transition: Transition): Reward = getReward(transition.finalState)

}

object RewardsMap {
  type GetStateRewards = (String, Set[State]) => Map[State, Reward]
}
