package ie.nix.rl.peersim

import ie.nix.peersim.Utils.protocolAs
import ie.nix.rl.Environment.StateAction

abstract class NetworkedLearningAgent(prefix: String) extends LearningAgent(prefix) {

  override def toString: String = s"NetworkedLearningAgent"

  override protected def learn(previousStateAction: StateAction, nextStateAction: StateAction): Unit =
    learn(previousStateAction, nextStateAction, neighboursStateActionValues(previousStateAction))

  protected def learn(previousStateAction: StateAction,
                      nextStateAction: StateAction,
                      neighboursPreviousStateActionValues: Vector[Double]): Unit

  protected def neighboursStateActionValues(stateAction: StateAction): Vector[Double] =
    neighbours
      .map(protocolAs[NetworkedLearningAgent](_, protocolID))
      .map(_.actionValueFunction.getValue(stateAction))
}
