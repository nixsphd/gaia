package ie.nix.rl

import ie.nix.rl.Environment.State
import ie.nix.rl.Rewards.Reward
import ie.nix.rl.Transitions.Transition

trait Rewards {
  environment: Environment =>

  def getReward(transition: Transition): Reward = getReward(transition.finalState)
  def getReward(state: State): Reward = 0d

}

object Rewards {

  type Reward = Double

  case class TransitionReward(transition: Transition, reward: Reward)

}
