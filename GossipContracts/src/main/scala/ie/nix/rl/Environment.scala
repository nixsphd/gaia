package ie.nix.rl

import ie.nix.rl.Environment.{Action, State, StateAction}
import org.apache.logging.log4j.scala.Logging

trait Environment extends Logging {

  def getStates: Vector[State]
  def getTerminalStates: Vector[State]
  def getNonTerminalStates: Vector[State]
  def getStateActions: Vector[StateAction]

  def getActionsForState(state: State): Vector[Action]
  def getStateActionsForState(state: State): Vector[StateAction]

}

object Environment extends Logging {

  trait State extends Serializable { def isTerminal: Boolean = false }
  object NullState extends State {
    override def toString() = "NullState"
    override def isTerminal: Boolean = true
  }

  trait Action extends Serializable { def isTerminal: Boolean = false }
  object NullAction extends Action {
    override def toString() = "NullAction"
    override def isTerminal: Boolean = true
  }

  case class StateAction(state: State, action: Action) {
    override def toString() = s"StateAction($state,$action)"
    def isTerminal: Boolean = state.isTerminal || action.isTerminal
  }
  object NullStateAction extends StateAction(NullState, NullAction)

}
