package ie.nix.rl.policy.vf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.vf.AbstractValueFunction.getValueMap
import ie.nix.rl.policy.vf.EligibilityTracing.EligibilityTracingValue

import scala.util.Random

class EligibilityTracing(val valueMap: Map[StateAction, EligibilityTracingValue],
                         val discount: Double,
                         val eligibilityDecay: Double)
    extends AbstractValueFunction[EligibilityTracingValue]
    with Serializable {

  override def updateValue(stateAction: StateAction, error: Double): EligibilityTracing =
    incrementEligibilityForState(stateAction)
      .updateErrorForAllStates(error)
      .decayEligibilityForAllStates()

  override protected def mapValue(eligibilityTracingValue: EligibilityTracingValue): Double =
    eligibilityTracingValue.value

  protected def incrementEligibilityForState(stateAction: StateAction): EligibilityTracing = {
    val eligibilityTracingValue = getValueAndEligibility(stateAction)
    val newEligibilityOfState =
      if (discount == 1d)
        eligibilityTracingValue.eligibility + 1d // Z(S) ← Z(S) + 1 Accumulating traces
      else
        1d // Z(S) ← 1 replacing traces
    updateValueAndEligibilityTrace(stateAction, eligibilityTracingValue.value, newEligibilityOfState)
  }

  protected def updateErrorForAllStates(error: Double): EligibilityTracing =
    getStateActions.foldLeft(this)((eligibilityTraces, stateAction) => {
      val eligibilityTracingValue = getValueAndEligibility(stateAction)
      // V(s) ← V(s) + αδZ(s)
      val newValue = eligibilityTracingValue.value + (error * eligibilityTracingValue.eligibility)
      eligibilityTraces.updateValueAndEligibilityTrace(stateAction, newValue, eligibilityTracingValue.eligibility)
    })

  protected def decayEligibilityForAllStates(): EligibilityTracing =
    getStateActions.foldLeft(this)((eligibilityTraces, stateAction) => {
      val eligibilityTracingValue = getValueAndEligibility(stateAction)
      // Z(s) ← γλZ(s)
      val newEligibility = discount * eligibilityDecay * eligibilityTracingValue.eligibility
      eligibilityTraces.updateValueAndEligibilityTrace(stateAction, eligibilityTracingValue.value, newEligibility)
    })

  def clearEligibilityForAllStates(): EligibilityTracing =
    getStateActions.foldLeft(this)((eligibilityTraces, stateAction) => {
      val eligibilityTracingValue = getValueAndEligibility(stateAction)
      eligibilityTraces.updateValueAndEligibilityTrace(stateAction, eligibilityTracingValue.value, 0d)
    })

  protected def getValueAndEligibility(stateAction: StateAction): EligibilityTracingValue =
    valueMap.getOrElse(stateAction, EligibilityTracingValue(0d, 0d))

  protected def updateValueAndEligibilityTrace(stateAction: StateAction,
                                               value: Double,
                                               eligibilityTrace: Double): EligibilityTracing =
    new EligibilityTracing(valueMap + (stateAction -> EligibilityTracingValue(value, eligibilityTrace)),
                           discount,
                           eligibilityDecay)

}

object EligibilityTracing {

  case class EligibilityTracingValue(value: Double, eligibility: Double)

  val randomValue: () => EligibilityTracingValue = () => EligibilityTracingValue(Random.nextDouble(), 0d)

  def apply(policy: Policy, discount: Double, eligibilityDecay: Double): EligibilityTracing =
    new EligibilityTracing(valueMap(policy), discount, eligibilityDecay)

  def apply(policy: Policy, discount: Double, eligibilityDecay: Double, initialValue: Double): EligibilityTracing =
    new EligibilityTracing(valueMap(policy, initialValue), discount, eligibilityDecay)

  def valueMap(policy: Policy): Map[StateAction, EligibilityTracingValue] =
    getValueMap[EligibilityTracingValue](policy)(randomValue)

  def valueMap(policy: Policy, initialValue: Double): Map[StateAction, EligibilityTracingValue] =
    getValueMap[EligibilityTracingValue](policy)(() => EligibilityTracingValue(initialValue, 0d))

}
