package ie.nix.rl.policy

import ie.nix.rl.Environment
import ie.nix.rl.Environment.{Action, State, StateAction}
import ie.nix.rl.policy.Policy.Determinism
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

class DeterministicPolicy(protected val stateActionProbabilityMap: Map[StateAction, Double])
    extends Policy
    with Determinism
    with Logging {

  override def getAction(state: State): Action = {
    val actionsWithProbabilityEquals1 = for {
      (action, probability) <- getActionAndProbabilities(state)
      if probability === 1d
    } yield action
    logger.trace(s"~actionsWithProbabilityEquals1=$actionsWithProbabilityEquals1")

    actionsWithProbabilityEquals1 match {
      case action +: Vector() => action
      case actions if actions.length === 0 =>
        throw new RuntimeException(s"Deterministic policy does not have an action for state, $state")
      case actions if actions.length >= 2 =>
        throw new RuntimeException(s"Deterministic policy has multiple actions for state, $state. The are $actions")
    }

  }

  override def updateProbability(stateAction: StateAction, probability: Double): DeterministicPolicy =
    new DeterministicPolicy(stateActionProbabilityMap + (stateAction -> probability))

  override def updateAction(state: State, action: Action): DeterministicPolicy =
    updateAction(StateAction(state, action))

  def updateAction(stateAction: StateAction): DeterministicPolicy =
    getStateActions(stateAction.state)
      .foldLeft(this)((policy, stateActionForState) => {
        if (stateActionForState.action === stateAction.action) {
          policy.updateProbability(stateActionForState, 1d)
        } else {
          policy.updateProbability(stateActionForState, 0d)
        }
      })

}

object DeterministicPolicy extends Logging {

  def apply(implicit environment: Environment): DeterministicPolicy = {
    new DeterministicPolicy(getRandomStateActionProbabilityMap(environment))
  }

  def apply(stateActionProbabilityMap: Map[StateAction, Double]): DeterministicPolicy = {
    new DeterministicPolicy(stateActionProbabilityMap)
  }

  def getRandomStateActionProbabilityMap(implicit environment: Environment): Map[StateAction, Double] = {
    (for {
      state <- environment.getNonTerminalStates
      actions = environment.getActionsForState(state)
      randomAction = actions(Random.nextInt(actions.length))
      action <- actions
    } yield {
      if (action === randomAction) {
        (StateAction(state, action), 1d)
      } else {
        (StateAction(state, action), 0d)
      }
    }).toMap
  }

  def isDeterministic(policy: Policy): Boolean = {
    (for {
      state <- policy.getStates
      actionForState = policy.getAction(state)
      stateActions = policy.getStateActions(state)
      stateAction <- stateActions
      probability = policy.getProbability(stateAction)
    } yield {
      if (stateAction.action === actionForState) {
        probability === 1d
      } else {
        probability === 0d
      }
    }).reduce(_ && _)
  }

  def isEquivalent(deterministicPolicy: DeterministicPolicy, policy: Policy): Boolean = {
    (for {
      state <- deterministicPolicy.getStates
      actionForDeterministicPolicy = deterministicPolicy.getAction(state)
      actionForPolicy = policy.getMostProbableAction(state)
    } yield {
      actionForDeterministicPolicy === actionForPolicy
    }).reduce(_ && _)
  }

}
