package ie.nix.rl.policy.vf

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.vf.AbstractValueFunction.{getValueMap, randomValue}

class ConstantStepSize private[policy] (val valueMap: Map[StateAction, Double])
    extends AbstractValueFunction[Double]
    with Serializable {

  override def toString: String = s"ConstantStepSize $valueMap"

  override def mapValue(value: Double): Double = value

  override def updateValue(stateAction: StateAction, error: Double): ConstantStepSize =
    new ConstantStepSize(valueMap + (stateAction -> newValue(stateAction, error)))

  def newValue(stateAction: StateAction, error: Double): Double = {
    val originalValue = getValue(stateAction)
    // error = α(target − V(St))
    // V(St) ← V(St) + error
    originalValue + error
  }

}

object ConstantStepSize {

  def apply(policy: Policy): ConstantStepSize =
    new ConstantStepSize(getValueMap[Double](policy)(randomValue))

  def apply(policy: Policy, initialValue: Double): ConstantStepSize = {
    new ConstantStepSize(getValueMap[Double](policy)(() => initialValue))
  }

}
