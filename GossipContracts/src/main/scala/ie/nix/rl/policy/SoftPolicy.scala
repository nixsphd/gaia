package ie.nix.rl.policy

import ie.nix.rl.Environment
import ie.nix.rl.Environment.{Action, NullAction, State, StateAction}
import ie.nix.rl.policy.Policy.Determinism
import org.apache.logging.log4j.scala.Logging
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

class SoftPolicy(protected val stateActionProbabilityMap: Map[StateAction, Double],
                 val probabilityOfRandomAction: Double)
    extends Policy
    with Determinism
    with Serializable
    with Logging {

  override def toString: String = s" $probabilityOfRandomAction,  ${super[Policy].toString}"

  override def getAction(state: State): Action = state.isTerminal match {
    case false =>
      val stateActionProbabilitiesForState = getStateActionAndProbabilities(state)
      assume(stateActionProbabilitiesForState.nonEmpty, s"No possible actions for state $state, $getStateActions")
      logger.debug(s"state=$state, stateActionProbabilitiesForState=$stateActionProbabilitiesForState")
      val probability = Random.nextDouble()
      val stateAction = selectItem[StateAction](stateActionProbabilitiesForState, probability)
      logger.debug(s"$this probability=$probability, state=$state -> action=${stateAction.action}")
      stateAction.action
    case _ => NullAction
  }

  override def updateProbability(stateAction: StateAction, probability: Double): SoftPolicy =
    new SoftPolicy(stateActionProbabilityMap + (stateAction -> probability), probabilityOfRandomAction)

  override def updateAction(state: State, action: Action): SoftPolicy = {
    updateAction(StateAction(state, action))
  }

  def updateAction(stateAction: StateAction): SoftPolicy = {
    logger trace s"~stateAction=$stateAction"
    val stateActions = getStateActions(stateAction.state)
    val probabilityOfNonGreedyAction = probabilityOfRandomAction / stateActions.size
    val probabilityOfGreedyAction = 1d - probabilityOfRandomAction + probabilityOfNonGreedyAction
    stateActions
      .foldLeft(this)((policy, stateActionForState) => {
        if (stateActionForState.action === stateAction.action) {
          policy.updateProbability(stateActionForState, probabilityOfGreedyAction)
        } else {
          policy.updateProbability(stateActionForState, probabilityOfNonGreedyAction)
        }
      })
  }

}

object SoftPolicy extends Logging {

  def apply(probabilityOfRandomAction: Double)(implicit environment: Environment): SoftPolicy = {
    new SoftPolicy(getRandomStateActionProbabilityMap(probabilityOfRandomAction)(environment),
                   probabilityOfRandomAction)
  }

  def apply(probabilityOfRandomAction: Double, policy: ie.nix.rl.policy.Policy): SoftPolicy = {
    new SoftPolicy(getSoftStateActionProbabilityMap(probabilityOfRandomAction, policy), probabilityOfRandomAction)
  }

  def getRandomStateActionProbabilityMap(probabilityOfRandomAction: Double)(
      implicit environment: Environment): Map[StateAction, Double] = {
    (for {
      state <- environment.getNonTerminalStates
      actions = environment.getActionsForState(state)
      randomlySelectedAction = actions(Random.nextInt(actions.length))
      probabilityOfOtherActions = probabilityOfRandomAction / actions.size
      probabilityOfRandomlySelectedAction = 1d - probabilityOfRandomAction + probabilityOfOtherActions
      action <- actions
    } yield {
      if (action === randomlySelectedAction) {
        (StateAction(state, action), probabilityOfRandomlySelectedAction)
      } else {
        (StateAction(state, action), probabilityOfOtherActions)
      }
    }).toMap
  }

  def getSoftStateActionProbabilityMap(probabilityOfRandomAction: Double,
                                       policy: ie.nix.rl.policy.Policy): Map[StateAction, Double] = {
    (for {
      state <- policy.getStates
      actions = policy.getActions(state)
      selectedAction = policy.getMostProbableAction(state)
      probabilityOfOtherActions = probabilityOfRandomAction / actions.size
      probabilityOfSelectedAction = 1d - probabilityOfRandomAction + probabilityOfOtherActions
      action <- actions
    } yield {
      if (action === selectedAction) {
        (StateAction(state, action), probabilityOfSelectedAction)
      } else {
        (StateAction(state, action), probabilityOfOtherActions)
      }
    }).toMap
  }

  def isEquivalent(softPolicy: SoftPolicy, policy: Policy): Boolean = {
    (for {
      state <- softPolicy.getStates
      actionForDeterministicPolicy = softPolicy.getAction(state)
      actionForSoftPolicy = policy.getMostProbableAction(state)
    } yield {
      actionForDeterministicPolicy === actionForSoftPolicy
    }).reduce(_ && _)
  }

}
