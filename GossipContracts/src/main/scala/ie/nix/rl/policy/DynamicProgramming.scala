package ie.nix.rl.policy

import ie.nix.rl.Environment.StateAction
import ie.nix.rl.Transitions.Transition
import ie.nix.rl.peersim.{Environment, RewardsMap, TransitionsMap}
import ie.nix.rl.policy.DynamicProgramming.{EnvironmentModel, PolicyActionValueFunction}
import ie.nix.rl.policy.vf.AbstractValueFunction.difference
import ie.nix.rl.policy.vf.ActionValueFunction
import org.apache.logging.log4j.scala.Logging

import scala.sys.exit

class DynamicProgramming(val discount: Double) extends Logging {

  def evaluateEnvironment(environment: EnvironmentModel, numberOfSweeps: Int): PolicyActionValueFunction = {
    logger info s"environment=$environment"
    val policy = DeterministicPolicy(environment)
    val actionValueFunction = ActionValueFunction(policy, initialValue = 0d)
    (1 to numberOfSweeps)
      .foldLeft(PolicyActionValueFunction(policy, actionValueFunction))((policyActionValueFunction, _) => {
        sweep(environment, policyActionValueFunction)
      })
  }

  def evaluateEnvironment(environment: EnvironmentModel,
                          delta: Double,
                          maxNumberOfSweeps: Int): PolicyActionValueFunction = {

    def evaluateEnvironment(policyActionValueFunction: PolicyActionValueFunction,
                            maxNumberOfSweepsRemaining: Int): PolicyActionValueFunction = {
      val updatedPolicyActionValueFunction = sweep(environment, policyActionValueFunction)
      val actionValueFunctionDifference =
        difference(policyActionValueFunction.actionValueFunction, updatedPolicyActionValueFunction.actionValueFunction)
      logger trace s"diff=$actionValueFunctionDifference, maxNumberOfSweepsRemaining=$maxNumberOfSweepsRemaining"

      if (actionValueFunctionDifference < delta || maxNumberOfSweepsRemaining == 0) {
        updatedPolicyActionValueFunction
      } else {
        evaluateEnvironment(updatedPolicyActionValueFunction, maxNumberOfSweepsRemaining - 1)
      }
    }

    val policy = DeterministicPolicy(environment)
    val actionValueFunction = ActionValueFunction(policy, initialValue = 0d)
    val policyActionValueFunction = PolicyActionValueFunction(policy, actionValueFunction)
    evaluateEnvironment(policyActionValueFunction, maxNumberOfSweeps)

  }

  protected def sweep(environment: EnvironmentModel,
                      policyActionValueFunction: PolicyActionValueFunction): PolicyActionValueFunction = {
    val updatedActionValueFunction =
      backupAllState(environment, policyActionValueFunction.policy, policyActionValueFunction.actionValueFunction)
    val improvedPolicy = improvePolicy(policyActionValueFunction.policy, updatedActionValueFunction)
    PolicyActionValueFunction(improvedPolicy, updatedActionValueFunction)
  }

  protected def improvePolicy(policy: DeterministicPolicy,
                              actionValueFunction: ActionValueFunction): DeterministicPolicy = {
    PolicyImprover.improvePolicy(policy, actionValueFunction) match {
      case policy: DeterministicPolicy => policy
      case notASoftPolicy =>
        logger error s"Fatal Error, expected a SoftPolicy, " +
          s"but got a ${notASoftPolicy.getClass.getSimpleName}"
        exit
    }
  }

  protected def backupAllState(environment: EnvironmentModel,
                               policy: DeterministicPolicy,
                               actionValueFunction: ActionValueFunction): ActionValueFunction = {
    val newActionValueFunction = policy.getStateActions
      .foldLeft(ActionValueFunction(policy, 0d))((newActionValueFunction, stateAction) => {
        val newStateActionValue = backupStateAction(environment, stateAction, policy, actionValueFunction)
        logger.trace(s"~stateAction=$stateAction, newStateActionValue=$newStateActionValue")
        newActionValueFunction.updateValue(stateAction, newStateActionValue)
      })
    newActionValueFunction

  }

  protected def backupStateAction(environment: EnvironmentModel,
                                  stateAction: StateAction,
                                  policy: DeterministicPolicy,
                                  actionValueFunction: ActionValueFunction): Double = {

    val transitionProbabilities = environment.transitionProbabilities(stateAction)
    logger trace s"$stateAction, transitionProbabilities=$transitionProbabilities"
    val stateActionBackUp = (for {
      (transition, probability) <- transitionProbabilities
      reward = environment.getReward(transition)
      // p(s'|a,s) * (reward(s,a,s') + (y * q(s'a')))
      transitionBackup = probability * (reward + (discount * nextStateActionValue(transition,
                                                                                  policy,
                                                                                  actionValueFunction)))
    } yield transitionBackup).sum
    logger trace s"$stateAction => $stateActionBackUp"
    stateActionBackUp
  }

  protected def nextStateActionValue(transition: Transition,
                                     policy: DeterministicPolicy,
                                     actionValueFunction: ActionValueFunction): Double =
    transition.isTerminal match {
      case true => 0d
      case _ =>
        val nextState = transition.finalState // S'
        val nextAction = policy.getAction(nextState); // A'
        val nextStateAction = StateAction(nextState, nextAction) // S'A'
        actionValueFunction.getValue(nextStateAction)
    }

}

object DynamicProgramming extends Logging {

  type EnvironmentModel = Environment with TransitionsMap with RewardsMap

  case class PolicyActionValueFunction(policy: DeterministicPolicy, actionValueFunction: ActionValueFunction)

  def apply(discount: Double): DynamicProgramming =
    new DynamicProgramming(discount)

}
