package ie.nix.rl.policy.vf
import ie.nix.rl.Environment
import ie.nix.rl.Environment.StateAction
import ie.nix.rl.policy.Policy
import ie.nix.rl.policy.vf.AbstractValueFunction.{getValueMap, randomValue}

class ActionValueFunction(protected val valueMap: Map[Environment.StateAction, Double])
    extends AbstractValueFunction[Double]
    with Serializable {

  override protected def mapValue(value: Double): Double = value

  override def updateValue(stateAction: StateAction, value: Double): ActionValueFunction =
    new ActionValueFunction(valueMap + (stateAction -> value))

}

object ActionValueFunction {

  def apply(policy: Policy): ActionValueFunction = {
    new ActionValueFunction(getValueMap[Double](policy)(randomValue))
  }

  def apply(policy: Policy, initialValue: Double): ActionValueFunction = {
    new ActionValueFunction(getValueMap[Double](policy)(() => initialValue))
  }

  def asCSV(avf: ActionValueFunction): String =
    avf.valueMap.toVector
      .sortBy(_._1.toString())
      .map(entry => Vector(entry._1.state, entry._1.action, entry._2).mkString(","))
      .mkString(start = "State,Action,Value\n", sep = "\n", end = "\n")

}
