package ie.nix.rl.policy.vf

import ie.nix.rl.Environment.{Action, NullAction, State, StateAction}
import ie.nix.rl.policy.Policy
import org.scalactic.TypeCheckedTripleEquals._

import scala.util.Random

trait AbstractValueFunction[V] {

  override def toString: String = {
    valueMap.toVector
      .sortBy(_._1.toString)
      .mkString(start = getClass.getSimpleName + "(", sep = ", ", end = ")")
  }

  protected val valueMap: Map[StateAction, V]

  def getStateActions: Vector[StateAction] = valueMap.keySet.toVector

  def getValue(stateAction: StateAction): Double =
    valueMap.get(stateAction: StateAction) match {
      case Some(value) => mapValue(value)
      case None        => 0d // assume state not stored in the map are terminal, value is always = 0
    }

  def getStateActionsValues(state: State): Vector[(StateAction, Double)] =
    getStateActions
      .filter(stateAction => stateAction.state === state)
      .map(stateAction => (stateAction, getValue(stateAction)))

  def getMaxActionValueForState(state: State): Double =
    state.isTerminal match {
      case true => 0d
      case _ =>
        getStateActionsValues(state)
          .map(_._2) // map to value Value
          .max
    }

  def getBestActionForState(state: State): Action =
    state.isTerminal match {
      case true => NullAction
      case _ =>
        getStateActionsValues(state)
          .maxBy(_._2) // max by Value
          ._1 // get StateAction
          .action // get Action
    }

  protected def mapValue(value: V): Double

  def updateValue(stateAction: StateAction, value: Double): AbstractValueFunction[V]

}

object AbstractValueFunction {

  def getValueMap[V](policy: Policy)(initialValue: () => V): Map[StateAction, V] =
    (for { stateAction <- policy.getStateActions } yield {
      (stateAction, initialValue())
    }).toMap

  val randomValue: () => Double = () => Random.nextDouble()

  def difference(anValueFunction: AbstractValueFunction[_], anotherValueFunction: AbstractValueFunction[_]): Double = {
    val states = (anValueFunction.valueMap.keySet ++ anotherValueFunction.valueMap.keySet).toVector
    val difference = (for { state <- states } yield {
      val aValue = anValueFunction.getValue(state)
      val otherValue = anotherValueFunction.getValue(state)
      math.abs(aValue - otherValue)
    }).sum
    difference
  }

}
