package ie.nix.rl.policy

import ie.nix.rl.Environment.{State, StateAction}
import ie.nix.rl.policy.Policy.Determinism
import ie.nix.rl.policy.vf.AbstractValueFunction
import org.apache.logging.log4j.scala.Logging

object PolicyImprover extends Logging {

  def improvePolicy(policy: Policy with Determinism,
                    actionValueFunction: AbstractValueFunction[_]): Policy with Determinism = {
    policy.getStates.foldLeft(policy)((policy, state) => {
      improvePolicyForState(actionValueFunction, policy, state)
    })
  }

  protected def improvePolicyForState(actionValueFunction: AbstractValueFunction[_],
                                      policy: Policy with Determinism,
                                      state: State): Policy with Determinism = {
    val policyAction = policy.getMostProbableAction(state)
    val policyActionValue = actionValueFunction.getValue(StateAction(state, policyAction))
    val bestAction = actionValueFunction.getBestActionForState(state)
    val bestActionValue = actionValueFunction.getValue(StateAction(state, bestAction))
    logger trace s"state=$state, policyAction=$policyAction, bestAction=$bestAction"
    logger trace s"state=$state, policyActionValue=$policyActionValue, bestActionValue=$bestActionValue"

    if (policyAction != bestAction && policyActionValue < bestActionValue) {
      val updatedPolicy = policy.updateAction(state, bestAction)
      logger trace s"Improved action for $state to $bestAction value=$bestActionValue " +
        s"from $policyAction value=$policyActionValue"
      updatedPolicy
    } else {
      policy
    }
  }
}
