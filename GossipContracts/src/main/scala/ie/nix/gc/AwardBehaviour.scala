package ie.nix.gc

import ie.nix.gc.message.ContractMessage.ContractId
import ie.nix.gc.message.{AwardMessage, BidMessage, MessageFactory}
import ie.nix.peersim.Scheduler.Callback
import ie.nix.peersim.Utils.now
import ie.nix.peersim.{MessageStore, Node, Protocol}

import scala.util.Random

trait AwardBehaviour[T, P] {
  agent: Protocol with MessageFactory[T, P] with MessageStore =>

  val tenderTimeout: Int
  val awardTimeout: Int

  def handleBidMessage(bidMessage: BidMessage[T, P]): Unit = {
    logger trace s"[$now]~node=$node, bidMessage=$bidMessage"
    addMessage(bidMessage)
  }

  def awardBehavior(node: Node, contractId: ContractId): Unit = {
    logger trace s"[$now]~node=$node tenderTimeout=$tenderTimeout"
    addCallback(tenderTimeout, Callback(() => evaluateBids(contractId)))
  }

  def evaluateBids(contractId: ContractId): Unit = {
    logger trace s"[$now]~node=$node"
    maybeTopBid(contractId) match {
      case Some(topBid) =>
        if (shouldAward(topBid)) {
          award(topBid)
        }
      case _ => // nothing to do
    }
  }

  def maybeTopBid(contractId: ContractId): Option[BidMessage[T, P]] = {
    val topBids = topBidMessages(contractId)
    if (topBids.nonEmpty)
      Some(Random.shuffle(topBids).head)
    else
      None
  }

  def topBidMessages(contractId: ContractId): Vector[BidMessage[T, P]] = {
    val bidMessages = liveReceivedMessages[BidMessage[T, P]].filter(_.contractId == contractId)
    val sortedBidMessages = sortBidMessages(bidMessages)
    sortedBidMessages.filter(_._2 == sortedBidMessages.head._2).map(_._1)
  }

  def sortBidMessages(bidMessages: Vector[BidMessage[T, P]]): Vector[(BidMessage[T, P], Double)] =
    bidMessages
      .map(bidMessage => (bidMessage, bidToDouble(bidMessage)))
      .sortBy(_._2)

  def award(bidMessage: BidMessage[T, P]): Unit = {
    logger trace s"[$now]~node=$node"
    val expirationTime: Long = now + awardTimeout
    val awardMessage = AwardMessage(bidMessage, expirationTime)
    logger debug s"[$now]~Sending $awardMessage"

    addMessage(awardMessage)
    sendMessage(awardMessage)
    award(awardMessage)

  }

  def bidToDouble(bidMessage: BidMessage[T, P]): Double = -bidMessage.offer

  def shouldAward(bidMessage: BidMessage[T, P]): Boolean

  def award(awardMessage: AwardMessage[T, P]): Unit

}
