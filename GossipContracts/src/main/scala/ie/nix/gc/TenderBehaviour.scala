package ie.nix.gc

import ie.nix.gc.message.MessageFactory
import ie.nix.peersim.Utils.now
import ie.nix.peersim.{Gossiping, MessageStore, Node}

trait TenderBehaviour[T, P] {
  agent: GCAgent[T, P] with Gossiping with MessageFactory[T, P] with MessageStore =>

  val tenderTimeout: Int
  val tenderGossipCount: Int

  def tenderBehaviour(node: Node): Option[Int] = {
    if (agent.isIdle && shouldTender(node)) {
      val expirationTime: Long = now + tenderTimeout
      val task: T = getTask(node)
      val guide: Double = getGuide(node, task)
      val tenderMessage = TenderMessage(node, node, expirationTime, tenderGossipCount, task, guide)
      logger debug s"[$now]~Sending tenderMessage=$tenderMessage"

      addMessage(tenderMessage)
      gossipMessage(tenderMessage)

      Some(tenderMessage.contractId)
    } else {
      logger debug s"[$now]~Not tendering"
      None
    }

  }

  def shouldTender(node: Node): Boolean

  def getTask(node: Node): T

  def getGuide(node: Node, task: T): Double

}
