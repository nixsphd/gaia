package ie.nix.gc

import ie.nix.gc.message.{MessageFactory, TenderMessage}
import ie.nix.peersim.Scheduler.Callback
import ie.nix.peersim.Utils.now
import ie.nix.peersim.{Gossiping, MessageStore}

import scala.util.Random

trait BidBehaviour[T, P] {
  agent: GCAgent[T, P] with Gossiping with MessageFactory[T, P] with MessageStore =>

  val bidBuffer: Int

  def handleTenderMessage(tenderMessage: TenderMessage[T]): Unit = {
    logger trace s"[$now]~node=$node, tenderMessage=$tenderMessage"
    if (!hasTenderMessagesForContract(tenderMessage)) {
      addMessage(tenderMessage)
      addCallback(bidEvaluationDelay(tenderMessage), Callback(() => evaluateTenders()))
      gossipMessage(tenderMessage)
    }
  }

  def hasTenderMessagesForContract(tenderMessage: TenderMessage[T]): Boolean =
    receivedMessages[TenderMessage[T]].count(_.contractId == tenderMessage.contractId) > 0

  def bidEvaluationDelay(tenderMessage: TenderMessage[T]): Int =
    (tenderMessage.expiresAt - now).toInt - bidBuffer

  def evaluateTenders(): Unit = {
    maybeTopTenderExpiringSoon match {
      case Some(topTenderMessageExpiringSoon) =>
        if (isIdle && shouldBid(topTenderMessageExpiringSoon)) {
          bid(topTenderMessageExpiringSoon)
        }
      case _ => // nothing to do
    }
  }

  def maybeTopTenderExpiringSoon: Option[TenderMessage[T]] = {
    val theTopTenderMessagesExpiringSoon = topTenderMessagesExpiringSoon
    if (theTopTenderMessagesExpiringSoon.nonEmpty) {
      Some(Random.shuffle(theTopTenderMessagesExpiringSoon).head)
    } else
      None
  }

  def topTenderMessagesExpiringSoon: Vector[TenderMessage[T]] = topTenderMessages.filter(isExpiringSoon)

  def isExpiringSoon(tenderMessage: TenderMessage[T]): Boolean =
    (tenderMessage.expiresAt - bidBuffer - now) <= 0

  def topTenderMessages: Vector[TenderMessage[T]] = {
    val theSortedTenderMessages = sortTenderMessages(liveReceivedMessages[TenderMessage[T]])
    logger trace s"theSortedTenderMessages=$theSortedTenderMessages"
    theSortedTenderMessages.filter(_._2 == theSortedTenderMessages.head._2).map(_._1)
  }

  def sortTenderMessages(tenderMessages: Vector[TenderMessage[T]]): Vector[(TenderMessage[T], Double)] =
    tenderMessages
      .map(tenderMessage => (tenderMessage, tenderToDouble(tenderMessage)))
      .sortBy(_._2)

  def bid(tenderMessage: TenderMessage[T]): Unit = {
    logger trace s"[$now]~node=$node"

    val proposal = getProposal(tenderMessage)
    val offer = getOffer(tenderMessage, proposal)
    val bidMessage = BidMessage(tenderMessage, proposal, offer)
    logger debug s"[$now]~Sending $bidMessage"

    addMessage(bidMessage)
    sendMessage(bidMessage)
  }

  def tenderToDouble(tenderMessage: TenderMessage[T]): Double = -tenderMessage.guide

  def shouldBid(tenderMessage: TenderMessage[T]): Boolean

  def getProposal(tenderMessage: TenderMessage[T]): P

  def getOffer(tenderMessage: TenderMessage[T], proposal: P): Double

}
