package ie.nix.gc.message

import ie.nix.gc.message.ContractMessage.ContractId
import ie.nix.peersim.{EventHandler, Expirations, Node}

abstract class ContractMessage protected (from: Node, to: Node, val expiresAt: Long, val contractId: ContractId)
    extends EventHandler.Message(from, to)
    with Expirations {

  override def toString: String = s"${super.toString}, contractId=$contractId"

}

object ContractMessage {
  type ContractId = Int
}
