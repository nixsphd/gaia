package ie.nix.gc.message

import ie.nix.gc.message.ContractMessage.ContractId
import ie.nix.gc.message.MessageFactory.nextContractId
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

trait MessageFactory[T, P] extends Logging {

  def TenderMessage(from: Node,
                    to: Node,
                    expirationTime: Long,
                    gossipCounter: Int,
                    task: T,
                    guide: Double): TenderMessage[T] =
    new TenderMessage[T](from, to, expirationTime, nextContractId, gossipCounter, task, guide)

  def TenderMessage(tenderMessage: TenderMessage[T], to: Node, gossipCounter: Int): TenderMessage[T] =
    new TenderMessage[T](tenderMessage.from,
                         to,
                         tenderMessage.expiresAt,
                         tenderMessage.contractId,
                         gossipCounter,
                         tenderMessage.task,
                         tenderMessage.guide)

  def BidMessage(tenderMessage: TenderMessage[T], proposal: P, offer: Double): BidMessage[T, P] = {
    new BidMessage(tenderMessage.to,
                   tenderMessage.from,
                   tenderMessage.expiresAt,
                   tenderMessage.contractId,
                   tenderMessage.task,
                   proposal,
                   offer)
  }

  def AwardMessage(bidMessage: BidMessage[T, P], expirationTime: Long): AwardMessage[T, P] = {
    new AwardMessage(bidMessage.to,
                     bidMessage.from,
                     expirationTime,
                     bidMessage.contractId,
                     bidMessage.task,
                     bidMessage.proposal)

  }

}

object MessageFactory {

  private var _nextContractId: ContractId = 1000

  protected def nextContractId: ContractId = {
    _nextContractId += 1
    _nextContractId - 1
  }

  /*
  private var messageCounts = Map[Class[_ <: ContractMessage[_]], Long]()

  def incrementMessageCount(messageClass: Class[_ <: ContractMessage]): Unit =
    messageCounts.get(messageClass) match {
      case Some(messageCount) => messageCounts += messageClass -> messageCount.+(1L)
      case _                  => messageCounts += messageClass -> 1L
    }

  def messageCount(messageClass: Class[_ <: ContractMessage]): Long =
    messageCounts.get(messageClass) match {
      case Some(messageCount) => messageCount
      case _                  => 0L
    }

  def resetMessageCount(): Unit =
    messageCounts = Map[Class[_ <: ContractMessage], Long]()

  def allMessagesCount: Long =
    messageCounts.values.map(_.longValue).sum
 */

}
