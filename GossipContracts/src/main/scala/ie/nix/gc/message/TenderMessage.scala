package ie.nix.gc.message

import ie.nix.gc.message.ContractMessage.ContractId
import ie.nix.peersim.Gossiping.Gossips
import ie.nix.peersim.Node
import org.apache.logging.log4j.scala.Logging

class TenderMessage[T] private[message] (from: Node,
                                         to: Node,
                                         expirationTime: Long,
                                         contractID: ContractId,
                                         val gossipCounter: Int,
                                         val task: T,
                                         val guide: Double)
    extends ContractMessage(from, to, expirationTime, contractID)
    with Gossips
    with Logging {

  override def toString: String =
    s"TenderMessage $from -> $to, expirationTime=$expirationTime, gossipCounter=$gossipCounter, contractID=$contractID, task=$task, guide=$guide"

  override def copy(to: Node, gossipCounter: Int): TenderMessage[T] =
    new TenderMessage[T](from, to, expirationTime, contractID, gossipCounter, task, guide)

}
