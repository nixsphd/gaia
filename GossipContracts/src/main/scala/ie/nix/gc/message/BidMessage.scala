package ie.nix.gc.message

import ie.nix.gc.message.ContractMessage.ContractId
import ie.nix.peersim.Node

class BidMessage[T, P] private[message] (from: Node,
                                         to: Node,
                                         expirationTime: Long,
                                         contractID: ContractId,
                                         val task: T,
                                         val proposal: P,
                                         val offer: Double)
    extends ContractMessage(from, to, expirationTime, contractID) {

  override def toString: String = s"${super.toString}, proposal=$proposal, offer=$offer"

}
