package ie.nix.gc.message

import ie.nix.gc.message.ContractMessage.ContractId
import ie.nix.peersim.Node

class AwardMessage[T, P] private[message] (from: Node,
                                           to: Node,
                                           expirationTime: Long,
                                           contractID: ContractId,
                                           val task: T,
                                           val proposal: P)
    extends ContractMessage(from, to, expirationTime, contractID) {

  override def toString: String = s"${super.toString},  task=$task, proposal=$proposal"

}
