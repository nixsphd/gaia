package ie.nix.gc

import ie.nix.gc.GCAgentLogger.getProtocolId
import ie.nix.peersim.Logger
import ie.nix.peersim.Utils.{now, protocolsAs}
import peersim.config.Configuration

class GCAgentLogger(val prefix: String, protocolID: Int) extends Logger(prefix) {

  def this(prefix: String) = this(prefix, getProtocolId(prefix))

  override def init(): Boolean = {
    csvFileWriter.writeHeaders("time", "tenderingHosts", "biddingHosts", "awardingHosts", "beingAwardedHosts")
    false
  }

  override def log: Unit = {
    csvFileWriter.writeRow(now, numberOfTendering, numberOfBidding, numberOfAwarding, numberOfBeingAwarded)
  }

  override def observe: Unit = {
    logger info s"[$now]~numberOfTendering=$numberOfTendering, numberOfBidding=$numberOfBidding, " +
      s"numberOfAwarding=$numberOfAwarding, numberOfBeingAwarded=$numberOfBeingAwarded"
  }

  def gcAgents: Vector[GCAgent[_, _]] = protocolsAs[GCAgent[_, _]](protocolID)
  def numberOfTendering: Int = gcAgents.count(_.isTendering)
  def numberOfBidding: Int = gcAgents.count(_.isBidding)
  def numberOfAwarding: Int = gcAgents.count(_.isAwarding)
  def numberOfBeingAwarded: Int = gcAgents.count(_.isBeingAwarded)
}

object GCAgentLogger {

  private val AGENT = "gc_agent"

  def getProtocolId(prefix: String): Int =
    Configuration.getPid(prefix + "." + AGENT)

}
