package ie.nix.gc

import ie.nix.gc.message.{AwardMessage, MessageFactory}
import ie.nix.peersim.{MessageStore, Protocol}
import ie.nix.peersim.Utils.now

trait AwardedBehaviour[T, P] {
  agent: Protocol with MessageFactory[T, P] with MessageStore =>

  final def handleAwardMessage(awardMessage: AwardMessage[T, P]): Unit = {
    logger trace s"[$now]~awardMessage=$awardMessage"
    addMessage(awardMessage)
    awarded(awardMessage)
  }

  def awarded(awardMessage: AwardMessage[T, P]): Unit

}
