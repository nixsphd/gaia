package ie.nix.gc

import ie.nix.gc.GCAgent.{getAwardTimeout, getBidBuffer, getTenderGossipCount, getTenderTimeout}
import ie.nix.gc.message.ContractMessage.ContractId
import ie.nix.gc.message._
import ie.nix.peersim.Scheduler.Callback
import ie.nix.peersim.Utils.now
import ie.nix.peersim.{EventHandler, Gossiping, MessageStore, Node, Protocol}
import org.apache.logging.log4j.scala.Logging
import peersim.config.Configuration

abstract class GCAgent[T, P](prefix: String,
                             val tenderTimeout: Int,
                             val tenderGossipCount: Int,
                             val bidBuffer: Int,
                             val awardTimeout: Int)
    extends Protocol(prefix)
    with Gossiping
    with TenderBehaviour[T, P]
    with BidBehaviour[T, P]
    with AwardBehaviour[T, P]
    with AwardedBehaviour[T, P]
    with MessageFactory[T, P]
    with MessageStore
    with Logging {

  logger trace s"tenderTimeout=$tenderTimeout, tenderGossipCount=$tenderGossipCount, bidBuffer=$bidBuffer, awardTimeout=$awardTimeout"

  def this(prefix: String) =
    this(prefix, getTenderTimeout(prefix), getTenderGossipCount(prefix), getBidBuffer(prefix), getAwardTimeout(prefix))

  def isTendering: Boolean = hasLiveSentMessage[TenderMessage[T]]
  def isBidding: Boolean = hasLiveSentMessage[BidMessage[T, P]]
  def isAwarding: Boolean = hasLiveSentMessage[AwardMessage[T, P]]
  def isBeingAwarded: Boolean = hasLiveReceivedMessage[AwardMessage[T, P]]
  def isManaging: Boolean = isTendering || isAwarding
  def isContracting: Boolean = isBidding || isBeingAwarded
  def isIdle: Boolean = !isManaging && !isContracting

  def nextCycle(node: Node): Unit = {
    logger trace s"[$now]~node=$node, id=${node.getIndex}, isUp=${node.isUp}"
    preTenderAwardCycle(node)
    val maybeContractId = tenderBehaviour(node)
    maybeContractId match {
      case Some(contractId: ContractId) =>
        awardBehavior(node, contractId)
        addCallback(postTenderDelay, Callback(() => postTenderAwardCycle(node)))
      case None =>
        postTenderAwardCycle(node)
    }
    clearExpiredMessages()
  }

  def postTenderDelay: Int = tenderTimeout + awardTimeout + 1
  def preTenderAwardCycle(node: Node): Unit = { logger trace s"[$now] node=$node" }
  def postTenderAwardCycle(node: Node): Unit = { logger trace s"[$now] node=$node" }

  override def processEvent(node: Node, message: EventHandler.Message): Unit = {
    logger trace s"[$now]~node=$node, message=$message"
    message match {
      case tenderMessage: TenderMessage[T]  => handleTenderMessage(tenderMessage)
      case bidMessage: BidMessage[T, P]     => handleBidMessage(bidMessage)
      case awardMessage: AwardMessage[T, P] => handleAwardMessage(awardMessage)
      case _                                => logger error s"Error: unrecognised message type, ${message.getClass.getName}"
    }
    clearExpiredMessages()
  }

}

object GCAgent {

  private val TENDER_TIMEOUT = "tender-timeout"
  private val TENDER_GOSSIP_COUNTER = "tender-gossip-counter"
  private val BID_BUFFER = "bid-buffer"
  private val AWARD_TIMEOUT = "award-timeout"

  val DEFAULT_TENDER_TIMEOUT = 3
  val DEFAULT_TENDER_GOSSIP_COUNT = 3
  val DEFAULT_BID_BUFFER = 1
  val DEFAULT_AWARD_TIMEOUT = 1

  def getTenderTimeout(prefix: String): Int =
    Configuration.getInt(prefix + "." + GCAgent.TENDER_TIMEOUT, GCAgent.DEFAULT_TENDER_TIMEOUT)
  def getTenderGossipCount(prefix: String): Int =
    Configuration.getInt(prefix + "." + GCAgent.TENDER_GOSSIP_COUNTER, GCAgent.DEFAULT_TENDER_GOSSIP_COUNT)
  def getBidBuffer(prefix: String): Int =
    Configuration.getInt(prefix + "." + GCAgent.BID_BUFFER, GCAgent.DEFAULT_BID_BUFFER)
  def getAwardTimeout(prefix: String): Int =
    Configuration.getInt(prefix + "." + GCAgent.AWARD_TIMEOUT, GCAgent.DEFAULT_AWARD_TIMEOUT)

}
