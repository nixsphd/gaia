#!/usr/bin/python

import os, sys
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats
from scipy.stats import wilcoxon

default_ticks_per_episode = 100 * 10000
avf_file= 'AVF'
policy_file = "Policy"
default_model_types = ["QLearning", "QDLearning", "QLambdaLearning", "QDLambdaLearning"]

def get_seeds(results_dir):
    seed_dirs = [filename for filename in os.listdir(results_dir) if os.path.isdir(os.path.join(results_dir,filename))]
    seeds =  list(map(int, seed_dirs))
    seeds.sort()
    return(seeds)

def get_models(results_dir, seed):
    seed_dir = f"{results_dir}/{seed}"
    models = [filename for filename in os.listdir(seed_dir) if os.path.isdir(os.path.join(seed_dir, filename))]
    models.sort()
    return(models)
 
# 
# process_seeds
# 

# Read data
def read_data(results_dir, observer_file, models, ticks_per_episode=default_ticks_per_episode):
    df = pd.DataFrame()
    for model in models:
        dataset_path = f'{results_dir}/{model}/{observer_file}.csv'
#         print(f"{dataset_path}")
        model_df = pd.read_csv(dataset_path, skipinitialspace=True)
        model_df["Model"] = model
        model_df["Episode"] = (model_df["Time"]/(ticks_per_episode)).apply(np.int64)
        if 'Error' in model_df:
            model_df['Squared Error'] = pow(model_df['Error'], 2)
        df = df.append(model_df)
    return(df)

def save_data(results_dir, observer_file, rmse_models_df, suffix='RMSE'):
    dataset_path = f'{results_dir}/{observer_file}_{suffix}.csv'
    rmse_models_df.to_csv(dataset_path, header=True , index=False, chunksize=100000)
    print(f"Wrote {dataset_path}")
    
def rmse(df, models, rolling_count=5):
    rmse_df = pd.DataFrame()
    for model in models:
        model_df = df[df["Model"] == model]
        model_pivot_df = pd.pivot_table(model_df, index=['Model','Episode'], values=['Error','Squared Error'], aggfunc=np.mean)
        model_pivot_df = pd.DataFrame(model_pivot_df.to_records())
        model_pivot_df['RMSE'] = pow(model_pivot_df['Squared Error'], 0.5)
        model_pivot_df['Rolling Mean RMSE'] = model_pivot_df['RMSE'].rolling(rolling_count).mean()
        rmse_df = rmse_df.append(model_pivot_df)
    return(rmse_df)

def armse(df, seeds, models=[]):
    if models == []:
        models = df['Model'].unique()
    accumulate_field_name = f"ARMSE"
    new_df = pd.DataFrame()
    for model in models:
        for seed in seeds:
            seed_model_df = df[(df['Model'] == model) & (df['Seed'] == seed)]
            accumulate_field = seed_model_df['RMSE'].cumsum()
            seed_model_df = seed_model_df.assign(accumulate_field_name=accumulate_field) 
            seed_model_df.rename(columns={"accumulate_field_name":'ARMSE'}, inplace=True)
            new_df = new_df.append(seed_model_df)
    return(new_df)

# def mean_error(df, seeds, models=[]):
#     if models == []:
#         models = df['Model'].unique()
#     accumulate_field_name = f"ARMSE"
#     new_df = pd.DataFrame()
#     for model in models:
#         for seed in seeds:
#             seed_model_df = df[(df['Model'] == model) & (df['Seed'] == seed)]
#             accumulate_field = seed_model_df['RMSE'].cumsum()
#             seed_model_df = seed_model_df.assign(accumulate_field_name=accumulate_field) 
#             seed_model_df.rename(columns={"accumulate_field_name":'ARMSE'}, inplace=True)
#             new_df = new_df.append(seed_model_df)
#     return(new_df)

def save_rmse_data(results_dir, observer_file, rmse_models_df):
    dataset_path = f'{results_dir}/{observer_file}_RMSE.csv'
    rmse_models_df.to_csv(dataset_path, header=True , index=False, chunksize=100000)
#     print(f"Wrote {dataset_path}")
#     print(".", end='')
    
def read_rmse_data(results_dir, seeds, observer_file):
    df = pd.DataFrame()
    for seed in seeds:
        dataset_path = f'{results_dir}/{seed}/{observer_file}_RMSE.csv'
        seed_df = pd.read_csv(dataset_path)
#         print(f"Read {dataset_path}")
        seed_df["Seed"] = seed
        df = df.append(seed_df)
    df = df.reset_index()
    return(df)

def read_armse_data(results_dir, observer_file):
    dataset_path = f'{results_dir}/{observer_file}_ARMSE.csv'
    armse_df = pd.read_csv(dataset_path)
    return(armse_df)

def clamp_episodes(df, min_episode, max_episode):
    df = df[df['Episode'] >= min_episode]
    df = df[df['Episode'] <= max_episode]
    return(df)

# Summary
def summary_data(rmse_models_df):
    models = list(set(rmse_models_df['Model']))
    df = pd.DataFrame(columns=['Model','Rolling Mean RMSE'])
    for model in models:
        rmse_model_df = rmse_models_df[rmse_models_df['Model'] == model]
        final_rrmse = rmse_model_df.tail(1).iloc[0]['Rolling Mean RMSE']
        row_data = {"Model" : model, "Rolling Mean RMSE" : final_rrmse}
        df = df.append(row_data, ignore_index=True)
    return(df)
    
def save_summary_data(results_dir, observer_file, rmse_models_df):
    dataset_path = f'{results_dir}/{observer_file}_Summary.csv'
    rmse_models_df.to_csv(dataset_path, header=True , index=False, chunksize=100000)
#     print(f"Wrote {dataset_path}")
#     print(".", end='')

def read_summary_data(results_dir, seeds, models, observer_file):
    df = pd.DataFrame()
    for seed in seeds:
        dataset_path = f'{results_dir}/{seed}/{observer_file}_Summary.csv'
        model_df = pd.read_csv(dataset_path)
        model_df["Seed"] = seed
        df = df.append(model_df)
    df = df.reset_index()
    return(df)

# Models

def get_model_name(model):
    result = model.split("_")
    model_name, *params = result
    arrays_of_params = np.array_split(params, len(params)/2)
    for (param_name, param_value) in arrays_of_params:
        model_name = f'{model_name} {param_name}={param_value}'
    return(model_name)

def process_models(results_dir, seeds, observer_file, models):
    for seed in seeds:
        seed_dir = f'{results_dir}/{seed}'
        print(f"Processing seed={seed}")
        df = read_data(seed_dir, observer_file,  models)
        if 'Squared Error' in df:
            rmse_df = rmse(df, models)
            rmse_df['Model Name'] = rmse_df.apply(lambda row: get_model_name(row.Model), axis=1)
            save_rmse_data(seed_dir, observer_file, rmse_df)
        
# 
# overview_data
# 

def overview_data(results_dir, seeds, models, observer_file):
    df = read_summary_data(results_dir, seeds, models, observer_file)
    overview_df = df.groupby(['Model'], as_index=False).agg({'Rolling Mean RMSE':['mean']})
    save_overview_data(results_dir, observer_file, overview_df)
    return(overview_df)

def save_overview_data(results_dir, observer_file, df):
    dataset_path = f'{results_dir}/{observer_file}_Overview.csv'
    df.to_csv(dataset_path, header=True , index=False, chunksize=100000)
    print(f"Wrote {dataset_path}")

def last(df):
    max_time = max(df["Episode"])
    last_df = df[df["Episode"] == max_time]
    return(last_df)

def filter_episodes(df, episodes):
    max_time = max(df["Episode"])
    last_df = df[df["Episode"].isin(episodes)]
    return(last_df)

def best_by_model(df, y='Rolling Mean RMSE'):
    last_df = last(df)
    last_pivot_df= pd.pivot_table(last_df, index=['Model','Model Name'], values=['Rolling Mean RMSE'], aggfunc=np.mean)
    last_pivot_df = pd.DataFrame(last_pivot_df.to_records())
    last_pivot_df = last_pivot_df.sort_values(by='Rolling Mean RMSE').reset_index(drop=True)
    bests = last_pivot_df['Model Name']
    best = bests[0]
    best_values = last_df[last_df["Model Name"] == best]["Rolling Mean RMSE"].values
    for other_best in bests[1:]:
        other_best_values = last_df[last_df["Model Name"] == other_best]["Rolling Mean RMSE"].values
        pvalue = wilcoxon(best_values, other_best_values, alternative="two-sided").pvalue
        last_pivot_df.loc[(last_pivot_df['Model Name'] == other_best), 'p-value'] = pvalue
    return(last_pivot_df)

def best_by_model_type(df, y='Rolling Mean RMSE', model_types=default_model_types):
    last_episode = max(df['Episode'])
    last_df = df[df['Episode'] == last_episode]
    pivot_df = pd.pivot_table(data=last_df, index=['Model','Model Name','Episode'], values=[y])
    pivot_df = pd.DataFrame(pivot_df.to_records())
    pivot_df = pivot_df.sort_values(y)
    best_models = []
    for model_type in model_types:
        best_model = pivot_df[pivot_df.Model.str.startswith(model_type)].iloc[0,0]
        best_models.append(best_model)
    best_models_df = df[df['Model'].isin(best_models)]
    return(best_models_df)

def best_by_model_for_episodes(df, y='Rolling Mean RMSE', episodes = []):
    if not episodes:
        episodes = [max(df["Episode"])]
    episodes_df = filter_episodes(df, episodes)
    episodes_pivot_df = pd.pivot_table(episodes_df, index=['Model','Model Name','Episode'], values=[y], aggfunc=np.mean)
    episodes_pivot_df = pd.DataFrame(episodes_pivot_df.to_records())
    episodes_pivot_df = episodes_pivot_df.sort_values(by=['Episode', y]).reset_index(drop=True)
    bests = episodes_pivot_df[episodes_pivot_df['Episode'] == episodes[0]]['Model Name']
    best = bests.iloc[0]
    best_values = episodes_df[(episodes_df["Model Name"] == best) & (episodes_df["Episode"] == episodes[0])][y].values

    for episode in episodes:
        for other_best in bests[1:]:
            other_best_values = episodes_df[(episodes_df["Model Name"] == other_best) & (episodes_df["Episode"] == episode)][y].values
            pvalue = wilcoxon(best_values, other_best_values, alternative="two-sided").pvalue
            episodes_pivot_df.loc[((episodes_pivot_df['Model Name'] == other_best) & (episodes_pivot_df["Episode"] == episode)), 'p-value'] = pvalue
    return(episodes_pivot_df)

def split_by(df, column_name='Episode'):
    column_values = df[column_name].unique()
    new_df = pd.DataFrame()
    for column_value in column_values:
        new_value_df = df[df[column_name] == column_value]
        new_value_df = new_value_df.drop(['Episode'], axis=1)
        new_column_names = [new_value_df.columns[0]] + list(new_value_df.columns[1:] + f" ({column_name}={column_value})")
        new_value_df.columns = new_column_names
        if (len(new_df.columns) == 0):
            new_df = new_value_df
        else:
            new_df = pd.merge(new_df, new_value_df, left_on="Model Name", right_on="Model Name")
    return(new_df)

def widen_best_by_model_for_episodes(df, y='Rolling Mean RMSE', episodes = []):
    if not episodes:
        episodes = [max(df["Episode"])]
    split_df = split_by(df.drop(['Model'], axis=1))
    split_df.insert(0,'Model Type', split_df['Model Name'].str.split().str.get(0))
    split_df.insert(1,'Configuration', split_df['Model Name'].str.split(' ', 1).str.get(1))
    split_df = split_df.drop('Model Name', axis=1)
    split_df = split_df.sort_values(f'{y} (Episode={episodes[0]})')
    return(split_df)

# 
# tables
# 

def latex_table(df):
    latex_table_str = ""
    for index, row in df.iterrows():
        for value in list(row):
            if type(value) is float:
                if math.isnan(value):
                    latex_table_str += ' - & '
                else: 
                    latex_table_str += '{:.4f}'.format(value)+' & '
            else:
                latex_table_str += value+' & '
        latex_table_str = latex_table_str[:len(latex_table_str) - len(' & ')]
        latex_table_str += ' \\\\ \n'
    return(latex_table_str)

# 
# models_plots
# 

def generate_plot(df, model, y, ax=None):
    model_df = df[df["Model"] == model]
    if ax is not None:
        sns_plot = sns.lineplot(data=model_df, x="Episode", y=y, ax=ax, ci=None)
    else:
        sns_plot = sns.lineplot(data=model_df, x="Episode", y=y, ci=None)
    sns_plot.set_xlabel("Episode")
    sns_plot.set_ylabel(f"{y}")
    sns_plot.legend(loc='upper left', bbox_to_anchor=(1, 1))
    sns_plot.set_title(model)
    sns_plot.grid()
    if (y == 'Error' or y == "SquaredError" or y == "RMSE"):
        sns_plot.axhline(0, ls=':')
    return(sns_plot)

def save_plot(plot_path, sns_plot):
    sns_plot.get_figure().savefig(plot_path)
    print(f"Saved {plot_path}")

def plots(results_dir, models, observer_file, df, y="Value"):
    for model in models:
        plt.figure()
        plot = generate_plot(df, model, y)
        plot_path = f'{results_dir}/{model}/{observer_file}_{y}.png'
        save_plot(plot_path, plot)
        plt.close()

def models_plots(results_dir, seeds, models, observer_file):
    for seed in seeds:
        seed_dir = f'{results_dir}/{seed}'
        models_df = read_data(seed_dir, observer_file, models)
        plots(seed_dir, models, observer_file, models_df, y="Error")
        plots(seed_dir, models, observer_file, models_df, y="Squared Error")

def plot(results_dir, dataset_name, df, y='Rolling Mean RMSE', ylim=None, loc='upper left'):
    sns_plot = sns.lineplot(data=df, x="Episode", y=y, hue='Model Name', ci=None)
    sns_plot.legend(loc=loc) 
    sns_plot.set(ylim=(0, ylim))
    plot_path = f'{results_dir}/{dataset_name}.png'
    save_plot(plot_path, sns_plot)
    return(sns_plot)

def plot_armse(results_dir, dataset_name, df, y='Rolling Mean RMSE', loc='lower right'):
    sns_plot = sns.lineplot(data=df, x="Episode", y=y, hue='Model Name', ci=None)
    sns_plot.legend(loc=loc) 
    sns_plot.set(ylim=(0, None))
    plot_path = f'{results_dir}/{dataset_name}.png'
    save_plot(plot_path, sns_plot)
    return(sns_plot)

def plot_by_type(df, dataset_name, results_dir, y='Rolling Mean RMSE', model_types = default_model_types):
    rows = int(math.floor(len(model_types)/2))
    fig, axes = pyplot.subplots(rows, 2, figsize=(16,6*rows))
    axes = axes.flatten()
    axis = 0
    for model_type in model_types:
        model_type_df = df[df.Model.str.startswith(model_type)]
        sns.lineplot(ax=axes[axis], data=model_type_df, x="Episode", y=y, hue='Model Name', ci=None)
        axes[axis].set_title(model_type)
        axes[axis].legend(loc='upper right', bbox_to_anchor=(1, 1))
        axis = axis + 1
    plot_path = f'{results_dir}/{dataset_name}_by_model_type.png'
    fig.savefig(plot_path)
    print(f"Saved {plot_path}")
    plt.close()
    return(fig)
        
# 
# models_multi_plots
# 

def generate_multi_plot(df, models, y, ax=None):
    if ax is not None:
        sns_plot = sns.lineplot(data=df, x="Episode", y=y, hue="Model Name", ax=ax, ci=None)
    else:
        sns_plot = sns.lineplot(data=df, x="Episode", y=y, hue="Model Name", ci=None)
    sns_plot.set_xlabel("Episode")
    sns_plot.set_ylabel(f"{y}")
    sns_plot.legend(loc='upper left', bbox_to_anchor=(1, 1))
    sns_plot.grid()
    if (y == 'Error' or y == "SquaredError" or y == "RMSE"):
        sns_plot.axhline(0, ls=':')
    return(sns_plot)

def multi_plot(results_dir, observer_file, models, df, y="Error"):
    plt.figure()
    plot = generate_multi_plot(df, models, y)
    plot_path = f'{results_dir}/{observer_file}_{y}.png'
    save_plot(plot_path, plot)
    plt.close()

def models_multi_plots(results_dir, seeds, models, observer_file):
    for seed in seeds:
        seed_dir = f'{results_dir}/{seed}'
        df = read_data(seed_dir, observer_file, models)
        rmse_df = rmse(df)
        multi_plot(seed_dir, observer_file, models, df, y="Error")
        multi_plot(seed_dir, observer_file, models, df, y="Squared Error")
        multi_plot(seed_dir, observer_file, models, rmse_df, y="RMSE")

# 
# Plot the model types showing params impact
# 

def split_model_name(df):
    df = df.reset_index()
    df.insert(0,'Model Type', df['Model Name'].str.split().str.get(0))
    df.insert(1,'Configuration', df['Model Name'].str.split(' ', 1).str.get(1))
    params = df['Configuration'][0].split(' ')
    for param_index in range(len(params)):
        param_name = params[param_index].split('=')[0]
        df.insert(1, param_name, df['Configuration'].str.split(' ').str.get(param_index).str.split('=').str.get(1))
    return(df)

def plot_model_type(df, results_dir, model_type, dataset_name, hue, col=None, row=None, y='Rolling Mean RMSE', col_wrap=None, ylim=0.2):
    model_type_df = df[df.Model.str.startswith(model_type)]
    model_type_df = split_model_name(model_type_df)
    model_type_df = model_type_df.drop('Model Name', axis=1)
    plot_path = f'{results_dir}/{dataset_name}_{y}_by_model_type.eps'
    
    if col is not None:
        if row is not None:
            sns_plot = sns.FacetGrid(model_type_df, col=col, row=row, hue=hue) 
        else:    
            sns_plot = sns.FacetGrid(model_type_df, col=col, hue=hue, col_wrap=col_wrap)
        sns_plot.map_dataframe(sns.lineplot, x="Episode", y=y, ci=None)
        sns_plot.add_legend()
        sns_plot.set(ylim=(0, ylim))
        sns_plot.set_axis_labels("Episode", y)
        plt.subplots_adjust(left=0.1, bottom=0.15)
        sns_plot.fig.savefig(plot_path)
            
    else:
        sns_plot = sns.lineplot(data=model_type_df, x="Episode", y=y, hue=hue, ci=None)
        plt.ylim(0, 0.2)
        sns_plot.get_figure().savefig(plot_path)
    print(f"Saved {plot_path}")
    plt.close()

def main(argv):
    if (len(argv) > 0):
        results_dir = argv[0]   
        seeds = get_seeds(results_dir)
        models = get_models(results_dir, 0)
        if (len(argv) > 1):
            file = argv[1]
        else:
            file = avf_file
        print(f'Processing results_dir={results_dir}')
        print(f'           file={file}')
        print(f'           seeds={seeds}')
        print(f'           models={models}')
        process_models(results_dir, seeds, file, models)
    else:
        print(f"Usage: python experiments.py <results_dir> <Policy>")


if __name__ == "__main__":
    main(sys.argv[1:])

