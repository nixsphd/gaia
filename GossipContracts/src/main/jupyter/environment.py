import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def read_data(results_dir, observer_file):
    dataset_path = f'{results_dir}/{observer_file}.csv'
    print(f"{dataset_path}")
    df = pd.read_csv(dataset_path)
    return(df)

# 
# Enviornment Transion probabilities 
# 
def reshape_tp_df(df, number_of_states):
    df.set_index(['State', 'Action', 'FinalState'], inplace=True)
    df = df.pivot_table(index=['State', 'Action'], columns=['FinalState'], values=['Transition Probability'])
    state_order = list(map(lambda s: f"State{s}", range(1, number_of_states+1)))
    df = df.loc[state_order, :]
    df = df['Transition Probability'][state_order]
    return(df)

def tp_plot_heatmap(plot_dir, number_of_states, observer_file, df):
    if number_of_states == 2:
        fig, ax = plt.subplots(figsize=(1.75, 1.5))
    elif number_of_states == 4:
        fig, ax = plt.subplots(figsize=(3, 2.5))
    elif number_of_states == 8:
        fig, ax = plt.subplots(figsize=(7, 5))
    elif number_of_states == 16:
        fig, ax = plt.subplots(figsize=(12, 8))
    elif number_of_states == 32:
        fig, ax = plt.subplots(figsize=(20, 16))
    else:
        print("ERROR, can't pront heatmay for number_of_states={number_of_states}.") 
    
    heatmap_plot = sns.heatmap(df, cmap="rocket_r", vmin=0, vmax=1, annot=True, fmt=".2f", ax=ax)
    
    plot_path = f'{plot_dir}/s_{number_of_states}_{observer_file}.eps'
    fig.savefig(plot_path, transparent=True, bbox_inches='tight')
    print(f"Saved {plot_path}")
    
# 
# Enviornment DYnamic Programming AVF 
# 
def reshape_dp_avf_df(df, number_of_states):
    df.set_index(['State', 'Action', ], inplace=True)
    df = df.pivot_table(index=['State'], columns=['Action'], values=['Value'])
    state_order = list(map(lambda s: f"State{s}", range(1, number_of_states+1)))
    df = df.loc[state_order, :]
    df = df['Value']
    return(df)


def dp_avf_plot_heatmap(plot_dir, number_of_states, observer_file, df):
    if number_of_states == 2:
        fig, ax = plt.subplots(figsize=(3, 0.75))
    elif number_of_states == 4:
        fig, ax = plt.subplots(figsize=(3, 1.5))
    elif number_of_states == 8:
            fig, ax = plt.subplots(figsize=(3, 3))
    elif number_of_states == 16:
        fig, ax = plt.subplots(figsize=(3, 6))  
    elif number_of_states == 32:
        fig, ax = plt.subplots(figsize=(3, 12))
    else:
        print("ERROR, can't pront heatmay for number_of_states={number_of_states}.") 
    
    heatmap_plot = sns.heatmap(df, cmap="rocket_r", annot=True, fmt=".3f", ax=ax)
    
    plot_path = f'{plot_dir}/s_{number_of_states}_{observer_file}.eps'
    fig.savefig(plot_path, transparent=True, bbox_inches='tight')
    print(f"Saved {plot_path}")