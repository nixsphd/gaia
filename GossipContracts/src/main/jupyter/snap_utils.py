import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

avf_file = "LGCAVF"
policy_file = "LGCPolicy"
snaps_file = "Snaps"
ticks_per_episode = 300*150

# Read data
def read_avf_data(results_dir, model):
    dataset_path = f'{results_dir}/{model}/{avf_file}.csv'
    avf_df = pd.read_csv(dataset_path, skipinitialspace=True)
    avf_df["Episode"] = avf_df["Time"] / ticks_per_episode
    print(f"Read {dataset_path}")
    return(avf_df)

def last_avf_values(df):
    max_time = max(df["Time"])
    last_df = df[(df["Time"] == max_time)]
    last_state_action_values = last_df.groupby(['StateAction'], as_index=False).agg({'Value':['mean','std']})
    last_state_action_values.columns = last_state_action_values.columns.droplevel()
    return(last_state_action_values)

def plot_avf_data(df):
    plt.figure()
    sns_plot = sns.lineplot(data=df, x="Episode", y='Value', hue="StateAction", ci=None)
    sns_plot.set_title("AVF")
    plt.legend(loc='upper left', bbox_to_anchor=(1, 1))

def read_policy_data(results_dir, model):
    dataset_path = f'{results_dir}/{model}/{policy_file}.csv'
    policy_df = pd.read_csv(dataset_path, skipinitialspace=True)
    policy_df["Episode"] = policy_df["Time"] / ticks_per_episode
    policy_df['StateAction'] = policy_df["State"].astype(str) + " " + policy_df["Action"].astype(str)
    print(f"Read {dataset_path}")
    return(policy_df)

def last_policy_values(df):
    max_episode = max(df["Episode"])
    last_df = df[(df["Episode"] == max_episode)]
    return(last_df[['Episode','StateAction','size']])

def plot_policy_actions_data(df):
    policy_actions_df = df.groupby(['Episode', 'Time', 'State', 'Action', 'StateAction'], as_index=False).size()
    plt.figure()
    sns_plot = sns.lineplot(data=policy_actions_df, x="Episode", y='size', hue=("StateAction"), ci=None)
    sns_plot.set_title("Policy")
    plt.ylim([0, 50])
    plt.legend(loc='upper left', bbox_to_anchor=(1, 1))
    return(policy_actions_df)
    
def plot_policy_data(df, y='Value'):
    plt.figure()
    sns_plot = sns.lineplot(data=df, x="Episode", y=y, hue="Agent", ci=None).set_title("Policy")
    plt.legend(loc='upper left', bbox_to_anchor=(1, 1)) 
    return(sns_plot)

def read_snaps_data(results_dir, model, seed):
    dataset_path = f'{results_dir}/{seed}/{model}/{snaps_file}.csv'
    df = pd.read_csv(dataset_path)
    df['Model'] = model
    df['Seed'] = seed
    df["Episode"] = (df["Time"] / ticks_per_episode).apply(np.int64)
    return(df)

def plot_snaps(df):
    sns_plot = sns.lineplot(data=df, x="Episode", y='NumberOfSnaps', legend=False).set_title("Snaps")
    return(sns_plot)