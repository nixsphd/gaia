#!/bin/bash

export LOG_DIR=$RESULTS_DIR/$SEED/$RL_AGENT
mkdir -p $LOG_DIR
echo "Snap $RL_AGENT ($SEED) to $LOG_DIR"
echo "java -cp $CLASSPATH ie.nix.examples.snap.SnapLGCDemo\
      -file src/main/resources/ie/nix/rl/peersim/$RL_AGENT.properties\
      random.seed=$SEED\
      log_dir=$LOG_DIR\
      checkpoint_dir=$LOG_DIR\
      >$LOG_DIR/log.txt 2>&1"
java -cp $CLASSPATH ie.nix.examples.snap.SnapLGCDemo\
      -file src/main/resources/ie/nix/rl/peersim/$RL_AGENT.properties\
      random.seed=$SEED\
      log_dir=$LOG_DIR\
      checkpoint_dir=$LOG_DIR\
      >$LOG_DIR/log.txt 2>&1
