
if [[ -z "$EXAMPLE" ]]; then
    echo "Must provide EXAMPLE in environment, for example export EXAMPLE=Example_s_2" 1>&2
    exit 1
fi

for model in QLearning QDLearning QLambdaLearning QDLambdaLearning
do
	export RL_AGENT=$model
	echo "sbatch --job-name=$RL_AGENT --output=$RL_AGENT.out kay/rl/rl_agent_devq.sbatch"
	sbatch --job-name=$RL_AGENT --output=$RL_AGENT.out kay/rl/rl_agent_devq.sbatch
done
