#!/bin/bash

export EXAMPLE_FILE=src/main/resources/ie/nix/examples/rl/${EXAMPLE}.properties
export MODEL_FILE=src/main/resources/ie/nix/rl/peersim/${RL_AGENT}.properties

for STEP_SIZE in 0.05 0.025 0.01 0.005 ;
do
  export STEP_SIZE
  if [[ $MODE = "job_per_example" ]]; then
    export JOB_NAME="${RL_AGENT}_s_${STEP_SIZE}"
    export RL_AGENT_SCRIPT=kay/rl/${RL_AGENT}Example.sh
    echo "sbatch --job-name=$JOB_NAME --output=$JOB_NAME.out kay/rl/rl_agent_prodq.sbatch"
    sbatch --job-name=$JOB_NAME --output=$JOB_NAME.out kay/rl/rl_agent_prodq.sbatch
  elif [[ $MODE = "job_per_examples" ]]; then
    kay/rl/${RL_AGENT}Example.sh
  fi
done
