#!/bin/bash

export LOG_DIR=$RESULTS_DIR/$SEED/${RL_AGENT}_is_${INNOVATION_STEP_SIZE}_cs_${CONSENSUS_STEP_SIZE}_ed_${ELIGIBITY_DECAY}
mkdir -p $LOG_DIR
cp $EXAMPLE_FILE $LOG_DIR
cp $MODEL_FILE $LOG_DIR
export EXAMPLE_MODEL_FILE=$LOG_DIR/$RL_AGENT.properties
echo "protocol.rl_agent.innovation_step_size=$INNOVATION_STEP_SIZE" >> $EXAMPLE_MODEL_FILE
echo "protocol.rl_agent.consensus_step_size=$CONSENSUS_STEP_SIZE" >> $EXAMPLE_MODEL_FILE
echo "protocol.rl_agent.eligibility_decay=$ELIGIBITY_DECAY" >> $EXAMPLE_MODEL_FILE
echo "Running EXAMPLE=$EXAMPLE Seed=$SEED Model=$RL_AGENT INNOVATION_STEP_SIZE=$INNOVATION_STEP_SIZE CONSENSUS_STEP_SIZE=$CONSENSUS_STEP_SIZE ELIGIBITY_DECAY=$ELIGIBITY_DECAY"
echo "        LOG_DIR=$LOG_DIR"
java -cp $CLASSPATH\
  ie.nix.examples.rl.Experiment\
  -file $EXAMPLE_FILE\
  -file $EXAMPLE_MODEL_FILE\
  random.seed=$SEED\
  log_dir=$LOG_DIR\
  checkpoint_dir=$LOG_DIR\
  >$LOG_DIR/log.txt 2>&1
