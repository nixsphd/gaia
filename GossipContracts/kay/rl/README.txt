
# Pick the experiment
export EXAMPLE=Example_s_2
export EXAMPLE=Example_s_4
export EXAMPLE=Example_s_8
export EXAMPLE=Example_s_16
export EXAMPLE=Example_s_32

# Main way to run a new experiment
export MODE=job_per_example
kay/rl/example.sh
# Or
kay/rl/example_batch1.sh
# Then later run
kay/rl/example_batch2.sh

# Main way to process the results
source src/main/jupyter/venv/bin/activate
python3 src/main/jupyter/experiments.py results/rl/examples/${EXAMPLE}
# or
python3 src/main/jupyter/experiments.py results/rl/examples/${EXAMPLE} Policy

# Check progress
wc -l results/rl/examples/Example_s_2/0/*/AVF.csv | grep -v 40001
wc -l results/rl/examples/Example_s_4/0/*/AVF.csv | grep -v 80001
wc -l results/rl/examples/Example_s_8/0/*/AVF.csv | grep -v 160001
wc -l results/rl/examples/Example_s_16/0/*/AVF.csv | grep -v 320001
wc -l results/rl/examples/Example_s_32/0/*/AVF.csv | grep -v 640001

# Slower way with a job per Agent
export EXAMPLE=Example_s_2
export MODE=job_per_examples
export RL_AGENT=QLearning
export RL_AGENT=QDLearning
export RL_AGENT=QLambdaLearning
export RL_AGENT=QDLambdaLearning
export RL_AGENT_SCRIPT=kay/rl/${RL_AGENT}Examples.sh
sbatch --job-name=$RL_AGENT --output=$RL_AGENT.out kay/rl/rl_agent_prodq.sbatch
