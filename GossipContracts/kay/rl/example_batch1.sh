
if [[ -z "$EXAMPLE" ]]; then
    echo "Must provide EXAMPLE in environment, for example export EXAMPLE=Example_s_2" 1>&2
    exit 1
else
    echo "EXAMPLE is $EXAMPLE"
fi

if [[ -z $MODE ]]; then
    echo "Must provide $MODE in environment, for example export MODE=job_per_example or export MODE=job_per_examples" 1>&2
    exit 1
elif [[ $MODE = "job_per_example" ]]; then
    echo "Mode is job_per_example"
elif [[ $MODE = "job_per_examples" ]]; then
    echo "Mode is job_per_examples"
else
    echo "Mode is not recognised"
    exit 1
fi

for model in QLearning QDLearning QLambdaLearning ;
do
  export RL_AGENT=$model
  if [[ $MODE = "job_per_example" ]]; then
      kay/rl/${RL_AGENT}Examples.sh
  elif [[ $MODE = "job_per_examples" ]]; then
      export RL_AGENT_SCRIPT=kay/rl/${RL_AGENT}Examples.sh
      echo "sbatch --job-name=$RL_AGENT --output=$RL_AGENT.out kay/rl/rl_agent_prodq.sbatch"
      sbatch --job-name=$RL_AGENT --output=$RL_AGENT.out kay/rl/rl_agent_prodq.sbatch
  fi
done